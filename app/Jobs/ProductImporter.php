<?php

namespace App\Jobs;

use App\Helpers\Helper;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Quantity;
use App\Models\Store;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductImporter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $chunk;
    private $auth_user;

    public function __construct($chunk, $auth_user)
    {
        $this->chunk = $chunk;
        $this->auth_user = $auth_user;
    }

    public function handle()
    {
        $rows = $this->chunk;
        foreach ($rows as $row) {
            // dd($row);
            if (isset($row['nom']) /*&& isset($row['maison'])*/) {
                $currencyValue = $this->getCurrencyValue($row['devise']);

                $product = $this->findExistingProduct($row['nom'], ($row['maison'] ?? ''), ($row['sku'] ?? ''));

                if ($product) {
                    $this->updateProduct($product, $row, $currencyValue);
                } else {
                    $product = $this->createProduct($row, $currencyValue);
                }

                $this->updateProductAttributes($product, $row);
                $this->updateProductCategories($product, $row);
                $this->updateProductImage($product, $row);
            }
        }
    }

    private function getCurrencyValue($symbol)
    {
        return Currency::where('symbol', $symbol)->value('value') ?? 1;
    }

    private function findExistingProduct($name, $maison, $sku)
    {
        if($sku && $sku != ''){
            $product = Product::where('sku', $sku)->first();
            if($product){
                return $product;
            }
        }
        if($maison && $maison != ''){
            return Product::where('name', $name)
                ->whereHas('productAttributes', function ($query) use ($maison) {
                    $query->whereHas('attribute', function ($q) {
                        $q->where('name', 'دار النشر');
                    });
                    $query->whereHas('values', function ($q) use ($maison) {
                        $q->whereHas('attributeValue', function ($q) use ($maison) {
                            $q->where('value', $maison);
                        });
                    });
                })
                ->first();
        }
        return Product::where('name', $name)->first();
    }

    private function updateProduct($product, $row, $currencyValue)
    {
        $row = $this->convertCurrencyValues($row, $currencyValue);

        $product->update([
            'name' => $row['nom'],
            'buying' => $row['prix_achat'],
            'selling1' => $row['prix_de_vente_1'],
            'selling2' => $row['prix_de_vente_2'],
            'selling3' => $row['prix_de_vente_3'],
            'tva' => $row['tva']
        ]);

        $quantity = Quantity::firstOrCreate([
            'product_id' => $product->id,
            'store_id' => $this->auth_user->store->id,
        ]);

        Helper::updateQuantity(null, 'Importation Excel: ', ($row['quantite'] ?? 0), $quantity, 'add', $this->auth_user->name);
    }

    private function convertCurrencyValues($row, $currencyValue)
    {
        $row['prix_achat'] = isset($row['prix_achat']) ? number_format((float)$row['prix_achat'] * $currencyValue, 3) : 0;
        $row['prix_de_vente_1'] = isset($row['prix_de_vente_1']) ? number_format((float)$row['prix_de_vente_1'] * $currencyValue, 3) : 0;
        $row['prix_de_vente_2'] = isset($row['prix_de_vente_2']) ? number_format((float)$row['prix_de_vente_2'] * $currencyValue, 3) : 0;
        $row['prix_de_vente_3'] = isset($row['prix_de_vente_3']) ? number_format((float)$row['prix_de_vente_3'] * $currencyValue, 3) : 0;

        return $row;
    }

    private function createProduct($row, $currencyValue)
    {
        $row = $this->convertCurrencyValues($row, $currencyValue);

        $edition = Attribute::where('name', 'الطبعة')->first();
        $row['edition'] = $row['edition'] ?? 'الطبعة الاولى';
        $name = AttributeValue::firstOrCreate(['value' => $row['edition'], 'attribute_id' => $edition->id]);

        $product = Product::create([
            'name' => $row['nom'],
            'status' => $row['actif'] ?? 1,
            'sku' => $row['sku'] ?? null,
            'description' => $row['description'] ?? '',
            'tva' => $row['tva'] ?? 0,
            'maxdiscount' => $row['maxdiscount'] ?? 100,
            'discount' => $row['discount'] ?? 0,
            'minimum_quantity' => $row['quantite_minimal'] ?? 0,
            'reserve_quantity' => $row['quantite_reserve'] ?? 0,
            'buying' => $row['prix_achat'],
            'selling1' => $row['prix_de_vente_1'],
            'selling2' => $row['prix_de_vente_2'],
            'selling3' => $row['prix_de_vente_3'],
            'currency_id' => '1',
            'quantity_type' => ["type" => 'Dépend d\'un attribut', "attributes" => [$edition->id]],
            'price_type' => ["type" => "Simple"],
            'unity' => 'Pièce',
        ]);

        $product->productAttributes()->create(['attribute_id' => $edition->id]);
        $attr_id = $product->productAttributes()->first();
        $attr_id->values()->create(['attribute_value_id' => $name->id]);
        $value_id = $product->productAttributes()->first()->values()->first()->id;

        $quantity = Quantity::firstOrCreate([
            'product_id' => $product->id,
            'store_id' => $this->auth_user->store->id,
            'product_attribute1' => $attr_id->id,
            'product_value1' =>  $value_id,
        ], ['quantity' => ($row['quantite'] ?? 0)]);

        Helper::updateQuantity(null, 'Importation Excel: ', ($row['quantite'] ?? 0), $quantity, 'forced', $this->auth_user->name);

        $stores = Store::whereNot('id', $this->auth_user->store->id)->get();

        foreach ($stores as $store) {
            $quantity = Quantity::firstOrCreate([
                'product_id' => $product->id,
                'product_attribute1' => $attr_id->id,
                'product_value1' =>  $value_id,
                'store_id' => $store->id,
            ]);
    
            Helper::updateQuantity(null, 'Importation Excel: ', 0, $quantity, 'forced', $this->auth_user->name);
        }

        return $product;
    }

    private function updateProductAttributes($product, $row)
    {
        $attributeMappings = [
            'nom_dauteur' => 'اسم المؤلف',
            'maison' => 'دار النشر',
            'nombre_de_pages' => 'عدد الصفحات',
            'nombre_de_tomes' => 'عدد المجلدات',
            'type_de_papiers' => 'نوعية الورق',
            'nom_denqueteur' => 'المحقق',
            'poids' => 'الوزن',
            'taille' => 'الحجم',
            'reliure' => 'التجليد',
            'marque' => 'Marque',
            'fournisseur' => 'Fournisseur',
        ];

        foreach ($attributeMappings as $attributeKey => $attributeName) {
            if (isset($row[$attributeKey]) && $row[$attributeKey] != '') {
                $attribute = Attribute::firstOrCreate(['name' => $attributeName]);
                $value = AttributeValue::firstOrCreate(['value' => $row[$attributeKey], 'attribute_id' => $attribute->id]);

                $product->productAttributes()->where('attribute_id', $attribute->id)->delete();
                $product->productAttributes()->create(['attribute_id' => $attribute->id])->values()->create(['attribute_value_id' => $value->id]);
            }
        }
    }

    private function updateProductCategories($product, $row)
    {
        if (isset($row['categories']) && !empty($row['categories'])) {
            $category = Category::firstOrCreate(['name' => $row['categories']]);
            $product->categories()->firstOrCreate(['category_id' => $category->id]);
        }
    }

    private function updateProductImage($product, $row)
    {
        if (isset($row['image']) && !empty($row['image'])) {
            $product->images()->create(['url' => '/storage/uploads/products/images/' . $row['image'], 'is_default' => 1]);
        }
    }
}
