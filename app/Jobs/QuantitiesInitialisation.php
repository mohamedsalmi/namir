<?php

namespace App\Jobs;

use App\Models\Attribute;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class QuantitiesInitialisation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $store;
    private $chunk;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Store $store,$chunk)
    {
       $this->store= $store;
       $this->chunk= $chunk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $store=$this->store;
            $products=$this->chunk;
            DB::beginTransaction();
            $edition = Attribute::where('name', 'الطبعة')->first();
            foreach ($products as $key => $product) {
                $productAttribute1 = $product->productAttributes->where('attribute_id', $edition->id)->first();
                $value1=$product->productAttributes()->first()->values()->where(['attribute_value_id' => $edition->values()->first()->id])->first();
                    $quantity = [
                        'product_attribute1' => $productAttribute1->id,
                        'product_value1' => $value1->id,
                        'store_id' => $store->id,
                    ];
                    $product->quantities()->create($quantity);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }
}
