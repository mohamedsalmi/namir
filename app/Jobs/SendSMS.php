<?php

namespace App\Jobs;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendSMS //implements ShouldQueue
{
    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $to;
    private $message;
    private $response;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($to, $message)
    {
        $this->to = $to;
        $this->message = $message;
    }

    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $myMobile = $this->to;
        $mySms = str_replace(" ", "+", $this->message);
        $mySender = 'TunSMS+Test';
        $myDate = '12/84/2020';
        $myTime = '20:52';

        $Url = "https://www.tunisiesms.tn/client/Api/Api.aspx?fct=sms&key=MYKEY&mobile=1d&sender=YVYYY&date=jj/mm/aaaa&heure=hh:mm:55";
        // $Url = "https://www.tunisiesms.tn/client/Api/Api.aspx?fct=sms&key=IMcTQzN8TcvEbTRdrBTS9ZtYyDDL61Ba2IQleQ6UtJgc4pkcAZ05OvT8bDyP71XrAWVRgAyvjCEL/-/e6KuRZ/lFzWoO5B88hVpo6pG1Mr1IM=&mobile=" . $myMobile . "&sms=" . $mySms . "&sender=" . $mySender;

        try {
            $client = new Client();
            $result = $client->get($Url);
            if ($result) {
                $result_status = $result->getStatusCode();
                $result_contents = $result->getBody()->getContents();
                //The response from the providers might be different from this.
                //Customise according to the response.
                if (!isset($result_contents))
                    $result_contents = "No Response from operator";
            } else {
                $result_contents = "No Response from operator";
            }
            $contents = str_contains($result_contents, 'code:');
            $xml = simplexml_load_string($result_contents);
            $response_json = json_encode($xml);
            $response_array = json_decode($response_json, TRUE);
            // dd($result_status, $response_array);
            
            return ['status_code' => $response_array['status']['status_code'], 'status_msg' => str_replace("![CDATA[OK]]", "Envoyé avec succès", $response_array['status']['status_msg'])];
            // if($result_status == 200 && ($contents == FALSE))
            // { 
            //     // SMS send successfully. You can update any related tables, when queue finished and much more here 
            // }elseif($result_status == 200 && ($contents == TRUE)){
            //     // Error
            //     // SMS sending failed. You can update any related tables, when queue finished and much more here 
            // }else{
            //     // Couldn't connect with operator
            //     // SMS sending failed. You can update any related tables, when queue finished and much more here
            // }
            // //DB::beginTransaction();
            // try {
            //     //any business logic
            //     //DB::commit();// all good
            // }catch (\Exception $e) {
            //     //DB::rollback();// something went wrong
            //     $this->failed($e,$result_contents);
            // }
        } catch (\Exception $e) {
            $this->failed($e, $result_contents);
        }
    }

    public function failed(Exception $exception, $result_contents)
    {
        Log::error($result_contents);
        Log::error($exception->getMessage());
        //DB::beginTransaction();
        try {
            //any business logic
            //DB::commit();// all good
        } catch (Exception $e) {
            //DB::rollback();// something went wrong
        }
    }
}
