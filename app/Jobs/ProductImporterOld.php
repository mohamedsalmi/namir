<?php

namespace App\Jobs;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;


class ProductImporterOld implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $chunk;
    private $auth_user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($chunk, $auth_user)
    {
        $this->chunk = $chunk;
        $this->auth_user = $auth_user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $rows = $this->chunk;
        foreach ($rows as $row) {
            $product = Product::where('name', $row['nom'])->first();
            if ($product) {
                if (!isset($row['prix_de_vente_1'])) {
                    $row['prix_de_vente_1'] = 0;
                }
                if (!isset($row['prix_de_vente_2'])) {
                    $row['prix_de_vente_2'] = 0;
                }
                $product->update([
                    'selling1' => $row['prix_de_vente_1'],
                    'selling2' =>  $row['prix_de_vente_2']
                ]);
            }else{
                if (!isset($row['edition'])) {
                    $row['edition'] = 'الطبعة الاولى';
                }
                if (!isset($row['maxdiscount'])) {
                    $row['maxdiscount'] = 100;
                }
                if (!isset($row['discount'])) {
                    $row['discount'] = 0;
                }
                if (!isset($row['quantite_minimal'])) {
                    $row['quantite_minimal'] = 0;
                }
                if (!isset($row['quantite_reserve'])) {
                    $row['quantite_reserve'] = 0;
                }

                if (!isset($row['prix_de_vente_1'])) {
                    $row['prix_de_vente_1'] = 0;
                }
                if (!isset($row['prix_achat'])) {
                    $row['prix_achat'] = $row['prix_de_vente_1'];
                }
                if (!isset($row['prix_de_vente_2'])) {
                    $row['prix_de_vente_2'] = 0;
                }
                if (!isset($row['prix_de_vente_3'])) {
                    $row['prix_de_vente_3'] = 0;
                }
                if (!isset($row['actif'])) {
                    $row['actif'] = 1;
                }
                if (!isset($row['tva'])) {
                    $row['tva'] = 0;
                }
                if (!isset($row['description'])) {
                    $row['description'] = '';
                }
                $edition = Attribute::where('name', 'الطبعة')->first();
                $name = AttributeValue::where(['value' => $row['edition'], 'attribute_id' => $edition->id])->first();
                $name = $name ? $name : AttributeValue::create(['value' => $row['edition'], 'attribute_id' => $edition->id]);
                $product = Product::create([
                    'name'     => $row['nom'],
                    'status' => $row['actif'],
                    // 'reference'     => $row['reference'],
                    // 'sku'     => $row['sku'],
                    'description'     => $row['description'],
                    'tva'     => $row['tva'],
                    'maxdiscount'     => $row['maxdiscount'],
                    'discount'     => $row['discount'],
                    'minimum_quantity'    => $row['quantite_minimal'],
                    'reserve_quantity'    => $row['quantite_reserve'],
                    'buying'    => $row['prix_achat'],
                    'selling1'    => $row['prix_de_vente_1'],
                    'selling2'    => $row['prix_de_vente_2'],
                    'selling3'    => $row['prix_de_vente_3'],
                    'currency_id'    => '1',
                    'quantity_type'    => ["type" => 'Dépend d\'un attribut', "attributes" => [$edition->id]],
                    'price_type'  => ["type" => "Simple"]
                ]);

                $product->productAttributes()->create(['attribute_id' => $edition->id]);
                $attr_id = $product->productAttributes()->first();
                $attr_id->values()->create(['attribute_value_id' => $name->id]);
                $value_id = $product->productAttributes()->first()->values()->first()->id;
                if (!isset($row['quantite'])) {
                    $row['quantite'] = 0;
                }
                $quantity = [
                    'product_attribute1' => $attr_id->id,
                    'product_value1' =>  $value_id,
                    'store_id' => $this->auth_user->store->id,
                    'quantity' => $row['quantite'],
                ];
                $product->quantities()->create($quantity);
                $stores = Store::whereNot('id', $this->auth_user->store->id)->get();
                foreach ($stores as $i => $store) {
                    $quantity = [
                        'product_attribute1' => $attr_id->id,
                        'product_value1' =>  $value_id,
                        'store_id' => $store->id,
                    ];
                    $product->quantities()->create($quantity);
                }
                //*************************************************** */
                if (isset($row['non_dauteur'])) {

                    $auteur = Attribute::where('name', 'اسم المؤلف')->first();
                    $name = AttributeValue::where(['value' => $row['non_dauteur'], 'attribute_id' => $auteur->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['non_dauteur'], 'attribute_id' => $auteur->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $auteur->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['maison'])) {

                    $maison = Attribute::where('name', 'دار النشر')->first();
                    $name = AttributeValue::where(['value' => $row['maison'], 'attribute_id' => $maison->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['maison'], 'attribute_id' => $maison->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $maison->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['nombre_de_pages'])) {
                    $pages = Attribute::where('name', 'عدد الصفحات')->first();
                    $name = AttributeValue::where(['value' => $row['nombre_de_pages'], 'attribute_id' => $pages->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['nombre_de_pages'], 'attribute_id' => $pages->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $pages->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['nombre_de_tomes'])) {

                    $tomes = Attribute::where('name', 'عدد المجلدات')->first();
                    $name = AttributeValue::where(['value' => $row['nombre_de_tomes'], 'attribute_id' => $tomes->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['nombre_de_tomes'], 'attribute_id' => $tomes->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $tomes->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['type_de_papiers'])) {

                    $papier = Attribute::where('name', 'نوعية الورق')->first();
                    $name = AttributeValue::where(['value' => $row['type_de_papiers'], 'attribute_id' => $papier->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['type_de_papiers'], 'attribute_id' => $papier->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $papier->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['non_denqueteur'])) {

                    $enqueteur = Attribute::where('name', 'المحقق')->first();
                    $name = AttributeValue::where(['value' => $row['non_denqueteur'], 'attribute_id' => $enqueteur->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['non_denqueteur'], 'attribute_id' => $enqueteur->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $enqueteur->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['poids'])) {
                    $weight = Attribute::where('name', 'الوزن')->first();
                    $name = AttributeValue::where(['value' => $row['poids'], 'attribute_id' => $weight->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['poids'], 'attribute_id' => $weight->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $weight->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['taille'])) {
                    $taille = Attribute::where('name', 'الحجم')->first();
                    $name = AttributeValue::where(['value' => $row['taille'], 'attribute_id' => $taille->id])->first();
                    $name = $name ? $name : AttributeValue::create(['value' => $row['taille'], 'attribute_id' => $taille->id]);
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $taille->id]);
                    $productAttribute->values()->create(['attribute_value_id' => $name->id]);
                }
                //************************************ */
                if (isset($row['categories']) && !empty($row['categories'])) {
                    /***  many catgs */
                    // foreach ($row['categories'] as $key => $cat) {
                    // if (!empty($cat)) {

                    /**  import categories with name */

                    $category = Category::where('name', $row['categories'])->first();
                    $category = $category ? $category : Category::create(['name' => $row['categories']]);
                    $product->categories()->create(['category_id' => $category]);

                    /**  import categories with id */
                    // $product->categories()->create(['category_id' => $row['categories']]);
                    // }
                    // }
                }
                if (isset($row['image']) && !empty($row['image'])) {
                    $product->images()->create(['url' => '/storage/uploads/products/images/' . $row['image'], 'is_default' => 1]);
                }
            }
        }
    }
}
