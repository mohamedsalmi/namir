<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentCart extends Model
{
    use HasFactory;
    protected $fillable = [
        'financial_commitment_id',
        'client_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'numbre',
        'bank',
        'created_by',
        'updated_by',
    ];
    protected $casts = [
        'details' => 'array',
    ];
    public function commitment() {
        return $this->belongsTo(FinancialCommitment::class, 'financial_commitment_id');
    }
}
