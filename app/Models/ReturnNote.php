<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnNote extends Model
{
    use HasFactory;

    protected $fillable = [
        'note',
        'client_id',
        'store_id',
        'user_id',
        'cart_id',
        'status',
        'paiment_status',
        'client_details',
        'number',
        'total',
        'created_by',
        'updated_by',
        'timbre',
        'codification',
        'date'
    ];

    protected $casts = [
        'client_details' => 'array',
    ];

    public function items() {
        return $this->hasMany(ReturnNoteItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }
    public function invoice() {
        return $this->belongsTo(Cart::class);
    }

    public function commitments() {
        return $this->hasMany(ReturnNoteCommitment::class);
    }
    public function payments()
    {
        return $this->hasManyThrough(ReturnNotePayment::class, ReturnNoteCommitment::class);
    }
}
