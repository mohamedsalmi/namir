<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPricing extends Model
{
    use HasFactory;

    protected $fillable = [
        'discount',
        'price_cv',
        'date_cv',
        'client_id',
        'product_id',
    ];
}
