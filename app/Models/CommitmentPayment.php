<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommitmentPayment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'voucher_commitment_id',
        'provider_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'numbre',
        'bank',
        'created_by',
        'updated_by',
    ];
    protected $casts = [
        'details' => 'array',
    ];
    public function commitment() {
        return $this->belongsTo(ProviderCommitment::class, 'provider_commitment_id');
    }
}
