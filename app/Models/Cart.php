<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'note',
        'store_id',
        'user_id',
        'invoice_id',
        'status',
        'return_status',
        'paiment_status',
        'type',
        'client_details',
        'number',
        'total',
        'created_by',
        'updated_by',
        'timbre',
        'date',
        'codification'
    ];
    protected $casts = [
        'client_details' => 'array',
    ];
    public function items() {
        return $this->hasMany(CartsItems::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }
    
    public function commitments() {
        return $this->hasMany(FinancialCommitment::class);
    }
    public function client() {
        return $this->belongsTo(Client::class,'client_id');
    }
    public function invoice() {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }

    public function returnNote() {
        return $this->hasOne(ReturnNote::class);
    }
    public function payments()
    {
        return $this->hasManyThrough(PaymentCart::class, FinancialCommitment::class);
    }
}
