<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentCreditnote extends Model
{
    use HasFactory;

    protected $fillable = [
        'creditnote_commitment_id',
        'client_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'numbre',
        'bank',
        'created_by',
        'updated_by',
    ];
    
    protected $casts = [
        'details' => 'array',
    ];
    public function commitment() {
        return $this->belongsTo(CreditnoteCommitment::class, 'creditnote_commitment_id');
    }
}
