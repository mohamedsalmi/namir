<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditnoteCommitment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'credit_note_id',
        'amount',
        'date',
        'paiment_status',
    ];

    protected $appends = [
        'parent',
    ];

    public function getParentAttribute() {
        return $this->creditnote;
    }

    public function paiements()
    {
        return $this->hasMany(PaymentCreditnote::class);
    }
    public function creditnote()
    {
        return $this->belongsTo(CreditNote::class,'credit_note_id');
    }

}
