<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnNoteItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_note_id',
        'product_id',
        'product_name',
        'product_quantity',
        'product_tva',
        'quantity_id',
        'price_id',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'product_currency_id',
        'product_currency_value',
        'options'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    
    public function price()
    {
        return $this->belongsTo(Price::class);
    }
}
