<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'url',
        'created_by',
        'updated_by',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if($image){
            return $image->url;
        }
        return config('stock.image.default.provider');
    }
}

