<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sku',
        'reference',
        'description',
        'price_type',
        'quantity_type',
        'negative_inventory',
        'status',
        'unity',
        'tva',
        'metatitle',
        'metakeys',
        'metadescription',
        'currency_id',
        'minimum_quantity',
        'reserve_quantity',
        'buying',
        'selling1',
        'selling2',
        'selling3',
        'discount',
        'by_order',
        'maxdiscount',
        'resume',
    ];

    protected $casts = [
        'price_type' => 'array',
        'quantity_type' => 'array',
    ];

    protected $appends = [
        'default_image_url',
        'writer', 'house', 'data', 'categories_names', 'check_wishlist',
    ];

    public function getCategoriesNamesAttribute()
    {
        return $this->categories->pluck("category.name", "category.id");
    }

    public function getProducAttributeValue($attribute)
    {
        $writerProductAttribute = ProductAttribute::where('product_id', $this->id)->whereHas('attribute', function ($q) use ($attribute) {
            $q->where('name', $attribute);
        })->first();
        return $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';
    }

    public function getDataAttribute()
    {
        $writerProductAttribute = ProductAttribute::where('product_id', $this->id)->whereHas('attribute', function ($q) {
            $q->where('name', 'اسم المؤلف');
        })->first();
        $writerName =  $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';
        return (object)[
            'writer' => $this->writer,
            'publisher' => $this->house,
        ];
    }

    public function getDefaultImageUrlAttribute()
    {
        $image = $this->images()->where('is_default', true)->first();
        if ($image) {
            return $image->url;
        }
        return config('stock.image.default.product');
    }
    public function getWriterAttribute()
    {
        $writerProductAttribute = ProductAttribute::where('product_id', $this->id)->whereHas('attribute', function ($q) {
            $q->where('name', 'اسم المؤلف');
        })->first();
        $writerName =  $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';
        return $writerName;
    }
    public function getHouseAttribute()
    {
        $writerProductAttribute = ProductAttribute::where('product_id', $this->id)->whereHas('attribute', function ($q) {
            $q->where('name', 'دار النشر');
        })->first();
        $writerName =  $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';
        return $writerName;
    }

    public function values()
    {
        // return $this->hasManyThrough(PaymentCart::class, FinancialCommitment::class);
        return $this->hasManyThrough(ProductAttributeValue::class, ProductAttribute::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function categories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'product_attributes', 'product_id', 'attribute_id');
    }

    public function quantities()
    {
        return $this->hasMany(Quantity::class);
    }

    public function barcodes()
    {
        return $this->hasMany(Barcode::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function cartItems()
    {
        return $this->hasMany(CartsItems::class);
    }

    public function getCheckWishlistAttribute()
    {
        $wishlist = Wishlist::where(['product_id' => $this->id, 'client_id' => auth()->user()->id ?? null])->first();
        if ($wishlist) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the product's image.
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function pricings()
    {
        return $this->hasMany(ClientPricing::class);
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class, 'carts_items', 'product_id', 'cart_id')->distinct();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_items', 'product_id', 'order_id')->distinct();
    }

    public function onlinesales()
    {
        return $this->belongsToMany(OnlineSale::class, 'online_sale_items', 'product_id', 'online_sale_id')->distinct();
    }

    public function onlinesaleitems()
    {
        return $this->hasMany(OnlineSaleItem::class);
    }

    public function orderitems()
    {
        return $this->hasMany(OrderItem::class);
    }
}
