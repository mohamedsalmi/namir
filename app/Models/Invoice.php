<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'note',
        'store_id',
        'user_id',
        'status',
        'paiment_status',
        'client_details',
        'number',
        'total',
        'created_by',
        'updated_by',
        'timbre',
        'codification',
        'creditnote_id',
        'date'
    ];

    protected $casts = [
        'client_details' => 'array',
    ];

    public function items() {
        return $this->hasMany(InvoiceItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function carts() {
        return $this->hasMany(Cart::class, 'invoice_id');
    }  

    public function onlinesales() {
        return $this->hasMany(OnlineSale::class, 'invoice_id');
    }  

    public function orders() {
        return $this->hasMany(Order::class, 'invoice_id');
    } 
}
