<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    use HasFactory;
    protected $fillable = [
    'inventory_id',
    'product_id',
    'product_name',
    'product_old_quantity',
    'product_new_quantity',
    'product_unity',
    'pattern'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class);
    } 
    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
