<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnNotePayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_note_commitment_id',
        'client_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'numbre',
        'bank',
        'created_by',
        'updated_by',
    ];
    
    protected $casts = [
        'details' => 'array',
    ];
    public function commitment() {
        return $this->belongsTo(ReturnNoteCommitment::class, 'return_note_commitment_id');
    }
}
