<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProviderCommitment extends Model
{
    use HasFactory;

    protected $fillable = [
        'voucher_id',
        'amount',
        'date',
        'paiment_status'
    ];

    protected $appends = [
        'parent',
    ];

    public function getParentAttribute() {
        return $this->voucher;
    }

    public function paiements() {
        return $this->hasMany(CommitmentPayment::class);
    }
    public function voucher() {
        return $this->belongsTo(Voucher::class);
    }
}
