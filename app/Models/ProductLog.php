<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity',
        'motif',
        'action',
        'product_value1',
        'user_name',
        'product_id',
        'store_id',
    ];

    /**
     * Get the parent logable model
     */
    public function logable()
    {
        return $this->morphTo();
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productValue1()
    {
        return $this->belongsTo(ProductAttributeValue::class, 'product_value1');
    }
}
