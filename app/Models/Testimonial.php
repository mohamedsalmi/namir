<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'rating',
        'testimonial',
        'writer_details',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'writer_details' => 'array',
    ];
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if($image){
            return $image->url;
        }
        return config('stock.image.default.provider');
    }
}
