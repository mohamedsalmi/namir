<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'provider_id',
        'store_id',
        'user_id',
        'status',
        'type',
        'provider_details',
        'number',
        'total',
        'timbre',
        'created_by',
        'updated_by',
        'codification',
        'date',
        'paiment_status'
    ];

    protected $casts = [
        'provider_details' => 'array',
    ];

    public function items() {
        return $this->hasMany(VoucherItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function commitments() {
        return $this->hasMany(ProviderCommitment::class);
    }

    public function provider() {
        return $this->belongsTo(Provider::class);
    }

    public function payments()
    {
        return $this->hasManyThrough(CommitmentPayment::class, ProviderCommitment::class);
    }

    public function returnCoupon() {
        return $this->hasOne(ReturnCoupon::class);
    }
}
