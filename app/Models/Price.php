<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;

    protected $fillable = [
        'buying',
        'selling1',
        'selling2',
        'selling3',
        'product_attribute1',
        'product_attribute2',
        'product_value1',
        'product_value2',
        'product_id',
        'store_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function productAttribute1()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute1');
    }

    public function productAttribute2()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute2');
    }

    public function productValue1()
    {
        return $this->belongsTo(ProductAttributeValue::class, 'product_value1');
    }

    public function productValue2()
    {
        return $this->belongsTo(ProductAttributeValue::class, 'product_value2');
    }
}
