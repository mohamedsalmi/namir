<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnPayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_commitment_id',
        'provider_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'numbre',
        'bank',
        'created_by',
        'updated_by',
    ];
    
    protected $casts = [
        'details' => 'array',
    ];
    public function commitment() {
        return $this->belongsTo(ReturnCommitment::class, 'return_commitment_id');
    }
}
