<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable = [
        'codification',
        'note',
        'store_id',
        'user_id',
        'created_by',
        'updated_by',
        'status',
        'number'
    ];
    public function items()
    {
        return $this->hasMany(InventoryItem::class); 
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
     
    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }
    
}
