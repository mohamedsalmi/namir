<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'note',
        'store_id',
        'user_id',
        'invoice_id',
        'status',
        'paiment_status',
        'client_details',
        'number',
        'total',
        'timbre',
        'created_by',
        'updated_by',
        'codification',
        'date'
    ];

    protected $casts = [
        'client_details' => 'array',
    ];

    public function items() {
        return $this->hasMany(CreditNoteItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }
    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }

    public function commitments() {
        return $this->hasMany(CreditnoteCommitment::class);
    }
    public function payments()
    {
        return $this->hasManyThrough(PaymentCreditnote::class, CreditnoteCommitment::class);
    }
    
}
