<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'invoice_id',
        'product_id',
        'product_name',
        'product_quantity',
        'product_tva',
        'quantity_id',
        'price_id',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'product_currency_id',
        'product_currency_value',
        'options'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    public function price()
    {
        return $this->belongsTo(Price::class);
    }
    public function quantity()
    {
        return $this->belongsTo(Quantity::class);
    }
}
