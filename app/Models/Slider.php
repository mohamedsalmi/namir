<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'url',
        'slogan',
        'exclusive',
        'offre',
        'color1',
        'color2',
        'color3',
        'discount',
        'created_by',
        'updated_by',
    ];
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if($image){
            return $image->url;
        }
        return config('stock.image.default.slider');
    }
}
