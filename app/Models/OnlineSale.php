<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnlineSale extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'note',
        'store_id',
        'user_id',
        'invoice_id',
        'status',
        'source',
        'coupon',
        'created_by',
        'updated_by',
        'paiment_status',
        'client_details',
        'number',
        'total',
        'date',
        'codification',
    ];
    protected $casts = [
        'client_details' => 'array',
    ];
    public function items() {
        return $this->hasMany(OnlineSaleItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }
    
    public function invoice() {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }
    public function client() {
        return $this->belongsTo(Client::class,'client_id');
    }
 

  
}
