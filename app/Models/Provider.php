<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'provider_code',
        'email',
        'address',
        'mf',
        'rib',
        'phone',
        'phone2',
        'price',
    ];

    protected $appends = [
        'image_url',
    ];

     /**
     * Get the product's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if($image){
            return $image->url;
        }
        return config('stock.image.default.provider');
    }
}
