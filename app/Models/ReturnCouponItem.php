<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnCouponItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_coupon_id',
        'product_id',
        'product_name',
        'product_quantity',
        'quantity_id',
        'price_id',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'product_currency_id',
        'product_currency_value',
        'options'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function returnCoupon()
    {
        return $this->belongsTo(ReturnCoupon::class);
    }
}
