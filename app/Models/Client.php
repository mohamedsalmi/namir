<?php

namespace App\Models;

use App\Notifications\ShopPasswordReset;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Client  extends Authenticatable
{
    use HasFactory, SoftDeletes;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'site',
        'password',
        'client_code',
        'email',
        'adresse',
        'mf',
        'rib',
        'phone',
        'phone2',
        'price',
        'discount',
        'google_id',
        'state',
        'city',
    ];

    protected $guard = 'shop';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'default_image_url'
    ];
    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if ($image) {
            return $image->url;
        }
        return config('stock.image.default.provider');
    }
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function pricings()
    {
        return $this->hasMany(ClientPricing::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ShopPasswordReset($token));
    }
}
