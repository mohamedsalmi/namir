<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialCommitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'cart_id',
        'amount',
        'date',
        'paiment_status',
    ];

    protected $appends = [
        'parent',
    ];

    public function getParentAttribute() {
        return $this->cart;
    }

    public function paiements()
    {
        return $this->hasMany(PaymentCart::class);
    }
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}