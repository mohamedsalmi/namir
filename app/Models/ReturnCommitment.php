<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnCommitment extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_coupon_id',
        'amount',
        'date',
        'paiment_status'
    ];

    protected $appends = [
        'parent',
    ];

    public function getParentAttribute() {
        return $this->returnCoupon;
    }

    public function paiements() {
        return $this->hasMany(ReturnPayment::class);
    }
    
    public function returnCoupon() {
        return $this->belongsTo(ReturnCoupon::class);
    }
}
