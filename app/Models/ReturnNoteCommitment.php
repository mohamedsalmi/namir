<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnNoteCommitment extends Model
{
    use HasFactory;

    protected $fillable = [
        'return_note_id',
        'amount',
        'date',
        'paiment_status',
    ];

    protected $appends = [
        'parent',
    ];

    public function getParentAttribute() {
        return $this->returnnote;
    }

    public function paiements()
    {
        return $this->hasMany(ReturnNotePayment::class);
    }
    public function returnnote()
    {
        return $this->belongsTo(ReturnNote::class,'return_note_id');
    }
}
