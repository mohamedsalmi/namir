<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleQuote extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'client_id',
        'store_id',
        'user_id',
        'status',
        'client_details',
        'number',
        'total',
        'created_by',
        'updated_by',
        'timbre',
        'codification',
        'date'
    ];

    protected $casts = [
        'client_details' => 'array',
    ];

    public function items() {
        return $this->hasMany(SaleQuoteItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }
}
