<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity',
        'minimum_quantity',
        'product_attribute1',
        'product_attribute2',
        'product_value1',
        'product_value2',
        'barcode_id',
        'product_id',
        'store_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function barcode()
    {
        return $this->belongsTo(Barcode::class);
    }

    public function productAttribute1()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute1');
    }

    public function productAttribute2()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute2');
    }

    public function productValue1()
    {
        return $this->belongsTo(ProductAttributeValue::class, 'product_value1');
    }

    public function productValue2()
    {
        return $this->belongsTo(ProductAttributeValue::class, 'product_value2');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
