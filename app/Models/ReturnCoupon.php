<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnCoupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'note',
        'voucher_id',
        'provider_id',
        'store_id',
        'user_id',
        'status',
        'created_by',
        'updated_by',
        'type',
        'provider_details',
        'number',
        'total',
        'timbre',
        'date',
        'codification'
    ];

    protected $casts = [
        'provider_details' => 'array',
    ];
    public function items() {
        return $this->hasMany(ReturnCouponItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }

    public function commitments() {
        return $this->hasMany(ReturnCommitment::class);
    }

    public function provider() {
        return $this->belongsTo(Provider::class);
    }

    public function voucher() {
        return $this->belongsTo(Voucher::class);
    }

    public function payments()
    {
        return $this->hasManyThrough(ReturnPayment::class, ReturnCommitment::class);
    }
    
}
