<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'is_default',
    ];

    /**
     * Get the parent imageable model
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'imageable_id');
    }
    public function client()
    {
        return $this->belongsTo(Client::class,'imageable_id');
    }
}
