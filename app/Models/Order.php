<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'note',
        'store_id',
        'invoice_id',
        'created_by',
        'updated_by',
        'user_id',
        'status',
        'client_details',
        'number',
        'total',
        'timbre',
        'date',
        'codification'
    ];

    protected $casts = [
        'client_details' => 'array',
    ];
 
    public function items() {
        return $this->hasMany(OrderItem::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }
    
    public function invoice() {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }
    public function client() {
        return $this->belongsTo(Client::class);
    }
    
}
