<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mu extends Model
{
    use HasFactory;
    protected $fillable = [
        'store_origin',
        'note',
        'store_destination',
        'user_id',
        'created_by',
        'updated_by',
        'status',
        'type',
        'number',
        'codification',
        'date'
    ];

    public function items() {
        return $this->hasMany(MuItems::class);
    }

    public function productlog()
    {
        return $this->morphOne(ProductLog::class, 'logable');
    }
    
    public function storeOrigin() {
        return $this->belongsTo(Store::class,'store_origin');
    }
    public function storeDestination() {
        return $this->belongsTo(Store::class,'store_destination');
    }
    
}
