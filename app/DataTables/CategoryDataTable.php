<?php

namespace App\DataTables;

use App\Models\Category;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Carbon\Carbon;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn("created_at", function (Category $category  ) {
                if ($category->created_at!= null){
                return Carbon::createFromFormat('Y-m-d H:i:s', $category->created_at)->format('d/m/y H:i');
                }else {
                    return '';
                }
                })
              ->editColumn("updated_at", function (Category $category) {
                if ($category->created_at!= null){
                 return Carbon::createFromFormat('Y-m-d H:i:s', $category->updated_at)->format('d/m/y H:i');
                }else {
                    return '';
                }
               })
           ->addColumn('action', function (Category $category ) {
            return '<a class="btn btn-primary" href="' . route('diamond-clarity-grade.edit', $category) . '">'.trans('misc.edit').'</a>
            <a class="btn btn-primary" href="' . route('diamond-clarity-grade.destroy', $category ) . '">'.trans('misc.delete').'</a>';
             });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery()->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('category-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0,'asc')
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('#'),
            Column::make('name'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
        ];
    }
}
