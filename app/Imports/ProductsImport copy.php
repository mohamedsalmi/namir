<?php

namespace App\Imports;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductsImport implements ToCollection, WithHeadingRow, SkipsEmptyRows
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     *
     */
    // use Importable, SkipsErrors;

    public function collection(Collection $rows)
    {
        //    dd($rows);
        $v =  Validator::make(
            $rows->toArray(),
            [
                // '*.nom' => 'distinct|required|unique:products,name',
                // '*.quantite' => 'required',
                // '*.quantite_minimal' => 'required',
                // '*.quantite_reserve' => 'required',
                // '*.prix_achat' => 'required',
                // '*.prix_de_vente_1' => 'required',
                // '*.prix_de_vente_2' => 'required',
                // '*.prix_de_vente_3' => 'required',
                // '*.non_dauteur' => 'required',
                // '*.non_denqueteur' => 'required',
                // '*.maison' => 'required',
                // '*.edition' => 'required',
                // '*.nombre_de_pages' => 'required',
                // '*.nombre_de_tomes' => 'required',
                // '*.type_de_papiers' => 'required',
                // 'sku' => 'required',
            ],
            [
                '*.nom.required' => 'Nom vide dans la ligne :index',
                '*.nom.distinct' => 'Nom en double dans la ligne :index',
                '*.nom.unique' => 'Nom existe déja dans la ligne :index',
                '*.quantite.required' => 'quantite vide dans la ligne :index',
                '*.quantite_minimal.required' => 'quantite_minimal vide dans la ligne :index',
                '*.quantite_reserve.required' => 'quantite_reserve vide dans la ligne :index',
                '*.prix_achat.required' => 'prix_achat vide dans la ligne :index',
                '*.prix_de_vente_1.required' => 'prix_de_vente_1 vide dans la ligne :index',
                '*.prix_de_vente_2.required' => 'prix_de_vente_2 vide dans la ligne :index',
                '*.prix_de_vente_3.required' => 'prix_de_vente_3 vide dans la ligne :index',
            ]
        )->validate();

        foreach ($rows as $row) {
            $edition = Attribute::where('name', 'الطبعة')->first();
            $name = AttributeValue::create(['value' => '1', 'attribute_id' => $edition->id]);      
            $product = Product::create([
                'name'     => $row['nom'],
                'reference'     => $row['reference'],
                // 'minimum_quantity'    => $row['quantite_minimal'],
                // 'reserve_quantity'    => $row['quantite_reserve'],
                'buying'    => $row['prix_achat'],
                'selling1'    => $row['prix_de_vente_1'],
                // 'selling2'    => $row['prix_de_vente_2'],
                // 'selling3'    => $row['prix_de_vente_3'],
                'currency_id'    => '1',
                'quantity_type'    => ["type" => 'Dépend d\'un attribut', "attributes" => [$edition->id]],
                'price_type'  => ["type" => "Simple"]
            ]);

            $product->productAttributes()->create(['attribute_id' => $edition->id]);
            $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            if(!isset($row['quantite'])){
                $row['quantite']=0;
            }
            $quantity = [
                // 'product_attribute1' => '1',
                // 'product_value1' => '1',
                'store_id' => auth()->user()->store->id,
                'quantity' => $row['quantite'],
            ];
            $product->quantities()->create($quantity);
            //*************************************************** */
            // $auteur = Attribute::where('name', 'اسم المؤلف')->first();
            // $name = AttributeValue::create(['value' => $row['non_dauteur'], 'attribute_id' => $auteur->id]);
            // $product->productAttributes()->create(['attribute_id' => $auteur->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            // //************************************ */
            // $maison = Attribute::where('name', 'دار النشر')->first();
            // $name = AttributeValue::create(['value' => $row['maison'], 'attribute_id' => $maison->id]);
            // $product->productAttributes()->create(['attribute_id' => $maison->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            // //************************************ */
            // $pages = Attribute::where('name', 'عدد الصفحات')->first();
            // $name = AttributeValue::create(['value' => $row['nombre_de_pages'], 'attribute_id' => $pages->id]);
            // $product->productAttributes()->create(['attribute_id' => $pages->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            // //************************************ */
            // $tomes = Attribute::where('name', 'عدد المجلدات')->first();
            // $name = AttributeValue::create(['value' => $row['nombre_de_tomes'], 'attribute_id' => $tomes->id]);
            // $product->productAttributes()->create(['attribute_id' => $tomes->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            // //************************************ */
            // $papier = Attribute::where('name', 'نوعية الورق')->first();
            // $name = AttributeValue::create(['value' => $row['type_de_papiers'], 'attribute_id' => $papier->id]);
            // $product->productAttributes()->create(['attribute_id' => $papier->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            // //************************************ */
            // $enqueteur = Attribute::where('name', 'المحقق')->first();
            // $name = AttributeValue::create(['value' => $row['non_denqueteur'], 'attribute_id' => $enqueteur->id]);
            // $product->productAttributes()->create(['attribute_id' => $enqueteur->id]);
            // $product->productAttributes()->first()->values()->create(['attribute_value_id' => $name->id]);
            //************************************ */

        }
    }
    public function getRowCount(): int
    {
        return $this->rows;
    }
}
