<?php

namespace App\Imports;

use App\Jobs\ProductImporter;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;


class ProductsImport implements ToCollection, WithHeadingRow, SkipsEmptyRows, WithCalculatedFormulas
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     *
     */
    // use Importable, SkipsErrors;

    public function collection(Collection $rows)
    {
        ini_set('max_execution_time', '0');
        $v =  Validator::make(
            $rows->toArray(),
            [
                // '*.nom' => 'distinct|required|unique:products,name',
                // '*.quantite' => 'required',
                // '*.quantite_minimal' => 'required',
                // '*.quantite_reserve' => 'required',
                // '*.prix_achat' => 'required',
                // '*.prix_de_vente_1' => 'required',
                // '*.prix_de_vente_2' => 'required',
                // '*.prix_de_vente_3' => 'required',
                // '*.non_dauteur' => 'required',
                // '*.non_denqueteur' => 'required',
                // '*.maison' => 'required',
                // '*.edition' => 'required',
                // '*.nombre_de_pages' => 'required',
                // '*.nombre_de_tomes' => 'required',
                // '*.type_de_papiers' => 'required',
                // 'sku' => 'required',
            ],
            [
                '*.nom.required' => 'Nom vide dans la ligne :index',
                '*.nom.distinct' => 'Nom en double dans la ligne :index',
                '*.nom.unique' => 'Nom existe déja dans la ligne :index',
                '*.quantite.required' => 'quantite vide dans la ligne :index',
                '*.quantite_minimal.required' => 'quantite_minimal vide dans la ligne :index',
                '*.quantite_reserve.required' => 'quantite_reserve vide dans la ligne :index',
                '*.prix_achat.required' => 'prix_achat vide dans la ligne :index',
                '*.prix_de_vente_1.required' => 'prix_de_vente_1 vide dans la ligne :index',
                '*.prix_de_vente_2.required' => 'prix_de_vente_2 vide dans la ligne :index',
                '*.prix_de_vente_3.required' => 'prix_de_vente_3 vide dans la ligne :index',
            ]
        )->validate();
        $chunks = $rows->chunk(200);
        $chunks->toArray();
        $auth_user=auth()->user();
        foreach ($chunks as $key => $chunk) {
            ProductImporter::dispatch($chunk,$auth_user)->onQueue('importer');
    }
        
    }
    public function getRowCount(): int
    {
        return $this->rows;
    }
}
