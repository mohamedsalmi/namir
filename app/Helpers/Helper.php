<?php

namespace App\Helpers;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Barcode;
use App\Models\Category;
use App\Models\ClientPricing;
use App\Models\Currency;
use App\Models\Partner;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductLog;
use App\Models\Quantity;
use App\Models\Setting;
use App\Models\Store;
use App\Models\User;
use App\Notifications\MinQuantityNotification;
use Elibyy\TCPDF\Facades\TCPDF as PDF;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use NumberToWords\NumberToWords;
use Spatie\Permission\Models\Role;

function getSettingValue($setting)
{
  return Setting::where('name', $setting)->first()?->value;
}

function generateBarcode(Product $product, $product_attribute1, $product_value1, $product_attribute2 = null, $product_value2 = null)
{
  $barcode = '';
  switch ($product->quantity_type['type']) {
    case 'Sipmle':
      $barcode = $product->sku;
      break;
    case 'Dépend d\'un attribut':
      $productAttribute1 = $product->productAttributes()->where(['id' => $product_attribute1])->first();
      $productValue1 = $productAttribute1->values()->where(['id' => $product_value1])->first();
      $barcode = substr($product->name, 0, 2) . substr($productAttribute1->attribute->name, 0, 2) . substr($productValue1->attributeValue->value, 0, 2) . $productValue1->attributeValue->id;
      break;
    case 'Dépend de deux attributs':
      $productAttribute1 = $product->productAttributes()->where(['id' => $product_attribute1])->first();
      $productAttribute2 = $product->productAttributes()->where(['id' => $product_attribute2])->first();
      $productValue1 = $productAttribute1->values()->where(['id' => $product_value1])->first();
      $productValue2 = $productAttribute2->values()->where(['id' => $product_value2])->first();
      $barcode = substr($product->name, 0, 2) . substr($productAttribute1->attribute->name, 0, 2) . substr($productValue1->attributeValue->value, 0, 2) . $productValue1->attributeValue->id . substr($productAttribute2->attribute->name, 0, 2) . substr($productValue2->attributeValue->value, 0, 2) . $productValue2->attributeValue->id;
      break;

    default:
      $barcode = $product->sku;
      break;
  }
  return $barcode;
}

function asLetters($number)
{
  $convert = explode('.', $number);
  $num[17] = array(
    'zero', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit',
    'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize'
  );

  $num[100] = array(
    20 => 'vingt', 30 => 'trente', 40 => 'quarante', 50 => 'cinquante',
    60 => 'soixante', 70 => 'soixante-dix', 80 => 'quatre-vingt', 90 => 'quatre-vingt-dix'
  );

  if (isset($convert[1]) && $convert[1] != '') {
    return asLetters($convert[0]) . ' et ' . asLetters($convert[1]);
  }
  if ($number < 0) return 'moins ' . asLetters(-$number);
  if ($number < 17) {
    return $num[17][$number];
  } elseif ($number < 20) {
    return 'dix-' . asLetters($number - 10);
  } elseif ($number < 100) {
    if ($number % 10 == 0) {
      return $num[100][$number];
    } elseif (substr($number, -1) == 1) {
      if (((int)($number / 10) * 10) < 70) {
        return asLetters((int)($number / 10) * 10) . '-et-un';
      } elseif ($number == 71) {
        return 'soixante-et-onze';
      } elseif ($number == 81) {
        return 'quatre-vingt-un';
      } elseif ($number == 91) {
        return 'quatre-vingt-onze';
      }
    } elseif ($number < 70) {
      return asLetters($number - $number % 10) . '-' . asLetters($number % 10);
    } elseif ($number < 80) {
      return asLetters(60) . '-' . asLetters($number % 20);
    } else {
      return asLetters(80) . '-' . asLetters($number % 20);
    }
  } elseif ($number == 100) {
    return 'cent';
  } elseif ($number < 200) {
    return asLetters(100) . ' ' . asLetters($number % 100);
  } elseif ($number < 1000) {
    return asLetters((int)($number / 100)) . ' ' . asLetters(100) . ($number % 100 > 0 ? ' ' . asLetters($number % 100) : '');
  } elseif ($number == 1000) {
    return 'mille';
  } elseif ($number < 2000) {
    return asLetters(1000) . ' ' . asLetters($number % 1000) . ' ';
  } elseif ($number < 1000000) {
    return asLetters((int)($number / 1000)) . ' ' . asLetters(1000) . ($number % 1000 > 0 ? ' ' . asLetters($number % 1000) : '');
  } elseif ($number == 1000000) {
    return 'millions';
  } elseif ($number < 2000000) {
    return asLetters(1000000) . ' ' . asLetters($number % 1000000);
  } elseif ($number < 1000000000) {
    return asLetters((int)($number / 1000000)) . ' ' . asLetters(1000000) . ($number % 1000000 > 0 ? ' ' . asLetters($number % 1000000) : '');
  }
}

class Helper
{

  public static function get_currency_value()
  {
    $value = Currency::where('id', session('currency')->id)->select('value')->first();
    return $value->value;
  }
  public static function getSettingValue($name)
  {
    $value = Setting::where('name', $name)->value('value');
    return $value ?? '';
  }
  public static function get_offers()
  {
    $offers = Attribute::where('name', 'العروض')->get()->pluck('values')->collapse('values');
    return $offers;
  }
  public static function get_product_offer($product)
  {
    $offer = ProductAttribute::where('product_id', $product)->whereHas('attribute', function ($q) {
      $q->where('name', 'العروض');
    })->first()?->values?->first()?->attributeValue?->value;

    return $offer;
  }
  public static function get_stores()
  {
    $stores = Store::all();
    return $stores;
  }
  public static function get_partners()
  {
    $partners = Partner::all();
    return $partners;
  }
  public static function get_product($id)
  {
    $product = Product::where('id', $id)->get();
    return $product;
  }
  public static function categories_parent()
  {
    $cat = Category::where('parent_id', null)->get();
    return $cat;
  }
  public static function categorie_children($categorie)
  {
    $cat = Category::where('parent_id', $categorie)->orderBy('id', 'ASC')->get();
    return $cat;
  }
  public static function get_hot_deals()
  {
    $products = Product::orderBy('discount')->limit(12)->select('id', 'name', 'selling1', 'discount', 'selling2')->get();
    return $products;
  }
  public static function makeDropDownListFromModel(Model $model, $field, $default = false)
  {
    $allItems = $model::orderBy($field)->get();
    $list = array();
    if ($default) {
      $list[''] = "Choisir une option";
    }
    if ($allItems->count()) {
      foreach ($allItems as $item) {
        $list[$item->id] = $item->$field;
      }
    }
    return $list;
  }

  public static function makeDropDownListFromModel2(Model $model, $field, $useAsValue = false)
  {
    $allItems = $model::orderBy($field)->get();

    // $list[''] = "Choisir une option";
    if ($allItems->count()) {
      foreach ($allItems as $item) {
        $list[$useAsValue ? $item->$field : $item->id] = $item->$field;
      }
    }
    return $list;
  }

  public static function makeDropDownListFromModelExcept(Model $model, $field, $except)
  {

    if (is_array($except)) {

      $allItems = $model::orderBy($field)->whereNotIn($field, $except)->get();
    } else {

      $allItems = $model::orderBy($field)->whereNot($field, $except)->get();
    }


    $list[''] = "Choisir une option";
    if ($allItems->count()) {
      foreach ($allItems as $item) {
        $list[$item->id] = $item->$field;
      }
    }
    return $list;
  }

  public static function makeDropDownListFromCollection($collection, $field, $default = false)
  {
    $list = array();
    if (!$default) {
      $list[''] = "Choisir une option";
    }
    if ($collection->count()) {
      foreach ($collection as $item) {
        $list[$item->id] = $item->$field;
      }
    }
    return $list;
  }

  public static function makeDropDownListFromModelRelation(Model $model, $id, $field, $relation)
  {
    $item = $model::find($id);
    $allItems = $item->$relation()->orderBy($field)->get();

    $list[''] = "Choisir une option";
    if ($allItems->count()) {
      foreach ($allItems as $item) {
        $list[$item->id] = $item->$field;
      }
    }
    return $list;
  }

  public static function makeDropDownListFromConfig($config)
  {
    $allItems = config($config);

    foreach ($allItems as $item) {
      $list[$item] = $item;
    }
    return $list;
  }

  public static function makeAttributeTypesDropDownList()
  {
    $types = [
      'text' => 'Texte',
      'decimal' => 'Numérique',
      // 'color' => 'Couleur',
      // 'batch' => 'الطبعة',
    ];
    return $types;
  }

  public static function getAttributeType($type)
  {
    $types = [
      'string' => 'Champ de texte',
      'text' => 'Paragraphe',
      'int' => 'Numéro entier',
      'decimal' => 'Numérique',
      'color' => 'Couleur',
      'batch' => 'الطبعة',
    ];
    return $types[$type];
  }

  public static function getProductQuantityValue($product_id, $field, $product_value1, $product_value2 = null)
  {
    $store_id =  auth()->user()->store_id;
    if ($product_value2) {
      $quantity = Quantity::where(['store_id' => $store_id, 'product_id' => $product_id, 'product_value1' => $product_value1, 'product_value2' => $product_value2])->first();
      if (!$quantity) {
        $quantity = Quantity::where(['product_id' => $product_id, 'product_value1' => $product_value2, 'product_value2' => $product_value1])->first();
      }
    } else {
      $quantity = Quantity::where(['store_id' => $store_id, 'product_id' => $product_id, 'product_value1' => $product_value1])->first();
    }
    return $quantity ? $quantity?->$field : 0;
  }

  public static function getQuantityValue($quantity_id, $field)
  {
    // $store_id =  auth()->user()->store_id;
    $quantity = Quantity::find($quantity_id);
    return $quantity?->productValue1?->attributeValue?->$field;
    // return $quantity ? $quantity?->$field : 0;
  }

  public static function getProductPricingValue($product, $client_id, $field)
  {
    $pricing = $product->pricings->where('client_id', $client_id)->first();
    // dd($product->pricings);
    return $pricing ? $pricing?->$field : '';
  }

  public static function getProductBarcodeValue($product, $field, $product_attribute1, $product_value1, $product_attribute2 = null, $product_value2 = null)
  {
    $product_id = $product->id;
    if ($product_value2) {
      $barcode = Barcode::where(['product_id' => $product_id, 'product_value1' => $product_value1, 'product_value2' => $product_value2])->first();
      if (!$barcode) {
        $barcode = Barcode::where(['product_id' => $product_id, 'product_value1' => $product_value2, 'product_value2' => $product_value1])->first();
      }
    } else {
      $barcode = Barcode::where(['product_id' => $product_id, 'product_value1' => $product_value1])->first();
    }
    return $barcode ? $barcode?->$field : generateBarcode($product, $product_attribute1, $product_value1, $product_attribute2, $product_value2);
  }

  public static function getProductPriceValue($product_id, $product_value1, $field, $product_value2 = null)
  {
    if ($product_value2) {
      $price = Price::where(['product_id' => $product_id, 'product_value1' => $product_value1, 'product_value2' => $product_value2])->first();
      if (!$price) {
        $price = Price::where(['product_id' => $product_id, 'product_value1' => $product_value2, 'product_value2' => $product_value1])->first();
      }
    } else {
      $price = Price::where(['product_id' => $product_id, 'product_value1' => $product_value1])->first();
    }
    return $price?->$field;
  }

  public static function formatNotificationType($type)
  {
    $types = [
      'App\Notifications\MinQuantityNotification' => 'Quantité minimale atteinte',
      'App\Notifications\NewWebsiteOrderNotification' => 'Nouvelle commande'
    ];
    return isset($types[$type]) ? $types[$type] : 'Notification';
  }

  public static function getNotificationMessage($notification)
  {
    $message = '';
    switch ($notification->type) {
      case 'App\Notifications\MinQuantityNotification':
        $message = $notification->data['product'] . '(' . $notification->data['attribute1_name'] . ':' . $notification->data['attribute1_value'] . '/' . $notification->data['attribute2_name'] . ':' . $notification->data['attribute2_value'] . ')';
        break;
      case 'App\Notifications\NewWebsiteOrderNotification':
        $message = 'La commande n° ' . $notification->data['codification'] . ' a été passé à partir de site web.';
        break;

      default:
        $message = '';
        break;
    }
    return $message;
  }

  public static function format($number)
  {
    $numberToWords = new NumberToWords();
    $numberTransformer = $numberToWords->getNumberTransformer('fr');
    $convert = explode('.', $number);
    if (isset($convert[1])) {
      return $numberTransformer->toWords($convert[0]) . ' dirhams et ' . $numberTransformer->toWords(sprintf("%-03s", $convert[1])) . ' centimes';
    }
    return $numberTransformer->toWords($number) . ' dirhams et ' . $numberTransformer->toWords(0) . ' centimes';
  }

  public static function format_product_name($name, ?Product $product){
    $formated_name = '';
    if($product){
      $formated_name = $product->name;
    }else{
      $houses = AttributeValue::select('value')->whereHas('attribute', function ($q) {
          $q->where('name',  'دار النشر');
      })->get()->pluck('value')->toArray();
      foreach ($houses as $key => $house) {
        $formated_name = str_replace("/ " . $house, "", $name);
        $formated_name = str_replace("-" . $house, "", $formated_name);
        $formated_name = str_replace($house, "", $formated_name);
      }
    }
    return $formated_name;
  }

  public static function print($invoice, $type)
  {
    $title = '';
    switch ($type) {
      case 'invoice':
        $title = 'Facture N: ' . $invoice->codification;
        break;
      case 'cart':
        $title = 'BL N: ' . $invoice->codification;
        break;
      case 'order':
        $title = 'Commande N: ' . $invoice->codification;
        break;
      case 'salequote':
        $title = 'Devis N: ' . $invoice->codification;
        break;
      case 'creditnote':
        $title = 'Avoire N: ' . $invoice->codification;
        break;
      case 'returnnote':
        $title = 'Bon de retour BL N: ' . $invoice->codification;
        break;
      default:
        $title = 'Document';
        break;
    }

    PDF::SetFont('aefurat', '', 10);
    // Custom Header
    PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {

      // Position at 15 mm from bottom
      $pdf->SetY(10);
      // Set font
      $pdf->setCellPadding(2);
      PDF::SetFont('aealarabiya', '', 10);
      $pdf->Cell(95, 5, config('stock.info.ar_name'),  'LT', 0, 'L');


      $pdf->Cell(100, 5, (getSettingValue('Domaine') ?? config('stock.info.domaine')),  'RT', 0, 'L');
      $pdf->Ln();

      $pdf->Cell(95, 5, (getSettingValue('Adresse') ?? config('stock.info.address')),  'L', 0, 'L');


      $pdf->Cell(100, 5, 'TEL : ' . (getSettingValue('Téléphone') ?? config('stock.info.phone')),  'R', 0, 'L');
      // $pdf->Image('assets/images/logoo.jpg', 160, 15, 30, 0, 'JPG', false, '', true, 100, '', false, false, 1, false, false, false);
      $pdf->Ln();

      $pdf->Cell(95, 5, 'MF : ' . (getSettingValue('MF') ?? config('stock.info.mf')),  'LB', 0, 'L');
      $pdf->Cell(100, 5, '',  'RB', 0, 'L');
      PDF::writeHTMLCell($w = 25, $h = 0, $x = '170', $y = '10', '<img src=' . config('stock.info.image') . '/>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
      // $pdf->Cell(100, 5, 'RIB : 0702 4005 9101 1011 7053',  'RB', 0, 'L');
      $pdf->Ln(30);
      $pdf->Cell(85, 5, '', 0, 0, 'C');
      $pdf->Cell(15, 5, 'Client : ',  'LT', 0, 'L');
      $pdf->Cell(95, 5,  $invoice->client_details['name'],  'RT', 0, 'L');
      $pdf->Ln();
      $pdf->Cell(75, 5, $title,  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'Adresse : ' . $invoice->client_details['adresse'],  'LRT', 0, 'L');
      $pdf->Ln();
      $pdf->Cell(25, 5, 'Page',  1, 0, 'C');
      $pdf->Cell(25, 5, 'Client ',  1, 0, 'C');
      $pdf->Cell(25, 5, 'Date  ',  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'Numéro : ' . $invoice->client_details['phone'],  'LR', 0, 'L');
      $pdf->Ln();
      $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
      $pdf->Cell(25, 5, '                    ' . $page, 1, 0, 'C');
      $pdf->Cell(25, 5, $invoice->client()->exists() ? str_pad($invoice->client->id, 4, 0, STR_PAD_LEFT) : '0000',  1, 0, 'C');
      // $pdf->Cell(25, 5, str_pad($invoice->client_details['name'], 5, '0', STR_PAD_LEFT), 1, 0, 'C');
      $pdf->Cell(25, 5, date_format($invoice->created_at, 'd/m/Y'),  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'MF : ' . $invoice->client_details['mf'],  'LRB', 0, 'L');
      $pdf->Ln();
      $pdf->Cell(85, 5, '', 0, 0, 'C');
      // $pdf->Cell(105, 5, 'Vehicule : ' . $invoice->CustomerCar, 'LRB', 0, 'L');
      $pdf->Ln(15);
      // $pdf->Cell(20, 10, 'Réf',  1, 0, 'L');
      $pdf->Cell(90, 10, 'Désignation',  1, 0, 'L');
      $pdf->Cell(15, 10, 'Qté',  1, 0, 'C');
      $pdf->Cell(20, 10, 'P.U.',  1, 0, 'C');
      $pdf->Cell(15, 10, 'REMISE', 1, 0, 'C');
      $pdf->Cell(15, 10, 'TVA', 1, 0, 'C');
      $pdf->Cell(20, 10, 'PUTTC', 1, 0, 'C');
      $pdf->Cell(20, 10, 'Total',  1, 0, 'C');
    });
    // Custom Footer
    PDF::setFooterCallback(function ($pdf) {

      // Position at 15 mm from bottom
      $pdf->SetY(-15);
      // Set font
      $pdf->SetFont('helvetica', 'I', 8);
      // Page number
      $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    });
    PDF::SetAuthor(config('stock.info.name'));
    PDF::SetTitle($title);
    PDF::SetSubject($title);
    PDF::SetMargins(7, 99, 7);
    PDF::SetFontSubsetting(false);
    // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    PDF::AddPage('P', 'A4');
    PDF::SetFont('aealarabiya', '', 8);
    $total = 0;
    $total_HT = 0;
    foreach ($invoice->items as $item) {
      // PDF::Cell(20, 5, $item->product->reference,  'RL', 0, 'L');
      PDF::SetFont('aealarabiya', '', 11);
      
      PDF::Cell(90, 5, self::format_product_name($item->product_name, $item->product),  'RLB', 0, 'L');
      PDF::SetFont('aealarabiya', '', 8);
      PDF::Cell(15, 5, $item->product_quantity,  'RLB', 0, 'C');
      // PDF::Cell(15, 5, number_format($row["product_price"] / (1 + ($row["product_tva"] / 100)), 3), 'RL', 0, 'R');
      PDF::Cell(20, 5, number_format($item->product_price_selling * (1 - ($item->product_tva / 100)), 3),  'RLB', 0, 'R');
      PDF::Cell(15, 5, $item->product_remise, 'RLB', 0, 'R');
      PDF::Cell(15, 5, $item->product_tva, 'RLB', 0, 'R');
      PDF::Cell(20, 5, number_format(($item->product_price_selling  * (1 - ($item->product_remise / 100))), 3), 'RLB', 0, 'R');
      PDF::Cell(20, 5, number_format($item->product_price_selling * (1 - ($item->product_remise / 100)) * $item->product_quantity, 3),  'RLB', 0, 'R');
      PDF::Ln();
      // $total += $item->product_price_selling * (1 - ($item->product_remise / 100)) * $item->product_quantity;
      // $total = $total + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * (1 + ($row["product_tva"] / 100)) * $row["product_quantity"]);
      // $total_HT = $total_HT + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * $row["product_quantity"]);
    }
    PDF::Cell(195, 30, '',  'T', 0, 'C');
    PDF::Ln();
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, 'Total Brut TTC', 0, 0, 'L');
    PDF::Cell(39, 5, number_format($invoice->total, 3), 0, 0, 'L');
    PDF::Cell(39, 5, 'Net A Payer',  1, 0, 'C');
    PDF::Ln();
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, 'Timbre fiscal', 0, 0, 'L');
    if ($invoice->timbre == '1') {
      $timbre = Setting::where('name', 'Timbre fiscal')->first()->value;
      $topay = $invoice->total + $timbre;
    } else {
      $timbre = 0;
      $topay = $invoice->total;
    }
    PDF::Cell(39, 5, number_format($timbre, 3), 0, 0, 'L');
    PDF::Cell(39, 5, number_format(($topay), 3) . ' DM',  1, 0, 'C');
    PDF::Ln(10);
    PDF::Cell(195, 0, 'Arrêtée la présente facture à la somme de : ', 0, 0, 'L');
    PDF::Ln(5);
    PDF::SetFont('helvetica', 'B',);
    PDF::Cell(0, 0, ucfirst(\App\Helpers\Helper::format($topay)), 0, 0, 'L');
    PDF::SetFont('helvetica', 'I', 8);
    PDF::lastPage();
    return PDF::Output($title . '.pdf', 'I');
    // exit;
  }

  public static function printPurchase($invoice, $type)
  {
    $title = '';
    switch ($type) {
      case 'voucher':
        $title = 'Bon d\'achat N: ' . $invoice->codification;
        break;
      case 'returncoupon':
        $title = 'Bon de retour N: ' . $invoice->codification;
        break;
      default:
        $title = 'Document';
        break;
    }

    PDF::SetFont('aefurat', '', 10);
    // Custom Header
    PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {

      // Position at 15 mm from bottom
      $pdf->SetY(10);
      // Set font
      $pdf->setCellPadding(2);
      PDF::SetFont('aealarabiya', '', 10);
      $pdf->Cell(95, 5, config('stock.info.ar_name'),  'LT', 0, 'L');


      $pdf->Cell(100, 5, config('stock.info.domaine'),  'RT', 0, 'L');
      $pdf->Ln();

      $pdf->Cell(95, 5, config('stock.info.address'),  'L', 0, 'L');


      $pdf->Cell(100, 5, 'TEL : ' . config('stock.info.phone'),  'R', 0, 'L');
      // $pdf->Image('assets/images/logoo.jpg', 160, 15, 30, 0, 'JPG', false, '', true, 100, '', false, false, 1, false, false, false);
      $pdf->Ln();

      $pdf->Cell(95, 5, 'MF : 119 66 67 E M N 000',  'LB', 0, 'L');
      $pdf->Cell(100, 5, '',  'RB', 0, 'L');
      PDF::writeHTMLCell($w = 25, $h = 0, $x = '170', $y = '10', '<img src=' . config('stock.info.image') . '/>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
      // $pdf->Cell(100, 5, 'RIB : 0702 4005 9101 1011 7053',  'RB', 0, 'L');
      $pdf->Ln(30);
      $pdf->Cell(85, 5, '', 0, 0, 'C');
      $pdf->Cell(25, 5, 'Fournisseur : ',  'LT', 0, 'L');
      $pdf->Cell(85, 5, $invoice->provider_details['name'],  'RT', 0, 'L');
      $pdf->Ln();
      $pdf->Cell(75, 5, $title,  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'Adresse : ' . $invoice->provider_details['adresse'],  'LRT', 0, 'L');
      $pdf->Ln();
      $pdf->Cell(25, 5, 'Page',  1, 0, 'C');
      $pdf->Cell(25, 5, 'Fournisseur ',  1, 0, 'C');
      $pdf->Cell(25, 5, 'Date  ',  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'Numéro : ' . $invoice->provider_details['phone'],  'LR', 0, 'L');
      $pdf->Ln();
      $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
      $pdf->Cell(25, 5, '                    ' . $page, 1, 0, 'C');

      $pdf->Cell(25, 5, $invoice->provider()->exists() ? str_pad($invoice->provider->id, 4, 0, STR_PAD_LEFT) : '0000',  1, 0, 'C');
      // $pdf->Cell(25, 5, str_pad($invoice->provider_details['name'], 5, '0', STR_PAD_LEFT), 1, 0, 'C');
      $pdf->Cell(25, 5, date_format($invoice->created_at, 'd/m/Y'),  1, 0, 'C');
      $pdf->Cell(10, 5, '', 0, 0, 'C');
      $pdf->Cell(110, 5, 'MF : ' . $invoice->provider_details['mf'],  'LRB', 0, 'L');
      $pdf->Ln(15);
      $pdf->Cell(130, 10, 'Désignation', $type == 'cart' ? '' : 1, 0, 'L');
      $pdf->Cell(20, 10, 'Qté', $type == 'cart' ? '' : 1, 0, 'C');
      $pdf->Cell(25, 10, 'P.U.', $type == 'cart' ? '' : 1, 0, 'C');
      // $pdf->Cell(15, 10, 'REMISE', 1, 0, 'C');
      // $pdf->Cell(10, 10, 'TVA', 1, 0, 'C');
      // $pdf->Cell(20, 10, 'PUTTC', 1, 0, 'C');
      $pdf->Cell(20, 10, 'Total', $type == 'cart' ? '' : 1, 0, 'C');
      $pdf->Ln();
    });

    // Custom Footer
    PDF::setFooterCallback(function ($pdf) {

      // Position at 15 mm from bottom
      $pdf->SetY(-15);
      // Set font
      $pdf->SetFont('helvetica', 'I', 8);
      // Page number
      $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    });

    PDF::SetAuthor(config('stock.info.name'));
    PDF::SetTitle($title);
    PDF::SetSubject($title);
    PDF::SetMargins(7, 90, 7);
    PDF::SetFontSubsetting(false);

    // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    PDF::AddPage('P', 'A4');
    PDF::SetFont('aealarabiya', '', 8);
    $total = 0;
    $total_HT = 0;
    foreach ($invoice->items as $item) {

      PDF::SetFont('aealarabiya', '', 11);
      PDF::Cell(130, 5, self::format_product_name($item->product_name, $item->product), $type == 'cart' ? '' : 'RLB', 0, 'L');
      PDF::SetFont('aealarabiya', '', 8);
      PDF::Cell(20, 5, $item->product_quantity, $type == 'cart' ? '' : 'RLB', 0, 'C');
      // PDF::Cell(15, 5, number_format($row["product_price"] / (1 + ($row["product_tva"] / 100)), 3), 'RLB', 0, 'C');
      PDF::Cell(25, 5, $item->product_price_buying, $type == 'cart' ? '' : 'RLB', 0, 'C');
      // PDF::Cell(15, 5, $row["product_remise"], 'RLB', 0, 'C');
      // PDF::Cell(10, 5, $row["product_tva"], 'RLB', 0, 'C');
      // PDF::Cell(20, 5, number_format((($row["product_price"] / (1 + ($row["product_tva"] / 100))) * (1 - ($row["product_remise"] / 100))) * (1 + ($row["product_tva"] / 100)), 3), 'RLB', 0, 'C');
      PDF::Cell(20, 5, number_format($item->product_price_buying * (1 - ($item->product_remise / 100)) * $item->product_quantity, 3), $type == 'cart' ? '' : 'RLB', 0, 'C');

      PDF::Ln();
      $total += (($item->product_price_buying * $item->product_tva) / 100) * $item->product_quantity;

      // $total = $total + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * (1 + ($row["product_tva"] / 100)) * $row["product_quantity"]);
      // $total_HT = $total_HT + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * $row["product_quantity"]);
    }


    PDF::Cell(195, 30, '', $type == 'cart' ? '' : 'T', 0, 'C');
    PDF::Ln();
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, 'Total Brut TTC', 0, 0, 'L');
    PDF::Cell(39, 5, number_format($invoice->total, 3), 0, 0, 'L');
    PDF::Cell(39, 5, 'Net A Payer', $type == 'cart' ? '' : 1, 0, 'C');
    PDF::Ln();

    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, '', 0, 0, 'C');
    PDF::Cell(39, 5, 'Timbre fiscal', 0, 0, 'L');
    if ($invoice->timbre == '1') {
      $timbre = Setting::where('name', 'Timbre fiscal')->first()->value;
      $topay = $invoice->total + $timbre;
    } else {
      $timbre = 0;
      $topay = $invoice->total;
    }
    PDF::Cell(39, 5, number_format($timbre, 3), 0, 0, 'L');
    PDF::Cell(39, 5, number_format(($topay), 3) . ' DM', $type == 'cart' ? '' : 1, 0, 'C');
    PDF::Ln(10);

    PDF::Cell(195, 0, 'Arrêtée la présente facture à la somme de : ', 0, 0, 'L');
    PDF::Ln(5);
    PDF::SetFont('helvetica', 'B', 10);
    PDF::Cell(0, 0, ucfirst(\App\Helpers\Helper::format($topay)), 0, 0, 'L');
    PDF::SetFont('helvetica', 'I', 8);
    PDF::lastPage();
    return PDF::Output($title . '.pdf', 'I');
    // exit;
  }

  public static function print2($invoice, $type)
  {
    $title = 'BS';
    $title = '';
    switch ($type) {
      case 'mu':
        $title = 'BS N:' . $invoice->codification;
        break;
      case 'inventory':
        $title = 'Inventaire N: ' . $invoice->codification;
        break;
      default:
        $title = 'Document';
        break;
    }
    PDF::SetFont('aefurat', '', 10);
    // Custom Header
    PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {

      // Position at 15 mm from bottom
      $pdf->SetY(10);
      // Set font
      $pdf->setCellPadding(2);
      PDF::SetFont('aealarabiya', '', 10);
      $pdf->Cell(95, 5, config('stock.info.ar_name'), 'LT', 0, 'L');


      $pdf->Cell(100, 5, config('stock.info.domaine'), 'RT', 0, 'L');
      $pdf->Ln();

      $pdf->Cell(95, 5, config('stock.info.address'), 'L', 0, 'L');
      $pdf->Cell(100, 5, 'TEL : ' . config('stock.info.phone'), 'R', 0, 'L');
      $pdf->Ln();

      $pdf->Cell(95, 5, 'MF : 119 66 67 E M N 000', 'LB', 0, 'L');
      // $pdf->Cell(100, 5, 'RIB : 0702 4005 9101 1011 7053', 'RB', 0, 'L');
      $pdf->Cell(100, 5, '', 'RB', 0, 'L');
      PDF::writeHTMLCell($w = 25, $h = 0, $x = '170', $y = '10', '<img src=' . config('stock.info.image') . '/>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
      $pdf->Ln(30);
      $pdf->Cell(70, 5, $title, 1, 0, 'C');
      $pdf->Cell(55, 5, '', 0, 0, 'C');
      $pdf->Cell(70, 5, 'Points de vente', 1, 0, 'C');
      $pdf->Ln();
      $pdf->Cell(35, 5, 'Page', 1, 0, 'C');
      $pdf->Cell(35, 5, 'Date  ', 1, 0, 'C');
      $pdf->Cell(55, 5, '', 0, 0, 'C');
      $pdf->Cell(35, 5, 'Origine', 1, 0, 'C');
      $pdf->Cell(35, 5, 'Destination  ', 1, 0, 'C');
      $pdf->Ln();
      $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
      $pdf->Cell(35, 5, '                    ' . $page, 1, 0, 'C');

      $pdf->Cell(35, 5, date_format($invoice->created_at, 'd/m/Y'), 1, 0, 'C');
      $pdf->Cell(55, 5, '', 0, 0, 'C');
      $pdf->Cell(35, 5, $type == 'mu' ? $invoice->storeOrigin->name : '', 1, 0, 'C');
      $pdf->Cell(35, 5, $type == 'mu' ? $invoice->storeDestination->name : '', 1, 0, 'C');
      $pdf->Ln(5);
      $pdf->Cell(195, 5, '', 0, 0, 'C');
      $pdf->Ln(10);
      $pdf->Cell(195, 2, 'Note: ' . $invoice->note, '', 0, 'L');
      $pdf->Ln(10);
      $pdf->Cell(120, 10, 'Désignation', 1, 0, 'L');
      $pdf->Cell($type == 'inventory' ? 40 : 75, 10, $type == 'inventory' ? 'Qté ancienne' : 'Qté', 1, 0, 'C');
      $type == 'inventory' ?  $pdf->Cell(35, 10, 'Qté Enrigitrée', 1, 0, 'C') : '';
      $pdf->Ln();
    });

    // Custom Footer
    PDF::setFooterCallback(function ($pdf) {

      // Position at 15 mm from bottom
      $pdf->SetY(-15);
      // Set font
      $pdf->SetFont('helvetica', 'I', 8);
      // Page number
      $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    });

    PDF::SetAuthor(config('stock.info.name'));
    PDF::SetTitle($title);
    PDF::SetSubject($title);
    PDF::SetMargins(7, 80, 7);
    PDF::SetFontSubsetting(false);

    PDF::AddPage('P', 'A4');
    PDF::SetFont('aealarabiya', '', 8);

    foreach ($invoice->items as $item) {
      PDF::SetFont('aealarabiya', '', 11);
      PDF::Cell(120, 5, self::format_product_name($item->product_name, $item->product),  'RLB', 0, 'L');
      PDF::SetFont('aealarabiya', '', 8);
      PDF::Cell($type == 'inventory' ? 40 : 75, 5,  $type == 'inventory' ? $item->product_old_quantity : $item->product_quantity,  'RLB', 0, 'C');
      $type == 'inventory' ? PDF::Cell(35, 5,  $type == 'inventory' ? $item->product_new_quantity : $item->product_quantity,  'RLB', 0, 'C') : '';
      PDF::Ln();
    }


    PDF::Cell(195, 30, '', 'T', 0, 'C');
    PDF::Ln();

    PDF::lastPage();
    return PDF::Output($title . '.pdf', 'I');
    // exit;
  }

  public static function getWords($sentence, $start, $count = null)
  {
    return implode(' ', array_slice(explode(' ', $sentence), $start, $count));
  }

  public static function printTicket($cart, $type = null)
  {
    $height = 100 + (($cart->items->count() ?? 1) * 15);
    PDF::SetFont('aefurat', '', 10);

    PDF::SetAuthor(config('stock.info.name'));
    PDF::SetTitle('Ticket n° ' . $cart->codification);
    PDF::SetSubject('Ticket');
    PDF::SetMargins(1, 10, 1);
    PDF::SetFontSubsetting(false);

    $page_format = array(
      'MediaBox' => array('llx' => 0, 'lly' => 0, 'urx' => 80, 'ury' => $height),
      // 'MediaBox' => array('llx' => 0, 'lly' => 0, 'urx' => 80, 'ury' => PDF::GetY()),
      // 'CropBox' => array('llx' => 0, 'lly' => 0, 'urx' => 80, 'ury' => 297),
      // 'BleedBox' => array('llx' => 5, 'lly' => 5, 'urx' => 80, 'ury' => 292),
      // 'TrimBox' => array('llx' => 10, 'lly' => 10, 'urx' => 80, 'ury' => 287),
      // 'ArtBox' => array('llx' => 15, 'lly' => 15, 'urx' => 80, 'ury' => 282),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      // 'Rotate' => 90,
      'PZ' => 1,
    );

    // add first page ---

    PDF::AddPage('P', $page_format, false, false);

    // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // PDF::AddPage('P', 'A7');
    PDF::SetFont('aealarabiya', '', 8);

    PDF::SetY(1);
    // Set font
    PDF::setCellPadding(0);
    PDF::SetFont('aealarabiya', '', 12);
    PDF::Cell(80, 2, '', 0, 0, 'L');
    PDF::Ln();
    PDF::Cell(0, 2, config('stock.info.ar_name'), 0, 0, 'R');
    PDF::Ln(5);
    PDF::SetFont('aealarabiya', '', 8);
    // PDF::Cell(0, 2, config('stock.info.address'), 0, 0, 'R');
    // PDF::Ln(4);
    // PDF::Cell(0, 2, '( على الطريق X) تونس 2042', 0, 0, 'R');

    PDF::Cell(0, 2, self::getWords((getSettingValue('Adresse') ?? config('stock.info.address')), 0, 6), 0, 0, 'R');
    PDF::Ln(4);
    PDF::Cell(0, 2, self::getWords((getSettingValue('Adresse') ?? config('stock.info.address')), 6), 0, 0, 'R');

    PDF::Ln(4);
    PDF::Cell(65, 2, config('stock.info.phone'), 0, 0, 'R');
    PDF::Cell(0, 2, 'رقم الجوال :   ', 0, 0, 'R');
    PDF::Ln(4);
    PDF::Cell(65, 2, ($cart->store->email ?? config('stock.info.mail')), 0, 0, 'R');
    PDF::Cell(0, 2, ' إيميل :   ', 0, 0, 'R');
    PDF::Ln(4);
    PDF::Cell(0, 2, 'ـــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــ', 0, 0, 'L');
    PDF::Ln(4);
    PDF::Cell(10, 2, 'Date :', 0, 0, 'L');
    PDF::Cell(20, 2, date_format($cart->created_at, 'd/m/Y'), 0, 0, 'L');
    PDF::Ln(4);
    PDF::SetFont('aealarabiya', '', 12);
    $html = '<h3 style=" border: 1px solid !;"> Vente</h3>';
    PDF::writeHTMLCell($w = 20, $h = 0, $x = '65', $y = '', $html, $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    PDF::Ln(6);
    PDF::SetFont('aealarabiya', '', 8);
    PDF::Cell(0, 2, 'Ticket n° ' . str_pad($cart->id, 4, 0, STR_PAD_LEFT), 0, 0, 'L');
    PDF::Ln(6);
    PDF::SetFont('helvetica', 'B', 8);
    PDF::Cell(0, 2, 'Désiniation ', 0, 0, 'L');
    PDF::Cell(0, 2, 'Total ', 0, 0, 'R');
    PDF::Ln(4);
    PDF::SetFont('aealarabiya', 'I', 8);

    $total = 0;
    $total_HT = 0;
    $total_remise = 0;
    foreach ($cart->items as $item) {
      PDF::Cell(48, 5,  self::format_product_name($item->product_name, $item->product), 0, 0, 'L');
      PDF::SetFont('helvetica', 'B', 8);
      PDF::Cell(0, 5, number_format(($item->product_price_selling * (1 - ($item->product_remise / 100)) * $item->product_quantity), 3) . ' DM', 0, 0, 'R');

      PDF::SetFont('aealarabiya', 'I', 8);
      PDF::Ln();
      PDF::Cell(0, 5, $item->product_quantity . ' x ' . number_format(($item->product_price_selling), 3) . ' DM', 0, 0, 'C');
      PDF::Cell(0, 5, ' Rem ' . number_format(($item->product_remise), 2) . ' %', 0, 0, 'R');
      $total_remise += $item->product_remise * $item->product_price_selling * $item->product_quantity;

      PDF::Ln(8);
    }

    PDF::Cell(20, 5, 'Remise total :', 0, 0, 'L');
    PDF::Cell(58, 5, number_format(($total_remise), 3) . ' DM', 0, 0, 'R');
    PDF::Ln();
    // PDF::Cell(20, 5, 'Montant total', 0, 0, 'L');
    if ($cart->timbre == '1') {
      $timbre = Setting::where('name', 'Timbre fiscal')->first()->value;
      $topay = $cart->total + $timbre;
    } else {
      $timbre = 0;
      $topay = $cart->total;
    }
    $html = '<h3 style="border-top: 1px solid !; border-bottom: 1px solid !;"> Total TTC :</h3>';
    $html2 = '<h3 >' . $topay . ' DM</h3>';
    PDF::writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    PDF::writeHTMLCell($w = 0, $h = 0, $x = '60', $y = '', $html2, $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    PDF::Ln(10);
    // PDF::Cell(58, 5, number_format(($topay), 3) . ' DM', 0, 0, 'R');
    PDF::Cell(0, 5, 'شكرا لزيارتكم .. مع تحيات ' . config('stock.info.ar_name'), 0, 0, 'R');


    PDF::lastPage();
    // dd(PDF::GetY());
    return PDF::Output('Ticket n° ' . $cart->codification . '.pdf', 'I');
    // exit;
  }

  // public static function printTicket($cart, $type)
  // {
  //   $pdf = App::make('dompdf.wrapper');
  //   $customPaper = array(0, 0, 841.89, 226.77);
  //   $pdf->loadView('admin.pdf', compact('cart', 'type'))->setPaper($customPaper, 'landscape');
  //   return $pdf;
  // }

  public static function getProductLogMotif(?Model $document, $type, $qt, Quantity $quantity, $action)
  {
    if(!$document){
      return $type . $qt . ' p ajouté(s) au stock';
    }
    switch ($action) {
      case 'add':
        $motif = $type . Helper::getModelName(strtolower(class_basename($document))) . ' ' . $document->codification . ' : ' . $qt . ' p ajouté(s) au stock';
        break;
      case 'remove':
        $motif = $type . Helper::getModelName(strtolower(class_basename($document))) . ' ' . $document->codification . ' : ' . $qt . ' p retiré(s) du stock';
        break;
      case 'forced':
        $motif = $type . Helper::getModelName(strtolower(class_basename($document))) . ' ' . $document->codification . ' : ' . $qt . ' p en stock';
        break;
      default:
        $motif = $type . Helper::getModelName(strtolower(class_basename($document))) . ' ' . $document->codification;
        break;
    }
    return $motif;
  }

  public static function updateQuantity(?Model $document, $type, $qt, $quantity, $action, $username = null)
  {
    $log = [
      'quantity' => $qt,
      'motif' => self::getProductLogMotif($document, $type, $qt, $quantity, $action),
      'action' => $action,
      'product_value1' => $quantity->product_value1,
      'user_name' => $username ? $username : auth()->user()->name,
      'product_id' => $quantity->product_id,
      'store_id' => $quantity->store_id,
    ];

    if(!$document){
      ProductLog::create($log);
    }else{
      $document->productlog()->create($log);
    }

    if ($action == 'remove') {
      $quantity_new = (float)($quantity->quantity - $qt);
    } 
    elseif ($action == 'forced') {
      return $quantity->update(['quantity' => $qt]);
    }
    else {
      $quantity_new = (float)($quantity->quantity + $qt);
    }
    if ($quantity_new <= $quantity->minimum_quantity) {
      $admins = User::role('Admin')->get();
      foreach ($admins as $key => $admin) {
        $admin->notify(new MinQuantityNotification([
          'prd' => $quantity->product_id,
          'qt' => $quantity,
          'attr1' => $quantity->product_value1,
          'attr2' => $quantity->product_value2,
        ]));
      }
    }
    return $quantity->update(['quantity' => $quantity_new]);
  }

  public static function getTitle($type, $page)
  {
    $title = '';
    switch ($type) {
      case 'invoice':
        $title = $page == 'create' ? 'Nouvelle facture' : 'Liste des factures';
        break;
      case 'cart':
        $title = $page == 'create' ? 'Nouveau BL' : 'Liste des bons de livraison';
        break;
      case 'order':
        $title = $page == 'create' ? 'Nouvelle commande' : 'Liste des commandes';
        break;
      case 'salequote':
        $title = $page == 'create' ? 'Nouveau devis' : 'Liste des devis';
        break;
      case 'inventory':
        $title = $page == 'create' ? 'Nouveau inventaire' : 'Liste des inventaires';
        break;
      case 'onlinesale':
        $title = $page == 'create' ? 'Nouveau vente en ligne' : 'Liste des ventes en ligne';
        break;

      default:
        $title = $page == 'create' ? 'Nouveau' : 'Liste';
        break;
    }

    return $title;
  }

  public static function roleHasPermission(Role $role, $permission)
  {
    $hasPermission = $role->hasPermissionTo($permission);

    return $hasPermission ? 'checked' : '';
  }
  public static function getPaymentMethodName($type)
  {
    $name = 'N/A';
    switch ($type) {
      case 'cash':
        $name = 'Espèce';
        break;
      case 'check':
        $name = 'Chèque';
        break;
      case 'exchange':
        $name = 'Traite';
        break;
      case 'transfer':
        $name = 'Virement';
        break;
      case 'rs':
        $name = 'R à S';
        break;
      case 'ra':
        $name = 'Retour';
        break;

      default:
        break;
    }

    return $name;
  }
  public static function getModelName($model)
  {
    $name = '';
    switch ($model) {
      case 'cart':
        $name = 'BL';
        break;
      case 'returnnote':
        $name = 'Bon de retour BL';
        break;
      case 'invoice':
        $name = 'Facture';
        break;
      case 'salequote':
        $name = 'Devis';
        break;
      case 'order':
        $name = 'Commande';
        break;
      case 'voucher':
        $name = 'BA';
        break;
      case 'bill':
        $name = 'Facture d\'achat';
        break;
      case 'billcreditnote':
        $name = 'Avoir d\'achat';
        break;
      case 'returnCoupon':
        $name = 'Bon de retour';
        break;
      case 'financialcreditnote':
        $name = 'Avoir financière';
        break;
      case 'mu':
        $name = 'Mouvement de stock';
        break;
      case 'inventory':
        $name = 'Inventaire';
        break;
      case 'onlinesale':
        $name = 'Vente en ligne';
        break;

      default:
        $name = 'Document';
        break;
    }

    return $name;
  }

  public static function generateCodification($codification_setting)
  {
    try {
      $setting = Setting::where('name', $codification_setting)->first();
      $codification = $setting->value;
      /** Start check new year */
      $current_year = date("Y");
      if(explode("-", $codification)[0] != $current_year){
          $setting->update(['value' => $current_year . "-00000"]);
          $codification = $setting->value;
      }
      /** End check new year */
      $new_codification = explode("-", $codification)[0] . "-" . str_pad(((int)explode("-", $codification)[1] + 1), 5, 0, STR_PAD_LEFT);
      $setting->update(['value' => $new_codification]);
    } catch (\Throwable $th) {
      //throw $th;
    }
    return $new_codification;
  }

  public static function custom_in_array($objects, $value, $field = 'id')
  {
    return array_filter($objects, function ($toCheck) use ($value, $field) {
      return $toCheck[$field] == $value;
    });
  }

  public static function getCartItemQty($item)
  {
    $cart = session()->get('cart') ?? [];
    $filter = array_filter($cart, function ($toCheck) use ($item) {
      return $toCheck['id'] == $item['id'];
    });

    // dd(!empty(array_values($filter)) ? array_values($filter)[0]['quantity'] : 0);
    $qty = !empty(array_values($filter)) ? array_values($filter)[0]['quantity'] : 0;
    return $qty;
  }

  public static function printRas($invoice)
  {
    $title = 'Retenue à la source';

    PDF::SetFont('aefurat', '', 10);
    // Custom Header
    PDF::setHeaderCallback(function ($pdf) use ($invoice, $title) {

      // Position at 15 mm from bottom
      $pdf->SetY(10);
      // Set font
      $pdf->setCellPadding(2);
      PDF::SetFont('helvetica', '', 10);

      $pdf->Cell(100, 5, 'REPUBLIQUE TUNISIENNE',  '', 0, 'L');
      $pdf->SetFont('helvetica', 'I', 10);
      $pdf->Cell(95, 5, 'ANNEXE A LA NOTE COMMUNE N° 10/98',  '', 0, 'C');
      PDF::SetFont('helvetica', '', 10);
      $pdf->Ln(5);
      $pdf->Cell(100, 5, 'MINISTERE DES FINANCES',  '', 0, 'L');
      PDF::SetFont('helvetica', 'B', 10);
      $pdf->Cell(95, 5, 'CERTIFICAT DE RETENUE A LA SOURCE',  '', 0, 'C');
      PDF::SetFont('helvetica', '', 10);
      $pdf->Ln(10);
    });
    // Custom Footer
    PDF::setFooterCallback(function ($pdf) {
      // Position at 15 mm from bottom
      $pdf->SetY(-15);
      // Set font
      $pdf->SetFont('helvetica', 'I', 8);
      // Page number
      // $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    });
    PDF::SetAuthor(config('stock.info.name'));
    PDF::SetTitle($title);
    PDF::SetSubject($title);
    PDF::SetMargins(7, 20, 7);
    PDF::SetFontSubsetting(false);
    // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    PDF::AddPage('P', 'A4');
    PDF::SetFont('helvetica', '', 8);
    PDF::Ln(15);

    //----- A/ ORGANISME DEBITEUR -----//
    PDF::SetFont('helvetica', 'BU', 9);
    PDF::Cell(195, 7, 'A/ ORGANISME DEBITEUR',  'LTR', 0, 'L');
    PDF::SetFont('aealarabiya', '', 8);
    PDF::Ln();
    PDF::Cell(140, 5, '',  'L', 0, 'L');

    PDF::Cell(55, 5, 'IDENTIFICATION',  'R', 0, 'L');
    PDF::Ln();

    PDF::Cell(110, 5, '   RAISON SOCIALE: ' . (getSettingValue('Nom commercial') ?? config('stock.info.name')),  'L', 0, 'L');

    PDF::Cell(25, 5, 'Matricule Fiscale',  'LT', 0, 'L');
    PDF::Cell(10, 5, 'C. TVA',  'LT', 0, 'L');
    PDF::Cell(18, 5, 'C. Catégorie',  'LT', 0, 'L');
    PDF::Cell(30, 5, 'N° Etablissement',  'LTR', 0, 'L');
    PDF::Cell(2, 5, '',  'R', 0, 'L');
    PDF::Ln();

    PDF::Cell(110, 5, '   ACTIVITE: ' . (getSettingValue('Domaine') ?? config('stock.info.domaine')),  'L', 0, 'L');
    PDF::Cell(25, 5, (getSettingValue('MF') ?? config('stock.info.mf')),  'BLT', 0, 'L');
    PDF::Cell(10, 5, 'E',  'BLT', 0, 'L');
    PDF::Cell(18, 5, 'M',  'BLT', 0, 'L');
    PDF::Cell(30, 5, '000',  'BLTR', 0, 'L');
    PDF::Cell(2, 5, '',  'R', 0, 'L');

    PDF::Ln();

    PDF::Cell(195, 5, '   ADRESSE : ' . (getSettingValue('Adresse') ?? config('stock.info.address')),  'LBR', 0, 'L');
    PDF::Cell(100, 5, '',  '', 0, 'L');
    PDF::Ln(6);

    //----- B/ LE BENEFICIAIRE -----//
    PDF::SetFont('helvetica', 'BU', 9);
    PDF::Cell(195, 7, 'B/ LE BENEFICIAIRE',  'LTR', 0, 'L');
    PDF::SetFont('aealarabiya', '', 8);
    PDF::Ln();
    PDF::Cell(140, 5, '',  'L', 0, 'L');

    PDF::Cell(55, 5, 'IDENTIFICATION',  'R', 0, 'L');
    PDF::Ln();

    PDF::Cell(30, 5, '   DENOMINATION : ',  'L', 0, 'L');
    PDF::Cell(80, 5,  $invoice->client_details['name'],  '', 0, 'L');


    PDF::Cell(41, 5, 'TYPE',  'LT', 0, 'L');
    PDF::Cell(42, 5, 'N° OU MATRICULE',  'LTR', 0, 'L');
    PDF::Cell(2, 5, '',  'R', 0, 'L');
    PDF::Ln();

    PDF::Cell(110, 5, '   ACTIVITE: ',  'L', 0, 'L');
    PDF::Cell(41, 5, 'Matricule Fiscale',  'BLT', 0, 'L');
    PDF::Cell(42, 5, $invoice->client_details['mf'],  'BLTR', 0, 'L');
    PDF::Cell(2, 5, '',  'R', 0, 'L');

    PDF::Ln();

    PDF::Cell(195, 5, '   ADRESSE : ' . $invoice->client_details['adresse'],  'LBR', 0, 'L');
    PDF::Cell(100, 5, '',  '', 0, 'L');
    PDF::Ln(6);

    //----- C/ INFORMATION RELATIVE AU MARCHE -----//
    PDF::SetFont('helvetica', 'BU', 9);
    PDF::Cell(195, 7, 'C/ INFORMATION RELATIVE AU MARCHE',  'LTR', 0, 'L');
    PDF::SetFont('aealarabiya', '', 8);
    PDF::Ln();

    PDF::Cell(195, 5, '   Objet du marché: ',  'LR', 0, 'L');

    PDF::Ln();

    PDF::Cell(195, 5, '   Montant du Marché ou Facture(s): ',  'LR', 0, 'L');


    PDF::Ln();

    PDF::Cell(195, 5, '   Date de la conclusion du Marché ou Facture(s) : ',  'LR', 0, 'L');
    PDF::Ln();

    //----- D/ RETENUE EFFECTUE -----//
    PDF::SetFont('helvetica', 'BU', 9);
    PDF::Cell(195, 7, 'D/ RETENUE EFFECTUE',  'LR', 0, 'L');
    PDF::SetFont('aealarabiya', '', 8);
    PDF::Ln();

    PDF::Cell(5, 5, '',  'L', 0, 'L');
    PDF::Cell(32, 5, 'Montant HTVA',  'LT', 0, 'C');
    PDF::Cell(25, 5, 'Taux TVA',  'LTR', 0, 'C');
    PDF::Cell(32, 5, 'TVA Due',  'LTR', 0, 'C');
    PDF::Cell(32, 5, 'Montant total',  'LTR', 0, 'C');
    PDF::Cell(32, 5, 'TVA R à S',  'LTR', 0, 'C');
    PDF::Cell(32, 5, 'Retenue à la source',  'LTR', 0, 'C');
    PDF::Cell(5, 5, '',  'R', 0, 'L');
    PDF::Ln();

    $tva = ($invoice->items->sum('product_tva') / $invoice->items->count()) ?? 7;
    $ras = (getSettingValue('Pourcentage RàS') ?? config('stock.ras'));
    $htva_amount = $invoice->total / (1 + $tva / 100);
    $tva_amount = $invoice->total - $htva_amount;
    $ras_amount = $invoice->total * ($ras / 100);

    PDF::Cell(5, 5, '',  'L', 0, 'L');
    PDF::Cell(32, 5, '(1)',  'L', 0, 'C');
    PDF::Cell(25, 5, '(2)',  'LR', 0, 'C');
    PDF::Cell(32, 5, '(1)x(2)',  'LR', 0, 'C');
    PDF::Cell(32, 5, '(3)',  'LR', 0, 'C');
    PDF::Cell(32, 5, '(1)x(2)/(2)',  'LR', 0, 'C');
    PDF::Cell(16, 5, 'Taux(%)',  'LTR', 0, 'L');
    PDF::Cell(16, 5, 'Montant',  'LTR', 0, 'R');
    PDF::Cell(5, 5, '',  'R', 0, 'L');
    PDF::Ln();
    PDF::Cell(5, 5, '',  'L', 0, 'L');
    PDF::Cell(32, 5, number_format($htva_amount, 3),  'LTB', 0, 'C');
    PDF::Cell(25, 5, $tva,  'LTRB', 0, 'C');
    PDF::Cell(32, 5, number_format($tva_amount, 3),  'LTRB', 0, 'C');
    PDF::Cell(32, 5, $invoice->total,  'LTRB', 0, 'C');
    PDF::Cell(32, 5, '',  'LTRB', 0, 'C');
    PDF::Cell(16, 5, $ras, 'LTRB', 0, 'L');
    PDF::Cell(16, 5, number_format($ras_amount, 3),  'LTRB', 0, 'R');
    PDF::Cell(5, 5, '',  'R', 0, 'L');
    PDF::Ln();

    PDF::Cell(195, 15, '',  'LBR', 0, 'L');
    PDF::Ln(20);
    PDF::Cell(195, 0, 'Je soussigné, certifié exacts et sincères les renseignements figurants sur le présent certificat', 0, 0, 'L');
    PDF::Ln(5);
    PDF::Cell(195, 0, 'et m\'expose aux sanctions prévues par la loi pour toutes inexactitudes.', 0, 0, 'L');
    PDF::Ln(5);
    PDF::Cell(195, 0, 'A Tunis LE ' . date('d/m/Y'), 0, 0, 'L');
    PDF::Ln(5);
    PDF::SetFont('helvetica', '', 6);
    PDF::Cell(195, 0, '(Signature et Cachet de débiteur)', 0, 0, 'L');
    PDF::SetFont('helvetica', 'I', 8);
    PDF::lastPage();

    return PDF::Output($title . '.pdf', 'I');
  }
}
