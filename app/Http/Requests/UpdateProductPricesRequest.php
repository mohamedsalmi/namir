<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductPricesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'prices.*.product_attribute1' => 'required_if:type,Dépend d\'un attribut|required_if:type,Dépend de deux attributs',
            'prices.*.product_value1' => 'required_if:type,Dépend d\'un attribut|required_if:type,Dépend de deux attributs',
            'prices.*.product_attribute2' => 'required_if:type,Dépend de deux attributs',
            'prices.*.product_value2' => 'required_if:type,Dépend de deux attributs',
            'prices.*.buying' => 'required|numeric|min:0',
            'prices.*.selling1' => 'required|numeric|min:0',
            'prices.*.selling2' => 'required|numeric|min:0',
            'prices.*.selling3' => 'required|numeric|min:0',
        ];

        return $rules;
    }
}
