<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'price_type.type' => 'required',
            'quantity_type.type' => 'required',
            'price_type.attributes' => 'required_if:price_type.type,Dépend d\'un attribut|required_if:price_type.type,Dépend de deux attributs|min:' . $this->getMinAttributesLength('price_type'),
            'quantity_type.attributes' => 'required_if:quantity_type.type,Dépend d\'un attribut|required_if:quantity_type.type,Dépend de deux attributs|min:' . $this->getMinAttributesLength('quantity_type'),
        ];

        return $rules;
    }

    public function getMinAttributesLength($type)
    {
        $min = '0';
        if ($this->$type['type'] == 'Dépend de deux attributs') {
            $min = '2';
        } else if ($this->$type['type'] == 'Dépend d\'un attribut'){
            $min = '1';
        } 
        return $min;
    }
}
