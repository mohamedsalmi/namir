<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = $this->route('category');

        $rules = [
            'name' => ['required', Rule::unique('categories')->where(function ($query) use ($category) {
                if($this->parent_id){
                    return $query->where('parent_id', $this->parent_id)->whereNot('id', $category->id);
                }else{
                    return $query->where('parent_id', null)->whereNot('id', $category->id);
                }
            })],
            'parent_id' => 'nullable',
        ];

        return $rules;
    }
}
