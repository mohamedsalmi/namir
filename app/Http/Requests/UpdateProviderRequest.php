<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $provider = $this->route('provider');
        $rules = [
            'name' => 'required|unique:providers,name,'.$provider->id,
            'provider_code'=>'nullable|sometimes|unique:providers,provider_code,'.$provider->id,
            'avatar' => 'image',
        ];

        return $rules;
    }
}
