<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductQuantitiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'quantities.*.product_attribute1' => 'required_if:type,Dépend d\'un attribut|required_if:type,Dépend de deux attributs',
            // 'quantities.*.product_value1' => 'required_if:type,Dépend d\'un attribut|required_if:type,Dépend de deux attributs',
            // 'quantities.*.product_attribute2' => 'required_if:type,Dépend de deux attributs',
            // 'quantities.*.product_value2' => 'required_if:type,Dépend de deux attributs',
            // 'quantities.*.minimum_quantity' => 'required|numeric|min:0',
            'quantities' => 'min:1|array|required',
            'quantities.*.quantity' => 'required|numeric',
        ];

        return $rules;
    }
}
