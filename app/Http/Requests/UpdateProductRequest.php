<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->route('product');
        
        $rules = [
            // 'name' => 'required|unique:products,name,' . $product->id,
            // 'sku' => 'nullable|sometimes|unique:products,sku,'. $product->id,
            // 'reference' => 'required|unique:products,reference,' . $product->id,
            'unity' => 'required',
            'tva' => 'required',
            'currency_id' => 'required',
            // 'categories' => 'required',
            'attributes' => 'array',
            'attributes.*.attribute_id' => 'required',
            'attributes.*.attribute_values' => 'required',
        ];

        return $rules;
    }

    public function getMinAttributesLength()
    {
        $min = '0';
        if ($this->price_type['type'] == 'Dépend de deux attributs' || $this->quantity_type['type'] == 'Dépend de deux attributs') {
            $min = '2';
        } else if ($this->price_type['type'] == 'Dépend d\'un attribut' || $this->quantity_type['type'] == 'Dépend d\'un attribut'){
            $min = '1';
        } 
        return $min;
    }
}
