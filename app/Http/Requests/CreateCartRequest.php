<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'client_id' => 'required',
            'client_details.name' => 'required',
            'client_details.mf' => 'required',
            'client_details.adresse' => 'required',
            'client_details.phone' => 'required',
            'Prix_T' => 'required',
            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',
            'Deadline.*.amount'=>'required_if:Deadline_check,on',
            'Deadline.*.date'=>'required_if:Deadline_check,on',
            'Deadline.*.currency_id'=>'required_if:Deadline_check,on',
            'paiements'=> [Rule::requiredIf(function() {
                return (empty($this->request->get('Deadline_check')))  ;
            })],
            'paiements.cash.*.amount'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['cash']))  ;
            })],
            'paiements.cash.*.currency_id'=>[Rule::requiredIf(function() {
                return (!empty($this->paiements['cash']))  ;
            })],
            'paiements.check.*.amount'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['check']))  ;
            })],
            'paiements.check.*.numbre'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['check']))  ;
            })],
            'paiements.check.*.bank'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['check']))  ;
            })],
            'paiements.check.*.received_at'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['check']))  ;
            })],
            'paiements.transfer.*.amount'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['transfer']))  ;
            })],
            'paiements.transfer.*.numbre'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['transfer']))  ;
            })],
            'paiements.transfer.*.bank'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['transfer']))  ;
            })],
            'paiements.transfer.*.received_at'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['transfer']))  ;
            })],
            'paiements.exchange.*.amount'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['exchange']))  ;
            })],
            'paiements.exchange.*.numbre'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['exchange']))  ;
            })],
            'paiements.exchange.*.received_at'=> [Rule::requiredIf(function() {
                return (!empty($this->paiements['exchange']))  ;
            })],

        ];

        return $rules;
    }
}
