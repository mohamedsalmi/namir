<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currency = $this->route('currency');

        $rules = [
            'name' => 'required|unique:currencies,name,' . $currency->id,
            'value' => 'required|numeric|min:0',
            'symbol' => 'required|unique:currencies,symbol,' . $currency->id,
        ];

        return $rules;
    }
}
