<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attribute = $this->route('attribute');

        $rules = [
            'name' => 'required|unique:attributes,name,' . $attribute->id,
            'type' => 'required',
            'attribute_values' => 'required_unless:type,color',
            'colors' => 'array|min:1|required_if:type,color',
            'colors.*.color_name' => 'required_if:type,color',
            'colors.*.color_code' => 'required_if:type,color',
        ];

        return $rules;
    }
}
