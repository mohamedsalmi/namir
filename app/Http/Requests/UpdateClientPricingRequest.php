<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientPricingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'pricings.*.discount' => 'required_if:type,Dépend d\'un attribut|required_if:type,Dépend de deux attributs',
            'price_cv' => 'required_with:date_cv',
            'date_cv' => 'required_with:price_cv',
            // 'pricings.*.product_value2' => 'required_if:type,Dépend de deux attributs',
            // 'pricings.*.quantity' => 'required|numeric',
            // 'pricings.*.minimum_quantity' => 'required|numeric|min:0',
        ];

        return $rules;
    }
}
