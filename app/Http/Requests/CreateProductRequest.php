<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'images.*' => 'image',
            // 'minimum_quantity' => 'required_if:duplicated_product,0',
            // 'reserve_quantity' => 'required_if:duplicated_product,0',
            //'buying' => 'required_if:duplicated_product,0',
            'selling1' => 'required_if:duplicated_product,0',
            'selling2' => 'required_if:duplicated_product,0',
            //'selling3' => 'required_if:duplicated_product,0',
            'sku' => 'nullable|sometimes|unique:products,sku',
            // 'reference' => 'required_if:duplicated_product,0|unique:products,reference',
            'unity' => 'required',
            'currency_id' => 'required',
            // 'categories' => 'required',
            'attributes' => 'array',
            
            'tva' => 'required|min:0|max:100',
            'maxdiscount' => 'required|min:0|max:100',
            // 'attributes' => ['array', 'required','min:2'],
            'attributes.*.attribute_id' => 'required',
            'attributes.*.attribute_values' => 'required',

            'duplications' => 'array',
            // 'duplications.*.minimum_quantity' => 'required_if:duplicated_product,1',
            // 'duplications.*.reserve_quantity' => 'required_if:duplicated_product,1',
            'duplications.*.buying' => 'required_if:duplicated_product,1',
            'duplications.*.selling1' => 'required_if:duplicated_product,1',
            'duplications.*.selling2' => 'required_if:duplicated_product,1',
            'duplications.*.selling3' => 'required_if:duplicated_product,1',
            'duplications.*.sku' => 'distinct|unique:products,sku',
            // 'duplications.*.reference' => 'distinct|required_if:duplicated_product,1|unique:products,reference',
        ];

        return $rules;
    }

    public function getMinAttributesLength()
    {
        $min = '0';
        if ($this->price_type['type'] == 'Dépend de deux attributs' || $this->quantity_type['type'] == 'Dépend de deux attributs') {
            $min = '2';
        } else if ($this->price_type['type'] == 'Dépend d\'un attribut' || $this->quantity_type['type'] == 'Dépend d\'un attribut'){
            $min = '1';
        }
        return $min;
    }
}
