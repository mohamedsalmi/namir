<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCartItemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'cart_id' => 'required',
            'product_id' => 'required',
            'product_name' => 'required',
            'product_currency_id' => 'required',
            'product_currency_value' => 'required',
        ];

        return $rules;
    }
}
