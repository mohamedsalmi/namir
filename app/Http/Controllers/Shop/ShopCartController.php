<?php

namespace App\Http\Controllers\Shop;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewOrderRequest;
use App\Models\Coupon;
use App\Models\OnlineSale;
use App\Models\Order;
use App\Models\Product;
use App\Models\Region;
use App\Models\Store;
use App\Models\User;
use App\Notifications\NewWebsiteOrderNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShopCartController extends Controller
{
    public function index(Request $request)
    {
        return view('shop.cart.index');
    }

    public function checkout(Request $request)
    {
        $states = Region::groupBy('state')->get();
        $cities = Region::groupBy('city')->get();
        return view('shop.cart.checkout', compact('states', 'cities'));
    }

    public function orderSuccess(Request $request)
    {
        return view('shop.cart.success');
    }

    // public function addToCart(Request $request)
    // {
    //     $data = $request->only('id', 'quantity');
    //     $product = Product::find($data['id']);
    //     $product = array_merge($data, $product->toArray());
    //     // dd($product);
    //     $cart = session()->get('cart');
    //     if (!is_array($cart)) {
    //         $cart = [];
    //     }
    //     if (in_array($product, $cart)) {
    //         return response()->json([
    //             'count' => count(session('cart')),
    //             'status' => 'info',
    //             'message' => __('Produit déja ajouté au panier'),
    //             'title' => __('Oops'),
    //         ]);
    //     } else {
    //         if ($oldProduct = Helper::custom_in_array($cart, $product['id'])) {
    //             if (($key = array_search(array_values($oldProduct)[0], $cart)) !== false) {
    //                 unset($cart[$key]);
    //             }
    //             session()->put('cart', $cart);
    //         }

    //         session()->push('cart', $product);

    //         return response()->json([
    //             'count' => count(session('cart')),
    //             'status' => 'success',
    //             'message' => __('Produit ajouté au panier'),
    //             'title' => __('Bravo !'),
    //         ]);
    //     }
    // }
    public function addToCart(Request $request)
    {
        $product = Product::find($request->product_id);
        $store=Store::where('website_sales', true)->first()->id ?? Store::first()->id;
        $status ='success';
        if ($product->quantities->where('store_id',$store)->sum('quantity') < $request->quantity) {
            if ($product->by_order) {
                    $status ='warning';
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Product is out of stock',
                ]);
            }
            
        }
        $cart = session()->get('cart');
        if (!$cart) {
            $cart = [
                $request->product_id => [
                    "id" => $product->id,
                    "name" => $product->name,
                    "quantity" => $request->quantity,
                    "selling1" => $product->selling1,
                    "selling2" => $product->selling2,
                    "discount" => $product->discount,
                    "default_image_url" => $product->default_image_url,
                    "writer" => $product->writer,
                    "house" => $product->house
                ]
            ];
            session()->put('cart', $cart);
            return response()->json([
                'status' => $status,
                'message' => 'Product added to cart',
                'cart' => session()->get('cart'),
                'item_section' => view('shop.layouts.item-section')->render(),
                'cart_hover' => view('shop.layouts.cart-hover')->render()
            ]);
        }
        if (isset($cart[$request->product_id])) {
            $cart[$request->product_id]['quantity'] = $request->quantity;
            session()->put('cart', $cart);
            return response()->json([
                'status' => $status,
                'message' => 'Product added to cart',
                'cart' => session()->get('cart'),
                'item_section' => view('shop.layouts.item-section')->render(),
                'cart_hover' => view('shop.layouts.cart-hover')->render()
            ]);
        }
        $cart[$request->product_id] = [
            "id" => $product->id,
            "name" => $product->name,
            "quantity" => $request->quantity,
            "selling1" => $product->selling1,
            "selling2" => $product->selling2,
            "discount" => $product->discount,
            "default_image_url" => $product->default_image_url,
            "writer" => $product->writer,
            "house" => $product->house
        ];
        session()->put('cart', $cart);
        return response()->json([
            'status' => $status,
            'message' => 'Product added to cart',
            'cart' => session()->get('cart'),
            'item_section' => view('shop.layouts.item-section')->render(),
            'cart_hover' => view('shop.layouts.cart-hover')->render()
        ]);
    }

    // public function deleteCartItem(Request $request)
    // {
    //     $data = $request->only('id', 'quantity');
    //     $product = Product::find($data['id']);
    //     $product = array_merge($data, $product->toArray());

    //     $cart = session()->get('cart');
    //     if (!is_array($cart)) {
    //         $cart = [];
    //     }
    //     if (in_array($product, $cart)) {
    //         if (($key = array_search($product, $cart)) !== false) {
    //             unset($cart[$key]);
    //         }
    //         session()->put('cart', $cart);
    //         return response()->json([
    //             'count' => count(session('cart')),
    //             'status' => 'success',
    //             'message' => __('Produit supprimé du panier'),
    //             'title' => __(''),
    //         ]);
    //     }
    //     return '';
    // }
    public function deleteCartItem(Request $request)
    {
        if ($request->ajax()) {
            $product_id = $request->input('product_id');
            if (session()->has('cart')) {
                $cart = session()->get('cart');
                if (array_key_exists($product_id, $cart)) {
                    unset($cart[$product_id]);
                    session()->put('cart', $cart);
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Product removed from cart',
                        'cart' => session()->get('cart'),
                        'item_section' => view('shop.layouts.item-section')->render(),
                        'cart_hover' => view('shop.layouts.cart-hover')->render()
                    ]);
                }
            }
        }
        return response()->json(['success' => false]);
    }


    public function order(NewOrderRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $codification_setting = 'Codification vente en ligne';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            $data['codification'] = $nextCodification;
            $discount = 0;
            if (session()->has('coupon') && !empty(session()->get('coupon'))) {
                $coupon = Coupon::find(session()->get('coupon'));
                $discount = $coupon ? $coupon->discount : 0;
            }
            if (auth()->user()) {
                $data['client_id'] = auth()->user()->id;
            } else {
                $data['client_id'] = 0;
            }

            $cart = session()->get('cart');
            $number = count($cart);
            $data['number'] = $number;
            $data['date'] = date('Y-m-d');
            $total = 0;
            $data['store_id'] = Store::where('website_sales', true)->first()->id ?? Store::first()->id;
            $data['coupon'] = isset($coupon->code) ? $coupon->code : 0;
            $data['source'] = 'site';

            foreach ($cart as $key => $item) {
                $item = (object)$item;
                $product = Product::select('buying', 'tva', 'currency_id', 'unity', 'selling1', 'selling2')->where('id', $item->id)->first();
                $product_price_selling = $product->selling1 > $product->selling2 && $product->selling2 != 0 ? $product->selling2 : $product->selling1;
                $total += $product_price_selling * $item->quantity;
                $data['items'][$key]['product_id'] = $item->id;
                $data['items'][$key]['product_price_selling'] = $product_price_selling;
                $data['items'][$key]['product_quantity'] = $item->quantity;
                $data['items'][$key]['product_price_buying'] = $product->buying;
                $data['items'][$key]['product_tva'] = $product->tva;
                $data['items'][$key]['product_name'] = $item->name . '-' . $item->writer . '-'  . $item->house;
                $data['items'][$key]['product_currency_id'] = $product->currency_id;
                $data['items'][$key]['product_currency_value'] = $product->currency->symbol;
                $data['items'][$key]['product_unity'] = $product->unity;
                $data['items'][$key]['product_remise'] = $discount;
            }

            $data['total'] = $total * (1 - $discount / 100);

            $order = OnlineSale::create($data);
            $order->items()->createMany($data['items']);

            DB::commit();
            session()->put('cart', []);
            session()->put('coupon');

            $admins = User::role('Admin')->get();
            foreach ($admins as $key => $admin) {
                $admin->notify(new NewWebsiteOrderNotification([
                    'codification' => $order->codification,
                ]));
            }
            
            return redirect()->route('shop.cart.order.success')->with('order', $order);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
