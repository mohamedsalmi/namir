<?php

namespace App\Http\Controllers\Shop;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCouponRequest;
use App\Http\Requests\UpdateCouponRequest;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CouponController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Coupon::query();
            return DataTables::eloquent($data)
                ->editColumn('status', function ($row) {
                    return $row->status ? '<span class="badge bg-success">Activé</span>' : '<span class="badge bg-danger">Désactivé</span>';
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier attribut') ? '<a href="' . route('admin.coupon.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer attribut') ? '<form method="POST" action="' . route('admin.coupon.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'status'])

                ->make(true);
        }
        return view('admin.coupon.index');
    }

    public function checkCoupon(Request $request)
    {
        $data = $request->only('code');
        $coupon = Coupon::where(['code' => $data['code'], 'status' => true])->where('expired_at', '>=',  date('Y-m-d'))->first();
        $session_coupon = session()->get('coupon');
        if (!is_array($session_coupon)) {
            $session_coupon = [];
        }

        if ($coupon) {
            session()->put('coupon', $coupon->id);
            return response()->json([
                'coupon' => $coupon,
                'success' => true,
                'message' => __('Coupon valide'),
            ]);
        } else {
            session()->put('coupon', null);
            return response()->json([
                'success' => false,
                'message' => __('Coupon invalide'),
            ]);
        }
    }

    public function create()
    {
        return view('admin.coupon.create');
    }

    public function store(CreateCouponRequest $request)
    {
        try {
            $data = $request->all();
            $coupon = Coupon::create($data);
           
            return redirect()->route('admin.coupon.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Request $request, Coupon $coupon)
    {
        return view('admin.coupon.edit', compact('coupon'));
    }

    public function update(UpdateCouponRequest $request, Coupon $coupon)
    {
        try {
            DB::beginTransaction();

            $data = $request->all();
            $coupon->update($data);

            DB::commit();
            return redirect()->route('admin.coupon.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Coupon $coupon)
    {
        try {
            $coupon->delete();
            return redirect()->route('admin.coupon.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
