<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request, $ajax = false)
    {
        $query = Product::query();
        $query->select('id', 'name', 'selling1', 'selling2', 'discount', 'resume', 'description','by_order')->where('status', 1);
        if ($request->has('name') && !empty($request->get('name'))) {
            $name = $request->get('name');
            $query->where('name', 'like', "%$name%");
        }
        if ($request->has('writer') && !empty($request->get('writer'))) {
            $writer = $request->get('writer');
            $query->whereHas('productAttributes', function ($query) use ($writer) {
                $query->whereHas('attribute', function ($q) {
                    $q->where('name', 'اسم المؤلف');
                });
                $query->whereHas('values', function ($query) use ($writer) {
                    $query->whereHas('attributeValue', function ($query) use ($writer) {
                        $query->where('value', 'like', "%$writer%");
                    });
                });
            });
        }
        if ($request->has('publisher') && !empty($request->get('publisher'))) {
            $publisher = $request->get('publisher');
            $query->whereHas('productAttributes', function ($query) use ($publisher) {
                $query->whereHas('attribute', function ($q) {
                    $q->where('name',  'دار النشر');
                });
                $query->whereHas('values', function ($query) use ($publisher) {
                    $query->whereHas('attributeValue', function ($query) use ($publisher) {
                        $query->where('value', 'like', "%$publisher%");
                    });
                });
            });
        }

        if ($request->has('deal') && !empty($request->get('deal'))) {
            $deal = $request->get('deal');
            $query->whereHas('productAttributes', function ($query) use ($deal) {
                $query->whereHas('attribute', function ($q) {
                    $q->where('name',  'العروض');
                });
                $query->whereHas('values', function ($query) use ($deal) {
                    $query->whereHas('attributeValue', function ($query) use ($deal) {
                        $query->where('value', 'like', "%$deal%");
                    });
                });
            });
        }

        if ($request->has('categories') && !empty($request->get('categories'))) {
            $categories = explode(",", $request->get('categories'));
            $query->whereHas('categories', function ($query) use ($categories) {
                $query->whereIn('category_id', $categories);
            });
        }
        if ($request->has('discounts') && !empty($request->get('discounts'))) {
            $discounts = explode(",", $request->get('discounts'));
            $query->where(function ($query) use ($discounts) {
                foreach ($discounts as $key => $discount) {
                    $range = explode("-", $discount);
                    if ($key == 0) {
                        $query->whereBetween('discount', $range);
                    } else {
                        $query->orWhereBetween('discount', $range);
                    }
                }
            });
        }
        if ($request->has('price') && !empty($request->query('price'))) {
            $range = explode(";", $request->query('price'));
            $query->where(function ($query) use ($range) {
                $query->whereBetween('selling1', $range);
            });
        }

        $sort_by = $request->has('sort_by') && !empty($request->get('sort_by')) ? $request->get('sort_by') : 'created_at';
        $direction = $request->has('direction') && !empty($request->get('direction')) ? $request->get('direction') : 'desc';
        $query->orderBy($sort_by, $direction);

        $data = $query->paginate(20);
        $data->appends(request()->all());

        $categories = Category::select('id', 'name')->get();

        // dd($data, $categories);
        $max_price = Product::max('selling1');

        if($request->ajax()){
            return response()->json([
                'status' => 'success',
                'html' => view('shop.shop.ajax', compact('data', 'categories', 'max_price'))->render()
            ]);
        }

        return view('shop.shop.index', compact('data', 'categories', 'max_price'));
    }

    public function show(Product $product)
    {
        $categories_ids = $product->categories->pluck('category_id')->toArray();
        $related_products = Product::whereNot('id', $product->id)->whereHas('categories', function ($query) use ($categories_ids) {
            $query->whereIn('category_id', $categories_ids);
        })->limit(6)->get();

        if ($this->getProductsByAttributeValue('الاكثر مبيعا')->count() > 0) {
            $trending_products = $this->getProductsByAttributeValue('الاكثر مبيعا');
        } else {
            $items = Product::has('orderitems')->orHas('cartitems')->orHas('onlinesaleitems')->get();

            $trending_products = $items->sortByDesc(function ($item) {
                return $item->orderitems->sum('product_quantity') + $item->cartitems->sum('product_quantity') + $item->onlinesaleitems->sum('product_quantity');
            })->slice(0, 4);
        }

        return view('shop.shop.show', ['product' => $product, 'related_products' => $related_products, 'trending_products' => $trending_products]);
    }

    private function getProductsByAttributeValue($value, $limit = null)
    {
        return Product::whereHas('productAttributes', function ($query) use ($value) {
            $query->whereHas('attribute', function ($q) {
                $q->where('name',  'العروض');
            });
            $query->whereHas('values', function ($q) use ($value) {
                $q->whereHas('attributeValue', function ($q) use ($value) {
                    $q->where('value', $value);
                });
            });
        })->when($limit, function ($query) use ($limit) {
            return $query->take($limit);
        })->limit(4)->get();
    }
}
