<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Mail\SendMessage;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        $sliders = Slider::all();
        $weeklies = $this->getProductsByAttributeValue('العروض الاسبوعية', 5);
        $recently = Product::latest()->take(12)->get();
        $on_sells = $this->getProductsByAttributeValue('العروض المحدودة');
        $best_sells = $this->getProductsByAttributeValue('الاكثر مبيعا');
        $features = $this->getProductsByAttributeValue('المميزة');
        $news = $this->getProductsByAttributeValue('حديثا');
        $testimonials = Testimonial::all();
        return view('shop.home', compact('sliders', 'weeklies', 'news', 'features', 'best_sells', 'on_sells', 'testimonials', 'recently'));
    }
    
    private function getProductsByAttributeValue($value, $limit = null)
    {
        return Product::whereHas('productAttributes', function ($query) use ($value) {
            $query->whereHas('attribute', function ($q) {
                $q->where('name',  'العروض');
            });
            $query->whereHas('values', function ($q) use ($value) {
                $q->whereHas('attributeValue', function ($q) use ($value) {
                    $q->where('value', $value);
                });
            });
        })->when($limit, function ($query) use ($limit) {
            return $query->take($limit);
        })->get();
    }
    
    public static function send_mail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $data = $request->all();
        Mail::to('saif.elarbi@ryeda.com')->send(new SendMessage($data));
        return redirect()->route('shop.contact_us')->with('success', __('app.form_contact.success_message'));
    }
    public function hotDeals()
    {
        $products = Product::select('id', 'name', 'selling1', 'discount')
            ->orderBy('discount', 'desc')
            ->limit(12)
            ->get();

        return view('shop.partials.dealmodal', compact('products'));
    }
}
