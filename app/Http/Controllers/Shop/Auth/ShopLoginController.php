<?php

namespace App\Http\Controllers\Shop\Auth;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;


class ShopLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo()
    {
        return '/';
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        return view('shop.auth.login');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::guard('shop')->attempt($credentials, true)) {
            return redirect()
                ->intended(route('shop.home'))
                ->with('status', 'You are Logged in as Admin!');
        }

        $errors = new MessageBag(['email' => [ __('auth.failed')]]);
        return Redirect::back()->withErrors($errors)->withInput($request->only('email', 'remember'));
    }
    /*********** */
    // protected $redirectTo = '/dealer';


    public function logout()
    {
        Session::flush();
        Auth::guard('shop')->logout();
        return redirect('/login');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'username or password is incorrect!!!'
        ]);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function handleProviderCallback($provider)
    {
        //    dd('qdsa');
 
        $user = Socialite::driver($provider)->stateless()->user();
        $existingUser = Client::where('email', $user->email)->first();
        if ($existingUser) {
            // log the user in
            Auth::guard('shop')->login($existingUser, true);
            return redirect()->intended(route('shop.home'));
        } else {
            // create a new user
            $newUser = new Client;
            $newUser->name = $user->name;
            $newUser->email = $user->email;
            $newUser->site = 1;
            // $newUser->provider_id = $user->id;
            // $newUser->provider = $provider;
            $newUser->save();

            // log the new user in
            Auth::guard('shop')->login($newUser, true);
            return redirect()->intended(route('shop.home'));
        }
    }
}
