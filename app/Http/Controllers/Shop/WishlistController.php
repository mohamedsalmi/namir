<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\Request;

class WishlistController extends Controller
{


    public function addwishlist($product)
    {
        $client = auth()->user()->id;
        $wishlist = Wishlist::where(['product_id' => $product, 'client_id' => $client])->first();
        if ($wishlist) {
            $wishlist->delete();
            return 0;
        } else {
            Wishlist::create(['product_id' => $product, 'client_id' => $client]);
            return 1;
        }
       
    }
    public function delete(Wishlist $wishlist)
    {
        $wishlist->delete();
        return redirect()->route('shop.wishlist')->with('success', "Supprimé avec succès!");    }
}
