<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductBarcodesRequest;
use App\Http\Requests\UpdateProductPricesRequest;
use App\Http\Requests\UpdateProductQuantitiesRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Requests\UpdateProductSettingsRequest;
use App\Imports\ProductsImport;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Cart;
use App\Models\CartsItems;
use App\Models\Category;
use App\Models\Image;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductLog;
use App\Models\Quantity;
use App\Models\Setting;
use App\Models\Store;
use App\Notifications\MinQuantityNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::query();
            if ($request->has('name') && !empty($request->query('name'))) {
                $name = $request->query('name');
                $data->where('name', 'like', "%$name%");
            }
            if ($request->has('sku') && !empty($request->query('sku'))) {
                $sku = $request->query('sku');
                $data->where('sku', 'like', "%$sku%");
            }
            $status = $request->has('status') ? $request->query('status') : '1';
            // dd($status);
            if ($status == '0' || $status == '1') {
                $data->where('status', $status);
            }
            if ($request->has('categories') && !empty($request->query('categories'))) {
                $ids = explode(",", $request->query('categories'));
                $data->whereHas('categories', function ($query) use ($ids) {
                    $query->whereIn('category_id', $ids);
                });
            }
            if ($request->has('attributes') && !empty($request->query('attributes'))) {
                foreach ($request->query('attributes') as $key => $attribute) {
                    // dd($attribute);
                    if (!empty($attribute['attribute_id'])) {
                        $attribute_id = $attribute['attribute_id'];
                        $data->whereHas('productAttributes', function ($query) use ($attribute_id, $attribute) {
                            $query->where('attribute_id', $attribute_id);
                            // if (isset($attribute['attribute_values']) && !empty(array_filter($attribute['attribute_values']))) {
                            //     $ids = array_filter($attribute['attribute_values']);
                            //     $query->whereHas('values', function ($query) use ($ids) {
                            //         $query->whereIn('attribute_value_id', $ids);
                            //     });
                            // }
                            if (isset($attribute['attribute_values']) && !empty($attribute['attribute_values'])) {
                                $ids = $attribute['attribute_values'];
                                $query->whereHas('values', function ($q) use ($ids) {
                                    $q->whereHas('attributeValue', function ($q) use ($ids) {
                                        $q->where('value',  'like', '%' . $ids . '%');
                                    });
                                });
                            }
                        });
                    }
                }
            }
            if ($request->has('price') && !empty($request->query('price'))) {
                $range = explode(",", $request->query('price'));
                $data->where(function ($query) use ($range) {
                    $query->whereBetween('selling1', $range)
                        ->orWhereBetween('selling2', $range)
                        ->orWhereBetween('selling3', $range);
                });
            }
            if ($request->has('date') && $request->query('date') != 'undefined') {
                $date = explode('/', $request->query('date'));
                $data->whereBetween('created_at', [$date[0] . ' 00:00:00', $date[1] . ' 23:59:59']);
            }

            return DataTables::eloquent($data->withSum('quantities', 'quantity'))
                ->editColumn('name', function ($row) {
                    $html =
                        ' <div class="d-flex align-items-center gap-3">
                         <a class="elem " href="' . $row->default_image_url . '"
                        data-lcl-thumb="' . $row->default_image_url . '">
                        <div class="product-box border">
                        <img class="img-fluid img-responsive product-img"
                            src="' . $row->default_image_url . '" alt="' . $row->name . '">
                            </div>
                    </a>
                        <div class="product-info">
                            <h6 class="product-name mb-1">' . $row->name . '</h6>
                        </div>
                    </div>';
                    return $html;
                })
                ->addColumn('check', function ($row) {
                    $html =
                        '<div class="form-check">
                        <input name="product_id" id="product_id" value="' . $row->id . '"class="form-check-input" type="checkbox">
                         </div>';
                    return $html;
                })
                ->filterColumn('name', function ($query, $keyword) {
                    $query->where('name', 'like', "%$keyword%")->orWhereHas('productAttributes', function ($query) use ($keyword) {
                        $query->whereHas('attribute', function ($query) use ($keyword) {
                            $query->where('name', 'like', "%$keyword%");
                        })->orWhereHas('values', function ($query) use ($keyword) {
                            $query->whereHas('attributeValue', function ($query) use ($keyword) {
                                $query->where('value', 'like', "%$keyword%");
                            });
                        });
                    });
                })
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                    return $formattedDate;
                })
                ->editColumn('status', function ($row) {
                    return $row->status ? '<span class="badge bg-success">Activé</span>' : '<span class="badge bg-danger">Désactivé</span>';
                })
                ->editColumn('selling1', function ($row) {
                    return   '<span style="float:right;">' . $row->selling1 . '</span>';
                })
                ->addColumn('quantity', function ($row) {
                    // if (auth()->user()->role == 'Admin') $quantity = Quantity::where(['product_id' => $row->id])->sum('quantity');
                    // else 
                    $quantity = Quantity::where(['product_id' => $row->id, 'store_id' => auth()->user()->store->id])->sum('quantity');

                    return $quantity ? $quantity : '0';
                })
                ->orderColumn('quantity', function ($query, $order) {
                    $query->orderBy('quantities_sum_quantity', $order);
                })
                ->addColumn('writer', function ($row) {
                    $writerProductAttribute = ProductAttribute::where('product_id', $row->id)->whereHas('attribute', function ($q) {
                        $q->where('name', 'اسم المؤلف');
                    })->first();

                    $writerName =  $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';

                    return $writerName;
                })
                ->addColumn('publisher', function ($row) {
                    $publisherProductAttribute = ProductAttribute::where('product_id', $row->id)->whereHas('attribute', function ($q) {
                        $q->where('name', 'دار النشر');
                    })->first();

                    $publisherName =  $publisherProductAttribute ?  $publisherProductAttribute?->values?->first()?->attributeValue?->value : '-';

                    return $publisherName;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails article') ? '<a title="Détails" href="' . route('admin.product.show', $row->id) . '" class="text-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Détails" aria-label="Edit"><i class="bi bi-eye"></i></a>' : '';
                    $updateBtn = auth()->user()->can('Modifier article') ? '<a title="Modifier" href="' . route('admin.product.edit', $row->id) . '" class="text-success" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>' : '';
                    $settingsBtn = auth()->user()->can('Modifier les paramètres') ? '<a title="Modifier les paramètres" href="' . route('admin.product.settings', $row->id) . '" class="text-secondary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Paramètres" aria-label="Paramètres"><i class="bi bi-nut-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer article') ? '<form method="POST" action="' . route('admin.product.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <a href="javascript:;" id="show_confirm" type="submit" class="text-danger show_confirm" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Supprimer" aria-label="Supprimer"><i class="bi bi-trash-fill"></i></a>
                    </form>' : '';

                    $priceBtn = auth()->user()->can('Modifier les prix') && $this->showBtn($row, 'price_type') ? '<a title="Modifier les prix" href="' . route('admin.product.prices', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Prix" aria-label="Prix"><i class="bi bi-currency-dollar"></i></a>' : '';
                    $quantityBtn = auth()->user()->can('Modifier les quantités') && $this->showBtn($row, 'quantity_type') ? '<a title="Modifier les quantités" href="' . route('admin.product.quantities', $row->id) . '" class="text-info" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Quantités" aria-label="Quantités"><i class="bi bi-stack"></i></a>' : '';
                    $barcodesBtn = auth()->user()->can('Modifier les quantités') && $this->showBtn($row, 'quantity_type') ? '<a title="Modifier les codes à brre" href="' . route('admin.product.barcodes', $row->id) . '" class="text-success" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Code à barre" aria-label="Code à barre"><i class="bi bi-upc"></i></a>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $showBtn . $updateBtn . $confirmDeletion . /*$settingsBtn . $priceBtn . $quantityBtn . $barcodesBtn.*/ '</div>';

                    return $btns;
                })

                ->rawColumns(['action', 'name', 'status', 'selling1', 'check'])

                ->make(true);
        }

        $max_price = Product::select(DB::raw('GREATEST(buying,selling1, selling2, selling3) as max_price'))->get();
        $max_price = $max_price ? $max_price->max('max_price') : 0;
        $categories = Category::where('parent_id', null)->get();
        $attrs = Attribute::all();
        return view('admin.product.index', compact('categories', 'attrs', 'max_price'));
        // return view('admin.product.index',compact('categories', 'max_price'));
    }
    public function getData(Request $request)
    {

        $data = Product::query();
        if ($request->has('name') && !empty($request->query('name'))) {
            $name = $request->query('name');
            $data->where('name', 'like', "%$name%");
        }
        if ($request->has('sku') && !empty($request->query('sku'))) {
            $sku = $request->query('sku');
            $data->where('sku', 'like', "%$sku%");
        }


        if ($request->has('categories') && !empty($request->query('categories'))) {
            $ids = explode(",", $request->query('categories'));
            $data->whereHas('categories', function ($query) use ($ids) {
                $query->whereIn('category_id', $ids);
            });
        }
        if ($request->has('writer') && !empty($request->query('writer'))) {
            $attribute_id = 3;
            $data->whereHas('productAttributes', function ($query) use ($attribute_id, $request) {
                $query->where('attribute_id', $attribute_id);
                $query->whereHas('values', function ($q) use ($request) {
                    $q->whereHas('attributeValue', function ($q) use ($request) {
                        $q->where('value',  'like', '%' . $request->writer . '%');
                    });
                });
            });
        }
        if ($request->has('publisher') && !empty($request->query('publisher'))) {
            $attribute_id = 2;
            $data->whereHas('productAttributes', function ($query) use ($attribute_id, $request) {
                $query->where('attribute_id', $attribute_id);
                $query->whereHas('values', function ($q) use ($request) {
                    $q->whereHas('attributeValue', function ($q) use ($request) {
                        $q->where('value',  'like', '%' . $request->publisher . '%');
                    });
                });
            });
        }


        if ($request->has('price') && !empty($request->query('price'))) {
            $range = explode(",", $request->query('price'));
            $data->where(function ($query) use ($range) {
                $query->whereBetween('selling1', $range)
                    ->orWhereBetween('selling2', $range)
                    ->orWhereBetween('selling3', $range);
            });
        }
        if ($request->has('date') && $request->query('date') != 'undefined') {
            $date = explode('/', $request->query('date'));
            $data->whereBetween('created_at', [$date[0] . ' 00:00:00', $date[1] . ' 23:59:59']);
        }

        return DataTables::eloquent($data->withSum('quantities', 'quantity'))
            ->editColumn('name', function ($row) {
                $html =
                    ' <div class="d-flex align-items-center gap-3">
                         <a class="elem " href="' . $row->default_image_url . '"
                        data-lcl-thumb="' . $row->default_image_url . '">
                        <div class="product-box border">
                        <img class="img-fluid img-responsive product-img"
                            src="' . $row->default_image_url . '" alt="' . $row->name . '">
                            </div>
                    </a>
                        <div class="product-info">
                            <h6 class="product-name mb-1">' . $row->name . '</h6>
                        </div>
                    </div>';
                return $html;
            })
            ->addColumn('check', function ($row) {
                $html =
                    '<div class="form-check">
                        <input name="product_id" id="product_id" value="' . $row->id . '"class="form-check-input" type="checkbox">
                         </div>';
                return $html;
            })
            ->filterColumn('name', function ($query, $keyword) {
                $query->where('name', 'like', "%$keyword%")->orWhereHas('productAttributes', function ($query) use ($keyword) {
                    $query->whereHas('attribute', function ($query) use ($keyword) {
                        $query->where('name', 'like', "%$keyword%");
                    })->orWhereHas('values', function ($query) use ($keyword) {
                        $query->whereHas('attributeValue', function ($query) use ($keyword) {
                            $query->where('value', 'like', "%$keyword%");
                        });
                    });
                });
            })
            ->editColumn('updated_at', function ($row) {
                $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                return $formattedDate;
            })
            ->editColumn('status', function ($row) {
                return $row->status ? '<span class="badge bg-success">Activé</span>' : '<span class="badge bg-danger">Désactivé</span>';
            })
            ->editColumn('selling1', function ($row) {
                return   '<span style="float:right;">' . $row->selling1 . '</span>';
            })
            ->addColumn('quantity', function ($row) {
                // if (auth()->user()->role == 'Admin') $quantity = Quantity::where(['product_id' => $row->id])->sum('quantity');
                // else 
                $quantity = Quantity::where(['product_id' => $row->id, 'store_id' => auth()->user()->store->id])->sum('quantity');

                return $quantity ? $quantity : '0';
            })
            ->orderColumn('quantity', function ($query, $order) {
                $query->orderBy('quantities_sum_quantity', $order);
            })
            ->addColumn('writer', function ($row) {
                $writerProductAttribute = ProductAttribute::where('product_id', $row->id)->whereHas('attribute', function ($q) {
                    $q->where('name', 'اسم المؤلف');
                })->first();

                $writerName =  $writerProductAttribute ?  $writerProductAttribute?->values?->first()?->attributeValue?->value : '-';

                return $writerName;
            })
            ->addColumn('publisher', function ($row) {
                $publisherProductAttribute = ProductAttribute::where('product_id', $row->id)->whereHas('attribute', function ($q) {
                    $q->where('name', 'دار النشر');
                })->first();

                $publisherName =  $publisherProductAttribute ?  $publisherProductAttribute?->values?->first()?->attributeValue?->value : '-';

                return $publisherName;
            })
            ->addColumn('action', function ($row) {

                $btns = '
                        <a
                        id="addToCart' . $row->id . '"
                        class="btn btn-sm btn-primary px-2 rounded-pill addToCart " role="button"
                        product-id="' . $row->id . '"
                        product-name="' . $row->name . ' / '. $row->house. '"
                        price-buying="' . $row->buying . '"
                        price-selling1="' . $row->selling1 . '"
                        price-selling2="' . $row->selling2 . '"
                        price-selling3="' . $row->selling3 . '"
                        product-unity="Pièce"
                        product-tva="' . $row->tva . '"
                        product-sku="' . $row->sku . '"
                        product-currency="DM"
                        product-currency-id="1"
                        product-maxdiscount="' . $row->maxdiscount . '"
                        product-old-quantity="' . $row->quantities->where('store_id', auth()->user()->store_id)->sum('quantity') . '"
                        data-img="' . $row->id . '"
                        >
                        
                        Ajouter  </a>
                      ';

                return $btns;
            })

            ->rawColumns(['action', 'name', 'status', 'selling1', 'check'])

            ->make(true);



        // return view('admin.product.index',compact('categories', 'max_price'));
    }

    public function showBtn($product, $field)
    {
        $show = false;
        if ($product?->$field['type'] == 'Simple') {
            $show = true;
        } else if (($product?->$field['type'] == 'Dépend d\'un attribut' || $product?->$field['type'] == 'Dépend de deux attributs') && !empty($product?->$field['attributes'])) {
            $show = true;
        }
        return $show;
    }

    public function getSettings()
    {
        $selling1_coefficient = Setting::where('name', 'Coefficient prix détails')->first();
        $selling2_coefficient = Setting::where('name', 'Coefficient prix semi-gros')->first();
        $selling3_coefficient = Setting::where('name', 'Coefficient prix gros')->first();
        return (object) ['selling1_coefficient' => $selling1_coefficient?->value ?? 10, 'selling2_coefficient' => $selling2_coefficient?->value ?? 8, 'selling3_coefficient' => $selling3_coefficient?->value ?? 5];
    }

    public function create()
    {
        $settings = $this->getSettings();
        $attrs = Attribute::whereNot('name', 'الطبعة')->select('id', 'name')->get();
        $offers = Attribute::where('name', 'العروض')->get()->pluck('values')->collapse('values');

        // $attrs = Attribute::whereNot('name', 'الطبعة')->get()->load('values');
        return view('admin.product.create', compact('attrs', 'settings', 'offers'));
    }

    public function deleteImage(Image $image)
    {
        try {
            DB::beginTransaction();
            $fileuri = str_replace('/storage', '', $image->url);
            Storage::disk('public')->delete($fileuri);
            if ($image->is_default) {
                $nextImage = $image->product->images->where('is_default', false)->first();
                if ($nextImage) {
                    $nextImage->update(['is_default' => true]);
                }
            }
            $image->delete();
            DB::commit();
            return response()->json(['success' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function setDefaultImage(Image $image)
    {
        try {
            DB::beginTransaction();
            $image->product->images()->update(['is_default' => false]);
            $image->update(['is_default' => true]);
            DB::commit();
            return response()->json(['success' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function createProduct(Request $request, $data)
    {
        // dd($data);
        $Edition = Attribute::where('name', 'الطبعة')->first();
        $data['quantity_type'] = ["type" => 'Dépend d\'un attribut', "attributes" => [$Edition->id]];
        $data['price_type'] = ["type" => "Simple"];
        $product = Product::create($data);

        $product->productAttributes()->create(['attribute_id' => $Edition->id]);

        $product->productAttributes()->first()->values()->create(['attribute_value_id' => $Edition->values()->first()->id]);

        // dd($product->productAttributes, $data, $Edition);

        if ($request->file('images')) {
            foreach ($request->file('images') as $key => $imagefile) {
                $path = $this->addAttachment($imagefile);
                $product->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
            }
        }
        if (isset($data['categories']) && !empty($data['categories'])) {
            foreach ($data['categories'] as $key => $cat) {
                if (!empty($cat)) {
                    $product->categories()->create(['category_id' => $cat]);
                }
            }
        }

        if (isset($data['attributes']) && !empty($data['attributes'])) {
            foreach ($data['attributes'] as $key => $attr) {
                $productAttribute = $product->productAttributes()->create(['attribute_id' => $attr['attribute_id']]);
                // foreach ($attr['attribute_values'] as $key => $item) {
                //     if (!empty($item) && !empty($attr['attribute_id'])) {
                //         $attribute_value_id=AttributeValue::where('attribute_id',$attr['attribute_id'])->where(function ($q) use($item) {
                //             $q->where('id',$item)->orWhere('value',$item);
                //         })->first();                     
                //         if(!$attribute_value_id){
                //             $attr_value = AttributeValue::create(['value' => $item, 'attribute_id' => $attr['attribute_id']]);
                //             $item=$attr_value->id;
                //         }
                //             $value = $productAttribute->values()->create(['attribute_value_id' => $item]);                        
                //     }
                // }
                if (!empty($attr['attribute_values']) && !empty($attr['attribute_id'])) {
                    $attribute_value_id = AttributeValue::where('attribute_id', $attr['attribute_id'])->where('value', $attr['attribute_values'])->first();
                    if (!$attribute_value_id) {
                        $attr_value = AttributeValue::create(['value' => $attr['attribute_values'], 'attribute_id' => $attr['attribute_id']]);
                        $id = $attr_value->id;
                    } else {
                        $id = $attribute_value_id->id;
                    }
                    $value = $productAttribute->values()->create(['attribute_value_id' => $id]);
                }
            }
        }
        if (isset($data['offers_values']) && !empty($data['offers_values'])) {
            $offer_id = Attribute::where('name', 'العروض')->select('id')->first();
            $productAttribute = $product->productAttributes()->create(['attribute_id' => $offer_id->id]);
            foreach ($data['offers_values'] as $key => $offer) {
                $value = $productAttribute->values()->create(['attribute_value_id' => $offer]);
                // dd($value);
            }
        }
        $stores = Store::all();

        foreach ($stores as $i => $store) {
            $productAttribute1 = $product->productAttributes->where('attribute_id', $Edition->id)->first();
            // dd($productAttribute1->values);
            foreach ($productAttribute1->values as $key => $value1) {
                $quantity = [
                    'product_attribute1' => $productAttribute1->id,
                    'product_value1' => $value1->id,
                    'store_id' => $store->id,
                ];
                $product->quantities()->create($quantity);
            }
        }
    }

    public function store(CreateProductRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->all();
            if (isset($data['activation'])) {
                $data['status'] = '1';
            } else {
                $data['status'] = '0';
            }
            if (isset($data['by_order'])) {
                $data['by_order'] = '1';
            } else {
                $data['by_order'] = '0';
            }
            if (isset($data['duplicated_product']) && $data['duplicated_product'] == 1) {
                $name = $data['name'];
                foreach ($data['duplications'] as $key => $duplication) {
                    $data['name'] = $name . ' - ' . $duplication['attribute_value'];
                    $data['buying'] = $duplication['buying'];
                    $data['selling1'] = $duplication['selling1'];
                    $data['selling2'] = $duplication['selling2'];
                    $data['selling3'] = $duplication['selling3'];
                    $data['minimum_quantity'] = $duplication['minimum_quantity'];
                    $data['reserve_quantity'] = $duplication['reserve_quantity'];
                    $data['sku'] = $duplication['sku'];
                    $data['reference'] = $duplication['reference'];

                    $this->createProduct($request, $data);
                }
            } else {
                $this->createProduct($request, $data);
            }

            DB::commit();
            return redirect()->route('admin.product.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function addAttachment($file)
    {
        $filePath = 'uploads/products/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($data, $name)
    {
        if (isset($data[$name])) {
            if ($data[$name] != '') {
                $fileuri = str_replace('/storage', '', $data[$name]);
                Storage::disk('uploads')->delete($fileuri);
            }
        }
    }

    public function getProductPriceEvolution($product, $from = null, $to = null)
    {
        $data = CartsItems::select(
            DB::raw('year(created_at) as year'),
            DB::raw('month(created_at) as month'),
            DB::raw('avg(product_price_selling) as selling'),
            DB::raw('avg(product_price_buying) as buying'),
            DB::raw('avg(product_price_selling) - avg(product_price_buying) as margin'),
        )
            ->where('product_id', $product->id)

            ->when($from, function ($query) use ($from, $to) {
                $query->whereBetween('created_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })
            // ->where(DB::raw('year(created_at)'), '=', $year)
            // ->groupBy('year')
            // ->groupBy('month')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))
            ->orderBy('year')
            ->orderBy('month')
            ->get();

        return $data;
    }

    public function productPriceEvolution(Product $product, $from, $to)
    {
        $data = $this->getProductPriceEvolution($product, $from, $to);
        return response()->json(['data' => $data]);
    }

    public function show(Request $request, Product $product)
    {
        $statistics = [
            'current_quantity' => $product->quantities()->where('store_id', auth()->user()->store_id)->sum('quantity'),
            'carts_count' => $product->carts->count(),
            'total_sold' => $product->cartItems->sum('product_quantity'),
            'total_sale' => $product->cartItems->sum(
                function ($item) {
                    return ($item->product_price_selling * (1 - $item->product_remise / 100)) * $item->product_quantity;
                }
            ),
            'total_buying' => $product->cartItems->sum(
                function ($item) {
                    return $item->product_price_buying * $item->product_quantity;
                }
            ),
            'total_margin' => $product->cartItems->sum(
                function ($item) {
                    return (($item->product_price_selling * (1 - $item->product_remise / 100)) * $item->product_quantity) - ($item->product_price_buying * $item->product_quantity);
                }
            ),
            'product_price_evolution' => $this->getProductPriceEvolution($product),
        ];
        $statistics = (object) $statistics;

        $result = Cart::select(DB::raw('YEAR(created_at) as year'))->distinct()->get();
        $years = $result->pluck('year');

        $attrs = Attribute::all()->load('values');
        $product->load('productAttributes.values.attributeValue');

        if ($request->ajax()) {
            ///////////////////////////
            $table = $request->has('table') ? $request->get('table') : null;
            if ($table == 'log') {
                $logs = ProductLog::query()->where(['store_id' =>  auth()->user()->store_id, 'product_id' =>  $product->id]);
                if (!empty($request->query('type')) && $request->query('type') != '') {
                    $type = $request->query('type');
                    $logs->where('logable_type', 'App\Models\\' . $type);
                }

                $color = $request->query('color') ?? null;
                $size = $request->query('size') ?? null;

                $logs->when($color, function ($query) use ($color) {
                    return $query->where('color_value', $color);
                })
                    ->when($size, function ($query) use ($size) {
                        return $query->where('size_value', $size);
                    });

                return DataTables::of($logs)
                    ->addColumn('type', function ($row) {
                        $html = '<strong>' . \App\Helpers\Helper::getModelName(strtolower(class_basename($row->logable_type))) . '</strong>';

                        if (class_basename($row->logable_type) == 'Mu') {
                            if ($row->logable->store_destination == auth()->user()->store_id) {
                                $html = '<strong> Mvt BR </strong>';
                            } else {
                                $html = '<strong> Mvt BS </strong>';
                            }
                        }
                        return $html;
                    })
                    ->editColumn('date', function ($row) use ($product) {
                        $html = $row->date ? $row->date : Carbon::parse($row->created_at)->format('Y-m-d');
                        return $html;
                    })
                    ->rawColumns(['type',])

                    ->make(true);
            } else {

                ///////////////////////////

                $models = ['Cart', 'ReturnNote', 'Invoice', 'CreditNote', 'SaleQuote', 'Order', 'Voucher', 'ReturnCoupon', 'Mu', 'Inventory', 'OnlineSale'];
                $collection = collect();

                foreach ($models as $key => $model) {
                    $nameSpace = '\\App\Models\\';
                    $model = $nameSpace . $model;

                    $data = $model::whereHas('items', function ($query) use ($product) {
                        $query->where('product_id', $product->id);
                    })->get();

                    if ($data) {
                        foreach ($data as $item)
                            $collection->push($item);
                    }
                }

                return DataTables::of($collection)
                    ->addColumn('type', function ($row) {
                        $html = '<strong>' . \App\Helpers\Helper::getModelName(strtolower(class_basename($row))) . '</strong>';
                        return $html;
                    })
                    ->editColumn('date', function ($row) use ($product) {
                        $html = $row->date ? $row->date : Carbon::parse($row->created_at)->format('Y-m-d');
                        return $html;
                    })
                    ->editColumn('quantity', function ($row) use ($product) {
                        $html = '';
                        if (in_array(class_basename($row), ['Inventory'])) {
                            $item = $row->items->where('product_id', $product->id)->first();
                            $html = '<strong>' . ($item->product_new_quantity - $item->product_old_quantity) . '</strong>';
                        } else if (in_array(class_basename($row), ['Mu'])) {
                            $qty = $row->items->where('product_id', $product->id)?->sum('product_quantity');
                            auth()->user()->store_id == $row->store_origin ? $html = '<strong> -' . $qty . '</strong>' : '<strong>' . $qty . '</strong>';
                        } else {
                            $html = '<strong>' . $row->items->where('product_id', $product->id)?->sum('product_quantity') . '</strong>';
                        }
                        return $html;
                    })
                    ->editColumn('product_price', function ($row) use ($product) {
                        $html = '';
                        if (in_array(class_basename($row), ['Inventory', 'Mu'])) {
                            $html = '-';
                        } else {
                            $price = in_array(class_basename($row), ['Cart', 'CreditNote', 'ReturnNote', 'Invoice', 'SaleQuote', 'Order',  'Mu', 'OnlineSale']) ? 'product_price_selling' : 'product_price_buying';
                            // $items = $row->items->where('product_id', $product->id);
                            // foreach ($items as $key => $value) {
                            //     $html .= '<div> ' . $value->product_quantity . ' x '  . $value->$price . '</div>';
                            // }
                            $html .= '<div> ' . $row->items->where('product_id', $product->id)->first()->$price . '</div>';
                        }
                        return $html;
                    })
                    ->editColumn('product_remise', function ($row) use ($product) {
                        $html = '';
                        if (in_array(class_basename($row), ['Inventory', 'Mu'])) {
                            $html = '-';
                        } else {
                            // $items = $row->items->where('product_id', $product->id);
                            // foreach ($items as $key => $value) {
                            //     $html .= '<div> ' . $value->product_remise . '</div>';
                            // }
                            $html .= '<div> ' . $row->items->where('product_id', $product->id)->first()->product_remise . '</div>';
                        }
                        return $html;
                    })
                    ->addColumn('total', function ($row) use ($product) {
                        $total = 0;
                        if (in_array(class_basename($row), ['Inventory', 'Mu'])) {
                            $total = '-';
                        } else {
                            $price = in_array(class_basename($row), ['Cart', 'CreditNote', 'ReturnNote', 'Invoice', 'SaleQuote', 'Order',  'Mu', 'OnlineSale']) ? 'product_price_selling' : 'product_price_buying';
                            $items = $row->items->where('product_id', $product->id);
                            foreach ($items as $key => $value) {
                                $total += $value->product_quantity * $value->$price * (1 - $value->product_remise / 100);
                            }
                        }
                        return $total;
                    })

                    ->rawColumns(['type', 'quantity', 'product_price', 'product_remise', 'total'])

                    ->make(true);
            }
        }

        return view('admin.product.show', compact(['product', 'attrs', 'statistics', 'years',]));
    }

    public function edit(Product $product)
    {
        $settings = $this->getSettings();
        $offers = Attribute::where('name', 'العروض')->get()->pluck('values')->collapse('values');
        // $attrs = array();
        // $attrs = Attribute::whereHas('products', function ($query) use ($product) {
        //     $query->where('product_id', $product->id);
        // })->with('values', function ($query) use ($product) {
        //     $query->whereHas('productValues', function ($query) use ($product) {
        //         $query->whereHas('productAttribute', function ($query) use ($product) {
        //             $query->where('product_id', $product->id);
        //         });
        //     });
        // })->get();
        $attrs_offers = Attribute::where('name', 'العروض')->whereHas('products', function ($query) use ($product) {
            $query->where('product_id', $product->id);
        })->with('values', function ($query) use ($product) {
            $query->whereHas('productValues', function ($query) use ($product) {
                $query->whereHas('productAttribute', function ($query) use ($product) {
                    $query->where('product_id', $product->id);
                });
            });
        })->get();
        $attrs = Attribute::whereNot('name', 'الطبعة')->select('id', 'name')->get();

        $product->load('productAttributes.values.attributeValue');
        // dd( $attrs);
        return view('admin.product.edit', compact(['product', 'attrs', 'settings', 'offers', 'attrs_offers']));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if (isset($data['activation'])) {
                $data['status'] = '1';
            } else {
                $data['status'] = '0';
            }
            if (isset($data['by_order'])) {
                $data['by_order'] = '1';
            } else {
                $data['by_order'] = '0';
            }
            $product->update($data);

            if ($request->file('images')) {
                foreach ($request->file('images') as $key => $imagefile) {
                    $path = $this->addAttachment($imagefile);
                    $defaultImage = $product->images->where('is_default', true)->first();
                    if ($defaultImage) {
                        $product->images()->create(['url' => $path]);
                    } else {
                        $product->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
                    }
                }
            }

            $product?->categories()?->delete();
            if (isset($data['categories']) && !empty($data['categories'])) {
                foreach ($data['categories'] as $key => $cat) {
                    if (!empty($cat)) {
                        $product->categories()->create(['category_id' => $cat]);
                    }
                }
            }
            if (isset($data['attributes']) && !empty($data['attributes'])) {
                $product->productAttributes()->whereHas('attribute', function ($query) {
                    $query->whereNot('name', 'الطبعة');
                })->delete();
                foreach ($data['attributes'] as $key => $attribute) {
                    $productAttribute = $product->productAttributes()->create(['attribute_id' => $attribute['attribute_id']]);
                    if (!empty($attribute['attribute_values']) && !empty($attribute['attribute_id'])) {
                        $attribute_value_id = AttributeValue::where('attribute_id', $attribute['attribute_id'])->where('value', $attribute['attribute_values'])->first();
                        if (!$attribute_value_id) {
                            $attr_value = AttributeValue::create(['value' => $attribute['attribute_values'], 'attribute_id' => $attribute['attribute_id']]);
                            $id = $attr_value->id;
                        } else {
                            $id = $attribute_value_id->id;
                        }
                        $value = $productAttribute->values()->create(['attribute_value_id' => $id]);
                    }
                }
            }
            if (isset($data['offers_values']) && !empty($data['offers_values'])) {
                $offer_id = Attribute::where('name', 'العروض')->select('id')->first();
                $productAttribute = $product->productAttributes()->create(['attribute_id' => $offer_id->id]);
                foreach ($data['offers_values'] as $key => $offer) {
                    $value = $productAttribute->values()->create(['attribute_value_id' => $offer]);
                }
            }

            DB::commit();

            return redirect()->route('admin.product.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    protected function updateOrCreateProductAttributes($product, $relation, array $items)
    {
        $relation->getResults()->each(function ($model) use ($items, $product) {
            foreach ($items as $item) {
                $values = $item['attribute_values'];
                if ($model->attribute_id == ($item['attribute_id'] ?? null)) {
                    $model->fill(['attribute_id' => $item['attribute_id'], 'product_id' => $product->id])->save();

                    $model->values()->getResults()->each(function ($value) use ($values, $model) {
                        foreach ($values as $val) {
                            if ($value->attribute_value_id == ($val ?? null)) {
                                $value->fill(['attribute_value_id' => $val, 'product_attribute_id' => $model->id])->save();
                                return;
                            }
                        }

                        return $value->delete();
                    });

                    foreach ($values as $item) {
                        $find = ProductAttributeValue::where(['product_attribute_id' => $model->id, 'attribute_value_id' => $item])->first();
                        if (!$find) {
                            $model->values()->create(['attribute_value_id' => $item]);
                        }
                    };

                    return;
                }
            }

            return $model->delete();
        });

        foreach ($items as $item) {
            $find = ProductAttribute::where(['attribute_id' => $item['attribute_id'], 'product_id' => $product->id])->first();
            if (!$find) {
                $relation->create(['attribute_id' => $item['attribute_id'], 'product_id' => $product->id]);
            }
        };
    }

    public function delete(Product $product)
    {
        try {
            if ($product->images()->exists()) {
                foreach ($product->images as $key => $image) {
                    $fileuri = str_replace('/storage', '', $image->url);
                    Storage::disk('public')->delete($fileuri);
                    $image->delete();
                }
            }
            $product->delete();
            return redirect()->route('admin.product.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function settings(Product $product)
    {
        $attrs = $product->productAttributes()->with('attribute')->get()->pluck('attribute.name', 'attribute_id');
        $product->load('productAttributes.values.attributeValue');
        return view('admin.product.settings', compact(['product', 'attrs']));
    }

    public function generateDefaultPricesAndQuantities(Product $product, $type, $relation)
    {
        $stores = Store::all();
        switch ($product->$type['type']) {
            case 'Sipmle':
                foreach ($stores as $i => $store) {
                    $product->$relation()->create(['store_id' => $store->id]);
                }
                break;
            case 'Dépend d\'un attribut':
                $ids = isset($product->$type['attributes']) ? $product->$type['attributes'] : [];
                $attrs = $product->productAttributes()->whereIn('attribute_id', $ids)->get();

                foreach ($stores as $i => $store) {
                    foreach ($attrs->first()->values as $key => $value1) {
                        $quantity = [
                            'product_attribute1' => $attrs[0]->id,
                            'product_value1' => $value1->id,
                            'store_id' => $store->id,
                        ];
                        $product->$relation()->create($quantity);
                    }
                }
                break;
            case 'Dépend de deux attributs':
                // dd($product->$type);
                $ids = isset($product->$type['attributes']) ? $product->$type['attributes'] : [];
                $attrs = $product->productAttributes()->whereIn('attribute_id', $ids)->get();
                // dd($ids);
                foreach ($stores as $i => $store) {
                    foreach ($attrs->first()->values as $key => $value1) {
                        foreach ($attrs->last()->values as $key => $value2) {
                            $quantity = [
                                'product_attribute1' => $attrs[0]->id,
                                'product_attribute2' => $attrs[1]->id,
                                'product_value1' => $value1->id,
                                'product_value2' => $value2->id,
                                'store_id' => $store->id,
                            ];
                            $product->$relation()->create($quantity);
                        }
                    }
                }
                break;

            default:
                break;
        }
    }


    public function updateSettings(UpdateProductSettingsRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();

            // $is_new_quantity_type = $product->quantity_type && $data['quantity_type']['type'] != $product->quantity_type['type'];
            // $is_new_price_type = $product->price_type && $data['price_type']['type'] != $product->price_type['type'];

            $product->update($data);

            if (!$product->prices()->exists() /*|| $is_new_price_type*/) {
                // if($is_new_price_type){
                //     $product->prices()->delete();
                // }
                $this->generateDefaultPricesAndQuantities($product, 'price_type', 'prices');
            }
            if (!$product->quantities()->exists() /*|| $is_new_quantity_type*/) {
                // if($is_new_quantity_type){
                //     $product->quantities()->delete();
                // }
                $this->generateDefaultPricesAndQuantities($product, 'quantity_type', 'quantities');
            }

            DB::commit();
            return redirect()->route('admin.product.index')->with('success', "Enregistré avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function quantities(Product $product)
    {
        return redirect()->back();
        $ids = isset($product->quantity_type['attributes']) ? $product->quantity_type['attributes'] : [];
        $attrs = $product->productAttributes()->whereIn('attribute_id', $ids)->with('attribute', 'values.attributeValue')->get()->sortByDesc(function ($productAttributes) {
            return $productAttributes->values->count();
        });
        $attrs = $attrs->values()->toArray();
        $Editions = Attribute::where('name', 'الطبعة')->first()->values;
        return view('admin.product.quantities', compact(['Editions', 'product', 'attrs']));
    }

    public function updateQuantities(UpdateProductQuantitiesRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $store_id = auth()->user()->store_id;
            $Edition = Attribute::where('name', 'الطبعة')->first();
            // foreach ($product->quantities as $key => $qt) {
            //     $qt->productAttribute1->values()->delete();
            // }
            $product->quantities()->where('store_id', $store_id)->delete();
            foreach ($data['quantities'] as $key => $item) {
                // $attributeValue = $Edition->values()->create($item);
                $productAttribute = $product->productAttributes()->where(['attribute_id' => $Edition->id])->first();
                $productAttributeValue = $productAttribute->values()->create(['attribute_value_id' => $item['edition']]);
                $item['store_id'] = $store_id;
                $item['product_attribute1'] = $productAttribute->id;
                $item['product_value1'] = $productAttributeValue->id;
                $product->quantities()->create($item);
            }
            DB::commit();
            return redirect()->route('admin.product.index')->with('success', "Enregistré avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function barcodes(Product $product)
    {
        $ids = isset($product->quantity_type['attributes']) ? $product->quantity_type['attributes'] : [];
        $attrs = $product->productAttributes()->whereIn('attribute_id', $ids)->with('attribute', 'values.attributeValue')->get()->sortByDesc(function ($productAttributes) {
            return $productAttributes->values->count();
        });
        $attrs = $attrs->values()->toArray();

        return view('admin.product.barcodes', compact(['product', 'attrs']));
    }

    public function updateBarcodes(UpdateProductBarcodesRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $product->barcodes()->delete();
            foreach ($data['barcodes'] as $key => $item) {
                // $item['barcode'] = $this->generateBarcode($product, $item);
                $product->barcodes()->create($item);
            }
            DB::commit();
            return redirect()->route('admin.product.index')->with('success', "Enregistré avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function generateBarcode(Product $product, $item)
    {
        $barcode = '';
        switch ($product->quantity_type['type']) {
            case 'Sipmle':
                $barcode = $product->sku;
                break;
            case 'Dépend d\'un attribut':
                $productAttribute1 = $product->productAttributes()->where(['id' => $item['product_attribute1']])->first();
                $productValue1 = $productAttribute1->values()->where(['id' => $item['product_value1']])->first();
                $barcode = substr($product->name, 0, 2) . substr($productAttribute1->attribute->name, 0, 2) . substr($productValue1->attributeValue->value, 0, 2) . $productValue1->attributeValue->id;
                break;
            case 'Dépend de deux attributs':
                $productAttribute1 = $product->productAttributes()->where(['id' => $item['product_attribute1']])->first();
                $productAttribute2 = $product->productAttributes()->where(['id' => $item['product_attribute2']])->first();
                $productValue1 = $productAttribute1->values()->where(['id' => $item['product_value1']])->first();
                $productValue2 = $productAttribute2->values()->where(['id' => $item['product_value2']])->first();
                $barcode = substr($product->name, 0, 2) . substr($productAttribute1->attribute->name, 0, 2) . substr($productValue1->attributeValue->value, 0, 2) . $productValue1->attributeValue->id . substr($productAttribute2->attribute->name, 0, 2) . substr($productValue2->attributeValue->value, 0, 2) . $productValue2->attributeValue->id;
                break;

            default:
                $barcode = $product->sku;
                break;
        }
        return $barcode;
    }

    public function prices(Product $product)
    {
        $ids = isset($product->price_type['attributes']) ? $product->price_type['attributes'] : [];
        $attributes = $product->productAttributes()->whereIn('attribute_id', $ids)->with('attribute', 'values.attributeValue')->get()->sortByDesc(function ($productAttributes) {
            return $productAttributes->values->count();
        });
        $attributes = $attributes->values()->toArray();

        return view('admin.product.prices', compact(['product', 'attributes']));
    }

    public function updatePrices(UpdateProductPricesRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $store = auth()->user()->store;
            $product->prices()->delete();
            foreach ($data['prices'] as $key => $item) {
                $item['store_id'] = $store->id;
                $product->prices()->create($item);
            }
            DB::commit();
            return redirect()->route('admin.product.index')->with('success', "Enregistré avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    /*** importation */
    public function importation()
    {
        return view('admin.product.importation');
    }

    public function importationxl(Request $request)
    {

        $data = $request->all();
        $import = new ProductsImport;
        Excel::import($import, $data['File']);
        return redirect()->route('admin.product.index')->with('warning', "L'importation est encours...!");
    }
    /*** */
    /********* merging */
    public function merge(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $products = Product::whereIn('id', $data['product_id'])->get();
        // dd($products);
        return view('admin.product.merge', compact('products'));
    }
    public function mergeing(Request $request)
    {
        DB::beginTransaction();
        $data = $request->all();
        $product = Product::where('id', $data['selected'])->first();
        $documents = ['cart', 'order', 'onlinesale', 'wishlist'];
        $nameSpace = '\\App\Models\\';
        foreach ($documents as $key => $document) {
            if ($document == 'cart') {
                $model = $nameSpace . $document . 'sItems';
            } elseif ($document == 'wishlist') {
                $model = $nameSpace . ucfirst($document);
            } else {
                $model = $nameSpace . ucfirst($document) . 'Item';
            }
            $model::whereIn('product_id', json_decode($data['products']))->whereNot('product_id', $data['selected'])->update(['product_id' => $product->id]);
        }
        $stores = Store::all();
        foreach ($stores as $i => $store) {
            $quantity = Quantity::where('store_id', $store->id)->whereIn('product_id', json_decode($data['products']))->sum('quantity');
            Quantity::where('store_id', $store->id)->whereIn('product_id', json_decode($data['products']))->whereNot('product_id', $data['selected'])->delete();
            $product->quantities()->where('store_id', $store->id)->update(['quantity' => $quantity]);
        }
        $products = Product::whereIn('id', json_decode($data['products']))->whereNot('id', $data['selected'])->delete();
        DB::commit();

        return redirect()->route('admin.product.index')->with('success', "Ajouté avec succès!");
    }
    /******* */
    public function searchproduct()
    {
        return view('search');
    }
    public function showproduct(Request $request)
    {
        $data = $request->all();
        $product = Product::where('sku', $data['product'])
            ->orWhere('reference', $data['product'])
            ->orWhere('name', 'LIKE', '%' . $data['product'] . '%')->first();
        if ($product) {


            $auteur = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('attributes.name', '=', 'اسم المؤلف')
                ->where('products.id', '=', $product->id)
                ->select('attribute_values.value')->first();

            $maison = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'دار النشر')
                ->select('attribute_values.value')->first();

            $pages = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'عدد الصفحات')
                ->select('attribute_values.value')->first();

            $tomes = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'عدد المجلدات')
                ->select('attribute_values.value')->first();

            $papier = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'نوعية الورق')
                ->select('attribute_values.value')->first();

            $enqueteur = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'المحقق')
                ->select('attribute_values.value')->first();

            $poid = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'الوزن')
                ->select('attribute_values.value')->first();

            $taille = DB::table('product_attribute_values')
                ->join('product_attributes', 'product_attribute_values.product_attribute_id', '=', 'product_attributes.id')
                ->join('attribute_values', 'product_attribute_values.attribute_value_id', '=', 'attribute_values.id')
                ->join('attributes', 'attribute_values.attribute_id', '=', 'attributes.id')
                ->join('products', 'product_attributes.product_id', '=', 'products.id')
                ->where('products.id', '=', $product->id)
                ->where('attributes.name', '=', 'الحجم')
                ->select('attribute_values.value')->first();


            return view('show', compact('product', 'auteur', 'maison', 'pages', 'tomes', 'papier', 'enqueteur', 'poid', 'taille'));
        }
        return redirect()->back()->with('error', "Aucun article avec ces données!");
    }
    public function getAttributeValue(Request $request)
    {
        $data = $request->all();
        // dd($data);       
        $attr = Attribute::where('id', $data['attribute_id'])->with('values', function ($query) use ($data) {
            $query->whereHas('productValues', function ($query) use ($data) {
                $query->whereHas('productAttribute', function ($query) use ($data) {
                    $query->where('value', 'like', '%' . $data['query'] . '%');
                });
            })->limit(10);
        })->first();
        return ($attr);
    }

    public function multipleDelete(Request $request)
    {
        try {
            $data = $request->all();
            if (isset($data['product_id'])) {
                $products = Product::whereIn('id', $data['product_id'])->get();
                foreach ($products as $key => $product) {
                    if ($product->images()->exists()) {
                        foreach ($product->images as $k => $image) {
                            $fileuri = str_replace('/storage', '', $image->url);
                            Storage::disk('public')->delete($fileuri);
                            $image->delete();
                        }
                    }
                    $product->delete();
                }
                return redirect()->route('admin.product.index')->with('success', "Supprimé avec succès!");
            } else {
                return redirect()->route('admin.product.index')->with('error', "Veuillez séléctionner au moins un article!");
            }
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return redirect()->route('admin.product.index')->with('error', "Échec de suppression!");
        }
    }
}
