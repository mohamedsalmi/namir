<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Http\Requests\CreateAttributeRequest;
use App\Http\Requests\UpdateAttributeRequest;
use App\Models\Attribute;
use App\Models\AttributeValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class AttributeController extends Controller
{
    /**
     * Show the list of available attributes.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Attribute::query();
            $data->whereNot('name','الطبعة')->whereNot('name', 'العروض');
            return DataTables::eloquent($data)
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                    return $formattedDate;
                })
                ->editColumn('type', function ($row) {
                    return Helper::getAttributeType($row->type);
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier attribut') ? '<a href="' . route('admin.attribute.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer attribut') ? '<form method="POST" action="' . route('admin.attribute.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.attribute.list');
    }

    public function getAll()
    {
        try {
            $data = Attribute::whereNot('name', 'الطبعة')->get();
            return response()->json(['data' => $data, 'success' => true]);
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function create()
    {
        return view('admin.attribute.create');
    }

    public function store(CreateAttributeRequest $request)
    {
        try {
            $data = $request->all();
            $attributeValues = explode(',', $data['attribute_values']);
            $attribute = Attribute::create($data);
            if ($data['type'] != 'color' && !empty($data['attribute_values']) && !empty($attributeValues) && count($attributeValues) > 0 ) {
                foreach ($attributeValues as $key => $item) {
                    $attribute->values()->create(['value' => $item]);
                }
            }
            if ($data['type'] == 'color' && isset($data['colors']) && !empty($data['colors']) && count($data['colors']) > 0) {
                foreach ($data['colors'] as $key => $item) {
                    $attribute->values()->create(['value' => $item['color_name'], 'color' => $item['color_code']]);
                }
            }
            return redirect()->route('admin.attribute.list')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function new(CreateAttributeRequest $request)
    {
        try {
            $data = $request->all();
            $attributeValues = explode(',', $data['attribute_values']);
            $attribute = Attribute::create($data);
            foreach ($attributeValues as $key => $item) {
                $attribute->values()->create(['value' => $item]);
            }
            return response()->json(['success' => true, 'message' => 'Ajouté avec succès!', view('admin.attribute.create')->renderSections()['content']], 201);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Request $request,Attribute $attribute)
    {    
    //     if($name){
                 
    //     $values=DB::table('attribute_values')
    //    ->leftJoin('attributes','attributes.id','=','attribute_values.attribute_id')
    //    ->where('attribute_values.attribute_id',$attribute->id)
    //    ->where('attribute_values.value', 'LIKE', '%' . $name . '%')
    //    ->select('attribute_values.value','attribute_values.id')->paginate(50);
    //    return view('admin.attribute.edit', compact('attribute','values'));
    //            }
    if ($request->ajax()) {
        $data = DB::table('attribute_values')
        ->leftJoin('attributes','attributes.id','=','attribute_values.attribute_id')
        ->where('attribute_values.attribute_id',$attribute->id)
        ->select('attribute_values.value','attribute_values.id');
        return DataTables::of($data)
        // ->editColumn('value', function ($row) {
        //     $html ='<td data-id="'.$row->id.'"  class="value">'.$row->value.'</td>' ;
        //     return $html;
        // })
        ->addColumn('action', function ($row) {
            $confirmDeletion = auth()->user()->can('Supprimer attribut') ? '<form method="POST" action="' . route('admin.attribute.deleteValue', $row->id) . '">'
                . csrf_field() .
                '<input name="_method" type="hidden" value="GET">
                <a id="show_confirm" type="submit"
                class=" text-danger show_confirm" data-toggle="tooltip"
                title="Supprimer"><i class="bi bi-trash-fill"></i></a>            </form>' : '';

            $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                 $confirmDeletion
                . '</div>';

            return $btns;
        })
        ->rawColumns(['action'])

        ->make(true);
         
       }
        return view('admin.attribute.edit', compact('attribute'));
    }

    public function update(Request $request, Attribute $attribute)
    {
        try {
            DB::beginTransaction();

            $data = $request->all();
            // $attributeValues = explode(',', $data['attribute_values']);

            // if ($data['type'] != 'color' && !empty($data['attribute_values']) && !empty($attributeValues) && count($attributeValues) > 0 ) {
            //     $this->proccessRelationWithRequest(
            //         $attribute,
            //         $attribute->values(),
            //         $attributeValues
            //     );
            // }
            // if ($data['type'] == 'color' && isset($data['colors']) && !empty($data['colors']) && count($data['colors']) > 0) {
            //     $this->proccessRelationWithRequest2(
            //         $attribute,
            //         $attribute->values(),
            //         $data['colors']
            //     );
            // }
            $attribute->update($data);

            DB::commit();
            return redirect()->route('admin.attribute.list')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Attribute $attribute)
    {
        try {
            $attribute->delete();
            return redirect()->route('admin.attribute.list')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function deleteValue(AttributeValue $value)
    {
        try {
            $value->delete();
            return redirect()->back()->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function valuevalidation(Request $request)
    {
        $validated = $request->validate([
            'value' => 'distinct|required|unique:attribute_values,value',
        ]);

        return response()->json($validated);
    }
    public function AddValue(Request $request ,Attribute $attribute)
    {
        try {
            $data = $request->all();
            $attribute->values()->create(['value' => $data['value']]);
            return redirect()->back()->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function EditValue(Request $request ,AttributeValue $value)
    {
        $validated = $request->validate([
            
            'value' => 'distinct|required|unique:attribute_values,value,'.$value->id,
        ]);
        if(!$validated){return response()->json($validated);}
        try {
            $data = $request->all();
            $value->update($data);
            return response()->json(['sucess'=> true]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json([],500);

        }
    }

    protected function proccessRelationWithRequest($attribute, $relation, array $items)
    {
        $relation->getResults()->each(function ($model) use ($items, $attribute) {
            foreach ($items as $item) {
                if ($model->value === ($item ?? null)) {
                    $model->fill(['value' => $item, 'attribute_id' => $attribute->id])->save();
                    return;
                }
            }

            return $model->delete();
        });

        foreach ($items as $item) {
            $find = AttributeValue::where(['value' => $item, 'attribute_id' => $attribute->id])->first();
            if (!$find) {
                $relation->create(['value' => $item, 'attribute_id' => $attribute->id]);
            }
        };
    }

    protected function proccessRelationWithRequest2($attribute, $relation, array $items)
    {
        $relation->getResults()->each(function ($model) use ($items, $attribute) {
            foreach ($items as $item) {
                if ($model->value == $item['color_name']) {
                    $model->fill(['value' => $item['color_name'], 'color' => $item['color_code'], 'attribute_id' => $attribute->id])->save();
                    return;
                }
            }

            return $model->delete();
        });

        foreach ($items as $item) {
            $find = AttributeValue::where(['value' => $item['color_name'], 'attribute_id' => $attribute->id])->first();
            if (!$find) {
                $relation->create(['value' => $item['color_name'], 'color' => $item['color_code'], 'attribute_id' => $attribute->id]);
            }
        };
    }
}
