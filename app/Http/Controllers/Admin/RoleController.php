<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * Show the list of available roles.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Role::query();
            return DataTables::eloquent($data)
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                    return $formattedDate;
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier rôle') ? '<a href="' . route('admin.role.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer rôle') ? '<form method="POST" action="' . route('admin.role.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.role.index');
    }

    public function create()
    {
        $permissions = Permission::all()->groupBy('group');
        return view('admin.role.create', compact('permissions'));
    }

    public function store(CreateRoleRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['guard_name'] = 'web';
            $role = Role::create($data);
            if (isset($data['permissions'])) {
                foreach ($data['permissions'] as $key => $permission) {
                    $role->givePermissionTo($permission);
                }
            }
            DB::commit();
            return redirect()->route('admin.role.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Role $role)
    {
        $permissions = Permission::all()->groupBy('group');
        return view('admin.role.edit', compact('role', 'permissions'));
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $role->update($data);
            $permissions = isset($data['permissions']) ? $data['permissions'] : [];
            $role->syncPermissions($permissions);
            DB::commit();
            return redirect()->route('admin.role.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Role $role)
    {
        try {
            $role->delete();
            return redirect()->route('admin.role.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
