<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(CreateUserRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['password'] = bcrypt('password');
            $user = User::create($data);
            $role = Role::find($data['role']);
            $user->assignRole($role);
            if ($request->file('avatar')) {
                $path = $this->addAttachment($request->file('avatar'));
                $user->image()->create(['url' => $path]);
            }
            DB::commit();
            return redirect()->route('admin.user.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ($e->getMessage());
        }
    }

    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        try {
            $data = $request->all();
            if ($request->file('avatar')) {
                if ($user->image()->exists()) {
                    $this->deleteAttachment($user->image->url);
                    $user->image()->delete();
                }
                $path = $this->addAttachment($request->file('avatar'));
                $user->image()->create(['url' => $path]);
            }

            $role = Role::find($data['role']);
            $user->syncRoles([$role]);

            $user->update($data);
            return redirect()->route('admin.user.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(User $User)
    {
        try {
            $User->delete();
            return redirect()->route('admin.user.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function addAttachment($file)
    {
        $filePath = 'uploads/users/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($path)
    {
        if (isset($path)) {
            if ($path != '') {
                $fileuri = str_replace('/storage', '', $path);
                Storage::disk('public')->delete($fileuri);
            }
        }
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        try {
            $user = User::find(auth()->user()->id);
            $data = $request->all();
            if ($request->file('avatar')) {
                if ($user->image()->exists()) {
                    $this->deleteAttachment($user->image->url);
                    $user->image()->delete();
                }
                $path = $this->addAttachment($request->file('avatar'));
                $user->image()->create(['url' => $path]);
            }
            if (isset($data['password']) && !empty($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            }else{
                unset($data['password']);
            }
            $user->update($data);
            return redirect()->back()->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
