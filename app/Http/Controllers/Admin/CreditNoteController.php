<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\CreditNote;
use App\Models\CreditnoteCommitment;
use App\Models\Invoice;
use App\Models\Quantity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class CreditNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carts = CreditNote::query();
        if ($request->ajax()) {
            $carts->where('store_id', auth()->user()->store_id);

            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('client') && $request->query('client') != '') {
                $carts->where('client_id', $request->query('client'));
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->editColumn('status', function ($row) {                 
                            $status = '<span class="badge rounded-pill alert-success">'. $row->status .'</span>';
                    return $status;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total . 'DM';
                    return $total;
                })
                ->addColumn('check', function ($row) {
                    $check = ' <div class="form-check">';
                    if ($row->invoice_id == null) {
                        $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input" type="checkbox"
                        form="create-invoice-form">';
                    }
                    $check .= '</div>';
                    return $check;
                })
                ->addColumn('client', function ($row) {
                    $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->client) {
                        $client .= ' <img src="' . asset($row->client->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client->code . '-' . $row->client_details['name'] . '</p>';
                    } else {
                        $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client_details['name'] . '</p>';
                    }
                    $client .= '</div>';
                    return $client;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails commande') ? '<a href="' . route('admin.creditnote.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Voir les détails" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                    // $updateBtn = auth()->user()->can('Modifier commande') ? '<a href="' . route('admin.creditnote.edit', $row->id) . '"
                    // class="text-warning" data-bs-toggle="tooltip"
                    // data-bs-placement="bottom" title=""
                    // data-bs-original-title="Modifier" aria-label="Edit"><i
                    //     class="bi bi-pencil-fill"></i></a>' : '';
                    $printBtn = auth()->user()->can('Imprimer commande') ? '<a href="' . route('admin.creditnote.print', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    target="blank" data-bs-original-title="Imprimer commande"
                    aria-label="Views"><i
                        class="bi bi-printer-fill text-black"></i></a>' : '';
                
                    $confirmDeletion = auth()->user()->can('Supprimer commande') ? '' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                    $btns .=  $confirmDeletion;
                    $btns .=  $printBtn . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'check', 'client', 'status'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.creditnote.index', compact('sum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Invoice $invoice)
    {
        return view('admin.creditnote.create', compact('invoice'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request)
    {
        $validated = $request->validate([
            'client_id' => 'required',
            'invoice_id' => 'required',
            'client_details.name' => 'required',
            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',


        ]);
        // dd($validated);

        return response()->json($validated);
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            // get last record
            $codification_setting = 'Codification facture avoir';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            
            $data['codification'] = $nextCodification;

            $data['number'] = $number;
                $data['store_id'] = auth()->user()->store->id;
                $data['user_id'] = auth()->user()->id;
                $data['created_by'] = auth()->user()->name;
                if (isset($data['timbre'])) {
                    $data['timbre'] = '1';
                } else {
                    $data['timbre'] = '0';
                }
                $creditnote = CreditNote::create($data);
                foreach ($data['items'] as $key => $item) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id' => $data['items'][$key]['product_id']
                        ]
                    );
                    $update = Helper::updateQuantity($creditnote, 'Création ', $data['items'][$key]['product_quantity'], $quantity, 'add');  
                }
                $creditnote->items()->createMany($data['items']);
            
                DB::commit();
                return redirect()->route('admin.creditnote.index')->with('success', "Ajouté avec succès!");

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CreditNote $creditnote)
    {
        return view('admin.creditnote.show', compact('creditnote'));

    }
    public function pdf(CreditNote $creditnote)
    {

        Helper::print($creditnote, 'creditnote');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
