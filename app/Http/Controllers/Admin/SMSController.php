<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateSmsRequest;
use App\Jobs\SendSMS;
use App\Models\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class SMSController extends Controller
{
    public function create()
    {
        $clients = Client::whereNotNull('phone')->orderBy('client_code', 'asc')->get();
        return view('admin.sms.create', compact('clients'));
    }

    public function store(CreateSmsRequest $request)
    {
        try {
            $data = $request->all();
            $results = [];
            foreach ($data['clients'] as $key => $item) {
                $client = Client::find($item);
                $sendSMS = new SendSMS($client->phone, $data['message']);
                $sms = $sendSMS->handle();
                $sms['client_name'] = $client->name;
                $sms['client_phone'] = $client->phone;
                array_push($results, $sms);
            }

            return redirect()->back()->with('result', $results);
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ($e->getMessage());
        }
    }
}
