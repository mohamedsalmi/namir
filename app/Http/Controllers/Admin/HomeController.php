<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Client;
use App\Models\FinancialCommitment;
use App\Models\PaymentCart;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Store;
use App\Models\User;
use App\Notifications\MinQuantityNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function initProducts()
    {
        try {
            DB::beginTransaction();
            $Lot = Attribute::where('name', 'الطبعة')->first();
    
            $products = Product::all();
            foreach ($products as $key => $product) {
                $product->productAttributes()->create(['attribute_id' => $Lot->id]);
                $product->productAttributes()->first()->values()->create(['attribute_value_id' => $Lot->values()->first()->id]);
    
                $stores = Store::all();
    
                foreach ($stores as $i => $store) {
                    $productAttribute1 = $product->productAttributes->where('attribute_id', $Lot->id)->first();
                    foreach ($productAttribute1->values as $key => $value1) {
                        $quantity = [
                            'product_attribute1' => $productAttribute1->id,
                            'product_value1' => $value1->id,
                            'store_id' => $store->id,
                        ];
                        $product->quantities()->create($quantity);
                    }
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $this->initProducts();
        $statistics = [
            'clients_count' => Client::count(),
            'orders_count' => Cart::count(),
            'users_count' => User::count(),
            'products_count' => Product::count(),
            'stores_count' => Store::count(),
            'providers_count' => Provider::count(),
            'late_checks_count' => $this->getLateChecksCount(),
            'commitments_date_reached_count' => $this->getCommitmentsDateReachedCount(),
            'carts_total_amounts_by_year_and_month' => $this->getCartsTotalAmountsByYearAndMonth(date('Y')),
        ];
        $statistics = (object) $statistics;

        $result = Cart::select(DB::raw('YEAR(created_at) as year'))->distinct()->get();
        $years = $result->pluck('year');

        return view('admin.home', compact('statistics', 'years'));
    }

    public function cartTotalAmountByyearAndMonth(string $year)
    {
        $data = $this->getCartsTotalAmountsByYearAndMonth($year);
        return response()->json(['data' => $data]);
    }

    public function getLateChecksCount()
    {
        return  PaymentCart::where('type', 'check')->whereDate('due_at', '<', Carbon::now())->count();
    }

    public function getCommitmentsDateReachedCount()
    {
        return  FinancialCommitment::whereDate('date', '<=', Carbon::now())->where('paiment_status', 0)->count();
    }

    public function getCartsTotalAmountsByYearAndMonth($year)
    {
        $data = Cart::select(
            DB::raw('year(created_at) as year'),
            DB::raw('month(created_at) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('year(created_at)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();

        $array = [];

        for ($month = 1; $month <= 12; $month++) {
            $array[] = [
                'year' => $year,
                'month' => $month,
                'total' => optional($data->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
            ];
        }

        return $array;
    }
}
