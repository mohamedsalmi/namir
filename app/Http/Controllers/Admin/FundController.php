<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FinancialCommitment;
use App\Models\PaymentCart;
use App\Models\User;
use App\Models\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class FundController extends Controller
{
    public function deleteWithdraw(Withdraw $withdraw)
    {
        try {
            $withdraw->delete();
            return redirect()->route('admin.fund.fund', ['from' => request()->query('from') ?? date('Y-m-d'), 'to' => request()->query('to') ?? date('Y-m-d')])->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
    public function EditWithdraw(Request $request ,withdraw $withdraw)
    {
        $validated = $request->validate([            
            'amount' => 'required',
        ]);
        if(!$validated){return response()->json($validated);}
        try {
            $data = $request->all();
            $withdraw->update($data);
            return response()->json(['sucess'=> true]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json([],500);

        }
    }
    public function storechange($store)
    {
        try {
            $user = User::where('id', auth()->user()->id);
            $user->update(['store_id' => $store]);
            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function fund(Request $request)
    {
        $from = $request->has('from') ? $request->query('from') : null;
        $to = $request->has('to') ? $request->query('to') : null;

        $store_id = auth()->user()->store_id;
        $withdraws = Withdraw::where('store_id', $store_id)->when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('created_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
        });
        // ->get();

        $paiments = PaymentCart::whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })
            ->when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('received_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
            });
        // ->get();

        $table = $request->has('table') ? $request->get('table') : null;

        if ($request->ajax() && $table == 'withdraw') {
            return DataTables::eloquent($withdraws)
                ->addColumn('action', function ($row) {
                    $confirmDeletion = auth()->user()->can('Supprimer attribut') ? '<form method="POST" action="' . route('admin.withdraw.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                    <a id="show_confirm" type="submit"
                    class=" text-danger show_confirm" data-toggle="tooltip"
                    title="Supprimer"><i class="bi bi-trash-fill"></i></a>            </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-Y h:i:s');
                    return $formattedDate;
                })

                ->rawColumns(['action', 'created_at'])

                ->make(true);
        }

        if ($request->ajax() && $table == 'paiments') {
            return DataTables::eloquent($paiments)
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-Y h:i:s');
                    return $formattedDate;
                })
                ->editColumn('type', function ($row) {
                    $html = '';
                    switch ($row->type) {
                        case 'cash':
                            $html = '<span class="badge  rounded-pill alert-success">Espèce</span>';
                            break;
                        case 'check':
                            $html = '<span class="badge  rounded-pill alert-primary">Chèque</span>';
                            break;
                        case 'transfer':
                            $html = '<span class="badge  rounded-pill alert-info">Virement</span>';
                            break;
                        case 'exchange':
                            $html = '<span class="badge  rounded-pill alert-danger">Traite</span>';
                            break;
                    }
                    return $html;
                })
                ->editColumn('status', function ($row) {
                    $html = null;
                    switch ($row->status) {
                        case '1':
                            $html = '<span class="badge rounded-pill alert-success">Débité</span>';
                            break;
                        case '0':
                            $html = '<span class="badge rounded-pill alert-danger">Rejeté</span>';
                            break;
                    }
                    return $html;
                })
                ->addColumn('codification', function ($row) {
                    $type = class_basename($row) == "PaymentCart" ? "BL" : "FA";
                    $badge = class_basename($row) == "PaymentCart" ? "success" : "info";
                    $html = '<span class="badge alert-' .  $badge . '">' . $type . ' : ' . $row->commitment->parent->codification . '</span>';
                    return $html;
                })
                // ->filterColumn('type', function($query, $keyword) {
                //     // dd($keyword);
                //     $query->where('type', 'LIKE', '%' . $keyword . '%');
                // })
                ->addColumn('client', function ($row) {
                    $html = $row->commitment->parent->client_details['name'];
                    return $html;
                })
                ->rawColumns(['created_at', 'type','codification', 'status'])
                ->make(true);
        }



        $total = FinancialCommitment::whereHas('cart', function ($query) use ($store_id) {
            $query->where('store_id', $store_id);
        })->whereDate('created_at', '<', date('Y-m-d'))->sum('amount');

        $total_cash = PaymentCart::where('type', 'cash')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })
            // ->whereDate('created_at', '<=', date('Y-m-d'))
            ->when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('received_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })
            ->sum('amount');

        $today_cash = PaymentCart::where('type', 'cash')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })->whereDate('received_at', date('Y-m-d'))->sum('amount');

        $total_check = PaymentCart::where('type', 'check')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })
            ->when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('received_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })
            ->whereDate('received_at', '<=', date('Y-m-d'))
            ->sum('amount');

        $today_check = PaymentCart::where('type', 'check')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })->whereDate('received_at', date('Y-m-d'))->sum('amount');

        $today_transfer = PaymentCart::where('type', 'transfer')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })->whereDate('received_at', date('Y-m-d'))->sum('amount');

        $today_rs = PaymentCart::where('type', 'exchange')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })->whereDate('received_at', date('Y-m-d'))->sum('amount');

        $todayIncome = $today_check + $today_cash;
        $total = $total_check + $total_cash;

        // dd($total, $withdraws);
        $total = $total - ($withdraws->sum('amount'));
        $total_cash = $total_cash - ($withdraws->sum('amount'));

        // dd($total);
        return view('admin.fund.fund', compact('withdraws', 'paiments', 'total', 'todayIncome', 'total_cash', 'today_cash', 'total_check', 'today_check', 'today_transfer', 'today_rs'));
    }

    public function withdraw(Request $request)
    {
        $store_id = auth()->user()->store_id;
        $withdraws = Withdraw::where('store_id', $store_id)->get();

        $total_cash = PaymentCart::where('type', 'cash')->whereHas('commitment', function ($query) use ($store_id) {
            $query->whereHas('cart', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        })->sum('amount');

        $total_cash = $total_cash - ($withdraws->sum('amount'));

        $request->validate([
            'amount' => 'required|lte:' . $total_cash,
        ]);
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data);
            $store_id = auth()->user()->store_id;
            $data['store_id'] = $store_id;
            $withdraw = Withdraw::create($data);
            DB::commit();
            return redirect()->route('admin.fund.fund')->with('success', "Opération effectuée avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
