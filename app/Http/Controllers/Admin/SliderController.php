<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if (isset($data['exclusive'])) {
                $data['exclusive'] = '1';
                DB::table('sliders')->update(['exclusive' => 0]);
            } else {
                $data['exclusive'] = '0';
            }
            $data['created_by'] = auth()->user()->name;
            $slider = Slider::create($data);
            if ($request->file('image')) {
                $path = $this->addAttachment($request->file('image'));
                $slider->image()->create(['url' => $path]);
            }
            DB::commit();
            return redirect()->route('admin.slider.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function addAttachment($file)
    {
        $filePath = 'uploads/sliders/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($path)
    {
        if (isset($path)) {
            if ($path != '') {
                $fileuri = str_replace('/storage', '', $path);
                Storage::disk('public')->delete($fileuri);
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        try {
            $data = $request->all();
            if (isset($data['exclusive'])) {
                $data['exclusive'] = '1';
                Slider::whereNot('id', $slider->id)->update(['exclusive' => 0]);
            } else {
                $data['exclusive'] = '0';
            }
            if ($request->file('image')) {
                if ($slider->image()->exists()) {
                    $this->deleteAttachment($slider->image->url);
                    $slider->image()->delete();
                }
                $path = $this->addAttachment($request->file('image'));
                $slider->image()->create(['url' => $path]);
            }
            $slider->update($data);
            return redirect()->route('admin.slider.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Slider $slider)
    {
        try {
            $slider->delete();
            return redirect()->route('admin.slider.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}