<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('admin.testimonial.index', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function testimonialvalidation(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        return response()->json($validated);
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['created_by'] = auth()->user()->name;
            $testimonial = Testimonial::create($data);
            if ($request->file('image')) {
                $path = $this->addAttachment($request->file('image'));
                $testimonial->image()->create(['url' => $path]);
            }
            DB::commit();
            return redirect()->route('admin.testimonial.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function addAttachment($file)
    {
        $filePath = 'uploads/clients/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($path)
    {
        if (isset($path)) {
            if ($path != '') {
                $fileuri = str_replace('/storage', '', $path);
                Storage::disk('public')->delete($fileuri);
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonial.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        try {
            $data = $request->all();
            if ($request->file('image')) {
                if ($testimonial->image()->exists()) {
                    $this->deleteAttachment($testimonial->image->url);
                    $testimonial->image()->delete();
                }
                $path = $this->addAttachment($request->file('image'));
                $testimonial->image()->create(['url' => $path]);
            }
            $testimonial->update($data);
            return redirect()->route('admin.testimonial.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Testimonial $testimonial)
    {
        try {
            $testimonial->delete();
            return redirect()->route('admin.testimonial.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}