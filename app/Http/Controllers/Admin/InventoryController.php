<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\Quantity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
        $carts = Inventory::query();
        if ($request->ajax()) {
            if ($request->has('product_id')) {
                $product_id = $request->query('product_id');
                $carts->where('store_id', auth()->user()->store_id)->whereHas('items', function ($query) use ($product_id) {
                    $query->where('product_id', $product_id);
                });
            } else {
                $carts->where('store_id', auth()->user()->store_id);
            }
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('provider') && $request->query('provider') != '') {
                $carts->where('provider_id', $request->query('provider'));
            }
            return DataTables::eloquent($carts)
                ->addColumn('check', function ($row) {
                    $check = ' <div class="form-check">';
                    if ($row->invoice_id == null) {
                        $check .= '<input name="cart_id[]" value="' . $row->id . '"
                    class="form-check-input" type="checkbox"
                    form="create-invoice-form">';
                    }
                    $check .= '</div>';
                    return $check;
                })
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->addColumn('user', function ($row) {
                    $user = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->user) {
                        $user .= ' <img src="' . asset($row->user->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $user .= '<p class="mb-0">' . $row->user->name . '</p>';
                    } else {
                        $user .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $user .= '<p class="mb-0">' . $row->user->name . '</p>';
                    }
                    $user .= '</div>';
                    return $user;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails inventaire') ? '  <a href="' . route('admin.inventory.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="View detail" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                    $updateBtn = auth()->user()->can('Modifier inventaire') ? '<a href="' . route('admin.inventory.edit', $row->id) . '" class="text-warning"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Edit info" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>' : '';

                    $printBtn = auth()->user()->can('Imprimer inventaire') ? ' <a href="' . route('admin.inventory.print', $row->id) . '"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                        target="blank" data-bs-original-title="Imprimer Inventaire"
                        aria-label="Views"><i class="bi bi-printer-fill text-black"></i>
                    </a>' : '';

                    $confirmDeletion = auth()->user()->can('Supprimer inventaire') ? '<form method="POST"
                        action="' . route('admin.inventory.delete', $row->id) . '"
                        class="delete_inventory">
                        ' . csrf_field() . '
                        <input name="_method" type="hidden" value="GET">
                        <a href="javascript:;" type="submit"
                            class="text-danger confirm-delete"
                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                            title="" data-bs-original-title="Supprimer"
                            aria-label="Supprimer"><i
                                class="bi bi-trash-fill"></i></a>
                        </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                    $btns .= $updateBtn . $confirmDeletion;
                    $btns .= $printBtn . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'user','check'])
                ->make(true);
        }
        return view('admin.inventory.index');
    }
    public function show(Inventory $inventory)
    {
        return view('admin.inventory.show', compact('inventory'));
    }
    public function inventoryvalidation(Request $request)
    {
        $validated = $request->validate([
            'items' => 'required|array|min:1',
        ]);

        return response()->json($validated);
    }
    public function create(Request $request)
    {
        // $products = Product::all();
        $products = array();
        return view('admin.inventory.create', compact('products'));
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data);
            /* Start Generate inventory number */
            $codification_setting = 'Codification inventaire';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);

            $data['codification'] = $nextCodification;

            /* End Generate inventory number */
            if (isset($data['items'])) {
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['store_id'] = auth()->user()->store->id;
                $data['user_id'] = auth()->user()->id;
                $data['created_by'] = auth()->user()->name;
                $inventory = Inventory::create($data);
                foreach ($data['items'] as $key => $item) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id' => $item['product_id']
                        ]
                    );
                    $data['items'][$key]['quantity_id'] = $quantity->id ?? null;
                  
                    Helper::updateQuantity($inventory, 'Création ', $data['items'][$key]['product_new_quantity'], $quantity, 'forced');
                }
                $inventory->items()->createMany($data['items']);

                DB::commit();

                if ($request->has('savewithprint')) {
                    $pdf = Helper::print2($inventory, 'inventory');
                    return $pdf->stream('inventaire.pdf');
                }

                return redirect()->route('admin.inventory.index')->with('success', "Ajouté avec succès!");
            } else {
                return redirect()->route('admin.inventory.create')->with('error', "Ajouter les produits!");
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function print(Inventory $inventory)
    {
        $pdf = Helper::print2($inventory, 'inventory');
        return $pdf->stream('inventaire.pdf');
    }


    public function edit(Inventory $inventory)
    {
        // $products = Product::all();
        $products = array();
        return view('admin.inventory.edit', compact('inventory', 'products'));
    }
    public function update(Request $request, Inventory $inventory)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            $data['number'] = $number;
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;
            $inventory->update($data);
            foreach ($data['items'] as $key => $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $item['product_id']
                    ]
                );
                $data['items'][$key]['quantity_id'] = $quantity->id ?? null;
              
                Helper::updateQuantity($inventory, 'Modification ', $data['items'][$key]['product_new_quantity'], $quantity, 'forced');
            }
            $inventory->items()->delete();
            $inventory->items()->createMany($data['items']);
            DB::commit();
            return redirect()->route('admin.inventory.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ($e->getLine());
        }
    }

    public function delete(Inventory $inventory)
    {
        try {
            DB::beginTransaction();
            foreach ($inventory->items as $key => $item) {
                $quantity = Quantity::firstOrCreate([
                    'product_id' => $item->product_id,
                    'store_id' => $inventory->store_id,
                ]);
                Helper::updateQuantity($inventory, 'Suppression ', $item->product_old_quantity, $quantity, 'forced');
            }
            $inventory->delete();
            DB::commit();
            return redirect()->route('admin.inventory.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
