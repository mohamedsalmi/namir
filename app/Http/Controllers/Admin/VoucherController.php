<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Barcode;
use App\Models\Voucher;
use App\Models\VouchersItems;
use App\Models\Provider;
use App\Models\ProviderCommitment;
use App\Models\Price;
use App\Models\Product;
use App\Models\Quantity;
use App\Notifications\MinQuantityNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class VoucherController extends Controller
{

    public function index(Request $request)
    {
        // $vouchers = Voucher::where('store_id', auth()->user()->store_id)->get();
        $carts = Voucher::query();
        if ($request->ajax()) {
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('provider') && $request->query('provider') != '') {
                $carts->where('provider_id', $request->query('provider'));
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total . 'DM';
                    return $total;
                })
                ->addColumn('provider', function ($row) {
                    $provider = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->provider) {
                        $provider .= ' <img src="' . asset($row->provider->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $provider .= '<p class="mb-0">' . $row->provider->code . '-' . $row->provider_details['name'] . '</p>';
                    } else {
                        $provider .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $provider .= '<p class="mb-0">' . $row->provider_details['name'] . '</p>';
                    }
                    $provider .= '</div>';
                    return $provider;
                })
                ->addColumn('status', function ($row) {
                    if ($row->paiment_status == 0) {
                        if ($row->payments()->exists()) {
                            $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                        } else {
                            $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                        }
                    } else {
                        $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails bon d\'achat') ? '  <a href="' . route('admin.voucher.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="View detail" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                    $updateBtn = auth()->user()->can('Modifier bon d\'achat') ? '<a href="' . route('admin.voucher.edit', $row->id) . '" class="text-warning"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Edit info" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>' : '';
                    $returnBtn = auth()->user()->can('Ajouter bon de retour') ? '<a href="' . route('admin.returncoupon.create', ['voucher' => $row->id]) . '"
                    class="text-success" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Créer un bon de retour" aria-label="Edit"><i
                        class="bi bi-arrow-return-left"></i></a>' : '';

                    $printBtn = auth()->user()->can('Imprimer bon d\'achat') ? '<div class="btn-group">
                    <button type="button" class="btn text-black dropdown-toggle-split"
                        data-bs-placement="bottom" title="" target="blank"
                        data-bs-original-title="Imprimer" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-printer-fill text-black"></i>
                        <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" style="">
                        <li><a href="' . route('admin.voucher.print', $row->id) . '"
                                class="dropdown-item" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title=""
                                target="blank" data-bs-original-title="Imprimer Bl"
                                aria-label="Views"><i
                                    class="bi bi-printer-fill text-black"></i> Imprimer
                                BA</a>
                        </li>' .
                        // <li><a href="' . route('admin.voucher.printticket', $row->id) . '"
                        //         class="dropdown-item" data-bs-toggle="tooltip"
                        //         data-bs-placement="bottom" title=""
                        //         target="blank"
                        //         data-bs-original-title="Imprimer ticket"
                        //         aria-label="Views"><i
                        //             class="bi bi-printer-fill text-black"></i> Imprimer
                        //         ticket</a>
                        // </li>
                        // </li>
                        '
                    </ul>
                </div>' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer bon d\'achat') ? '<a href="' . route('admin.voucher.delete', $row->id) . '"
                    class="text-danger" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Delete" aria-label="Delete"><i
                        class="bi bi-trash-fill"></i></a>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn . $updateBtn . $confirmDeletion;
                    
                    if (!$row->returnCoupon()->exists()) {
                        $btns .= $returnBtn;
                    }

                    $btns .= $printBtn . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'provider', 'status'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.voucher.index', compact('sum'));
    }
    public function show(Voucher $voucher)
    {
        $voucheritems = $voucher->items;
        return view('admin.voucher.show', compact('voucher', 'voucheritems'));
    }
    public function vouchervalidation(Request $request)
    {
        $validated = $request->validate([
            'provider_id' => 'required',
            'provider_details.name' => 'required',

            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',
        ]);

        return response()->json($validated);
    }
    public function create()
    {
        // $products = Product::all();
        $products = array();
        $providers = Provider::all();
        return view('admin.voucher.create', compact('providers', 'products'));
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            /*** codificaton */
            $codification_setting = 'Codification BA';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            $data['codification'] = $nextCodification;
            /*********** */
            // dd($data);
            if (isset($data['items'])) {
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['store_id'] = auth()->user()->store->id;
                $data['user_id'] = auth()->user()->id;
                $data['created_by'] = auth()->user()->name;

                $voucher = Voucher::create($data);
                // dd($data);
                foreach ($data['items'] as $key => $item) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id' => $data['items'][$key]['product_id']
                        ]
                    );
                    $update = Helper::updateQuantity($voucher, 'Création ', $data['items'][$key]['product_quantity'], $quantity, 'add');

                    if (isset($data['items'][$key]['update_price'])) {
                        $this->updatePrice($data['items'][$key]['product_id'], $data['items'][$key]['product_price_buying']);
                    }
                }
                $voucher->items()->createMany($data['items']);

                /**Payment */
                if (isset($data['Deadline_check'])) {
                    $voucher->commitments()->createMany($data['Deadline']);
                } elseif(!empty($data['paiements'])) {
                    $financial_data['voucher_id'] = $voucher->id;
                    $financial_data['date'] = date('Y-m-d H:i:s');
                    $financial_data['amount'] = $data['total'];
                    $total_payments_amounts = 0;
                    $methods = ['cash', 'check', 'exchange', 'transfer', 'rs', 'ra'];
                    foreach ($methods as $key => $method) {
                        if (isset($data['paiements'][$method])) {
                            foreach ($data['paiements'][$method] as $key => $payment) {
                                $total_payments_amounts += $payment['amount'];
                            }
                        }
                    }
                    $paiment_status = $total_payments_amounts >= $data['total'] ? 1 : 0;
                    $financial_data['paiment_status'] = $paiment_status;
                    $financial = ProviderCommitment::create($financial_data);
                    if (isset($data['paiements']['cash'])) {
                        $financial->paiements()->createMany($data['paiements']['cash']);
                    }
                    if (isset($data['paiements']['check'])) {
                        $financial->paiements()->createMany($data['paiements']['check']);
                    }
                    if (isset($data['paiements']['transfer'])) {
                        $financial->paiements()->createMany($data['paiements']['transfer']);
                    }
                    if (isset($data['paiements']['exchange'])) {
                        $financial->paiements()->createMany($data['paiements']['exchange']);
                    }
                    
                    $voucher->update(['paiment_status' => $paiment_status]);
                    $voucher->payments()->update(['created_by' => auth()->user()->name]);
                }
                /**Payment */

                DB::commit();
                if ($request->has('savewithprint')) {
                    $pdf = Helper::printPurchase($voucher, 'voucher');
                    return $pdf->stream('ba.pdf');
                }

                if ($request->has('savewithticket')) {
                    $pdf = Helper::printTicket($voucher, 'voucher');
                    return $pdf->stream('ticket.pdf');
                }
                return redirect()->route('admin.voucher.index')->with('success', "Ajouté avec succès!");
            } else {
                return redirect()->route('admin.voucher.create')->with('error', "Ajouter les produits!");
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function edit(Voucher $voucher)
    {
        $providers = Provider::all();
        // $products = Product::all();
        $products = array();
        return view('admin.voucher.edit', compact('voucher', 'providers', 'products'));
    }
    public function update(Request $request, Voucher $voucher)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            $data['number'] = $number;
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;

            $voucher->update($data);
            foreach ($data['items'] as $key => $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $data['items'][$key]['product_id']
                    ]
                );
                $update = Helper::updateQuantity($voucher, 'Modification ', $data['items'][$key]['product_quantity'], $quantity, 'add');

                if (isset($data['items'][$key]['update_price'])) {
                    $this->updatePrice($data['items'][$key]['product_id'], $data['items'][$key]['product_price_buying']);
                }
            }
            foreach ($voucher->items as $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $item->product_id
                    ]
                );
                $update = Helper::updateQuantity($voucher, 'Modification ', $item->product_quantity, $quantity, 'remove');
            }
            $voucher->items()->delete();
            $voucher->items()->createMany($data['items']);

            /**Payment */
            $voucher->commitments()->delete();
            if (isset($data['Deadline_check'])) {
                $voucher->commitments()->createMany($data['Deadline']);
                $voucher->update(['paiment_status' => 0]);
            } elseif(!empty($data['paiements'])) {
                $financial_data['voucher_id'] = $voucher->id;
                $financial_data['date'] = date('Y-m-d H:i:s');
                $financial_data['amount'] = $data['total'];
                $total_payments_amounts = 0;
                $methods = ['cash', 'check', 'exchange', 'transfer', 'rs', 'ra'];
                foreach ($methods as $key => $method) {
                    if (isset($data['paiements'][$method])) {
                        foreach ($data['paiements'][$method] as $key => $payment) {
                            $total_payments_amounts += $payment['amount'];
                        }
                    }
                }
                $paiment_status = $total_payments_amounts >= $data['total'] ? 1 : 0;
                $financial_data['paiment_status'] = $paiment_status;
                $financial = ProviderCommitment::create($financial_data);
                if (isset($data['paiements']['cash'])) {
                    $financial->paiements()->createMany($data['paiements']['cash']);
                }
                if (isset($data['paiements']['check'])) {
                    $financial->paiements()->createMany($data['paiements']['check']);
                }
                if (isset($data['paiements']['transfer'])) {
                    $financial->paiements()->createMany($data['paiements']['transfer']);
                }
                if (isset($data['paiements']['exchange'])) {
                    $financial->paiements()->createMany($data['paiements']['exchange']);
                }
                
                $voucher->update(['paiment_status' => $paiment_status]);
                $voucher->payments()->update(['created_by' => auth()->user()->name]);
            }
            /**Payment */

            DB::commit();
            return redirect()->route('admin.voucher.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ($e->getLine());
        }
    }
    public function delete(Voucher $voucher)
    {
        try {
            foreach ($voucher->items as $key => $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $item->product_id
                    ]
                );
                $update = Helper::updateQuantity($voucher, 'Suppression ', $item->product_quantity, $quantity, 'remove');
            }
            $voucher->delete();
            return redirect()->route('admin.voucher.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function updatePrice($product_id, $new_price_buying)
    {
        $ok = true;
        $product = Product::find($product_id);
        // $cof = (($product->selling1 / $product->buying) - 1) * 100;
        // $selling1 = $new_price_buying * (1 + (($product->selling1 / $product->buying) - 1));
        // $selling2 = $new_price_buying * (1 + (($product->selling2 / $product->buying) - 1));
        // $selling3 = $new_price_buying * (1 + (($product->selling3 / $product->buying) - 1));
        // $product->update(['buying' => $new_price_buying, 'selling1' => $selling1, 'selling2' => $selling2, 'selling3' => $selling3]);
        $ok = $product->update(['buying' => $new_price_buying,]);
        return ($ok);
    }
    public function print(Voucher $voucher)
    {
        $pdf = Helper::printPurchase($voucher, 'voucher');
        return $pdf->stream('bl.pdf');
    }

    public function printTicket(Voucher $voucher)
    {
        $pdf = Helper::printTicket($voucher, 'voucher');
        return $pdf->stream('ticket.pdf');
    }
}
