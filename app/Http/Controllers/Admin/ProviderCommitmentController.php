<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use App\Models\CommitmentPayment;
use App\Models\ReturnCoupon;
use App\Models\ProviderCommitment;
use App\Models\ReturnCommitment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class ProviderCommitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from = $request->has('from') ? $request->get('from') : null;
        $to = $request->has('to') ? $request->get('to') : null;

        if ($request->has('filter')) {
            $vouchers = Voucher::whereHas('commitments', function ($query) {
                $query->whereDate('date', '<=', Carbon::now())->where('paiment_status', 0);
            })->get();
            $returncoupons = ReturnCoupon::whereHas('commitments', function ($query) {
                $query->whereHas('paiements')->whereDate('date', '<=', Carbon::now())->where('paiment_status', 0);
            })->get();
        } else {
            $vouchers = Voucher::when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })->get();
            $returncoupons = ReturnCoupon::when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })->get();
        }
        $collection = collect();

        foreach ($vouchers as $voucher)
            $collection->push($voucher);
        foreach ($returncoupons as $returncoupon)
            $collection->push($returncoupon);

        $vouchersTotal = Voucher::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->sum('total');
        $returnCouponTotal = ReturnCoupon::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->sum('total');

        $total = $vouchersTotal - $returnCouponTotal;

        return view('admin.providercommitment.index', compact('collection', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $voucher)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'voucher':
                $model = 'App\Models\Voucher';
                break;
            case 'returncoupon':
                $model = 'App\Models\ReturnCoupon';
                break;
            default:
                break;
        }

        $voucher = $model::find($voucher);
        return view('admin.providercommitment.show', compact('voucher'));
    }

    public function showCommitment(Request $request, $commitment)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'voucher':
                $commitment_model = 'App\Models\ProviderCommitment';
                break;
            case 'returncoupon':
                $commitment_model = 'App\Models\ReturnCommitment';
                break;

            default:
                $commitment_model = 'App\Models\ProviderCommitment';
                break;
        }

        $commitment = $commitment_model::find($commitment);
        // dd($type,$commitment);
        return view('admin.providercommitment.showcommitment', compact('commitment'));
    }

    public function showReturn(ReturnCommitment $commitment)
    {
        return view('admin.providercommitment.returncoupon.show', compact('commitment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $voucher)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'voucher':
                $model = 'App\Models\Voucher';
                break;
            case 'returncoupon':
                $model = 'App\Models\ReturnCoupon';
                break;

            default:
                break;
        }

        $voucher = $model::find($voucher);

        return view('admin.providercommitment.edit', compact('voucher'));
    }

    public function paiementvalidation(Request $request)
    {
        $validated = $request->validate([
            'Deadline.*.amount' => 'required_if:Deadline_check,on|exclude_unless:Deadline_check,on|gt:0',
            'Deadline.*.date' => 'required_if:Deadline_check,on',
            'Deadline.*.currency_id' => 'required_if:Deadline_check,on',
            'paiements' => [Rule::requiredIf(function () use ($request) {
                return (empty($request->request->get('Deadline_check')));
            })],
            'paiements.cash.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['cash']) && empty($request->request->get('Deadline_check')));
            })],
            'paiements.cash.*.currency_id' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['cash']) && empty($request->request->get('Deadline_check')));
            })],
            'paiements.check.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.check.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.check.*.bank' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.check.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.transfer.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.transfer.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.transfer.*.bank' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.transfer.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.exchange.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.exchange.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')));
            })],
            'paiements.exchange.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')));
            })],

        ]);
        // dd($validated);

        return response()->json($validated);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $commitment)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'providercommitment':
                $model = 'App\Models\ProviderCommitment';
                $relation = 'voucher';
                break;
            case 'returncommitment':
                $model = 'App\Models\ReturnCommitment';
                $relation = 'returnCoupon';
                break;

            default:
                break;
        }

        if($request->has('commitment_id')){
            $commitment = $request->get('commitment_id');
        }

        $commitment = $model::find($commitment);

        try {
            DB::beginTransaction();
            $data = $request->all();
            if (isset($data['paiements']['cash'])) {
                $commitment->paiements()->createMany($data['paiements']['cash']);
            }
            if (isset($data['paiements']['check'])) {
                $commitment->paiements()->createMany($data['paiements']['check']);
            }
            if (isset($data['paiements']['transfer'])) {
                $commitment->paiements()->createMany($data['paiements']['transfer']);
            }
            if (isset($data['paiements']['exchange'])) {
                $commitment->paiements()->createMany($data['paiements']['exchange']);
            }
            $total = $commitment->paiements->sum('amount');
            // dd($commitment->amount);
            if ($total > $commitment->amount) {
                return redirect()->back()->with('error', "Total est supérieur au montant de l'échéance!");
            }
            if ($total == $commitment->amount) {
                $commitment->update(['paiment_status' => 1]);
            }
            // dd($commitment->voucher->total == $commitment->voucher->commitments()->where('paiment_status', 1)->sum('amount'), $commitment->voucher->total, $commitment->voucher->commitments()->where('paiment_status', 1)->sum('amount'));
            if ($commitment->$relation->total == $commitment->$relation->commitments()->where('paiment_status', 1)->sum('amount')) {
                $commitment->$relation()->update(['paiment_status' => 1]);
            }

            $commitment->paiements()->update(['created_by' => auth()->user()->name]);

            DB::commit();
            return redirect()->back()->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function updateCommitments(Request $request, $voucher)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $type = $request->query('type');
            switch ($type) {
                case 'voucher':
                    $model = 'App\Models\Voucher';
                    $commitment_model = 'App\Models\ProviderCommitment';
                    $foreign = 'voucher_id';
                    break;
                case 'returncoupon':
                    $model = 'App\Models\ReturnCoupon';
                    $commitment_model = 'App\Models\ReturnCommitment';
                    $foreign = 'return_coupon_id';
                    break;
                default:
                    break;
            }

            $voucher = $model::find($voucher);

            $voucher->commitments()->delete();

            if (isset($data['Deadline_check'])) {
                $voucher->commitments()->createMany($data['Deadline']);
                $voucher->update(['paiment_status' => 0]);
            } else {
                $financial_data[$foreign] = $voucher->id;
                $financial_data['date'] = date('Y-m-d H:i:s');
                $financial_data['amount'] = $data['total'];
                $financial_data['paiment_status'] = 1;
                $finacial = $commitment_model::create($financial_data);
                if (isset($data['paiements']['cash'])) {
                    $finacial->paiements()->createMany($data['paiements']['cash']);
                }
                if (isset($data['paiements']['check'])) {
                    $finacial->paiements()->createMany($data['paiements']['check']);
                }
                if (isset($data['paiements']['transfer'])) {
                    $finacial->paiements()->createMany($data['paiements']['transfer']);
                }
                if (isset($data['paiements']['exchange'])) {
                    $finacial->paiements()->createMany($data['paiements']['exchange']);
                }

                $voucher->update(['paiment_status' => 1]);
                $voucher->payments()->update(['created_by' => auth()->user()->name]);
            }
            DB::commit();
            return redirect()->route('admin.providercommitment.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Voucher $voucher)
    {
        try {
            DB::beginTransaction();
            $voucher->commitments()->delete();
            DB::commit();
            return redirect()->route('admin.providercommitment.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function deleteReturn(ReturnCommitment $commitment)
    {
        try {
            $commitment->delete();
            return redirect()->route('admin.providercommitment.returncoupon.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function deletePayment($payment, Request $request)
    {
        try {
            DB::beginTransaction();
            $model = null;
            $type = $request->query('type');

            switch ($type) {
                case 'voucher':
                    $model = 'App\Models\CommitmentPayment';
                    break;
                case 'returncoupon':
                    $model = 'App\Models\ReturnPayment';
                    break;
                case 'billcreditnote':
                    $model = 'App\Models\BillCreditNotePayment';
                    break;
                case 'financialcreditnote':
                    $model = 'App\Models\FinancialCreditNotePayment';
                    break;
                default:
                    break;
            }

            $payment = $model::find($payment);
            $payment->commitment()->update(['paiment_status' => 0]);
            $commitment = $payment->commitment;
            $parent = $commitment->parent->update(['paiment_status' => 0]);
            $payment->delete();
            // if ((!$commitment->paiements()->exists()) && ($commitment->parent->commitments->count() < 2)) {
            //     $commitment->delete();
            //     DB::commit();
            //     return redirect()->route('admin.providercommitment.index', ['from' => date("Y-m-d"), 'to' => date("Y-m-d")])->with('success', "Supprimé avec succès!");
            // }
            DB::commit();
            return redirect()->back()->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
