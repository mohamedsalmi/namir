<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Http\Requests\UpdateGeneratedInvoiceRequest;
use App\Models\Cart;
use App\Models\CartsItems;
use App\Models\Client;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\Facade\PDF;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

// use Barryvdh\DomPDF\Facade\Pdf;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $invoices = Invoice::where('store_id', auth()->user()->store_id)->get();
        $carts = Invoice::query();
        if ($request->ajax()) {
            $carts->where('store_id', auth()->user()->store_id);
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('client') && $request->query('client') != '') {
                $carts->where('client_id', $request->query('client'));
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total ;
                    return $total;
                })
                ->addColumn('check', function ($row) {
                    $check = ' <div class="form-check">';
                    if ($row->invoice_id == null) {
                        $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input" type="checkbox"
                        form="create-invoice-form">';
                    }
                    $check .= '</div>';
                    return $check;
                })
                ->addColumn('document', function ($row) {
                    $document = $this->getDocumentByInvoiceId($row->id);
                    $labels = [
                        'Cart' => 'Générer de Bls',
                        'Order' => 'Générer de comandes',
                        'OnlineSale' => 'Générer de Vente en ligne',
                    ];
                    return $labels[$document] ?? 'Générer';
                })
                ->addColumn('client', function ($row) {
                    $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->client) {
                        $client .= ' <img src="' . asset($row->client->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client->code . '-' . $row->client_details['name'] . '</p>';
                    } else {
                        $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client_details['name'] . '</p>';
                    }
                    $client .= '</div>';
                    return $client;
                })
                ->filterColumn('client', function($query, $keyword) {
                    $query->whereHas('client', function($q) use ($keyword){
                        $q->where('client_code', 'LIKE', '%' . $keyword . '%')->orWhere( 'name', 'LIKE', '%' . $keyword . '%');
                    })
                    ->orWhere('client_details->name', 'LIKE', '%' . $keyword . '%');
                })
                ->orderColumn('client', function($query, $dir) {
                    $query->orderBy('client_details->name', $dir);
                })
                ->addColumn('status', function ($row) {
                    $document = $this->getDocumentByInvoiceId($row->id);
                    if($document == 'Order'){
                        $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                    }else {
                        $relations = [
                            'Cart' => 'carts',
                            'Order' => 'orders',
                            'OnlineSale' => 'onlinesales',
                        ];
                        $relation = $relations[$document] ?? 'carts';

                        if ($row->$relation->count() == $row->$relation->where('paiment_status', 1)->count()) {
                            $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                        } 
                        else if($row->$relation->where('paiment_status', 1)->count() > 0){
                            $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                        }
                        else {
                            $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                        }
                    }
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails facture') ? '<a href="' . route('admin.invoice.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Voir en Facture" aria-label="Views"><i
                        class="bi bi-file-pdf-fill text-danger"></i></a>' : '';

                    // $updateBtn = auth()->user()->can('Modifier facture') ? ' <a href="' . route('admin.invoice.edit', $row->id) . '"
                    // class="text-warning" data-bs-toggle="tooltip"
                    // data-bs-placement="bottom" title=""
                    // data-bs-original-title="Modifier" aria-label="Edit"><i
                    //     class="bi bi-pencil-fill"></i></a>' : '';

                    $updateBtn = '<a href="' . route('admin.invoice.edit.generated', $row->id) . '"
                        class="text-warning" data-bs-toggle="tooltip"
                        data-bs-placement="bottom" title="Modifier"
                        data-bs-original-title="Modifier" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>';

                    $avoirBtn = auth()->user()->can('Ajouter facture avoir') ? '<a href="' . route('admin.creditnote.create', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Créer Avoir" aria-label="Views"><i
                        class="bi bi-eraser text-success"></i></a>' : '';
                        $printBtn = auth()->user()->can('Imprimer facture') ? '  <div class="btn-group">
                        <button type="button"
                            class="btn text-black dropdown-toggle-split"
                            data-bs-placement="bottom" title="" target="blank"
                            data-bs-original-title="Imprimer" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="bi bi-printer-fill text-black"></i>
                            <span class="visually-hidden">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" style="">
                            <li><a href="' . route('admin.invoice.pdf', ['invoice' => $row->id, 'type' => 'download']) . '"
                                    class="dropdown-item" data-bs-toggle="tooltip"
                                    data-bs-placement="bottom" title="Imprimer facture"
                                    target="_blank" data-bs-original-title="Imprimer facture"
                                    aria-label="Views"><i
                                        class="bi bi-printer-fill text-black"></i> Imprimer facture</a>
                            </li>
                            <li><a href="' . route('admin.invoice.printras', $row->id) . '"
                                    class="dropdown-item" data-bs-toggle="tooltip"
                                    data-bs-placement="bottom" title="Imprimer RàS"
                                    target="blank"
                                    data-bs-original-title="Imprimer RàS"
                                    aria-label="Views"><i
                                        class="bi bi-printer-fill text-black"></i> Imprimer RàS</a>
                            </li>
                            </li>
                        </ul>
                    </div>' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer bon de livraison') ? '' : '';
                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;

                    $btns .= $updateBtn . $confirmDeletion;

                    $btns .= $avoirBtn . $printBtn . '</div>';
                    return $btns;
                })
                ->rawColumns(['action', 'check', 'client', 'status','document'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.invoice.index', compact('sum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {

    //     try {
    //         $data = $request->all();
    //         $nameSpace = '\\App\Models\\';
    //         $model = $nameSpace . ucfirst($data['document']);
    //         if ($data['document'] == 'cart') {
    //             $modelitem = $nameSpace . $data['document'] . 'sItems';
    //         }else{
    //             $modelitem = $nameSpace . ucfirst($data['document']) . 'Item';
    //         }
    //         DB::beginTransaction();

    //         if (isset($data['cart_id'])) {
    //             $carts = $model::whereIn('id', $data['cart_id']);
    //             if ($carts->groupBy('client_id')->get()->count() > 1) {
    //                 return redirect()->route('admin.'.$data['document'].'.index')->with('error', "Choisir des documents de même client !");
    //             }
    //             $number = count($data["cart_id"]);
    //             // get last record
    //             $codification_setting = 'Codification facture';
    //             $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);

    //             $data['codification'] = $nextCodification;

    //             $data['number'] = $number;
    //             $data['store_id'] = auth()->user()->store->id;
    //             $data['user_id'] = auth()->user()->id;
    //             $data['created_by'] = auth()->user()->name;
    //             if (isset($data['timbre'])) {
    //                 $data['timbre'] = '1';
    //             } else {
    //                 $data['timbre'] = '0';
    //             }
    //             $data['client_id'] = $carts->first()->client_id;
    //             $data['client_details'] = $carts->first()->client_details;
    //             $paiment_status = $carts->sum('paiment_status');
    //             if ($paiment_status == $number) {
    //                 $data['paiment_status'] = 1;
    //             }
    //             $data['total'] = $carts->sum('total');
    //             $invoice = Invoice::create($data);
    //             $model::whereIn('id', $data['cart_id'])->update(['invoice_id' => $invoice->id]);                
    //             $cart_items = $modelitem::whereIn($data['document'].'_id', $data['cart_id'])
    //                 ->get();
    //             $data["items"] = $cart_items->toArray();
    //             $invoice->items()->createMany($data["items"]);
    //             DB::commit();
    //             return redirect()->route('admin.invoice.index')->with('success', "Créer avec succès!");
    //         } else {
    //             return redirect()->route('admin.'.$data['document'].'.index')->with('error', "Ajouter les documents à facturer !");
    //         }
    //     } catch (\Exception $e) {
    //         DB::rollBack();
    //         Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
    //         return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
    //     }
    // }
    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'document' => 'required',
                'cart_id' => 'required|array',
                'timbre' => 'nullable',
                'date' => 'required',
            ]);

            $modelClass = 'App\Models\\' . $data['document'];
            $modelItemClass = 'App\Models\\' . $data['document'] . 'Item';
            if ($data['document'] == 'Cart') {
                $modelItemClass = 'App\Models\\' . $data['document'] . 'sItems';
            }

            // Ensure all carts belong to same client
            $carts = $modelClass::whereIn('id', $data['cart_id']);
            $uniqueClientIds = $carts->get()->unique('client_id')->pluck('client_id');
            if ($uniqueClientIds->count() > 1) {
                return redirect()->route('admin.' . strtolower($data['document']) . '.index')
                    ->with('error', "Choisir des documents de même client !");
            }

            DB::beginTransaction();

            // Generate invoice data
            $number = count($data["cart_id"]);
            $codification_setting = 'Codification facture';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);

            $invoiceData = [
                'codification' => $nextCodification,
                'number' => $number,
                'store_id' => auth()->user()->store->id,
                'user_id' => auth()->user()->id,
                'created_by' => auth()->user()->name,
                'client_id' => $uniqueClientIds->first(),
                'client_details' => $carts->first()->client_details,
                'total' => $carts->sum('total'),
                'timbre' => isset($data['timbre']) ? '1' : '0',
                'date' => $data['date'],
            ];

            $invoice = Invoice::create($invoiceData);

            // Update cart items with invoice id
            $carts->update(['invoice_id' => $invoice->id]);

            // Create invoice items        
            $cartItems = $modelItemClass::whereIn($data['document'] == 'OnlineSale' ? 'online_sale_id' : strtolower($data['document']) . '_id', $data['cart_id'])->get();
            $itemsData = $cartItems->toArray();
            $invoice->items()->createMany($itemsData);

            DB::commit();

            return redirect()->route('admin.invoice.index')
                ->with('success', "Créer avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return view('admin.invoice.show', compact('invoice'));
    }

    public function pdf(Invoice $invoice, string $type)
    {

        Helper::print($invoice, 'invoice');
    }

    public function pdf2(Invoice $invoice)
    {
        return view('admin.invoice.pdf.invoice', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        $clients = Client::all();
        // $products = Product::all();
        $products = array();
        $type = 'invoice';
        $cart = $invoice;
        $title = 'Facture';
        return view('admin.cart.edit', compact('cart', 'title', 'type', 'clients', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            $data['number'] = $number;
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;
            $invoice->update($data);
            $invoice->items()->delete();
            $invoice->items()->createMany($data['items']);

            DB::commit();
            return redirect()->route('admin.invoice.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ($e->getLine());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   /**
     * check the document .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDocumentByInvoiceId($invoiceId)
    {
        $documentModels = [
            'App\Models\Cart',
            'App\Models\OnlineSale',
            'App\Models\Order'
        ];
    
        foreach ($documentModels as $documentModel) {
            if ($documentModel::where('invoice_id', $invoiceId)->exists()) {
                return (new \ReflectionClass($documentModel))->getShortName();
            }
        }
    
        return null;
    }
    
    public function printras(Invoice $invoice)
    {
        Helper::printRas($invoice);
    }

    
    public function editGenerated(Invoice $invoice)
    {
        $clients = Client::all();
        return view('admin.invoice.edit', compact('invoice', 'clients'));
    }

    public function updateGenerated(UpdateGeneratedInvoiceRequest $request, Invoice $invoice)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;
            $invoice->update($data);
           
            DB::commit();
            return redirect()->route('admin.invoice.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            $error = "File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage();
            Log::error($error);
            return $error;
        }
    }
    
}
