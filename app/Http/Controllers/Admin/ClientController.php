<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientPricingRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $clients = Client::all();
        return view('admin.client.index',compact('clients'));
    }

    public function create()
    {

        return view('admin.client.create');
    }
    public function store(CreateClientRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $client = Client::create($data);
            if ($request->file('avatar')) {
                $path = $this->addAttachment($request->file('avatar'));
                $client->image()->create(['url' => $path]);
            }
            DB::commit();
            return redirect()->route('admin.client.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        return($e->getMessage())  ;   }
    }
    public function edit(Client $client)
    {
        return view('admin.client.edit',compact('client'));
    }
    public function update(UpdateClientRequest $request  ,Client $client )
    {
        try {
            $data = $request->all();
            if ($request->file('avatar')) {
                if($client->image()->exists()){
                    $this->deleteAttachment($client->image->url);
                    $client->image()->delete();
                }
                $path = $this->addAttachment($request->file('avatar'));
                $client->image()->create(['url' => $path]);
            }            
            $client->update($data);
            return redirect()->route('admin.client.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function delete(Client $Client)
    {
        try {
            $Client->delete();
            return redirect()->route('admin.client.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function addAttachment($file)
    {
        $filePath = 'uploads/clients/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($path)
    {
        if (isset($path)) {
            if ($path != '') {
                $fileuri = str_replace('/storage', '', $path);
                Storage::disk('public')->delete($fileuri);
            }
        }
    }

    public function pricing(Client $client)
    {
        $products = Product::all();
        return view('admin.client.pricing', compact('client', 'products'));
    }
public function validatepricing(Request $request){
    $validated = $request->validate([
        'price_cv' => 'required_with:date_cv',
        'date_cv' => 'required_with:price_cv',
    ]);
    return response()->json($validated);
}
    public function updatePricing(UpdateClientPricingRequest $request, Client $client)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data);
       
                    $client->pricings()->updateOrCreate(['product_id' => $data['product_id']], $data);
                    // dd($client->pricings()->where(['product_id' => $data['product_id']])->first());

            DB::commit();
            return response()->json(['sucess'=> true]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json([],500);

        }
    }
    public function valdationprofile(Request $request){
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:clients,email,' . auth()->user()->id,
            'password' => 'nullable|confirmed|min:6',
            // 'avatar' => 'image',
        ]);

        return response()->json($validated);
    }
    public function avatarchange(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image',
        ]);
        try {
            $client = Client::find(auth()->user()->id);
            if ($request->file('avatar')) {
                if ($client->image()->exists()) {
                    $this->deleteAttachment($client->image->url);
                    $client->image()->delete();
                }
                $path = $this->addAttachment($request->file('avatar'));
                $client->image()->create(['url' => $path]);
            }
            return redirect()->back()->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
    public function updateProfile(Request $request)
    {
        try {
            $client = Client::find(auth()->user()->id);
            $data = $request->all();
            if (isset($data['password']) && !empty($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }
            $client->update($data);
            return redirect()->back()->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
