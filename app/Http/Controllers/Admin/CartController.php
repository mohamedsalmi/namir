<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use App\Helpers\Helper;
    use App\Http\Requests\CreateCartRequest;
    use App\Http\Requests\CreateOfferRequest;
    use App\Http\Requests\UpdateOfferRequest;
    use App\Models\AttributeValue;
    use App\Models\Barcode;
    use App\Models\Cart;
    use App\Models\CartsItems;
    use App\Models\Client;
    use App\Models\ClientPricing;
    use App\Models\FinancialCommitment;
    use App\Models\Order;
    use App\Models\Price;
    use App\Models\Product;
    use App\Models\ProductAttributeValue;
    use App\Models\Quantity;
    use App\Models\SaleQuote;
    use App\Notifications\MinQuantityNotification;
    use Carbon\Carbon;
    use Faker\Core\Number;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Validation\Rule;
    use NumberFormatter;
    use PhpParser\JsonDecoder;
    use Yajra\DataTables\Facades\DataTables;
    
    class CartController extends Controller
    {
        
        public function searchByName(Request $request, $query)
        {
            return Product::where('name', 'like', '%' . $query . '%')
                ->orWhere('sku', $query)->limit(1000)->get();
        }
        public function filter(Request $request)
        {
            $data = $request->all();
            $products = Product::query();
            if (!empty($data['name'])) {
                $products->where('name', 'like', '%' . $data['name'] . '%');
            }
            if ($data['all_status'] != 'true') {
                $products->where('status', 1);
            }
            if (!empty($data['sku'])) {
                $products->where('sku', $data['sku']);
            }
            if (!empty($data['ecrivain'])) {
                $products->whereHas('productAttributes', function ($query) use ($data) {
                    // $query->where('attribute_id', $attribute_id);
                    //     $query ->whereHas('attribute', function ($query) use($data) {
                    //         $query->where('name', 'اسم المؤلف');
                    // });
                    $query->whereHas('values', function ($query) use ($data) {
                        $query->whereHas('attributeValue', function ($query) use ($data) {
                            $query->where('value', 'like', '%' . $data['ecrivain'] . '%');
                        });
                    });
                });
            }
            if (!empty($data['maison'])) {
                $products->whereHas('productAttributes', function ($query) use ($data) {
                    // $query->where('attribute_id', $attribute_id);
                    //     $query ->whereHas('attribute', function ($query) use($data) {
                    //         $query->where('name', 'دار النشر');
                    // });
                    $query->whereHas('values', function ($query) use ($data) {
                        $query->whereHas('attributeValue', function ($query) use ($data) {
                            $query->where('value',  'like', '%' . $data['maison'] . '%');
                        });
                    });
                });
            }
            if (!empty($data['enquete'])) {
                $products->whereHas('productAttributes', function ($query) use ($data) {
                    // $query->where('attribute_id', $attribute_id);
                    //     $query ->whereHas('attribute', function ($query) use($data) {
                    //         $query->where('name','المحقق');
                    // });
                    $query->whereHas('values', function ($query) use ($data) {
                        $query->whereHas('attributeValue', function ($query) use ($data) {
                            $query->where('value',  'like', '%' . $data['enquete'] . '%');
                        });
                    });
                });
            }
            // $products->withSum("quantities as old_quantity", 'quantity');
            $products->withCount(['quantities as old_quantity' => function ($query) {
                $query->select(DB::raw('sum(quantities.quantity)'))->where('store_id', auth()->user()->store_id);
            }]);
            
            return $products->limit(50)->get();
        }
        public function index(Request $request)
        {
            $carts = Cart::query();
            if ($request->ajax()) {
                if ($request->has('product_id')) {
                    $product_id = $request->query('product_id');
                    $carts->where('store_id', auth()->user()->store_id)->whereHas('items', function ($query) use ($product_id) {
                        $query->where('product_id', $product_id);
                    });
                } else {
                    $carts->where('store_id', auth()->user()->store_id);
                }
                if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                    $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                }
                if ($request->has('client') && $request->query('client') != '') {
                    $carts->where('client_id', $request->query('client'));
                }
                return DataTables::eloquent($carts)
                    ->editColumn('date', function ($row) {
                        $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                        return $formattedDate;
                    })
                    ->editColumn('created_at', function ($row) {
                        $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('total', function ($row) {
                        $total = $row->total ;
                        return $total;
                    })
                    ->addColumn('check', function ($row) {
                        $check = ' <div class="form-check">';
                        if ($row->invoice_id == null) {
                            $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input multi-check" type="checkbox"
                        form="create-invoice-form">';
                        }
                        $check .= '</div>';
                        return $check;
                    })
                    ->addColumn('client', function ($row) {
                        $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                        if ($row->client) {
                            $client .= ' <img src="' . asset($row->client->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                            $client .= '<p class="mb-0">' . $row->client->code . '-' . $row->client_details['name'] . '</p>';
                        } else {
                            $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                            $client .= '<p class="mb-0">' . $row->client_details['name'] . '</p>';
                        }
                        $client .= '</div>';
                        return $client;
                    })
                    ->filterColumn('client', function($query, $keyword) {
                        $query->whereHas('client', function($q) use ($keyword){
                            $q->where('client_code', 'LIKE', '%' . $keyword . '%')->orWhere( 'name', 'LIKE', '%' . $keyword . '%');
                        })
                        ->orWhere('client_details->name', 'LIKE', '%' . $keyword . '%');
                    })
                    ->orderColumn('client', function($query, $dir) {
                        $query->orderBy('client_details->name', $dir);
                    })
                    ->addColumn('status', function ($row) {
                        if ($row->paiment_status == 0) {
                            if ($row->payments()->exists()) {
                                $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                            } else {
                                $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                            }
                        } else {
                            $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $showBtn = auth()->user()->can('Détails bon de livraison') ? '<a href="' . route('admin.cart.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title="Voir détails"
                    data-bs-original-title="Voir les détails" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                        $updateBtn = auth()->user()->can('Modifier bon de livraison') ? '<a href="' . route('admin.cart.edit', $row->id) . '"
                    class="text-warning" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="Modifier"
                    data-bs-original-title="Modifier" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>' : '';
                        $carttosalequtoBtn = auth()->user()->can('Modifier bon de livraison') ? ' <a href="' . route('admin.cart.CartToSaleQuote', $row->id) . '"
                    class="text-danger" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="Transformer en devis"
                    data-bs-original-title="Transformer en devis"
                    aria-label="Transformer en devis"><i
                        class="bx bx-recycle"></i></a>' : '';
                        $returnBtn = auth()->user()->can('Ajouter bon de retour BL') ? ' <a href="' . route('admin.returnnote.create', $row->id) . '"
                    class="text-primary" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="Créer bon de retour"
                    data-bs-original-title="Créer bon de retour"
                    aria-label="Views">
                    <i class="bi bi-arrow-90deg-left text-sucess"></i>
                </a>' : '';
                        $showinvoiceBtn = auth()->user()->can('Détails facture') ? ' <a href="' . route('admin.invoice.show', $row->invoice_id) . '"
                    class="text-primary" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="Voir Facture"
                    data-bs-original-title="Voir Facture" aria-label="Views"><i
                        class="bi bi-file-pdf-fill text-danger"></i></a>' : '';
                        $duplicateBtn = auth()->user()->can('Détails facture') ? ' <a href="' . route('admin.cart.duplicatecart', $row->id) . '"
                    class="text-info" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="Duppliquer"
                    data-bs-original-title="Dupliquer" aria-label="Dupliquer"><i
                        class="bx bx-duplicate"></i></a>' : '';
                        $printBtn = auth()->user()->can('Imprimer bon de livraison') ? '  <div class="btn-group">
                    <button type="button"
                        class="btn text-black dropdown-toggle-split"
                        data-bs-placement="bottom" title="" target="blank"
                        data-bs-original-title="Imprimer" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-printer-fill text-black"></i>
                        <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" style="">
                        <li><a href="' . route('admin.cart.print', $row->id) . '"
                                class="dropdown-item" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title=""
                                target="blank" data-bs-original-title="Imprimer Bl"
                                aria-label="Views"><i
                                    class="bi bi-printer-fill text-black"></i> Imprimer
                                Bl</a>
                        </li>
                        <li><a href="' . route('admin.cart.printticket', $row->id) . '"
                                class="dropdown-item" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title=""
                                target="blank"
                                data-bs-original-title="Imprimer ticket"
                                aria-label="Views"><i
                                    class="bi bi-printer-fill text-black"></i> Imprimer
                                ticket</a>
                        </li>
                        </li>
                    </ul>
                </div>' : '';
                        $confirmDeletion = auth()->user()->can('Supprimer bon de livraison') ? '<form method="POST"
                            action="' . route('admin.cart.delete', $row->id) . '"
                            class="delete_cart">
                            ' . csrf_field() . '
                            <input name="_method" type="hidden" value="GET">
                            <a href="javascript:;" id="show_confirm" type="submit"
                                class="text-danger show_confirm" title="Supprimer"
                                data-bs-toggle="tooltip" data-bs-placement="bottom"
                                title="" data-bs-original-title="Supprimer"
                                aria-label="Supprimer"><i
                                    class="bi bi-trash-fill"></i></a>
                        </form>' : '';
                        
                        $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                        if ($row->invoice_id == null) {
                            $btns .= $updateBtn . $confirmDeletion;
                            if (!$row->payments()->exists()) {
                                $btns .= $carttosalequtoBtn;
                            } else {
                                if ($row->return_status) {
                                    $btns .= $returnBtn;
                                }
                            }
                        } else {
                            $btns .= $showinvoiceBtn . '<span>' . $row->invoice->codification . '</span>';
                        }
                        
                        $btns .= $printBtn . $duplicateBtn . '</div>';
                        
                        return $btns;
                    })
                    ->rawColumns(['action', 'check', 'client', 'status'])
                    ->withQuery('total', function ($filteredQuery) {
                        return $filteredQuery->sum('total');
                    })
                    ->make(true);
            }
            $sum = $carts->sum('total');
            return view('admin.cart.index', compact('sum'));
        }
        public function show(Cart $cart)
        {
            $cartitems = $cart->items;
            return view('admin.cart.show', compact('cart', 'cartitems'));
        }
        public function cartvalidation(Request $request)
        {
            $validated = $request->validate([
                'client_id' => 'required',
                'client_details.name' => 'required',
                'date' => 'required',
                'total' => 'required|numeric|gte:0',
                'items' => 'required|array|min:1',
                'Deadline.*.amount' => 'required_if:Deadline_check,on|exclude_unless:Deadline_check,on|gt:0',
                'Deadline.*.date' => 'required_if:Deadline_check,on',
                'Deadline.*.currency_id' => 'required_if:Deadline_check,on',
                // 'paiements' => [Rule::requiredIf(function () use ($request) {
                //     return (empty($request->request->get('Deadline_check')));
                // })],
                'paiements.cash.*.amount' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['cash'] && empty($request->request->get('Deadline_check'))));
                })],
                'paiements.cash.*.currency_id' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['cash'] && empty($request->request->get('Deadline_check'))));
                })],
                'paiements.check.*.amount' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['check'] && empty($request->request->get('Deadline_check'))));
                })],
                'paiements.check.*.numbre' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.check.*.bank' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.check.*.received_at' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.transfer.*.amount' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.transfer.*.numbre' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.transfer.*.bank' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.transfer.*.received_at' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.exchange.*.amount' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.exchange.*.numbre' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
                })],
                'paiements.exchange.*.received_at' => [Rule::requiredIf(function () use ($request) {
                    return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
                })],
            ]);
            
            
            return response()->json($validated);
        }
        public function create(Request $request)
        {
            $salequote = null;
            $order = null;
            $type = 'cart';
            if ($request->has('order') && $request->query('order') != '') {
                $order = Order::find($request->query('order'));
            }
            $clients = Client::all();
            //$products = Product::all();
            $products = array();
            return view('admin.cart.create', compact('clients', 'order', 'type', 'products'));
        }
        public function store(Request $request)
        {
            
            try {
                DB::beginTransaction();
                $data = $request->all();
                $codification_setting = 'Codification BL';
                $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
                $data['codification'] = $nextCodification;
                
                /* End Generate cart number */
                if (isset($data['items'])) {
                    $number = count($data["items"]);
                    $data['number'] = $number;
                    $data['store_id'] = auth()->user()->store->id;
                    $data['user_id'] = auth()->user()->id;
                    $data['created_by'] = auth()->user()->name;
                    $data['return_status'] = 1;
                    $cart = Cart::create($data);
                    // dd($data);
                    foreach ($data['items'] as $key => $item) {
                        $data['items'][$key]['possible_return'] = $data['items'][$key]['product_quantity'];
                        $qt = $data['items'][$key]['product_quantity'];
                        if ($qt > 0) {
                            $quantity=Quantity::firstOrCreate(
                                [
                                    'store_id' => auth()->user()->store_id,
                                    'product_id'=>$data['items'][$key]['product_id']
                                ]
                            );
                            $quantity_update = Helper::updateQuantity($cart, 'Création ', $item['product_quantity'], $quantity, 'remove');

                        }
                        $data['items'][$key]['quantity_id'] = $quantity->id;
                    }
                    
                    $cart->items()->createMany($data['items']);
                    if (isset($data['Deadline_check'])) {
                        $cart->commitments()->createMany($data['Deadline']);
                    } elseif(!empty($data['paiements'])) {
                        $financial_data['cart_id'] = $cart->id;
                        $financial_data['date'] = date('Y-m-d H:i:s');
                        $financial_data['amount'] = $data['total'];
                        $total_payments_amounts = 0;
                        $methods = ['cash', 'check', 'exchange', 'transfer', 'rs', 'ra'];
                        foreach ($methods as $key => $method) {
                            if (isset($data['paiements'][$method])) {
                                foreach ($data['paiements'][$method] as $key => $payment) {
                                    $total_payments_amounts += $payment['amount'];
                                }
                            }
                        }
                        $paiment_status = $total_payments_amounts >= $data['total'] ? 1 : 0;
                        // dd($paiment_status);
                        $financial_data['paiment_status'] = $paiment_status;
                        $financial = FinancialCommitment::create($financial_data);
                        if (isset($data['paiements']['cash'])) {
                            $financial->paiements()->createMany($data['paiements']['cash']);
                        }
                        if (isset($data['paiements']['check'])) {
                            $financial->paiements()->createMany($data['paiements']['check']);
                        }
                        if (isset($data['paiements']['transfer'])) {
                            $financial->paiements()->createMany($data['paiements']['transfer']);
                        }
                        if (isset($data['paiements']['exchange'])) {
                            $financial->paiements()->createMany($data['paiements']['exchange']);
                        }
                        
                        $cart->update(['paiment_status' => $paiment_status]);
                        $cart->payments()->update(['created_by' => auth()->user()->name]);
                    }
                    DB::commit();
                    
                    if ($request->has('savewithprint')) {
                        $pdf = Helper::print($cart, 'cart');
                        return $pdf->stream('bl.pdf');
                    }
                    
                    if ($request->has('savewithticket')) {
                        $pdf = Helper::printTicket($cart, 'cart');
                        return $pdf->stream('ticket.pdf');
                    }
                    return redirect()->route('admin.cart.index')->with('success', "Ajouté avec succès!");
                } else {
                    return redirect()->route('admin.cart.create')->with('error', "Ajouter les produits!");
                }
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        public function duplicatecart(Cart $cart)
        {
            try {
                DB::beginTransaction();
                
                /* Start Generate cart number */
                $codification_setting = 'Codification BL';
                $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
                $data['codification'] = $nextCodification;
                /* End Generate cart number */
                $newcart = $cart->replicate();
                $newcart->codification = $nextCodification;
                $newcart->paiment_status = '0';
                $newcart->invoice_id = '0';
                $newcart->push();
                $newcart->items()->createMany($cart->items->toArray());
                foreach ($cart->items as $item) {
                    $quantity = Quantity::firstOrCreate([
                        'store_id' => auth()->user()->store_id,
                        'product_id'=>$item['product_id']
                    ]);
                    $update = Helper::updateQuantity($newcart, 'Création ', $item['product_quantity'], $quantity, 'remove');
                }
                
                DB::commit();
                
                return redirect()->route('admin.cart.edit', $newcart->id)->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return redirect()->route('admin.cart.index')->with('error', "Echec de duplication!");
            }
        }
        public function CartToSaleQuote(Cart $cart)
        {
            try {
                DB::beginTransaction();
                
                $codification_setting = 'Codification devis';
                $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
                
                $salequote = SaleQuote::create($cart->toArray());
                $salequote->update(['codification' => $nextCodification]);
                $salequote->items()->createMany($cart->items->toArray());
                foreach ($cart->items as $item) {
                    $quantity=Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id'=>$item->product_id
                        ]
                    );
                    $update = Helper::updateQuantity($cart, 'Conversion vers devis ', $item->product_quantity, $quantity, 'add');   
                }
                $cart->delete();
                DB::commit();
                return redirect()->route('admin.cart.index')->with('success', "Effectué avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        public function print(Cart $cart)
        {
            $pdf = Helper::print($cart, 'cart');
            return $pdf->stream('bl.pdf');
        }
        
        public function printTicket(Cart $cart)
        {
            $pdf = Helper::printTicket($cart, 'cart');
            return $pdf->stream('ticket.pdf');
        }
        
        public function edit(Cart $cart)
        {
            $clients = Client::all();
            // $products = Product::all();
            $products = array();
            $type = 'cart';
            $title = 'BL';
            return view('admin.cart.edit', compact('type', 'title', 'cart', 'clients', 'products'));
        }
        public function update(Request $request, Cart $cart)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['user_id'] = auth()->user()->id;
                $data['updated_by'] = auth()->user()->name;
                $cart->update($data);
                foreach ($data['items'] as $key => $item) {
                    $qt = $data['items'][$key]['product_quantity'];
                    if ($qt > 0) {
                        $quantity=Quantity::firstOrCreate(
                            [
                                'store_id' => auth()->user()->store_id,
                                'product_id'=>$data['items'][$key]['product_id']
                            ]
                        );
                        $update = Helper::updateQuantity($cart, 'Modification ', $item['product_quantity'], $quantity, 'remove');
                    }
                }
                
                foreach ($cart->items as $item) {
                    $quantity=Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id'=>$item->product_id
                        ]
                    );
                    $update = Helper::updateQuantity($cart, 'Modification ', $item->product_quantity, $quantity, 'add');   
                }
                
                $cart->items()->delete();
                $cart->items()->createMany($data['items']);
                
                /* Payment */
                $cart->commitments()->delete();
                if (isset($data['Deadline_check'])) {
                    $cart->commitments()->createMany($data['Deadline']);
                    $cart->update(['paiment_status' => 0]);
                } elseif(!empty($data['paiements'])) {
                    $financial_data['cart_id'] = $cart->id;
                    $financial_data['date'] = date('Y-m-d H:i:s');
                    $financial_data['amount'] = $data['total'];
                    $total_payments_amounts = 0;
                    $methods = ['cash', 'check', 'exchange', 'transfer', 'rs', 'ra'];
                    foreach ($methods as $key => $method) {
                        if (isset($data['paiements'][$method])) {
                            foreach ($data['paiements'][$method] as $key => $payment) {
                                $total_payments_amounts += $payment['amount'];
                            }
                        }
                    }
                    $paiment_status = $total_payments_amounts >= $data['total'] ? 1 : 0;
                    $financial_data['paiment_status'] = $paiment_status;
                    $financial = FinancialCommitment::create($financial_data);
                    if (isset($data['paiements']['cash'])) {
                        $financial->paiements()->createMany($data['paiements']['cash']);
                    }
                    if (isset($data['paiements']['check'])) {
                        $financial->paiements()->createMany($data['paiements']['check']);
                    }
                    if (isset($data['paiements']['transfer'])) {
                        $financial->paiements()->createMany($data['paiements']['transfer']);
                    }
                    if (isset($data['paiements']['exchange'])) {
                        $financial->paiements()->createMany($data['paiements']['exchange']);
                    }
                    
                    $cart->update(['paiment_status' => $paiment_status]);
                    $cart->payments()->update(['created_by' => auth()->user()->name]);
                }
                /* Payment */

                DB::commit();
                return redirect()->route('admin.cart.index')->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return ($e->getLine() . $e->getMessage());
            }
        }
        public function delete(Cart $cart)
        {
            try {
                foreach ($cart->items as $key => $item) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id'=>$item->product_id
                        ]
                    );
                    $update = Helper::updateQuantity($cart, 'Suppression ', $item->product_quantity, $quantity, 'add');   
                }
                $cart->delete();
                return redirect()->route('admin.cart.index')->with('success', "Supprimé avec succès!");
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        public function get_pricing(Product $product, $price_type, $client_id)
        {
            switch ($price_type) {
                case 'detail':
                    $price = $product->selling1;
                    break;
                case 'semigros':
                    $price = $product->selling2;
                    break;
                case 'gros':
                    $price = $product->selling3;
                    break;
            }
            $pricing = ClientPricing::where(['product_id' => $product->id, 'client_id' => $client_id])->first();
            if ($pricing) {
                if ($pricing->date_cv && $pricing->date_cv >= date('Y-m-d')) {
                    $price = $pricing->price_cv;
                } else if ($pricing->discount) {
                    $price = $price * (1 - ($pricing->discount / 100));
                    return ['price' => $price, 'remise' => $pricing->discount];
                }
            }
            return ['price' => $price];
        }
        public function serach_product(Request $request, $prd, $price_type)
        {
            
            $products = Product::where('name', 'LIKE', '%' . $prd . '%')->whereHas('quantities', function ($query) {
                $query->where('store_id', auth()->user()->store_id);
            })->get();
            $client_id = $request->has('client_id') ? $request->query('client_id') : null;
            $html = '<div class="card-body"><div class="alert border-0 alert-dismissible fade show py-2">
        <div class="table-responsive"><table class="table align-middle table-striped">   <tbody></div>';
            if (count($products)) {
                foreach ($products as $product) {
                    $quantity = Quantity::where(['product_id' => $product->id, 'store_id' => auth()->user()->store_id])->sum('quantity');
                    switch ($price_type) {
                        case 'detail':
                            $price = $product->selling1;
                            break;
                        case 'semigros':
                            $price = $product->selling2;
                            break;
                        case 'gros':
                            $price = $product->selling3;
                            break;
                    }
                    $pricing = ClientPricing::where(['product_id' => $product->id, 'client_id' => $client_id])->first();
                    if ($pricing) {
                        if ($pricing->date_cv && $pricing->date_cv >= date('Y-m-d')) {
                            $price = $pricing->price_cv;
                        } else if ($pricing->discount) {
                            $price = $price * (1 - ($pricing->discount / 100));
                        }
                    }
                    $html .= '  <tr>
            <td>
            <td class="productlist">
              <a class="d-flex align-items-center gap-2" href="#">
                <div class="product-box">
                    <img src=' . $product->default_image_url . ' alt="">
                </div>
                <div>
                    <h6 class="mb-0 product-title">' . $product->name . '</h6>
                </div>
               </a>
            </td>';
                    $html .= '<td><span >' . $price . '  ' . $product->currency->name . '</span></td>';
                    $html .= '<td><span >' . $quantity . '  ' . $product->unity . '</span></td>';
                    if ($product->status == 1) {
                        $html .= '<td><span class="badge rounded-pill alert-success">Active</span></td>';
                    } else {
                        $html .= '<td><span class="badge rounded-pill alert-danger">Disabled</span></td>';
                    }
                    $html .= '<td><span>' . $product->updated_at . '</span></td>
            <td>
              <div class="d-flex align-items-center gap-3 fs-6">
              <input type="button" name="add_to_cart" id="' . $product->id . '" style="" class="btn btn-primary btn-sm form-control form-control-sm add_to_cart" value="Ajouter" />
              <input type="hidden"  id="name' . $product->id . '" value="' . $product->name . '" />
              <input type="hidden"  id="product_price_buying' . $product->id . '"  value="' . $product->buying . '"/>
              <input type="hidden"  id="product_price_selling' . $product->id . '"  value="' . $price . '"/>
              <input type="hidden"  id="quantity_id' . $product->id . '"  value="0"/>
              <input type="hidden"  id="product_unity' . $product->id . '"  value="' . $product->unity . '"/>
              <input type="hidden"  id="product_tva' . $product->id . '"  value="' . $product->tva . '"/>
              <input type="hidden"  id="product_sku' . $product->id . '"  value="' . $product->sku . '"/>
              <input type="hidden"  id="currency' . $product->id . '"  value="' . $product->currency->name . '"/>
              <input type="hidden"  id="currency_id' . $product->id . '"  value="' . $product->currency->id . '"/>
              <input type="hidden"  id="price_type' . $product->id . '"  value="' . $product->price_type['type'] . '"/>
            </div>
            </td>
          </tr>';
                }
                $html .= '</tbody></table></div><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
            } else {
                $html = Null;
            }
            return $html;
        }
        
        public function serach_product_c_bar($prd, Request $request)
        {
            $product = Product::where('sku', $prd)->orWhere('reference', $prd)->first();
            if ($product) {
                $product->load('pricings');
                $quantity = Quantity::where('product_id', $product->id)->first();
                $price = 0;
                
                $client_id = $request->has('client_id') ? $request->query('client_id') : null;
                $price_type = $request->has('price_type') ? $request->get('price_type') : 'detail';
                $pricing = ClientPricing::where(['product_id' => $product->id, 'client_id' => $client_id])->first();
                // dd($price_type);
                switch ($price_type) {
                    case 'detail':
                        $price = $product->selling1;
                        break;
                    case 'semigros':
                        $price = $product->selling2;
                        break;
                    case 'gros':
                        $price = $product->selling3;
                        break;
                }
                if ($pricing) {
                    if ($pricing->date_cv && $pricing->date_cv >= date('Y-m-d')) {
                        $price = $pricing->price_cv;
                    } else if ($pricing->discount) {
                        $price = $price * (1 - ($pricing->discount / 100));
                    }
                }
                return [$product, $quantity?->quantity ? $quantity->quantity : 0, $price, $product->currency->name];
        }
            return null;
        }
        
        public function get_product_price($prd, $client_id, $price_type, $attr1, $attr2 = '0')
        {
            $new_price = null;
            if ($attr2 == '0') {
                $attr2 = NULL;
            }
            if ($attr2 == 'undefined') {
                $attr2 = NULL;
            }
            switch ($price_type) {
                case 'detail':
                    $price = Price::select('selling1 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                    $price = $price ? $price : Price::select('selling1 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                    break;
                case 'semigros':
                    $price = Price::select('selling2 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                    $price = $price ? $price : Price::select('selling2 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                    break;
                case 'gros':
                    $price = Price::select('selling3 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                    $price = $price ? $price : Price::select('selling3 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                    break;
            }
            
            $new_price = $price->price;
            
            $pricing = ClientPricing::where(['product_id' => $prd, 'client_id' => $client_id])->first();
            if ($pricing) {
                if ($pricing->date_cv && $pricing->date_cv >= date('Y-m-d')) {
                    $new_price = $pricing->price_cv;
                } else if ($pricing->discount) {
                    $new_price = $new_price * (1 - ($pricing->discount / 100));
                }
            }
            
            return $new_price;
        }
    }
