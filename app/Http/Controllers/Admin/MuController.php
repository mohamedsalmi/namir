<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Http\Requests\CreateMuRequest;
use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Models\Barcode;
use App\Models\Mu;
use App\Models\Price;
use App\Models\Product;
use App\Models\Quantity;
use App\Models\Store;
use App\Notifications\MinQuantityNotification;
use Carbon\Carbon;
use Faker\Core\Number;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use NumberFormatter;
use PhpParser\JsonDecoder;
use Yajra\DataTables\Facades\DataTables;

class MuController extends Controller
{

    public function index(Request $request,$type = 'bs')
    {    

        $carts = Mu::query();
        if ($request->ajax()) {
        if (!in_array($type, ['br', 'bs', 'bcr', 'bc'])) return redirect()->back();
        if ($type == 'bs') {
            $carts ->where(['store_origin' => auth()->user()->store_id, 'type' => NULL]);
        } elseif ($type == 'br') {
            $carts ->where(['store_destination' => auth()->user()->store_id, 'type' => NULL]);
        } elseif ($type == 'bcr') {
            $carts ->where(['store_destination' => auth()->user()->store_id, 'type' => 'bc']);
        } else {
            $carts ->where(['store_origin' => auth()->user()->store_id, 'type' => 'bc']);
        }
        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
            $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
        }
        if ($request->has('client') && $request->query('client') != '') {
            $carts->where('client_id', $request->query('client'));
        }
        return DataTables::eloquent($carts)
            ->editColumn('date', function ($row) {
                $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                return $formattedDate;
            })
            ->editColumn('created_at', function ($row) {
                $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                return $formattedDate;
            })
            ->editColumn('status', function ($row) {                 
                if ($row->status == 0) {
                    $status = '<td><span class="badge rounded-pill alert-danger">En cours</span></td>';
                } else {
                    $status = '<td><span class="badge rounded-pill alert-success">Reçu </span></td>';
                }               
                 return $status;
            })
            ->addColumn('storeDestination', function ($row) {
                $storeDestination = '<div class="d-flex align-items-center gap-3 cursor-pointer"> <div class="">';                   
                    if($row->store_destination == auth()->user()->store_id){
                    $storeDestination .= '<p class="mb-0">'.$row->storeOrigin->name.'</p>';}
                    else{
                    $storeDestination .= '<p class="mb-0">'.$row->storeDestination->name .'</p>';}
                    $storeDestination .='</div>  </div>';
          
                return $storeDestination;
            })
            ->addColumn('check', function ($row) {
                $check = ' <div class="form-check">';
                if ($row->invoice_id == null) {
                    $check .= '<input name="cart_id[]" value="' . $row->id . '"
                    class="form-check-input" type="checkbox"
                    form="create-invoice-form">';
                }
                $check .= '</div>';
                return $check;
            })
            ->addColumn('action', function ($row) use($type) {
                $showBtn = auth()->user()->can('Détails bon de sortie', 'Détails bon de commande', 'Détails bon de réception') ? ' <a href="'.route('admin.mu.show', $row->id) .'" class="text-primary"
                data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                data-bs-original-title="View detail" aria-label="Views"><i
                    class="bi bi-eye-fill"></i></a>' : '';
                $updateBtn = auth()->user()->can('Modifier bon de réception') ? '<a href="'.route('admin.mu.edit',['mu' => $row->id, 'type' => $type]) .'" class="text-warning"
                    data-bs-toggle="tooltip" data-bs-placement="bottom"
                    title="Modifier" data-bs-original-title="Modifier"
                    aria-label="Modifier"><i class="bi bi-pencil"></i></a>' : '';
                $updatestatusBtn = auth()->user()->can('Modifier bon de réception') ? '<a href="'.route('admin.mu.updatestatus',[$row->id,$type]) .'" class="text-success"
                data-bs-toggle="tooltip" data-bs-placement="bottom"
                title="" data-bs-original-title="confirmer"
                aria-label="confirmer"><i class="lni lni-checkbox"></i></a>' : '';
                $printBtn = auth()->user()->can('Imprimer bon de sortie', 'Imprimer bon de commande', 'Imprimer bon de réception') ? '<a href="'. route('admin.mu.print',$row->id).'"
                class="text-primary" data-bs-toggle="tooltip"
                data-bs-placement="bottom" title="" target="blank"
                data-bs-original-title="Imprimer" aria-label="Views"><i
                    class="bi bi-printer-fill text-black"></i></a>' : '';
                $createBsFromBcBtn=auth()->user()->can('Modifier bon de réception') ? ' <a href="'.route('admin.mu.create',['commande'=>$row->id,'bs']) .'" class="text-success"
                data-bs-toggle="tooltip" data-bs-placement="bottom"
                title="" data-bs-original-title="confirmer"
                aria-label="confirmer"><i class="lni lni-checkbox"></i></a>':'';
                $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                if(($row->status == 0) && ($type == 'bs')){
                    $btns .= $updateBtn;
                }
                if(($row->status == 0) && ($type == 'br')){
                    $btns .= $updatestatusBtn;
                }
                if(($row->status == 0) && ($type == 'bcr'))
                {
                    $btns .= $createBsFromBcBtn ;
                }
                $btns .=  $printBtn . '</div>';

                return $btns;
            })
            ->rawColumns(['date','action', 'check',  'status','storeDestination'])
      
            ->make(true);
    }
        return view('admin.mu.index',compact('type'));
    }
    public function indexadmin()
    {
        $mus = Mu::all();
        return view('admin.mu.index', compact('mus'));
    }

    public function show(Mu $mu)
    {
        $muitems = $mu->items;
        return view('admin.mu.show', compact('mu', 'muitems'));
    }
    public function muvalidation(Request $request)
    {
        $validated = $request->validate([
            'date' => 'required',
            'store_destination' => 'required',
            'items' => 'required|array|min:1',
        ]);
        // dd($validated);

        return response()->json($validated);
    }
    public function create(Request $request, $type = 'bs')
    {
        if (!in_array($type, ['bc', 'bs'])) return redirect()->back();
        $store = Store::whereNot('id', auth()->user()->store_id)->get()->pluck('name', 'id');
        $mu = null;
        if ($request->has('commande') && $request->query('commande') != '') {
            $mu = Mu::find($request->query('commande'));
        }
        $products = array();
        return view('admin.mu.create', compact('store', 'type', 'mu', 'products'));
    }
    public function store(Request $request, $type = 'bs')
    {
        if (!in_array($type, ['bc', 'bs'])) return redirect()->back();
        try {
            DB::beginTransaction();
            $data = $request->all();
            /****** */
            $codification_setting = 'Codification stock';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            $data['codification'] = $nextCodification;
            /********** */

            if (isset($data['items'])) {
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['status'] = 0;
                $data['store_origin'] = auth()->user()->store->id;
                $data['user_id'] = auth()->user()->id;
                $data['created_by'] = auth()->user()->name;
                $mu = Mu::create($data);
                
                foreach ($data['items'] as $key => $item) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id' => $item['product_id']
                        ]
                    );
                    $data['items'][$key]['quantity_id'] = $quantity->id ?? null;
                    if ($type == 'bc') {
                        $data['type'] = 'bc';
                    } else {
                        Helper::updateQuantity($mu, 'Création ', $item['product_quantity'], $quantity, 'remove');
                    }
                }
                
                $mu->items()->createMany($data['items']);
                DB::commit();
                if ($request->has('savewithprint')) {
                    $pdf = Helper::print2($mu, 'mu');
                    return $pdf->stream('mu.pdf');
                }

                return redirect()->route('admin.mu.index', $type)->with('success', "Ajouté avec succès!");
            } else {
                return redirect()->route('admin.mu.create')->with('error', "Ajouter les produits!");
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            Log::error($e->getLine());
            return ($e->getMessage());
        }
    }
    public function print(Mu $mu)
    {
        Helper::print2($mu, 'mu');
    }
    
    public function edit(Request $request, Mu $mu)
    {
        $type = $request->get('type') ?? null;
        if (!in_array($type, ['bs'])) return redirect()->route('admin.mu.index')->with('error', "Modification interdite!");
        $store = Store::whereNot('id', $mu->store_origin)->get()->pluck('name', 'id');
        return view('admin.mu.edit',  compact('store', 'mu',));
    }

    public function update(Request $request, Mu $mu)
    {
        try {
            if($mu->status == 1){
                return redirect()->route('admin.mu.index')->with('error', "Modification échouée! Bon de sortie déjà accepté!");
            }
            DB::beginTransaction();
            $data = $request->all();

            $number = count($data["items"]);
            $data['number'] = $number;
            $data['store_origin'] = $mu->store_origin;
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;
            $data['status'] = 0;
            $mu->update($data);

            foreach ($mu->items as $item) {
                if ($mu->type == 'bc') {
                    $data['type'] = 'bc';
                } else {
                    $quantity = Quantity::find($item->quantity_id);
                    $update = Helper::updateQuantity($mu, 'Modification ', $item->product_quantity, $quantity, 'add');
                }
            }

            foreach ($data['items'] as $key => $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => $data['store_origin'],
                        'product_id'=>$data['items'][$key]['product_id']
                    ]
                );
                $data['items'][$key]['quantity_id'] = $quantity->id ?? null;
                if ($mu->type == 'bc') {
                    $data['type'] = 'bc';
                } else {
                    Helper::updateQuantity($mu, 'Modification ', $item['product_quantity'], $quantity, 'remove');
                }
            }
            
            $mu->items()->delete();
            $mu->items()->createMany($data['items']);

            DB::commit();
            return redirect()->route('admin.mu.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            $error = "File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage();
            Log::error($error);
            return ($error);
        }
    }
    public function delete(Mu $mu)
    {
    }

    public function updatestatus(Mu $mu, $type)
    {
        if (!in_array($type, ['br'])) return redirect()->back();
        try {
            DB::beginTransaction();
            $mu->update(['status' => 1]);
            foreach ($mu->items as $item) {
                if ($mu->type == 'bc') {
                    $data['type'] = 'bc';
                } else {
                    $origin_quantity = Quantity::findOrFail($item->quantity_id, 
                        [
                            'product_attribute1',
                            'product_value1',
                            'product_id'
                        ]
                    );
                    
                    $destination_quantity = $origin_quantity->replicate();
                    $destination_quantity->store_id = $mu->store_destination;
                    $destination_quantity= $destination_quantity->toArray();
                    
                    $quantity = Quantity::firstOrCreate($destination_quantity, $destination_quantity);                    
                    $update = Helper::updateQuantity($mu, 'Modification ', $item->product_quantity, $quantity, 'add');
                }
            }
            DB::commit();
            return redirect()->route('admin.mu.index', $type)->with('success', "Effectué avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return ($e->getMessage());
        }
    }
}
