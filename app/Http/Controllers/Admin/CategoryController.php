<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    protected Collection $categoriesCollection;

    public function __construct()
    {
        $this->categoriesCollection = Collection::make();
    }

    public function tree($category){
        $this->categoriesCollection->push($category);
        
        if ($category->children()->exists()){
            foreach ($category->children as $key => $sub){
                $data = $this->tree($sub);
                $this->categoriesCollection->push($data);
            }
        }
    }

    /**
     * Show the list of available categories.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::where('parent_id', null)->get();
            foreach ($data as $row) {
                $this->tree($row);
            }
            $result = $this->categoriesCollection->filter()->values();
            // dd($result);
            return DataTables::of($result)
                ->editColumn('name', function ($row) {
                    $prefix = '';
                    $category = Category::find($row['id']);
                    if ($category->parent()->exists()){
                        for ($i = 1; $i < $category->level; $i++){
                            $prefix .= '➧';
                        }
                    } 
                    return $prefix . $category->name;
                })
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row['updated_at'])->format('d/m/y');
                    return $formattedDate;
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier catégorie') ? '<a href="' . route('admin.category.edit', $row['id']) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer catégorie') ? '<form method="POST" action="' . route('admin.category.delete', $row['id']) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                    $updateBtn . $confirmDeletion
                    . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'name'])

                ->make(true);
        }
        return view('admin.category.list');
    }

    public function create()
    {
        $categories = Category::where('parent_id', null)->get();
        return view('admin.category.create', compact('categories'));
    }

    public function store(CreateCategoryRequest $request)
    {
        try {
            $data = $request->all();
            if (isset($data['parent_id'])) {
                $parent = Category::find($data['parent_id']);
                $data['level'] = $parent->level + 1;
            }
            $category = Category::create($data);
            return redirect()->route('admin.category.list')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Category $category)
    {
        $categories = Category::where('parent_id', null)->get();
        return view('admin.category.edit', compact(['categories', 'category']));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        try {
            $data = $request->all();
            if (isset($data['parent_id'])) {
                $parent = Category::find($data['parent_id']);
                $data['level'] = $parent->level + 1;
            }
            $category->update($data);
            return redirect()->route('admin.category.list')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Category $category)
    {
        try {
            $category->delete();
            return redirect()->route('admin.category.list')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
