<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProviderRequest;
use App\Http\Requests\UpdateProviderRequest;
use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        $providers = Provider::all();
        return view('admin.provider.index', compact('providers'));
    }

    public function create()
    {

        return view('admin.provider.create');
    }
    public function store(CreateProviderRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $provider = Provider::create($data);
            if ($request->file('avatar')) {
                $path = $this->addAttachment($request->file('avatar'));
                $provider->image()->create(['url' => $path]);
            }
            DB::commit();
            return redirect()->route('admin.provider.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function addAttachment($file)
    {
        $filePath = 'uploads/providers/images/';
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFilename = $filename . '-' . uniqid();
        $fullPath = $filePath . $newFilename . '.' . $extension;

        if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
            $path = Storage::url($fullPath);
        }

        return $path;
    }

    public function deleteAttachment($path)
    {
        if (isset($path)) {
            if ($path != '') {
                $fileuri = str_replace('/storage', '', $path);
                Storage::disk('public')->delete($fileuri);
            }
        }
    }

    public function edit(Provider $provider)
    {
        return view('admin.provider.edit', compact('provider'));
    }

    public function update(UpdateProviderRequest $request, Provider $provider)
    {
        try {
            $data = $request->all();
            if ($request->file('avatar')) {
                if($provider->image()->exists()){
                    $this->deleteAttachment($provider->image->url);
                    $provider->image()->delete();
                }
                $path = $this->addAttachment($request->file('avatar'));
                $provider->image()->create(['url' => $path]);
            }
            $provider->update($data);
            return redirect()->route('admin.provider.index')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Provider $Provider)
    {
        try {
            $Provider->delete();
            return redirect()->route('admin.provider.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
