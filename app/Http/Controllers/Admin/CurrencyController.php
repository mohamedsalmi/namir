<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCurrencyRequest;
use App\Http\Requests\UpdateCurrencyRequest;
use App\Models\Currency;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CurrencyController extends Controller
{
    /**
     * Show the list of available currencies.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Currency::query();
            return DataTables::eloquent($data)
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                    return $formattedDate;
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier devise') ? '<a href="' . route('admin.currency.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer devise') ? '<form method="POST" action="' . route('admin.currency.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';
                    $btns ='';
                    if($row->name !=  'Dinar Tunisien'){
                        $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';
                    }

                    return $btns;
                })
                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.currency.list');
    }

    public function create()
    {
        $currencies = Currency::pluck('name', 'id');
        return view('admin.currency.create', compact('currencies'));
    }

    public function store(CreateCurrencyRequest $request)
    {
        try {
            $data = $request->all();
            $currency = Currency::create($data);
            return redirect()->route('admin.currency.list')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Currency $currency)
    {
        return view('admin.currency.edit', compact('currency'));
    }

    public function update(UpdateCurrencyRequest $request, Currency $currency)
    {
        try {
            DB::beginTransaction();

            $data = $request->all();
            // dd($data);
            if (isset($data['price_update'])) {
                $extchange_rate=$data['value']/$currency->value-1;
                $products=Product::where('currency_id',$currency->id)->get();
              foreach ($products as $key => $product) {
                if (isset($data['buying'])) {
                $buying=$product->buying*(1+$extchange_rate);}else{
                    $buying=$product->buying;
                }
                if (isset($data['selling1'])) {
                $selling1=$product->selling1*(1+$extchange_rate);}else{
                    $selling1=$product->selling1;
                }
                if (isset($data['selling2'])) {
                $selling2=$product->selling2*(1+$extchange_rate);}else{
                    $selling2=$product->selling2;
                }
                if (isset($data['selling3'])) {
                $selling3=$product->selling3*(1+$extchange_rate);}else{
                    $selling3=$product->selling3;
                }
                $product->update(['buying'=>$buying , 'selling1'=>$selling1, 'selling2'=>$selling2, 'selling3'=>$selling3]);
              }
            }
            $currency->update($data);
            DB::commit();

            return redirect()->route('admin.currency.list')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Currency $currency)
    {
        try {
            $currency->delete();
            return redirect()->route('admin.currency.list')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    } 
     public function setCurrency(Request $request)
    {
        $currency = Currency::find($request->currencyId);
        if ($currency) {
            session()->put('currency', $currency);
            session()->put('currency_value', $currency->value);                                    

        }
        
        return response()->json(['status' => 'success']);
    }
}
