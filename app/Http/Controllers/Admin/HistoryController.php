<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartsItems;
use App\Models\FinancialCommitment;
use App\Models\OnlineSale;
use App\Models\PaymentCart;
use App\Models\ReturnNote;
use App\Models\SaleQuote;
use App\Models\User;
use App\Models\Voucher;
use App\Models\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HistoryController extends Controller
{
    public function history(Request $request )
    {
        $now = Carbon::now();
        $year=$now->year;
        if ($request->has('year') && $request->query('year') != '') {
            $year =$request->query('year');
        }
        $month=$now->month;
        if ($request->has('month') && $request->query('month') != '') {
            $month =$request->query('month');
        }
        $recettes= $this->getCartsTotalAmountsByYearAndMonth($year);
        $result = Cart::select(DB::raw('YEAR(date) as year'))->distinct()->get();
        $years = $result->pluck('year');
        // $result = Cart::select(DB::raw('month(date) as month'))->distinct()->get();
        // $months = $result->pluck('month');
        $months = array(1, 2, 3, 4, 5, 6,7,8,9,10,11,12);
        $daylilies=$this->getCartsTotalAmountsByYearAndMonthAndDay($year,$month);
        return view('admin.history.history', compact('months','years','recettes','daylilies','year','month'));
    }
    public function historyOnline(Request $request)
    {
        $now = Carbon::now();
        $year = $now->year;
        if ($request->has('year') && $request->query('year') != '') {
            $year = $request->query('year');
        }
        $month = $now->month;
        if ($request->has('month') && $request->query('month') != '') {
            $month = $request->query('month');
        }
        $recettes = $this->getOnlineTotalAmountsByYearAndMonth($year);
        $result = OnlineSale::select(DB::raw('YEAR(date) as year'))->distinct()->get();
        $years = $result->pluck('year');
        // $result = Cart::select(DB::raw('month(date) as month'))->distinct()->get();
        // $months = $result->pluck('month');
        $months = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        $daylilies = $this->getOnlineTotalAmountsByYearAndMonthAndDay($year, $month);
        return view('admin.history.historyonline', compact('months', 'years', 'recettes', 'daylilies', 'year', 'month'));
    }
    public function getCartsTotalAmountsByYearAndMonth($year)
    {
        
        $data = Cart::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total')
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $voucher = Voucher::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total')
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $salequote = SaleQuote::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total')
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $paiement=PaymentCart::select(
            DB::raw('year(received_at) as year'),
            DB::raw('month(received_at) as month'),
            DB::raw('sum(amount) as amount')
        )
            ->where(DB::raw('year(received_at)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $returnnote = ReturnNote::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total')
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        // $items = CartsItems::select(
        //     DB::raw('year(date) as year'),
        //     DB::raw('month(date) as month'),
        //     DB::raw('product_price_buying '),
        //     DB::raw('product_quantity'),
        // )
        //     ->where(DB::raw('year(date)'), '=', $year)
        //     ->get();
           
        $array = [];

        for ($month = 1; $month <= 12; $month++) {
            // $totalbuying=0;
            // foreach ($items as $key => $item) {
            //     if($item->month == $month  && $item->year == $year){
            //         $totalbuying+=$item->product_price_buying * $item->product_quantity;
            //     }
            // }
            $array[] = [
                'year' => $year,
                'month' => $month,
                'total' => optional($data->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'total_salequote' => optional($salequote->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'totalreturnnote' => optional($returnnote->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'totalbuying' => optional($voucher->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'paiement' => optional($paiement->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->amount ?? 0,
            ];
        }

        return $array;
    }
    public function getCartsTotalAmountsByYearAndMonthAndDay($year,$month)
    {
        
        $data = Cart::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $voucher = Voucher::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $salequote = SaleQuote::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
            $paiement=PaymentCart::select(
                DB::raw('year(received_at) as year'),
                DB::raw('month(received_at) as month'),
                DB::raw('day(received_at) as day'),
                DB::raw('sum(amount) as amount'),
            )
                ->where(DB::raw('year(received_at)'), '=', $year)
                ->groupBy('year')
                ->groupBy('month')
                ->groupBy('day')
                ->get();
        $returnnote = ReturnNote::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        // $items = CartsItems::select(
        //     DB::raw('year(date) as year'),
        //     DB::raw('month(date) as month'),
        //     DB::raw('day(date) as day'),
        //     DB::raw('product_price_buying '),
        //     DB::raw('product_quantity'),
        // )
        //     ->where(DB::raw('year(date)'), '=', $year)
        //     ->where(DB::raw('month(date)'), '=', $month)
        //     ->get();
           
        $array = [];
          $daynumber=Carbon::now()->month($month)->daysInMonth;

        for ($day = 1; $day <= $daynumber; $day++) {
            // $totalbuying=0;
            // foreach ($items as $key => $item) {
            //     if($item->day == $day  && $item->month == $month && $item->year == $year){
            //         $totalbuying+=$item->product_price_buying * $item->product_quantity;
            //     }
            // }
            $array[] = [
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'total' => optional($data->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'total_salequote' => optional($salequote->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'totalreturnnote' => optional($returnnote->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'totalbuying' => optional($voucher->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'paiement' => optional($paiement->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->amount ?? 0,

            ];
        }

        return $array;
    }
    public function getOnlineTotalAmountsByYearAndMonth($year)
    {
        
        $success = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'livrée')
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $return = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'retour')
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $ready = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'en attente')
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $canceled = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'annulée')
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        $progress = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'en cours')
            ->where(DB::raw('year(date)'), '=', $year)
            ->groupBy('year')
            ->groupBy('month')
            ->get();
        // $items = CartsItems::select(
        //     DB::raw('year(date) as year'),
        //     DB::raw('month(date) as month'),
        //     DB::raw('product_price_buying '),
        //     DB::raw('product_quantity'),
        // )
        //     ->where(DB::raw('year(date)'), '=', $year)
        //     ->get();
        
        $array = [];
        
        for ($month = 1; $month <= 12; $month++) {
            // $totalbuying=0;
            // foreach ($items as $key => $item) {
            //     if($item->month == $month  && $item->year == $year){
            //         $totalbuying+=$item->product_price_buying * $item->product_quantity;
            //     }
            // }
            $array[] = [
                'year' => $year,
                'month' => $month,
                'success' => optional($success->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'canceled' => optional($canceled->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'ready' => optional($ready->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'progress' => optional($progress->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'return' => optional($return->where(fn ($row) => $row->month == $month && $row->year == $year)->first())?->total ?? 0,
            ];
        }
        
        return $array;
    }
    public function getOnlineTotalAmountsByYearAndMonthAndDay($year, $month)
    {
        
        $success = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'livrée')
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $progress = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'en cours')
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $ready = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'en attente')
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $canceled = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'annulée')
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        $return = OnlineSale::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('day(date) as day'),
            DB::raw('sum(total) as total'),
        )
            ->where(DB::raw('status'), '=', 'retour')
            ->where(DB::raw('year(date)'), '=', $year)
            ->where(DB::raw('month(date)'), '=', $month)
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('day')
            ->get();
        // $items = CartsItems::select(
        //     DB::raw('year(date) as year'),
        //     DB::raw('month(date) as month'),
        //     DB::raw('day(date) as day'),
        //     DB::raw('product_price_buying '),
        //     DB::raw('product_quantity'),
        // )
        //     ->where(DB::raw('year(date)'), '=', $year)
        //     ->where(DB::raw('month(date)'), '=', $month)
        //     ->get();
        
        $array = [];
        $daynumber = Carbon::now()->month($month)->daysInMonth;
        
        for ($day = 1; $day <= $daynumber; $day++) {
            // $totalbuying=0;
            // foreach ($items as $key => $item) {
            //     if($item->day == $day  && $item->month == $month && $item->year == $year){
            //         $totalbuying+=$item->product_price_buying * $item->product_quantity;
            //     }
            // }
            $array[] = [
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'success' => optional($success->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'canceled' => optional($canceled->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'ready' => optional($ready->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'progress' => optional($progress->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,
                'return' => optional($return->where(fn ($row) => $row->day == $day && $row->month == $month && $row->year == $year)->first())?->total ?? 0,

            ];
        }
        
        return $array;
    }

}
