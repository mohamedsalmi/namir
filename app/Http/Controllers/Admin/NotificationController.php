<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function index()
    {
        return view('admin.notification.index');
    }

    public function markAsRead(DatabaseNotification $notification)
    {
        try {
            $notification->markAsRead();
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json(['success' => false], 500);
        }
        return response()->json(['success' => true]);
    }

    public function markAllAsRead(User $user)
    {
        try {
            foreach ($user->unreadNotifications->where('type', '!=', 'App\Notifications\NewWebsiteOrderNotification') as $notification) {
                $notification->markAsRead();
            }
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json(['success' => false], 500);
        }
        return response()->json(['success' => true]);
    }

    public function markOrdersAsRead(User $user)
    {
        dd('test');
        try {
            foreach ($user->unreadNotifications->where('type', 'App\Notifications\NewWebsiteOrderNotification') as $notification) {
                $notification->markAsRead();
            }
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json(['success' => false], 500);
        }
        return response()->json(['success' => true]);
    }
}
