<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateSettingRequest;
use App\Http\Requests\UpdateSettingsRequest;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Redirect;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $settings = Setting::all();
        return view('admin.setting.index', compact('settings'));
    }

    public function updateSettings(UpdateSettingsRequest $request)
    {
        $documents = [
            'Codification BL' => 'Cart',  'Codification devis' => 'SaleQuote',  'Codification BR' => 'ReturnCoupon',  'Codification commande' => 'Order',  'Codification facture achat' => 'Bill',  'Codification facture avoir achat' => 'BillCreditNote',  'Codification facture avoir' => 'CreditNote',
            'Codification avoir financière' => 'FinancialCreditNote',  'Codification inventaire' => 'Inventory',  'Codification facture' => 'Invoice',  'Codification stock' => 'Mu',  'Codification bon de retour BL' => 'ReturnNote',  'Codification BA' => 'Voucher', 'Codification vente en ligne' => 'OnlineSale'
        ];
        try {
            DB::beginTransaction();
            $data = $request->all();
            $errors = [];
            foreach ($data['settings'] as $key => $item) {
                if (str_contains($item['name'], 'Codification ')) {
                    $nameSpace = '\\App\Models\\';
                    $model = $nameSpace . $documents[$item['name']];
                    $lastRecord = $model::whereNotNull('id')->latest()->first();
                    if ($lastRecord) {
                        $codification = $lastRecord->codification;
                        if ($item['value'] < $codification) {
                            $errors['settings.' . $key . '.value'] = ['Vous devez saisir une valeur supérieure ou égales à ' . $lastRecord->codification];
                        }
                    }
                }
                Setting::where('name', $item['name'])->update($item);
            }

            if (count($errors) > 0) {
                return Redirect::back()->withErrors(new MessageBag($errors))->withInput($request->only('settings'));
            }
            DB::commit();
            return redirect()->back()->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
        }
    }
    
    public function updateSetting(UpdateSettingRequest $request, Setting $setting)
    {
        $documents = [
            'Codification BL' => 'Cart',  'Codification devis' => 'SaleQuote',  'Codification BR' => 'ReturnCoupon',  'Codification commande' => 'Order',  'Codification facture achat' => 'Bill',  'Codification facture avoir achat' => 'BillCreditNote',  'Codification facture avoir' => 'CreditNote',
            'Codification avoir financière' => 'FinancialCreditNote',  'Codification inventaire' => 'Inventory',  'Codification facture' => 'Invoice',  'Codification stock' => 'Mu',  'Codification bon de retour BL' => 'ReturnNote',  'Codification BA' => 'Voucher'
        ];
        try {
            DB::beginTransaction();
            $data = $request->all();
            $errors = [];
                if (str_contains($data['name'], 'Codification ') && $data['value'] != $data['old_value']) {
                    $nameSpace = '\\App\Models\\';
                    $model = $nameSpace . $documents[$data['name']];
                    $lastRecord = $model::where('codification', $data['value'])->exists();
                    if ($lastRecord) {
                        $errors['value'] = ['Valeur déjà utilisée.'];
                    }
                }
                unset($data['old_value']);
                $setting->update($data);

            if (count($errors) > 0) {
                return response()->json(['errors' => new MessageBag($errors)], 422);
            }
            DB::commit();
            return response()->json(['sucess' => true]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getLine() . $e->getMessage() . $e->getFile());
        }
    }
}
