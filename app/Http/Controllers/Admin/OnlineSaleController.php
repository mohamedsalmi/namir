<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use App\Helpers\Helper;
    use App\Models\Client;
    use App\Models\OnlineSale;
    use App\Models\Product;
    use App\Models\Quantity;
    use App\Models\Region;
    use App\Notifications\MinQuantityNotification;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Yajra\DataTables\Facades\DataTables;
    
    class OnlineSaleController extends Controller
    {
        
        public function index(Request $request)
        {
            
            // $onlinesales = OnlineSale::where('store_id', auth()->user()->store_id)->get();
            $carts = OnlineSale::query();
            if ($request->ajax()) {
                $carts->where('store_id', auth()->user()->store_id);
                if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                    $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                }
                if ($request->has('client') && $request->query('client') != '') {
                    $carts->where('client_id', $request->query('client'));
                }
                return DataTables::eloquent($carts)
                    ->editColumn('date', function ($row) {
                        $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                        return $formattedDate;
                    })
                    ->editColumn('created_at', function ($row) {
                        $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('total', function ($row) {
                        $total = $row->total ;
                        return $total;
                    })
                    ->editColumn('status', function ($row) {
                        $status = '';
                        switch ($row->status) {
                            
                            case 'en attente':
                                $status .= '<span class="badge bg-primary ">' . $row->status . '</span>';
                                break;
                            
                            case 'en cours':
                                $status .= '<span class="badge bg-info">' . $row->status . '</span>';
                                break;
                            
                            case 'livrée':
                                $status .= '<span class="badge bg-success">' . $row->status . '</span>';
                                break;
                            
                            case 'retour':
                                $status .= '<span class="badge bg-danger">' . $row->status . '</span>';
                                break;
                            
                            case 'annulée':
                                $status .= '<span class="badge bg-dark">' . $row->status . '</span>';
                                break;
                        }
                        
                        return $status;
                    })
                    ->addColumn('check', function ($row) {
                        $check = ' <div class="form-check">';
                        if ($row->invoice_id == null) {
                            $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input multi-check" type="checkbox"
                        form="create-invoice-form">';
                        }
                        $check .= '</div>';
                        return $check;
                    })
                    ->addColumn('client', function ($row) {
                        $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                        if ($row->client) {
                            // $client .= ' <img src="' . asset($row->client->image_url) . '"
                            // class="rounded-circle" width="44" height="44"
                            // alt="">';
                            $client .= '<p class="mb-0">'  . $row->client_details['name'] . '-' . $row->client_details['phone'] . '</p>';
                        } else {
                            // $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                            // class="rounded-circle" width="44" height="44"
                            // alt="">';
                            $client .= '<p class="mb-0">' . $row->client_details['name'] . '-' . $row->client_details['phone'] . '</p>';
                        }
                        $client .= '</div>';
                        return $client;
                    })
                    ->filterColumn('client', function ($query, $keyword) {
                        $query->whereHas('client', function ($q) use ($keyword) {
                            $q->where('name', 'LIKE', '%' . $keyword . '%');
                        })->orWhere('client_details->name', 'LIKE', '%' . $keyword . '%')->orWhere('client_details->phone', 'LIKE', '%' . $keyword . '%');
                    })
                    ->orderColumn('client', function($query, $dir) {
                        $query->orderBy('client_details->name', $dir);
                    })
                    ->addColumn('state', function ($row) {
                        $state =  $row->client_details['state'];
                        return $state;
                    })
                    ->addColumn('city', function ($row) {
                        $city =  $row->client_details['city'];
                        return $city;
                    })
                    ->addColumn('paiement_status', function ($row) {
                        if ($row->paiment_status == 0) {
                            $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                        } else {
                            $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $showBtn = auth()->user()->can('Détails bon de livraison') ? ' <a href="' . route('admin.onlinesale.show', $row->id) . '"
                    class="text-primary" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Voir les détails" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                        $updateBtn = auth()->user()->can('Modifier bon de livraison') ? '<a href="' . route('admin.onlinesale.edit', $row->id) . '"
                    class="text-warning" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Modifier" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>' : '';
                        $printBtn = auth()->user()->can('Imprimer bon de livraison') ? '<div class="btn-group">
                    <button type="button"
                        class="btn text-black dropdown-toggle-split"
                        data-bs-placement="bottom" title="" target="blank"
                        data-bs-original-title="Imprimer" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-printer-fill text-black"></i>
                        <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" style="">
                        <li><a href="' . route('admin.onlinesale.print', $row->id) . '"
                                class="dropdown-item" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title=""
                                target="blank" data-bs-original-title="Imprimer Bl"
                                aria-label="Views"><i
                                    class="bi bi-printer-fill text-black"></i> Imprimer
                            </a>
                        </li>
                        <li><a href="' . route('admin.onlinesale.printticket', $row->id) . '"
                                class="dropdown-item" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title=""
                                target="blank"
                                data-bs-original-title="Imprimer ticket"
                                aria-label="Views"><i
                                    class="bi bi-printer-fill text-black"></i> Imprimer
                                ticket</a>
                        </li>
                        </li>
                    </ul>
                </div>' : '';
                        $changestatusBtn = auth()->user()->can('Supprimer bon de livraison') ? ' <select class="changestatus form-select " name="status" id="status"
                data-id="' . $row->id . '">
                <option value="en attente"' . ($row->status == 'en attente' ? 'selected' : '') . '>en attente</option>
                <option value="en cours"' . ($row->status == 'en cours' ? 'selected' : '') . '>en cours</option>
                <option value="livrée"' . ($row->status == 'livrée' ? 'selected' : '') . '>livrée</option>
                <option value="retour"' . ($row->status == 'retour' ? 'selected' : '') . '>retour</option>
                <option value="annulée"' . ($row->status == 'annulée' ? 'selected' : '') . '>annulée</option>
                 </select>' : '';
                        $confirmDeletion = auth()->user()->can('Supprimer bon de livraison') ? '<form method="POST"
                    action="' . route('admin.onlinesale.delete', $row->id) . '"
                    class="delete_cart">
                    ' . csrf_field() . '
                    <input name="_method" type="hidden" value="GET">
                    <a href="javascript:;" id="show_confirm" type="submit"
                        class="text-danger show_confirm" data-bs-toggle="tooltip"
                        data-bs-placement="bottom" title=""
                        data-bs-original-title="Supprimer"
                        aria-label="Supprimer"><i
                            class="bi bi-trash-fill"></i></a>
                           </form>' : '';
                        $btns = '<div class="d-flex align-items-center gap-3 fs-6 status1' . $row->id . '">' . $showBtn;
                        if ($row->status != 'livrée' && $row->status != 'en cours') {
                            $btns .= $updateBtn . $confirmDeletion;
                        }
                        
                        $btns .=  $printBtn . '</div>';
                        if ($row->invoice_id != null) {
                            $showinvoiceBtn = auth()->user()->can('Détails facture') ? ' <a href="' . route('admin.invoice.show', $row->invoice_id) . '"
                        class="text-primary" data-bs-toggle="tooltip"
                        data-bs-placement="bottom" title=""
                        data-bs-original-title="Voir Facture" aria-label="Views"><i
                            class="bi bi-file-pdf-fill text-danger"></i></a>' : '';
                            $btns .= $showinvoiceBtn . '<span>' . $row->invoice->codification . '</span>';
                        }
                        $btns .= $changestatusBtn;
                        return $btns;
                    })
                    ->rawColumns(['action', 'check', 'client', 'status', 'paiement_status', 'state', 'city'])
                    ->withQuery('total', function ($filteredQuery) {
                        return $filteredQuery->sum('total');
                    })
                    ->make(true);
            }
            $sum = $carts->sum('total');
            
            return view('admin.onlinesale.index', compact('sum'));
        }
        public function show(OnlineSale $onlinesale)
        {
            return view('admin.onlinesale.show', compact('onlinesale'));
        }
        public function onlinesalevalidation(Request $request)
        {
            $validated = $request->validate([
                'client_id' => 'required',
                'client_details.name' => 'required',
                // 'client_details.state' => 'required',
                // 'client_details.city' => 'required',
                'date' => 'required',
                'total' => 'required|numeric|gte:0',
                'items' => 'required|array|min:1',
            ]);
            
            return response()->json($validated);
        }
        public function create(Request $request)
        {
            $order = null;
            $type = 'onlinesale';
            $clients = Client::all();
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            $products = array();
            return view('admin.cart.create', compact('clients', 'order', 'type', 'products', 'states', 'cities'));
        }
        public function getcities(Request $request)
        {
            $data = $request->all();
            $cities = Region::where('state', $data['state'])->groupBy('city')->get();
            
            return $cities;
        }
        
        public function store(Request $request)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                /* Start Generate onlinesale number */
                $codification_setting = 'Codification vente en ligne';
                $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
                
                $data['codification'] = $nextCodification;
                
                /* End Generate onlinesale number */
                if (isset($data['items'])) {
                    $number = count($data["items"]);
                    $data['number'] = $number;
                    $data['store_id'] = auth()->user()->store->id;
                    $data['user_id'] = auth()->user()->id;
                    $data['created_by'] = auth()->user()->name;
                    $data['status'] = 'livrée';
                    $data['paiment_status'] = 1;
                    $onlinesale = OnlineSale::create($data);
                    $onlinesale->items()->createMany($data['items']);
                    foreach ($data['items'] as $key => $item) {
                        $data['items'][$key]['possible_return'] = $data['items'][$key]['product_quantity'];
                        $qt = $data['items'][$key]['product_quantity'];
                        if ($qt > 0) {
                            $quantity=Quantity::firstOrCreate(
                                [
                                    'store_id' => auth()->user()->store_id,
                                    'product_id'=>$data['items'][$key]['product_id']
                                ]
                            );
                            $update = Helper::updateQuantity($onlinesale, 'Création ', $item['product_quantity'], $quantity, 'remove');
                        }
                        $data['items'][$key]['quantity_id'] = $quantity->id;
                    }
                    DB::commit();
                    
                    return redirect()->route('admin.onlinesale.index')->with('success', "Ajouté avec succès!");
                } else {
                    return redirect()->route('admin.onlinesale.create')->with('error', "Ajouter les produits!");
                }
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        
        public function print(OnlineSale $onlinesale)
        {
            $pdf = Helper::print($onlinesale, 'onlinesale');
            return $pdf->stream('venteenline.pdf');
        }
        
        public function printTicket(OnlineSale $onlinesale)
        {
            $pdf = Helper::printTicket($onlinesale, 'onlinesale');
            return $pdf->stream('ticket.pdf');
        }
        
        public function edit(OnlineSale $onlinesale)
        {
            $clients = Client::all();
            // $products = Product::all();
            $products = array();
            $type = 'onlinesale';
            $title = 'Vente en line';
            $cart = $onlinesale;
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            return view('admin.cart.edit', compact('type', 'title', 'cart', 'clients', 'products', 'states', 'cities'));
        }
        public function update(Request $request, OnlineSale $onlinesale)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['user_id'] = auth()->user()->id;
                $data['updated_by'] = auth()->user()->name;
                $onlinesale->update($data);
                foreach ($onlinesale->items as $key => $item) {
                    $quantity=Quantity::firstOrCreate(
                        [
                            'store_id' => auth()->user()->store_id,
                            'product_id'=>$item->product_id
                        ]
                    );
                    $update = Helper::updateQuantity($onlinesale, 'Modification ', $item->product_quantity, $quantity, 'add');
                }
                $onlinesale->items()->delete();
                $onlinesale->items()->createMany($data['items']);
                foreach ($data['items'] as $key => $item) {
                    $data['items'][$key]['possible_return'] = $data['items'][$key]['product_quantity'];
                    $qt = $data['items'][$key]['product_quantity'];
                    if ($qt > 0) {
                        $quantity=Quantity::firstOrCreate(
                            [
                                'store_id' => auth()->user()->store_id,
                                'product_id'=>$data['items'][$key]['product_id']
                            ]
                        );
                        $update = Helper::updateQuantity($onlinesale, 'Modification ', $item['product_quantity'], $quantity, 'remove');
                    }
                    $data['items'][$key]['quantity_id'] = $quantity->id;
                }
                DB::commit();
                return redirect()->route('admin.onlinesale.index')->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return ($e->getLine());
            }
        }
        public function delete(OnlineSale $onlinesale)
        {
            try {
                if ($onlinesale->status != 'en attente' && $onlinesale->status !='retour') {
                    foreach ($onlinesale->items as $key => $item) {
                        $quantity=Quantity::firstOrCreate(
                            [
                                'store_id' => auth()->user()->store_id,
                                'product_id'=>$item->product_id
                            ]
                        );
                        $update = Helper::updateQuantity($onlinesale, 'Suppression ', $item->product_quantity, $quantity, 'add');
                    }
                }
                $onlinesale->delete();
                return redirect()->route('admin.onlinesale.index')->with('success', "Supprimé avec succès!");
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        public function statuschange(OnlineSale $onlinesale, $status)
        {
            try {
                DB::beginTransaction();
                if ($status == 'livrée') {
                    $onlinesale->update(['paiment_status' => 1]);
                }elseif ($onlinesale->status) {
                    $onlinesale->update(['paiment_status' => 0]);
                }
                switch ($onlinesale->status) {
                    case 'en attente':
                        if ($status != 'annulée' && $status != 'retour') {
                            $action = 'remove';
                        }
                        break;
                    case 'en cours':
                        if ($status != 'livrée') {
                            $action = 'add';
                        }
                        break;
                    case 'livrée':
                        if ($status != 'en cours') {
                            $action = 'add';
                        }
                        break;
                    case 'retour':
                        if ($status != 'annulée' && $status != 'en attente') {
                            $action = 'remove';
                        }
                        break;
                    case 'annulée':
                        if ($status != 'en attente' && $status != 'retour') {
                            $action = 'remove';
                        }
                        break;
                }
                if (isset($action)) {
                    foreach ($onlinesale->items as  $item) {
                        $quantity=Quantity::firstOrCreate(
                            [
                                'store_id' => auth()->user()->store_id,
                                'product_id'=>$item->product_id
                            ]
                        );
                        $update = Helper::updateQuantity($onlinesale, 'Modification état (' . $onlinesale->status . ' -> ' . $status . ') ', $item->product_quantity, $quantity, $action);
                    }
                }
                
                $onlinesale->update(['status' => $status]);
                DB::commit();
                return $status;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    }
