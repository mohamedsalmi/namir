<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Jobs\QuantitiesInitialisation;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\Store;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class StoreController extends Controller
{
    /**
     * Show the list of available stores.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Store::query();
            return DataTables::eloquent($data)
                ->editColumn('manager_id', function ($row) {
                    $manager = $row->manager ? $row->manager->name : 'N/A';
                    return $manager;
                })
                ->editColumn('website_sales', function ($row) {
                    if ($row->website_sales) {
                        $html = '<span class="badge rounded-pill alert-success">Activé</span>';
                    } else {
                        $html = '<span class="badge rounded-pill alert-danger">Désactivé</span>';
                    }
                    return $html;
                })
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d/m/y');
                    return $formattedDate;
                })
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier point de vente') ? '<a href="' . route('admin.store.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer point de vente') ? '<form method="POST" action="' . route('admin.store.delete', $row->id) . '">'
                        . csrf_field() .
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'website_sales'])

                ->make(true);
        }
        return view('admin.store.index');
    }

    public function create()
    {
        $managers = User::whereHas('roles', function (Builder $query) {
            $query->where('name', 'manager');
        })->get();
        return view('admin.store.create', compact('managers'));
    }

    public function initProducts(Store $store)
    {
        try {
            DB::beginTransaction();
            $edition = Attribute::where('name', 'الطبعة')->first();

            $products = Product::all();
            foreach ($products as $key => $product) {
                $product->productAttributes()->create(['attribute_id' => $edition->id]);
                $product->productAttributes()->first()->values()->create(['attribute_value_id' => $edition->values()->first()->id]);

                $productAttribute1 = $product->productAttributes->where('attribute_id', $edition->id)->first();
                foreach ($productAttribute1->values as $key => $value1) {
                    $quantity = [
                        'product_attribute1' => $productAttribute1->id,
                        'product_value1' => $value1->id,
                        'store_id' => $store->id,
                    ];
                    $product->quantities()->create($quantity);
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function store(CreateStoreRequest $request)
    {
        try {
            $data = $request->all();
            $store = Store::create($data);
            if($store->website_sales){
                Store::whereNot('id', $store->id)->update(['website_sales' => false]); 
            }
            $products = Product::all();
            $chunks=$products->chunk(200);
            $chunks->toArray();
            foreach ($chunks as $key => $chunk) {
                QuantitiesInitialisation::dispatch($store,$chunk)->onQueue('quantities');   
            }     
            return redirect()->route('admin.store.list')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Store $store)
    {
        $managers = User::whereHas('roles', function (Builder $query) {
            $query->where('name', 'manager');
        })->get();
        return view('admin.store.edit', compact(['store', 'managers']));
    }

    public function update(UpdateStoreRequest $request, Store $store)
    {
        try {
            $data = $request->all();
            $store->update($data);
            if($store->website_sales){
                Store::whereNot('id', $store->id)->update(['website_sales' => false]); 
            }
            return redirect()->route('admin.store.list')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Store $store)
    {
        try {
            $store->delete();
            return redirect()->route('admin.store.list')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function storechange($store)
    {
        try {
            $user = User::where('id', auth()->user()->id);
            $user->update(['store_id' => $store]);
            return true;
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
