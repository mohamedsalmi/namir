<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Quantity;
use App\Models\ReturnCommitment;
use App\Models\ReturnCoupon;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class ReturnCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carts = ReturnCoupon::query();
        if ($request->ajax()) {
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('provider') && $request->query('provider') != '') {              
                    $carts->where('provider_id',$request->query('provider'));                
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total . 'DM';
                    return $total;
                })
                ->addColumn('provider', function ($row) {
                    $provider = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->provider) {
                        $provider .= ' <img src="' . asset($row->provider->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $provider .= '<p class="mb-0">' . $row->provider->code . '-' . $row->provider_details['name'] . '</p>';
                    } else {
                        $provider .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $provider .= '<p class="mb-0">' . $row->provider_details['name'] . '</p>';
                    }
                    $provider .= '</div>';
                    return $provider;
                })
                ->addColumn('status', function ($row) {
                    if ($row->paiment_status == 0) {
                        if ($row->payments()->exists()) {
                            $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                        } else {
                            $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                        }
                    } else {
                        $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails bon de retour') ? '  <a href="'.route('admin.returncoupon.show', $row->id) .'" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="View detail" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                    
                    $printBtn = auth()->user()->can('Imprimer bon de retour') ? ' <a href="'.route('admin.returncoupon.print', $row->id) .'"
                    class="btn text-black" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title="" target="blank"
                    data-bs-original-title="Imprimer" aria-label="Views"><i
                        class="bi bi-printer-fill text-black"></i></a>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;                     
                    $btns .= $printBtn . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'provider', 'status'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.returncoupon.index', compact('sum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $voucher = null;
        if ($request->has('voucher') && $request->query('voucher') != '') {
            $voucher = Voucher::find($request->query('voucher'));
        }
        return view('admin.returncoupon.create', compact('voucher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function returncouponvalidation(Request $request)
    {
        $validated = $request->validate([
            'provider_id' => 'required',
            'voucher_id' => 'required',
            'provider_details.name' => 'required',
            // 'provider_details.mf' => 'required',
            // 'provider_details.adresse' => 'required',
            // 'provider_details.phone' => 'required',
            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',

        ]);

        return response()->json($validated);
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            /******* */
            $codification_setting = 'Codification BR';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            
            $data['codification'] = $nextCodification;
            /******* */
            $data['number'] = $number;
            $data['store_id'] = auth()->user()->store->id;
            $data['user_id'] = auth()->user()->id;
            $data['created_by'] = auth()->user()->name;

            $returncoupon = ReturnCoupon::create($data);

            foreach ($data['items'] as $key => $item) {
                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $data['items'][$key]['product_id']
                    ]
                );
                $update = Helper::updateQuantity($returncoupon, 'Création ', $data['items'][$key]['product_quantity'], $quantity, 'remove');
            }
            $returncoupon->items()->createMany($data['items']);

            DB::commit();
            return redirect()->route('admin.returncoupon.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReturnCoupon $returncoupon)
    {
        return view('admin.returncoupon.show', compact('returncoupon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print(ReturnCoupon $returncoupon)
    {
        $pdf = Helper::printPurchase($returncoupon, 'returncoupon');
        return $pdf->stream('returncoupon.pdf');
    }
}
