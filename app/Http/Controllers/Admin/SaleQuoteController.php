<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Barcode;
use App\Models\SaleQuote;
use App\Models\Client;
use App\Models\FinancialCommitment;
use App\Models\Order;
use App\Models\Price;
use App\Models\Product;
use App\Models\Quantity;
use App\Notifications\MinQuantityNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class SaleQuoteController extends Controller
{

    public function index(Request $request)
    {
        $carts = SaleQuote::query();
        if ($request->ajax()) {
            $carts->where('store_id', auth()->user()->store_id);

            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('client') && $request->query('client') != '') {
                $carts->where('client_id', $request->query('client'));
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total;
                    return $total;
                })
                ->addColumn('check', function ($row) {
                    $check = ' <div class="form-check">';
                    if ($row->invoice_id == null) {
                        $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input" type="checkbox"
                        form="create-invoice-form">';
                    }
                    $check .= '</div>';
                    return $check;
                })
                ->addColumn('client', function ($row) {
                    $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->client) {
                        $client .= ' <img src="' . asset($row->client->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client->code . '-' . $row->client_details['name'] . '</p>';
                    } else {
                        $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client_details['name'] . '</p>';
                    }
                    $client .= '</div>';
                    return $client;
                })
                ->filterColumn('client', function($query, $keyword) {
                    $query->whereHas('client', function($q) use ($keyword){
                        $q->where('client_code', 'LIKE', '%' . $keyword . '%')->orWhere( 'name', 'LIKE', '%' . $keyword . '%');
                    })
                    ->orWhere('client_details->name', 'LIKE', '%' . $keyword . '%');
                })
                ->orderColumn('client', function($query, $dir) {
                    $query->orderBy('client_details->name', $dir);
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails devis') ? '<a href="' . route('admin.salequote.show', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    data-bs-original-title="Voir les détails" aria-label="Views"><i
                        class="bi bi-eye-fill"></i></a>' : '';
                    $updateBtn = auth()->user()->can('Modifier devis') ? '<a href="' . route('admin.salequote.edit', $row->id) . '"
                    class="text-warning" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Modifier" aria-label="Edit"><i
                        class="bi bi-pencil-fill"></i></a>' : '';
                    $printBtn = auth()->user()->can('Imprimer devis') ? '<a href="' . route('admin.salequote.print', $row->id) . '" class="text-primary"
                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                    target="blank" data-bs-original-title="Imprimer commande"
                    aria-label="Views"><i
                        class="bi bi-printer-fill text-black"></i></a>' : '';
                    $carttosalequtoBtn = auth()->user()->can('Générer commande') ? '<a href="'.route('admin.order.create', ['order' => $row->id]).'"
                    class="text-success" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Générer une commande" aria-label="Edit"><i
                        class="bi bi-file-text"></i></a>' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer devis') ? '<form method="POST"
                    action="' . route('admin.salequote.delete', $row->id) . '"
                    class="delete_cart">
                    ' . csrf_field() . '
                    <input name="_method" type="hidden" value="GET">
                    <a href="javascript:;" id="show_confirm" type="submit"
                        class="text-danger show_confirm"
                        data-bs-toggle="tooltip" data-bs-placement="bottom"
                        title="" data-bs-original-title="Supprimer"
                        aria-label="Supprimer"><i
                            class="bi bi-trash-fill"></i></a>
                </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                    $btns .= $updateBtn . $confirmDeletion;
                    $btns .= $carttosalequtoBtn . $printBtn . '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'check', 'client', 'status'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.salequote.index', compact('sum'));
    }
    public function show(SaleQuote $salequote)
    {
        $salequoteitems = $salequote->items;
        return view('admin.salequote.show', compact('salequote', 'salequoteitems'));
    }
    public function salequotevalidation(Request $request)
    {
        $validated = $request->validate([
            'client_id' => 'required',
            'client_details.name' => 'required',
      
            'Prix_T' => 'required',
            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',

        ]);
        // dd($validated);

        return response()->json($validated);
    }
    public function create(Request $request)
    {

        $order = null;
        $type='salequote';
        $clients = Client::all();
        // $products = Product::all();
        $products = array();
        return view('admin.cart.create', compact('clients','order','type','products'));
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();

            $codification_setting = 'Codification devis';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            $data['codification'] = $nextCodification;

            if (isset($data['items'])) {
                $number = count($data["items"]);
                $data['number'] = $number;
                $data['store_id'] = auth()->user()->store->id;
                $data['user_id'] = auth()->user()->id;
                $data['created_by'] = auth()->user()->name;
                $salequote = SaleQuote::create($data);

                $salequote->items()->createMany($data['items']);

                DB::commit();
                return redirect()->route('admin.salequote.index')->with('success', "Ajouté avec succès!");
            } else {
                return redirect()->route('admin.salequote.create')->with('error', "Ajouter les produits!");
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function edit(SaleQuote $salequote)
    {
        $clients = Client::all();
        // $products = Product::all();
        $products = array();
        $type='salequote';
        $cart= $salequote;
        $title = 'Devis';
        return view('admin.cart.edit', compact('cart','title','type','clients','products'));
    }
    public function update(Request $request, SaleQuote $salequote)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if (isset($data['timbre'])) {
                $data['timbre'] = '1';
            } else {
                $data['timbre'] = '0';
            }
            $number = count($data["items"]);
            $data['number'] = $number;
            $data['user_id'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->name;
            $salequote->update($data);

            // @todo: Make quantity_id & price_id nullable
            foreach ($data['items'] as $key => $item) {
                if (!isset($item['quantity_id'])) {
                    $quantity = Quantity::firstOrCreate(
                        [
                            'store_id' => $salequote->store_id,
                            'product_id'=>$item['product_id']
                        ]
                    );
                    $data['items'][$key]['quantity_id'] = $quantity->id;
                }
                if (!isset($item['price_id'])) {
                    $data['items'][$key]['price_id'] = 0;
                }
            }

            $salequote->items()->delete();
            $salequote->items()->createMany($data['items']);

            DB::commit();
            return redirect()->route('admin.salequote.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
    public function delete(SaleQuote $salequote)
    {
        try {
            $salequote->delete();
            return redirect()->route('admin.salequote.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function serach_product($prd, $price_type)
    {

        $products = Product::where('name', 'LIKE', '%' . $prd . '%')->has('prices')->whereHas('quantities', function ($query) {
            $query->where('store_id', auth()->user()->store_id);
        })->get();
        $html = '              <div class="card-body">
        <div class="alert border-0 alert-dismissible fade show py-2">
        <div class="table-responsive">
          <table class="table align-middle table-striped">
            <tbody>

</div>';
        if (count($products)) {
            foreach ($products as $product) {
                $price_achat = Price::select('buying as price')->where('product_id', $product->id)->first();
                if (isset($price_achat)) {
                    $price_achat = number_format($price_achat->price, 3);
                } else {
                    $price_achat = 0;
                }
                /** prices attributes */
                $att1 = 0;
                $att2 = 0;
                $att1_values = '';
                $att2_values = '';
                $att1_ids = '';
                $att2_ids = '';
                $ids = isset($product->price_type['attributes']) ? $product->price_type['attributes'] : [];
                $attributes = $product->productAttributes()->whereIn('attribute_id', $ids)->with('attribute', 'values.attributeValue')->get()->sortByDesc(function ($productAttributes) {
                    return $productAttributes->values->count();
                });
                if (isset($product->price_type['attributes'][0])) {
                    $att1 = $attributes->first()->attribute_id;
                    $att1_values = $product->productAttributes()->where('attribute_id', $att1)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('attributeValue.value');
                    // $att1_values=$product->price?->productValue1->attributevalue->value;
                    $att1_ids = $product->productAttributes()->where('attribute_id', $att1)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('id');
                    // dd($att1_values);
                }
                if (isset($product->price_type['attributes'][1])) {
                    $att2 = $attributes->last()->attribute_id;
                    $att2_values = $product->productAttributes()->where('attribute_id', $att2)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('attributeValue.value');
                    $att2_ids = $product->productAttributes()->where('attribute_id', $att2)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('id');
                }
                $id1 = isset($att1_ids[0]) ? $att1_ids[0] : Null;
                $id2 = isset($att2_ids[0]) ? $att2_ids[0] : Null;
                // dd($att1_values);
                $price = $this->get_product_price($product->id, $price_type, $id1, $id2);
                // dd($price);

                /** quantity attributes */

                $att3 = 0;
                $att4 = 0;
                $att3_values = '';
                $att4_values = '';
                $att3_ids = '';
                $att4_ids = '';
                $ids = isset($product->quantity_type['attributes']) ? $product->quantity_type['attributes'] : [];
                $attributes = $product->productAttributes()->whereIn('attribute_id', $ids)->with('attribute', 'values.attributeValue')->get()->sortByDesc(function ($productAttributes) {
                    return $productAttributes->values->count();
                });
                if (isset($product->quantity_type['attributes'][0])) {
                    $att3 =  $attributes->first()->attribute_id;
                    $att3_values = $product->productAttributes()->where('attribute_id', $att3)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('attributeValue.value');
                    $att3_ids = $product->productAttributes()->where('attribute_id', $att3)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('id');
                    // dd($att1_values);
                }
                if (isset($product->quantity_type['attributes'][1])) {
                    $att4 =  $attributes->last()->attribute_id;
                    $att4_values = $product->productAttributes()->where('attribute_id', $att4)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('attributeValue.value');
                    $att4_ids = $product->productAttributes()->where('attribute_id', $att4)->with('attribute', 'values.attributeValue')->get()->pluck('values')->collapse()->pluck('id');
                }
                /**     */
                $html .= '  <tr>
            <td>
            <td class="productlist">
              <a class="d-flex align-items-center gap-2" href="#">
                <div class="product-box">
                    <img src=' . $product->default_image_url . ' alt="">
                </div>
                <div>
                    <h6 class="mb-0 product-title">' . $product->name . '</h6>
                </div>
               </a>
            </td>';
                $html .= '<td><span>' . $price . '  ' . $product->currency->name . '</span></td>';
                if ($product->status == 1) {
                    $html .= '<td><span class="badge rounded-pill alert-success">Active</span></td>';
                } else {
                    $html .= '<td><span class="badge rounded-pill alert-danger">Disabled</span></td>';
                }
                $html .= '<td><span>' . $product->updated_at . '</span></td>
            <td>
              <div class="d-flex align-items-center gap-3 fs-6">
              <input type="button" name="add_to_salequote" id="' . $product->id . '" style="" class="btn btn-primary btn-sm form-control form-control-sm add_to_salequote" value="Ajouter" />
              <input type="hidden"  id="name' . $product->id . '" value="' . $product->name . '" />
              <input type="hidden"  id="product_price_buying' . $product->id . '"  value="' . $price_achat . '"/>
              <input type="hidden"  id="product_price_selling' . $product->id . '"  value="' . $price . '"/>
              <input type="hidden"  id="product_unity' . $product->id . '"  value="' . $product->unity . '"/>
              <input type="hidden"  id="product_tva' . $product->id . '"  value="' . $product->tva . '"/>
              <input type="hidden"  id="currency' . $product->id . '"  value="' . $product->currency->name . '"/>
              <input type="hidden"  id="currency_id' . $product->id . '"  value="' . $product->currency->id . '"/>
              <input type="hidden"  id="price_type' . $product->id . '"  value="' . $product->price_type['type'] . '"/>
              <input type="hidden"  id="attA' . $product->id . '"  value="' . $att1 . '"/>';
                $html .= "<input type='hidden'  id='attA_values" . $product->id . "'  value='" .  $att1_values . "'/>";
                $html .= "<input type='hidden'  id='attA_ids" . $product->id . "'  value='" .  $att1_ids . "'/>";
                $html .=  '<input type="hidden"  id="attB' . $product->id . '"  value="' . $att2 . '"/>';
                $html .= "<input type='hidden'  id='attB_values" . $product->id . "'  value='" .  $att2_values . "'/>";
                $html .= "<input type='hidden'  id='attB_ids" . $product->id . "'  value='" .  $att2_ids . "'/>";
                $html .=  '<input type="hidden"  id="attC' . $product->id . '"  value="' . $att3 . '"/>';
                $html .= "<input type='hidden'  id='attC_values" . $product->id . "'  value='" .  $att3_values . "'/>";
                $html .= "<input type='hidden'  id='attC_ids" . $product->id . "'  value='" .  $att3_ids . "'/>";
                $html .=  '<input type="hidden"  id="attD' . $product->id . '"  value="' . $att4 . '"/>';
                $html .= "<input type='hidden'  id='attD_values" . $product->id . "'  value='" .  $att4_values . "'/>";
                $html .= "<input type='hidden'  id='attD_ids" . $product->id . "'  value='" .  $att4_ids . "'/>";


                $html .= '</div>
            </td>
          </tr>';
            }
            $html .= '</tbody></table></div><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
        } else {
            $html = Null;
        }
        return $html;
    }
    public function serach_product_c_bar($prd, $price_type)
    {
        $product_id = Barcode::where('barcode', $prd)->first();
        $product = Product::where('id', $product_id->product_id)->has('prices')->has('quantities')->first();
        $price = $this->get_product_price($product_id->product_id, $price_type, $product_id->product_value1, $product_id->product_value2);
        //   dd($product_id);
        $attribute1 = $product_id?->productValue1?->attributeValue->value;
        $attribute2 = $product_id?->productValue2?->attributeValue->value;
        $price_achat = Price::select('buying as price')->where('product_id', $product->id)->first();
        return [$product, $price, $attribute1, $attribute2, $product_id->product_value1, $product_id->product_value2, $price_achat->price, $product->currency->name];
    }

    public function get_product_price($prd, $price_type, $attr1, $attr2 = '0')
    {
        if ($attr2 == '0') {
            $attr2 = NULL;
        }
        if ($attr2 == 'undefined') {
            $attr2 = NULL;
        }
        // dd($attr2);
        switch ($price_type) {
            case 'detail':
                $price = Price::select('selling1 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                $price = $price ? $price : Price::select('selling1 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                break;
            case 'semigros':
                $price = Price::select('selling2 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                $price = $price ? $price : Price::select('selling2 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                break;
            case 'gros':
                $price = Price::select('selling3 as price')->where(['product_id' => $prd, 'product_value1' => $attr1, 'product_value2' => $attr2])->first();
                $price = $price ? $price : Price::select('selling3 as price')->where(['product_id' => $prd, 'product_value1' => $attr2, 'product_value2' => $attr1])->first();
                break;
        }

        // dd($prd, $price_type,$attr1,$attr2);
        return ($price->price);
    }

    public function print(SaleQuote $salequote)
    {
        $pdf = Helper::print($salequote,'salequote');
        return $pdf->stream('devis.pdf');
    }
}
