<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Cart;
use App\Models\CartsItems;
use App\Models\Quantity;
use App\Models\ReturnNote;
use App\Models\ReturnNoteCommitment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class ReturnNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carts = ReturnNote::query();
        if ($request->ajax()) {
            $carts->where('store_id', auth()->user()->store_id);

            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $carts->whereBetween('created_at', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('client') && $request->query('client') != '') {
                $carts->where('client_id', $request->query('client'));
            }
            return DataTables::eloquent($carts)
                ->editColumn('date', function ($row) {
                    $formattedDate = Carbon::parse($row->date)->format('d-m-y ');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->addColumn('status', function ($row) {
                    if ($row->paiment_status == 0) {
                        if ($row->payments()->exists()) {
                            $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                        } else {
                            $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                        }
                    } else {
                        $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                    }
                    return $status;
                })
                ->editColumn('total', function ($row) {
                    $total = $row->total . 'DM';
                    return $total;
                })
                ->addColumn('check', function ($row) {
                    $check = ' <div class="form-check">';
                    if ($row->invoice_id == null) {
                        $check .= '<input name="cart_id[]" value="' . $row->id . '"
                        class="form-check-input" type="checkbox"
                        form="create-invoice-form">';
                    }
                    $check .= '</div>';
                    return $check;
                })
                ->addColumn('client', function ($row) {
                    $client = ' <div class="d-flex align-items-center gap-3 cursor-pointer">';
                    if ($row->client) {
                        $client .= ' <img src="' . asset($row->client->image_url) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client->code . '-' . $row->client_details['name'] . '</p>';
                    } else {
                        $client .= ' <img src="' . asset(config('stock.image.default.passenger')) . '"
                        class="rounded-circle" width="44" height="44"
                        alt="">';
                        $client .= '<p class="mb-0">' . $row->client_details['name'] . '</p>';
                    }
                    $client .= '</div>';
                    return $client;
                })
                ->addColumn('action', function ($row) {
                    $showBtn = auth()->user()->can('Détails bon de retour BL') ? '  <a href="'.route('admin.returnnote.show', $row->id) .'"
                    class="text-primary" data-bs-toggle="tooltip"
                    data-bs-placement="bottom" title=""
                    data-bs-original-title="Voir détails" aria-label="Views"><i
                        class="bi bi-file-pdf-fill text-danger"></i></a>' : '';
                    $updateBtn = auth()->user()->can('Modifier commande') ? '' : '';
                    $printBtn = auth()->user()->can('Imprimer commande') ? '' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer commande') ? '' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' . $showBtn;
                    $btns .=  '</div>';

                    return $btns;
                })
                ->rawColumns(['action', 'check', 'client', 'status'])
                ->withQuery('total', function ($filteredQuery) {
                    return $filteredQuery->sum('total');
                })
                ->make(true);
        }
        $sum = $carts->sum('total');
        return view('admin.returnnote.index', compact('sum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cart $cart)
    {
        return view('admin.returnnote.create', compact('cart'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request)
    {
        $validated = $request->validate([
            'client_id' => 'required',
            'cart_id' => 'required',
            'client_details.name' => 'required',
            'date' => 'required',
            'total' => 'required|numeric|gt:0',
            'items' => 'required|array|min:1',
        ]);

        return response()->json($validated);
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $number = count($data["items"]);
            // get last record
            $codification_setting = 'Codification bon de retour BL';
            $nextCodification = \App\Helpers\Helper::generateCodification($codification_setting);
            $data['codification'] = $nextCodification;
            $data['number'] = $number;
            $data['store_id'] = auth()->user()->store->id;
            $data['user_id'] = auth()->user()->id; 
            $data['created_by'] = auth()->user()->name; 

            $returnnote = ReturnNote::create($data);

            foreach ($data['items'] as $key => $item) {
                $CartItem=CartsItems::where('cart_id',$data['cart_id'])->where('product_id',$data['items'][$key]['product_id'])->first();
                $possible_return=$CartItem->possible_return - $data['items'][$key]['product_quantity'];
                $CartItem->update(['possible_return'=>$possible_return]);

                $quantity = Quantity::firstOrCreate(
                    [
                        'store_id' => auth()->user()->store_id,
                        'product_id' => $data['items'][$key]['product_id']
                    ]
                );
                $update = Helper::updateQuantity($returnnote, 'Création ', $data['items'][$key]['product_quantity'], $quantity, 'add');

            }
            $cart=Cart::find($data['cart_id']);
            $count=$cart->items->where('possible_return','!=',0)->count();
            if($count==0){
                $cart->update(['return_status'=> 0]);
            }
            $returnnote->items()->createMany($data['items']);

            DB::commit();
            return redirect()->route('admin.returnnote.index')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReturnNote $returnnote)
    {
        return view('admin.returnnote.show', compact('returnnote'));
    }

    public function print(ReturnNote $returnnote)
    {
        $pdf = Helper::print($returnnote, 'returnnote');
        return $pdf->stream('returnnote.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
