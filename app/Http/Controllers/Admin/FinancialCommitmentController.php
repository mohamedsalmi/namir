<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Client;
use App\Models\CommitmentPayment;
use App\Models\CreditNote;
use App\Models\CreditnoteCommitment;
use App\Models\FinancialCommitment;
use App\Models\PaymentCart;
use App\Models\PaymentCreditnote;
use App\Models\ReturnNote;
use App\Models\ReturnNotePayment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class FinancialCommitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from = $request->has('from') ? $request->get('from') : null;
        $to = $request->has('to') ? $request->get('to') : null;

        if ($request->has('filter')) {
            if ($request->query('filter') == 'check') {
                $carts = Cart::whereHas('commitments', function ($query) {
                    $query->whereHas('paiements', function ($query) {
                        $query->where('type', 'check')->whereDate('due_at', '<', Carbon::now())->whereNull('status');
                    });
                })->get();
            } else {
                $carts = Cart::whereHas('commitments', function ($query) {
                    $query->whereDate('date', '<=', Carbon::now())->where('paiment_status', 0);
                })->get();
            }
        } else {
            $carts = Cart::when($from, function ($query) use ($from, $to) {
                return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
            })->get();
        }

        $creditnotes = CreditNote::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->get();
        $returnnotes = ReturnNote::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->get();

        $collection = collect();

        if (isset($carts)) {
            foreach ($carts as $cart)
                $collection->push($cart);
        }
        if (isset($creditnotes)) {
            foreach ($creditnotes as $creditnote)
                $collection->push($creditnote);
        }
        if (isset($returnnotes)) {
            foreach ($returnnotes as $returnnote)
                $collection->push($returnnote);
        }

        $CartsTotal = Cart::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->sum('total');
        $CreditNoteTotal = CreditNote::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->sum('total');
        $ReturnNoteTotal = ReturnNote::when($from, function ($query) use ($from, $to) {
            return $query->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
        })->sum('total');

        $total = $CartsTotal - ($CreditNoteTotal +  $ReturnNoteTotal);

        return view('admin.financialcommitment.index', compact('collection', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $cart)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'cart':
                $model = 'App\Models\Cart';
                break;
            case 'creditnote':
                $model = 'App\Models\CreditNote';
                break;
            case 'returnnote':
                $model = 'App\Models\ReturnNote';
                break;

            default:
                break;
        }

        $cart = $model::find($cart);
        return view('admin.financialcommitment.show', compact('cart'));
    }

    public function showCommitment(Request $request, $commitment)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'cart':
                $commitment_model = 'App\Models\FinancialCommitment';
                break;
            case 'creditnote':
                $commitment_model = 'App\Models\CreditnoteCommitment';
                break;
            case 'returnnote':
                $commitment_model = 'App\Models\ReturnNoteCommitment';
                break;

            default:
                $commitment_model = 'App\Models\FinancialCommitment';
                break;
        }

        $commitment = $commitment_model::find($commitment);

        $ReturnNoteAmounts = ReturnNote::where(['paiment_status' => 0, 'client_id' => $commitment->parent->client_id])->get();
        $CreditNoteAmounts = CreditNote::where(['paiment_status' => 0, 'client_id' => $commitment->parent->client_id])->get();

        $returnedamounts = collect();

        if (isset($ReturnNoteAmounts)) {
            foreach ($ReturnNoteAmounts as $amount)
                $returnedamounts->push($amount);
        }
        if (isset($CreditNoteAmounts)) {
            foreach ($CreditNoteAmounts as $amount)
                $returnedamounts->push($amount);
        }
        return view('admin.financialcommitment.showcommitment', compact('commitment', 'returnedamounts'));
    }

    public function showReturn(CreditnoteCommitment $commitment)
    {
        return view('admin.financialcommitment.creditnote.show', compact('commitment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $cart)
    {
        $type = $request->query('type');
        switch ($type) {
            case 'cart':
                $model = 'App\Models\Cart';
                break;
            case 'creditnote':
                $model = 'App\Models\CreditNote';
                break;
            case 'returnnote':
                $model = 'App\Models\ReturnNote';
                break;

            default:
                break;
        }

        $cart = $model::find($cart);

        $ReturnNoteAmounts = ReturnNote::where(['paiment_status' => 0, 'client_id' => $cart->client_id])->get();
        $CreditNoteAmounts = CreditNote::where(['paiment_status' => 0, 'client_id' => $cart->client_id])->get();

        $returnedamounts = collect();

        if (isset($ReturnNoteAmounts)) {
            foreach ($ReturnNoteAmounts as $amount)
                $returnedamounts->push($amount);
        }
        if (isset($CreditNoteAmounts)) {
            foreach ($CreditNoteAmounts as $amount)
                $returnedamounts->push($amount);
        }

        return view('admin.financialcommitment.edit', compact('cart', 'returnedamounts'));
    }

    public function paiementvalidation(Request $request)
    {
        $validated = $request->validate([
            'Deadline.*.amount' => 'required_if:Deadline_check,on|exclude_unless:Deadline_check,on|gt:0',
            'Deadline.*.date' => 'required_if:Deadline_check,on',
            'Deadline.*.currency_id' => 'required_if:Deadline_check,on',
            'paiements' => [Rule::requiredIf(function () use ($request) {
                return (empty($request->request->get('Deadline_check')));
            })],
            'paiements.cash.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['cash'] && empty($request->request->get('Deadline_check'))));
            })],
            'paiements.cash.*.currency_id' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['cash'] && empty($request->request->get('Deadline_check'))));
            })],
            'paiements.check.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'] && empty($request->request->get('Deadline_check'))));
            })],
            'paiements.check.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.check.*.bank' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.check.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['check'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.transfer.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.transfer.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.transfer.*.bank' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.transfer.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['transfer'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.exchange.*.amount' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.exchange.*.numbre' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
            })],
            'paiements.exchange.*.received_at' => [Rule::requiredIf(function () use ($request) {
                return (!empty($request->paiements['exchange'])  && empty($request->request->get('Deadline_check')) );
            })],

        ]);
        // dd($validated);

        return response()->json($validated);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $commitment)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data);
            $type = $request->query('type');

            switch ($type) {
                case 'financialcommitment':
                    $model = 'App\Models\FinancialCommitment';
                    $relation = 'cart';
                    break;
                case 'creditnotecommitment':
                    $model = 'App\Models\CreditnoteCommitment';
                    $relation = 'creditnote';
                    break;
                case 'returnnotecommitment':
                    $model = 'App\Models\ReturnNoteCommitment';
                    $relation = 'returnNote';
                    break;

                default:
                    break;
            }

            if($request->has('commitment_id')){
                $commitment = $request->get('commitment_id');
            }

            $commitment = $model::find($commitment);

            if (isset($data['paiements']['cash'])) {
                $commitment->paiements()->createMany($data['paiements']['cash']);
            }
            if (isset($data['paiements']['check'])) {
                $commitment->paiements()->createMany($data['paiements']['check']);
            }
            if (isset($data['paiements']['transfer'])) {
                $commitment->paiements()->createMany($data['paiements']['transfer']);
            }
            if (isset($data['paiements']['exchange'])) {
                $commitment->paiements()->createMany($data['paiements']['exchange']);
            }       
            if (isset($data['paiements']['ra'])) {
                foreach ($data['paiements']['ra'] as $key => $ra) {
                    $nameSpace = '\\App\Models\\';
                    $model = $nameSpace . $ra['model'];
                    $modelToUpdate = $model::where('id', $ra['model_id'])->first();
                    if ($ra['amount'] <= ($commitment->$relation->total - $commitment->paiements->sum('amount'))) {
                        $modelToUpdate->update(['paiment_status' => 1]);
                        $modelToUpdate->commitments()->update(['paiment_status' => 1]);
                    }
                    else{
                        $ra['amount'] = $commitment->$relation->total - $commitment->paiements->sum('amount');
                    }
                    if ($modelToUpdate->commitments()->exists()) {
                        $comt = $modelToUpdate->commitments()->first();
                    } else {
                        $comt = $modelToUpdate->commitments()->create(['date' => date('Y-m-d'), 'amount' => $modelToUpdate->total]);
                    }
                    $comt->paiements()->create(['amount' => $ra['amount'], 'type' => 'ra']);
                    $commitment->paiements()->create($ra);
                }
            }
            $total = $commitment->paiements->sum('amount');
            // dd($total);
            // if ($total > $commitment->amount) {
            //     return redirect()->back()->with('error', "Total est supérieur au montant de l'échéance!");
            // }
            if ($total >= $commitment->amount) {
                $commitment->update(['paiment_status' => 1]);
            }
            if ($commitment->$relation->total <= $commitment->$relation->commitments()->where('paiment_status', 1)->sum('amount')) {
                $commitment->$relation()->update(['paiment_status' => 1]);
            }

            $commitment->paiements()->update(['created_by' => auth()->user()->name]);

            DB::commit();
            
            return redirect()->back()->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function updateCommitments(Request $request, $cart)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data);
            $model = null;
            $commitment_model = null;
            $foreign = null;
            $type = $request->query('type');

            switch ($type) {
                case 'cart':
                    $model = 'App\Models\Cart';
                    $commitment_model = 'App\Models\FinancialCommitment';
                    $foreign = 'cart_id';
                    $relation = 'cart';
                    break;
                case 'creditnote':
                    $model = 'App\Models\CreditNote';
                    $commitment_model = 'App\Models\CreditnoteCommitment';
                    $foreign = 'credit_note_id';
                    $relation = 'creditnote';
                    break;
                case 'returnnote':
                    $model = 'App\Models\ReturnNote';
                    $commitment_model = 'App\Models\ReturnNoteCommitment';
                    $foreign = 'return_note_id';
                    $relation = 'returnNote';
                    break;

                default:
                    break;
            }

            $cart = $model::find($cart);

            $cart->commitments()->delete();

            if (isset($data['Deadline_check'])) {
                $cart->commitments()->createMany($data['Deadline']);
                $cart->update(['paiment_status' => 0]);
            } else {
                $financial_data[$foreign] = $cart->id;
                $financial_data['date'] = date('Y-m-d H:i:s');
                $financial_data['amount'] = $data['total'];
                $total_payments_amounts = 0;
                $methods = ['cash', 'check', 'exchange', 'transfer', 'rs', 'ra'];
                foreach ($methods as $key => $method) {
                    if (isset($data['paiements'][$method])) {
                        foreach ($data['paiements'][$method] as $key => $payment) {
                            $total_payments_amounts += $payment['amount'];
                        }
                    }
                }
                $paiment_status = $total_payments_amounts >= $data['total'] ? 1 : 0;
                // dd($paiment_status);
                $financial_data['paiment_status'] = $paiment_status;
                $financial = $commitment_model::create($financial_data);
                if (isset($data['paiements']['cash'])) {
                    $financial->paiements()->createMany($data['paiements']['cash']);
                }
                if (isset($data['paiements']['check'])) {
                    $financial->paiements()->createMany($data['paiements']['check']);
                }
                if (isset($data['paiements']['transfer'])) {
                    $financial->paiements()->createMany($data['paiements']['transfer']);
                }
                if (isset($data['paiements']['exchange'])) {
                    $financial->paiements()->createMany($data['paiements']['exchange']);
                }
                if (isset($data['paiements']['rs'])) {
                    $financial->paiements()->createMany($data['paiements']['rs']);
                }
                if (isset($data['paiements']['ra'])) {
                    foreach ($data['paiements']['ra'] as $key => $ra) {
                        $nameSpace = '\\App\Models\\';
                        $model = $nameSpace . $ra['model'];
                        $modelToUpdate = $model::where('id', $ra['model_id'])->first();
                        if ($ra['amount'] <= ($financial->$relation->total - $financial->paiements->sum('amount'))) {
                            $modelToUpdate->update(['paiment_status' => 1]);
                            $modelToUpdate->commitments()->update(['paiment_status' => 1]);
                        }
                        else{
                            $ra['amount'] = $financial->$relation->total - $financial->paiements->sum('amount');
                        }
                        if ($modelToUpdate->commitments()->exists()) {
                            $comt = $modelToUpdate->commitments()->first();
                        } else {
                            $comt = $modelToUpdate->commitments()->create(['date' => date('Y-m-d'), 'amount' => $modelToUpdate->total]);
                        }
                        $comt->paiements()->create(['amount' => $ra['amount'], 'type' => 'ra']);
                        $financial->paiements()->create($ra);
                    }
                }
                $cart->update(['paiment_status' => $paiment_status]);
                $cart->payments()->update(['created_by' => auth()->user()->name]);
            }
            DB::commit();
            return redirect()->route('admin.financialcommitment.index', ['from' => date("Y-m-d"), 'to' => date("Y-m-d")])->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Cart $cart)
    {
        try {
            DB::beginTransaction();
            $cart->commitments()->delete();
            DB::commit();
            return redirect()->route('admin.financialcommitment.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function deleteReturn(CreditnoteCommitment $commitment)
    {
        try {
            $commitment->delete();
            return redirect()->route('admin.financialcommitment.creditnote.index')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function deletePayment($payment, Request $request)
    {
        try {
            DB::beginTransaction();
            $model = null;
            $type = $request->query('type');

            switch ($type) {
                case 'cart':
                    $model = 'App\Models\PaymentCart';
                    break;
                case 'creditnote':
                    $model = 'App\Models\PaymentCreditnote';
                    break;
                case 'returnnote':
                    $model = 'App\Models\ReturnNotePayment';
                    break;

                default:
                    break;
            }

            $payment = $model::find($payment);
            $payment->commitment()->update(['paiment_status' => 0]);
            $commitment = $payment->commitment;
            $parent = $commitment->parent->update(['paiment_status' => 0]);
            $payment->delete();
            // if ((!$commitment->paiements()->exists()) && ( $commitment->parent->commitments->count() < 2))
            // {
            //     $commitment->delete();
            //     DB::commit();
            //     return redirect()->route('admin.financialcommitment.index', ['from' => date("Y-m-d"), 'to' => date("Y-m-d")])->with('success', "Supprimé avec succès!");
            // }
            DB::commit();
            return redirect()->back()->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
