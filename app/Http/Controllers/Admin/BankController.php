<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\CreateBankRequest;
use App\Http\Requests\UpdateBankRequest;
use App\Models\Bank;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
class BankController extends Controller
{
        /**
     * Show the list of available banks.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Bank::query();
            return DataTables::eloquent($data)
                ->addColumn('action', function ($row) {
                    $updateBtn = auth()->user()->can('Modifier banque') || true ? '<a href="' . route('admin.bank.edit', $row->id) . '" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Edit info" aria-label="Edit"><i class="bi bi-pencil-fill"></i></a>
                    ' : '';
                    $confirmDeletion = auth()->user()->can('Supprimer banque') || true ? '<form method="POST" action="' . route('admin.bank.delete', $row->id) . '">'
                        . csrf_field() . 
                        '<input name="_method" type="hidden" value="GET">
                        <button id="show_confirm" type="submit" class="btn btn-sm text-danger show_confirm" data-toggle="tooltip" title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                    </form>' : '';

                    $btns = '<div class="d-flex align-items-center gap-3 fs-6">' .
                        $updateBtn . $confirmDeletion
                        . '</div>';

                    return $btns;
                })
                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.bank.list');
    }

    public function create()
    {
        $banks = Bank::pluck('name', 'id');
        return view('admin.bank.create', compact('banks'));
    }

    public function store(CreateBankRequest $request)
    {
        try {
            $data = $request->all();
            $bank = Bank::create($data);
            return redirect()->route('admin.bank.list')->with('success', "Ajouté avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function edit(Bank $bank)
    {
        return view('admin.bank.edit', compact('bank'));
    }

    public function update(UpdateBankRequest $request, Bank $bank)
    {
        try {
            $data = $request->all();
            $bank->update($data);
            return redirect()->route('admin.bank.list')->with('success', "Modifié avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    public function delete(Bank $bank)
    {
        try {
            $bank->delete();
            return redirect()->route('admin.bank.list')->with('success', "Supprimé avec succès!");
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
