<?php

namespace App\Http\Middleware;

use App\Helpers\Helper;
use App\Models\Currency;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SetSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $default = Currency::where('value', 1)->first();
        if (!Session::has('currency')) {
            session()->put('currency', $default);
        }
        $value = Helper::get_currency_value();
        session()->put('currency_value', $value);

    
        return $next($request);
    }
}
