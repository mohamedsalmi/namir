<?php

namespace App\Notifications;

use App\Models\Attribute as ModelsAttribute;
use App\Models\AttributeValue;
use App\Models\Product;
use App\Models\ProductAttributeValue;
use Attribute;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MinQuantityNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        $product = Product::select('name', 'id')->where('id', $this->data['prd'])->first();
        // $attr1=ProductAttributeValue::select('product_attribute_id', 'attribute_value_id')->where('id', $this->data['attr1'])->first();
        // $attr1_value=AttributeValue::select('value')->where('id', $attr1->attribute_value_id)->first();
        // $attr1_name=ModelsAttribute::select('name')->where('id', $attr1->product_attribute_id)->first();
        // $attr2=ProductAttributeValue::select('product_attribute_id', 'attribute_value_id')->where('id', $this->data['attr2'])->first();
        // $attr2_value=AttributeValue::select('value')->where('id', $attr2->attribute_value_id)->first();
        // $attr2_name=ModelsAttribute::select('name')->where('id', $attr2->product_attribute_id)->first();
        $attr1_value=$this->data['qt']?->productValue1?->attributevalue?->value;
        $attr1_name=$this->data['qt']?->productAttribute1?->attribute?->name;
        $attr2_value=$this->data['qt']?->productValue2?->attributevalue?->value;
        $attr2_name=$this->data['qt']?->productAttribute2?->attribute?->name;
                // dd($this->data['qt']);

        return [
            'product' => $product->name,
            'attribute1_name' => $attr1_name,
            'attribute1_value' => $attr1_value,
            'attribute2_name' => $attr2_name,
            'attribute2_value' => $attr2_value,
        ];
    }
}
