@extends('shop.layouts.app')
@section('title', __('app.commun.home') )
@section('stylesheet')
    <link rel="stylesheet" href="{{ asset('assets/shop/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/shop/owlcarousel/owl.theme.default.min.css') }}">
@endsection
@section('content')
    <!-- Home Section Start -->
    <section class="home-section pt-2 ratio_50">
        <div class="container-fluid-lg">
            <div class="row g-4">
                <div class="col-xl-9 col-lg-8 ratio_50_1 ">
                    <div class="owl-carousel">
                        @php $exclusive=null; @endphp
                        @if ($sliders->count() > 0)
                            @php
                                $exclusive = $sliders->firstWhere('exclusive', true);
                            @endphp

                            @foreach ($sliders as $slider)
                                @if ($slider != $exclusive)
                                    <!-- Your code to display the slider goes here -->

                                    <div class="home-contain furniture-contain-2  ">
                                        <img src="{{ asset($slider->image_url) }}" class="bg-img blur-up lazyload"
                                            alt="{{ $slider->name }}">
                                        <div class="home-detail p-top-left mend-auto w-100 ">
                                            <div>
                                                <h6 class="{{ $slider->color2 ? $slider->color2 : 'text-content' }}">
                                                    {{ $slider->offre }}<span
                                                        class="{{ $slider->color3 ? $slider->color3 : 'daily' }}">{{ $slider->discount > 0 ? $slider->discount . '%' : '' }}</span>
                                                </h6>
                                                <h1
                                                    class="text-uppercase poster-1 {{ $slider->color1 ? $slider->color2 : 'text-content' }} furniture-heading">
                                                    {{ $slider->slogan }}<span
                                                        class="{{ $slider->color1 ? $slider->color1 : 'daily' }}">{{ $slider->name }}</span>
                                                </h1>
                                                @if ($slider->url)
                                                    <button onclick="location.href = '{{ $slider->url }}';"
                                                        class="btn btn-furniture mt-xxl-4 mt-3 home-button mend-auto {{ $slider->color2 ? $slider->color2 : 'text-content' }}">{{ __('app.commun.shop_now') }}<i
                                                            class="fa-solid fa-right-long ms-2 icon"></i></button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="home-contain furniture-contain-2  ">
                                <img src="{{ asset('assets/shop/images/furniture/banner/1.jpg') }}"
                                    class="bg-img blur-up lazyload" alt="config('stock.info.ar_name')">
                                <div class="home-detail p-top-left mend-auto w-100 ">
                                    <div>
                                        <h6 class="text-content"><span class="daily"></span></h6>
                                        <h1 class="text-uppercase poster-1 text-content furniture-heading"><span
                                                class="daily"></span></h1>
                                        <button onclick="location.href = "
                                            class="btn btn-furniture mt-xxl-4 mt-3 home-button mend-auto text-content">{{ __('app.commun.shop_now') }}
                                            <i class="fa-solid fa-right-long ms-2 icon"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 d-lg-inline-block d-none">
                    <div class="home-contain h-100 home-furniture">
                        <img src="{{ asset($exclusive ? $exclusive->image_url : 'assets/shop/images/furniture/banner/2.png') }}"
                            class="bg-img blur-up lazyload" alt="{{ $exclusive->name??'' }}">
                        <div class="home-detail p-top-left home-p-sm feature-detail mend-auto">
                            <div>
                                {{-- <h2 class="mt-0 theme-color text-kaushan fw-normal">{{ __('app.commun.exclusive') }}</h2> --}}
                                <h3 class="furniture-content">{{ __('app.commun.furniture') }}</h3>
                                <a href="{{ $exclusive ? $exclusive->url : 'javascript:void(0)' }}"
                                    class="shop-button btn btn-furniture mt-0 d-inline-block btn-md {{ $exclusive ? $exclusive->color2 : 'text-content' }}">{{ __('app.commun.shop_now') }}<i
                                        class="fa-solid fa-right-long ms-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Deal Section Start -->
    @if ($weeklies->count() > 0)
        <section class="deal-section">
            <div class="container-fluid-lg">
                <div class="title">
                    <h2>{{ __('app.commun.weekly_deal') }}</h2>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="three-slider-1 arrow-slider ">
                            @foreach ($weeklies as $key => $weekly)
                                <div>
                                    <div class="deal-box wow fadeInUp">
                                        <a href="{{ route('shop.show', ['product' => $weekly->id]) }}"
                                            class="category-image order-sm-2">
                                            <img src="{{ asset($weekly->default_image_url) }}"
                                                class="img-fluid blur-up lazyload" alt="{{ $weekly->name }}">
                                        </a>

                                        <div class="deal-detail order-sm-1">
                                            <button class="buy-box btn theme-bg-color text-white btn-cart addToCart"
                                                data-product-id="{{ $weekly->id }}">
                                                <i class="iconly-Buy icli m-0"></i>
                                            </button>
                                            <input class="form-control input-number qty-input"
                                                id="qty-input-{{ $weekly->id }}" type="hidden" name="quantity"
                                                value="1">
                                            <div class="hot-deal">
                                                <span>{{ __('app.commun.weekly_deal') }}</span>
                                            </div>
                                            <ul class="rating">
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                            </ul>
                                            <a href="{{ route('shop.show', ['product' => $weekly->id]) }}"
                                                class="text-title">
                                                <h5>{{ $weekly->name }}</h5>
                                            </a>
                                            @if ($weekly->discount > 0)
                                                <h5 class="price">
                                                    {{ number_format($weekly->selling1 / session('currency_value'), 3) }}
                                                    {{ session('currency')->symbol }}
                                                    <span>{{ number_format(($weekly->selling1 / session('currency_value')) * (1 - $weekly->discount / 100), 3) }}
                                                        {{ session('currency')->symbol }}</span>
                                                </h5>
                                            @else
                                                <h5 class="price">
                                                    {{ number_format($weekly->selling1 / session('currency_value'), 3) }}
                                                    {{ session('currency')->symbol }}
                                                </h5>
                                            @endif
                                            {{-- <div class="progress custom-progressbar">
                                            <div class="progress-bar" style="width: 50%" role="progressbar"
                                                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div> --}}
                                            @if ($weekly->quantities()->whereHas('store', function ($q) {
                                                $q->where('website_sales', 1);
                                            })->sum('quantity') < 1)
                                                <h4 class="item">
                                                    <span
                                                        class="badge bg-danger">{{ __('app.shop.single.out_stock') }}</span>
                                                </h4>
                                            @endif
                                            <h4 class="offer">{{ __('app.commun.hurry_up') }}</h4>
                                            <div class="timer" id="clockdiv-{{ $key + 1 }}" data-hours="1"
                                                data-minutes="2" data-seconds="3">
                                                <ul>
                                                    <li>
                                                        <div class="counter">
                                                            <div class="days">
                                                                <h6></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="counter">
                                                            <div class="hours">
                                                                <h6></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="counter">
                                                            <div class="minutes">
                                                                <h6></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="counter">
                                                            <div class="seconds">
                                                                <h6></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <script src="{{ asset('assets/shop/js/timer' . ($key + 1) . '.js') }}"></script>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!-- Deal Section End -->

    <!-- Product Sction Start -->
    <section class="product-section">
        <div class="container-fluid-lg">
            <div class="title title-flex-2">
                <h2>{{ __('app.commun.our_products') }}</h2>

            </div>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                    <div class="row g-8">

                        @foreach ($recently as $item)
                            <div class="col-xxl-2 col-lg-3 col-md-4 col-6 wow fadeInUp">
                                @include('shop.layouts.card', ['product' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Sction End -->

    <!-- Product Section Start -->
    @if ($news->count() > 0 || $features->count() > 0 || $best_sells->count() > 0 || $on_sells->count() > 0)
        <section class="product-section-2">
            <div class="container-fluid-lg">
                <div class="row gy-sm-5 gy-4">
                    <div class="col-xxl-3 col-md-6">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="title title-border d-block">
                                    <h3>{{ __('app.commun.new_products') }}</h3>
                                </div>
                                @php
                                    $lists = $news->chunk(3);
                                @endphp
                                <div class="product-category-1 arrow-slider-2">
                                    @foreach ($lists as $key => $list)
                                        <div>
                                            <div class="row gy-sm-4 gy-3">
                                                @foreach ($list as $key => $item)
                                                    <div class="col-12">
                                                        <div class="product-box-4 wow fadeInUp">
                                                            <a href="shop-left-sidebar.html" class="product-image">
                                                                <img src="{{ asset($item->default_image_url) }}"
                                                                    class="img-fluid" alt="{{ $item->name }}">
                                                            </a>
                                                            <div class="product-details">
                                                                <ul class="rating">
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                </ul>
                                                                <a
                                                                    href="{{ route('shop.show', ['product' => $item->id]) }}">
                                                                    <h4 class="name">{{ $item->name }}</h4>
                                                                </a>
                                                                @if ($item->selling1 > $item->selling2 && $item->selling2 != 0)
                                                                    <h5 class="price">
                                                                        {{ number_format(($item->selling2 / session('currency_value')) , 3) }}
                                                                        {{ session('currency')->symbol }}<del>{{ number_format($item->selling1 / session('currency_value'), 3) }}</del>
                                                                    </h5>
                                                                @else
                                                                    <h5 class="price">
                                                                        {{ number_format($item->selling1 / session('currency_value'), 3) }} {{ session('currency')->symbol }}
                                                                    </h5>
                                                                @endif
                                                                <ul class="option">
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.add_to_cart') }}">
                                                                        <button class="buy-box btn  btn-cart addToCart"
                                                                            data-product-id="{{ $item->id }}">
                                                                            <i class="iconly-Buy icli"></i> </button>
                                                                        <input id="qty-input-{{ $item->id }}"
                                                                            type="hidden" name="quantity"
                                                                            value="1"> </button>
                                                                        {{-- <a href="cart.html">
                                                                        <i class="iconly-Buy icli"></i>
                                                                    </a> --}}
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.quick_view') }}">
                                                                        <a data-bs-toggle="modal"
                                                                            data-product="{{ $item }}"
                                                                            data-bs-target="#view-modal">
                                                                            <i class="iconly-Show icli"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.wishlist') }}">
                                                                        <a class="add_wishlist"
                                                                            id="wish{{ $item->id }}"
                                                                            data="{{ $item->id }}">
                                                                            <i class="iconly-Heart icli"
                                                                                @if ($item->check_wishlist) style="color:red" @endif></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xxl-3 col-md-6">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="title title-border d-block">
                                    <h3>{{ __('app.commun.feature_products') }}</h3>
                                </div>

                                @php
                                    $lists = $features->chunk(3);
                                @endphp
                                <div class="product-category-1 arrow-slider-2">
                                    @foreach ($lists as $key => $list)
                                        <div>
                                            <div class="row gy-sm-4 gy-3">
                                                @foreach ($list as $key => $item)
                                                    <div class="col-12">
                                                        <div class="product-box-4 wow fadeInUp">
                                                            <a href="{{ route('shop.show', ['product' => $item->id]) }}"
                                                                class="product-image">
                                                                <img src="{{ asset($item->default_image_url) }}"
                                                                    class="img-fluid" alt="{{ $item->name }}">
                                                            </a>
                                                            <div class="product-details">
                                                                <ul class="rating">
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                </ul>
                                                                <a
                                                                    href="{{ route('shop.show', ['product' => $item->id]) }}">
                                                                    <h4 class="name">{{ $item->name }}</h4>
                                                                </a>
                                                                @if ($item->selling1 > $item->selling2 && $item->selling2 != 0)
                                                                    <h5 class="price">
                                                                        {{ number_format(($item->selling2 / session('currency_value')) , 3) }}
                                                                        {{ session('currency')->symbol }}<del>{{ number_format($item->selling1 / session('currency_value'), 3) }}</del>
                                                                    </h5>
                                                                @else
                                                                    <h5 class="price">
                                                                        {{ number_format($item->selling1 / session('currency_value'), 3) }} {{ session('currency')->symbol }}
                                                                    </h5>
                                                                @endif
                                                                <ul class="option">
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.add_to_cart') }}">
                                                                        <button class="buy-box btn  btn-cart addToCart"
                                                                            data-product-id="{{ $item->id }}">
                                                                            <i class="iconly-Buy icli"></i> </button>
                                                                        <input id="qty-input-{{ $item->id }}"
                                                                            type="hidden" name="quantity"
                                                                            value="1"> </button>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.quick_view') }}">
                                                                        <a data-bs-toggle="modal"
                                                                            data-product="{{ $item }}"
                                                                            data-bs-target="#view-modal">
                                                                            <i class="iconly-Show icli"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.wishlist') }}">
                                                                        <a class="add_wishlist"
                                                                            id="wish{{ $item->id }}"
                                                                            data="{{ $item->id }}"
                                                                            @if ($item->check_wishlist) style="color:red" @endif>
                                                                            <i class="iconly-Heart icli"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xxl-3 col-md-6">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="title title-border d-block">
                                    <h3>{{ __('app.commun.best_seller') }}</h3>
                                </div>

                                @php
                                    $lists = $best_sells->chunk(3);
                                @endphp
                                <div class="product-category-1 arrow-slider-2">
                                    @foreach ($lists as $key => $list)
                                        <div>
                                            <div class="row gy-sm-4 gy-3">
                                                @foreach ($list as $key => $item)
                                                    <div class="col-12">
                                                        <div class="product-box-4 wow fadeInUp">
                                                            <a href="{{ route('shop.show', ['product' => $item->id]) }}"
                                                                class="product-image">
                                                                <img src="{{ asset($item->default_image_url) }}"
                                                                    class="img-fluid" alt="{{ $item->name }}">
                                                            </a>
                                                            <div class="product-details">
                                                                <ul class="rating">
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                </ul>
                                                                <a
                                                                    href="{{ route('shop.show', ['product' => $item->id]) }}">
                                                                    <h4 class="name">{{ $item->name }}</h4>
                                                                </a>
                                                                @if ($item->selling1 > $item->selling2 && $item->selling2 != 0)
                                                                    <h5 class="price">
                                                                        {{ number_format(($item->selling2 / session('currency_value')) , 3) }}
                                                                        {{ session('currency')->symbol }}<del>{{ number_format($item->selling1 / session('currency_value'), 3) }}</del>
                                                                    </h5>
                                                                @else
                                                                    <h5 class="price">
                                                                        {{ number_format($item->selling1 / session('currency_value'), 3) }} {{ session('currency')->symbol }}
                                                                    </h5>
                                                                @endif
                                                                <ul class="option">
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.add_to_cart') }}">
                                                                        <button class="buy-box btn  btn-cart addToCart"
                                                                            data-product-id="{{ $item->id }}">
                                                                            <i class="iconly-Buy icli"></i> </button>
                                                                        <input id="qty-input-{{ $item->id }}"
                                                                            type="hidden" name="quantity"
                                                                            value="1"> </button>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.quick_view') }}">
                                                                        <a data-bs-toggle="modal"
                                                                            data-product="{{ $item }}"
                                                                            data-bs-target="#view-modal">
                                                                            <i class="iconly-Show icli"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.wishlist') }}">
                                                                        <a class="add_wishlist"
                                                                            id="wish{{ $item->id }}"
                                                                            data="{{ $item->id }}"
                                                                            @if ($item->check_wishlist) style="color:red" @endif>
                                                                            <i class="iconly-Heart icli"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xxl-3 col-md-6">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="title title-border d-block">
                                    <h3>{{ __('app.commun.limited_edition') }}</h3>
                                </div>
                                @php
                                    $lists = $on_sells->chunk(3);
                                @endphp
                                <div class="product-category-1 arrow-slider-2">
                                    @foreach ($lists as $key => $list)
                                        <div>
                                            <div class="row gy-sm-4 gy-3">
                                                @foreach ($list as $key => $item)
                                                    <div class="col-12">
                                                        <div class="product-box-4 wow fadeInUp">
                                                            <a href="{{ route('shop.show', ['product' => $item->id]) }}"
                                                                class="product-image">
                                                                <img src="{{ asset($item->default_image_url) }}"
                                                                    class="img-fluid" alt="{{ $item->name }}">
                                                            </a>
                                                            <div class="product-details">
                                                                <ul class="rating">
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                    <li>
                                                                        <i data-feather="star" class="fill"></i>
                                                                    </li>
                                                                </ul>
                                                                <a
                                                                    href="{{ route('shop.show', ['product' => $item->id]) }}">
                                                                    <h4 class="name">{{ $item->name }}</h4>
                                                                </a>
                                                                @if ($item->selling1 > $item->selling2 && $item->selling2 != 0)
                                                                    <h5 class="price">
                                                                        {{ number_format(($item->selling2 / session('currency_value')) , 3) }}
                                                                        {{ session('currency')->symbol }}<del>{{ number_format($item->selling1 / session('currency_value'), 3) }}</del>
                                                                    </h5>
                                                                @else
                                                                    <h5 class="price">
                                                                        {{ number_format($item->selling1 / session('currency_value'), 3) }} {{ session('currency')->symbol }}
                                                                    </h5>
                                                                @endif
                                                                <ul class="option">
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.add_to_cart') }}">
                                                                        <button class="buy-box btn  btn-cart addToCart"
                                                                            data-product-id="{{ $item->id }}">
                                                                            <i class="iconly-Buy icli"></i> </button>
                                                                        <input id="qty-input-{{ $item->id }}"
                                                                            type="hidden" name="quantity"
                                                                            value="1"> </button>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.quick_view') }}">
                                                                        <a data-bs-toggle="modal"
                                                                            data-product="{{ $item }}"
                                                                            data-bs-target="#view-modal">
                                                                            <i class="iconly-Show icli"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bs-toggle="tooltip" data-bs-placement="top"
                                                                        title="{{ __('app.shop.item.wishlist') }}">
                                                                        <a class="add_wishlist"
                                                                            id="wish{{ $item->id }}"
                                                                            data="{{ $item->id }}"
                                                                            @if ($item->check_wishlist) style="color:red" @endif>
                                                                            <i class="iconly-Heart icli"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!-- Product Section End -->



    <!-- Review Section Start -->
    @if ($testimonials->count() > 0)
        <section class="review-section section-lg-space mt-5 mb-5">
            <div class="container-fluid">
                <div class="about-us-title text-center">
                    <h4 class="text-content">{{ __('app.commun.testimonials') }}</h4>
                    <h2 class="center">{{ __('app.commun.what_they_say') }}</h2>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="slider-4-half product-wrapper">
                            @foreach ($testimonials as $key => $testimonial)
                                <div>
                                    <div class="reviewer-box">
                                        <i class="fa-solid fa-quote-right"></i>
                                        <div class="product-rating">
                                            <ul class="rating">

                                                @for ($i = 1; $i < $testimonial->rating + 1; $i++)
                                                    <li>
                                                        <i data-feather="star" class="fill"></i>
                                                    </li>
                                                @endfor
                                                @for ($i = $testimonial->rating + 1; $i < 6; $i++)
                                                    <li>
                                                        <i data-feather="star"></i>
                                                    </li>
                                                @endfor

                                            </ul>
                                        </div>

                                        <h3>{{ $testimonial->name }}</h3>

                                        <p>"{{ $testimonial->testimonial }}"</p>

                                        <div class="reviewer-profile">
                                            <div class="reviewer-image">
                                                <img src="{{ asset($testimonial->image_url) }}" class="blur-up lazyload"
                                                    alt="{{ $testimonial->name }}">
                                            </div>

                                            <div class="reviewer-name">
                                                <h4>{{ $testimonial->writer_details['name'] }}</h4>
                                                <h6>{{ $testimonial->writer_details['title'] }},
                                                    {{ $testimonial->writer_details['company'] }}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!-- Review Section End -->


    <!-- Home Section End -->
@endsection
@section('scripts')
    <script src="{{ asset('assets/shop/owlcarousel/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    600: {
                        items: 1,
                        nav: false
                    },
                    1000: {
                        items: 1,
                        nav: false,
                        loop: true
                    }
                },
                lazyLoad: true,
                pagination: false,
                dots: false,
                autoplay: true,
                autoplayTimeout: 3000,
                navigation: false,
                stopOnHover: true,
                rtl: true,
                nav: false,
                navigationText: ["<i class='mdi mdi-chevron-left'></i>",
                    "<i class='mdi mdi-chevron-right'></i>"
                ]

            });
        });
    </script>
@endsection
