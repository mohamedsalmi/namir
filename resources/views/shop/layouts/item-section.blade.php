<button class="close-button">
    <i class="fas fa-times"></i>
</button>
<h6>
    <i class="iconly-Bag-2 icli"></i>
    <span>{{ Session::has('cart') ? count(Session::get('cart')) : 0 }} {{ __('app.commun.items') }}</span>
</h6>
@if (Session::has('cart'))
    @php
        $cart_total = collect(Session::get('cart'))->sum(function ($t) {
            return ($t['selling1'] > $t['selling2'] && $t['selling2'] != 0) ? $t['quantity'] * $t['selling2'] : $t['quantity'] * $t['selling1'];
        });
    @endphp
    <ul class="items-image">

        @php
            $counter = 0;
        @endphp

        @foreach (Session::get('cart') as $key => $item)
            @if ($counter >= 3)
            @break
        @endif
        <li>
            <img src="{{ asset($item['default_image_url']) }}" alt="">
        </li>
        @php
            $counter++;
        @endphp
    @endforeach
    @if (Session::has('cart') && count(Session::get('cart')) > 3)
        <li>+{{ count(Session::get('cart')) - 3 }}</li>
    @endif
</ul>
@endif

<button onclick="window.location.href='{{ route('shop.cart.index') }}'"
class="btn item-button btn-sm fw-bold">{{ Session::has('cart') ? number_format($cart_total / session('currency_value'), 3) : 0 }}
{{ session('currency')->symbol }}</button>
