<div class="product-box-4  ribbon-content">
    @if (Helper::get_product_offer($product->id))
        <a href="{{ route('shop.index', ['deal' => Helper::get_product_offer($product->id)]) }}"
            class="{{ str_replace('_', '-', app()->getLocale()) == 'ar' ? 'ribbon-top-right' : 'ribbon-top-left' }}">{{ Helper::get_product_offer($product->id) }}</a>
    @endif
    <div class="product-image">
        <div class="label-flex">
            <a class="btn p-0 wishlist btn-wishlist notifi-wishlist add_wishlist" id="wish{{ $product->id }}"
                data="{{ $product->id }}" @if ($product->check_wishlist) style="color:red" @endif>
                <i class="iconly-Heart icli"></i>
            </a>
        </div>

        <a href="{{ route('shop.show', ['product' => $product->id]) }}">
            <img src="{{ asset($product->default_image_url) }}" class="img-fluid" alt="{{ $product->name }}">
        </a>

        <ul class="option">
            <li data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('app.shop.item.quick_view') }}">
                <a data-bs-toggle="modal" data-product="{{ $product }}" data-bs-target="#view-modal">
                    <i class="iconly-Show icli"></i>
                </a>
            </li>
        </ul>
        @if ($product->quantities()->whereHas('store', function ($q) {
            $q->where('website_sales', 1);
        })->sum('quantity') < 1)
            @if ($product->by_order)
                <h4 class="item">
                    <span class="badge bg-success">{{ __('app.shop.single.by_order') }}</span>
                </h4>
            @else
                <h4 class="item">
                    <span class="badge bg-danger">{{ __('app.shop.single.out_stock') }}</span>
                </h4>
            @endif
        @endif
    </div>
    <div class="product-detail">
        @foreach ($product->categories_names ?? [] as $key => $cat)
            <a href="{{ route('shop.index', ['categories' => $key]) }}">
                @if ($loop->first)
                    {{ $cat }}
                @else
                    , {{ $cat }}
                @endif
            </a>
        @endforeach
        <ul class="rating">
            <li>
                <i data-feather="star" class="fill"></i>
            </li>
            <li>
                <i data-feather="star" class="fill"></i>
            </li>
            <li>
                <i data-feather="star" class="fill"></i>
            </li>
            <li>
                <i data-feather="star" class="fill"></i>
            </li>
            <li>
                <i data-feather="star" class="fill"></i>
            </li>
        </ul>
        <a href="{{ route('shop.show', ['product' => $product->id]) }}">
            <h5 class="name">{{ $product->name }}</h5>
        </a>
        @if ($product->selling1 > $product->selling2 && $product->selling2!=0)
            <h5 class="price">
                {{ number_format(($product->selling2 / session('currency_value')) , 3) }}
                {{ session('currency')->symbol }}<del>{{ number_format($product->selling1 / session('currency_value'), 3) }}
                    {{ session('currency')->symbol }}</del>
            </h5>
        @else
            <h5 class="price">
                {{ number_format($product->selling1 / session('currency_value'), 3) }}
                {{ session('currency')->symbol }}
            </h5>
        @endif

        @if ($product->writer && $product->writer != '-')
            <h6 class="unit"><a
                    href="{{ route('shop.index', ['writer' => $product->writer]) }}">{{ $product->writer }}</a>
            </h6>
        @endif
        @if ($product->house && $product->house != '-')
            <h6 class="unit"><a
                    href="{{ route('shop.index', ['publisher' => $product->house]) }}">{{ $product->house }}</a>
            </h6>
        @endif

        @php
            $qty = Helper::getCartItemQty($product);
        @endphp
        <div class="price-qty">
            <div class="counter-number">
                <div class="counter">
                    <div class="qty-left-minus " data-product-id="{{ $product->id }}" data-type="minus"
                        data-field="">
                        <i class="fa-solid fa-minus"></i>
                    </div>
                    <input class="form-control input-number qty-input" id="qty-input-{{ $product->id }}"
                        type="text" name="quantity" value="{{ $qty != 0 ? $qty : 1 }}">
                    <div class="qty-right-plus " data-product-id="{{ $product->id }}" data-type="plus" data-field="">
                        <i class="fa-solid fa-plus"></i>
                    </div>
                </div>
            </div>

            <button class="buy-button buy-button-2 btn btn-cart addToCart" data-product-id="{{ $product->id }}">
                <i class="iconly-Buy icli text-white m-0"></i>
            </button>
        </div>
    </div>
</div>
