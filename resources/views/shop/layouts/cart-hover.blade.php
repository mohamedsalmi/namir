<a href="{{ route('shop.cart.index') }}" class="header-icon bag-icon">
    <small
        class="badge-number cart-count">{{ Session::has('cart') ? count(Session::get('cart')) : 0 }}</small>
    <i class="iconly-Bag-2 icli"></i>
</a>
<div class="onhover-div">
    @if (!Session::has('cart') || count(Session::get('cart')) == 0)
        <a class="dropdown-item">
            <div class="text-center">{{ __('app.cart.empty_cart') }}
            </div>
        </a>
    @else
        <ul class="cart-list">
            @foreach (Session::get('cart') as $key => $item)
                <li>
                    <div class="drop-cart">
                        <a href="{{ route('shop.show', ['product' => $item['id'] ?? 1]) }}"
                            class="drop-image">
                            <img src="{{ asset($item['default_image_url']) }}"
                                class="blur-up lazyload" alt="">
                        </a>

                        <div class="drop-contain">
                            <a title="{{ $item['name'] }}"
                                href="{{ route('shop.show', ['product' => $item['id'] ?? 1]) }}">
                                <h5>{{ $item['name'] }}</h5>
                            </a>
                            <h6><span>{{ $item['quantity'] }} x</span>
                                {{-- {{ number_format($item['selling1']/ session('currency_value'), 3) }} {{ session('currency')->symbol }} --}}
                                @if ($item['selling1'] > $item['selling2'] && $item['selling2']!=0)
                                    {{-- <h5 class="price"> --}}
                                        {{ number_format(($item['selling2'] / session('currency_value')) , 3) }}
                                        {{ session('currency')->symbol }}<del>{{ number_format($item['selling1'] / session('currency_value'), 3) }}</del>
                                    {{-- </h5> --}}
                                @else
                                    {{-- <h5 class="price"> --}}
                                        {{ number_format($item['selling1'] / session('currency_value'), 3) }}
                                        {{ session('currency')->symbol }}
                                    {{-- </h5> --}}
                                @endif
                            </h6>
                            <button class="close-button remove"
                                data-product-id="{{ $item['id'] }}">
                                <i class="fa-solid fa-xmark"></i>
                            </button>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>

        @php
            $cart_total = collect(Session::get('cart'))->sum(function ($t) {
                return ($t['selling1'] > $t['selling2'] && $t['selling2'] != 0) ? $t['quantity'] * $t['selling2'] : $t['quantity'] * $t['selling1'];
            });
        @endphp

        <div class="price-box">
            <h5>{{ __('app.cart.total') }} :</h5>
            <h4 class="theme-color fw-bold">
                {{ number_format($cart_total/ session('currency_value'), 3) }}
                {{ session('currency')->symbol }}</h4>
        </div>

        <div class="button-group">
            <a href="{{ route('shop.cart.index') }}"
                class="btn btn-sm cart-button">{{ __('app.cart.view_cart') }}</a>
            <a href="{{ route('shop.cart.checkout') }}"
                class="btn btn-sm cart-button theme-bg-color
  text-white">{{ __('app.cart.checkout') }}</a>
        </div>
    @endif
</div>