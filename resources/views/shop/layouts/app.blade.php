<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
    dir="{{ str_replace('_', '-', app()->getLocale()) == 'ar' ? 'rtl' : 'ltr' }}">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('meta_description', config('stock.info.ar_name'))">
    <meta name="keywords" content="@yield('meta_keywords', config('stock.info.ar_name'))">
    <meta name="author" content="@yield('meta_author', config('stock.info.ar_name'))">
    <link rel="icon" href="{{ asset(config('stock.info.image')) }}" type="image/x-icon">
    <title>@yield('title',config('stock.info.name')) </title>
    @if (app()->getLocale() == 'ar')
        <link id="rtl-link" rel="stylesheet" type="text/css"
            href="{{ asset('assets/shop/css/vendors/bootstrap.rtl.css') }}">
    @endif

    <!-- Google font -->
    @if (str_replace('_', '-', app()->getLocale()) == 'ar')
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=El+Messiri&display=swap" rel="stylesheet">
    @else
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link
            href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
    @endif

    <!-- bootstrap css -->
    <link id="rtl-link" rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/vendors/bootstrap.css') }}">

    <!-- wow css -->
    <link rel="stylesheet" href="{{ asset('assets/shop/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/shop/css/components/ribbon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/shop/css/custom-style.css') }}">

    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/vendors/font-awesome.css') }}">

    <!-- feather icon css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/vendors/feather-icon.css') }}">

    <!-- Plugin CSS file with desired skin css -->
    <link rel="stylesheet" href="{{ asset('assets/shop/css/vendors/ion.rangeSlider.min.css') }}">

    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/vendors/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/vendors/slick/slick-theme.css') }}">

    <!-- animation css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/font-style.css') }}">

    <!-- Template css -->
    <link id="color-link" rel="stylesheet" type="text/css" href="{{ asset('assets/shop/css/style.css') }}">

    @yield('stylesheet')
</head>

<body class="theme-color-3 dark">
    <!-- Loader Start -->
    <div class="fullpage-loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
    <!-- Loader End -->
    @include('shop.partials.header')
    @yield('content')
    @include('shop.partials.footer')
    @include('shop.partials.modals.quick-view-modal')

    {{-- @include('shop.partials.cookie') --}}
    <!-- Items section Start -->
    <div class="button-item">
        <button class="item-btn btn text-white">
            <i class="iconly-Bag-2 icli"></i>
        </button>
    </div>
    <div class="item-section" id="item-section">
        @include('shop.layouts.item-section')

    </div>
    <!-- Items section End -->
    @include('shop.partials.dealmodal')
    <!-- Tap to top start -->
    <div class="theme-option">
        <div class="back-to-top">
            <a id="back-to-top" href="#">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <!-- Tap to top end -->

    <!-- Bg overlay Start -->
    <div class="bg-overlay"></div>
    <!-- Bg overlay End -->

    <!-- latest jquery-->
    <script src="{{ asset('assets/shop/js/jquery-3.6.0.min.js') }}"></script>

    <!-- jquery ui-->
    <script src="{{ asset('assets/shop/js/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap js-->
    <script src="{{ asset('assets/shop/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/bootstrap/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/bootstrap/popper.min.js') }}"></script>

    <!-- feather icon js-->
    <script src="{{ asset('assets/shop/js/feather/feather.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/feather/feather-icon.js') }}"></script>

    <!-- Lazyload Js -->
    <script src="{{ asset('assets/shop/js/lazysizes.min.js') }}"></script>

    <!-- Slick js-->
    <script src="{{ asset('assets/shop/js/slick/slick.js') }}"></script>
    <script src="{{ asset('assets/shop/js/slick/slick-animation.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/custom-slick-animated.js') }}"></script>
    <script src="{{ asset('assets/shop/js/slick/custom_slick.js') }}"></script>

    <!-- Range slider js -->
    <script src="{{ asset('assets/shop/js/ion.rangeSlider.min.js') }}"></script>

    <!-- Auto Height Js -->
    <script src="{{ asset('assets/shop/js/auto-height.js') }}"></script>

    <!-- Lazyload Js -->
    <script src="{{ asset('assets/shop/js/lazysizes.min.js') }}"></script>

    <!-- Quantity js -->
    <script src="{{ asset('assets/shop/js/quantity-2.js') }}"></script>

    <!-- Fly Cart Js -->
    <script src="{{ asset('assets/shop/js/fly-cart.js') }}"></script>

    <!-- Copy clipboard Js -->
    <script src="{{ asset('assets/shop/js/clipboard.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/copy-clipboard.js') }}"></script>

    <!-- sidebar open js -->
    <script src="{{ asset('assets/shop/js/filter-sidebar.js') }}"></script>

    <!-- WOW js -->
    <script src="{{ asset('assets/shop/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/shop/js/custom-wow.js') }}"></script>

    <!-- script js -->
    <script src="{{ asset('assets/shop/js/script.js') }}"></script>

    <!-- thme setting js -->
    <script src="{{ asset('assets/shop/js/theme-setting.js') }}"></script>
    <script>
        var wishlistNotify = "{{ __('app.notify.wishlist_notify') }}";
        var login = "{{ __('login.log_in') }}";
    </script>

    <script src="{{ asset('assets/shop/js/wishlist.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        const currencyOptions = document.querySelectorAll('.currency-option');
        currencyOptions.forEach(function(option) {
            option.addEventListener('click', function(event) {
                event.preventDefault();
                const currencyId = this.getAttribute('data-currency-id');
                axios.post('/set-currency', {
                        currencyId
                    })
                    .then(response => {
                        location.reload();
                    });
            });
        });
    </script>
    <script>
        document.getElementById('hot_deal').addEventListener('click', function() {
            axios.get('/get-hot-deals')
                .then(function(response) {
                    if (!response.data || !response.data.length) {
                        return document.getElementById('hotDealsList').innerHTML =
                            '<li>No hot deals found.</li>';
                    }
                    let hotDealsHtml = '';
                    response.data.forEach(function(hotDeal) {
                        let price = '';
                        if (hotDeal.selling1 > hotDeal.selling2 && hotDeal.selling2!=0)
                        {
                            price = `
                            <h6 class="price">
                                ${(hotDeal.selling2 / @json(session('currency_value'))).toFixed(3)} {{ session('currency')->symbol }}
                                <del>${(hotDeal.selling1 / @json(session('currency_value'))).toFixed(3)}
                                    {{ session('currency')->symbol }}</del>
                            </h6>
                            `;
                        }  
                        else{
                            price = `
                            <h6 class="price">
                                ${(hotDeal.selling1 / @json(session('currency_value'))).toFixed(3)} {{ session('currency')->symbol }}
                            </h6>
                            `;
                        }
                        
                        hotDealsHtml += `
                        <li class="list-${hotDeal.id}">
                            <div class="deal-offer-contain">
                                <a href="/shop/show/${hotDeal.id}" class="deal-image">
                                    <img src="${hotDeal.default_image_url}" class="blur-up lazyload" alt="">
                                </a>
                                <a href="/shop/show/${hotDeal.id}" class="deal-contain">
                                    <h5>${hotDeal.name}</h5>
                                    ${price}    
                                </a>
                            </div>
                        </li>
                        `;
                    });
                    document.getElementById('hotDealsList').innerHTML = hotDealsHtml;
                })
                .catch(function(error) {
                    console.error(error);
                });
        });
    </script>
    <script>
        // $(document).on('click','.quick-view',function(e){
        //     let product_id=1;
        //     $("#view-modal").load('/quick-view-modal/'+product_id + " #view-modal");
        // });
        $("#view-modal").on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget);
            var object = button.data('product');
            $('#name').text(object.name);
            let price = '';
            if (object.selling1 > object.selling2 && object.selling2 != 0)
            {
                price = `
                <span class="theme-color" id="price_discoount">
                    ${(object.selling2 / @json(session('currency_value'))).toFixed(3)} {{ session('currency')->symbol }}
                </span>
                <del>${(object.selling1 / @json(session('currency_value'))).toFixed(3)}
                    {{ session('currency')->symbol }}</del>
                `;
            }  
            else{
                price = `
                <span class="theme-color">
                    ${(object.selling1 / @json(session('currency_value'))).toFixed(3)} {{ session('currency')->symbol }}
                </span>
                `;
            }
            $('#price').html(price);
            $('#resume').text(object.resume);
            $('#writer').text(object.writer);
            $('#writer').attr('href', "/shop?writer=" + object.writer);
            $('#house').text(object.house);
            $('#house').attr('href', "/shop?publisher=" + object.house);
            $('#image').attr('src', object.default_image_url);
            $('#show_btn').attr('href', "/shop/show/" + object.id);
            $('#add-cart-button').attr('data-product-id', object.id);

            // $('#email').text(object.email);
            // Set the values of other elements in the modal based on the object properties
            // ...
        });
    </script>
    <script>
        function calculateTotal() {
            var total = 0;
            var discount = $('#discount-percentage').data('discount');
            $(".total").each(function() {
                total += parseFloat($(this).text().replace(',',''));
            });
            $('#subtotal-amount').text(total.toFixed(3));
            $('#discount-value').text((total * (discount / 100)).toFixed(3));
            discount != 0 ? total = total * (1 - discount / 100) : total;
            $('#total-amount').text(total.toFixed(3));
        }

        $(document).ready(function() {
            // $('.addToCart').on('click', function() {
                $(document).on('click', '.addToCart', function(event) {
                let product_id = $(this).attr('data-product-id');
                var product_qty = $('#qty-input-' + product_id).val();
                $.ajax({
                    type: "GET",
                    url: "/shop/cart/addtocart",
                    data: {
                        product_id: product_id,
                        quantity: product_qty ? product_qty : 1,
                    },
                    success: function(response) {
                        if (response.status === 'success' || response.status === 'warning') {
                            if (response.status === 'warning') {
                            $.notify({ 
                                icon: "fa fa-exclamation-triangle",
                                message: "{{ __('app.notify.product_by_order') }}"
                            }, {
                                type: "success",
                                allow_dismiss: true,
                                newest_on_top: true,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },
                                template: '<div data-notify="container" class="alert alert-success text-center" role="alert">' +
                                    '<button type="button" aria-hidden="true" class="btn-close" data-notify="dismiss"></button>' +
                                    '<span data-notify="icon"></span> ' +
                                    '<span data-notify="message">{2}</span>' +
                                    '</div>'
                            });
                        }
                            $('#item-section').html(response.item_section);
                            $('#cart-hover').html(response
                                .cart_hover);
                            $('.cart-count').html(Object.keys(response.cart).length);
                            // $('.close-button').on("click", function() {
                            $(document).on('click', '.close-button', function(event) {
                                $('.item-section').removeClass("active");
                            });
                        } else if (response.status === 'error') {
                            $.notify({ 
                                icon: "fa fa-exclamation-triangle",
                                message: "{{ __('app.notify.product_out_of_stock') }}"
                            }, {
                                type: "danger",
                                allow_dismiss: true,
                                newest_on_top: true,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },
                                template: '<div data-notify="container" class="alert alert-danger text-center" role="alert">' +
                                    '<button type="button" aria-hidden="true" class="btn-close" data-notify="dismiss"></button>' +
                                    '<span data-notify="icon"></span> ' +
                                    '<span data-notify="message">{2}</span>' +
                                    '</div>'
                            });
                        } 
                    }
                });
            });
            $(document).on('click', '.remove', function() {
                var product_id = $(this).data('product-id');
                var product_qty = $('#qty-input-' + product_id).val();
                $.ajax({
                    type: "GET",
                    url: '/shop/cart/removecartitem',
                    data: {
                        product_id: product_id
                    },
                    success: function(response) {
                        if (response.status === 'success') {
                            $('#item-section').html(response.item_section);
                            $('#cart-hover').html(response.cart_hover);
                            $('.cart-count').html(Object.keys(response.cart).length);
                            // $('.close-button').on("click", function() {
                            $(document).on('click', '.close-button', function(event) {
                                $('.item-section').removeClass("active");
                            });
                        }
                    }
                });
            });

        });
    </script>
    <script>
        // $('.qty-left-minus , .qty-right-minus').on('click', function() {
        $(document).on('click', '.qty-left-minus , .qty-right-minus', function(event) {
            var $qty = $(this).siblings(".qty-input");
            var _val = $($qty).val();
            if (_val == '1') {
                var _removeCls = $(this).parents('.cart_qty');
                $(_removeCls).removeClass("open");
            }
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 1) {
                $qty.val(currentVal - 1);

                var current_total = $(this).next().attr('data-total');
                var current_price = $(this).next().attr('data-price');
                $('#' + current_total).text((parseFloat($qty.val()) * parseFloat($('#' + current_price).text()))
                    .toFixed(3));
                $(document).ready(function() {
                    calculateTotal();
                });

            }
        });

        // $('.qty-right-plus , .qty-left-plus').click(function() {
        $(document).on('click', '.qty-right-plus , .qty-left-plus', function(event) {
            // .qty-left-minus , .qty-right-minus
            var qty = $(this).prev().val(+parseFloat($(this).prev().val()) + 1).val();
            var current_total = $(this).prev().attr('data-total');
            var current_price = $(this).prev().attr('data-price');
            $('#' + current_total).text((qty * parseFloat($('#' + current_price).text())).toFixed(3));
            $(document).ready(function() {
                calculateTotal();
            });
        });

        $(document).on('click', '.qty-right-plus', function(event) {
        // $('.qty-right-plus').click(function() {
            var qty = $(this).next().val(+parseFloat($(this).next().val()) + 1).val();
            var current_total = $(this).next().attr('data-total');
            var current_price = $(this).next().attr('data-price');
            $('#' + current_total).text((qty * parseFloat($('#' + current_price).text())).toFixed(3));
            $(document).ready(function() {
                calculateTotal();
            });
        });



        $(document).on('change', '.qty-input', function(e) {
            var qty = $(this).val();
            if (qty == '' || qty == 0) {
                $(this).val(1);
                qty = 1;
            }
            var current_total = $(this).attr('data-total');
            var current_price = $(this).attr('data-price');
            $('#' + current_total).text((qty * parseFloat($('#' + current_price).text())).toFixed(3));
            $(document).ready(function() {
                calculateTotal();
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            let currentRoute = window.location.href;
            var Url = new URL(currentRoute);
            Url.search = "";
            currentRoute = Url.toString();
            if (currentRoute.endsWith("/")) {
                currentRoute = currentRoute.slice(0, -1);
            }
            let activeItem = $(".mobile-menu li a[href='" + currentRoute + "']").closest("li");
            let activeNav = $(".navbar-nav li a[href='" + currentRoute + "']").closest("li");
            $('.mobile-menu li').removeClass("active");
            $('.navbar-nav li').removeClass("active");
            activeItem.addClass("active");
            activeNav.addClass("active");
        });
    </script>

    @yield('scripts')
</body>

</html>
