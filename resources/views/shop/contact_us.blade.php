@extends('shop.layouts.app')
@section('title', __('app.commun.contact_us'))
@section('content')
    <!-- Breadcrumb Section Start -->
    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2>{{ __('app.commun.contact_us') }}</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('shop.home') }}">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{ __('app.commun.contact_us') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
    {!! Form::open([
        'enctype' => 'multipart/form-data',
        'route' => 'shop.send_mail',
        'method' => 'POST',
        'id' => 'send-mail-form',
    ]) !!}
    <!-- Contact Box Section Start -->
    <section class="contact-box-section">
        <div class="container-fluid-lg">
            <div class="row g-lg-5 g-3">
                <div class="col-lg-6">
                    <div class="left-sidebar-box">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="contact-image">
                                    <img src="{{ asset('assets/shop/images/inner-page/contact-us.svg') }}"
                                        class="img-fluid blur-up lazyloaded" alt="">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="contact-title">
                                    <h3>{{ __('app.commun.get_in_touch') }}</h3>
                                </div>

                                <div class="contact-detail">
                                    <div class="row g-4">
                                        <div class="col-xxl-6 col-lg-12 col-sm-6">
                                            <div class="contact-detail-box">
                                                <div class="contact-icon">
                                                    <i class="fa-solid fa-phone"></i>
                                                </div>
                                                <div class="contact-detail-title">
                                                    <h4>{{ __('app.commun.phone') }}</h4>
                                                </div>

                                                <div class="contact-detail-contain">
                                                    <p>{{ config('stock.info.phone') }}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xxl-6 col-lg-12 col-sm-6">
                                            <div class="contact-detail-box">
                                                <div class="contact-icon">
                                                    <i class="fa-solid fa-envelope"></i>
                                                </div>
                                                <div class="contact-detail-title">
                                                    <h4>{{ __('app.commun.mail') }}</h4>
                                                </div>

                                                <div class="contact-detail-contain">
                                                    <p>{{ config('stock.info.mail') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-6 col-lg-12 col-sm-6">
                                            <div class="contact-detail-box">
                                                <div class="contact-icon">
                                                    <i class="fa-solid fa-location-dot"></i>
                                                </div>
                                                <div class="contact-detail-title">
                                                    <h4>{{ __('app.commun.address') }}</h4>
                                                </div>

                                                <div class="contact-detail-contain">
                                                    <p>{{ config('stock.info.address') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            $stores = Helper::get_stores();
                                        @endphp
                                        @foreach ($stores as $key => $store)
                                            <div class="col-xxl-6 col-lg-12 col-sm-6">
                                                <div class="contact-detail-box">
                                                    <div class="contact-icon">
                                                        <i class="fa-solid fa-building"></i>
                                                    </div>
                                                    <div class="contact-detail-title">
                                                        <h4>{{ $store->name }}</h4>
                                                    </div>

                                                    <div class="contact-detail-contain">
                                                        <p>{{ $store->address }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="title d-xxl-none d-block">
                        <h2>{{ __('app.commun.contact_us') }}</h2>
                    </div>
                    <div class="right-sidebar-box">
                        <div class="row">
                            <div class="col-xxl-6 col-lg-12 col-sm-6">
                                <div class="mb-md-4 mb-3 custom-form">
                                    <label for="exampleFormControlInput"
                                        class="form-label">{{ __('app.form_contact.name') }}</label>
                                    <div class="custom-input">
                                        <input type="text" name="name" id="name" class="form-control"
                                            id="exampleFormControlInput" placeholder="{{ __('app.form_contact.name') }}"
                                            required>
                                        <i class="fa-solid fa-user"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xxl-6 col-lg-12 col-sm-6">
                                <div class="mb-md-4 mb-3 custom-form">
                                    <label for="exampleFormControlInput1"
                                        class="form-label">{{ __('app.form_contact.lastname') }}</label>
                                    <div class="custom-input">
                                        <input type="text" name="lastname" id="lastname" class="form-control"
                                            id="exampleFormControlInput1"
                                            placeholder="{{ __('app.form_contact.lastname') }}" required>
                                        <i class="fa-solid fa-user"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xxl-6 col-lg-12 col-sm-6">
                                <div class="mb-md-4 mb-3 custom-form">
                                    <label for="exampleFormControlInput2"
                                        class="form-label">{{ __('app.form_contact.email_adress') }}</label>
                                    <div class="custom-input">
                                        <input type="email" name="email" id="email" class="form-control"
                                            id="exampleFormControlInput2"
                                            placeholder="{{ __('app.form_contact.lastname') }}" required>
                                        <i class="fa-solid fa-envelope"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xxl-6 col-lg-12 col-sm-6">
                                <div class="mb-md-4 mb-3 custom-form">
                                    <label for="exampleFormControlInput3"
                                        class="form-label">{{ __('app.form_contact.phone_number') }}</label>
                                    <div class="custom-input">
                                        <input type="tel" name="phone" class="form-control"
                                            id="exampleFormControlInput3"
                                            placeholder="{{ __('app.form_contact.phone_number') }}" maxlength="10"
                                            oninput="javascript: if (this.value.length > this.maxLength) this.value =
                                            this.value.slice(0, this.maxLength);"
                                            required>
                                        <i class="fa-solid fa-mobile-screen-button"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-md-4 mb-3 custom-form">
                                    <label for="exampleFormControlTextarea"
                                        class="form-label">{{ __('app.form_contact.message') }}</label>
                                    <div class="custom-textarea">
                                        <textarea class="form-control" name="message" id="exampleFormControlTextarea"
                                            placeholder="{{ __('app.form_contact.message') }}" rows="6" required></textarea>
                                        <i class="fa-solid fa-message"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="g-recaptcha"  data-sitekey="{{config('recaptcha.api_site_key')}}" data-callback='onSubmit' data-action='submit'></div> --}}
                        <div class="g-recaptcha" data-sitekey="{{ config('recaptcha.api_site_key') }}"></div>
                        @error('g-recaptcha-response')
                            <span class="text-danger">{{$message}}</span>
                        @enderror

                        <button
                            class="btn btn-animation btn-md fw-bold ms-auto ">{{ __('app.form_contact.send_message') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Box Section End -->
    {!! Form::close() !!}

    <!-- Map Section Start -->
    <section class="map-section">
        <div class="container-fluid p-0">
            <div class="map-box">
                <iframe src="{{ config('stock.info.map_adress') }}" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </section>
    <!-- Map Section End --->
@endsection
@section('scripts')
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <script>
        function onSubmit(token) {
            document.getElementById("send-mail-form").submit();
        }
    </script>

@if (Session::has('success'))
    <script>
        $(document).ready(function() {
            $.notify({
                icon: "fa fa-check",
                // title: "__('app.commun.success')",
                message: "{{ Session::get('success') }}",
            }, {
                element: "body",
                position: null,
                type: "info",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: true,
                placement: {
                    from: "top",
                    align: "right",
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 6000,
                animate: {
                    enter: "animated fadeInDown",
                    exit: "animated fadeOutUp",
                },
                icon_type: "class",
                template: '<div data-notify="container" class="col-xxl-3 col-lg-5 col-md-6 col-sm-7 col-12 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="btn-close" data-notify="dismiss"></button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-info progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    "</div>" +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    "</div>",
            });
        });
    </script>
    {{ Session::forget('success') }}
    {{ Session::save() }}
    @endif
@endsection
