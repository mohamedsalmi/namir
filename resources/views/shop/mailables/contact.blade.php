<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Site Email</title>
</head>
<body>
    <h1>SITE EMAIL</h1>
    <p>Name: {{ $data['name'] }}</p>
    <p>LastName: {{ $data['lastname'] }}</p>
    <p>phone: {{ $data['phone'] }}</p>
    <p>Email: {{ $data['email'] }}</p>
    <p>Message: {{ $data['message'] }}</p>
</body>
</html>