@extends('shop.layouts.app')

@section('content')

    <!-- Breadcrumb Section Start -->
    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2>{{ __('app.form_contact.sign_in') }}</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('shop.home') }}">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">{{ __('app.form_contact.sign_in') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- log in section start -->
    <section class="log-in-section section-b-space">
        <div class="container-fluid-lg w-100">
            <div class="row">
                <div class="col-xxl-6 col-xl-5 col-lg-6 d-lg-block d-none ms-auto">
                    <div class="image-contain">
                        <img src="{{ asset('assets/shop/images/inner-page/register1.svg')}}" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-xxl-4 col-xl-5 col-lg-6 col-sm-8 mx-auto">
                    <div class="log-in-box">
                        <div class="log-in-title">
                            <h3>{{ __('app.commun.welcome_to') }} {{config('stock.info.ar_name')}}</h3>
                            <h4>{{ __('app.commun.create_new_account') }}</h4>
                        </div>

                        <div class="input-box">
                                {!! Form::open([
                                    'enctype' => 'multipart/form-data',
                                    'route' => 'shop.account.create',
                                    'method' => 'POST',
                                    'id' => 'account-create-form',
                                    'class' => 'row g-4',
                                ]) !!}
                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="{{ __('app.form_contact.name') }}" >
                                        <label for="name">{{ __('app.form_contact.name') }}</label>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="{{ __('app.form_contact.email_adress') }}" >
                                        <label for="email">{{ __('app.form_contact.email_adress') }}</label>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password"
                                          placeholder="{{ __('app.form_contact.pwd') }}">
                                        <label for="password">{{ __('app.form_contact.pwd') }}</label>
                                      
                                        
                                      @error('password')
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                      @enderror
                                    </div>
                                    
                                  </div>
                                <div class="col-12">
                                    <div class="row">

                                        <div class="col-12 form-floating theme-form-floating">
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" id="password_confirmation"
                                                placeholder=" {{ __('app.form_contact.pwd_confirmation') }}">
                                            <label for="password"> {{ __('app.form_contact.pwd_confirmation') }}</label>
                                            @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col eye">
                                        
                                            <button class="btn-show-password btn btn-secondary" type="button">
                                                <i class="fa fa-eye"></i>
                                              </button>
                                        </div>
                                    </div>
                                    </div>

                                <div class="col-12">
                                    <div class="forgot-box">
                                        <div class="form-check ps-0 m-0 remember-box">
                                            <input class="checkbox_animated check-box" type="checkbox"
                                                id="flexCheckDefault" required>
                                            <label class="form-check-label" for="flexCheckDefault">{{ __('app.form_contact.i_agree_with') }}  {{ __('app.commun.terms') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button class="btn btn-animation w-100" type="submit">{{ __('app.form_contact.sign_up') }}</button>
                                </div>
                                {!! Form::close() !!}
                        </div>

                        <div class="other-log-in">
                            <h6>{{ __('auth.login.or') }}</h6>
                        </div>

                        <div class="log-in-button">
                            <ul>
                                <li>
                                    <a href="{{ route('shop.provider.redirect', ['provider' => 'google']) }}"
                                        class="btn google-button w-100">
                                        <img src="{{ asset('assets/shop/images/inner-page/google.png')}}" class="blur-up lazyload"
                                            alt="">
                                            {{ __('auth.login.login_with_google') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('shop.provider.redirect', ['provider' => 'facebook']) }}" class="btn google-button w-100">
                                        <img src="{{ asset('assets/shop/images/inner-page/facebook.png')}}" class="blur-up lazyload"
                                            alt=""> {{ __('auth.login.login_with_facebook') }}
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="other-log-in">
                            <h6></h6>
                        </div>

                        <div class="sign-up-box">
                            <h4>{{ __('app.form_contact.already_have_an_account') }}</h4>
                            <a href="{{route('shop.login')}}">{{ __('app.form_contact.sign_in') }}</a>
                        </div>
                    </div>
                </div>

                <div class="col-xxl-7 col-xl-6 col-lg-6"></div>
            </div>
        </div>
    </section>
    <!-- log in section end -->
@endsection
@section('scripts')
<script>
    $('.btn-show-password').on('click', function() {
      var passwordField = $('#password');
      var passwordConfirmationField = $('#password_confirmation');
      var passwordFieldType = passwordField.attr('type');
      if (passwordFieldType === 'password') {
        passwordField.attr('type', 'text');
        passwordConfirmationField.attr('type', 'text');
        $(this).html('<i class="fa fa-eye-slash"></i>');
      } else {
        passwordField.attr('type', 'password');
        passwordConfirmationField.attr('type', 'password');
        $(this).html('<i class="fa fa-eye"></i>');
      }
    });
  </script>
@endsection

