@extends('shop.layouts.app')

@section('content')
    <!-- Cart Section Start -->
    <section class="cart-section section-b-space">
        <div class="container-fluid-lg">
            <div class="row g-sm-5 g-3">
                <div class="col-xxl-9">
                    <div class="cart-table" id="cart-table">
                        @if (!Session::has('cart') || count(Session::get('cart')) == 0)
                            <a class="dropdown-item">
                                <div class="text-center"> {{ __('app.cart.empty_cart') }}</div>
                            </a>
                            <a href= "{{ route('shop.index') }}"
                                class="btn btn-light shopping-button text-dark">
                                {{ __('app.cart.return_shop') }}
                            </a>
                        @else
                            <div class="table-responsive-xl">
                                <table class="table">
                                    <tbody>
                                        @foreach (Session::get('cart') as $key => $item)
                                            <tr class="product-box-contain">
                                                <td class="product-detail">
                                                    <div class="product border-0">
                                                        <a href="{{ route('shop.show', ['product' => $item['id'] ?? 1]) }}"
                                                            class="product-image">
                                                            <img src="{{ asset($item['default_image_url']) }}"
                                                                class="img-fluid blur-up lazyload"
                                                                alt="{{ $item['name'] }}">
                                                        </a>
                                                        <div class="product-detail">
                                                            <ul>
                                                                <li class="name">
                                                                    <a
                                                                        href="{{ route('shop.show', ['product' => $item['id'] ?? 1]) }}">{{ $item['name'] }}</a>
                                                                </li>

                                                                {{-- <li class="text-content"><span class="text-title">Sold
                                                            By:</span> Fresho</li> --}}
                                                            @if ($item['writer'] && $item['writer'] != '-')
                                                                <li class="text-content"><span
                                                                        class="text-title">{{ $item['writer'] }}</span>
                                                                </li>
                                                                @endif
                                                                @if ($item['house'] && $item['house'] != '-')
                                                                <li>
                                                                    <h5 class="text-content d-inline-block">
                                                                        {{ $item['house'] }}</h5>
                                                                </li>
                                                                @endif

                                                                {{-- <li>
                                                                    <h5 class="saving theme-color">Saving : $20.68</h5>
                                                                </li> --}}

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="price">
                                                    <h4 class="table-title text-content">{{__('app.cart.price')}}</h4>
                                                    @if($item['selling1'] > $item['selling2'] && $item['selling2'] != 0)
                                                        <h5 id="price-{{ $key }}">{{ number_format(($item['selling2'] / session('currency_value')), 3) }}</h5>
                                                        <h5><del class="text-content">{{ number_format($item['selling1'] / session('currency_value') ,3)}}</del></h5>
                                                        {{-- <h6 class="theme-color">You Save : $20.68</h6> --}}
                                                    @else
                                                        <h5 id="price-{{ $key }}">{{ number_format($item['selling1'] / session('currency_value') ,3)}}</h5>
                                                    @endif
                                                </td>

                                                <td class="quantity">
                                                    <h4 class="table-title text-content">{{__('app.cart.qty')}}</h4>
                                                    <div class="quantity-price">
                                                        <div class="cart_qty">
                                                            <div class="input-group">
                                                                <button type="button" class="btn qty-left-minus addToCart"
                                                                    data-product-id="{{ $item['id'] }}" data-type="minus"
                                                                    data-field="">
                                                                    <i class="fa fa-minus ms-0" aria-hidden="true"></i>
                                                                </button>
                                                                <input class="form-control input-number qty-input"
                                                                    id="qty-input-{{ $item['id'] }}" type="number"
                                                                    name="quantity" value="{{ $item['quantity'] }}"
                                                                    min="1" data-total="total-{{ $key }}"
                                                                    data-price="price-{{ $key }}" required>
                                                                <button type="button" class="btn qty-right-plus addToCart"
                                                                    data-product-id="{{ $item['id'] }}" data-type="plus"
                                                                    data-field="">
                                                                    <i class="fa fa-plus ms-0" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="subtotal">
                                                    <h4 class="table-title text-content">{{__('app.cart.total')}}</h4>
                                                    <h5 class="total" id="total-{{ $key }}">
                                                        {{ $item['selling1'] > $item['selling2'] && $item['selling2'] != 0 ? number_format(($item['selling2'] / session('currency_value')) * $item['quantity'], 3) : number_format(($item['selling1'] / session('currency_value')) * $item['quantity'], 3) }}</h5>
                                                </td>

                                                <td class="save-remove">
                                                    <h4 class="table-title text-content"></h4>
                                                    {{-- <h4 class="table-title text-content">{{__('app.cart.action')}}</h4> --}}
                                                    {{-- <a class="save notifi-wishlist" href="javascript:void(0)">Save for later</a> --}}
                                                    <a class="removeItem" href="#"
                                                        data-product-id="{{ $item['id'] }}">{{__('app.cart.remove')}}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-xxl-3">
                    <div class="summery-box p-sticky">
                        <div class="summery-header">
                            <h3>{{__('app.cart.cart_total')}}</h3>
                        </div>

                        <div class="summery-contain">
                            <div class="coupon-cart">
                                <h6 class="text-content mb-2">{{__('app.cart.coupon_apply')}}</h6>
                                <div class="mb-1 coupon-box input-group">
                                    <input type="email" class="form-control" id="coupon-code"
                                        placeholder="{{__('app.cart.enter_coupon')}}">
                                    <button class="btn-apply" id="apply-coupon">{{__('app.cart.apply')}}</button>
                                </div>
                                <div class="text-danger d-none invalid-coupon">{{__('app.cart.invalid_coupon')}}</div>
                            </div>
                            <ul>
                                <li>
                                    <h4>{{__('app.cart.subtotal')}}</h4>
                                    <h4 class="price"><span id="subtotal-amount"></span>
                                        {{ session('currency')->symbol }}</h4>
                                </li>

                                @php
                                    $discount = 0;
                                    $coupon_id = Session::has('coupon') && !empty(Session::get('coupon')) ? Session::get('coupon') : null;
                                    if($coupon_id){
                                        $coupon = App\Models\Coupon::find($coupon_id);
                                        $discount = $coupon->discount;
                                    }
                                @endphp

                                <li>
                                    <h4>{{__('app.cart.coupon_discount')}}</h4>
                                    <h4 class="price"><span data-discount="{{$discount}}" id="discount-percentage">({{$discount}}%)</span> <span id="discount-value">0</span></h4>
                                </li>

                                {{-- <li class="align-items-start">
                                    <h4>Shipping</h4>
                                    <h4 class="price text-end">$6.90</h4>
                                </li> --}}
                            </ul>
                        </div>

                        <ul class="summery-total">
                            <li class="list-total border-top-0">
                                <h4>{{ __('app.cart.total') }} ({{ session('currency')->symbol }})</h4>
                                <h4 class="price theme-color" id="total-amount"></h4>
                            </li>
                        </ul>

                        <div class="button-group cart-button">
                            <ul>
                                <li>
                                    <button onclick="location.href = '{{ route('shop.cart.checkout') }}';"
                                        class="btn btn-animation proceed-btn fw-bold">{{ __('app.cart.process_checkout') }}
                                        </button>
                                </li>

                                <li>
                                    <button onclick="location.href = '{{ route('shop.index') }}';"
                                        class="btn btn-light shopping-button text-dark">
                                        <i class="fa-solid fa-arrow-left-long"></i>{{ __('app.cart.return_shop') }}
                                        </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cart Section End -->
@endsection

@section('scripts')
    {{-- <script src="{{ asset('assets/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script> --}}
    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>

    <script>
        // $('.qty-left-minus').on('click', function() {
        //     console.log('minus');
        //     var $qty = $(this).siblings(".qty-input");
        //     var _val = $($qty).val();
        //     if (_val == '1') {
        //         var _removeCls = $(this).parents('.cart_qty');
        //         $(_removeCls).removeClass("open");
        //     }
        //     var currentVal = parseInt($qty.val());
        //     console.log(currentVal);
        //     if (!isNaN(currentVal) && currentVal > 1) {
        //         $qty.val(currentVal - 1);

        //         var current_total = $(this).next().attr('data-total');
        //         var current_price = $(this).next().attr('data-price');
        //         console.log(current_total, current_price, currentVal, $qty);
        //         $('#' + current_total).text((parseFloat($qty.val()) * parseFloat($('#' + current_price).text()))
        //             .toFixed(3));
        //         $(document).ready(function() {
        //             calculateTotal();
        //         });

        //     }
        // });

        // $('.qty-right-plus').click(function() {
        //     // console.log('plus');
        //     // if ($(this).prev().val() < 9) {
        //     var qty = $(this).prev().val(+parseFloat($(this).prev().val()) + 1).val();
        //     console.log(qty);
        //     var current_total = $(this).prev().attr('data-total');
        //     var current_price = $(this).prev().attr('data-price');
        //     $('#' + current_total).text((qty * parseFloat($('#' + current_price).text())).toFixed(3));
        //     $(document).ready(function() {
        //         calculateTotal();
        //     });
        //     // }
        // });

        $(document).on('change', '.qty-input', function(e) {
            var qty = $(this).val();
            if (qty == '' || qty == 0) {
                $(this).val(1);
                qty = 1;
            }
            var current_total = $(this).attr('data-total');
            var current_price = $(this).attr('data-price');
            $('#' + current_total).text((qty * parseFloat($('#' + current_price).text())).toFixed(3));
            $(document).ready(function() {
                calculateTotal();
            });
        });
    </script>

    <script>
        // function calculateTotal() {
        //     var total = 0;
        //     var discount = $('#discount-percentage').data('discount'); 
        //     $(".total").each(function() {
        //         total += parseFloat($(this).text().replace(',',''));
        //     });
        //     $('#subtotal-amount').text(total.toFixed(3));
        //     console.log(total);
        //     $('#discount-value').text((total * (discount / 100)).toFixed(3));
        //     discount != 0 ? total = total * (1 - discount / 100) : total;
        //     console.log(total);
        //     $('#total-amount').text(total.toFixed(3));
        // }

        $(document).ready(function() {
            calculateTotal();
        });

        $(document).ready(function() { 
            $(document).on('click', '.removeItem', function(e) {
                // $('.remove').click(function(e) {
                let _this = this;
                e.preventDefault();
                let product_id = $(this).attr('data-product-id');
                var product_qty = $('#qty-input-' + product_id).val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/shop/cart/removecartitem',
                    method: "GET",
                    dataType: "json",
                    data: {
                        product_id: product_id,
                        // quantity: product_qty ? product_qty : 1,
                    },
                    success: function(data) {
                        $(document).ready(function() {
                            $(_this).closest(".product-box-contain").fadeOut("slow",
                                function() {
                                    $(_this).closest(".product-box-contain")
                                    .remove();
                                    calculateTotal();
                                });
                        });

                        if (data.status === 'success') {
                            $('#item-section').html(data.item_section);
                            $('#cart-hover').html(data.cart_hover);
                            $('.close-button').on("click", function() {
                                $('.item-section').removeClass("active");
                            });
                        }

                        if (Object.keys(data.cart).length == 0) {
                            $("#cart-table").load(location.href + " #cart-table");
                        }

                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });

            $(document).on('click', '#apply-coupon', function(e) {
                $(".invalid-coupon").addClass('d-none');
                let _this = this;
                e.preventDefault();
                var code = $('#coupon-code').val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/shop/cart/coupon/check?code=' + code,
                    method: "GET",
                    dataType: "json",
                    success: function(data) {
                        var discount = 0;
                        if(data.success){
                            discount = data.coupon.discount
                        }else{
                            $(".invalid-coupon").removeClass('d-none');
                        }
                       
                        $('#discount-percentage').text('(' + discount + '%)');
                        $('#discount-percentage').data('discount', discount); 
                       
                        calculateTotal();

                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
        });
    </script>
@endsection
