@extends('shop.layouts.app')

@section('content')
@php
    if(!session()->has('cart') || count(session()->get('cart')) == 0){
        echo('<script>window.location = "/shop";</script>');
    }
@endphp
    <!-- Checkout section Start -->
    <section class="checkout-section-2 section-b-space">
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'shop.cart.order',
            'method' => 'POST',
            'id' => 'checkout-form',
        ]) !!}
        <div class="container-fluid-lg">
            <div class="row g-sm-4 g-3">
                <div class="col-lg-8">
                    <div class="left-sidebar-checkout">
                        <div class="checkout-detail-box">
                            <ul>
                                <li>
                                    <div class="checkout-icon">
                                        <lord-icon target=".nav-item" src="https://cdn.lordicon.com/ggihhudh.json"
                                            trigger="loop-on-hover"
                                            colors="primary:#121331,secondary:#646e78,tertiary:#0baf9a"
                                            class="lord-icon">
                                        </lord-icon>
                                    </div>
                                    <div class="checkout-box">
                                        <div class="checkout-title">
                                            <h4>{{__('checkout.delivery_address')}}</h4>
                                        </div>
                                        <div class="checkout-detail">
                                            
                                            <div class="row g-2" id="">
                                                <div class="col-xxl-6">
                                                    <div
                                                        class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                                        <input type="text" class="form-control @error('client_details.name') is-invalid @enderror" name="client_details[name]"
                                                            id="name" placeholder="{{__('checkout.name')}}" value="{{auth()->user()->name??''}}">
                                                        <label for="name">{{__('checkout.name')}}</label>
                                                        @error('client_details.name')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-xxl-6">
                                                    <div
                                                        class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                                        <input type="number" class="form-control @error('client_details.phone') is-invalid @enderror" id="phone" name="client_details[phone]"
                                                            placeholder="{{__('checkout.phone')}}" value="{{auth()->user()->phone??''}}">
                                                        <label for="phone">{{__('checkout.phone')}}</label>
                                                        @error('client_details.phone')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="payment-method">
                                                        <div
                                                            class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                                            <input type="text" class="form-control @error('client_details.adresse') is-invalid @enderror" name="client_details[adresse]"
                                                                id="address"
                                                                placeholder="{{__('checkout.address')}}" value="{{auth()->user()->adresse??''}}">
                                                            <label for="address">{{__('checkout.address')}}</label>
                                                            @error('client_details.adresse')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-6">
                                                    <div
                                                        class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                                        <select class="form-select single-select" id="state"
                                                        name="client_details[state]">
                                                        <option value="">{{__('checkout.choose_option')}}</option>
                                                        @foreach ($states as $state)
                                                            <option value="{{ $state->state }}"{{ old('client_details.state', (auth()->user()->state ?? '')) == $state->state ? 'selected' : '' }}>
                                                                {{ $state->state }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                        <label for="address">{{__('checkout.state')}}</label>
                                                        @error('client_details.state')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                   
                                                </div>

                                                <div class="col-6">
                                                    <div
                                                        class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                                        <select class="form-select single-select" id="city"
                                                        name="client_details[city]">
                                                        <option value="">{{__('checkout.choose_option')}}</option>
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->city }}"{{ old('client_details.city', (auth()->user()->city ?? '')) == $city->city ? 'selected' : '' }}>
                                                                {{ $city->city }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                        <label for="address">{{__('checkout.city')}}</label>
                                                        @error('client_details.city')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                   
                                                </div>

                                            </div>
                                            
                                        </div>
                                      
                                    </div>
                                </li>

                                <li>
                                    <div class="checkout-icon">
                                        <lord-icon target=".nav-item" src="https://cdn.lordicon.com/oaflahpk.json"
                                            trigger="loop-on-hover" colors="primary:#0baf9a" class="lord-icon">
                                        </lord-icon>
                                    </div>
                                    <div class="checkout-box">
                                        <div class="checkout-title">
                                            <h4>{{__('checkout.delivery_options')}}</h4>
                                        </div>

                                        <input type="hidden" name="client_details[delivery_price]" value="0" />

                                        <div class="checkout-detail">
                                            <div class="row g-4">
                                                <div class="col-xxl-6">
                                                    <div class="delivery-option">
                                                        <div class="delivery-category">
                                                            <div class="shipment-detail">
                                                                <div
                                                                    class="custom-form-check  mb-0">
                                                                    <label class="form-check-label"
                                                                        for="standard"><input class="form-check-input mt-0" type="radio"
                                                                        value="store_delivery_option" name="client_details[delivery]" id="standard" checked>{{__('checkout.store_delivery_option')}}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xxl-6">
                                                    <div class="delivery-option">
                                                        <div class="delivery-category">
                                                            <div class="shipment-detail">
                                                                <div
                                                                    class="custom-form-check mb-0">
                                                                    <label class="form-check-label" for="future"> <input class="form-check-input mt-0" type="radio"
                                                                        value="home_delivery_option" name="client_details[delivery]" id="future">{{__('checkout.home_delivery_option')}}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 future-box">
                                                    <div class="future-option">
                                                        <div class="row g-md-0 gy-4">
                                                            <div class="col-md-6">
                                                                <div class="delivery-items">
                                                                    <div>
                                                                        <h5 class="items text-content"><span>3
                                                                                Items</span>@
                                                                            $693.48</h5>
                                                                        <h5 class="charge text-content">Delivery Charge
                                                                            $34.67
                                                                            <button type="button" class="btn p-0"
                                                                                data-bs-toggle="tooltip"
                                                                                data-bs-placement="top"
                                                                                title="Extra Charge">
                                                                                <i
                                                                                    class="fa-solid fa-circle-exclamation"></i>
                                                                            </button>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <form
                                                                    class="form-floating theme-form-floating date-box">
                                                                    <input type="date" class="form-control">
                                                                    <label>Select Date</label>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="checkout-icon">
                                        <lord-icon target=".nav-item" src="https://cdn.lordicon.com/qmcsqnle.json"
                                            trigger="loop-on-hover" colors="primary:#0baf9a,secondary:#0baf9a"
                                            class="lord-icon">
                                        </lord-icon>
                                    </div>
                                    <div class="checkout-box">
                                        <div class="checkout-title">
                                            <h4>{{__('checkout.payment_options')}}</h4>
                                        </div>

                                        <div class="checkout-detail">
                                            <div class="accordion accordion-flush custom-accordion"
                                                id="accordionFlushExample">
                                                <div class="accordion-item">
                                                    <div class="accordion-header" id="flush-headingFour">
                                                        <div class="accordion-button collapsed"
                                                            data-bs-toggle="collapse"
                                                            data-bs-target="#flush-collapseFour">
                                                            <div class="custom-form-check form-check mb-0">
                                                                <label class="form-check-label" for="cash"><input
                                                                        class="form-check-input mt-0" type="radio"
                                                                        name="flexRadioDefault" id="cash" checked>{{__('checkout.cash_on_delivery')}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="flush-collapseFour"
                                                        class="accordion-collapse collapse show"
                                                        data-bs-parent="#accordionFlushExample">
                                                        <div class="accordion-body">
                                                            <p class="cod-review">{{__('checkout.cash_on_delivery_details')}}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="right-side-summery-box">
                        <div class="summery-box-2">
                            <div class="summery-header">
                                <h3>{{__('checkout.order_summary')}}</h3>
                            </div>

                            <ul class="summery-contain">
                                @if(Session::has('cart') && !empty(Session::get('cart')))
                                @foreach (Session::get('cart') as $key => $item)
                                <li>
                                    <img src="{{ asset($item['default_image_url']) }}"
                                        class="img-fluid blur-up lazyloaded checkout-image" alt="{{ $item['name'] }}">
                                    <h4><strong>{{ $item['quantity'] }} </strong> x <span>{{ $item['name'] }}</span></h4>
                                    {{-- <input type="hidden" name="items[][product_id]" value="{{$item['id']}}">
                                    <input type="hidden" name="items[][product_quantity]" value="{{$item['quantity']}}"> --}}
                                    <h4 data-item-total="{{ $item['selling1'] > $item['selling2'] && $item['selling2'] != 0 > 0 ? number_format($item['selling2'] / session('currency_value'), 3) * $item['quantity'] : $item['selling1'] / session('currency_value') * $item['quantity'] }}" class="price total">
                                        {{ $item['selling1'] > $item['selling2'] && $item['selling2'] != 0 ? number_format(($item['selling2'] / session('currency_value')) * $item['quantity'], 3) : number_format(($item['selling1'] / session('currency_value')) * $item['quantity'], 3) }}
                                    </h4>
                                    {{-- @if ($item['discount'] != 0)
                                    <h4 id="price-{{ $key }}">{{ $item['discount'] }} <del
                                            class="text-content">{{ $item['selling1'] }}</del></h4>
                                @else
                                    <h4 id="price-{{ $key }}">{{ $item['selling1'] }}</h4>
                                @endif --}}
                                </li>
                                @endforeach
                                @endif
                            </ul>

                            <ul class="summery-total">
                                <li>
                                    {{-- <h4>Subtotal</h4>
                                    <h4 class="price">$111.81</h4> --}}
                                    <h4>{{__('app.cart.subtotal')}}</h4>
                                    <h4 class="price"><span id="subtotal-amount">0</span>
                                        {{ session('currency')->symbol }}</h4>
                                </li>

                                <li>
                                    <h4>{{__('checkout.shipping')}}</h4>
                                    <h4 class="price" id="shipping">0</h4>
                                </li>

                                {{-- <li>
                                    <h4>Tax</h4>
                                    <h4 class="price">$29.498</h4>
                                </li> --}}

                                <li>
                                    {{-- <h4>Coupon/Code</h4>
                                    <h4 class="price">$-23.10</h4> --}}
                                    @php
                                        $discount = 0;
                                        $coupon_id = Session::has('coupon') && !empty(Session::get('coupon')) ? Session::get('coupon') : null;
                                        if($coupon_id){
                                            $coupon = App\Models\Coupon::find($coupon_id);
                                            $discount = $coupon->discount;
                                        }
                                    @endphp
                                    <h4>{{__('app.cart.coupon_discount')}}</h4>
                                    <h4 class="price"><span data-discount="{{$discount}}" id="discount-percentage">({{$discount}}%)</span> <span id="discount-value">{{$discount}}</span></h4>
                                </li>

                                <li class="list-total">
                                    {{-- <h4>Total (USD)</h4>
                                    <h4 class="price">$19.28</h4> --}}
                                    <h4>{{ __('app.cart.total') }} ({{ session('currency')->symbol }})</h4>
                                    <h4 class="price theme-color" id="total-amount">0</h4>
                                </li>
                            </ul>
                        </div>

                        {{-- <div class="checkout-offer">
                            <div class="offer-title">
                                <div class="offer-icon">
                                    <img src="{{asset('assets/shop/images/inner-page/offer.svg')}}" class="img-fluid" alt="">
                                </div>
                                <div class="offer-name">
                                    <h6>Available Offers</h6>
                                </div>
                            </div>

                            <ul class="offer-detail">
                                <li>
                                    <p>Combo: BB Royal Almond/Badam Californian, Extra Bold 100 gm...</p>
                                </li>
                                <li>
                                    <p>combo: Royal Cashew Californian, Extra Bold 100 gm + BB Royal Honey 500 gm</p>
                                </li>
                            </ul>
                        </div> --}}

                        <button type="submit" class="btn theme-bg-color text-white btn-md w-100 mt-4 fw-bold">{{__('checkout.order')}}</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <!-- Checkout section End -->
    @endsection

    @section('scripts')
    <script>
        function calculateTotal(shipping = 0) {
            var total = 0;
            var discount = $('#discount-percentage').data('discount'); 
            $(".total").each(function() {
                total += parseFloat($(this).data('item-total'));
            });
            $('#subtotal-amount').text(total.toFixed(3));

            $('#shipping').text(shipping.toFixed(3));
            
            $('#discount-value').text((total * (discount / 100)).toFixed(3));
            discount != 0 ? total = total * (1 - discount / 100) : total;
            total += shipping;
            $('#total-amount').text(total.toFixed(3));
        }

        $(document).ready(function() {
            calculateTotal();
        });
    </script>

    <script>
        $(document).ready(function () {
        $("#state").change(function () {
            var val = $(this).val();
            var cities = @json($cities);
            var options = '';
            cities.filter(el => el.state == val).forEach(city => {
                options += "<option value='"+ city.city + "'>"+ city.city + "</option>"
            });

            $("#city").html(options);
        });

        @php
           $shipping_price = \App\Models\Setting::where('name', 'Prix de livraison')->first()->value ?? 7;
        @endphp

        $("input[name='client_details[delivery]']").change(function () {
            var val = $("input[name='client_details[delivery]']:checked").val();
            var shipping = @json($shipping_price);
            console.log(shipping);
            $("input[name='client_details[delivery_price]']").val(shipping);
            if(val == 'home_delivery_option'){
                calculateTotal(parseFloat(shipping));
            }else{
                calculateTotal(0);
            }
        });
        
    });
    </script>
    @endsection