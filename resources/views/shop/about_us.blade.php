@extends('shop.layouts.app')
@section('title', __('app.commun.about_us'))
@section('content')
    <!-- Breadcrumb Section Start -->
    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2>{{ __('app.commun.about_us') }}</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="index.html">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{ __('app.commun.about_us') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Fresh Vegetable Section Start -->
    <section class="fresh-vegetable-section section-lg-space">
        <div class="container-fluid-lg">
            <div class="row gx-xl-5 gy-xl-0 g-3 ratio_148_1">
                <div class="col-xl-6 col-12">
                    <div class="row g-sm-4 g-2">
                        <div class="col-6">
                            <div class="fresh-image-2">
                                <div>
                                    <img src="{{ asset('assets/shop/images/inner-page/about-us/1.jpg') }}"
                                        class="bg-img blur-up lazyload" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="fresh-image">
                                <div>
                                    <img src="{{ asset('assets/shop/images/inner-page/about-us/2.jpg') }}"
                                        class="bg-img blur-up lazyload" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-12">
                    <div class="fresh-contain p-center-left">
                        <div>
                            <div class="review-title">
                                <h4>{{ __('app.commun.about_us') }}</h4>
                                <h2>نحن نجعل وصولك للكتاب أسهل</h2>
                            </div>

                            <div class="delivery-list">
                                <p class="text-content">دار المازري لأحباء الكتب! مكتبة ودار نشر، ننشر ونوزع كل نافع ومفيد،
                                    بأفضل الطبعات وأجودها، نرسل الكتب لكل العالم بأفضل الأسعار!</p>

                                <ul class="delivery-box">
                                    <li>
                                        <div class="delivery-box">
                                            <div class="delivery-icon">
                                                <img src="{{ asset('assets/shop/svg/3/delivery.svg') }}"
                                                    class="blur-up lazyload" alt="">
                                            </div>

                                            <div class="delivery-detail">
                                                <h5 class="text">Free delivery for all orders</h5>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="delivery-box">
                                            <div class="delivery-icon">
                                                <img src="{{ asset('assets/shop/svg/3/leaf.svg') }}"
                                                    class="blur-up lazyload" alt="">
                                            </div>

                                            <div class="delivery-detail">
                                                <h5 class="text">Only fresh foods</h5>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="delivery-box">
                                            <div class="delivery-icon">
                                                <img src="{{ asset('assets/shop/svg/3/delivery.svg') }}"
                                                    class="blur-up lazyload" alt="">
                                            </div>

                                            <div class="delivery-detail">
                                                <h5 class="text">Free delivery for all orders</h5>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="delivery-box">
                                            <div class="delivery-icon">
                                                <img src="{{ asset('assets/shop/svg/3/leaf.svg') }}"
                                                    class="blur-up lazyload" alt="">
                                            </div>

                                            <div class="delivery-detail">
                                                <h5 class="text">Only fresh foods</h5>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Fresh Vegetable Section End -->
    <!-- Client Section Start -->
    <section class="client-section section-lg-space">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="about-us-title text-center">
                        <h4>What We Do</h4>
                        <h2 class="center">We are Trusted by Clients</h2>
                    </div>

                    <div class="slider-3_1 product-wrapper">
                        <div>
                            <div class="clint-contain">
                                <div class="client-icon">
                                    <img src="{{ asset('assets/shop/svg/3/work.svg') }}" class="blur-up lazyload"
                                        alt="">
                                </div>
                                <h2>10</h2>
                                <h4>Business Years</h4>
                                <p>A coffee shop is a small business that sells coffee, pastries, and other morning
                                    goods. There are many different types of coffee shops around the world.</p>
                            </div>
                        </div>

                        <div>
                            <div class="clint-contain">
                                <div class="client-icon">
                                    <img src="{{ asset('assets/shop/svg/3/buy.svg') }}" class="blur-up lazyload"
                                        alt="">
                                </div>
                                <h2>80 K+</h2>
                                <h4>Products Sales</h4>
                                <p>Some coffee shops have a seating area, while some just have a spot to order and then
                                    go somewhere else to sit down. The coffee shop that I am going to.</p>
                            </div>
                        </div>

                        <div>
                            <div class="clint-contain">
                                <div class="client-icon">
                                    <img src="{{ asset('assets/shop/svg/3/user.svg') }}" class="blur-up lazyload"
                                        alt="">
                                </div>
                                <h2>90%</h2>
                                <h4>Happy Customers</h4>
                                <p>My goal for this coffee shop is to be able to get a coffee and get on with my day.
                                    It's a Thursday morning and I am rushing between meetings.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Client Section End -->




    <!-- Review Section Start -->
    <section class="review-section section-lg-space mt-5 mb-5">
        <div class="container-fluid">
            <div class="about-us-title text-center">
                <h4 class="text-content">{{ __('app.commun.testimonials') }}</h4>
                <h2 class="center">{{ __('app.commun.what_they_say') }}</h2>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slider-4-half product-wrapper">
                        @foreach ($testimonials as $key => $testimonial)
                            <div>
                                <div class="reviewer-box">
                                    <i class="fa-solid fa-quote-right"></i>
                                    <div class="product-rating">
                                        <ul class="rating">

                                            @for ($i = 1; $i < $testimonial->rating + 1; $i++)
                                                <li>
                                                    <i data-feather="star" class="fill"></i>
                                                </li>
                                            @endfor
                                            @for ($i = $testimonial->rating + 1; $i < 6; $i++)
                                                <li>
                                                    <i data-feather="star"></i>
                                                </li>
                                            @endfor

                                        </ul>
                                    </div>

                                    <h3>{{ $testimonial->name }}</h3>

                                    <p>"{{ $testimonial->testimonial }}"</p>

                                    <div class="reviewer-profile">
                                        <div class="reviewer-image">
                                            <img src="{{ asset($testimonial->image_url) }}" class="blur-up lazyload"
                                                alt="">
                                        </div>

                                        <div class="reviewer-name">
                                            <h4>{{ $testimonial->writer_details['name'] }}</h4>
                                            <h6>{{ $testimonial->writer_details['title'] }},
                                                {{ $testimonial->writer_details['company'] }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Review Section End -->
    @php
        $partners = Helper::get_partners();
    @endphp
    <!-- Team Section Start -->
    <section class="team-section section-lg-space">
        <div class="container-fluid-lg">
            <div class="about-us-title text-center">
                <h4 class="text-content">{{__('app.about_us.our_confident_partners')}}</h4>
                <h2 class="center"> {{ str_replace('_', '-', app()->getLocale()) == 'ar' ?  __('app.about_us.partners').' '. config('stock.info.ar_name') : config('stock.info.name') .' '. __('app.about_us.partners')}} </h2>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slider-user product-wrapper">
                        @foreach ($partners as $partner)
                            <div>
                                <a href="{{$partner->url}}">
                                    <div class="team-box">
                                        <div class="team-iamge">
                                            <img src="{{ asset($partner->image_url) }}"
                                                class="img-fluid blur-up lazyload" alt="">
                                        </div>    
                                        <div class="team-name">
                                            <h3>{{$partner->name}}</h3>                                   
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->
@endsection
