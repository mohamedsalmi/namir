@extends('shop.layouts.app')

@section('stylesheet')
<style>
#click-me {
  -webkit-transition: all 0.35s ease-in-out;
  -moz-transition: all 0.35s ease-in-out;
  transition: all 0.35s ease-in-out;
  /* -webkit-border-radius: 50%;
  -moz-border-radius: 50%; */
  /* border-radius: 50%; */
  -moz-background-clip: padding;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  -webkit-box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.3);
  box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.3);
  /* position: fixed;
  right: 50px;
  bottom: 40px;
  height: 55px;
  width: 55px; */
  line-height: 15px;
  font-size: 20px;
  padding: 10px;
  border: 0;
  color: #fff;
  text-align: center;
  /* background: #c54987; */
  cursor: pointer;
  /* border: 1px solid #c54987; */
  -moz-animation: shakeme 3s infinite;
  -o-animation: shakeme 3s infinite;
  -webkit-animation: shakeme 3s infinite;
  animation: shakeme 3s infinite;
}

@keyframes shakeme {
  0% {
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -ms-transform: scale(1);
    -o-transform: scale(1);
  }
  5% {
    -webkit-transform: scale(0.7);
    -moz-transform: scale(0.7);
    -ms-transform: scale(0.7);
    -o-transform: scale(0.7);
  }
  10% {
    -webkit-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -ms-transform: scale(1.5);
    -o-transform: scale(1.5);
  }
  15% {
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -ms-transform: scale(1.1);
    -o-transform: scale(1.1);
  }
  20% {
    -webkit-transform: scale(1.4);
    -moz-transform: scale(1.4);
    -ms-transform: scale(1.4);
    -o-transform: scale(1.4);
  }
  30% {
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -ms-transform: scale(1.1);
    -o-transform: scale(1.1);
  }
}
</style>
@endsection

@section('content')
    <!-- Breadcrumb Section Start -->
    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2>{{ __('app.profile.user_dashboard') }}</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="index.html">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{ __('app.profile.user_dashboard') }}
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- User Dashboard Section Start -->
    <section class="user-dashboard-section section-b-space">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-xxl-3 col-lg-4">
                    <div class="dashboard-left-sidebar">
                        <div class="close-button d-flex d-lg-none">
                            <button class="close-sidebar">
                                <i class="fa-solid fa-xmark"></i>
                            </button>
                        </div>
                        <div class="profile-box">
                            <div class="cover-image">
                                <img src="{{ asset('assets/shop/images/inner-page/cover-img.jpg') }}"
                                    class="img-fluid blur-up lazyload" alt="">
                            </div>

                            <div class="profile-contain">
                                <div class="profile-image">
                                    <div class="position-relative">
                                        <img src="{{ asset(auth()->user()->image_url) }}"
                                            class="blur-up lazyload update_img" alt="">
                                        <div class="cover-icon">
                                            <i class="fa-solid fa-pen">
                                                <form id="avatar-form" enctype="multipart/form-data">
                                                    <input type="file" accept="image/*" onchange="readURL(this,0)" id="avatar">
                                                </form>
                                            </i>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile-name">
                                    <h3>{{ auth()->user()->name }}</h3>
                                    <h6 class="text-content">{{ auth()->user()->email }}</h6>
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-pills user-nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-dashboard-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-dashboard" type="button" role="tab"
                                    aria-controls="pills-dashboard" aria-selected="true"><i data-feather="home"></i>
                                    {{ __('app.profile.user_dashboard') }}</button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-order-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-order" type="button" role="tab" aria-controls="pills-order"
                                    aria-selected="false"><i
                                        data-feather="shopping-bag"></i>{{ __('app.commun.your_order') }}</button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-wishlist-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-wishlist" type="button" role="tab"
                                    aria-controls="pills-wishlist" aria-selected="false"><i data-feather="heart"></i>
                                    {{ __('app.profile.wishlist') }}</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-profile" type="button" role="tab"
                                    aria-controls="pills-profile" aria-selected="false"><i data-feather="user"></i>
                                    {{ __('app.profile.profile') }}</button>
                            </li>

                            {{-- <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-security-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-security" type="button" role="tab"
                                    aria-controls="pills-security" aria-selected="false"><i data-feather="shield"></i>
                                    Privacy</button>
                            </li> --}}
                        </ul>
                    </div>
                </div>

                <div class="col-xxl-9 col-lg-8">
                    <button id="click-me" class="btn left-dashboard-show btn-animation btn-md fw-bold d-block mb-4 d-lg-none">{{ __('app.commun.show_menu') }}</button>
                    <div class="dashboard-right-sidebar">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-dashboard" role="tabpanel"
                                aria-labelledby="pills-dashboard-tab">
                                <div class="dashboard-home">
                                    <div class="title">
                                        <h2>{{ __('app.profile.my_dashboard') }}</h2>
                                        <span class="title-leaf">
                                            <img class="icon-width"  src="{{ asset('assets/shop/svg/read-book-icon.svg') }}" alt="">
                                        </span>
                                    </div>

                                    <div class="dashboard-user-name">
                                        <h6 class="text-content">السلام عليكم و رحمة اللّه و بركاته مرحبا, <b
                                                class="text-title">{{ auth()->user()->name }}</b></h6>
                                        {{-- <p class="text-content">{{ __('app.profile.intro') }}</p> --}}
                                    </div>

                                    <div class="total-box">
                                        <div class="row g-sm-4 g-3">
                                            <div class="col-xxl-4 col-lg-6 col-md-4 col-sm-6">
                                                <a href="{{route('shop.order')}}">
                                                    <div class="totle-contain">
                                                        <img src="{{ asset('assets/shop/images/svg/order.svg') }}"
                                                            class="img-1 blur-up lazyload" alt="">
                                                        <img src="{{ asset('assets/shop/images/svg/order.svg') }}"
                                                            class="blur-up lazyload image-color" alt="">
                                                        <div class="totle-detail">
                                                            <h5>{{ __('app.profile.total_order') }}</h5>
                                                            <h3>{{ isset($orders_number)? $orders_number:''}}</h3>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-xxl-4 col-lg-6 col-md-4 col-sm-6">
                                                <a href="{{route('shop.order')}}">
                                                    <div class="totle-contain">
                                                        <img src="{{ asset('assets/shop/images/svg/pending.svg') }}"
                                                            class="img-1 blur-up lazyload" alt="">
                                                        <img src="{{ asset('assets/shop/images/svg/pending.svg') }}"
                                                            class="blur-up lazyload image-color" alt="">
                                                        <div class="totle-detail">
                                                            <h5>{{ __('app.profile.total_success_order') }}</h5>
                                                            <h3>{{ isset($confirmed_orders_number)? $confirmed_orders_number:''}}</h3>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-xxl-4 col-lg-6 col-md-4 col-sm-6">
                                                <a href="{{route('shop.wishlist')}}">
                                                    <div class="totle-contain">
                                                        <img src="{{ asset('assets/shop/images/svg/wishlist.svg') }}"
                                                            class="img-1 blur-up lazyload" alt="">
                                                        <img src="{{ asset('assets/shop/images/svg/wishlist.svg') }}"
                                                            class="blur-up lazyload image-color" alt="">
                                                        <div class="totle-detail">
                                                            <h5>{{ __('app.profile.total_wishlist') }}</h5>
                                                            <h3>{{ isset($wishlists_number)? $wishlists_number:''}}</h3>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="dashboard-title">
                                        <h3>{{ __('app.profile.account_info') }}</h3>
                                    </div>

                                    <div class="row g-4">
                                        <div class="col-xxl-6">
                                            <div class="dashboard-contant-title">
                                                <h4>{{ __('app.profile.contact_info') }}<a href="javascript:void(0)"
                                                        data-bs-toggle="modal" data-bs-target="#editProfile">{{ __('app.commun.edit') }}</a>
                                                </h4>
                                            </div>
                                            <div class="dashboard-detail">
                                                <h6 class="text-content">{{ auth()->user()->name }}</h6>
                                                <h6 class="text-content">{{ auth()->user()->email }}</h6>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade show" id="pills-wishlist" role="tabpanel"
                                aria-labelledby="pills-wishlist-tab">
                                <div class="dashboard-wishlist">
                                    <div class="title">
                                        <h2>{{ __('app.commun.your_wishlist') }}</h2>
                                        <span class="title-leaf title-leaf-gray">
                                            <img class="icon-width"  src="{{ asset('assets/shop/svg/read-book-icon.svg') }}" alt="">
                                        </span>
                                    </div>
                                    <div class="row g-sm-4 g-3">
                                        @if (isset($wishlists))
                                            @foreach ($wishlists as $key => $item)
                                                <div class="col-xxl-3 col-lg-6 col-md-4 col-sm-6">
                                                    @include('shop.layouts.card', ['product' => $item->product])
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade show" id="pills-order" role="tabpanel"
                                aria-labelledby="pills-order-tab">
                                <div class="dashboard-order">
                                    <div class="title">
                                        <h2>{{ __('app.commun.your_order') }}</h2>
                                        <span class="title-leaf title-leaf-gray">
                                            <img class="icon-width"  src="{{ asset('assets/shop/svg/read-book-icon.svg') }}" alt="">
                                        </span>
                                    </div>

                                    <div class="order-contain">
                                        @if (isset($orders))
                                            @foreach ($orders as $key => $order)
                                                <div class="order-box dashboard-bg-box">
                                                    <div class="order-container">
                                                        <div class="order-icon">
                                                            <i data-feather="box"></i>
                                                        </div>

                                                        <div class="order-detail">
                                                            <h4>{{ __('الحالة') }} : <span class=" {{ $order->status === 'livrée' ? 'success-bg' : ($order->status === 'en cours' ? 'badge-warning' :($order->status === 'retour' ? 'badge-danger' : 'badge-info')) }}">{{ $order->status }}</span></h4>
                                                            <h6 class="text-content">
                                                                {{ $order->created_at->format('y-m-d H:i:s') }}</h6>
                                                        </div>
                                                    </div>

                                                    <div class="product-order-detail">
                                                        <div class="order-wrap">
                                                            <a href="{{ route('shop.order.show', $order->id) }}">
                                                                <ul class="product-size">
                                                                    <li>
                                                                        <div class="size-box">
                                                                            <h6 class="text-content">المبلغ الإجمالي : </h6>
                                                                            <h5>{{ $order->total }}DM</h5>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="size-box">
                                                                            <h6 class="text-content">التخفيض : </h6>
                                                                            <h5>{{ $order->items->first()->product_remise }}%
                                                                            </h5>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="size-box">
                                                                            <h6 class="text-content">عدد المنتجات :
                                                                            </h6>
                                                                            <h5>{{ $order->items->count() }}</h5>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade show" id="pills-profile" role="tabpanel"
                                aria-labelledby="pills-profile-tab">
                                <div class="dashboard-profile">
                                    <div class="title">
                                        <h2>{{ __('app.profile.profile') }}</h2>
                                        <span class="title-leaf">
                                            <img class="icon-width"  src="{{ asset('assets/shop/svg/read-book-icon.svg') }}" alt="">
                                        </span>
                                    </div>

                                    <div class="profile-detail dashboard-bg-box">
                                        <div class="dashboard-title">
                                            <h3>{{ __('app.profile.profile') }}</h3>
                                        </div>
                                        <div class="profile-name-detail">
                                            <div class="d-sm-flex align-items-center d-block">
                                                <h3>{{ auth()->user()->name }}</h3>
                                            </div>

                                            <a href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#editProfile">{{ __('app.commun.edit') }}</a>
                                        </div>

                                        <div class="location-profile">
                                            <ul>
                                                @if (auth()->user()->adresse)
                                                    <li>
                                                        <div class="location-box">
                                                            <i data-feather="map-pin"></i>
                                                            <h6>{{ auth()->user()->adresse }}</h6>
                                                        </div>
                                                    </li>
                                                @endif

                                                <li>
                                                    <div class="location-box">
                                                        <i data-feather="mail"></i>
                                                        <h6>{{ auth()->user()->email }}</h6>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="profile-about dashboard-bg-box">
                                        <div class="row">
                                            <div class="col-xxl-7">
                                                <div class="dashboard-title mb-3">
                                                    <h3>{{ __('app.profile.contact_info') }}</h3>
                                                </div>

                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            {{-- <tr>
                                                                <td>MF :</td>
                                                                <td>{{ auth()->user()->mf }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>RIB :</td>
                                                                <td>{{ auth()->user()->rib }}</td>
                                                            </tr> --}}
                                                            <tr>
                                                                <td>رقم الهاتف :</td>
                                                                <td>
                                                                    <a href="javascript:void(0)">
                                                                        {{ auth()->user()->phone }}</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>رقم الهاتف الثاني :</td>
                                                                <td>
                                                                    <a
                                                                        href="javascript:void(0)">{{ auth()->user()->phone2 }}</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>العنوان :</td>
                                                                <td>
                                                                    {{ auth()->user()->adresse }}
                                                                    <p class="mb-1"><strong>{{auth()->user()->city ?? ''}} {{auth()->user()->state ?? ''}}</strong></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="dashboard-title mb-3">
                                                    <h3>تفاصيل تسجيل الدخول</h3>
                                                </div>

                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>البريد الإلكتروني :</td>
                                                                <td>
                                                                    <a
                                                                        href="javascript:void(0)">{{ auth()->user()->email }}</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>كلمة المرور :</td>
                                                                <td>
                                                                    <a href="javascript:void(0)">●●●●●●</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-xxl-5">
                                                <div class="profile-image">
                                                    <img src="{{ asset('assets/shop/images/inner-page/log-in.png') }}"
                                                        class="img-fluid blur-up lazyload " alt="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            {{-- <div class="tab-pane fade show" id="pills-security" role="tabpanel"
                                aria-labelledby="pills-security-tab">
                                <div class="dashboard-privacy">
                                    <div class="dashboard-bg-box">
                                        <div class="dashboard-title mb-4">
                                            <h3>Privacy</h3>
                                        </div>

                                        <div class="privacy-box">
                                            <div class="d-flex align-items-start">
                                                <h6>Allows others to see my profile</h6>
                                                <div class="form-check form-switch switch-radio ms-auto">
                                                    <input class="form-check-input" type="checkbox" role="switch"
                                                        id="redio" aria-checked="false">
                                                    <label class="form-check-label" for="redio"></label>
                                                </div>
                                            </div>

                                            <p class="text-content">all peoples will be able to see my profile</p>
                                        </div>

                                        <div class="privacy-box">
                                            <div class="d-flex align-items-start">
                                                <h6>who has save this profile only that people see my profile</h6>
                                                <div class="form-check form-switch switch-radio ms-auto">
                                                    <input class="form-check-input" type="checkbox" role="switch"
                                                        id="redio2" aria-checked="false">
                                                    <label class="form-check-label" for="redio2"></label>
                                                </div>
                                            </div>

                                            <p class="text-content">all peoples will not be able to see my profile</p>
                                        </div>

                                        <button class="btn theme-bg-color btn-md fw-bold mt-4 text-white">Save
                                            Changes</button>
                                    </div>

                                    <div class="dashboard-bg-box mt-4">
                                        <div class="dashboard-title mb-4">
                                            <h3>Account settings</h3>
                                        </div>

                                        <div class="privacy-box">
                                            <div class="d-flex align-items-start">
                                                <h6>Deleting Your Account Will Permanently</h6>
                                                <div class="form-check form-switch switch-radio ms-auto">
                                                    <input class="form-check-input" type="checkbox" role="switch"
                                                        id="redio3" aria-checked="false">
                                                    <label class="form-check-label" for="redio3"></label>
                                                </div>
                                            </div>
                                            <p class="text-content">Once your account is deleted, you will be logged out
                                                and will be unable to log in back.</p>
                                        </div>

                                        <div class="privacy-box">
                                            <div class="d-flex align-items-start">
                                                <h6>Deleting Your Account Will Temporary</h6>
                                                <div class="form-check form-switch switch-radio ms-auto">
                                                    <input class="form-check-input" type="checkbox" role="switch"
                                                        id="redio4" aria-checked="false">
                                                    <label class="form-check-label" for="redio4"></label>
                                                </div>
                                            </div>

                                            <p class="text-content">Once your account is deleted, you will be logged out
                                                and you will be create new account</p>
                                        </div>

                                        <button class="btn theme-bg-color btn-md fw-bold mt-4 text-white">Delete My
                                            Account</button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- User Dashboard Section End -->
    @include('shop.partials.modals.profile-modal')
@endsection
@section('scripts')
    <script>
        $(document).on("change", "#avatar", function(e) {
            var url = "/avatarchange";
            var fd = new FormData();
            var files = $('#avatar')[0].files;

            // Check file selected or not
            if (files.length > 0) {
                fd.append('avatar', files[0]);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if (response != 0) {
                            $("#img").attr("src", response);
                            $(".preview img").show(); // Display image element
                        } else {
                            alert('file not uploaded');
                        }
                    },
                });
            } else {
                alert("Please select a file.");
            }
        });

        $(document).on("click", "#save", function(e) {
            submit(e);
        });
        var errors = null;

        function submit(e, options = '') {
            e.preventDefault();
            var Form = $('#update-profil-form');
            var url = "/valdationprofile";
            if (options != '') {
                var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
                Form.append(input);
            }
            if (errors) {
                $.each(errors, function(key, value) {
                    var element = document.getElementById(key);
                    if (element) {
                        element.classList.remove('is-invalid')
                    };
                });
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data: Form.serialize(),
                async: false,
                success: function(response, textStatus, jqXHR) {
                    Form.submit();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    errors = jqXHR.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        var element = document.getElementById(key);
                        if (element) {
                            element.classList.add('is-invalid')
                        };
                    });
                }
            });

        }
    </script>
    @switch($nav)
        @case('profile')
            <script>
                $(document).ready(function() {
                    var triggerEl = document.getElementById('pills-profile-tab');
                    var tabTrigger = new bootstrap.Tab(triggerEl);
                    bootstrap.Tab.getInstance(triggerEl).show();
                });
            </script>
        @break

        @case('wishlist')
            <script>
                $(document).ready(function() {
                    var triggerEl = document.getElementById('pills-wishlist-tab');
                    var tabTrigger = new bootstrap.Tab(triggerEl);
                    bootstrap.Tab.getInstance(triggerEl).show();

                });
            </script>
        @break

        @case('order')
            <script>
                $(document).ready(function() {
                    var triggerEl = document.getElementById('pills-order-tab');
                    var tabTrigger = new bootstrap.Tab(triggerEl);
                    bootstrap.Tab.getInstance(triggerEl).show();
                });
            </script>
        @break
    @endswitch
    <script>
        $(document).on('click', '#pills-wishlist-tab', function() {
            // window.location.href = "{{ route('shop.wishlist') }}";
            $("#pills-wishlist").load('/wishlist' + " #pills-wishlist");

        });
        $(document).on('click', '#pills-order-tab', function() {
            // window.location.href = "{{ route('shop.order') }}";
            $("#pills-order").load('/order' + " #pills-order");

        });
        $(document).on('click', '#pills-dashboard-tab', function() {
            // window.location.href = "{{ route('shop.order') }}";
            $("#pills-dashboard").load('/dashboard' + " #pills-dashboard");

        });
    </script>

    <script>
        $(document).ready(function () {
            $("#state").change(function () {
                var val = $(this).val();
                var cities = @json($cities);
                var options = '';
                cities.filter(el => el.state == val).forEach(city => {
                    options += "<option value='"+ city.city + "'>"+ city.city + "</option>"
                });

                $("#city").html(options);
            });
        });
    </script>
@endsection
