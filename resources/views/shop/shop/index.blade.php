@extends('shop.layouts.app')
@section('title', __('app.commun.shop') )

@section('content')
    <!-- Shop Section Start -->
    <section class="section-b-space shop-section">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-custome-3">
                    <div class="left-box wow fadeInUp">
                        <div class="shop-left-sidebar">
                            <div class="back-button">
                                <h3><i class="fa-solid fa-arrow-left"></i> {{ __('app.shop.filter.back') }}</h3>
                            </div>

                            {{-- <div class="filter-category">
                            <div class="filter-title">
                                <h2>Filters</h2>
                                <a href="javascript:void(0)">Clear All</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Vegetable</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Fruit</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Fresh</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Milk</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Meat</a>
                                </li>
                            </ul>
                        </div> --}}

                            <div class="accordion custome-accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span>{{ __('app.shop.filter.categories') }}</span>
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne">
                                        <div class="accordion-body">
                                            <ul class="category-list custom-padding custom-height">
                                                @foreach ($categories as $category)
                                                    <li>
                                                        <div class="form-check ps-0 m-0 category-list-box">
                                                            <input
                                                                {{ request()->get('categories') && in_array($category->id, explode(',', request()->get('categories'))) ? 'checked' : '' }}
                                                                class="checkbox_animated category_checkbox" type="checkbox"
                                                                id="category_checkbox" value="{{ $category->id }}">
                                                            <label class="form-check-label" for="category_checkbox">
                                                                <span class="name">{{ $category->name }}</span>
                                                                <span
                                                                    class="number">{{ '(' . $category->prods->count() . ')' }}</span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span>{{ __('app.shop.filter.advanced_search') }}</span>
                                        </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse show"
                                        aria-labelledby="headingTwo">
                                        <div class="accordion-body">
                                            <div class="form-floating theme-form-floating-2 mb-2">
                                                <input type="search" class="form-control" id="name_search"
                                                    value="{{ request()->get('name') }}" placeholder="Search ..">
                                                <label for="name_search">{{ __('app.shop.filter.book_name') }}</label>
                                            </div>
                                            <div class="form-floating theme-form-floating-2 mb-2">
                                                <input type="search" class="form-control" id="writer_search"
                                                    value="{{ request()->get('writer') }}" placeholder="Search ..">
                                                <label for="writer_search">{{ __('app.shop.filter.writer_name') }}</label>
                                            </div>
                                            <div class="form-floating theme-form-floating-2 mb-2">
                                                <input type="search" class="form-control" id="publisher_search"
                                                    value="{{ request()->get('publisher') }}" placeholder="Search ..">
                                                <label
                                                    for="publisher_search">{{ __('app.shop.filter.publisher_name') }}</label>
                                            </div>
                                            <div class="form-floating theme-form-floating-2 mb-2">
                                                @php
                                                    $deals=Helper::get_offers();
                                                @endphp
                                                <select class="form-select form-select-sm " style="text-align: center;"  id="deal_search"
                                                 >
                                                <option value="">اختر من بين العروض</option>
                                                @foreach ($deals as $item)
                                                @if ( request()->get('deal') == $item->value)
                                                <option value="{{$item->value}}" selected>{{$item->value}}</option>                                                    
                                                @else
                                                <option value="{{$item->value}}">{{$item->value}}</option>                                                    
                                                @endif
                                                @endforeach                                               
                                                </select>
                                                {{-- <input type="search" class="form-control" id="deal_search"
                                                    value="{{ request()->get('deal') }}" placeholder="Search .."> --}}
                                                <label for="deal_search">{{ __('app.shop.filter.deal_name') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            <span>{{ __('app.shop.filter.price') }}</span>
                                        </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse show"
                                        aria-labelledby="headingThree">
                                        <div class="accordion-body">
                                            <div class="range-slider">
                                                <input type="text" class="js-range-slider" value=""
                                                    data-type="double" data-prefix=" " data-step="1" data-min="0"
                                                    data-max="{{ $max_price }}"
                                                    data-from="{{ request()->get('price') ? explode(';', request()->get('price'))[0] : 0 }}"
                                                    data-to="{{ request()->get('price') ? explode(';', request()->get('price'))[1] : $max_price }}"
                                                    data-grid="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingFour">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            <span>{{ __('app.shop.filter.discount') }}</span>
                                        </button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse show"
                                        aria-labelledby="headingFour">
                                        <div class="accordion-body">
                                            <ul class="category-list custom-padding">
                                                <li>
                                                    <div class="form-check ps-0 m-0 category-list-box">
                                                        <input
                                                            {{ request()->get('discounts') && in_array('0-5', explode(',', request()->get('discounts'))) ? 'checked' : '' }}
                                                            class="checkbox_animated discount_checkbox" type="checkbox"
                                                            id="flexCheckDefault" value="0-5">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            <span
                                                                class="name">{{ __('app.shop.filter.up_to_5') }}</span>
                                                            <span
                                                                class="number">({{ App\Models\Product::whereBetween('discount', [0, 5])->count() }})</span>
                                                        </label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="form-check ps-0 m-0 category-list-box">
                                                        <input
                                                            {{ request()->get('discounts') && in_array('5-10', explode(',', request()->get('discounts'))) ? 'checked' : '' }}
                                                            class="checkbox_animated discount_checkbox" type="checkbox"
                                                            id="flexCheckDefault1" value="5-10">
                                                        <label class="form-check-label" for="flexCheckDefault1">
                                                            <span class="name">5% - 10%</span>
                                                            <span
                                                                class="number">({{ App\Models\Product::whereBetween('discount', [5, 10])->count() }})</span>
                                                        </label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="form-check ps-0 m-0 category-list-box">
                                                        <input
                                                            {{ request()->get('discounts') && in_array('10-15', explode(',', request()->get('discounts'))) ? 'checked' : '' }}
                                                            class="checkbox_animated discount_checkbox" type="checkbox"
                                                            id="flexCheckDefault2" value="10-15">
                                                        <label class="form-check-label" for="flexCheckDefault2">
                                                            <span class="name">10% - 15%</span>
                                                            <span
                                                                class="number">({{ App\Models\Product::whereBetween('discount', [10, 15])->count() }})</span>
                                                        </label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="form-check ps-0 m-0 category-list-box">
                                                        <input
                                                            {{ request()->get('discounts') && in_array('15-25', explode(',', request()->get('discounts'))) ? 'checked' : '' }}
                                                            class="checkbox_animated discount_checkbox" type="checkbox"
                                                            id="flexCheckDefault3" value="15-25">
                                                        <label class="form-check-label" for="flexCheckDefault3">
                                                            <span class="name">15% - 25%</span>
                                                            <span
                                                                class="number">({{ App\Models\Product::whereBetween('discount', [15, 25])->count() }})</span>
                                                        </label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="form-check ps-0 m-0 category-list-box">
                                                        <input
                                                            {{ request()->get('discounts') && in_array('25-100', explode(',', request()->get('discounts'))) ? 'checked' : '' }}
                                                            class="checkbox_animated discount_checkbox" type="checkbox"
                                                            id="flexCheckDefault4" value="25-100">
                                                        <label class="form-check-label" for="flexCheckDefault4">
                                                            <span
                                                                class="name">{{ __('app.shop.filter.more_than_25') }}</span>
                                                            <span
                                                                class="number">({{ App\Models\Product::where('discount', '>=', 25)->count() }})</span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSix">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <span>Rating</span>
                                    </button>
                                </h2>
                                <div id="collapseSix" class="accordion-collapse collapse show" aria-labelledby="headingSix">
                                    <div class="accordion-body">
                                        <ul class="category-list custom-padding">
                                            <li>
                                                <div class="form-check ps-0 m-0 category-list-box">
                                                    <input class="checkbox_animated" type="checkbox">
                                                    <div class="form-check-label">
                                                        <ul class="rating">
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                        </ul>
                                                        <span class="text-content">(5 Star)</span>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="form-check ps-0 m-0 category-list-box">
                                                    <input class="checkbox_animated" type="checkbox">
                                                    <div class="form-check-label">
                                                        <ul class="rating">
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                        </ul>
                                                        <span class="text-content">(4 Star)</span>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="form-check ps-0 m-0 category-list-box">
                                                    <input class="checkbox_animated" type="checkbox">
                                                    <div class="form-check-label">
                                                        <ul class="rating">
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                        </ul>
                                                        <span class="text-content">(3 Star)</span>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="form-check ps-0 m-0 category-list-box">
                                                    <input class="checkbox_animated" type="checkbox">
                                                    <div class="form-check-label">
                                                        <ul class="rating">
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                        </ul>
                                                        <span class="text-content">(2 Star)</span>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="form-check ps-0 m-0 category-list-box">
                                                    <input class="checkbox_animated" type="checkbox">
                                                    <div class="form-check-label">
                                                        <ul class="rating">
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fill"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                            <li>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                            </li>
                                                        </ul>
                                                        <span class="text-content">(1 Star)</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> --}}

                                {{-- <button id="submit_filter" class="btn btn-animation btn-sm mend-auto">{{ __('app.shop.filter.search') }}</button> --}}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-custome-9" id="col-custome-9">
                    {{-- <div class="banner-contain-2 hover-effect">
                    <img src="../assets/images/shop/1.jpg" class="bg-img rounded-3 blur-up lazyload" alt="">
                    <div class="banner-detail p-center-right position-relative shop-banner ms-auto banner-small">
                        <div>
                            <h2>Healthy, nutritious & Tasty Fruits & Veggies</h2>
                            <h3>Save upto 50%</h3>
                        </div>
                    </div>
                </div> --}}
                    <div class="show-button">
                        <div class="filter-button-group">
                            <div class="filter-button d-inline-block d-lg-none">
                                <a><i class="fa-solid fa-filter"></i> {{ __('app.shop.filter.filter_menu') }}</a>
                            </div>
                        </div>

                        <div class="top-filter-menu">
                            <div class="category-dropdown">
                                {{-- <h5 class="text-content">{{ __('app.shop.sort_by') }} </h5> --}}
                                @php
                                    $sortby = request()->get('sort_by') . request()->get('direction');
                                @endphp
                                {{-- <div class="form-floating theme-form-floating"> --}}
                                <select class="form-select form-select-sm sort-select" id="floatingSelect">
                                    {{-- <option data-sort="" data-direction="" value="" selected="">{{__('app.shop.default_sort')}}</option> --}}
                                    <option data-sort="created_at" data-direction="desc" value="" selected>
                                        {{ __('app.shop.most_recent') }}</option>
                                    <option data-sort="created_at" data-direction="asc" value=""
                                        {{ $sortby == 'created_atasc' ? 'selected' : '' }}>
                                        {{ __('app.shop.less_recent') }}</option>
                                    <option data-sort="selling1" data-direction="desc" value=""
                                        {{ $sortby == 'selling1desc' ? 'selected' : '' }}>
                                        {{ __('app.shop.most_expensive') }}</option>
                                    <option data-sort="selling1" data-direction="asc" value=""
                                        {{ $sortby == 'selling1asc' ? 'selected' : '' }}>
                                        {{ __('app.shop.less_expensive') }}</option>
                                </select>
                                {{-- <label for="floatingSelect">City</label>
                                </div> --}}
                                {{-- <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown">
                                        <span>{{__('app.shop.default_sort')}}</span> <i class="fa-solid fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <a class="dropdown-item sort-option" data-sort="selling1" data-direction="desc"
                                                id="most_expensive"
                                                href="javascript:void(0)">{{ __('app.shop.most_expensive') }}</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item sort-option" data-sort="selling1" data-direction="asc"
                                                id="less_expensive"
                                                href="javascript:void(0)">{{ __('app.shop.less_expensive') }}</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item sort-option" data-sort="created_at" data-direction="desc"
                                                id="most_recent"
                                                href="javascript:void(0)">{{ __('app.shop.most_recent') }}</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item sort-option" data-sort="created_at" data-direction="asc"
                                                id="less_recent"
                                                href="javascript:void(0)">{{ __('app.shop.less_recent') }}</a>
                                        </li>
                                    </ul>
                                </div> --}}
                            </div>

                            <div class="grid-option d-none d-md-block">
                                <ul>
                                    <li class="three-grid">
                                        <a href="javascript:void(0)">
                                            <img src="{{ asset('assets/shop/svg/grid-3.svg') }}" class="blur-up lazyload"
                                                alt="">
                                        </a>
                                    </li>
                                    <li class="grid-btn d-xxl-inline-block d-none active">
                                        <a href="javascript:void(0)">
                                            <img src="{{ asset('assets/shop/svg/grid-4.svg') }}"
                                                class="blur-up lazyload d-lg-inline-block d-none" alt="">
                                            <img src="{{ asset('assets/shop/svg/grid.svg') }}"
                                                class="blur-up lazyload img-fluid d-lg-none d-inline-block"
                                                alt="">
                                        </a>
                                    </li>
                                    <li class="list-btn">
                                        <a href="javascript:void(0)">
                                            <img src="{{ asset('assets/shop/svg/list.svg') }}" class="blur-up lazyload"
                                                alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if (count($data) > 0)    
                    <div
                        class="row g-sm-4 g-3 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-2 row-cols-md-3 row-cols-1 product-list-section">
                                            
                        @foreach ($data as $item)
                            <div> 
                                @include('shop.layouts.card', ['product' => $item])

                            </div>
                        @endforeach
                       


                    </div>
                    @else
                    <div style="text-align: center;">

                        <img src="{{ asset('assets/shop/images/searsh.svg') }}" class="blur-up lazyload" style="width: 100%;  padding-bottom: 20px; max-width: 500px;"   alt="">  

                        <h5 style="font-weight: 800;">{{ __('app.shop.search_msg') }}</h5>

                    </div>
                    @endif
                    {{ $data->links('vendor.pagination.custom') }}

                    {{-- <nav class="custome-pagination">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-disabled="true">
                                <i class="fa-solid fa-angles-left"></i>
                            </a>
                        </li>
                        <li class="page-item active">
                            <a class="page-link" href="javascript:void(0)">1</a>
                        </li>
                        <li class="page-item" aria-current="page">
                            <a class="page-link" href="javascript:void(0)">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)">3</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)">
                                <i class="fa-solid fa-angles-right"></i>
                            </a>
                        </li>
                    </ul>
                </nav> --}}
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Section End -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/shop/js/jquery-query-object/jquery.query-object.js') }}"></script>

    <script>
        $(document).ready(function() {
            // var newUrl = @json(request()->query());
            // Get the current URL and query parameters
            let url = new URL(window.location.href);
            let params = new URLSearchParams(url.search);
            @php
                $discounts = request()->has('discounts') && request()->get('discounts') != '' ? explode(',', request()->get('discounts')) : [];
                $categories = request()->has('categories') && request()->get('categories') != '' ? explode(',', request()->get('categories')) : [];
            @endphp

            var discounts_filter = @json($discounts);
            var categories_filter = @json($categories);

            function advancedFilter(key, field){
                // Update the value of a query parameter
                params.set('page', '1');
                params.set(key, $(field).val());

                filter();
            }

            $(document).on('keyup , blur, change', '#name_search', function(event) {
                const processChange = debounce(() => advancedFilter("name", "#name_search"));
                processChange();
            });

            $(document).on('keyup , blur, change', '#writer_search', function(event) {
                const processChange = debounce(() => advancedFilter("writer", "#writer_search"));
                processChange();
            });

            $(document).on('keyup , blur, change', '#publisher_search', function(event) {
                const processChange = debounce(() => advancedFilter("publisher", "#publisher_search"));
                processChange();
            });

            $(document).on('keyup , blur, change', '#deal_search', function(event) {
                const processChange = debounce(() => advancedFilter("deal", "#deal_search"));
                processChange();
            });

            function filter() {
                // Create a new URL with the updated query parameters
                let newUrl = window.location.origin + window.location.pathname + '?' + params.toString();

                // Use history.pushState() to update the URL without refreshing the page
                history.pushState({}, '', newUrl);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // url: '/shop' + newUrl,
                    url: newUrl,
                    method: "GET",
                    dataType: "json",
                    success: function(data) {
                        if (data.status === 'success') {
                            $('.col-custome-9').html(data.html);
                        }
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            }

            $(document).on('click', '#submit_filter', function(event) {
                filter();
            });

            $('.category_checkbox').click(function() {
                if ($(this).is(':checked')) {
                    categories_filter.push($(this).val());
                } else {
                    var index = categories_filter.indexOf($(this).val());
                    if (index !== -1) {
                        categories_filter.splice(index, 1);
                    }
                }
                params.set('page', '1');
                params.set("categories", categories_filter.join(','));
                filter();
            });

            $('.discount_checkbox').click(function() {
                if ($(this).is(':checked')) {
                    discounts_filter.push($(this).val());
                } else {
                    var index = discounts_filter.indexOf($(this).val());
                    if (index !== -1) {
                        discounts_filter.splice(index, 1);
                    }
                }
                params.set('page', '1');
                params.set("discounts", discounts_filter.join(','));
                filter();
            });

            function debounce(func, timeout = 1000){
                let timer;
                return (...args) => {
                    clearTimeout(timer);
                    timer = setTimeout(() => { func.apply(this, args); }, timeout);
                };
            }

            const priceChange = debounce(() => priceFilter());

            function priceFilter(){
                params.set('page', '1');
                params.set("price", $('.js-range-slider').val());
                filter();
            }

            $(document).on('change', '.js-range-slider', function(event) {
                priceChange();
            });

            $(document).on('change', '.sort-select', function(event) {
                var sort_by = $('option:selected', this).data('sort');
                var direction = $('option:selected', this).data('direction');

                params.set('page', '1');
                params.set("sort_by", sort_by);
                params.set("direction", direction);

                let newUrl = window.location.origin + window.location.pathname + '?' + params.toString();
                window.location = newUrl;
            });
        });
    </script>
@endsection
