{{-- <div class="col-custome-9"> --}}
    {{-- <div class="banner-contain-2 hover-effect">
    <img src="../assets/images/shop/1.jpg" class="bg-img rounded-3 blur-up lazyload" alt="">
    <div class="banner-detail p-center-right position-relative shop-banner ms-auto banner-small">
        <div>
            <h2>Healthy, nutritious & Tasty Fruits & Veggies</h2>
            <h3>Save upto 50%</h3>
        </div>
    </div>
</div> --}}
    <div class="show-button">
        <div class="filter-button-group">
            <div class="filter-button d-inline-block d-lg-none">
                <a><i class="fa-solid fa-filter"></i> {{ __('app.shop.filter.filter_menu') }}</a>
            </div>
        </div>

        <div class="top-filter-menu">
            <div class="category-dropdown">
                {{-- <h5 class="text-content">{{ __('app.shop.sort_by') }} </h5> --}}
                @php
                    $sortby = request()->get('sort_by') . request()->get('direction');
                @endphp
                {{-- <div class="form-floating theme-form-floating"> --}}
                <select class="form-select form-select-sm sort-select" id="floatingSelect">
                    {{-- <option data-sort="" data-direction="" value="" selected="">{{__('app.shop.default_sort')}}</option> --}}
                    <option data-sort="created_at" data-direction="desc" value="" selected>
                        {{ __('app.shop.most_recent') }}</option>
                    <option data-sort="created_at" data-direction="asc" value=""
                        {{ $sortby == 'created_atasc' ? 'selected' : '' }}>
                        {{ __('app.shop.less_recent') }}</option>
                    <option data-sort="selling1" data-direction="desc" value=""
                        {{ $sortby == 'selling1desc' ? 'selected' : '' }}>
                        {{ __('app.shop.most_expensive') }}</option>
                    <option data-sort="selling1" data-direction="asc" value=""
                        {{ $sortby == 'selling1asc' ? 'selected' : '' }}>
                        {{ __('app.shop.less_expensive') }}</option>
                </select>
                {{-- <label for="floatingSelect">City</label>
                </div> --}}
                {{-- <div class="dropdown">
                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown">
                        <span>{{__('app.shop.default_sort')}}</span> <i class="fa-solid fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li>
                            <a class="dropdown-item sort-option" data-sort="selling1" data-direction="desc"
                                id="most_expensive"
                                href="javascript:void(0)">{{ __('app.shop.most_expensive') }}</a>
                        </li>
                        <li>
                            <a class="dropdown-item sort-option" data-sort="selling1" data-direction="asc"
                                id="less_expensive"
                                href="javascript:void(0)">{{ __('app.shop.less_expensive') }}</a>
                        </li>
                        <li>
                            <a class="dropdown-item sort-option" data-sort="created_at" data-direction="desc"
                                id="most_recent"
                                href="javascript:void(0)">{{ __('app.shop.most_recent') }}</a>
                        </li>
                        <li>
                            <a class="dropdown-item sort-option" data-sort="created_at" data-direction="asc"
                                id="less_recent"
                                href="javascript:void(0)">{{ __('app.shop.less_recent') }}</a>
                        </li>
                    </ul>
                </div> --}}
            </div>

            <div class="grid-option d-none d-md-block">
                <ul>
                    <li class="three-grid">
                        <a href="javascript:void(0)">
                            <img src="{{ asset('assets/shop/svg/grid-3.svg') }}" class="blur-up lazyload"
                                alt="">
                        </a>
                    </li>
                    <li class="grid-btn d-xxl-inline-block d-none active">
                        <a href="javascript:void(0)">
                            <img src="{{ asset('assets/shop/svg/grid-4.svg') }}"
                                class="blur-up lazyload d-lg-inline-block d-none" alt="">
                            <img src="{{ asset('assets/shop/svg/grid.svg') }}"
                                class="blur-up lazyload img-fluid d-lg-none d-inline-block"
                                alt="">
                        </a>
                    </li>
                    <li class="list-btn">
                        <a href="javascript:void(0)">
                            <img src="{{ asset('assets/shop/svg/list.svg') }}" class="blur-up lazyload"
                                alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @if (count($data) > 0)    
    <div
        class="row g-sm-4 g-3 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-2 row-cols-md-3 row-cols-1 product-list-section">
                            
        @foreach ($data as $item)
            <div> 
                @include('shop.layouts.card', ['product' => $item])

            </div>
        @endforeach
       


    </div>
    @else
    <div style="text-align: center;">

        <img src="{{ asset('assets/shop/images/searsh.svg') }}" class="blur-up lazyload" style="width: 100%;  padding-bottom: 20px; max-width: 500px;"   alt="">  

        <h5 style="font-weight: 800;">{{ __('app.shop.search_msg') }}</h5>

    </div>
    @endif
    {{ $data->links('vendor.pagination.custom') }}

    {{-- <nav class="custome-pagination">
    <ul class="pagination justify-content-center">
        <li class="page-item disabled">
            <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-disabled="true">
                <i class="fa-solid fa-angles-left"></i>
            </a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="javascript:void(0)">1</a>
        </li>
        <li class="page-item" aria-current="page">
            <a class="page-link" href="javascript:void(0)">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">
                <i class="fa-solid fa-angles-right"></i>
            </a>
        </li>
    </ul>
</nav> --}}
{{-- </div> --}}