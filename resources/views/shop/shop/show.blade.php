@extends('shop.layouts.app')
@section('title', __('app.commun.shop').' | '.$product->name )
@section('meta_description',$product->metadescription??config('stock.info.ar_meta_default'))
@section('meta_author',$product->metatitle??config('stock.info.ar_meta_default'))
@section('meta_keywords',$product->metakeys??config('stock.info.ar_meta_default'))
@section('content')
    <!-- Product Left Sidebar Start -->
    <section class="product-section">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-xxl-9 col-xl-8 col-lg-7 wow fadeInUp">
                    <div class="row g-4 product-images">
                        <div class="col-xl-6 wow fadeInUp">
                            <div class="product-left-box">
                                <div class="row g-2">
                                    <div class="col-xxl-10 col-lg-12 col-md-10 order-xxl-2 order-lg-1 order-md-2">
                                        <div class="product-main-2 no-arrow">
                                            {{-- @json($product->images) --}}
                                            @if ($product->images()->exists())
                                                @foreach ($product->images as $image)
                                                    <div>
                                                        <div class="slider-image">
                                                            <img src="{{ asset($image->url) }}" id="img-{{ $image->id }}"
                                                                data-zoom-image="{{ asset($image->url) }}"
                                                                class="img-fluid image_zoom_cls-0 blur-up lazyload"
                                                                alt="{{ $product->name }}">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <div class="slider-image">
                                                        <img src="{{ asset($product->default_image_url) }}" id="img-1"
                                                            data-zoom-image="{{ asset($product->default_image_url) }}"
                                                            class="img-fluid image_zoom_cls-0 blur-up lazyload"
                                                            alt="{{ $product->name }}">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xxl-2 col-lg-12 col-md-2 order-xxl-1 order-lg-2 order-md-1">
                                        <div class="left-slider-image-2 left-slider no-arrow slick-top">
                                            @if ($product->images()->exists())
                                                @foreach ($product->images as $image)
                                                    <div>
                                                        <div class="sidebar-image">
                                                            <img src="{{ asset($image->url) }}"
                                                                class="img-fluid blur-up lazyload" alt="{{ $product->name }}">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <div class="sidebar-image">
                                                        <img src="{{ asset($product->default_image_url) }}"
                                                            class="img-fluid blur-up lazyload" alt="{{ $product->name }}">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="right-box-contain">
                                {{-- <h6 class="offer-top">30% Off</h6> --}}
                                <h2 class="name">{{ $product->name }}</h2>
                                <div class="price-rating">
                                    @if($product->selling2 > 0 && $product->selling2!=0)
                                     <h3 class="theme-color price">{{ $product->selling2 }} {{ __('app.shop.item.currency.dm') }}
                                        <del class="text-content">{{ $product->selling1 }} {{ __('app.shop.item.currency.dm') }}</del>
                                    </h3>
                                    @else
                                        <h3 class="theme-color price">{{ $product->selling }} {{ __('app.shop.item.currency.dm') }}
                                        </h3>
                                    @endif

                              
                                 
                                    <div class="product-rating custom-rate">
                                        <ul class="rating">
                                            <li>
                                                <i data-feather="star" class="fill"></i>
                                            </li>
                                            <li>
                                                <i data-feather="star" class="fill"></i>
                                            </li>
                                            <li>
                                                <i data-feather="star" class="fill"></i>
                                            </li>
                                            <li>
                                                <i data-feather="star" class="fill"></i>
                                            </li>
                                            <li>
                                                <i data-feather="star" class="fill"></i>
                                            </li>
                                        </ul>
                                        <span class="review">(23)</span>
                                    </div>
                                </div>
                                @if ($product->writer &&  $product->writer != '-')
                                    <h6 class="unit"><a href="{{route('shop.index', ['writer' => $product->writer])}}">{{ $product->writer }}</a></h6>
                                @endif
                                @if ($product->house &&  $product->house != '-')
                                    <h6 class="unit"><a href="{{route('shop.index', ['publisher' => $product->house])}}">{{ $product->house }}</a></h6>
                                @endif

                                <div class="procuct-contain">
                                    <p>{{ $product->resume }}
                                    </p>
                                </div>                         

                                <div class="note-box product-packege">
                                    <div class="cart_qty qty-box product-qty">
                                        <div class="input-group">
                                            <button type="button" class="qty-right-plus" data-type="plus" data-field="" data-product-id="{{ $product['id'] }}">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                            <input class="form-control input-number qty-input" type="text" id="qty-input-{{ $product->id }}"
                                                name="quantity" value="{{ Helper::getCartItemQty($product) > 0 ? Helper::getCartItemQty($product) : 1 }}">
                                            <button type="button" class="qty-left-minus" data-type="minus" data-field="" data-product-id="{{ $product['id'] }}">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <button 
                                        class="btn btn-md bg-dark cart-button text-white w-100 btn-cart addToCart"  data-product-id="{{ $product->id }}">{{ __('app.shop.item.add_to_cart') }}</button>
                                </div>

                                <div class="buy-box">
                                    <a class="add_wishlist" id="wish{{$product->id}}" data="{{$product->id}}" style="cursor: pointer; " >
                                        <i class="iconly-Heart icli"  style="font-size: 20px;  @if($product->check_wishlist)  color:red  @endif "  ></i>
                                        <span>{{ __('app.shop.item.wishlist') }}</span>
                                    </a>
                                </div>

                                <div class="pickup-box">
                                    <div class="product-info">
                                        <ul class="product-info-list product-info-list-2 list-group">
                                            <span @if(app()->getLocale() == 'ar') style="text-align: right;" @endif>
                                                {{ __('app.shop.single.categories') }} : 
                                                @foreach ($product->categories_names as $key => $cat)
                                                <span class="span-name"><a href="{{ route('shop.index', ['categories' => $key]) }}">
                                                    @if ($loop->first)
                                                    {{ $cat }}
                                                @else
                                                    , {{ $cat }}
                                                @endif
                                                </a></span>
                                            @endforeach

                                            </span>
                                            <span  @if(app()->getLocale() == 'ar') style="text-align: right;" @endif>
                                                {{ __('app.shop.single.sku') }} : {{ $product->sku }}</span>
                                            <span  @if(app()->getLocale() == 'ar') style="text-align: right;" @endif>
                                                {{ __('app.shop.single.stock') }} :
                                                @if ($product->quantities()->whereHas('store', function ($q) {
                                                    $q->where('website_sales', 1);
                                                })->sum('quantity') < 1)
                                                    @if ($product->by_order)
                                                        <span class="badge bg-info">{{ __('app.shop.single.by_order') }}</span>
                                                    @else
                                                        <span class="badge bg-danger">{{ __('app.shop.single.out_stock') }}</span>
                                                    @endif
                                                @else
                                                    <span class="badge bg-success">{{ __('app.shop.single.in_stock') }}</span>
                                                @endif
                                            </span>
                                            @if ($product->metakeys)
                                                <span  @if(app()->getLocale() == 'ar') style="text-align: right;" @endif>
                                                    {{ __('app.shop.single.tags') }} : <a
                                                        href="javascript:void(0)">{{ $product->metakeys }}</a></span>
                                            @endif
                                        {{-- </ul> --}}
                                    </div>
                                </div>
                                <div class="paymnet-option">
                                    <div class="product-title">
                                        <h4>{{__('app.shop.single.payment_methods')}}</h4>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <img src="{{ asset('assets/shop/images/product/payment/1.svg') }}"
                                                    class="blur-up lazyloaded" alt="">
                                            </a>
                                        </li>
                                        {{-- <li>
                                            <a href="javascript:void(0)">
                                                <img src="../assets/images/product/payment/2.svg" class="blur-up lazyloaded" alt="">
                                            </a>
                                        </li> --}}
                                        <li>
                                            <a href="javascript:void(0)">
                                                <img src="{{ asset('assets/shop/images/product/payment/3.svg') }}"
                                                    class="blur-up lazyloaded" alt="">
                                            </a>
                                        </li>
                                        {{-- <li>
                                            <a href="javascript:void(0)">
                                                <img src="../assets/images/product/payment/4.svg" class="blur-up lazyloaded" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <img src="../assets/images/product/payment/5.svg" class="blur-up lazyloaded" alt="">
                                            </a>
                                        </li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="product-section-box">
                                <ul class="nav nav-tabs custom-nav" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="description-tab" data-bs-toggle="tab"
                                            data-bs-target="#description" type="button" role="tab"
                                            aria-controls="description" aria-selected="true">{{ __('app.shop.single.description') }}</button>
                                    </li>

                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="info-tab" data-bs-toggle="tab"
                                            data-bs-target="#info" type="button" role="tab" aria-controls="info"
                                            aria-selected="false">{{ __('app.shop.single.additional_info') }}</button>
                                    </li>

                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="review-tab" data-bs-toggle="tab"
                                            data-bs-target="#review" type="button" role="tab"
                                            aria-controls="review" aria-selected="false">{{ __('app.shop.single.review') }}</button>
                                    </li>
                                </ul>

                                <div class="tab-content custom-tab" id="myTabContent">
                                    <div class="tab-pane fade show active" id="description" role="tabpanel"
                                        aria-labelledby="description-tab">
                                        <div class="product-description">
                                            <div class="nav-desh">
                                                <p>{!! $product->description !!}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="info" role="tabpanel"
                                        aria-labelledby="info-tab">
                                        <div class="table-responsive">
                                            <table class="table info-table">
                                                <tbody>
                                                    @foreach ($product->data as $key => $item)
                                                        <tr>
                                                            <td>{{ __('app.shop.single.' . $key) }}</td>
                                                            <td>{{ $item }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="review" role="tabpanel"
                                        aria-labelledby="review-tab">
                                        <div class="review-box">
                                            <div class="row g-4">
                                                <div class="col-xl-6">
                                                    <div class="review-title">
                                                        <h4 class="fw-500">{{__('app.shop.single.clients_reviews')}}</h4>
                                                    </div>

                                                    <div class="d-flex">
                                                        <div class="product-rating">
                                                            <ul class="rating">
                                                                <li>
                                                                    <i data-feather="star" class="fill"></i>
                                                                </li>
                                                                <li>
                                                                    <i data-feather="star" class="fill"></i>
                                                                </li>
                                                                <li>
                                                                    <i data-feather="star" class="fill"></i>
                                                                </li>
                                                                <li>
                                                                    <i data-feather="star"></i>
                                                                </li>
                                                                <li>
                                                                    <i data-feather="star"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <h6 class="ms-3">4.2 {{__('app.shop.single.outof')}} 5</h6>
                                                    </div>

                                                    <div class="rating-box">
                                                        <ul>
                                                            <li>
                                                                <div class="rating-list">
                                                                    <h5>5 </h5>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar"
                                                                            style="width: 68%" aria-valuenow="100"
                                                                            aria-valuemin="0" aria-valuemax="100">
                                                                            68%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="rating-list">
                                                                    <h5>4 </h5>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar"
                                                                            style="width: 67%" aria-valuenow="100"
                                                                            aria-valuemin="0" aria-valuemax="100">
                                                                            67%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="rating-list">
                                                                    <h5>3 </h5>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar"
                                                                            style="width: 42%" aria-valuenow="100"
                                                                            aria-valuemin="0" aria-valuemax="100">
                                                                            42%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="rating-list">
                                                                    <h5>2 </h5>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar"
                                                                            style="width: 30%" aria-valuenow="100"
                                                                            aria-valuemin="0" aria-valuemax="100">
                                                                            30%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="rating-list">
                                                                    <h5>1 </h5>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar"
                                                                            style="width: 24%" aria-valuenow="100"
                                                                            aria-valuemin="0" aria-valuemax="100">
                                                                            24%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="review-title">
                                                        <h4 class="fw-500">{{__('app.shop.single.add_review')}}</h4>
                                                    </div>

                                                    <div class="row g-4">
                                                        <div class="col-md-6">
                                                            <div class="form-floating theme-form-floating">
                                                                <input type="text" class="form-control" id="name"
                                                                    placeholder="Name">
                                                                <label for="name">{{__('app.shop.single.your_name')}}</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-floating theme-form-floating">
                                                                <input type="email" class="form-control" id="email"
                                                                    placeholder="Email Address">
                                                                <label for="email">{{__('app.shop.single.email_address')}}</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-floating theme-form-floating">
                                                                <input type="url" class="form-control" id="website"
                                                                    placeholder="Website">
                                                                <label for="website">{{__('app.shop.single.website')}}</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-floating theme-form-floating">
                                                                <input type="url" class="form-control" id="review1"
                                                                    placeholder="Give your review a title">
                                                                <label for="review1">{{__('app.shop.single.review_title')}}</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating theme-form-floating">
                                                                <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 150px"></textarea>
                                                                <label for="floatingTextarea2">{{__('app.shop.single.write_comment')}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    {{-- <div class="review-title">
                                                        <h4 class="fw-500">{{__('app.shop.single.clients_questions_answers')}}</h4>
                                                    </div> --}}

                                                    <div class="review-people">
                                                        <ul class="review-list">
                                                            <li>
                                                                <div class="people-box">
                                                                    <div>
                                                                        <div class="people-image">
                                                                            <img src="{{asset('assets/shop/images/review/1.jpg')}}"
                                                                                class="img-fluid blur-up lazyload"
                                                                                alt="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="people-comment">
                                                                        <a class="name"
                                                                            href="javascript:void(0)">Tracey</a>
                                                                        <div class="date-time">
                                                                            <h6 class="text-content">14 Jan, 2022 at
                                                                                12.58 AM</h6>

                                                                            <div class="product-rating">
                                                                                <ul class="rating">
                                                                                    <li>
                                                                                        <i data-feather="star"
                                                                                            class="fill"></i>
                                                                                    </li>
                                                                                    <li>
                                                                                        <i data-feather="star"
                                                                                            class="fill"></i>
                                                                                    </li>
                                                                                    <li>
                                                                                        <i data-feather="star"
                                                                                            class="fill"></i>
                                                                                    </li>
                                                                                    <li>
                                                                                        <i data-feather="star"></i>
                                                                                    </li>
                                                                                    <li>
                                                                                        <i data-feather="star"></i>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>

                                                                        <div class="reply">
                                                                            <p>Icing cookie carrot cake chocolate cake
                                                                                sugar plum jelly-o danish. Dragée dragée
                                                                                shortbread tootsie roll croissant muffin
                                                                                cake I love gummi bears. Candy canes ice
                                                                                cream caramels tiramisu marshmallow cake
                                                                                shortbread candy canes cookie.
                                                                                {{-- <a href="javascript:void(0)">Reply</a> --}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-xxl-3 col-xl-4 col-lg-5 d-none d-lg-block wow fadeInUp"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <div class="right-sidebar-box">
                        <!-- Trending Product -->
                        <div class="pt-25">
                            <div class="category-menu">
                                <h3>{{ __('app.shop.single.trending_products') }}</h3>

                                <ul class="product-list product-right-sidebar border-0 p-0">
                                    @foreach ($trending_products as $item)
                                        <li>
                                            <div class="offer-product">
                                                <a href="{{route('shop.show', ['product' => $item->id])}}" class="offer-image">
                                                    <img src="{{ asset($item->default_image_url) }}"
                                                        class="img-fluid blur-up lazyloaded" alt="{{ $item->name }}">
                                                </a>

                                                <div class="offer-detail">
                                                    <div>
                                                        <a href="{{route('shop.show', ['product' => $item->id])}}">
                                                            <h6 title="{{ $item->name }}" class="name">
                                                                {{ $item->name }}</h6>
                                                        </a>
                                                        <span>{{ $item->writer }}</span>
                                                        @if($item->selling1 > $item->selling2 && $item->selling2!=0)
                                                            <h6 class="theme-color price">{{ $item->selling2 }} {{ __('app.shop.item.currency.dm') }}
                                                                <del class="text-content">{{ $item->selling1 }} {{ __('app.shop.item.currency.dm') }}</del>
                                                            </h6>
                                                        @else
                                                            <h6 class="theme-color price">{{ $item->selling1 }} {{ __('app.shop.item.currency.dm') }}
                                                            </h6>
                                                        @endif
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <!-- Banner Section -->
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Product Left Sidebar End -->

    <!-- Releted Product Section Start -->
    <section class="product-list-section section-b-space">
        <div class="container-fluid-lg">
            <div class="title">
                <h2>{{ __('app.shop.single.related_products') }}</h2>
                <span class="title-leaf">
                    {{-- <svg class="icon-width">
                        <use xlink:href="{{ asset('assets/shop/svg/read-book-icon.svg') }}"></use>
                    </svg> --}}
                    <img  class="icon-width"  src="{{ asset('assets/shop/svg/read-book-icon.svg') }}" alt="">
                </span>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slider-4-custom product-wrapper">
                        @foreach ($related_products as $item)
                            <div>
                                @include('shop.layouts.card', ['product' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Releted Product Section End -->
@endsection

@section('scripts')
    
@endsection
