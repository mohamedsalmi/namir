@extends('shop.layouts.app')
@section('content')
    <!-- Breadcrumb Section Start -->
    @php
     if (!$order) {
        redirect()->route('shop.index');
     }   
    @endphp
    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain breadscrumb-order">
                        <div class="order-box">
                            <div class="order-contain">
                                {{-- <h3 class="theme-color">{{ __('order.order') }} {{ __('order.success') }}</h3>
                                <h5 class="text-content">{{ __('order.success_paiement') }}</h5> --}}
                                <h3>{{ __('order.order_id') }}: {{$order->codification}}</h3>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Cart Section Start -->
    <section class="cart-section section-b-space">
        <div class="container-fluid-lg">
            <div class="row g-sm-4 g-3">
                <div class="col-xxl-9 col-lg-8">
                    <div class="cart-table order-table order-table-2">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    @foreach ($order->items  as $key => $item )                                        
                                    <tr>
                                        <td class="product-detail">
                                            <div class="product border-0">
                                                <a href="product.left-sidebar.html" class="product-image">
                                                    <img src="{{asset($item->product->default_image_url ?? config('stock.image.default.product'))}}"
                                                        class="img-fluid blur-up lazyload" alt="">
                                                </a>
                                                <div class="product-detail">
                                                    <ul>
                                                        <li class="name">
                                                            <a href="product-left-thumbnail.html">{{$item->product_name}}</a>
                                                        </li>

                                                        <li class="text-content">{{ __('order.writer') }}: {{$item->product->writer}}</li>

                                                        <li class="text-content">{{ __('order.house') }} :{{$item->product->house}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>

                                        <td class="price">
                                            <h4 class="table-title text-content">{{ __('app.cart.price') }}</h4>
                                            <h6 class="theme-color">{{$item->product_price_selling}}DM</h6>
                                        </td>

                                        <td class="quantity">
                                            <h4 class="table-title text-content">{{ __('app.cart.qty') }}</h4>
                                            <h4 class="text-title">{{$item->product_quantity}}</h4>
                                        </td>

                                        <td class="subtotal">
                                            <h4 class="table-title text-content">{{ __('app.cart.total') }}</h4>
                                            <h5>{{$item->product_price_selling * $item->product_quantity}}DM</h5>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-xxl-3 col-lg-4">
                    <div class="row g-4">
                        <div class="col-lg-12 col-sm-6">
                            <div class="summery-box">
                                <div class="summery-header">
                                    <h3>{{ __('order.price_details') }}</h3>
                                    <h5 class="ms-auto theme-color">({{$order->number}} {{ __('order.articles') }})</h5>
                                </div>

                                <ul class="summery-contain">
                                    <li>
                                        <h4>{{ __('app.cart.total_price') }}</h4>
                                        <h4 class="price">{{$order->total}} DM</h4>
                                    </li>

                                 

                                    <li>
                                        <h4>{{ __('app.cart.coupon_discount') }}</h4>
                                        <h4 class="price text-danger">{{number_format($order->total*$order->items->first()->product_remise/100,3)}} DM</h4>
                                    </li>
                                </ul>

                                <ul class="summery-total">
                                    <li class="list-total">
                                        <h4>{{ __('app.cart.total') }} </h4>
                                        <h4 class="price">{{number_format($order->total*(1-$order->items->first()->product_remise/100),3)}} DM</h4>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-12 col-sm-6">
                            <div class="summery-box">
                                <div class="summery-header d-block">
                                    <h3>{{ __('order.shipping_address') }}</h3>
                                </div>

                                <ul class="summery-contain pb-0 border-bottom-0">
                                    <li class="d-block">
                                        <h4 class="mt-2">{{$order->client_details['adresse']}}</h4>
                                        <p>{{$order->client_details['city'] ?? ''}}</p>
                                        <p>{{$order->client_details['state'] ?? ''}}</p>
                                    </li>

                                    <li class="pb-0">
                                        <h4>{{ __('order.expected_date') }}:</h4>
                                        {{-- <h4 class="price theme-color">
                                            <a href="{{ route('shop.order.tracking', $order->id) }}" class="text-danger">{{ __('order.track_order') }}</a>
                                        </h4> --}}
                                    </li>
                                </ul>

                                <ul class="summery-total">
                                    <li class="list-total border-top-0 pt-2">
                                        <h4 class="fw-bold">{{\Carbon\Carbon::parse($order->date)->addDays(2)->format('Y-m-d')}}</h4>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="summery-box">
                                <div class="summery-header d-block">
                                    <h3>طريقة الدفع</h3>
                                </div>

                                <ul class="summery-contain pb-0 border-bottom-0">
                                    <li class="d-block pt-0">
                                        <p class="text-content">الدفع عند الاستلام.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cart Section End -->
@endsection