<!-- Footer Start -->
<footer class="section-t-space footer-section-2">
    <div class="container-fluid-lg">
        <div class="service-section">
            <div class="row g-3">
                <div class="col-12">
                    <div class="service-contain d-flex justify-content-around" >
                        <div class="service-box">
                            <img src="{{ asset('assets/shop/images/footer/icon1.png') }} " style="width: 180px;" class="blur-up lazyload"
                                alt="">

                        </div>

                        <div class="service-box">
                            <img src="{{ asset('assets/shop/images/footer/icon2.png') }}" style="width: 180px;" class="blur-up lazyload"
                                alt="">
                        </div>

                        <div class="service-box">
                            <img src="{{ asset('assets/shop/images/footer/icon3.png') }}" style="width: 180px;" class="blur-up lazyload"
                                alt="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-footer">
            <div class="row g-md-4 gy-sm-5 gy-2">
                <div class="col-xxl-3 col-xl-4 col-sm-6">
                    <a href="{{route('shop.home')}}" class="foot-logo">
                        <img src="{{ asset(config('stock.info.image')) }}" class="img-fluid" alt="">
                    </a>
                    <p class="information-text">
                        @switch(str_replace('_', '-', app()->getLocale()))
                            @case('en')
                                {{ config('stock.info.desc_en') }}
                            @break

                            @case('fr')
                                {{ config('stock.info.desc_fr') }}
                            @break

                            @default
                                {{ config('stock.info.desc_ar') }}
                        @endswitch
                        .</p>
                        <ul class="social-icon">
                            @php
                                $facebook = Helper::getSettingValue('facebook');
                                $twitter = Helper::getSettingValue('twitter');
                                $instagram = Helper::getSettingValue('instagram');
                                $linkedin = Helper::getSettingValue('linkedin');
                                $youtube = Helper::getSettingValue('youtube');
                            @endphp
                            @if ($facebook)
                                <li>
                                    <a href="{{ $facebook }}">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                            @endif
                            @if ($twitter)
                                <li>
                                    <a href="{{ $twitter }}">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                            @endif
                            @if ($instagram)
                                <li>
                                    <a href="{{ $instagram }}">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            @endif
                            @if ($linkedin)
                                <li>
                                    <a href="{{ $linkedin }}">
                                        <i class="fab fa-linkedin"></i>
                                    </a>
                                </li>
                            @endif
                            @if ($youtube)
                                <li>
                                    <a href="{{ $youtube }}">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>

                    <div class="social-app mt-sm-4 mt-3 mb-4">
                        <ul>
                            <li>
                                <a href="https://play.google.com/store/apps" target="_blank">
                                    <img src="{{ asset('assets/shop/images/playstore.svg') }}" class="blur-up lazyload"
                                        alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.apple.com/in/app-store/" target="_blank">
                                    <img src="{{ asset('assets/shop/images/appstore.svg') }}" class="blur-up lazyload"
                                        alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-xxl-2 col-xl-4 col-sm-6">
                    <div class="footer-title">
                        <h4>{{ __('app.commun.about') }} {{ (str_replace('_', '-', app()->getLocale())) == 'ar' ?config('stock.info.ar_name'): config('stock.info.name') }}</h4>
                    </div>
                    <ul class="footer-list footer-contact mb-sm-0 mb-3">
                        <li>
                            <a href="{{ route('shop.about_us') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.about_us') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('shop.contact_us') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.contact_us') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('shop.terms') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.terms') }}</a>
                        </li>
                    </ul>
                </div>

                <div class="col-xxl-2 col-xl-4 col-sm-6">
                    <div class="footer-title">
                        <h4>{{ __('app.commun.useful_links') }}</h4>
                    </div>
                    <ul class="footer-list footer-contact mb-sm-0 mb-3">
                        <li>
                            <a href="{{ route('shop.order') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.your_order') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('shop.profile') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.your_account') }}</a>
                        </li>
                        {{-- <li>
                            <a href="{{ __('app.profile.order') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.track_orders') }}</a>
                        </li> --}}
                        <li>
                            <a href="{{ route('shop.wishlist') }}" class="footer-contain-2">
                                <i class="fas fa-angle-right"></i>{{ __('app.commun.your_wishlist') }}</a>
                        </li>
                    </ul>
                </div>

                <div class="col-xxl-2 col-xl-4 col-sm-6">
                    <div class="footer-title">
                        <h4>{{ __('app.commun.offers') }}</h4>
                    </div>
                    <ul class="footer-list footer-contact mb-sm-0 mb-3">
                        @php
                        $offers=Helper::get_offers();
                         @endphp
                       @foreach ($offers as $offer)                           
                       <li>
                           <a href="{{ route('shop.index', ['deal' => $offer->value]) }}" class="footer-contain-2">
                               <i class="fas fa-angle-right"></i>{{$offer->value}}</a>
                       </li>                       
                       @endforeach
                    </ul>
                </div>

                <div class="col-xxl-3 col-xl-4 col-sm-6">
                    <div class="footer-title">
                        <h4>{{ __('app.commun.store_info') }}</h4>
                    </div>
                    <ul class="footer-address footer-contact">
                        <li>
                            <a href="javascript:void(0)">
                                <div class="inform-box flex-start-box">
                                    <i data-feather="map-pin"></i>
                                    <p>{{ config('stock.info.address') }}</p>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)">
                                <div class="inform-box">
                                    <i data-feather="phone"></i>
                                    <p>{{ config('stock.info.phone') }}</p>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)">
                                <div class="inform-box">
                                    <i data-feather="mail"></i>
                                    <p>{{ config('stock.info.mail') }}</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="sub-footer section-small-space">
            <div class="left-footer">
                <p>2023 Copyright By {{ config('stock.info.name') }} Powered By <a href="http://ryeda.com/">Ryeda</a></p>
            </div>
            <div class="right-footer">
                <ul class="payment-box">
                    <li>
                        <img src="{{ asset('assets/shop/images/icon/paymant/visa.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/shop/images/icon/paymant/discover.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/shop/images/icon/paymant/american.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/shop/images/icon/paymant/master-card.png') }}" alt="">
                    </li>
                    <li>
                        <img src="{{ asset('assets/shop/images/icon/paymant/giro-pay.png') }}" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
