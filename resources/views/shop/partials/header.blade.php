      <style>
        .border-none{
            border : none !important;
        }
      </style>
      <!-- mobile fix menu start -->
      <div class="mobile-menu d-md-none d-block mobile-cart">
          <ul>
              <li class="active">
                  <a href="{{ route('shop.home') }}">
                      <i class="iconly-Home icli"></i>
                      <span>{{ __('app.commun.home') }} </span>
                  </a>
              </li>

              <li class="mobile-category">
                  <a href="javascript:void(0)">
                      <i class="iconly-Category icli js-link"></i>
                      <span>{{ __('app.shop.filter.categories') }}</span>
                  </a>
              </li>

              <li>
                  <a href="{{ route('shop.index') }}" class="search-box">
                      <i class="iconly-Search icli"></i>
                      <span>{{ __('app.commun.shop') }}</span>
                  </a>
              </li>

              <li>
                  <a href="{{ route('shop.wishlist') }}" class="notifi-wishlist">
                      <i class="iconly-Heart icli"></i>
                      <span>{{ __('app.profile.wishlist') }}</span>
                  </a>
              </li>

              <li>
                  <a href="{{ route('shop.cart.index') }}">
                      <small
                          class="badge-number cart-count text-white">{{ Session::has('cart') ? count(Session::get('cart')) : 0 }}</small>
                      <i class="iconly-Bag-2 icli fly-cate"></i>
                      <span>{{ __('app.shop.item.cart') }}</span>
                  </a>
              </li>

          </ul>
      </div>
      <!-- mobile fix menu end -->
      <!-- Header Start -->
      
      <header class="header-2">
          {{-- <div class="header-notification theme-bg-color overflow-hidden py-2">
              <div class="notification-slider">
                  <div>
                      <div class="timer-notification text-center">
                          <h6><strong class="me-1">Welcome to Ryeda!</strong>Build your Website, your WebApp with best
                              SEO
                              .<strong class="ms-1">Phone: 58193836
                              </strong>
                          </h6>
                      </div>
                  </div>

                  <div>
                      <div class="timer-notification text-center">
                          <h6>Buy Now !<a href="SSG/Library.html" class="text-white">New: SSG for library</a>
                          </h6>
                      </div>
                  </div>
              </div>
              <button class="btn close-notification"><span>Close</span> <i class="fas fa-times"></i></button>
          </div> --}}

          <div class="top-nav top-header sticky-header sticky-header-3">
              <div class="container-fluid-lg">
                <div class="row">
                        <div class="col-12 mobile-search-box">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => 'shop.index',
                                'method' => 'GET',
                                'id' => 'product-search-form',
                            ]) !!}
                            <div class="container-fluid-lg m-2">
                                <div class="row">
                                    <div class="col-xxl-6 col-xl-8 mx-auto">
                                        <div class="border-none">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="name"
                                                    value="{{ request()->get('name') }}"
                                                    placeholder="{{ __('app.commun.search_for_product') }}"
                                                    aria-label="Example text with button addon" wfd-id="id2">
                                                <button type="submit" class="btn theme-bg-color text-white m-0">
                                                    <i class="iconly-Search icli"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                      <div class="col-12">
                          <div class="navbar-top">
                              <button class="navbar-toggler d-xl-none d-block p-0 me-3" type="button"
                                  data-bs-toggle="offcanvas" data-bs-target="#primaryMenu">
                                  <span class="navbar-toggler-icon">
                                      <i class="fa-solid fa-bars"></i>
                                  </span>
                              </button>
                              <a href="{{ route('shop.home') }}" class="nav-logo">
                                  <img src="{{ asset(config('stock.info.image')) }}" class="img-fluid blur-up lazyload"
                                      alt="">
                              </a>

                              <div class="search-full">
                                  <div class="input-group">
                                      <span class="input-group-text">
                                          <i data-feather="search" class="font-light"></i>
                                      </span>
                                      <input type="text" class="form-control search-type"
                                          placeholder="{{ __('app.commun.search_here') }}">
                                      <span class="input-group-text close-search">
                                          <i data-feather="x" class="font-light"></i>
                                      </span>
                                  </div>
                              </div>

                              {!! Form::open([
                                  'enctype' => 'multipart/form-data',
                                  'route' => 'shop.index',
                                  'method' => 'GET',
                                  'id' => 'product-search-form',
                              ]) !!}
                              <div class="middle-box">
                                  <div class="center-box">
                                      <div class="searchbar-box order-xl-1 d-none d-xl-block">
                                          <input type="search" class="form-control" name="name"
                                              value="{{ request()->get('name') }}"
                                              placeholder="{{ __('app.commun.search_for_product') }}">
                                          <button type="submit" class="btn search-button">
                                              <i class="iconly-Search icli"></i>
                                          </button>
                                      </div>
                                      {{-- <div class="location-box-2">
                                    <button class="btn location-button" data-bs-toggle="modal"
                                        data-bs-target="#locationModal">
                                        <i class="iconly-Location icli"></i>
                                        <span class="locat-name">Your Location</span>
                                        <i class="fa-solid fa-angle-down down-arrow"></i>
                                    </button>
                                </div> --}}
                                  </div>
                              </div>
                              {!! Form::close() !!}

                              <div class="rightside-menu">
                                  <div class="dropdown-dollar">
                                      <div class="dropdown">
                                          <button class="dropdown-toggle" type="button" id="dropdownMenuButton1"
                                              data-bs-toggle="dropdown" aria-expanded="false">
                                              @php
                                                  switch (str_replace('_', '-', app()->getLocale())) {
                                                      case 'ar':
                                                          $lang = 'العربية';
                                                          break;
                                                      case 'fr':
                                                          $lang = 'Français';
                                                          break;
                                                      case 'en':
                                                          $lang = 'English';
                                                          break;
                                                  }
                                                  
                                              @endphp
                                              <span>{{ $lang }}</span> <i class="fa-solid fa-angle-down"></i>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                              <li>
                                                  <a id="ar" class="dropdown-item"
                                                      href="{{ LaravelLocalization::getLocalizedURL('ar', null, [], true) }}">العربية</a>
                                              </li>
                                              <li>
                                                  <a id="eng" class="dropdown-item"
                                                      href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">English</a>
                                              </li>
                                              {{-- <li>
                                                  <a id="fr" class="dropdown-item"
                                                      href="{{ LaravelLocalization::getLocalizedURL('fr', null, [], true) }}">Français</a>
                                              </li> --}}


                                          </ul>
                                      </div>
                                      @php
                                          $currencies = \App\Models\Currency::all();
                                      @endphp

                                      <div class="dropdown">
                                          <button class="dropdown-toggle m-0" type="button" id="dropdownMenuButton2"
                                              data-bs-toggle="dropdown" aria-expanded="false">
                                              <span>{{ session('currency')->name }}</span> <i
                                                  class="fa-solid fa-angle-down"></i>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                              @foreach ($currencies as $currency)
                                                  <li>
                                                      <a class="dropdown-item currency-option" href="#"
                                                          data-currency-id="{{ $currency->id }}">{{ $currency->name }}</a>
                                                  </li>
                                              @endforeach
                                          </ul>
                                      </div>

                                  </div>

                                  <div class="option-list">
                                      <ul>
                                          <li class="onhover-dropdown">
                                              @if (auth()->user())
                                                  <a href="javascript:void(0)" class="header-icon swap-icon "
                                                      style="
                                            padding: 0%;
                                            background:url('{{ asset(auth()->user()->image_url) }}') no-repeat center,center;
                                            background-size:cover">
                                                      {{-- <img src="{{ asset(auth()->user()->image_url) }}"
                                                    class="rounded-circle lazyload " alt="" width="100%"> --}}
                                                  @else
                                                      <a href="javascript:void(0)" class="header-icon swap-icon"
                                                          style="padding: 0%">
                                                          <i class="iconly-Profile icli"></i>
                                              @endif
                                              </a>
                                              <div class="onhover-div onhover-div-login">
                                                  <ul class="user-box-name">
                                                      @if (!auth()->user())
                                                          <li class="product-box-contain">
                                                              <i></i>
                                                              <a
                                                                  href="{{ route('shop.login') }}">{{ __('login.log_in') }}</a>
                                                          </li>
                                                          <li class="product-box-contain">
                                                              <a
                                                                  href="{{ route('shop.register') }}">{{ __('app.form_contact.sign_up') }}</a>
                                                          </li>
                                                          <li class="product-box-contain">
                                                              <a
                                                                  href="{{ route('shop.password.request') }}">{{ __('login.forgot_password') }}</a>
                                                          </li>
                                                      @else
                                                        <li class="product-box-contain">
                                                            <a class="disabled" href="javascript:void(0)">{{auth()->user()->name}}</a>
                                                        </li>
                                                          <li class="product-box-contain">
                                                              <i></i>
                                                              <a
                                                                  href="{{ route('shop.profile') }}">{{ __('app.profile.profile') }}</a>
                                                          </li>
                                                          <li class="product-box-contain">
                                                              <i></i>
                                                              <a
                                                                  href="{{ route('shop.order') }}">{{ __('app.commun.your_order') }}</a>
                                                          </li>
                                                          <li class="product-box-contain">
                                                              <a
                                                                  href="{{ route('shop.logout') }}">{{ __('login.log_out') }}</a>
                                                          </li>
                                                      @endif
                                                  </ul>
                                              </div>

                                          </li>

                                          <li class="d-none">
                                              <a href="javascript:void(0)" class="header-icon search-box search-icon">
                                                  <i class="iconly-Search icli"></i>
                                              </a>
                                          </li>

                                          <li class="onhover-dropdown">
                                              <a href="{{ route('shop.wishlist') }}" class="header-icon swap-icon">
                                                  <i class="iconly-Heart icli"></i>
                                              </a>

                                          </li>

                                          <li class="onhover-dropdown" id="cart-hover">
                                              @include('shop.layouts.cart-hover')
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="container-fluid-lg">
              <div class="row">
                  <div class="col-12">
                      <div class="main-nav">
                          <div class="header-nav-left">
                              <button class="dropdown-category dropdown-category-2">
                                  <i class="iconly-Category icli"></i>
                                  <span>{{ __('app.shop.filter.categories') }}</span>
                              </button>

                              <div class="category-dropdown">
                                  <div class="category-title">
                                      <h5>Categories</h5>
                                      <button type="button" class="btn p-0 close-button text-content">
                                          <i class="fa-solid fa-xmark"></i>
                                      </button>
                                  </div>
                                  @php
                                  $cats = Helper::categories_parent(); @endphp
                                  @if (!empty($cats))
                                      <ul class="category-list">
                                          @foreach ($cats as $cat)
                                              @php
                                                  $childCats = Helper::categorie_children($cat->id);
                                              @endphp

                                              @if (!$childCats->isEmpty())
                                                  <li class="onhover-category-list">
                                                      <a href="{{ route('shop.index', ['categories' => $cat->id]) }}"
                                                          class="category-name">
                                                          {{-- <img src="{{ asset('assets/shop/svg/1/vegetable.svg') }}" alt=""> --}}
                                                          <h6>{{ $cat->name }}</h6>
                                                          <i class="fa-solid fa-angle-right"></i>
                                                      </a>

                                                      <div class="onhover-category-box">
                                                          @foreach ($childCats as $key => $child)
                                                              @php $catgs_2 = Helper::categorie_children($child->id); @endphp
                                                              @if (!$catgs_2->isEmpty())
                                                                  <div class="list-{{ $key }}">
                                                                      <div class="category-title-box">
                                                                          <a
                                                                              href="{{ route('shop.index', ['categories' => $child->id]) }}">
                                                                              <h5>{{ $child->name }}</h5>
                                                                          </a>
                                                                      </div>
                                                                      <ul>
                                                                          @foreach ($catgs_2 as $child2)
                                                                              <li>
                                                                                  <a
                                                                                      href="{{ route('shop.index', ['categories' => $child->id2]) }}">{{ $child2->name }}</a>
                                                                              </li>
                                                                          @endforeach

                                                                      </ul>
                                                                  </div>
                                                              @else
                                                                  <div class="list-{{ $key }}">
                                                                      <div class="category-title-box">
                                                                          <a
                                                                              href="{{ route('shop.index', ['categories' => $child->id]) }}">
                                                                              <h5>{{ $child->name }}</h5>
                                                                          </a>
                                                                      </div>
                                                                  </div>
                                                              @endif
                                                          @endforeach
                                                      </div>
                                                  </li>
                                              @else
                                                  <li class="">
                                                      <a href="{{ route('shop.index', ['categories' => $cat->id]) }}"
                                                          class="category-name">
                                                          <h6>{{ $cat->name }}</h6>
                                                      </a>
                                                  </li>
                                              @endif
                                          @endforeach
                                      </ul>
                                  @endif
                              </div>
                          </div>

                          <div class="main-nav navbar navbar-expand-xl navbar-light navbar-sticky">
                              <div class="offcanvas offcanvas-collapse order-xl-2" id="primaryMenu">
                                  <div class="offcanvas-header navbar-shadow">
                                    <h5>{{ __('app.commun.menu') }}</h5>
                                    <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas"
                                          aria-label="Close"></button>
                                  </div>
                                  <div class="offcanvas-body">
                                      <ul class="navbar-nav">
                                          <li class="nav-item "> <a class="nav-link nav-none"
                                                  href="{{ route('shop.home') }}">{{ __('app.commun.home') }} </a>
                                          </li>
                                          <li class="nav-item "> <a class="nav-link nav-none"
                                                  href="{{ route('shop.index') }}">{{ __('app.commun.shop') }}</a>
                                          </li>
                                          <li class="nav-item "> <a class="nav-link nav-none"
                                                  href="{{ route('shop.index') }}">{{ __('app.commun.about_us') }}</a>
                                          </li>
                                          <li class="nav-item "> <a class="nav-link nav-none"
                                                  href="{{ route('shop.contact_us') }}">{{ __('app.commun.contact_us') }}</a>
                                          </li>
                                          <li class="show-lang-curr nav-item dropdown">
                                              <a class="nav-link  dropdown-toggle" href="javascript:void(0)"
                                                  data-bs-toggle="dropdown">
                                                  @php
                                                      switch (str_replace('_', '-', app()->getLocale())) {
                                                          case 'ar':
                                                              $lang = 'العربية';
                                                              break;
                                                          case 'fr':
                                                              $lang = 'Français';
                                                              break;
                                                          case 'en':
                                                              $lang = 'English';
                                                              break;
                                                      }
                                                      
                                                  @endphp
                                                  <span>{{ $lang }}</span>
                                              </a>

                                              <ul class="dropdown-menu">
                                                  <li>
                                                      <a id="ar" class="dropdown-item"
                                                          href="{{ LaravelLocalization::getLocalizedURL('ar', null, [], true) }}">العربية</a>
                                                  </li>
                                                  <li>
                                                      <a id="eng" class="dropdown-item"
                                                          href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">English</a>
                                                  </li>
                                                  <li>
                                                      <a id="fr" class="dropdown-item"
                                                          href="{{ LaravelLocalization::getLocalizedURL('fr', null, [], true) }}">Français</a>
                                                  </li>

                                              </ul>
                                          </li>
                                          <li class="show-lang-curr nav-item dropdown">
                                              <a class="nav-link dropdown-toggle" href="javascript:void(0)"
                                                  data-bs-toggle="dropdown">

                                                  <span>{{ session('currency')->name }}</span> <i
                                                      class="fa-solid fa-angle-down"></i> </a>
                                              <ul class="dropdown-menu">
                                                  @foreach ($currencies as $currency)
                                                      <li>
                                                          <a class="dropdown-item currency-option" href="#"
                                                              data-currency-id="{{ $currency->id }}">{{ $currency->name }}</a>
                                                      </li>
                                                  @endforeach
                                              </ul>
                                          </li>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

                                          </ul>

                                      </ul>

                                  </div>

                              </div>
                          </div>

                          <div class="right-nav">
                              <div class="nav-number">
                                  <img src="{{ asset('assets/shop/images/icon/music.png') }}"
                                      class="img-fluid blur-up lazyload" alt="">
                                  <span><a
                                          href="tel:{{ config('stock.info.phone') }}">{{ config('stock.info.phone') }}</a></span>
                              </div>
                              <a href="javascript:void(0)" class="btn theme-bg-color ms-3 fire-button" id="hot_deal"
                                  data-bs-toggle="modal" data-bs-target="#deal-box">
                                  <div class="fire">
                                      <img src="{{ asset('assets/shop/images/icon/hot-sale.png') }}"
                                          class="img-fluid" alt="">
                                  </div>
                                  <span>{{ __('app.commun.hot_deals') }}</span>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </header>
      <!-- Header End -->
