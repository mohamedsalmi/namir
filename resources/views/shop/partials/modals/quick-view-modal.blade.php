<!-- Quick View Modal Box Start -->
<div class="modal fade theme-modal view-modal" id="view-modal" tabindex="-1"
aria-labelledby="exampleModalLabel" aria-hidden="true" >

    <div class="modal-dialog modal-dialog-centered modal-xl modal-fullscreen-sm-down">
        <div class="modal-content">
            <div class="modal-header p-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa-solid fa-xmark"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row g-sm-4 g-2">
                    <div class="col-lg-6">
                        <div class="slider-image product-image">
                            <img id="image"  class="img-fluid blur-up lazyload"
                                alt="">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="right-sidebar-modal">
                            <h4 class="title-name" id="name"></h4>
                            <h4 class="price" id="price"></h4>
                            <div class="product-rating">
                                <ul class="rating">
                                    <li>
                                        <i data-feather="star" class="fill"></i>
                                    </li>
                                    <li>
                                        <i data-feather="star" class="fill"></i>
                                    </li>
                                    <li>
                                        <i data-feather="star" class="fill"></i>
                                    </li>
                                    <li>
                                        <i data-feather="star" class="fill"></i>
                                    </li>
                                    <li>
                                        <i data-feather="star"></i>
                                    </li>
                                </ul>
                                <span class="ms-2">(8)</span>
                                {{-- <span class="ms-2 text-danger">6 sold in last 16 hours</span> --}}
                            </div>

                            <div class="product-detail">
                                <h4>{{ __('app.shop.single.product_details') }} :</h4>
                                <p id="resume"></p>
                            </div>

                            <ul class="brand-list">
                                <li>
                                    <div class="brand-box">
                                        <h5>{{ __('app.shop.single.writer') }}:</h5>
                                        <h6 class="unit"><a id="writer" >
                                        </a></h6>

                                    </div>
                                </li>
                             
                                <li>
                                    <div class="brand-box">
                                        <h5>{{ __('app.shop.single.publisher') }}:</h5>
                                        <h6 class="unit"><a id="house">
                                        </a></h6>
                                    </div>
                                </li>
                            </ul>

                            {{-- <div class="select-size">
                                <h4>Cake Size :</h4>
                                <select class="form-select select-form-size">
                                    <option selected>Select Size</option>
                                    <option value="1.2">1/2 KG</option>
                                    <option value="0">1 KG</option>
                                    <option value="1.5">1/5 KG</option>
                                    <option value="red">Red Roses</option>
                                    <option value="pink">With Pink Roses</option>
                                </select>
                            </div> --}}

                            <div class="modal-button">
                                <button id="add-cart-button" class="btn btn-md add-cart-button icon btn-cart addToCart">{{ __('app.shop.item.add_to_cart') }}</button>
                                <a id="show_btn"
                                    class="btn theme-bg-color view-button icon text-white fw-bold btn-md mx-1">
                                    {{ __('app.shop.item.view_more_details') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quick View Modal Box End -->
