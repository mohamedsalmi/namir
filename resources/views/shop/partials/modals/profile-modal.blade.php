
    <!-- Edit Profile Start -->
    {!! Form::open([
        'enctype' => 'multipart/form-data',
        'route' => 'shop.client.updateProfile',
        'method' => 'POST',
        'id' => 'update-profil-form',
    ]) !!}
    <div class="modal fade theme-modal" id="editProfile" tabindex="-1" aria-labelledby="exampleModalLabel2"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-fullscreen-sm-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">{{ __('app.profile.contact_info') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row g-4">
                        <div class="col-xxl-12">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" id="name" name="name" value="{{auth()->user()->name}}">
                                    <label for="pname">{{ __('app.form_contact.name') }}</label>
                                </div>
                        </div>
                        <div class="col-12">
                                <div class="form-floating theme-form-floating">
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{auth()->user()->email}}">
                                    <label for="email">{{ __('app.form_contact.email_adress') }}</label>
                                </div>
                        </div>
                        <div class="col-xxl-6">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{auth()->user()->phone}}">
                                    <label for="phone">{{ __('app.form_contact.phone_number') }}</label>
                                </div>
                        </div>
                        <div class="col-xxl-6">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" id="phone2" name="phone2" value="{{auth()->user()->phone2}}">
                                    <label for="phone2">{{ __('app.form_contact.phone_number') }}</label>
                                </div>
                            
                        </div>

                       

                        <div class="col-12">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" id="adresse" name="adresse"
                                        value="{{auth()->user()->adresse}}">
                                    <label for="adresse">{{ __('app.commun.adress') }}</label>
                                </div>
                            
                        </div>

                        <div class="col-6">
                            <div
                                class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                <select class="form-select single-select" id="state"
                                name="state">
                                <option value="">{{__('checkout.choose_option')}}</option>
                                @foreach ($states as $state)
                                    <option value="{{ $state->state }}" {{ old('state', (auth()->user()->state ?? '')) == $state->state ? 'selected' : '' }}>
                                        {{ $state->state }}
                                    </option>
                                @endforeach
                            </select>
                                <label for="address">{{__('checkout.state')}}</label>
                                @error('client_details.state')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                           
                        </div>

                        <div class="col-6">
                            <div
                                class="form-floating mb-lg-3 mb-2 theme-form-floating">
                                <select class="form-select single-select" id="city"
                                name="city">
                                <option value="">{{__('checkout.choose_option')}}</option>
                                @foreach ($cities as $city)
                                    <option value="{{ $city->city }}" {{ old('city', (auth()->user()->city ?? '')) == $city->city ? 'selected' : '' }}>
                                        {{ $city->city }}
                                    </option>
                                @endforeach
                            </select>
                                <label for="address">{{__('checkout.city')}}</label>
                                @error('client_details.city')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                           
                        </div>

                        {{-- <div class="col-xxl-6">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" id="rib" name="rib" value="{{auth()->user()->rib}}">
                                    <label for="addrib">RIB</label>
                                </div>
                            </form>
                        </div>
                        <div class="col-xxl-6">
                                <div class="form-floating theme-form-floating">
                                    <input type="text" class="form-control" name="mf" id="mf" value="{{auth()->user()->mf}}">
                                    <label for="mf">MF</label>
                                </div>
                        </div> --}}

                        <div class="col-xxl-6">
                            <div class="form-floating theme-form-floating">
                                <input type="password" class="form-control" id="password" name="password"
                                    value="" autocomplete="new-password">
                                <label for="password">{{ __('app.form_contact.pwd') }}</label>
                            </div>
                    </div>
                        <div class="col-xxl-6">
                            <div class="form-floating theme-form-floating">
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                                    value="" autocomplete="new-password">
                                <label for="password_confirmation">{{ __('app.form_contact.pwd_confirmation') }}</label>
                            </div>
                    </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-animation btn-md fw-bold"
                        data-bs-dismiss="modal">{{ __('app.commun.close') }}</button>
                    <button type="submit"  id="save"
                        class="btn theme-bg-color btn-md fw-bold text-light">{{ __('app.commun.save_changes') }}</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <!-- Edit Profile End -->

   