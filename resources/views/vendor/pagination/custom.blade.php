@if ($paginator->hasPages())

    <nav class="custome-pagination">
        <ul class="pagination justify-content-center">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-disabled="true">
                        <i class="fa-solid fa-angles-{{app()->getLocale() == 'ar' ? 'right' : 'left';}}"></i>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" tabindex="-1">
                        <i class="fa-solid fa-angles-{{app()->getLocale() == 'ar' ? 'right' : 'left';}}"></i>
                    </a>
                </li>
            @endif
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif



                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active">
                                <a class="page-link">{{ $page }}</a>
                            </li>
                        @else
                            <li class="page-item">
                                <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}">
                        <i class="fa-solid fa-angles-{{app()->getLocale() == 'ar' ? 'left' : 'right';}}"></i>
                    </a>
                </li>
            @else
                <li class="page-item disabled">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" tabindex="-1" aria-disabled="true">
                        <i class="fa-solid fa-angles-{{app()->getLocale() == 'ar' ? 'left' : 'right';}}"></i>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
