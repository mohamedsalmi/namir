@extends('admin.layouts.app')

@section('title', 'Recettes')


@section('stylesheets')
    <link href="{{ url('assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection


@section('content')
    <main class="page-content">
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Recettes anuelles et mensuelles</div>

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i> Tableau de
                                bord</a></li>
                        {{-- <li class="breadcrumb-item"><a href="{{route('store.show', $store->id)}}">{{$store->name}}</a> --}}
                        <li class="breadcrumb-item active" aria-current="page">Recettes anuelles et mensuelles</li>
                    </ol>
                </nav>
            </div>

        </div>


        <!--end row-->

        <div class="row">
            <div class="col-12 col-lg-12 col-xl-6 d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <h6 class="mb-0">Recette anuuelle</h6>
                            <div class="fs-5 ms-auto dropdown">
                                <select name="year" id="year" class="form-control form-control-sm"
                                    onchange="location = this.value;">
                                    @foreach ($years as $item)
                                        <option {{ $item == $year ? 'selected' : '' }}
                                            value="{{ route('admin.history.online', 'year=' . $item) }}">{{ $item }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="total-orders-by-month"></div>

                        </div>

                        <div class="table-responsive mt-2">
                            <table class="table align-middle mb-0 historytable">
                                <thead class="table-light">
                                    <tr>
                                        <th>Date</th>
                                        <th>en cours</th>
                                        <th>livrée</th>
                                        <th>Retour</th>
                                        <th>en attente</th>
                                        <th>annulée</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        setlocale(LC_TIME,'fr_FR');
                                    @endphp
                                    @foreach ($recettes as $recette)
                                        <tr>
                                            <td><a href="{{ route('admin.history.online', 'year=' . $year . '&&month=' . $recette['month']) }}">{{ date('F', mktime(0, 0, 0, $recette['month'], 1)) }}</a>
                                            </td>
                                            <td>{{ number_format($recette['progress'], 3) }} </td>
                                            <td>{{ number_format($recette['success'], 3) }} </td>
                                            <td>{{ number_format($recette['return'], 3) }} </td>
                                            <td>{{ number_format($recette['ready'], 3) }} </td>
                                            <td>{{ number_format($recette['canceled'], 3) }} </td>
                                        </tr>
                                    @endforeach
                                    <tr class="table-light">
                                        <td><strong>Total</strong></td>
                                        <td>{{number_format(collect($recettes)->sum('progress'), 3)}}</td>
                                        <td>{{number_format(collect($recettes)->sum('success'), 3)}}</td>
                                        <td>{{number_format(collect($recettes)->sum('return'), 3)}}</td>
                                        <td>{{number_format(collect($recettes)->sum('ready'), 3)}}</td>
                                        <td>{{number_format(collect($recettes)->sum('canceled'), 3)}}</td>
                       
                                
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-12 col-xl-6 d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <h6 class="mb-0">Recette mensuelle</h6>
                            <div class="fs-5 ms-auto dropdown">
                                <select class="form-control form-control-sm" onchange="location = this.value;">>
                                    @foreach ($months as $item)
                                        <option {{ $item == $month ? 'selected' : '' }}
                                            value="{{ route('admin.history.online', 'year=' . $year . '&&month=' . $item) }}">
                                            {{ date('F', mktime(0, 0, 0, $item, 1)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="total-orders-by-month"></div>

                        </div>

                        <div class="table-responsive mt-2">
                            <table class="table align-middle mb-0 historytable">
                                <thead class="table-light">
                                    <tr>
                                        <th>Date</th>
                                        <th>Jour</th>
                                        <th>en cours</th>
                                        <th>livrée</th>
                                        <th>Retour</th>
                                        <th>en attente</th>
                                        <th>annulée</th>
                                        {{-- <th>Recette Net</th> --}}
                                        {{-- <th>Caisse</th>
                                        <th>Devis</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($daylilies as $daylilie)
                                        <tr>
                                            <td>{{ $daylilie['day'] }}</td>
                                            <td>{{ date('l', mktime(0, 0, 0, $daylilie['month'], $daylilie['day'], $year)) }}
                                            </td>
                                            <td>{{ number_format($daylilie['progress'], 3) }} </td>
                                            <td>{{ number_format($daylilie['success'], 3) }} </td>
                                            <td>{{ number_format($daylilie['return'], 3) }} </td>
                                            <td>{{ number_format($daylilie['ready'], 3) }} </td>
                                            <td>{{ number_format($daylilie['canceled'], 3) }} </td>
                                        </tr>
                                    @endforeach
                                    <tr class="table-light">
                                        <td><strong>Total</strong></td>
                                        <td></td>
                                        <td>{{number_format(collect($daylilies)->sum('progress'), 3)}}</td>
                                        <td>{{number_format(collect($daylilies)->sum('success'), 3)}}</td>
                                        <td>{{number_format(collect($daylilies)->sum('return'), 3)}}</td>
                                        <td>{{number_format(collect($daylilies)->sum('ready'), 3)}}</td>
                                        <td>{{number_format(collect($daylilies)->sum('canceled'), 3)}}</td>
                       
                                
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end row-->



    </main>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/table-datatable.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript">
        function toMonthName(monthNumber) {
            const date = new Date();
            date.setMonth(monthNumber - 1);

            return date.toLocaleString('fr-FR', {
                month: 'short',
            });
        }

        //   $('#year').change(function () {
        //         var year = $(this).val();
        //         alert(year)

        //         $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             }
        //         });

        //         $.ajax({
        //             url : "/statistic/cart/total/" + year,
        //             type: "GET",
        //             async : false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
        //             success: function(response, textStatus, jqXHR) {
        //                 let data = response.data.map(element => { return {x: toMonthName(element.month), y : element.total} });
        //                 chart.updateSeries([{
        //                     name: 'Total',
        //                     data: data
        //                 }]);
        //             },
        //             error: function (jqXHR, textStatus, errorThrown) {
        //                 console.log(jqXHR, textStatus, errorThrown);
        //             }
        //         });
        //   }); 
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
