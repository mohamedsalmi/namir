    @extends('admin.layouts.public')

    @section('title', 'Chercher')
    <!--start wrapper-->
    <div class="wrapper">


        @section('content')
        <!--start content-->
        <main class="authentication-content" style="background-image: url('assets/admin/images/backgroud.jpg');">
            <div class="container-fluid">
                <div class="authentication-card">
                    <div class="card bg-light shadow radius-30 overflow-hidden ">
                        <div class="row g-0 ">
                            <div class="col-lg-4 bg-white d-flex align-items-center justify-content-center">
                                <img src="assets/images/logoo.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-lg-8 text-center">
                                <div class="card-body p-4 p-sm-5">
                                    <h5 class="card-title">Chercher dans {{config('stock.info.name')}}</h5>
                                    <p class="card-text mb-5">Vous pouvez chercher avec nom ou code à bar de l'article!</p>
                                    <form class="form-body" method="GET" action="{{ route('admin.product.showproduct') }}">
                                        @csrf                               
                                        <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Scanner code à bare</h5>
                                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">   
                                                                      <div id="qr-reader"></div>
                                              </div>
                                             
                                              </div>
                                            </div>
                                          </div>
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <div class="ms-auto position-relative">
                                                    <!-- <input type="email" class="form-control form-control-sm radius-30 ps-5" id="inputEmailAddress" placeholder="Email Address"> -->
                                                    <div class=" translate-middle-y search-icon px-3" style="position: absolute; top: 19; z-index: 999999999;"><i class="bi bi-search"></i></div>
                                                    <div class="input-group ">
                                                        <input  name="product" id="sku" type="text" placeholder="Chercher..." class="form-control form-control-sm radius-30 ps-5"    @error('product') is-invalid @enderror placeholder="product" value="{{ old('product', '') }}" required autocomplete="product" autofocus>
                                                        <button data-bs-toggle="modal" data-bs-target="#camera" id='barcode' type="button"  class="btn btn-outline-warning text-primary bx bx-barcode-reader radius-30 "></button>
                                                      </div>
                                                      @error('product')
                                                      <div class="invalid-feedback">{{ $message }}</div>
                                                     @enderror
                                                </div>
                                                
                                            </div>                                    
                                       
                                            <!-- <div class="col-6 text-end">	<a href="authentication-forgot-password.html">Forgot Password ?</a> -->
                                        </div>
                                        <div class="col-12 mt-4">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-primary radius-30">Chercher</button>
                                            </div>
                                        </div>
                                        <!-- <div class="col-12">
                                            <p class="mb-0">Don't have an account yet? <a href="authentication-signup.html">Sign up here</a></p>
                                        </div> 
                                    </div>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </main>

    <!--end page main-->

    @endsection
    @section('scripts')
    <script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>
    @if (Session::has('success'))
    <script>
        $(function() {
            Swal.fire({
                icon: 'success',
                title: "{{ Session::get('success') }}",
                showConfirmButton: false,
                timer: 2000
            })
        })
    </script>

    {{ Session::forget('success') }}
    {{ Session::save() }}
@endif

@if (Session::has('error'))
    <script>
        $(function() {
            Swal.fire({
                icon: 'error',
                title: "{{ Session::get('error') }}",
                showConfirmButton: false,
                timer: 2000
            })
        })
    </script>
@endif
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script>
       function onScanSuccess(decodedText, decodedResult) {
          //alert(`Code scanned = ${decodedText}`, decodedResult);
          $('#sku').val(decodedText);    
          $('#camera').modal('hide');
     
      }
   
      $(document).on("click", "#barcode", function (e) {
        const formatsToSupport = [
          Html5QrcodeSupportedFormats.ITF
      ];
      var html5QrcodeScanner = new Html5QrcodeScanner(
          "qr-reader", {
              fps: 10,
              qrbox: 250,
              experimentalFeatures: {
                  useBarCodeDetectorIfSupported: false
              }
          });
      html5QrcodeScanner.render(onScanSuccess);
      html5QrcodeScanner.clear();
      })
      
  </script>
    @endsection