<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset(config('stock.info.name'))}}" type="image/png" />
    <!-- Bootstrap CSS -->
    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/bootstrap-extended.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/icons.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- loader-->
    <link href="assets/css/pace.min.css" rel="stylesheet" />
    @yield('stylesheets')
    <title>@yield('title', 'Stock')</title>
</head>

<body>

    <!--start wrapper-->
    <div class="wrapper">

        @yield('content')

    </div> 
    <!--end wrapper-->


    <!--plugins-->
    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script src="{{asset('assets/admin/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/pace.min.js')}}"></script>
    <script src="assets/js/app.js"></script>
    @yield('scripts')

</body>

</html>