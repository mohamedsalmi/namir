<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Stock')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <!-- Theme head -->
    <!-- Required meta tags -->
    <link rel="icon" href="{{asset(config('stock.info.image'))}}" type="image/png" />
    <!--plugins-->
    <link href="{{asset('assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/bootstrap-extended.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/icons.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">


    <!-- loader-->
    <link href="{{asset('assets/admin/css/pace.min.css')}}" rel="stylesheet" />

    <!--Theme Styles-->
    <link href="{{asset('assets/admin/css/dark-theme.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/light-theme.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/semi-dark.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/header-colors.css')}}" rel="stylesheet" />
    <style>
        .required .form-label:after { 
            content:"*";
            color:red;
        }
    </style>
    <!-- Theme head -->
    @yield('stylesheets')
</head>

<body>
    <!--start wrapper-->
    <div class="wrapper">
        @include('admin.partials.header')

        @include('admin.partials.sidebar')

        @yield('content')

        @include('admin.partials.extras')
    </div>
    <!--end wrapper-->


    <!-- Bootstrap bundle JS -->
    <script src="{{asset('assets/admin/js/bootstrap.bundle.min.js')}}"></script>
    <!--plugins-->
    <script src="{{asset('assets/admin/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/simplebar/js/simplebar.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/metismenu/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('assets/admin/js/pace.min.js')}}"></script>
    <!--app-->
    <script src="{{asset('assets/admin/js/app.js')}}"></script>
    <script src="{{asset('assets/admin/js/index.js')}}"></script>
    <!-- <script>
            new PerfectScrollbar(".best-product")
        </script> -->
        <script type="text/javascript">
            $(document).ready(function () {
                  $(document).keydown(function(e) {
                switch (e.which) {
                    case 112: // F1
                        e.preventDefault();
                        var url='/admin/cart/create';
                        window.open(url, '_blank');
                        break;
        
                }
            });
    //         $(document).on('select2:open', () => {
    //     document.querySelector('.select2-search__field').focus();
    //   });
            });
        </script>
        @yield('scripts')
</body>

</html>