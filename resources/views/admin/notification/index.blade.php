@extends('admin.layouts.app')

@section('title', 'Liste des notifications')

@section('stylesheets')
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Notifications</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des notifications</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="">
            <div class="">
                <div class="container py-2">
                    <h2 class="font-weight-light text-center text-muted py-3">Notifications</h2>
                    @if (auth()->user()->notifications->where('type', '!=', 'App\Notifications\NewWebsiteOrderNotification')->count() == 0)
                        <a class="dropdown-item">
                            <div class="text-center">Aucune notification disponible</div>
                        </a>
                    @endif
                    @foreach (auth()->user()->notifications->where('type', '!=', 'App\Notifications\NewWebsiteOrderNotification') as $notification)
                        <div class="row">
                            <div class="col py-2">
                                <div class="float-end text-muted m-1">
                                    {{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</div>
                                <div
                                    class="@if($notification->read_at) opacity-50 @endif alert border-0 border-warning border-start border-4 bg-light-warning alert-dismissible fade show py-2">
                                    <h4 class="text-warning"><i class="bi bi-exclamation-triangle-fill"></i>
                                        {{ \App\Helpers\Helper::formatNotificationType($notification->type) }}</h4>
                                    <p>{{ \App\Helpers\Helper::getNotificationMessage($notification) }}</p>
                                </div>
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </main>
    <!--end page main-->
@endsection