@extends('admin.layouts.app')

@section('title', 'Liste des Paiements')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Décaissement
            </div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste de paiements</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <div class="col-md-3">
                    <h5 class="mb-2 mb-sm-0">Liste des paiements</h5>
                </div>
                <div class="col-md-3 offset-md-6">
                    <h4 class="mb-0 text-success"> Total: <span id="total">{{ number_format($total, 3) }}</span> </h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">
                    <div class="card-body">
                        <div class="table-responsive">

                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="input-group col-md-6 mb-1">
                                    <span class="input-group-text">Du</span>
                                    <input required type="date" value="{{ request()->get('from') }}"
                                        class="form-control form-control-sm" name="from" placeholder="De" id="from">
                                    <span class="input-group-text">Au</span>
                                    <input required type="date" value="{{ request()->get('to') }}"
                                        class="form-control form-control-sm" name="to" placeholder="Au" id="to">
                                    <span class="input-group-text">Etat</span>
                                    <select class="form-control form-control-sm" name="" id="statusfilter">
                                        <option value="">Tous</option>
                                        <option value="Payé">Payé</option>
                                        <option value="Non payé">Non payé</option>
                                        <option value="Payé partiellement">Payé partiellement</option>
                                    </select>
                                    <span class="input-group-text">Type</span>
                                    <select class="form-control form-control-sm" name="" id="typefilter">
                                        <option value="">Tous</option>
                                        <option value="Espèce">Espèce</option>
                                        <option value="Chèque">Chèque</option>
                                        <option value="Virement">Virement</option>
                                        <option value="Traite">Traite</option>
                                        <option value="R à S">R à S</option>
                                        <option value="Retour">Retour</option>
                                    </select>
                                </div>
                            </div>

                            <table id="example" class="table align-middle">
                                <thead class="table-light">
                                    <tr>
                                        <th>Fournisseur</th>
                                        <th>Numéro</th>
                                        <th>Total</th>
                                        <th>Etat</th>
                                        <th>Types de réglement</th>
                                        <th>Date</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($collection as $providercommitment)
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 cursor-pointer">
                                                    <div>
                                                        <p class="mb-0">
                                                            {{ $providercommitment->provider()->exists() ? str_pad($providercommitment->provider->id, 4, 0, STR_PAD_LEFT) : '0000' }}
                                                            - {{ $providercommitment->provider_details['name'] }} -
                                                            {{ $providercommitment->provider_details['mf'] }}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                @switch(class_basename($providercommitment))
                                                    @case('Voucher')
                                                        <span class="badge alert-success">BA :
                                                            {{ $providercommitment->codification }}</span>
                                                    @break

                                                    @case('ReturnCoupon')
                                                        <span class="badge alert-warning">BR :
                                                            {{ $providercommitment->codification }}</span>
                                                    @break

                                                    @default
                                                @endswitch
                                            </td>
                                            <td>{{ $providercommitment->total }}</td>
                                            @php
                                                if ($providercommitment->paiment_status == 0) {
                                                    if ($providercommitment->payments()->exists()) {
                                                        $status = '<td><span class="badge rounded-pill alert-warning">Payé partiellement</span></td>';
                                                    } else {
                                                        $status = '<td><span class="badge rounded-pill alert-danger">Non payé</span></td>';
                                                    }
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                            <td>
                                                @php
                                                    $methods = [];
                                                    foreach ($providercommitment->commitments as $key => $commitment) {
                                                        foreach ($commitment->paiements as $key => $paiement) {
                                                            array_push($methods, App\Helpers\Helper::getPaymentMethodName($paiement->type));
                                                        }
                                                    }
                                                    $methods = implode(', ', array_unique($methods));
                                                @endphp
                                                <span>
                                                    {{ $methods }}
                                                </span>
                                            </td>

                                            <td>{{ $providercommitment->date }}</td>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 fs-6">
                                                    @canany(['Détails décaissement'])
                                                        @if ($providercommitment->commitments()->exists())
                                                            <a href="{{ route('admin.providercommitment.show', ['voucher' => $providercommitment->id, 'type' => strtolower(class_basename($providercommitment))]) }}"
                                                                class="text-primary" data-bs-toggle="tooltip"
                                                                data-bs-placement="bottom" title=""
                                                                data-bs-original-title="Voir détails" aria-label="Views"><i
                                                                    class="bi bi-eye-fill"></i></a>
                                                        @endif
                                                    @endcanany
                                                    @canany(['Modifier décaissement'])
                                                        @if (!$providercommitment->commitments()->exists())
                                                            <a href="{{ route('admin.providercommitment.edit', ['voucher' => $providercommitment->id, 'type' => strtolower(class_basename($providercommitment))]) }}"
                                                                class="text-success" data-bs-toggle="tooltip"
                                                                data-bs-placement="bottom" title=""
                                                                data-bs-original-title="Modifier" aria-label="Modifier"><i
                                                                    class="bi bi-pencil-fill"></i></a>
                                                        @endif
                                                    @endcanany
                                                    {{-- <a href="{{ route('admin.providercommitment.delete', $providercommitment->id) }}"
                                                        class="text-danger" data-bs-toggle="tooltip"
                                                        data-bs-placement="bottom" title=""
                                                        data-bs-original-title="Supprimer" aria-label="Supprimer"><i
                                                            class="bi bi-trash-fill"></i></a> --}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="searchable">Fournisseur</th>
                                        <th class="searchable">Numéro</th>
                                        <th class="searchable">Total</th>
                                        <th class="searchable">Etat</th>
                                        <th class="searchable">Types de réglement</th>
                                        <th class="searchable">Date</th>
                                        <th class="not-export-col">Actions</th>

                                        {{-- <th class="searchable">id</th>
                                        <th class="searchable">Produit</th>
                                        <th class="searchable">Goût</th>
                                        <th class="searchable">Brand</th>
                                        <th class="searchable">Categorie</th>
                                        <th class="searchable">SKU</th>
                                        <th class="searchable">Quantity</th>
                                        <th class="searchable">P.Détails</th>
                                        <th ></th> --}}
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end row-->

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script> --}}
    <script src="{{ asset('assets/admin/plugins/jquery-query-object/jquery.query-object.js') }}"></script>

    <script>
        $(document).ready(function() {
            // var newUrl = $.query.set("section", 5).set("action", "do").toString();
            // window.location.search = newUrl;
        });
    </script>

    <script>
        $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
            var statusfilter = $('#statusfilter').val();
            var status = data[3] || '';

            if (
                statusfilter == '' || status == statusfilter
            ) {
                return true;
            }
            return false;
        });

        $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
            var typefilter = $('#typefilter').val();
            var type = data[4] || '';

            if (
                typefilter == '' || type.includes(typefilter)
            ) {
                return true;
            }
            return false;
        });

        $(document).ready(function() {
            var table = $('#example').DataTable({
                order: [
                    [3, 'asc'],
                    [5, 'desc']
                ],
                columnDefs: [
                    { type: 'num-fmt', targets: 2 }
                ],
                lengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Tous"]
                ],
                pageLength: 100,
                initComplete: function() {

                },
            });

            // Event listener to the two range filtering inputs to redraw on input
            $('#statusfilter').change(function() {
                table.draw();
                var sum = table.columns(2, {
                    search: 'applied'
                }).data().sum();
                $('#total').text(sum.toFixed(3));
            });

            $('#typefilter').change(function() {
                table.draw();
                var sum = table.columns(2, {
                    search: 'applied'
                }).data().sum();
                $('#total').text(sum.toFixed(3));
            });

            table.on('search.dt', function() {
                var sum = table.columns(2, {
                    search: 'applied'
                }).data().sum();
                $('#total').text(sum.toFixed(3));
            });

            // $('#example tfoot .searchable').each(function() {
            //     var title = $(this).text();
            //     $(this).html('<input class="form-control form-control-sm" type="text" placeholder="Rechercher par ' + title.toLowerCase() +
            //         '" />');
            // });

        });
    </script>

    <script>
        function dateCheck(from, to, check) {
            var fDate, lDate, cDate;
            fDate = Date.parse(from);
            lDate = Date.parse(to);
            cDate = Date.parse(check);

            if ((cDate <= lDate && cDate >= fDate)) {
                return true;
            }
            return false;
        }

        function dateFormat(date) {
            var todayDate = new Date(date).toISOString().slice(0, 10);
            return todayDate;
        }
    </script>

    <script>
        jQuery.fn.dataTable.Api.register('sum()', function() {
            return this.flatten().reduce(function(a, b) {
                if (typeof a === 'string') {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if (typeof b === 'string') {
                    b = b.replace(/[^\d.-]/g, '') * 1;
                }

                return a + b;
            }, 0);
        });
    </script>

    <script>
        $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
            var from = $('#from').val();
            var to = $('#to').val();
            var date = data[5] || '';
            if (
                from == '' || to == '' || dateCheck(dateFormat(from), dateFormat(to), dateFormat(date))
            ) {
                return true;
            }
            return false;
        });

        $(document).ready(function() {
            var table = $('#example').DataTable();
            let _this = this;
            $('#from, #to').change(function() {
                var newUrl = $.query.set("from", $("#from").val()).set("to", $("#to").val()).toString();
                if ($("#from").val() != '' && $("#to").val() != '')
                    window.location.search = newUrl;
                table.draw();
                var sum = table.columns(2, {
                    search: 'applied'
                }).data().sum();
                $('#total').text(sum.toFixed(3));
            });
        });
    </script>
@endsection
