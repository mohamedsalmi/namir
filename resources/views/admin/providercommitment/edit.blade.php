@extends('admin.layouts.app')

@section('title', 'Modifier paiement')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Décaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.providercommitment.index') }}">Liste des
                                paiements</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier paiement</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => [
                'admin.providercommitment.updatecommitments',
                ['voucher' => $voucher->id, 'type' => strtolower(class_basename($voucher))],
            ],
            'method' => 'POST',
            'id' => 'create-voucher-form',
        ]) !!}
        <div class="row">
            <input type="hidden" id="total_voucher" value="{{ $voucher->total }}" name="total">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <div class="col">
                                <div class="card radius-10 border-0 border-start border-success border-3 mb-0">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="">
                                                <p class="mb-1"><strong>BL : {{ $voucher->codification }}</strong></p>
                                                <h4 class="mb-0 text-success">{{ $voucher->total }} </h4>
                                            </div>
                                            <div class="ms-auto widget-icon bg-success text-white">
                                                <i class="bi bi-currency-dollar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            {{-- <h6 class=" text-center text-info">Détails de paiement</h6> --}}
                                            <div class="col-12">
                                                @php
                                                    $commitments = $voucher->commitments;
                                                    $noattributes = isset($commitments) ? count($commitments) : 0;
                                                    if (isset($commitments)) {
                                                        if (!is_array($commitments)) {
                                                            $nocommitments = count(json_decode($commitments, true));
                                                            $commitments = json_decode($commitments);
                                                        }
                                                    } else {
                                                        $commitments = [];
                                                    }
                                                @endphp
                                                <div class="form-check form-switch">
                                                    {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                    @if ($noattributes > 1)
                                                        <input class="form-check-input" name="Deadline_check"
                                                            type="checkbox" id="Deadline_check" checked>
                                                    @else
                                                        <input class="form-check-input" name="Deadline_check"
                                                            type="checkbox" id="Deadline_check">
                                                    @endif
                                                    <label class="form-check-label" for="Deadline">Echéance</label>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group col-12 col-lg-4">

                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="timbre" type="checkbox">
                                                    <label class="form-label" for="exampleFormControlInput1"><i
                                                            class="fa fa-tag"></i> Timbre </label>
                                                </div>

                                            </div> --}}
                                            <div class="col-lg-12 mx-auto" id="Add_paiement">
                                                <div class="card">
                                                    <div class="card-header py-3 bg-transparent">
                                                        <div class="d-sm-flex align-items-center">

                                                            <div class="col-4">
                                                                <h5 class="modal-title text-info">Liste des écheances </h5>
                                                            </div>
                                                            <h6 class="form-label m-3">Montant total à payer :</h6>
                                                            <input type="text"
                                                                class="btn btn-sm btn-warning px-5 radius-30 total"
                                                                value="{{ old('total', $voucher->total, '') }}" readonly>

                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="repeat_items">
                                                            @if ($nocommitments > 0)
                                                                @foreach ($commitments as $key => $item)
                                                                    <div class="card bg-light row pt-2">
                                                                        <div>
                                                                            <button type="button" class="btn-close"
                                                                                id="deleteItem"></button>
                                                                        </div>
                                                                        <div class="card-body ">
                                                                            <h6 class="text-info">Echéance
                                                                                {{ $key + 1 }}:</h6>
                                                                            <div class="row">
                                                                                <div class="col-lg-5">
                                                                                    <div>
                                                                                        <br>
                                                                                        <h6>Montant:</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div class=" col-lg-7 ">
                                                                                    <div class="input-group mb-3">
                                                                                        <input
                                                                                            id="Deadline{{ $key }}.amount"
                                                                                            name="Deadline{{ $key }}[amount]"
                                                                                            type="number"
                                                                                            class="form-control"
                                                                                            aria-label="Text input with checkbox"
                                                                                            value="{{ old('amount', $item->amount, '') }}">
                                                                                        <div class=" col-4">
                                                                                            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'Deadline[' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-5">
                                                                                    <div>
                                                                                        <br>
                                                                                        <h6>Date d'écheance:</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div class=" col-lg-7 ">
                                                                                    <input
                                                                                        id="Deadline.{{ $key }}.date"
                                                                                        name="Deadline{{ $key }}[date]"
                                                                                        type="date" class="form-control"
                                                                                        aria-label="Text input with checkbox"
                                                                                        value="{{ old('date', $item->date, '') }}">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>

                                                        <div class="ms-auto">
                                                            <button role="button" class="btn btn-sm btn-sm btn-warning"
                                                                id="addItem" onclick="return 0;"><i
                                                                    class="bi bi-plus"></i> Ajouter une
                                                                écheance</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-1">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-success px-2 rounded-pill" id="addCash">
                                                            <i class="bi bi-currency-dollar"></i> Espèce</button></div>

                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-2 rounded-pill" id="addCheck">
                                                            <i class="bi bi-card-heading"></i> Chèque</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-secondary px-2 rounded-pill" id="addTransfer">
                                                            <i class="bi bi-bank2"></i> Virement</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-danger px-2 rounded-pill" id="addExchange">
                                                            <i class="bi bi-file-earmark-text"></i> Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        @php
                                                            $paiements = $voucher?->commitments?->first()?->paiements;
                                                            $nopaiements = is_array($paiements) ? count($paiements) : 0;
                                                            if (isset($paiements)) {
                                                                if (!is_array($paiements)) {
                                                                    $nopaiements = count(json_decode($paiements, true));
                                                                    $paiements = json_decode($paiements);
                                                                }
                                                            } else {
                                                                $paiements = [];
                                                            }
                                                        @endphp
                                                        @if ($nopaiements > 0)
                                                            @foreach ($paiements as $key => $item)
                                                                <div class="repeat_cash">
                                                                    @if ($item->type == 'cash')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingOne{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseOne{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseOne{{ $key }}"
                                                                                        value="Espéces {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseOne{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingOne{{ $key }}"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label
                                                                                        class="form-label">Devise</label>
                                                                                    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'paiements[cash][' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
                                                                                    @error('currency_id')
                                                                                        <div class="invalid-feedback">
                                                                                            {{ $message }}
                                                                                        </div>
                                                                                    @enderror
                                                                                    <label class="form-label">Montant
                                                                                        payé</label>
                                                                                    <input type="text"
                                                                                        class="form-control amount"
                                                                                        name="paiements[cash][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        step="0.001"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control "
                                                                                        name="paiements[cash][{{ $key }}][type]"
                                                                                        placeholder="Montant payé"
                                                                                        value="cash">
                                                                                    <input
                                                                                        name="paiements[cash][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_check repeat">
                                                                    @if ($item->type == 'check')
                                                                        <div class="accordion-item">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingTwo{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseTwo{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseTwo{{ $key }}"
                                                                                        value="Chèque {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseTwo{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingTwo"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control amount amount"
                                                                                        name="paiements[check][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        step="0.001"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <label class="form-label">Numéro du
                                                                                        chèque</label>
                                                                                    <input type="number"
                                                                                        class="form-control"
                                                                                        name="paiements[check][{{ $key }}][numbre]"
                                                                                        placeholder="Numero du chèque"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Banque</label>
                                                                                    {{ Form::select('paiements[check]['. $key .'][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', $item->bank), ['id' => 'paiements[check][' . $key . '][bank]', 'name' => 'paiements[check][' . $key . '][bank]', 'class' => $errors->has('paiements.check.' . $key . '.bank') ? 'form-control form-select is-invalid' : 'form-control form-select' , 'required']) }}
                                                                                    <label class="form-label">Date de
                                                                                        réglement:</label>
                                                                                    <input
                                                                                        name="paiements[check][{{ $key }}][received_at]"
                                                                                        type="date"
                                                                                        class="form-control"
                                                                                        aria-label="Text input with checkbox"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control"
                                                                                        name="paiements[check][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="check">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_transfer">
                                                                    @if ($item->type == 'transfer')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingThree{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseThree{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseThree{{ $key }}"
                                                                                        value="Virement {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseThree{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingThree"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control amount "
                                                                                        name="paiements[transfer][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        step="0.001"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input
                                                                                        name="paiements[transfer][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">

                                                                                    <label class="form-label">Numero du
                                                                                        Virement</label>
                                                                                    <input type="number"
                                                                                        class="form-control"
                                                                                        name="paiements[transfer][{{ $key }}][numbre]"
                                                                                        placeholder="Numero du Virement"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Banque</label>
                                                                                    {{ Form::select('paiements[transfer]['. $key .'][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', $item->bank), ['id' => 'paiements[transfer][' . $key . '][bank]', 'name' => 'paiements[transfer][' . $key . '][bank]', 'class' => $errors->has('paiements.transfer.' . $key . '.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
                                                                                    <label class="form-label">Date du
                                                                                        Virement</label>
                                                                                    <input type="date"
                                                                                        class="form-control"
                                                                                        name="paiements[transfer][{{ $key }}][received_at]"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control"
                                                                                        name="paiements[transfer][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="transfer">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_exchange">
                                                                    @if ($item->type == 'exchange')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingFour{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseFour{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseFour{{ $key }}"
                                                                                        value="Traite {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseFour{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingFour"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control amount"
                                                                                        name="paiements[exchange][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        step="0.001"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input
                                                                                        name="paiements[exchange][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">
                                                                                    <label class="form-label">Numero de
                                                                                        Traite</label>
                                                                                    <input type="number"
                                                                                        class="form-control"
                                                                                        name="paiements[exchange][{{ $key }}][numbre]"
                                                                                        placeholder="Numero de la Lettre"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Date de
                                                                                        Traite</label>
                                                                                    <input type="date"
                                                                                        class="form-control"
                                                                                        name="paiements[exchange][{{ $key }}][received_at]"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control"
                                                                                        name="paiements[exchange][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="exchange">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="repeat_cash"></div>
                                                            <div class="repeat_check repeat"></div>
                                                            <div class="repeat_transfer"></div>
                                                            <div class="repeat_exchange"></div>
                                                            <div class="repeat_ra"></div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms-auto">
                                    <button type="submit" name="save" id="save"
                                        class="btn btn-sm btn-primary">Enregistrer</button>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/providercommitment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/template" id="itemsList">
        <div class="card bg-light row pt-2 mx-1">
            <div>
            <button type="button" class="btn-close" id="deleteItem"></button>
            </div>
            <div  class="card-body ">
                <h6 class="text-info">Echéance {?}:</h6>
                <div class="row">
                    <div class="col-lg-5">
                        <div>
                            <br>
                            <h6>Montant:</h6>
                        </div>
                    </div>
                    <div class=" col-lg-7 ">
                        <div class="input-group mb-3">
                            <input id="Deadline.{?}.amount" name="Deadline[{?}][amount]" type="number" class="form-control Deadline" aria-label="Text input with checkbox">
                            <div class=" col-4">
                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-lg-5">
                        <div>
                            <br>
                            <h6>Date d'écheance:</h6>
                        </div>
                    </div>
                    <div class=" col-lg-7 ">
                            <input id="Deadline.{?}.date" min="{{ date('Y-m-d'); }}" name="Deadline[{?}][date]" type="date" class="form-control" aria-label="Text input with checkbox">

                        </div>
                 </div>
            </div>
        </div>

</script>
<script type="text/template" id="cashdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingOne{?}">
    <div class="row">
    <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteCash"></button>
    <input class="accordion-button collapsed col" type="button"
    data-bs-toggle="collapse"
    data-bs-target="#flush-collapseOne{?}"
    aria-expanded="true"
    aria-controls="flush-collapseOne{?}" value="Espéces {?}">
    </div>
    </h2>
    <div id="flush-collapseOne{?}"
    class="accordion-collapse collapse show"
    aria-labelledby="flush-headingOne{?}"
    data-bs-parent="#accordionFlushExample">
    <div class="accordion-body">
    <label class="form-label">Devise</label>
    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
    @error('currency_id')
    <div class="invalid-feedback">
    {{ $message }}
    </div>
    @enderror
    <label class="form-label ">Montant Payé</label>
    <input type="number" class="form-control amount"
    name="paiements[cash][{?}][amount]" step="0.001"
    placeholder="Montant Payé" required>
    <label class="form-label ">Date</label>
    <input type="date"
        class="form-control"
        name="paiements[cash][{?}][received_at]"
        placeholder="Date"
        value="{{ date('Y-m-d') }}">
    <input type="hidden" class="form-control" name="paiements[cash][{?}][due_at]" value="{{date('Y-m-d')}}">
    
    <input type="hidden" class="form-control "
    name="paiements[cash][{?}][type]"
    placeholder="Montant Payé" value="cash">
    <input name="paiements[cash][{?}][provider_id]" type="hidden"  value="1">
    </div>
    </div>
    </div>
    </script>
        <script type="text/template" id="checkdetail">
    <div class="accordion-item ">
    
    <h2 class="accordion-header" id="flush-headingTwo{?}">
    <div class="row">
    <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCheck"></button>
    <input class="accordion-button collapsed col" type="button"
    data-bs-toggle="collapse"
    data-bs-target="#flush-collapseTwo{?}"
    aria-expanded="true"
    aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
    </div>
    </h2>
    <div id="flush-collapseTwo{?}"
    class="accordion-collapse collapse show"
    aria-labelledby="flush-headingTwo{?}"
    data-bs-parent="#accordionFlushExample">
    <div class="accordion-body">
    <label class="form-label">Montant Payé</label>
    <input type="number" class="form-control amount"
    name="paiements[check][{?}][amount]" step="0.001"
    placeholder="Montant Payé" required>
    <input name="paiements[check][{?}][provider_id]" type="hidden"  value="1">
    
    <label class="form-label">Numero du chèque</label>
    <input type="number" class="form-control"
    name="paiements[check][{?}][numbre]"
    placeholder="Numero du chèque" required>
    <label class="form-label">Banque</label>
    {{ Form::select('paiements[check][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[check][{?}][bank]', 'name' => 'paiements[check][{?}][bank]', 'class' => $errors->has('paiements.check.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
    <label class="form-label">Date de réglement:</label>
    <input name="paiements[check][{?}][due_at]" type="date"
    class="form-control"
    aria-label="Text input with checkbox" required>
    <label class="form-label ">Date</label>
    <input type="date"
        class="form-control"
        name="paiements[check][{?}][received_at]"
        placeholder="Date"
        value="{{ date('Y-m-d') }}">
    <input type="hidden" class="form-control"
    name="paiements[check][{?}][type]"
    placeholder="Montant Payé" value="check">
    </div>
    </div>
    </div>
    </script>
        <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
    <div class="row">
    <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteTransfer"></button>
    <input class="accordion-button collapsed col" type="button"
    data-bs-toggle="collapse"
    data-bs-target="#flush-collapseThree{?}"
    aria-expanded="true"
    aria-controls="flush-collapseThree{?}" value="Virement {?}">
    
    </div>
    </h2>
    <div id="flush-collapseThree{?}"
    class="accordion-collapse collapse show"
    aria-labelledby="flush-headingThree{?}"
    data-bs-parent="#accordionFlushExample">
    <div class="accordion-body">
    <label class="form-label">Montant Payé</label>
    <input type="number" class="form-control amount "
    name="paiements[transfer][{?}][amount]" step="0.001"
    placeholder="Montant Payé" required>
    <input name="paiements[transfer][{?}][provider_id]" type="hidden"  value="1">
    
    <label class="form-label">Numero du Virement</label>
    <input type="number" class="form-control"
    name="paiements[transfer][{?}][numbre]"
    placeholder="Numero du Virement" required>
    <label class="form-label">Banque</label>
    {{ Form::select('paiements[transfer][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[transfer][{?}][bank]', 'name' => 'paiements[transfer][{?}][bank]', 'class' => $errors->has('paiements.transfer.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
    <label class="form-label">Date du réglement</label>
    <input type="date" class="form-control"
        name="paiements[transfer][{?}][due_at]">
        <input type="hidden" class="form-control"
        name="paiements[transfer][{?}][type]"
        placeholder="Montant Payé" value="transfer">
        <label class="form-label ">Date</label>
        <input type="date"
            class="form-control"
            name="paiements[transfer][{?}][received_at]"
            placeholder="Date"
            value="{{ date('Y-m-d') }}">
    <input type="hidden" class="form-control"
    name="paiements[transfer][{?}][type]"
    placeholder="Montant Payé" value="transfer">
    </div>
    </div>
    </div>
    </script>
        <script type="text/template" id="exchangedetail">
        <div class="accordion-item ">
    
        <h2 class="accordion-header" id="flush-headingFour{?}">
            <div class="row">
                <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteExchange"></button>
            <input class="accordion-button collapsed col" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseFour{?}"
                aria-expanded="true"
                aria-controls="flush-collapseFour{?}" value="Traite {?}">
                    </div>
        </h2>
        <div id="flush-collapseFour{?}"
            class="accordion-collapse collapse show"
            aria-labelledby="flush-headingFour{?}"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Montant Payé</label>
                <input type="number" class="form-control" step="0.001"
                    name="paiements[exchange][{?}][amount]"
                    placeholder="Montant Payé">
                    <input name="paiements[exchange][{?}][provider_id]" type="hidden"  value="1">
                <label class="form-label">Numero de Traite</label>
                <input type="number" class="form-control"
                    name="paiements[exchange][{?}][numbre]"
                    placeholder="Numero de traite">
                    <label class="form-label">Date de traite</label>
                    <input type="date" class="form-control"
                        name="paiements[exchange][{?}][due_at]">
                        <label class="form-label ">Date</label>
                        <input type="date"
                            class="form-control"
                            name="paiements[exchange][{?}][received_at]"
                            placeholder="Date"
                            value="{{ date('Y-m-d') }}">
                    <input type="hidden" class="form-control"
                    name="paiements[exchange][{?}][type]"
                    placeholder="Montant Payé" value="exchange">
            </div>
        </div>
    </div>
    </script>

    <script>
        function getDate(added_months) {
            var date = new Date();
            var newDate = new Date(date.setMonth(date.getMonth() + added_months));
            var currentDate = newDate.toISOString().substring(0, 10);
            return currentDate;
        }

        function divideCommitments(total_items) {
            var total = $('.total').val();
            for (let index = 1; index <= total_items; index++) {
                let amount = document.getElementById('Deadline.' + index + '.amount');
                let date = document.getElementById('Deadline.' + index + '.date');
                date.value = getDate(index - 1);
                if (index == total_items) {
                    amount.value = (Math.floor(total / total_items) + total % total_items).toFixed(3);
                } else {
                    amount.value = (Math.floor(total / total_items)).toFixed(3);
                }
            }
        }

        function initAmount(method, item) {
            var input = item.find(".amount");
            input.focus();
            var total = $('.total').val();
            var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
            var amounts_count = $('.amount').length;
            if (amounts_count <= 1) {
                first_amount.val(total);
            }
        }
    </script>

    <script>
        $(function() {
            $(".repeat_items").repeatable1({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 2,
                template: "#itemsList",
                itemContainer: ".row",
                afterAdd: function(item) {
                    var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                    divideCommitments(totalItems);
                },
                afterDelete: function() {
                    var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                    divideCommitments(totalItems);
                },
                total: {{ $noattributes }},
            });
        })
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('check', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('cash', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('exchange', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('transfer', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
                total: {{ $noattributes }}
            });
        })
    </script>
  
@endsection
