@extends('admin.layouts.app')

@section('title', 'Paiement')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Décaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.providercommitment.index')}}">Liste des paiements</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Détails échéance</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="row g-3 align-items-center">
                    <div class="col-12 col-lg-4 col-md-6 me-auto">
                        <h5 class="mb-0">BL: {{ $voucher->codification }}</h5>
                        <p class="mb-1">{{ $voucher->date }}</p>
                    </div>
                    <div class="col">
                        <div class="card radius-10 border-0 border-start border-warning border-3">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="">
                                        <p class="mb-1">Total </p>
                                        <h4 class="mb-0 text-warning">{{ $voucher->total }}</h4>
                                    </div>
                                    <div class="ms-auto widget-icon bg-warning text-white">
                                        <i class="bi bi-currency-dollar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                        // $total = App\Models\PaymentVoucher::where('financial_commitment_id', $commitment->id)->sum('amount');
                        $total = $voucher
                            ->commitments()
                            ->with('paiements')
                            ->get()
                            ->pluck('paiements')
                            ->collapse()
                            ->sum('amount');
                    @endphp
                    <div class="col">
                        <div class="card radius-10 border-0 border-start border-success border-3">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="">
                                        <p class="mb-1">Montant payé</p>
                                        <h4 class="mb-0 text-success">{{ number_format($total, 3) }}</h4>
                                    </div>
                                    <div class="ms-auto widget-icon bg-success text-white">
                                        <i class="bi bi-currency-dollar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-primary bpayment-0">
                                        <i class="bi bi-person text-primary"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Provider</h6>
                                        <p class="mb-1">{{ $voucher->provider_details['name'] }}</p>
                                        <p class="mb-1">{{ $voucher->provider_details['adresse'] }}</p>
                                        <p class="mb-1">{{ $voucher->provider_details['phone'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-success bpayment-0">
                                        <i class="bi bi-truck text-success"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Info de Paiement </h6>
                                        <p class="mb-1"><strong>Expédition</strong> : {{config('stock.info.name')}} </p>
                                        <p class="mb-1"><strong>Statut</strong>
                                            @php
                                                if ($voucher->paiment_status == 0) {
                                                    $status = '<td><span class="badge rounded-pill alert-danger">Non Payé</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-danger bpayment-0">
                                        <i class="bi bi-geo-alt text-danger"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Livrer à</h6>
                                        <p class="mb-1"><strong>Address</strong> :
                                            {{ $voucher->provider_details['adresse'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <div class="card-header py-3">
                    <div class="row align-items-center g-3">
                      <div class="col-12 col-lg-6">
                        <h5 class="mb-0"></h5>
                      </div>
                      <div class="col-12 col-lg-6 text-md-end">
                        @if ($voucher->paiment_status == 0)
                            @canany(['Modifier encaissement'])
                                <button id="add_payment" type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#paymentmodal">Ajouter paiement</button>
                            @endcanany
                            <div class="modal fade" id="paymentmodal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Détails paiement</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Fermer"></button>
                                        </div>
                                        @php
                                            $selected_commitment = $voucher->commitments->where('paiment_status', 0)->first();
                                        @endphp
                                        {!! Form::open([
                                            'enctype' => 'multipart/form-data',
                                            'route' => [
                                                'admin.providercommitment.update',
                                                ['commitment' => $selected_commitment->id, 'type' => strtolower(class_basename($selected_commitment))],
                                            ],
                                            'method' => 'POST',
                                            'id' => 'update-commitement-form',
                                        ]) !!}
                                        <div class="modal-body">
                                            @php
                                                $unpaid_commitments = $voucher->commitments->where('paiment_status', 0);
                                            @endphp
                                            @if ($unpaid_commitments)
                                                <div class="col mb-2">
                                                    <select class="form-select form-select-sm" name="commitment_id" id="commitment_id">
                                                        @foreach ($unpaid_commitments as $k => $item)
                                                            <option value="{{$item->id}}">Echéance {{$k + 1}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif

                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-1">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-success px-2 rounded-pill" id="addCash">
                                                            <i class="bi bi-currency-dollar"></i> Espèce</button></div>

                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-2 rounded-pill" id="addCheck">
                                                            <i class="bi bi-card-heading"></i> Chèque</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-secondary px-2 rounded-pill"
                                                            id="addTransfer">
                                                            <i class="bi bi-bank2"></i> Virement</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-danger px-2 rounded-pill"
                                                            id="addExchange">
                                                            <i class="bi bi-file-earmark-text"></i> Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        <div class="repeat_cash"></div>
                                                        <div class="repeat_check repeat"></div>
                                                        <div class="repeat_transfer"></div>
                                                        <div class="repeat_exchange"></div>
                                                        <div class="repeat_rs"></div>
                                                        <div class="repeat_ra"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-secondary"
                                                data-bs-dismiss="modal">Fermer</button>
                                            <button id="save" type="submit"
                                                class="btn btn-sm btn-primary">Payer</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                      </div>
                    </div>
               </div>

                @foreach ($voucher->commitments as $key => $commitment)
                <div class="card border shadow-none">
                    <div class="card-header py-3">
                         <div class="row align-items-center g-3">
                           <div class="col-12 col-lg-6">
                             <h5 class="mb-0">Echéance {{$key + 1}}</h5>
                           </div>
                         </div>
                    </div>
                    <div class="card-header py-2 bg-light">
                      <div class="row row-cols-1 row-cols-lg-2">
                        <div class="col">
                         <div class="">
                           <address class="m-t-5 m-b-5">
                                Montant: <strong class="text-inverse text-warning">{{ $commitment->amount }}</strong><br>
                                Date: <strong class="text-inverse">{{ $commitment->date }}</strong><br>
                           </address>
                          </div>
                        </div>
                        <div class="col">
                         <div class="">
                           <address class="m-t-5 m-b-5">
                                Montant payé: <strong class="text-inverse text-success">{{ number_format($commitment->paiements()->sum('amount'), 3) }}</strong><br>
                                Etat: <span class="badge rounded-pill alert-{{$commitment->paiment_status ? 'success' : 'danger'}}">{{$commitment->paiment_status ? '' : 'Non '}}Payé</span><br>
                           </address>
                          </div>
                       </div>
                      </div>
                    </div>
                   <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle mb-0">
                            <thead class="table-light">
                                <tr>
                                    {{-- <th>ID de Paiement</th> --}}
                                    <th>Montant</th>
                                    <th>Méthode</th>
                                    <th>Date de réception</th>
                                    <th>Date de création</th>
                                    <th>Date de réglement</th>
                                    <th>Ajouté par</th> 
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($commitment->paiements as $paiement)
                                    <tr>
                                        <td>{{ $paiement->amount }}</td>
                                        @php
                                            $payment_type = '';
                                            switch ($paiement->type) {
                                                case 'cash':
                                                    $payment_type = '<span class="badge  rounded-pill alert-success">Espèce</span>';
                                                    break;
                                                case 'check':
                                                    $payment_type = '<span class="badge  rounded-pill alert-primary">Chèque</span>';
                                                    break;
                                                case 'transfer':
                                                    $payment_type = '<span class="badge  rounded-pill alert-info">Virement</span>';
                                                    break;
                                                case 'exchange':
                                                    $payment_type = '<span class="badge  rounded-pill alert-danger">Traite</span>';
                                                    break;
                                            }
                                        @endphp 
                                        <td>{!! $payment_type !!}</td>
                                        <td>{{ $paiement->received_at }}</td>
                                        <td>{{ $paiement->created_at }}</td>
                                        <td>{{ $paiement->due_at }}</td>
                                        <td>{{ $paiement->created_by }}</td>
                                        <td>
                                            @canany(['Supprimer encaissement'])
                                                <form method="POST"
                                                    action="{{ route('admin.providercommitment.payment.delete', ['payment' => $paiement->id, 'type' => request()->get('type')]) }}">
                                                    {{ csrf_field() }}
                                                    <input name="_method" type="hidden" value="GET">
                                                    <button id="show_confirm" type="submit"
                                                        class="btn btn-sm text-danger show_confirm"
                                                        data-toggle="tooltip" title="Supprimer"><i
                                                            class="bi bi-trash-fill"></i></button>
                                                </form>
                                            @endcanany
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                   </div>
                  </div>
                  @endforeach

                <!--end row-->

            </div>
        </div>

    </main>
    <!--end page main-->
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    
    <script type="text/template" id="cashdetail">
        <div class="accordion-item ">
        <h2 class="accordion-header" id="flush-headingOne{?}">
            <div class="row">
          <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCash"></button>
            <input class="accordion-button collapsed col" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne{?}"
                aria-expanded="true"
                aria-controls="flush-collapseOne{?}" value="Espéces {?}">
                    </div>
        </h2>
        <div id="flush-collapseOne{?}"
            class="accordion-collapse collapse show"
            aria-labelledby="flush-headingOne{?}"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Curency</label>
                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
                @error('currency_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
                <label class="form-label ">Montant Payé</label>
                <input type="number" class="form-control amount"
                    name="paiements[cash][{?}][amount]"
                    placeholder="Montant Payé" step="0.001" required>
                    <label class="form-label ">Date</label>
                    <input type="date"
                        class="form-control"
                        name="paiements[cash][{?}][received_at]"
                        placeholder="Date"
                        value="{{ date('Y-m-d') }}">
                    <input type="hidden" class="form-control" name="paiements[cash][{?}][due_at]" value="{{date('Y-m-d')}}">
                    <input type="hidden" class="form-control "
                    name="paiements[cash][{?}][type]"
                    placeholder="Montant Payé" value="cash">
                    <input name="paiements[cash][{?}][provider_id]" type="hidden"  value="1">
            </div>
        </div>
    </div>
    </script>
        <script type="text/template" id="checkdetail">
        <div class="accordion-item ">
    
        <h2 class="accordion-header" id="flush-headingTwo{?}">
            <div class="row">
                <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCheck"></button>
            <input class="accordion-button collapsed col" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseTwo{?}"
                aria-expanded="true"
                aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
                    </div>
        </h2>
        <div id="flush-collapseTwo{?}"
            class="accordion-collapse collapse show"
            aria-labelledby="flush-headingTwo{?}"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Montant Payé</label>
                <input type="number" class="form-control amount"
                    name="paiements[check][{?}][amount]"
                    placeholder="Montant Payé" required>
                    <input name="paiements[check][{?}][provider_id]" type="hidden"  value="1">
    
                <label class="form-label">Numéro du chèque</label>
                <input type="number" class="form-control"
                    name="paiements[check][{?}][numbre]"
                    placeholder="Numero du chèque" required>
                <label class="form-label">Bank</label>
                {{ Form::select('paiements[check][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[check][{?}][bank]', 'name' => 'paiements[check][{?}][bank]', 'class' => $errors->has('paiements.check.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
                
                <label class="form-label">Date de réglement:</label>
                <input name="paiements[check][{?}][due_at]" type="date" value="{{date('Y-m-d')}}"
                    class="form-control"
                    aria-label="Text input with checkbox" required>
                    <label class="form-label ">Date</label>
                    <input type="date"
                        class="form-control"
                        name="paiements[check][{?}][received_at]"
                        placeholder="Date"
                        value="{{ date('Y-m-d') }}">
                    <input type="hidden" class="form-control"
                    name="paiements[check][{?}][type]"
                    placeholder="Montant Payé" value="check">
            </div>
        </div>
    </div>
    </script>
        <script type="text/template" id="exchangedetail">
        <div class="accordion-item ">
    
        <h2 class="accordion-header" id="flush-headingFour{?}">
            <div class="row">
                <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteExchange"></button>
            <input class="accordion-button collapsed col" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseFour{?}"
                aria-expanded="true"
                aria-controls="flush-collapseFour{?}" value="Traite {?}">
                    </div>
        </h2>
        <div id="flush-collapseFour{?}"
            class="accordion-collapse collapse show"
            aria-labelledby="flush-headingFour{?}"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Montant payé</label>
                <input type="number" class="form-control amount"
                    name="paiements[exchange][{?}][amount]"
                    placeholder="Montant Payé" required>
                    <input name="paiements[exchange][{?}][provider_id]" type="hidden"  value="1">
                <label class="form-label">Numéro de traite</label>
                <input type="number" class="form-control"
                    name="paiements[exchange][{?}][numbre]"
                    placeholder="Numero de traite" required>
                <label class="form-label">Date de réglement</label>
                <input type="date" class="form-control" value="{{date('Y-m-d')}}"
                    name="paiements[exchange][{?}][due_at]" required>
                    <label class="form-label ">Date</label>
                    <input type="date"
                        class="form-control"
                        name="paiements[exchange][{?}][received_at]"
                        placeholder="Date"
                        value="{{ date('Y-m-d') }}">
                    <input type="hidden" class="form-control"
                    name="paiements[exchange][{?}][type]"
                    placeholder="Montant Payé" value="exchange" >
            </div>
        </div>
    </div>
    </script>
        <script type="text/template" id="transferdetail">
        <div class="accordion-item ">
        <h2 class="accordion-header" id="flush-headingThree{?}">
            <div class="row">
                <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteTransfer"></button>
            <input class="accordion-button collapsed col" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseThree{?}"
                aria-expanded="true"
                aria-controls="flush-collapseThree{?}" value="Virement {?}">
    
        </div>
        </h2>
        <div id="flush-collapseThree{?}"
            class="accordion-collapse collapse show"
            aria-labelledby="flush-headingThree{?}"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Montant payé</label>
                <input type="number" class="form-control amount"
                    name="paiements[transfer][{?}][amount]"
                    placeholder="Montant payé" required>
                    <input name="paiements[transfer][{?}][provider_id]" type="hidden"  value="1">
    
                <label class="form-label">Numéro du virement</label>
                <input type="number" class="form-control"
                    name="paiements[transfer][{?}][numbre]"
                    placeholder="Numero du Virement" required>
                    <label class="form-label">Banque</label>
                    {{ Form::select('paiements[transfer][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[transfer][{?}][bank]', 'name' => 'paiements[transfer][{?}][bank]', 'class' => $errors->has('paiements.transfer.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
                    
                <label class="form-label">Date de réglement</label>
                <input type="date" class="form-control" value="{{date('Y-m-d')}}"
                    name="paiements[transfer][{?}][due_at]" required>
                    <label class="form-label ">Date</label>
                    <input type="date"
                        class="form-control"
                        name="paiements[transfer][{?}][received_at]"
                        placeholder="Date"
                        value="{{ date('Y-m-d') }}">
                    <input type="hidden" class="form-control"
                    name="paiements[transfer][{?}][type]"
                    placeholder="Montant Payé" value="transfer">
            </div>
        </div>
    </div>
    </script>

    <script>
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                // total:
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
    </script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
