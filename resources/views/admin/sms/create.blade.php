@extends('admin.layouts.app')

@section('title', 'Envoyer SMS')

@section('stylesheets')
    {{-- <link href="https://unpkg.com/bootstrap@3.3.2/dist/css/bootstrap.min.css" rel="stylesheet" />--}}
    {{-- <link href="https://unpkg.com/bootstrap-multiselect@0.9.13/dist/css/bootstrap-multiselect.css" rel="stylesheet" />  --}}
    <link href="{{ asset('assets/admin/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" /> 
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">

        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">SMS</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i> Tableau de
                                bord</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Envoyer SMS</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row">
            <div class="col-12 col-lg-8  mx-auto">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        {!! Form::open([
                            'enctype' => 'multipart/form-data',
                            'route' => 'sms.store',
                            'method' => 'POST',
                            'id' => 'send-sms-form',
                        ]) !!}
                        {{-- <h5 class="mb-0">Mon compte</h5>
              <hr> --}}
                        <div class="card shadow-none border">
                            <div class="card-header">
                                <h6 class="mb-0">Envoyer SMS</h6>
                            </div>
                            <div class="card-body">
                                <div class="row g-3">
                                    <div class="col-12 col-lg-12 required">
                                        <label class="form-label">Clients</label>
                                        {{-- {!! Form::select('clients[]', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Client(), 'name') ,'', ['id' => 'clients', 'class' => $errors->has('clients') ? 'form-control is-invalid multiple' : 'form-control multiple', 'multiple' => 'multiple']) !!} --}}
                                        <select multiple name="clients[]" id="clients" class="form-select">
                                            @foreach ($clients as $client)
                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('clients')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-lg-12 required">
                                        <label class="form-label">Message</label>
                                        <textarea maxlength="160" name="message" id="message" rows="4"
                                            class="form-control @error('message') is-invalid @enderror" placeholder="Message (160)">{{ old('message', '') }}</textarea>
                                        @error('message')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <span id="sms-length"></span>/160
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-start">
                            <button type="submit" class="btn btn-sm btn-primary px-4"><i
                                    class="bi bi-send-fill"
                                    onclick="if(this.form.checkValidity()) {
                                        this.disabled = true;
                                        this.value = 'Envoi en cours...';
                                        this.form.submit();
                                    }"></i>Envoyer</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <div class="col">
            <!-- Button trigger modal -->
            {{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">Scrollable Modal</button> --}}
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Résultat d'envoi</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            @if (Session::has('result'))
                                @foreach (Session::get('result') as $item)
                                    <p><span class="badge rounded-pill alert-{{$item['status_code'] == '200' ? 'success' : 'danger'}}">{{$item['client_name'] . '(' . $item['client_phone'] . ') : ' . $item['status_msg']}}</span></p>
                                @endforeach
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!--end page main-->
@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>

    {{-- <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="https://unpkg.com/bootstrap@3.3.2/dist/js/bootstrap.min.js"></script> --}}
    {{-- <script src="https://unpkg.com/bootstrap-multiselect@0.9.13/dist/js/bootstrap-multiselect.js"></script> --}}


    <script type="text/javascript">
        $(document).ready(function() {
            $('#clients').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Tout sélectionner',
                selectAllValue: 'multiselect-all',
                selectAllName: false,
                selectAllNumber: true,
                selectAllJustVisible: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                enableFullValueFiltering: false,
                filterPlaceholder: 'Chercher',
                nonSelectedText: 'Sélectionner les clients',
                nSelectedText: 'sélectionnés',
                allSelectedText: 'Tous sélectionnés',
                numberDisplayed: 4,
            });
            $("#sms-length").html($('#message').val().length);
        });
    </script>

    <script>
        $(document).on('keyup', '#message', function() {
            console.log($(this).val().length);
            $("#sms-length").html($(this).val().length);
        });
    </script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        </script>
    @endif

    @if (Session::has('result'))
        <script>
            var myModal = new bootstrap.Modal(document.getElementById("myModal"), {});
            // document.onreadystatechange = function () {
            myModal.show();
            // };

            $("#myModal").on("hidden.bs.modal", function () {
                console.log('test');
                window.location.reload();
            });
            
        </script>
    @endif
@endsection
