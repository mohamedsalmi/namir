@extends('admin.layouts.app')

@section('title', 'Modifier ' . $title . $cart->codification)

@section('stylesheets')
<link href="{{ url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css') }} " rel="stylesheet">
<link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css') }} " rel="stylesheet">
<link href="{{ url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.css"
        integrity="sha512-2sFkW9HTkUJVIu0jTS8AUEsTk8gFAFrPmtAxyzIhbeXHRH8NXhBFnLAMLQpuhHF/dL5+sYoNHWYYX2Hlk+BVHQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .ms-container {
            width: 100%;
        }
        .select2-selection--single {
            height: 31px !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px;
            margin-left: 0px;
            display: inline;
            border: 0px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 0px;
            border-style: none;
        }

        .page-link {
            display: inline;
        }
    </style>
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}}</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('admin.cart.index')}}">Liste BL</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier {{ $title }} </li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['admin.'.$type . '.update', $cart->id],
            'method' => 'POST',
            'id' => 'create-cart-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Modifier {{ $title }} {{$cart->codification}}</h5>
                            <div class="ms-auto">
                                {{-- <button type="submit" name="save_draft" class="btn btn-secondary">Save to Draft</button> --}}
                                <button type="submit" name="save" id="save"
                                    class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{$type}}" id='document'>

                    <div class="card-body">
                        <div class="row g-3">

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                            <div class="col-12 col-lg-4">
                                                {{-- @if ($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}
                                                <label class="form-label">Liste des clients :</label>
                                                <select class="single-select" id="fidel_list" name="client_id">
                                                    <option value="0" class="text-primary" tel-fidel="11111111"
                                                        AD-fidel="adresse" MF-fidel='matricule' name-fidel="Passager"
                                                        Prix-fidel="detail" Remise-fidel="0">Client passager</option>
                                                    @foreach ($clients as $client)
                                                        <option
                                                            {{ $cart->client()->exists() && $client->id == $cart->client_id ? 'selected' : '' }}
                                                            value="{{ $client->id }}" name-fidel="{{ $client->name }}"
                                                            tel-fidel="{{ $client->phone }}" MF-fidel="{{ $client->mf }}"
                                                            Remise-fidel="{{ $client->discount }}"
                                                            AD-fidel="{{ $client->adresse }}"
                                                            Prix-fidel="{{ $client->price }}">{{ $client->name }} -
                                                            {{ $client->client_code }} - {{ $client->phone }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm "
                                                    name="client_details[name]" placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('name', $cart->client_details['name'], '') }}">
                                                @error('client_details.name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm " name="client_details[adresse]"
                                                    placeholder="Adresse" id="adresse"
                                                    value="{{ old('adresse', $cart->client_details['adresse'] ?? '', '') }}">
                                                @error('client_details.adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm MF" name="client_details[mf]"
                                                    placeholder="MF" id="marticule"
                                                    value="{{ old('mf', $cart->client_details['mf'] ?? '', '') }}">
                                                @error('client_details.mf')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile "
                                                    name="client_details[phone]" placeholder="tel" id="tel"
                                                    value="{{ old('phone', $cart->client_details['phone'], '') }}">
                                                @error('client_details.phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Type de Prix
                                                    :</label>
                                                <select class="form-select form-select-sm @error('Prix_T') is-invalid @enderror"
                                                    id="Prix_T" name="Prix_T">
                                                    <option
                                                        {{ $cart->client()->exists() && $cart->client->price == 'detail' ? 'selected' : 'selected' }}
                                                        value="detail">Detail</option>
                                                    <option
                                                        {{ $cart->client()->exists() && $cart->client->price == 'semigros' ? 'selected' : '' }}
                                                        value="semigros">Semi-Gros</option>
                                                    <option
                                                        {{ $cart->client()->exists() && $cart->client->price == 'gros' ? 'selected' : '' }}
                                                        value="gros">Gros</option>
                                                </select>
                                                @error('Prix_T')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date"
                                                    class="form-control form-control-sm datetimepicker-input"
                                                    name="date"
                                                    value="{{ old('date', $cart->date ? $cart->date : date('Y-m-d')) }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" >Note:</label>
                                                <input type="text"
                                                    class="form-control form-control-sm "
                                                    name="note" id="note"
                                                    value="{{ old('note', $cart->note ? $cart->note : '') }}" />
                                                @error('note')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            @if ($type == 'onlinesale')
                                                <div class="form-group col-12 col-lg-4">
                                                    <label class="form-label" for="exampleFormControlInput1">
                                                        <!--<span-->
                                                        <!--    style="color: red;">*</span>-->
                                                            <i class="fa fa-calendar"></i>
                                                        Gouvernerat
                                                        :</label>
                                                    <select class="single-select" id="state"
                                                        name="client_details[state]">
                                                        <option value="">Choisir optioin</option>
                                                        @foreach ($states as $state)
                                                            <option
                                                                {{ $state->state == $cart->client_details['state'] ? 'selected' : '' }}
                                                                value="{{ $state->state }}">{{ $state->state }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-12 col-lg-4">
                                                    <label class="form-label" for="exampleFormControlInput1">
                                                        <!--<span-->
                                                        <!--    style="color: red;">*</span>-->
                                                            <i class="fa fa-calendar"></i>
                                                        Delegation
                                                        :</label>
                                                    <select class="single-select" id="city"
                                                        name="client_details[city]">
                                                        <option value="">Choisir option</option>
                                                        @foreach ($cities as $city)
                                                            <option
                                                                {{ $city->city == $cart->client_details['city'] ? 'selected' : '' }}
                                                                data-state="{{ $city->state }}"
                                                                value="{{ $city->city }}">{{ $city->city }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="col-12 col-lg-12">
                                            <div class="row g-3">
                                                <h6 class=" text-center text-info" id="paiements">Détails
                                                    de paiement
                                                </h6>

                                                <div class="col-4">
                                                    @php
                                                        $commitments = $cart->commitments;
                                                        $nocommitments = isset($commitments) ? count($commitments) : 0;
                                                        if (isset($commitments)) {
                                                            if (!is_array($commitments)) {
                                                                $nocommitments = count(json_decode($commitments, true));
                                                                $commitments = json_decode($commitments);
                                                            }
                                                        } else {
                                                            $commitments = [];
                                                        }
                                                    @endphp
                                                    <div class="form-check form-switch">
                                                        {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                        @if ($nocommitments > 1)
                                                            <input class="form-check-input"
                                                                name="Deadline_check" type="checkbox"
                                                                id="Deadline_check" checked>
                                                        @else
                                                            <input class="form-check-input"
                                                                name="Deadline_check" type="checkbox"
                                                                id="Deadline_check">
                                                        @endif
                                                        <label class="form-check-label"
                                                            for="Deadline">Echéance</label>
                                                    </div>
                                                    <button type="button" name="Add_paiement"
                                                            id="Add_paiement" class="btn btn-primary "
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#paiment_modal">+ Créer les écheances</button>
                                                </div>

                                                <div class="form-group col-2 col-lg-4">

                                                    <div class="form-check form-switch">
                                                        <input type='hidden' value='0' name='timbre'>
                                                        <input class="form-check-input" name="timbre"
                                                            type="checkbox" @if($cart->timbre) checked @endif value="1">
                                                        <label class="form-label"
                                                            for="exampleFormControlInput1"><i
                                                                class="fa fa-tag"></i> Timbre </label>
                                                    </div>
                            
                                                </div>

                                                <div class="modal fade" id="paiment_modal" tabindex="-1" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-scrollable modal-xl">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <div class="col-4">
                                                                    <h5 class="modal-title text-info">Liste
                                                                        des écheances
                                                                    </h5>
                                                                </div>
                                                                <h6 class="form-label m-3">Montant total à payer :</h6>
                                                                <input type="text" class="btn btn-warning px-5 radius-30 total" value="{{$cart->total}}" readonly>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="col-lg-12 mx-auto" id="Add_paiement">
                                                                    <div class="card">
                                                                        {{-- <div class="card-header py-3 bg-transparent">
                                                                            <div class="d-sm-flex align-items-center">
                                                                                <div class="col-4">
                                                                                    <h5 class="modal-title text-info">Liste des
                                                                                        écheances </h5>
                                                                                </div>
                                                                                <h6 class="form-label m-3">Montant total à
                                                                                    payer :</h6>
                                                                                <input type="text"
                                                                                    class="btn btn-sm btn-warning px-5 radius-30 total"
                                                                                    value="{{ old('total', $cart->total, '') }}"
                                                                                    readonly>
                                                                            </div>
                                                                        </div> --}}
                                                                        <div class="card-body">
                                            
                                                                            <div class="card-body">
                                                                                <div class="repeat_items">
                                                                                    @if ($nocommitments > 0)
                                                                                        @foreach ($commitments as $key => $item)
                                                                                            <div
                                                                                                class="card bg-light row pt-2 mx-1">
                                                                                                <div>
                                                                                                    <button type="button"
                                                                                                        class="btn-close"
                                                                                                        id="deleteItem"></button>
                                                                                                </div>
                                                                                                <div class="card-body ">
                                                                                                    <h6 class="text-info">
                                                                                                        Echéance
                                                                                                        {{ $key + 1 }}:
                                                                                                    </h6>
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-5">
                                                                                                            <div>
                                                                                                                <br>
                                                                                                                <h6>Montant:
                                                                                                                </h6>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class=" col-lg-7 ">
                                                                                                            <div
                                                                                                                class="input-group mb-3">
                                                                                                                <input
                                                                                                                    id="Deadline{{ $key }}.amount"
                                                                                                                    name="Deadline[{{ $key }}][amount]"
                                                                                                                    type="number"
                                                                                                                    class="form-control form-control-sm"
                                                                                                                    aria-label="Text input with checkbox"
                                                                                                                    value="{{ old('amount', $item->amount, '') }}">
                                                                                                                <div
                                                                                                                    class=" col-4">
                                                                                                                    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'Deadline[' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                                                </div>
                                                                                                            </div>
                                            
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-5">
                                                                                                            <div>
                                                                                                                <br>
                                                                                                                <h6>Date
                                                                                                                    d'écheance:
                                                                                                                </h6>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class=" col-lg-7 ">
                                                                                                            <input
                                                                                                                id="Deadline.{{ $key }}.date"
                                                                                                                name="Deadline[{{ $key }}][date]"
                                                                                                                type="date"
                                                                                                                class="form-control form-control-sm"
                                                                                                                aria-label="Text input with checkbox"
                                                                                                                value="{{ old('date', $item->date, '') }}">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </div>
                                            
                                                                                <div class="ms-auto">
                                                                                    <button role="button" type="button"
                                                                                        class="btn btn-sm btn-sm btn-warning"
                                                                                        id="addItem"><i
                                                                                            class="bi bi-plus"></i> Ajouter une
                                                                                        échéance</button>
                                                                                </div>
                                                                            </div>
                                            
                                                                            {{-- <div class="modal-footer">
                                                                    <button type="button" class="btn btn-sm btn-secondary"
                                                                        data-bs-dismiss="modal">Terminer</button>
                                                                </div> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                            
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Terminer</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12" id="cash">
                                                    <div class="row row-cols-auto g-1">
                                                        <div class="col"><button type="button"
                                                                class="btn btn-sm btn-success px-2 rounded-pill"
                                                                id="addCash">
                                                                <i class="bi bi-currency-dollar"></i>
                                                                Espèce</button></div>

                                                        <div class="col"><button type="button"
                                                                class="btn btn-sm btn-primary px-2 rounded-pill"
                                                                id="addCheck">
                                                                <i class="bi bi-card-heading"></i>
                                                                Chèque</button></div>
                                                        <div class="col pr-1"><button type="button"
                                                                class="btn btn-sm btn-secondary px-2 rounded-pill"
                                                                id="addTransfer">
                                                                <i class="bi bi-bank2"></i> Virement</button>
                                                        </div>
                                                        <div class="col pr-1"><button type="button"
                                                                class="btn btn-sm btn-danger px-2 rounded-pill"
                                                                id="addExchange">
                                                                <i class="bi bi-file-earmark-text"></i>
                                                                Traite</button></div>

                                                        {{-- <div class="col pr-1"><button type="button"
                                                    class="btn btn-sm btn-warning px-2 rounded-pill" id="addRA">
                                                    <i class="bi bi-arrow-return-left"></i> Retour</button></div> --}}
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="accordion accordion-flush "
                                                            id="accordionFlushExample">
                                                            @php
                                                                $paiements = $cart?->commitments?->first()?->paiements;
                                                                $nopaiements = is_array($paiements) ? count($paiements) : 0;
                                                                if (isset($paiements)) {
                                                                    if (!is_array($paiements)) {
                                                                        $nopaiements = count(json_decode($paiements, true));
                                                                        $paiements = json_decode($paiements);
                                                                    }
                                                                } else {
                                                                    $paiements = [];
                                                                }
                                                            @endphp
                                                            @if ($nopaiements > 0)
                                                                @foreach ($paiements as $key => $item)
                                                                    <div class="repeat_cash">
                                                                        @if ($item->type == 'cash')
                                                                            <div class="accordion-item ">
                                                                                <h2 class="accordion-header"
                                                                                    id="flush-headingOne{{ $key }}">
                                                                                    <div class="row">
                                                                                        <button type="button"
                                                                                            class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                            id="deleteCash"></button>
                                                                                        <input
                                                                                            class="accordion-button collapsed col"
                                                                                            type="button"
                                                                                            data-bs-toggle="collapse"
                                                                                            data-bs-target="#flush-collapseOne{{ $key }}"
                                                                                            aria-expanded="true"
                                                                                            aria-controls="flush-collapseOne{{ $key }}"
                                                                                            value="Espéces {{ $key + 1 }}">
                                                                                    </div>
                                                                                </h2>
                                                                                <div id="flush-collapseOne{{ $key }}"
                                                                                    class="accordion-collapse collapse show"
                                                                                    aria-labelledby="flush-headingOne{{ $key }}"
                                                                                    data-bs-parent="#accordionFlushExample">
                                                                                    <div
                                                                                        class="accordion-body">
                                                                                        <label
                                                                                            class="form-label">Curency</label>
                                                                                        {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'paiements[cash][' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                        @error('currency_id')
                                                                                            <div
                                                                                                class="invalid-feedback">
                                                                                                {{ $message }}
                                                                                            </div>
                                                                                        @enderror
                                                                                        <label
                                                                                            class="form-label">Montant
                                                                                            Payé</label>
                                                                                        <input type="text"
                                                                                            class="form-control form-control-sm amount"
                                                                                            name="paiements[cash][{{ $key }}][amount]"
                                                                                            placeholder="Montant Payé"
                                                                                            step="0.001"
                                                                                            value="{{ old('amount', $item->amount, '') }}">
                                                                                        <input type="hidden"
                                                                                            class="form-control form-control-sm "
                                                                                            name="paiements[cash][{{ $key }}][type]"
                                                                                            placeholder="Montant Payé"
                                                                                            value="cash">
                                                                                        <input
                                                                                            name="paiements[cash][{{ $key }}][client_id]"
                                                                                            type="hidden"
                                                                                            value="1">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="repeat_check repeat">
                                                                        @if ($item->type == 'check')
                                                                            <div class="accordion-item">
                                                                                <h2 class="accordion-header"
                                                                                    id="flush-headingTwo{{ $key }}">
                                                                                    <div class="row">
                                                                                        <button type="button"
                                                                                            class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                            id="deleteCash"></button>
                                                                                        <input
                                                                                            class="accordion-button collapsed col"
                                                                                            type="button"
                                                                                            data-bs-toggle="collapse"
                                                                                            data-bs-target="#flush-collapseTwo{{ $key }}"
                                                                                            aria-expanded="true"
                                                                                            aria-controls="flush-collapseTwo{{ $key }}"
                                                                                            value="Chèque {{ $key + 1 }}">
                                                                                    </div>
                                                                                </h2>
                                                                                <div id="flush-collapseTwo{{ $key }}"
                                                                                    class="accordion-collapse collapse show"
                                                                                    aria-labelledby="flush-headingTwo"
                                                                                    data-bs-parent="#accordionFlushExample">
                                                                                    <div
                                                                                        class="accordion-body">
                                                                                        <label
                                                                                            class="form-label">Montant
                                                                                            Payé</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm amount amount"
                                                                                            name="paiements[check][{{ $key }}][amount]"
                                                                                            placeholder="Montant Payé"
                                                                                            step="0.001"
                                                                                            value="{{ old('amount', $item->amount, '') }}">
                                                                                        <label
                                                                                            class="form-label">Numero
                                                                                            du
                                                                                            chèque</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[check][{{ $key }}][numbre]"
                                                                                            placeholder="Numéro du chèque"
                                                                                            value="{{ old('numbre', $item->numbre, '') }}">
                                                                                        <label
                                                                                            class="form-label">Banque</label>
                                                                                        {{ Form::select('paiements[check][' . $key . '][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', $item->bank), ['id' => 'paiements[check][' . $key . '][bank]', 'name' => 'paiements[check][' . $key . '][bank]', 'class' => $errors->has('paiements.check.' . $key . '.bank') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select', 'required']) }}
                                                                                        <label
                                                                                            class="form-label">Date
                                                                                            de
                                                                                            réglement:</label>
                                                                                        <input
                                                                                            name="paiements[check][{{ $key }}][received_at]"
                                                                                            type="date"
                                                                                            class="form-control form-control-sm"
                                                                                            aria-label="Text input with checkbox"
                                                                                            value="{{ old('received_at', $item->received_at, '') }}">
                                                                                        <input type="hidden"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[check][{{ $key }}][type]"
                                                                                            placeholder="Montant Payé"
                                                                                            value="check">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="repeat_transfer">
                                                                        @if ($item->type == 'transfer')
                                                                            <div class="accordion-item ">
                                                                                <h2 class="accordion-header"
                                                                                    id="flush-headingThree{{ $key }}">
                                                                                    <div class="row">
                                                                                        <button type="button"
                                                                                            class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                            id="deleteCash"></button>
                                                                                        <input
                                                                                            class="accordion-button collapsed col"
                                                                                            type="button"
                                                                                            data-bs-toggle="collapse"
                                                                                            data-bs-target="#flush-collapseThree{{ $key }}"
                                                                                            aria-expanded="true"
                                                                                            aria-controls="flush-collapseThree{{ $key }}"
                                                                                            value="Virement {{ $key + 1 }}">
                                                                                    </div>
                                                                                </h2>
                                                                                <div id="flush-collapseThree{{ $key }}"
                                                                                    class="accordion-collapse collapse show"
                                                                                    aria-labelledby="flush-headingThree"
                                                                                    data-bs-parent="#accordionFlushExample">
                                                                                    <div
                                                                                        class="accordion-body">
                                                                                        <label
                                                                                            class="form-label">Montant
                                                                                            Payé</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm amount "
                                                                                            name="paiements[transfer][{{ $key }}][amount]"
                                                                                            placeholder="Montant Payé"
                                                                                            step="0.001"
                                                                                            value="{{ old('amount', $item->amount, '') }}">
                                                                                        <input
                                                                                            name="paiements[transfer][{{ $key }}][client_id]"
                                                                                            type="hidden"
                                                                                            value="1">

                                                                                        <label
                                                                                            class="form-label">Numero
                                                                                            du
                                                                                            Virement</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[transfer][{{ $key }}][numbre]"
                                                                                            placeholder="Numero du Virement"
                                                                                            value="{{ old('numbre', $item->numbre, '') }}">
                                                                                        <label
                                                                                            class="form-label">Bank</label>
                                                                                        {{ Form::select('paiements[transfer][' . $key . '][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', $item->bank), ['id' => 'paiements[transfer][' . $key . '][bank]', 'name' => 'paiements[transfer][' . $key . '][bank]', 'class' => $errors->has('paiements.transfer.' . $key . '.bank') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select', 'required']) }}
                                                                                        <label
                                                                                            class="form-label">Date
                                                                                            du
                                                                                            Virement</label>
                                                                                        <input type="date"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[transfer][{{ $key }}][received_at]"
                                                                                            value="{{ old('received_at', $item->received_at, '') }}">
                                                                                        <input type="hidden"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[transfer][{{ $key }}][type]"
                                                                                            placeholder="Montant Payé"
                                                                                            value="transfer">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="repeat_exchange">
                                                                        @if ($item->type == 'exchange')
                                                                            <div class="accordion-item ">
                                                                                <h2 class="accordion-header"
                                                                                    id="flush-headingFour{{ $key }}">
                                                                                    <div class="row">
                                                                                        <button type="button"
                                                                                            class="btn btn-sm bx bx-trash col-2 text-danger delete"
                                                                                            id="deleteCash"></button>
                                                                                        <input
                                                                                            class="accordion-button collapsed col"
                                                                                            type="button"
                                                                                            data-bs-toggle="collapse"
                                                                                            data-bs-target="#flush-collapseFour{{ $key }}"
                                                                                            aria-expanded="true"
                                                                                            aria-controls="flush-collapseFour{{ $key }}"
                                                                                            value="Traite {{ $key + 1 }}">
                                                                                    </div>
                                                                                </h2>
                                                                                <div id="flush-collapseFour{{ $key }}"
                                                                                    class="accordion-collapse collapse show"
                                                                                    aria-labelledby="flush-headingFour"
                                                                                    data-bs-parent="#accordionFlushExample">
                                                                                    <div
                                                                                        class="accordion-body">
                                                                                        <label
                                                                                            class="form-label">Montant
                                                                                            Payé</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm amount"
                                                                                            name="paiements[exchange][{{ $key }}][amount]"
                                                                                            placeholder="Montant Payé"
                                                                                            step="0.001"
                                                                                            value="{{ old('amount', $item->amount, '') }}">
                                                                                        <input
                                                                                            name="paiements[exchange][{{ $key }}][client_id]"
                                                                                            type="hidden"
                                                                                            value="1">
                                                                                        <label
                                                                                            class="form-label">Numero
                                                                                            de
                                                                                            Traite</label>
                                                                                        <input type="number"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[exchange][{{ $key }}][numbre]"
                                                                                            placeholder="Numero de la Lettre"
                                                                                            value="{{ old('numbre', $item->numbre, '') }}">
                                                                                        <label
                                                                                            class="form-label">Date
                                                                                            de
                                                                                            Traite</label>
                                                                                        <input type="date"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[exchange][{{ $key }}][received_at]"
                                                                                            value="{{ old('received_at', $item->received_at, '') }}">
                                                                                        <input type="hidden"
                                                                                            class="form-control form-control-sm"
                                                                                            name="paiements[exchange][{{ $key }}][type]"
                                                                                            placeholder="Montant Payé"
                                                                                            value="exchange">

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="repeat_cash">

                                                                </div>
                                                                <div class="repeat_check repeat">

                                                                </div>
                                                                <div class="repeat_transfer">

                                                                </div>
                                                                <div class="repeat_exchange">

                                                                </div>
                                                                <div class="repeat_rs">

                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            <div id="products" class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = $cart->items;
                                                        @endphp
                                                        @foreach ($items as $key => $item)
                                                            @if($item->product()->exists())
                                                            <tr id="row{{ $key }}" class="item_cart"
                                                                grade="{{ $key }}"
                                                                data-id="{{ $item->product->id }}">
                                                                <td> <input type="hidden"
                                                                        name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_price_selling]"value="{{ $item['product_price_selling'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_price_buying]"
                                                                        value="{{ $item['product_price_buying'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][quantity_id]"
                                                                        value="{{ $item['quantity_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][price_id]"
                                                                        value="{{ $item['price_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][old_quantity]"
                                                                        value="{{ $item['product_quantity'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_currency_value]"
                                                                        value="{{ $item['product_currency_value'] }}" />
                                                                    @php
                                                                        $quantity = \App\Models\Quantity::where('id', $item->quantity_id)->first();
                                                                        $product = \App\Models\Product::where('id', $item->product_id)->first();
                                                                        
                                                                    @endphp
                                                                    <input type="text"
                                                                        name="items[{{ $key }}][product_name]"
                                                                        placeholder="Nom"
                                                                        class="move form-control form-control-sm name_list"
                                                                        readonly value="{{ $item['product_name'] }}" />
                                                                </td>
                                                                <td><input type="number" style="width:80px;"
                                                                        name="items[{{ $key }}][product_quantity]"
                                                                        class="move form-control form-control-sm quantity"
                                                                        id="quantity{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_quantity'] }}"
                                                                        min="1" max="" step="1">
                                                                </td>
                                                                <td><input type="text" style="width: 70px;"
                                                                        class="move form-control form-control-sm"
                                                                        name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                        readonly /></td>
                                                                <td>
                                                                    <input type="number"
                                                                        name="items[{{ $key }}][product_remise]"
                                                                        style="width: 80px;" min="0"
                                                                        max="100" step="0.01"
                                                                        class="move form-control form-control-sm remise"
                                                                        id="remise{{ $key }}"
                                                                        data-maxdiscount="{{ $product->maxdiscount }} "
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_remise'] }}">
                                                                </td><input type="hidden"
                                                                        name="items[{{ $key }}][product_tva]"
                                                                        style="width: 50px;"
                                                                        class="move form-control form-control-sm tva"
                                                                        id="tva{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_tva'] }}" readonly>
                                                                <td><input type="text"
                                                                        name="items[{{ $key }}][product_price_selling]"
                                                                        style="width: 100px;"
                                                                        class="move form-control form-control-sm price"
                                                                        id="price{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_price_selling'] }}"
                                                                        readonly></td>
                                                                <td><input type="text" style="width: 100px;"
                                                                        class="move form-control form-control-sm price{{ $key }} "
                                                                        id="L_total{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_price_selling'] * ((100 - $item['product_remise']) / 100) * $item['product_quantity'] }}"
                                                                        readonly></td>
                                                                <td><button type="button" name="remove"
                                                                        id="{{ $key }}"
                                                                        tr="{{ $key }}"
                                                                        class="btn btn-danger btn-sm btn_remove delete"><i
                                                                            class="bi bi-trash-fill"></i></button></td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>
                                                                {{-- <select class="single-select" id="product_list"
                                                                    name="product_id">
                                                                    <option disabled selected>Choisir un produit</option>
                                                                    @foreach ($products as $product)
                                                                        <option product-id="{{ $product->id }}"
                                                                            product-name="{{ $product->name }}"
                                                                            price-buying="{{ $product->buying }}"
                                                                            price-selling1="{{ $product->selling1 }}"
                                                                            price-selling2="{{ $product->selling2 }}"
                                                                            price-selling3="{{ $product->selling3 }}"
                                                                            product-unity="{{ $product->unity }}"
                                                                            product-tva="{{ $product->tva }}"
                                                                            product-sku="{{ $product->sku }}"
                                                                            product-currency="{{ $product->currency->name }}"
                                                                            product-currency-id="{{ $product->currency->id }}"
                                                                            product-maxdiscount="{{ $product->maxdiscount }}"
                                                                            product-pricetype="{{ $product->price_type['type'] }}">
                                                                            {{ $product->name }} -
                                                                            {{ $product->reference }} -
                                                                            {{ $product->sku }} </option>
                                                                    @endforeach
                                                                </select> --}}
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <input type="number" id="global_discount" class="form-control form-control-sm" max="100" min="0" step="0.01" style="width: 80px;">
                                                            </td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', $cart->total, '') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                            </div>
                                            </div>
                                            {!! Form::close() !!}

                                            <div class="card g-3">
                                                <h6> Recherche avancée</h6>
                                                <div class="col-12 col-lg-12">
                                                    <div class="row g-3">
                                                        <div class="col-12 col-lg-3">
                                                            <label for="" class="form-label ">Nom</label>
                                                            <input class="form-control form-control-sm filter"
                                                                id="product_name" placeholder="Nom" value=""
                                                                autocomplete="product_name" autofocus>
                                                        </div>
                
                                                        <div class="col-12 col-lg-3">
                                                            <label for="" class="form-label ">Code à barre</label>
                
                                                            <div class="input-group mb-3">
                                                                <input class="form-control form-control-sm filter"
                                                                    id="sku" placeholder="Code à barre"
                                                                    autocomplete="sku">
                                                                <button data-bs-toggle="modal" data-bs-target="#camera"
                                                                    id='barcode' type="button"
                                                                    class="btn btn-sm btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                    <div class="mt-3 mb-3 d-none">
                
                                                        <button type="button" id="filter"
                                                            class="btn btn-primary ">Filtrer</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="table-responsive">
                                                <a id="addThis" role="button" class="btn btn-sm btn-success">Ajout Multiple</a>
                                                <table id="example" class="table align-middle table-striped dataTable" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:10px;    padding-left: 10px; iportant!">
                                                            <div class="form-check" ><input type="checkbox" class="form-check-input" id="check-all"></div>
                                                        </th>
                                                        {{-- <th class="is_filter">Référence</th> --}}
                                                        <th class="is_filter">Nom</th>
                                                        <th class="is_filter">Nom d'auteur</th>
                                                        <th class="is_filter">Maison</th>
                                                        <th class="is_filter">SKU</th>
                                                        <th class="is_filter">Qté</th>
                                                        <th class="is_filter">Prix Détail</th>
                                                        <th class="is_filter">Prix SemiGros</th>
                                                        <th width="100px">Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                
                                                </table>
                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                                                    
                                </div>

                            </div>

                            <div class="col-12 col-lg-5"></div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        <audio id="sucessAudio">
            <source src="{{ asset('assets/admin/plugins/notifications/sounds/mixkit-achievement-bell-600.wav') }}">
        </audio>
        <!--end form-->
    </main>
    <!--end page main-->

    <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scanner code à bare</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="qr-reader"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/arrow-table/arrow-table.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/cart.js') . '?ver=' . time() }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.js"
        integrity="sha512-X1iMoI6a2IoZFOheUVf3ZmcD1L7zN/eVtig6enIq8yBlwDcbPVao/LG8+/SdjcVn72zF+A/viRLPSxfXLu/rbQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('#my_multi_select2').multiSelect();
    </script>
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    {{--    dataTables--}}
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/vfs_fonts.js')}}"></script>

    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script>
        $(document).on('click','#addThis', function() {
            $("input:checkbox[name=product_id]:checked").each(function(){
                $('#addToCart'+$(this).val()).click();
            });
        });
    </script>
    <script type="text/javascript">
        var filter = {};

        //Price range input
        const setLabel = (lbl, val) => {
            const label = $(`#slider-${lbl}-label`);
            label.text(val);
            const slider = $(`#slider-div .${lbl}-slider-handle`);
            const rect = slider[0].getBoundingClientRect();
            label.offset({
                top: rect.top - 30,
                left: rect.left
            });
        }

        const setLabels = (values) => {
            setLabel("min", values[0]);
            setLabel("max", values[1]);
        }
        //Price range input

        function onFieldChange(e, fieldName){
            filter.fieldName = e.target.value;
        }
        $(document).ready(function() {
            var base_url = '{{url(' / ')}}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Tous"] ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: false,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.getData') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(8)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [
                    {
                        data: 'check',
                        name: 'id',
                        width:'10px',
                        sortable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'writer',
                        name: 'writer'
                    },
                    {
                        data: 'publisher',
                        name: 'publisher'
                    },
                    {
                        data: 'sku',
                        name: 'sku'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                        orderable: true,
                    },
                    {
                        data: 'selling1',
                        name: 'selling1'
                    },
                    {
                        data: 'selling2',
                        name: 'selling2'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            $('div.toolbar').html(`<div class="accordion mt-2" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse">
                        Filtres <i class="bi bi-filter"></i>
                    </button>
                </h2>
                <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body p-2">
                        <form id="filter-form">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Nom</label>
                                    <input type="text" name="name" class="form-control form-control-sm filter-field" required="required" placeholder="Nom" id="name">
                                </div>

                                    <div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Auteur</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="writer" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>
<div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Maison</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="publisher" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>





                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">SKU</label>
                                    <input type="text" name="sku" class="form-control form-control-sm filter-field" required="required" placeholder="SKU" id="sku">
                                </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Etat</label>
                                    <select name="status" class="form-control form-control-sm filter-field" id="status">
                                        <option value="all" >Tous</option>
                                        <option selected value="1">Activé</option>
                                        <option value="0">Désactivé</option>
                                    </select>
                                </div>



            </div>






                                <div class=" col-sm-12 col-md-4 mt-2 float-left">
                                    <button class="btn btn-primary" id="apply-filter">Appliquer les filtres</button>
                                </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>`);



            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('name');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter)
            });

            $('#apply-filter').on('click', function(e) {
                const queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.product.getData'));
                table.ajax.url(url + queryString).load();
                table.draw();
                e.preventDefault();
            });



            $(".multiple-select").select2({ width: '100%' });

            $('#ex2').slider().on('slide', function(ev) {
                setLabels(ev.value);
            });
            setLabels($('#ex2').attr("data-value").split(","));
        });
        $('#check-all').click(function() {
            var checked = this.checked;
            $('input[name="product_id"]').each(function() {
                this.checked = checked;
            });
        });
        lc_lightbox('.elem', {
            wrap_class: 'lcl_fade_oc',
            gallery: true,
            //  thumb_attr: 'data-lcl-thumb',
            fading_time: 100,
            skin: 'light',
            slideshow_time: 100,
            radius: 0,
            padding: 0,
            border_w: 0,
        });
    </script>
    {{--    dataTables--}}

    <script>
        function onScanSuccess(decodedText, decodedResult) {
            //alert(`Code scanned = ${decodedText}`, decodedResult);
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>

@if ($type == 'cart')

<script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
<script type="text/template" id="itemsList">
    <div class="card bg-light row pt-2 mx-1">
        <div>
        <button type="button" class="btn-close" id="deleteItem"></button>
        </div>
        <div  class="card-body ">
            <h6 class="text-info">Echéance {?}:</h6>
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <br>
                        <h6>Montant:</h6>
                    </div>
                </div>
                <div class=" col-lg-7 ">
                    <div class="input-group mb-3">
                        <input id="Deadline.{?}.amount" name="Deadline[{?}][amount]" type="number" class="form-control form-control-sm Deadline" aria-label="Text input with checkbox">
                        <div class=" col-4">
                            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-5">
                    <div>
                        <br>
                        <h6>Date d'écheance:</h6>
                    </div>
                </div>
                <div class=" col-lg-7 ">
                        <input id="Deadline.{?}.date" min="{{ date('Y-m-d') }}" name="Deadline[{?}][date]" type="date" class="form-control form-control-sm" aria-label="Text input with checkbox">
                    </div>
             </div>
        </div>
    </div>

</script>
<script type="text/template" id="cashdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingOne{?}">
<div class="row">
<button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCash"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseOne{?}"
aria-expanded="true"
aria-controls="flush-collapseOne{?}" value="Espéces {?}">
</div>
</h2>
<div id="flush-collapseOne{?}"
class="accordion-collapse collapse show"
aria-labelledby="flush-headingOne{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Devise</label>
{{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
@error('currency_id')
<div class="invalid-feedback">
{{ $message }}
</div>
@enderror
<label class="form-label ">Montant Payé</label>
<input type="number" class="form-control amount"
name="paiements[cash][{?}][amount]" step="0.001"
placeholder="Montant Payé" required>
<label class="form-label ">Date</label>
<input type="date"
    class="form-control"
    name="paiements[cash][{?}][received_at]"
    placeholder="Date"
    value="{{ date('Y-m-d') }}">
<input type="hidden" class="form-control" name="paiements[cash][{?}][due_at]" value="{{date('Y-m-d')}}">
<input type="hidden" class="form-control "
name="paiements[cash][{?}][type]"
placeholder="Montant Payé" value="cash">
<input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
</div>
</div>
</div>
</script>
    <script type="text/template" id="checkdetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingTwo{?}">
<div class="row">
<button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCheck"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseTwo{?}"
aria-expanded="true"
aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
</div>
</h2>
<div id="flush-collapseTwo{?}"
class="accordion-collapse collapse show"
aria-labelledby="flush-headingTwo{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control amount"
name="paiements[check][{?}][amount]" step="0.001"
placeholder="Montant Payé" required>
<input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

<label class="form-label">Numero du chèque</label>
<input type="number" class="form-control"
name="paiements[check][{?}][numbre]"
placeholder="Numero du chèque" required>

<label class="form-label">Bank</label>
{{ Form::select('paiements[check][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[check][{?}][bank]', 'name' => 'paiements[check][{?}][bank]', 'class' => $errors->has('paiements.check.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
<label class="form-label">Date de réglement:</label>
<input name="paiements[check][{?}][due_at]" type="date" value="{{ date('Y-m-d') }}"
class="form-control"
aria-label="Text input with checkbox" required>
<label class="form-label ">Date</label>
<input type="date"
    class="form-control"
    name="paiements[check][{?}][received_at]"
    placeholder="Date"
    value="{{ date('Y-m-d') }}">
<input type="hidden" class="form-control"
name="paiements[check][{?}][type]"
placeholder="Montant Payé" value="check">

</div>
</div>
</div>
</script>

    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="true"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control amount" step="0.001"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numéro de traite</label>
            <input type="number" class="form-control"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numéro de traite">
            <label class="form-label">Date de traite</label>
            <input type="date" class="form-control" value="{{ date('Y-m-d') }}"
                name="paiements[exchange][{?}][due_at]">
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[exchange][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="true"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control amount" step="0.001"
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numéro du Virement</label>
            <input type="number" class="form-control"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement">
                <label class="form-label">Bank</label>
                {{ Form::select('paiements[transfer][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[transfer][{?}][bank]', 'name' => 'paiements[transfer][{?}][bank]', 'class' => $errors->has('paiements.transfer.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
            <label class="form-label">Date du réglement</label>
            <input type="date" class="form-control" value="{{ date('Y-m-d') }}"
                name="paiements[transfer][{?}][due_at]">
                <input type="hidden" class="form-control"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[transfer][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
        </div>
    </div>
</div>
</script>



<script>
    function getDate(added_months) {
        var date = new Date();
        var newDate = new Date(date.setMonth(date.getMonth() + added_months));
        var currentDate = newDate.toISOString().substring(0, 10);
        return currentDate;
    }

    function divideCommitments(total_items) {
        var total = $('.total').val();
        for (let index = 1; index <= total_items; index++) {
            let amount = document.getElementById('Deadline.' + index + '.amount');
            let date = document.getElementById('Deadline.' + index + '.date');
            date.value = getDate(index - 1);
            if (index == total_items) {
                amount.value = (Math.floor(total / total_items) + total % total_items).toFixed(3);
            } else {
                amount.value = (Math.floor(total / total_items)).toFixed(3);
            }
        }
    }

    function initAmount(method, item) {
        var input = item.find(".amount");
        input.focus();
        var total = $('.total').val();
        var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
        var amounts_count = $('.amount').length;
        if (amounts_count <= 1) {
            first_amount.val(total);
        }
    }

    function initRSAmount(method, item) {
        var input = item.find(".amount");
        input.focus();
        var total = $('.total').val();
        var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
        var amounts_count = $('.amount').length;
        first_amount.val(total / 100);
    }
</script>

<script>
    $(function() {
        $(".repeat_items").repeatable1({
            addTrigger: "#addItem",
            deleteTrigger: "#deleteItem",
            max: 400,
            min: 2,
            template: "#itemsList",
            itemContainer: ".row",
            afterAdd: function(item) {
                // var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                // divideCommitments(totalItems);
            },
            afterDelete: function() {
                // var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                // divideCommitments(totalItems);
            },
        });
    });
    $(function() {
        $(".repeat_check").repeatable({
            addTrigger: "#addCheck",
            deleteTrigger: "#deleteCheck",
            max: 400,
            min: 0,
            template: "#checkdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('check', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_cash").repeatable2({
            addTrigger: "#addCash",
            deleteTrigger: "#deleteCash",
            max: 400,
            min: 0,
            template: "#cashdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('cash', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_exchange").repeatable3({
            addTrigger: "#addExchange",
            deleteTrigger: "#deleteExchange",
            max: 400,
            min: 0,
            template: "#exchangedetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('exchange', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_transfer").repeatable4({
            addTrigger: "#addTransfer",
            deleteTrigger: "#deleteTransfer",
            max: 400,
            min: 0,
            template: "#transferdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('transfer', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });

    $(function() {
        $(".repeat_ra").repeatable6({
            addTrigger: "#addRA",
            deleteTrigger: "#deleteRA",
            max: 400,
            min: 0,
            template: "#radetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                filterReturnedAmounts();
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
                filterReturnedAmounts();
            },

        });
    });
</script>
@endif

@endsection
