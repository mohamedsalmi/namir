@extends('admin.layouts.app')

@section('title','BL')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Détails BL</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i>Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.cart.index') }}">Liste des BLs</a></li>
                <li class="breadcrumb-item active" aria-current="page">Détails de la bl</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->
          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$cart->created_date}}</h5>
                  <p class="mb-0">N° BL : #{{$cart->codification}}</p>
                </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                   <div class="col">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-primary border-0">
                            <i class="bi bi-person text-primary"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Client</h6>
                             <p class="mb-1">{{$cart->client_details['name']}}</p>
                             <p class="mb-1">{{$cart->client_details['adresse']}}</p>
                             <p class="mb-1">{{$cart->client_details['phone']}}</p>
                          </div>
                       </div>
                       </div>
                     </div>
                   </div>
                   <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-success border-0">
                            <i class="bi bi-truck text-success"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">BL info</h6>
                             <p class="mb-1"><strong>Expédition</strong> :{{config('stock.info.name')}}</p>
                             @php
                                 $cart_status='';
                                if($cart->commitments->first()?->paiements->SUM('amount') >= $cart->total)
                                {
                                    $cart_status='Payé';
                                }else{
                                    $cart_status='Non Payé';
                                }
                             @endphp
                             <p class="mb-1"><strong>Statut</strong> {{$cart_status}} </p>
                          </div>
                       </div>
                       </div>
                      </div>
                   </div>
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-danger border-0">
                            <i class="bi bi-geo-alt text-danger"></i>
                          </div>
                          <div class="info">
                            <h6 class="mb-2">Livrer à</h6>
                            <p class="mb-1"><strong>Address</strong> : {{$cart->client_details['adresse']}}</p>
                          </div>
                        </div>
                      </div>
                     </div>
                </div>
              </div><!--end row-->

              <div class="row">
                  <div class="col-12 col-lg-8">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                           <div class="table-responsive">
                             <table class="table align-middle mb-0">
                               <thead class="table-light">
                                 <tr>
                                   <th>Produit</th>
                                   <th>Prix unitaire</th>
                                   <th>Quantité</th>
                                   <th>Total</th>
                                 </tr>
                               </thead>
                               <tbody>
                                @php
                                $total_tva = 0;
                                $total_ut = 0;
                            @endphp
                                @foreach($cartitems as $cartitem)
                                @php
                                $pu=($cartitem->product_price_selling*100)/(100+$cartitem->product_tva);
                                $couttva=($pu * $cartitem->product_tva) / 100;
                                $total_ut += $pu * $cartitem->product_quantity;
                                $total_tva += $couttva * $cartitem->product_quantity;
                            @endphp
                                 <tr>
                                   <td>
                                     <div class="orderlist">
                                      <a class="d-flex align-items-center gap-2" href="javascript:;">
                                        <div class="product-box">
                                            <img src="{{ asset( $cartitem->product->default_image_url ?? config('stock.image.default.product')) }}" alt="">
                                        </div>
                                        <div>
                                            <P class="mb-0 product-title">{{$cartitem->product_name}}</P>
                                        </div>
                                       </a>
                                     </div>
                                   </td>
                                   <td>{{$cartitem->product_price_selling}}  DM</td>
                                   <td>{{$cartitem->product_quantity}}</td>
                                   <td>{{(float)$cartitem->product_quantity*$cartitem->product_price_selling}} DM </td>
                                 </tr>
                                 @endforeach
                               </tbody>
                             </table>
                           </div>
                       </div>
                     </div>
                  </div>
                  <div class="col-12 col-lg-4">
                    <div class="card border shadow-none bg-light radius-10">
                      <div class="card-body">
                          <div class="d-flex align-items-center mb-4">
                             <div>
                                <h5 class="mb-0">Récapitulatif de la bl</h5>
                             </div>
                          </div>
                            <div class="d-flex align-items-center mb-3">

                                <div>
                                    <p class="mb-0">TOTAL HT</p>
                                  </div>
                                  <div class="ms-auto">
                                    <h5 class="mb-0">{{ number_format($cart->total - $total_tva, 3) }}  DM</h5>
                                </div>

                          </div>
                            <div class="d-flex align-items-center mb-3">

                                <div>
                                    <p class="mb-0">TOTAL TVA</p>
                                  </div>
                                  <div class="ms-auto">
                                    <h5 class="mb-0">{{ number_format($total_tva, 3) }}  DM</h5>
                                </div>
                          </div>
                            <div class="d-flex align-items-center mb-3">

                              <div>
                                <p class="mb-0">TTC</p>
                              </div>
                              <div class="ms-auto">
                                <h5 class="mb-0">{{$cart->total}}  DM</h5>
                            </div>
                          </div>

                      </div>
                    </div>


                 </div>
              </div><!--end row-->

            </div>
          </div>

      </main>
   <!--end page main-->
@endsection


