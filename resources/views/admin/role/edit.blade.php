@extends('admin.layouts.app')

@section('title', 'Modifier une rôle')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <style>
        ul {
            list-style: none;
        }
    </style>
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Rôles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.role.index') }}">Liste des rôles</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier un rôle</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <h5 class="mb-0">Modifier un rôle</h5>
                    </div>
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => ['admin.role.update', $role->id],
                                'method' => 'PUT',
                                'id' => 'update-role-form',
                                'class' => 'row g-3',
                            ]) !!}
                            <div class="col-12 required">
                                <label class="form-label">Nom</label>
                                <input name="name" type="text"
                                    class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom rôle"
                                    value="{{ old('name', $role->name) }}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            {{-- <div class="col-12 required" id="price-type-attributes">
                                <label class="form-label">Permissions</label>
                                {{ Form::select('permissions', $permissions, old('permissions', isset($role->permissions) ? $role->permissions->pluck('name') : ''), ['id' => 'price_type_attributes', 'name' => 'permissions[]', 'class' => $errors->has('permissions') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple']) }}
                                @error('permissions')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div> --}}

                            <div class="col-12 required" id="price-type-attributes">
                                <label class="form-label">Permissions</label>

                                <ul>
                                    <li>
                                        <label>
                                            <input name="selectall" {{ old('selectall') ? 'checked' : '' }}
                                                class="form-check-input" type="checkbox" />
                                            Tous séléctionner
                                        </label>
                                        <ul class="accounts">
                                            @php
                                                $index = 0;
                                            @endphp
                                            @foreach ($permissions as $group_name => $groups)
                                                <li class="mb-3">
                                                    <label>
                                                        <input class="form-check-input" type="checkbox"
                                                            name="group_{{ $group_name }}"
                                                            {{ old('group_' . $group_name) ? 'checked' : '' }} />
                                                        {{ ucfirst($group_name) }}
                                                    </label>
                                                    <ul class="subaccounts">
                                                        @foreach ($groups as $key => $permission)
                                                            <li style="display: inline-block">
                                                                <label>
                                                                    <input class="form-check-input sel" type="checkbox"
                                                                        {{ App\Helpers\Helper::roleHasPermission($role, $permission->name) }}
                                                                        id="permissions-{{ $index . $key }}"
                                                                        name="permissions[{{ $index . $key }}]"
                                                                        value="{{ $permission->id }}"
                                                                        {{ old('permissions.' . $index . $key) ? 'checked' : '' }} />
                                                                    {{ str_replace($group_name, '', $permission->name) }}
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                @php
                                                    $index++;
                                                @endphp
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-12">
                                <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

    </main>
@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.sel').each(function() { //loop through all the select elements
                var $cb = $(this);
                var $li = $cb.closest('li');
                var state = $cb.prop('checked');

                // check all children
                $li.find('.sel').prop('checked', state);

                // check all parents, as applicable
                if ($li.parents('li').length)
                    checkParents($li, state);
            });
        });

        function checkParents($li, state) {
            var $siblings = $li.siblings();
            var $parent = $li.parent().closest('li');
            state = state && $siblings.children('label').find('input').prop('checked');
            $parent.children('label').find('input').prop('checked', state);
            if ($parent.parents('li').length)
                checkParents($parent, state);
        }

        $('input').change(function() {
            var $cb = $(this);
            var $li = $cb.closest('li');
            var state = $cb.prop('checked');

            // check all children
            $li.find('input').prop('checked', state);

            // check all parents, as applicable
            if ($li.parents('li').length)
                checkParents($li, state);
        });
    </script>
@endsection
