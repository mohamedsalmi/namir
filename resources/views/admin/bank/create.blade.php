@extends('admin.layouts.app')

@section('title', 'Ajouter une banque')

@section('content')
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Banques</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i> Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{route('admin.bank.list')}}">Liste des banques</a>
                        </li>
          <li class="breadcrumb-item active" aria-current="page">Ajouter une banque</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">
    <div class="col-lg-8 mx-auto">
      <div class="card">
        <div class="card-header py-3 bg-transparent">
          <h5 class="mb-0">Ajouter une banque</h5>
        </div>
        <div class="card-body">
          <div class="border p-3 rounded">
            {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.bank.store', 'method'=>'POST', 'id'=>'form', 'class' => 'row g-3')) !!}
            <div class="col-12 required">
              <label class="form-label">Nom banque</label>
              <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom banque" value="{{ old('name', '') }}">
              @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12">
              <label class="form-label">Tél</label>
              <input name="phone" min="0" step="any" type="phone" class="form-control form-control-sm @error('phone') is-invalid @enderror" placeholder="Tél" value="{{ old('phone', '') }}">
              @error('phone')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12">
              <label class="form-label">Site web</label>
              <input name="website" min="0" step="any" type="website" class="form-control form-control-sm @error('website') is-invalid @enderror" placeholder="Site web" value="{{ old('website', '') }}">
              @error('website')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12">
              <label class="form-label">Adresse</label>
              <input name="address" step="any" type="text" class="form-control form-control-sm @error('address') is-invalid @enderror" placeholder="Adresse" value="{{ old('address', '') }}">
              @error('address')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12">
              <button type="submit" class="btn btn-sm btn-primary px-4">Enregistrer</button>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end row-->

</main>
@endsection