@extends('admin.layouts.app')

@section('title', "Liste des facture d'avoirs")

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}}</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Facture d'avoirs</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des facture d'avoirs</h5>
                <div class="col-md-3 offset-md-5">
                    <h4 class="mb-0 text-success"> Total: <span id="total"
                            data-column="3">{{ number_format($sum, 3) }}</span> </h4>
                </div> 
                @canany(['Ajouter facture avoir'])
                <div class="ms-auto">
                    <a class="btn btn-primary" href="{{ route('admin.invoice.index') }}"><i class="fa fa-plus" aria-hidden="true"
                            title="Ajouter"></i>Ajouter Facture d'avoir</a>
                </div>
                @endcanany
            </div>
        </div>
              
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">

                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="input-group col-md-6 mb-1">
                                    <span class="input-group-text">Du</span>
                                    <input required type="date" class="form-control form-control-sm filter-field" name="from"
                                        placeholder="De" id="from">
                                    <span class="input-group-text">Au</span>
                                    <input required type="date" class="form-control form-control-sm filter-field" name="to"
                                        placeholder="Au" id="to">
                                    <span class="input-group-text">Client</span>
    
                                    <select class="form-control " name="" id="clientfilter">
                                        <option value="">Tous</option>
                                        @foreach (\App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Client(), 'name') as $key => $client)
                                            <option value="{{ $key }}">{{ $client }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                                <table class="dataTable table align-middle w-100">
                                    <thead class="table-light">
                                        <tr>
                                          <th>Numéro</th>
                                          <th>Client</th>
                                          <th>Prix</th>
                                          <th>Etat</th>
                                          <th>Date</th>
                                          <th>Date de creation</th>
                                          <th>Creé par</th>
                                            {{-- <th>Modifié par</th> --}}
                                          <th class="not-export-col">Actions</th>
                                        </tr>
                                    </thead>    
                                <tbody>
                                    {{-- @foreach ($creditnotes as $creditnote)
                                        <tr>
                                            <td>{{$creditnote->codification}}</td>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 cursor-pointer">

                                                    @if ($creditnote->client)
                                                        <img src="{{ $creditnote->client->image_url }}"
                                                            class="rounded-circle" width="44" height="44"
                                                            alt="">
                                                    @else
                                                        <img src="{{ config('stock.image.default.passenger') }}"
                                                            class="rounded-circle" width="44" height="44"
                                                            alt="">
                                                    @endif
                                                    <div class="">
                                                        <p class="mb-0">{{ $creditnote->client_details['name'] }}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ $creditnote->total }} DM</td>
                                            @php
                                                if ($creditnote->paiment_status == 0) {
                                                    $status = '<td><span class="badge rounded-pill alert-danger">Non Payé</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                            <td>{{ $creditnote->created_at }}</td>
                                            <td>
                                            @canany(['Liste facture'])
                                                <div class="d-flex align-items-center gap-3 fs-6">
                                                                <a href="{{ route('admin.creditnote.show', $creditnote->id) }}" class="text-primary"
                                                                    data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                                                                    data-bs-original-title="Voir en Facture" aria-label="Views"><i
                                                                        class="bi bi-file-pdf-fill text-danger"></i></a>
                                                </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                    @endforeach --}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end row-->

    </main>
    <!--end page main-->
    <!--end page main-->
    @endsection
    @section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#select-all').click(function() {
                var checked = this.checked;
                $('input[type="checkbox"]').each(function() {
                    this.checked = checked;
                });
            })
        });
    </script>
      <script>
        $(document).ready(function() {
  
            var base_url = '{{ url(' / ') }}';
            var queryString = '';
  
            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                drawCallback: function(settings) {
                    $('#total').text(settings.json.total);
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [
  
                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },
  
                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },
  
                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
  
  
                    },
  
                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },
  
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.creditnote.index') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(2)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [
  
                 
                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'client',
                        name: 'client',
                    },
                    {
                        data: 'total',
                        name: 'total',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
  
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
  
            });
            function filter() {
                var queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.creditnote.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                // e.preventDefault();
            }
  
            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('id');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
                filter();
            });
  
            $(document).on('change', '#clientfilter', function(e) {
                var value = $(this).val();
                filter['client'] = value;
                console.log(filter);
                filter();
            });
        });
    </script>
@endsection
