@extends('admin.layouts.app')

@section('title', 'Ajouter un attribut')

@section('stylesheets')
  <link href="{{asset('assets/admin/plugins/input-tags/css/tagsinput.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Attributs</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.attribute.list')}}">Liste des attributs</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Ajouter un attribut</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">
    <div class="col-lg-8 mx-auto">
      <div class="card">
        <div class="card-header py-3 bg-transparent">
          <h5 class="mb-0">Ajouter un attribut</h5>
        </div>
        <div class="card-body">
          <div class="border p-3 rounded">
            {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.attribute.store', 'method'=>'POST', 'id'=>'create-attribute-form', 'class' => 'row g-3')) !!}
            <div class="col-12 required">
              <label class="form-label">Nom attribut</label>
              <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom attribut" value="{{ old('name', '') }}">
              @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12 required">
              <label class="form-label">Type</label>
              {{ Form::select('type', \App\Helpers\Helper::makeAttributeTypesDropDownList() , null, ['id'=> 'type', 'name'=> 'type', 'onchange' =>'onTypeChange()', 'class' => $errors->has('type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
              @error('type')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="col-12 required" id="values">
              <label class="form-label">Valeurs</label>
              <input id="tags" name="attribute_values" type="text" value="{{ old('attribute_values', '') }}" data-role="tagsinput" class="form-control form-control-sm @error('attribute_values') is-invalid @enderror" placeholder="Ajouter une valeur" value="{{ old('attribute_values', '') }}">
              @error('attribute_values')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12" id="colors">
              <button type="button" role="button" class="btn btn-sm btn-warning" id="addItem" onclick="return 0;"><i class="bi bi-plus"></i> Ajouter une couleur</button>

            <div id="attributes-container" class="repeat_items">
              @error('colors')
                <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2 mt-2">
                  <div class="d-flex align-items-center">
                    <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                    </div>
                    <div class="ms-3">
                      <div class="text-danger">{{ $message }}</div>
                    </div>
                  </div>
                </div>
              @enderror
              @php
                  $colors = old('colors', []);
                  $nocolors = is_array($colors) ? count($colors) : 0;

                  if (isset($colors)) {
                      if(!is_array($colors)){
                          $nocolors = count(json_decode($colors, true));
                          $colors = json_decode($colors);
                      }
                  } else {
                      $colors = [];
                  }

              @endphp
              @if($nocolors > 0)
                  @foreach($colors as $key => $item)
                  <div class="row">
                    <div class="col-md-5 required">
                      <label class="form-label">Nom couleur</label>
                      <input name="colors[{{$key}}][color_name]" id="colors-{{$key}}-color_name" type="text" class="form-control form-control-sm @error('colors.'. $key . '.color_name') is-invalid @enderror" placeholder="Nom couleur" value="{{ old('colors.'. $key . '.color_name', '') }}">
                      @error('colors.'. $key . 'color_name')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="col-md-6 required">
                      <label class="form-label">Code couleur</label>
                      <input class="form-control form-control-sm" type="color" id="colors-{{$key}}-color_code" name="colors[{{$key}}][color_code]" 
                      value="{{ old('colors.'. $key . 'color_code', '#e66465') }}">
                      @error('colors.'. $key . '.color_code')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="col-md-1 justify-content-around d-flex flex-column">
                      <label class="form-label"></label>
                      <div>
                        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
                      </div>
                    </div>
                  </div>
              @endforeach
              @endif
           </div>

            </div>
            <div class="col-12">
              <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
            </div>
            {!! Form::close() !!}
          </div>

        </div>
      </div>
    </div>
  </div>
  <!--end row-->

            {{--Template--}}
<script type="text/template" id="itemsList">
  <div class="row">
    <div class="col-md-5 required">
      <label class="form-label">Nom couleur</label>
      <input name="colors[{?}][color_name]" id="colors-{?}-color_name" type="text" class="form-control form-control-sm @error('colors.*.color_name') is-invalid @enderror" placeholder="Nom couleur" value="{{ old('colors.{?}.color_name', '') }}">
      @error('colors.{?}.color_name')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-6 required">
      <label class="form-label">Code couleur</label>
      <input class="form-control form-control-sm" type="color" id="colors-{?}-color_code" name="colors[{?}][color_code]" 
           value="#e66465">
      @error('colors.{?}.color_code')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>
  </div>
</script>
{{--Template--}}

</main>
@endsection

@section('scripts')
  <script src="{{asset('assets/admin/plugins/input-tags/js/tagsinput.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
  <script>
    var oldColorsCount = {{$nocolors}}
    var oldType = @json(old('type')) ;
    if(oldColorsCount == 0 && !oldType && oldType != 'color'){
      $(`#colors`).hide();
    }else{
      $(`#colors`).show();
      $(`#values`).hide();
    }
     $(function () {
      $(".repeat_items").repeatable({
          addTrigger: "#addItem",
          deleteTrigger: "#deleteItem",
          max: 400,
          min: 0,
          template: "#itemsList",
          itemContainer: ".row",
          total: $(".repeat_items").find(".row").length || 0 ,
          afterAdd: function (item) {
            },
          beforeDelete: function (item) {
          },
          afterDelete: function () {
          }
      });
  })

  $(document).on('change', '#type', function() {
    console.log('test');

    var val = $(this).val();
    
    if(val == 'color')
    {
      $(`#colors`).show();
      $(`#values`).hide();
    }
    else{
      $(`#colors`).hide();
      $(`#values`).show();
    }
  }); 
  </script>
  <script>
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
    });

    $('#tags').on('beforeItemAdd', function(event) {
      var type = $('#type').val();
      var regex;
      switch (type) {
        case 'text':
          regex = /[\s\S]*/
          break;
        case 'decimal':
          regex = /^-?\d+\.?\d*$/
          break;
      
        default:
          regex = null
          break;
      }
      if (!regex || !regex.test(event.item)){
        event.cancel = true;
      }
    });
  </script>

  <script>
    function onTypeChange(){
      $('#tags').tagsinput('removeAll');
    }
  </script>
@endsection