@extends('admin.layouts.app')

@section('title', 'Liste des attributs')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/datatable/css/jquery.dataTables.min.css')}} " rel="stylesheet">
<link href="{{asset('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css')}} " rel="stylesheet">
<link href="{{asset('assets/admin/plugins/datatable/css/responsive.dataTables.min.css')}} " rel="stylesheet">
<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding : 0px;
            margin-left: 0px;
            display: inline;
            border: 0px;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 0px;
            border-style: none;


        }
    .page-link{
            display: inline;
        }
</style>
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Attributs</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des attributs</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->

    <div class="card">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des attributs</h5>
                @canany(['Ajouter attribut'])
                <div class="ms-auto">
                    <a class="btn btn-primary" href="{{route('admin.attribute.create')}}"><i class="fa fa-plus" aria-hidden="true" title="Ajouter"></i>Ajouter un attribut</a>
                </div>
                @endcanany
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- <table class="table align-middle table-striped">
                </table> -->
                <table id="example" class="table table-striped table-bordered display nowrap dataTable " style="width:100%">
                    <thead>
                        <tr>                            
                            <th class="is_filter">ID</th>
                            <th class="is_filter">Nom</th>
                            <th class="is_filter">Type</th>
                            <th class="is_filter">Date</th>
                            <th width="100px">Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>


        </div>
    </div>

</main>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/vfs_fonts.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var base_url = '{{url(' / ')}}';
        window.pdfMake.fonts = {
            AEfont: {
                normal: 'AEfont-Regular.ttf',
                bold: 'AEfont-Regular.ttf',
                italics: 'AEfont-Regular.ttf',
                bolditalics: 'AEfont-Regular.ttf'
            }
        };
        var table = $('.dataTable').DataTable({
            "order": [
                [0, "desc"]
            ],
            dom: 'Bfrtip',
            buttons: [

                {
                    extend: 'excelHtml5',
                    text: "Excel",
                    exportOptions: {
                        columns: ':not(:last-child)',
                    },
                },

                {
                    extend: 'csvHtml5',
                    text: "CSV",
                    exportOptions: {
                        columns: ':not(:last-child)',
                    },
                },

                {
                    extend: 'pdfHtml5',
                    text: "PDF",
                    customize: function(doc) {
                        doc.defaultStyle = {
                            font: 'AEfont',
                            alignment: 'center',
                            fontSize: 16,
                        }
                    },

                },

                {
                    extend: 'print',
                    text: "Imprimer",
                    exportOptions: {
                        columns: ':not(:last-child)',
                    },
                },

            ],
            processing: true,
            serverSide: true,
            responsive: true,
            "language": {
                "sProcessing": "Traitement en cours",
                "sZeroRecords": "Il n'y a pas de données",
                "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                "sInfoEmpty": "Aucune donnée",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Rechercher",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Premier",
                    "sPrevious": "Précédent",
                    "sNext": "Suivant",
                    "sLast": "Dernier",
                }
            },
            ajax: "{{ route('admin.attribute.list') }}",
            aoColumns: [
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'type',
                    name: 'type'
                },
                {
                    data: 'updated_at',
                    name: 'updated_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    paging: false,
                    searchable: false,
                    bSearchable: false,
                },
            ],
            initComplete: function() {
                this.api().columns('.is_filter').every(function() {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).addClass("form-control form-control-sm")
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function() {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });
    });
</script>

<script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

@if (Session::has('success'))
<script>
    $(function() {
        Swal.fire({
            icon: 'success',
            title: "{{ Session::get('success') }}",
            showConfirmButton: false,
            timer: 500
        })
    })
</script>
@endif

@if (Session::has('error'))
<script>
    $(function() {
        Swal.fire({
            icon: 'error',
            title: "{{ Session::get('error') }}",
            showConfirmButton: false,
            timer: 500
        })
    })
</script>
@endif

<script type="text/javascript">
    $(document).on('click', '#show_confirm', function(event) {
        event.preventDefault();
         var form =  $(this).closest("form");
         var name = $(this).data("name");
         Swal.fire({
            title: 'Êtes-vous sûr?',
            text: "Vous ne pourrez pas revenir en arrière !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        });
     });
</script>

@endsection