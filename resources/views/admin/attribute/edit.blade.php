@extends('admin.layouts.app')

@section('title', 'Modifier un attribut')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection


@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Attributs</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.attribute.list') }}">Liste des attributs</a>
                        </li>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier un attribut</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        {{-- <div class="row">
    <div class="col-lg-8 mx-auto">
      <div class="card">
        <div class="card-header py-3 bg-transparent">
          <h5 class="mb-0">Modifier un attribut</h5>
        </div> --}}
        {{-- <div class="card-body">
          <div class="border p-3 rounded">
            {!! Form::open(array('enctype'=>'multipart/form-data','route' => array('admin.attribute.update', $attribute->id), 'method'=>'PUT', 'id'=>'update-attribute-form', 'class' => 'row g-3')) !!}
            <div class="col-12">
              <label class="form-label">Nom attribut</label>
              <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom attribut" value="{{ old('name', $attribute->name) }}">
              @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="col-12">
              <label class="form-label">Type</label>
              {{ Form::select('type', \App\Helpers\Helper::makeAttributeTypesDropDownList() , $attribute->type, ['id'=> 'type', 'name'=> 'type', 'onchange' =>'onTypeChange()', 'class' => $errors->has('type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
              @error('type')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="col-12 required" id="values">
              <label class="form-label">Valeurs</label>
              <input id="tags" name="attribute_values" type="text" value="{{ old('attribute_values', implode(",", $attribute->values()->pluck('value')->toArray()) ) }}" data-role="tagsinput" class="form-control form-control-sm @error('attribute_values') is-invalid @enderror" placeholder="Ajouter une valeur" value="{{ old('attribute_values', '') }}">
              @error('attribute_values')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            <div class="col-12" id="colors">
              <button type="button" role="button" class="btn btn-sm btn-warning" id="addItem" onclick="return 0;"><i class="bi bi-plus"></i> Ajouter une couleur</button>
            <div id="attributes-container" class="repeat_items">
              @error('colors')
                <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2 mt-2">
                  <div class="d-flex align-items-center">
                    <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                    </div>
                    <div class="ms-3">
                      <div class="text-danger">{{ $message }}</div>
                    </div>
                  </div>
                </div>
              @enderror
              @php
                  $colors = old('colors', $attribute->values);
                  $nocolors = count($colors) ?? 0;

                  if (isset($colors)) {
                      if(!is_array($colors)){
                          $nocolors = count(json_decode($colors, true));
                          $colors = json_decode($colors);
                      }
                  } else {
                      $colors = [];
                  }

              @endphp
              @if ($nocolors > 0 && $attribute->type == 'color')
                  @foreach ($colors as $key => $item)
                  <div class="row">
                    <div class="col-md-5 required">
                      <label class="form-label">Nom couleur</label>
                      <input name="colors[{{$key}}][color_name]" id="colors-{{$key}}-color_name" type="text" class="form-control form-control-sm @error('colors.' . $key . '.color_name') is-invalid @enderror" placeholder="Nom couleur" value="{{ old('colors.'. $key . '.color_name', is_object($item) ? $item->value : '') }}">
                      @error('colors.' . $key . 'color_name')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="col-md-6 required">
                      <label class="form-label">Code couleur</label>
                      <input class="form-control form-control-sm" type="color" id="colors-{{$key}}-color_code" name="colors[{{$key}}][color_code]" 
                      value="{{ old('colors.'. $key . '.color_code', is_object($item) ? $item?->color : '') }}">
                      @error('colors.' . $key . '.color_code')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="col-md-1 justify-content-around d-flex flex-column">
                      <label class="form-label"></label>
                      <div>
                        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
                      </div>
                    </div>
                  </div>
              @endforeach
              @endif
           </div>
            </div>

            <div class="col-12">
              <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
            </div>
            {!! Form::close() !!}
          </div>

        </div> --}}
        {{-- </div> --}}
        <div class="card">
            <div class="card-header py-3">
                <h6 class="mb-0">Ajouter un valeur pour {{ old('name', $attribute->name) }}</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-lg-4 d-flex">
                        <div class="card border shadow-none w-100">
                            <div class="card-body">
                                {!! Form::open([
                                    'enctype' => 'multipart/form-data',
                                    'route' => ['admin.attribute.update', $attribute->id],
                                    'method' => 'PUT',
                                    'id' => 'update-attribute-form',
                                    'class' => 'row g-3',
                                ]) !!}
                                <div class="col-12">
                                    <label class="form-label">Nom attribut</label>
                                    <input name="name" type="text"
                                        class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom attribut"
                                        value="{{ old('name', $attribute->name) }}">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Type</label>
                                    {{ Form::select('type', \App\Helpers\Helper::makeAttributeTypesDropDownList(), $attribute->type, ['id' => 'type', 'name' => 'type', 'onchange' => 'onTypeChange()', 'class' => $errors->has('type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                    @error('type')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-primary m-5">Modifier</button>

                                    </div>
                                </div>
                                {!! Form::close() !!}
                                {!! Form::open([
                                    'enctype' => 'multipart/form-data',
                                    'route' => ['admin.attribute.AddValue', $attribute->id],
                                    'method' => 'POST',
                                    'id' => 'add-attributevalue-form',
                                    'class' => 'row g-3',
                                ]) !!}
                                <div class="col-12 row ">

                                    <label class="form-label">Valeur à ajouter</label>
                                    <div class="col-6">
                                        <input name="value" id="value" type="text"
                                            class="form-control form-control-sm @error('value') is-invalid @enderror"
                                            placeholder="valeur de l'attribut">
                                        <div class="invalid-feedback d-none"></div>
                                    </div>

                                    <div class="col-6">
                                        <button type="submit" id="addValue" class="btn btn-primary">Ajouter</button>

                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 d-flex">
                        <div class="card border shadow-none w-100">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table align-middle dataTable" style="width:100%">
                                        <thead class="table-light">
                                            <tr>
                                                <th>Nom</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            {{-- @foreach ($values as $value)                            
                        <tr>
                          <td data-id="{{$value->id}}"  class="value">{{$value->value}}</td>                  
                          <td>
                            <div class="d-flex align-items-center gap-3 fs-6">
                             <form method="POST" action="{{ route('admin.attribute.deleteValue', $value->id) }}">
                              {{ csrf_field() }}
                               <input name="_method" type="hidden" value="GET">
                               <a id="show_confirm" type="submit"
                               class=" text-danger show_confirm" data-toggle="tooltip"
                               title="Supprimer"><i class="bi bi-trash-fill"></i></a>
                   
                           </form>   
                           </div>
                          </td>
                        </tr>                         
                        @endforeach --}}
                                        </tbody>
                                    </table>
                                    {{-- {{$values->links()  }} --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
        {{-- </div>
  </div> --}}
        <!--end row-->

        {{-- Template --}}
        {{-- <script type="text/template" id="itemsList">
  <div class="row">
    <div class="col-md-5 required">
      <label class="form-label">Nom couleur</label>
      <input name="colors[{?}][color_name]" id="colors-{?}-color_name" type="text" class="form-control form-control-sm @error('colors.*.color_name') is-invalid @enderror" placeholder="Nom couleur" value="{{ old('colors.{?}.color_name', '') }}">
      @error('colors.{?}.color_name')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-6 required">
      <label class="form-label">Code couleur</label>
      <input class="form-control form-control-sm" type="color" id="colors-{?}-color_code" name="colors[{?}][color_code]" 
           value="#e66465">
      @error('colors.{?}.color_code')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>
  </div>
</script> --}}
        {{-- Template --}}



    </main>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            });
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif
    <script>
        var errors = null;
        $("#addValue").on("click", function(e, options = '') {
            e.preventDefault();
            var Form = $('#add-attributevalue-form');
            var url = "/admin/attribute/valuevalidation";
            if (options != '') {
                var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
                Form.append(input);
            }
            if (errors) {
                $.each(errors, function(key, value) {
                    var element = document.getElementById(key);
                    if (element) {
                        element.classList.remove('is-invalid')
                        $("#" + key).next().html('');
                        $("#" + key).next().addClass('d-none');
                    };
                });
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data: Form.serialize(),
                async: false,
                success: function(response, textStatus, jqXHR) {
                    Form.submit();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    errors = jqXHR.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        console.log(value)

                        var element = document.getElementById(key);
                        if (element) {
                            element.classList.add('is-invalid')
                        };
                        $("#" + key).next().html(value[0]);
                        $("#" + key).next().removeClass('d-none');
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
        $(document).ready(function() {

            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.attribute.edit', $attribute->id) }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(0)')
                        .attr('data-id', data.id)
                        .addClass('value');
                },
                aoColumns: [

                    {
                        data: 'value',
                        name: 'value',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });
            $(document).on('dblclick', ".value", function(e) {
                newInput(this);
            });
            // var values = document.getElementsByClassName("value");
            // var clickEvent = document.createEvent('MouseEvents');
            // clickEvent.initEvent('dblclick', true, true);
            // values.dispatchEvent(clickEvent);



            function closeInput(elm) {
                var value = $(elm).find('input').val();
                id = elm.getAttribute('data-id')
                var url = "/admin/attribute/" + id + "/EditValue";

                if (errors) {
                    $.each(errors, function(key, value) {
                        var element = document.getElementById(key + 'Edit');
                        if (element) {
                            element.classList.remove('is-invalid')
                            $("#" + key + 'Edit').next().html('');
                            $("#" + key + 'Edit').next().addClass('d-none');
                        };
                    });
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    data: {
                        value
                    },
                    async: false,
                    success: function(response, textStatus, jqXHR) {
                        $(elm).empty().text(value);
                        $(elm).bind("dblclick", function() {
                            newInput(elm);
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        errors = jqXHR.responseJSON.errors;
                        $.each(errors, function(key, value) {

                            console.log(value)

                            var element = document.getElementById(key + 'Edit');
                            if (element) {
                                element.classList.add('is-invalid')
                            };
                            $("#" + key + 'Edit').next().html(value[0]);
                            $("#" + key + 'Edit').next().removeClass('d-none');
                        });
                    }

                });

            }

            function newInput(elm) {
                $(elm).unbind('dblclick');
                var value = $(elm).text();
                var type = $('#type').val();
                switch (type) {
                case 'text':
                    inputtype='text';
                    break;
                case 'decimal':
                    inputtype='number';
                    break;
            }
                $(elm).empty();
                $("<input>")
                    .attr('type', inputtype)
                    .attr('name', 'value')
                    .attr('id', 'valueEdit')
                    .attr('class', 'form-control form-control-sm ')
                    .val(value)
                    .blur(function() {
                        closeInput(elm);
                    })
                    .appendTo($(elm))
                    .focus();

                $("<div>")
                    .attr('class', 'invalid-feedback d-none')
                    .attr('name', 'value')
                    .appendTo($(elm))
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            // #login-box password field
            var type = $('#type').val();
            switch (type) {
                case 'text':
                    $('#value').attr('type', 'text');
                    $('#value').val('');
                    break;
                case 'decimal':
                    $('#value').attr('type', 'number');
                    $('#value').val('');
                    break;
            }
            $(document).on('change', '#type', function() {
                var type = $('#type').val();
            switch (type) {
                case 'text':
                    $('#value').attr('type', 'text');
                    $('#value').val('');
                    break;
                case 'decimal':
                    $('#value').attr('type', 'number');
                    $('#value').val('');
                    break;
            }
            });

        });
    </script>
@endsection
{{-- 
@section('scripts')
  <script src="{{asset('assets/admin/plugins/input-tags/js/tagsinput.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
  <script>
    var oldColorsCount = {{$nocolors}}
    var oldType = @json(old('type')) ;
    var type = @json($attribute->type) ;
    if(oldColorsCount > 0 && (oldType && oldType == 'color' || type && type == 'color')){
      $(`#colors`).show();
      $(`#values`).hide();
    }else{
      $(`#colors`).hide();
      $(`#values`).show();
    }
     $(function () {
      $(".repeat_items").repeatable({
          addTrigger: "#addItem",
          deleteTrigger: "#deleteItem",
          max: 400,
          min: 0,
          template: "#itemsList",
          itemContainer: ".row",
          total: $(".repeat_items").find(".row").length || 0 ,
          afterAdd: function (item) {
            },
          beforeDelete: function (item) {
          },
          afterDelete: function () {
          }
      });
  })

  $(document).on('change', '#type', function() {
    console.log('test');

    var val = $(this).val();
    
    if(val == 'color')
    {
      $(`#colors`).show();
      $(`#values`).hide();
    }
    else{
      $(`#colors`).hide();
      $(`#values`).show();
    }
  }); 
  </script>
  <script>
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
    });

  $('#tags').on('beforeItemAdd', function(event) {
    var type = $('#type').val();
    var regex;
    switch (type) {
      case 'text':
        regex = /[\s\S]*/
        break;
      case 'decimal':
        regex = /^-?\d+\.?\d*$/
        break;
    
      default:
        regex = null
        break;
    }
    if (!regex || !regex.test(event.item)){
      event.cancel = true;
    }
  });
  </script>

  <script>
    function onTypeChange(){
      $('#tags').tagsinput('removeAll');
    }
  </script>
@endsection --}}
