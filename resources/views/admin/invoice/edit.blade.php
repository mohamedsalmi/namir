@extends('admin.layouts.app')

@section('title', 'Modifier facture' . $invoice->codification)

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.css"
        integrity="sha512-2sFkW9HTkUJVIu0jTS8AUEsTk8gFAFrPmtAxyzIhbeXHRH8NXhBFnLAMLQpuhHF/dL5+sYoNHWYYX2Hlk+BVHQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .ms-container {
            width: 100%;
        }
        .select2-selection--single {
            height: 31px !important;
        }
    </style>
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Factures</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier facture </li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['admin.invoice.update.generated', $invoice->id],
            'method' => 'POST',
            'id' => 'create-cart-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Modifier facture {{$invoice->codification}}</h5>
                            <div class="ms-auto">
                                {{-- <button type="submit" name="save_draft" class="btn btn-secondary">Save to Draft</button> --}}
                                <button type="submit" name="save" id="save"
                                    class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row g-3">

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-2">
                                                {{-- @if ($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}
                                                <label class="form-label">Liste des clients :</label>
                                                <select class="single-select" id="fidel_list" name="client_id">
                                                    <option value="0" class="text-primary" tel-fidel="11111111"
                                                        AD-fidel="adresse" MF-fidel='matricule' name-fidel="Passager"
                                                        Prix-fidel="detail" Remise-fidel="0">Client passager</option>
                                                    @foreach ($clients as $client)
                                                        <option
                                                            {{ $invoice->client()->exists() && $client->id == $invoice->client_id ? 'selected' : '' }}
                                                            value="{{ $client->id }}" name-fidel="{{ $client->name }}"
                                                            tel-fidel="{{ $client->phone }}" MF-fidel="{{ $client->mf }}"
                                                            Remise-fidel="{{ $client->discount }}"
                                                            AD-fidel="{{ $client->adresse }}"
                                                            Prix-fidel="{{ $client->price }}">{{ $client->name }} -
                                                            {{ $client->client_code }} - {{ $client->phone }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm "
                                                    name="client_details[name]" placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('name', $invoice->client_details['name'], '') }}">
                                                @error('client_details.name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm " name="client_details[adresse]"
                                                    placeholder="Adresse" id="adresse"
                                                    value="{{ old('adresse', $invoice->client_details['adresse'] ?? '', '') }}">
                                                @error('client_details.adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm MF" name="client_details[mf]"
                                                    placeholder="MF" id="marticule"
                                                    value="{{ old('mf', $invoice->client_details['mf'] ?? '', '') }}">
                                                @error('client_details.mf')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile "
                                                    name="client_details[phone]" placeholder="tel" id="tel"
                                                    value="{{ old('phone', $invoice->client_details['phone'], '') }}">
                                                @error('client_details.phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            
                                            <div class="form-group col-12 col-lg-2">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date"
                                                    class="form-control form-control-sm datetimepicker-input"
                                                    name="date"
                                                    value="{{ old('date', $invoice->date ? $invoice->date : date('Y-m-d')) }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12 col-lg-2">
                                                <label class="form-label" >Note:</label>
                                                <input type="text"
                                                    class="form-control form-control-sm "
                                                    name="note" id="note"
                                                    value="{{ old('note', $invoice->note ? $invoice->note : '') }}" />
                                                @error('note')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        <audio id="sucessAudio">
            <source src="{{ asset('assets/admin/plugins/notifications/sounds/mixkit-achievement-bell-600.wav') }}">
        </audio>
        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/arrow-table/arrow-table.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/admin/js/cart.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.js"
        integrity="sha512-X1iMoI6a2IoZFOheUVf3ZmcD1L7zN/eVtig6enIq8yBlwDcbPVao/LG8+/SdjcVn72zF+A/viRLPSxfXLu/rbQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('#my_multi_select2').multiSelect();
    </script>
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    {{--    dataTables--}}
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/vfs_fonts.js')}}"></script>

    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script>
        $(document).on('click','#addThis', function() {
            $("input:checkbox[name=product_id]:checked").each(function(){
                $('#addToCart'+$(this).val()).click();
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.single-select').select2({
                theme: 'bootstrap4',
                disabled: true,
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            
            $('#fidel_list').change(function () {
                var name = $('#fidel_list').children("option:selected").attr('name-fidel');
                $('#name').val(name);
            });
        });
    </script>
@endsection
