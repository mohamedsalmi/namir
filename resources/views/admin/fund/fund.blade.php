@extends('admin.layouts.app')

@section('title', 'Caisse')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection

@section('content')
    <main class="page-content">
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Caisse</div>

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i> Tableau de
                                bord</a></li>
                        {{-- <li class="breadcrumb-item"><a href="{{route('admin.store.show', $store->id)}}">{{$store->name}}</a> --}}
                        <li class="breadcrumb-item active" aria-current="page">Caisse</li>
                    </ol>
                </nav>
            </div>

        </div>

        <div class="row mb-4">
            {!! Form::open([
                'enctype' => 'multipart/form-data',
                'route' => 'admin.fund.fund',
                'method' => 'POST',
                'id' => 'fund-form',
            ]) !!}
            <div class="input-group col-md-6">
                <span class="input-group-text">Du</span>
                <input required type="date" class="form-control form-control-sm" name="from" placeholder="De" value="{{request()->get('from')}}"
                    id="from">
                <span class="input-group-text">Au</span>
                <input required type="date" class="form-control form-control-sm" name="to" placeholder="Au" value="{{request()->get('to')}}"
                    id="to">
                {{-- <button title="Envoyer" class="input-group-text text-success"><i class="bi bi-arrow-right"></i></button> --}}
                <a href="{{ route('admin.fund.fund') }}"><span title="Réinitialiser" class="input-group-text text-info"><i
                            class="bi bi-arrow-clockwise"></i></span></a>

            </div>
            {!! Form::close() !!}

        </div>
        <div class="row row-cols-1 row-cols-sm-3 row-cols-md-3 row-cols-xl-3 row-cols-xxl-3">
            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1" style="    font-size: 30px; ">Caisse</p>
                                <h4 class="mb-0 text-{{ $todayIncome > 0 ? 'success' : 'danger' }}">{{ $todayIncome }}
                                </h4>
                            </div>
                            <div class="ms-auto fs-2 text-{{ $todayIncome > 0 ? 'success' : 'danger' }}">
                                <i class="lni lni-money-protection"></i>
                            </div>
                        </div>
                        <hr class="my-2">
                        <div class="ms-auto fs-2 text-{{ $total > 0 ? 'success' : 'danger' }}">
                            <small class="mb-0"><i class="bi bi-arrow-{{ $total > 0 ? 'up' : 'down' }}"></i>
                                <span>{{ $total }} </span></small>
                        </div>
                        {{-- <div class="actions d-flex align-items-center justify-content-center gap-2 mt-3">
                            <a href="javascript:;" class="btn btn-sm btn-sm btn-outline-danger"><i class="bi bi-trash-fill"></i> Delete</a>
                          </div> --}}
                        <div class="row justify-content-end mx-1 mt-1">
                            @canany(['Retirer'])
                                @if ($total_cash > 0)
                                    <button type="button" class="btn btn-sm btn-sm btn-outline-warning" data-bs-toggle="modal"
                                        data-bs-target="#paymentmodal">Retirer<i class="lni lni-money-location"></i> </button>
                                @endif
                            @endcanany
                            <div class="modal fade" id="paymentmodal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Détails de retrait</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        {!! Form::open([
                                            'enctype' => 'multipart/form-data',
                                            'route' => ['admin.fund.withdraw'],
                                            'method' => 'POST',
                                            'id' => 'create-withdraw-form',
                                        ]) !!}
                                        <div class="modal-body">
                                            <label class="form-label">Montant à retirer</label>
                                            <input type="number" class="form-control amount" name="amount"
                                                placeholder="Montant à retirer" step="0.001" max="{{ $total_cash }}"
                                                required>
                                            <label class="form-label">Motif</label>
                                            <input type="text" class="form-control motif" name="motif"
                                                placeholder="Motif">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-secondary"
                                                data-bs-dismiss="modal">Fermer</button>
                                            <input type="submit" value="Retirer"
                                                onclick="if(this.form.checkValidity()) {
                                                    this.disabled = true;
                                                    this.value = 'Soumission...';
                                                    this.form.submit();
                                                }"
                                                class="btn btn-sm btn-primary" id="retrait" />
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1" style="    font-size: 30px; ">Espèces</p>
                                <h4 class="mb-0 text-{{ $today_cash > 0 ? 'success' : 'danger' }}">{{ $today_cash }} DM
                                </h4>
                            </div>
                            <div class="ms-auto fs-2 text-{{ $today_cash > 0 ? 'success' : 'danger' }}">
                                <i class="bi bi-cash"></i>
                            </div>
                        </div>
                        <hr class="my-2">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1" style="    font-size: 30px; ">Chèques</p>
                                <h4 class="mb-0 text-{{ $today_check > 0 ? 'success' : 'danger' }}">{{ $today_check }}
                                    DM</h4>
                            </div>
                            <div class="ms-auto fs-2 text-{{ $today_check > 0 ? 'success' : 'danger' }}">
                                <i class="bi bi-card-heading"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1" style="    font-size: 30px; ">Virements</p>
                                <h4 class="mb-0 text-{{ $today_transfer > 0 ? 'success' : 'danger' }}">
                                    {{ $today_transfer }} DM</h4>
                            </div>
                            <div class="ms-auto fs-2 text-{{ $today_transfer > 0 ? 'success' : 'danger' }}">
                                <i class="bi bi-bank2"></i>
                            </div>
                        </div>
                        <hr class="my-2">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1" style="    font-size: 30px; ">Traite</p>
                                <h4 class="mb-0 text-{{ $today_rs > 0 ? 'success' : 'danger' }}">{{ $today_rs }} DM
                                </h4>
                            </div>
                            <div class="ms-auto fs-2 text-{{ $today_rs > 0 ? 'success' : 'danger' }}">
                                <i class="bi bi-percent"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-4">
            <div class="col">
                <div class="card overflow-hidden radius-10">

                </div>
            </div>
            <div class="col">
                <div class="card overflow-hidden radius-10">

                </div>
            </div>
            <div class="col">
                <div class="card overflow-hidden radius-10">

                </div>
            </div>
            <div class="col">
                <div class="card overflow-hidden radius-10">

                </div>
            </div>
        </div>
        <!--end row-->

        <div class="row">
            <div class="col-12 col-lg-12 col-xl-12 d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <h6 class="mb-0">Historique de caisse</h6>
                            <div class="fs-5 ms-auto dropdown">

                            </div>
                        </div>
                        <div class="table-responsive mt-2">
                            <table class="table align-middle mb-0 caisse_table w-100">
                                <thead class="table-light">
                                    <tr>
                                        {{-- <th>codification</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Montant Payé</th> --}}

                                        
                                        <th>Num.</th>
                                        <th>Client</th>
                                        <th>Type</th>
                                        <th>Montant Payé</th>
                                        <th>Date de réglement</th>
                                        <th>Date de création</th>
                                        <th>Date de versement</th>
                                        <th>Etat chèque</th>
                                        <th>Banque</th>
                                        <th>Ajouté par</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-12 col-xl-12 d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <h6 class="mb-0">Historique des retraits</h6>
                            <div class="fs-5 ms-auto dropdown">

                            </div>
                        </div>
                        <div class="table-responsive mt-2">
                            <table class="table align-middle mb-0 withdraw_table w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th>Date</th>
                                        <th>Montant</th>
                                        <th>Motif</th>
                                        <th>Ajouté par</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->



    </main>

@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/jquery-query-object/jquery.query-object.js') }}"></script>

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {
            var errors = null;
            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.withdraw_table').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                // ajax: "{{ route('admin.fund.fund', ['table' => 'withdraw', 'from' => request()->get('from'), 'to' => request()->get('to')]) }}",
                ajax: "/admin/fund?table=withdraw&from={{request()->get('from')}}&to={{request()->get('to')}}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(1)')
                        .attr('data-id', data.id)
                        .attr('style', "text-align: right;")
                        .addClass('withdraw');
                },
                columnDefs: [{
                    "defaultContent": "",
                    "targets": "_all"
                }],
                aoColumns: [

                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'amount',
                        name: 'amount',
                        style: "text-align: right;"
                    },
                    {
                        data: 'motif',
                        name: 'motif',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });
            var table1 = $('.caisse_table').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",

                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",

                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },



                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "/admin/fund?table=paiments&from={{request()->get('from')}}&to={{request()->get('to')}}",

                columnDefs: [{
                    "defaultContent": "-",
                    "targets": "_all"
                }],
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(2)')
                        .attr('data-id', data.id)
                        .attr('style', "text-align: right;");
                },
                aoColumns: [
                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'client',
                        name: 'client',
                    },
                    {
                        data: 'type',
                        name: 'type',
                    },
                    {
                        data: 'amount',
                        name: 'amount',
                    },
                    {
                        data: 'received_at',
                        name: 'received_at',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'due_at',
                        name: 'due_at',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'bank',
                        name: 'bank',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });
            $(document).on('dblclick', ".withdraw", function(e) {
                newInput(this);
            });

            function closeInput(elm) {
                var amount = $(elm).find('input').val();
                id = elm.getAttribute('data-id')
                var url = "/admin/fund/" + id + "/EditWithdraw";

                if (errors) {
                    $.each(errors, function(key, value) {
                        var element = document.getElementById(key);
                        if (element) {
                            element.classList.remove('is-invalid')
                            $("#" + key).next().html('');
                            $("#" + key).next().addClass('d-none');
                        };
                    });
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    data: {
                        amount
                    },
                    async: false,
                    success: function(response, textStatus, jqXHR) {
                        $(elm).empty().text(amount);
                        $(elm).bind("dblclick", function() {
                            newInput(elm);
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        errors = jqXHR.responseJSON.errors;
                        $.each(errors, function(key, value) {
                            console.log(value)
                            var element = document.getElementById(key);
                            if (element) {
                                element.classList.add('is-invalid')
                            };
                            $("#" + key).next().html(amount[0]);
                            $("#" + key).next().removeClass('d-none');
                        });
                    }

                });

            }

            function newInput(elm) {
                $(elm).unbind('dblclick');
                var value = $(elm).text();
                $(elm).empty();
                $("<input>")
                    .attr('type', 'number')
                    .attr('name', 'amount')
                    .attr('id', 'amount')
                    .attr('class', 'form-control form-control-sm ')
                    .val(value)
                    .blur(function() {
                        closeInput(elm);
                    })
                    .appendTo($(elm))
                    .focus();

                $("<div>")
                    .attr('class', 'invalid-feedback d-none')
                    .attr('name', 'amount')
                    .appendTo($(elm))
            }

            $('#from, #to').change(function() {
                if($("#from").val() && $("#to").val()){
                    var newUrl = $.query.set("from", $("#from").val()).set("to", $("#to").val()).toString();
                    window.location.search = newUrl;
                }
            });
        });
    </script>
@endsection
