@extends('admin.layouts.app')

@section('title', 'Modifier un article')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Articles</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.index')}}">Liste des articles</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Modifier les prix</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">

      {!! Form::open(array('enctype'=>'multipart/form-data', 'route' => array('product.updateprices', $product->id), 'method'=>'PUT', 'id'=>'product-prices-form', 'class' => 'row g-3')) !!}
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Modifier les prix</h5>
              <div class="ms-auto">
                <button name="Save_Price" type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>

          <div class="card-body">
            <div class="border p-3 rounded">

          @switch($product->price_type['type'])
              @case('Simple')
              <div class="row">
                <div class="col-lg-3 col-md-6 required">
                  <label class="form-label">Prix d'achat</label>
                  <input class="form-control form-control-sm @error('prices.0.buying') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[0][buying]" value="{{old('prices.0.buying', count($product->prices) ? $product->prices->first()->buying : '')}}">
                  @error('prices.0.buying')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="col-lg-3 col-md-6 required">
                  <label class="form-label">Prix détail</label>
                  <input class="form-control form-control-sm @error('prices.0.selling1') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[0][selling1]" value="{{old('prices.0.selling1', count($product->prices) ? $product->prices->first()->selling1 : '')}}">
                  @error('prices.0.selling1')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="col-lg-3 col-md-6 required">
                  <label class="form-label">Prix Semi-Gros</label>
                  <input class="form-control form-control-sm @error('prices.0.selling2') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[0][selling2]" value="{{old('prices.0.selling2', count($product->prices) ? $product->prices->first()->selling2 : '')}}">
                  @error('prices.0.selling2')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="col-lg-3 col-md-6 required">
                  <label class="form-label">Prix Gros</label>
                  <input class="form-control form-control-sm @error('prices.0.selling3') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[0][selling3]" value="{{old('prices.0.selling3', count($product->prices) ? $product->prices->first()->selling3 : '')}}">
                  @error('prices.0.selling3')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              @break
          
              @case("Dépend d'un attribut")
              <div class="table-responsive">
                <table class="table ">
                  <thead>
                    <tr><th>{{$attributes[0]["attribute"]["name"]}}</th>
                    @foreach($attributes[0]["values"] as $val)
                    <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                    @endforeach
                  </tr></thead>
                  <tbody>
                      <tr>
                        <td style="width:20%; background: #ffffff;">
                          <label class="form-label">Prix</label>
                        </td>
                        @foreach($attributes[0]["values"] as $key => $item)
                        <td>
                          <input type="hidden" value="{{$attributes[0]['id']}}" name="prices[{{ $key }}][product_attribute1]">
                          <input type="hidden" value="{{$item['id']}}" name="prices[{{ $key }}][product_value1]">
                          
                          <label class="form-label">Prix d'achat</label>
                          <input class="form-control form-control-sm @error('prices.'. $key . '.buying') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key }}][buying]" value="{{old('prices.' . $key .  '.buying', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'buying'))}}">
                          @error('prices.'. $key . '.buying')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                          <label class="form-label">Prix détail</label>
                          <input class="form-control form-control-sm @error('prices.'. $key . '.selling1') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key }}][selling1]" value="{{old('prices.' . $key .  '.selling1', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling1'))}}">
                          @error('prices.'. $key . '.selling1')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                          <label class="form-label">Prix Semi-Gros</label>
                          <input class="form-control form-control-sm @error('prices.'. $key . '.selling2') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key }}][selling2]" value="{{old('prices.' . $key .  '.selling2', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling2'))}}">
                          @error('prices.'. $key . '.selling2')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                          <label class="form-label">Prix Gros</label>
                          <input class="form-control form-control-sm @error('prices.'. $key . '.selling3') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key }}][selling3]" value="{{old('prices.' . $key .  '.selling3', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling3'))}}">
                          @error('prices.'. $key . '.selling3')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror

                        </td>
                          @endforeach
                      </tr>
                                      </tbody>
                </table>
              </div>
                  @break
                  @case("Dépend de deux attributs")
                  <div class="table-responsive">
                    <table class="table ">
                      <thead>
                        <tr><th>{{$attributes[1]["attribute"]["name"]}} / {{$attributes[0]["attribute"]["name"]}}</th>
                        @foreach($attributes[0]["values"] as $val)
                        <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                        @endforeach
                      </tr></thead>
                      <tbody>
                        @foreach($attributes[1]["values"] as $index => $val2)
                          <tr>
                            <td style="width:20%; background:{{ isset($val2['attribute_value']['color']) ? $val2['attribute_value']['color'] : '#ffffff;' }}">{{$val2['attribute_value']['value']}}</td>
                            @foreach($attributes[0]["values"] as $key => $item)
                            <td>
                              <input type="hidden" value="{{$attributes[0]['id']}}" name="prices[{{ $key . $index }}][product_attribute1]">
                              <input type="hidden" value="{{$item['id']}}" name="prices[{{ $key . $index }}][product_value1]">
                              <input type="hidden" value="{{$attributes[1]['id']}}" name="prices[{{ $key . $index }}][product_attribute2]">
                              <input type="hidden" value="{{$val2['id']}}" name="prices[{{ $key . $index }}][product_value2]">
                              <label class="form-label">Prix d'achat</label>
                              <input class="form-control form-control-sm @error('prices.'. $key . $index .'.buying') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key . $index }}][buying]" value="{{old('prices.' . $key . $index . '.buying', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'buying', $val2['id']))}}">
                              @error('prices.'. $key . $index .'.buying')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              <label class="form-label">Prix détail</label>
                              <input class="form-control form-control-sm @error('prices.'. $key . $index .'.selling1') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key . $index }}][selling1]" value="{{old('prices.' . $key . $index . '.selling1', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling1', $val2['id']))}}">
                              @error('prices.'. $key . $index .'.selling1')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              <label class="form-label">Prix Semi-Gros</label>
                              <input class="form-control form-control-sm @error('prices.'. $key . $index .'.selling2') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key . $index }}][selling2]" value="{{old('prices.' . $key . $index . '.selling2', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling2', $val2['id']))}}">
                              @error('prices.'. $key . $index .'.selling2')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              <label class="form-label">Prix Gros</label>
                              <input class="form-control form-control-sm @error('prices.'. $key . $index .'.selling3') is-invalid @enderror" style="width:100px;" type="number" min="0" name="prices[{{ $key . $index }}][selling3]" value="{{old('prices.' . $key . $index . '.selling3', \App\Helpers\Helper::getProductPriceValue($product->id, $item['id'], 'selling3', $val2['id']))}}">
                              @error('prices.'. $key . $index .'.selling3')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                            </td>
                              @endforeach
                          </tr>
                          @endforeach
                                          </tbody>
                    </table>
                  </div>
                  @break
          
              @default
              <div></div>
          @endswitch

        </div>
        </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var attributes = @json($attributes);
  });
</script>

@endsection
