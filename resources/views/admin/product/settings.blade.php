@extends('admin.layouts.app')

@section('title', 'Paramètres article')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
              <div class="breadcrumb-title pe-3">Articles</div>
              <div class="ps-3">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                      <a href="{{route('admin.product.index')}}">Liste des articles</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Paramètres article</li>
                  </ol>
                </nav>
              </div>
            </div>
            <!--end breadcrumb-->

              <div class="row">
                 <div class="col-lg-8 mx-auto">
                  <div class="card">
                    <div class="card-header py-3 bg-transparent"> 
                       <h5 class="mb-0">Paramètres article</h5>
                      </div>
                    <div class="card-body">
                      <div class="border p-3 rounded">
                      {!! Form::open(array('enctype'=>'multipart/form-data','route' => array('product.updatesettings', $product->id), 'method'=>'PUT', 'id'=>'product-settings-form', 'class' => 'row g-3')) !!}
                        
                        <div class="col-12 required">
                          <label class="form-label">Type prix</label>
                          {{ Form::select('price_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.price_types') , old('price_type', $product->price_type), ['id'=> 'price_type', 'name'=> 'price_type[type]', 'onchange' =>"onTypeChange('price', true)", 'class' => $errors->has('price_type.type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('price_type.type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-12 required" id="price-type-attributes">
                          <label class="form-label">Attributs de prix</label>
                          {{ Form::select('price_type_attributes', $attrs, old('price_type[attributes]', isset($product->price_type['attributes']) ? $product->price_type['attributes'] : ''), ['id'=> 'price_type_attributes', 'name'=> 'price_type[attributes][]', 'class' => $errors->has('price_type.attributes') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple'] ) }}
                                @error('price_type.attributes')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>

                        <div class="col-12 required">
                          <label class="form-label">Type de quantité</label>
                          {{ Form::select('quantity_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.quantity_types') , old('quantity_type', $product->quantity_type), ['id'=> 'quantity_type', 'name'=> 'quantity_type[type]', 'onchange' =>"onTypeChange('quantity', true)", 'class' => $errors->has('quantity_type.type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('quantity_type.type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-12 required" id="quantity-type-attributes">
                          <label class="form-label">Attributs de quantité</label>
                          {{ Form::select('quantity_type_attributes', $attrs, old('quantity_type[attributes]', isset($product->quantity_type['attributes']) ? $product->quantity_type['attributes'] : ''), ['id'=> 'quantity_type_attributes', 'name'=> 'quantity_type[attributes][]', 'class' => $errors->has('quantity_type.attributes') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple'] ) }}
                                @error('quantity_type.attributes')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-12 required">
                          <div class="form-check form-switch">
                            <input type='hidden' value='0' name='negative_inventory'>
                            <input class="form-check-input" {{$product->negative_inventory ? 'checked' : ''}} name="negative_inventory" type="checkbox" id="negative_inventory" value="1">
                            <label class="form-check-label" for="negative_inventory">Inventaire négatif</label>
                          </div>
                        </div>
                        <div class="col-12">
                          <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                        </div>
                        {!! Form::close() !!}
                      </div>
                      
                     </div>
                    </div>
                 </div>
              </div><!--end row-->

          </main>
          @endsection

@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {

    let negative_inventory = {{strval($product->negative_inventory)}}

    onTypeChange("price");
    onTypeChange("quantity");
    
    $(this).attr('value', negative_inventory);
    
    var attrs = @json($attrs);

    // $(".multiple-select").select2({
    //   // data: data,
    //   maximumSelectionLength: 2
    // });

    $("#price_type_attributes").select2({
      maximumSelectionLength: {{$product->price_type['type'] == "Dépend de deux attributs" ? 2 : 1}}
    });

    $("#quantity_type_attributes").select2({
      maximumSelectionLength: {{$product->quantity_type['type'] == "Dépend de deux attributs" ? 2 : 1}}
    });

  });

</script>

<script>
  function onTypeChange(type, change = false){
    var value =$(`#${type}_type`).val(); 

    if(value == `Dépend d'un attribut` || value == `Dépend de deux attributs`){
      $(`#${type}-type-attributes`).show();
      $(`#${type}_type_attributes`).select2({
        maximumSelectionLength: value == "Dépend de deux attributs" ? 2 : 1
      });
      if(change){ 
        $(`#${type}_type_attributes`).val(null).trigger('change'); 
      }
    }else{
      $(`#${type}-type-attributes`).hide();
    }
  }
  
</script>


@endsection 
