@extends('admin.layouts.app')

@section('title', 'Ajouter un article')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
              <div class="breadcrumb-title pe-3">Articles</div>
              <div class="ps-3">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Ajouter un article</li>
                  </ol>
                </nav>
              </div>
            </div>
            <!--end breadcrumb-->

              <div class="row">
                 <div class="col-lg-8 mx-auto">
                  <div class="card">
                    <div class="card-header py-3 bg-transparent">
                       <h5 class="mb-0">Ajouter un article</h5>
                      </div>
                    <div class="card-body">
                      <div class="border p-3 rounded">
                      {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.product.store', 'method'=>'POST', 'id'=>'create-product-form', 'class' => 'create-product-form row g-3')) !!}
                        <div class="col-6 required">
                          <label class="form-label">Nom produit</label>
                          <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom produit" value="{{ old('name', '') }}">
                          @error('name')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">SKU</label>
                          <input name="sku" type="text" class="form-control form-control-sm @error('sku') is-invalid @enderror" placeholder="SKU" value="{{ old('sku', '') }}">
                          @error('sku')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>

                        <div class="col-6 required">
                          <label class="form-label">Type de prix</label>
                          {{ Form::select('price_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.price_types') , null, ['id'=> 'price_type', 'name'=> 'price_type[type]', 'onchange' =>'onTypeChange(event)', 'class' => $errors->has('price_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('price_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>

                        

                        <div class="col-6 required">
                          <label class="form-label">Type de quantité</label>
                          {{ Form::select('quantity_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.quantity_types') , null, ['id'=> 'quantity_type', 'name'=> 'quantity_type[type]', 'onchange' =>'onTypeChange(event)', 'class' => $errors->has('quantity_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('quantity_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">Unité</label>
                          {{ Form::select('unity', \App\Helpers\Helper::makeDropDownListFromConfig('stock.unities') , null, ['id'=> 'unity', 'name'=> 'unity', 'class' => $errors->has('unity') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('unity')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">TVA</label>
                          <input name="tva" type="number" required min="0" max="100" pattern="^([0-9][0-9]?|)$"class="form-control form-control-sm @error('tva') is-invalid @enderror" placeholder="TVA (entre 0 et 100)" value="{{ old('tva', '') }}">
                          @error('tva')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">Catégories</label>
                          {{ Form::select('categories', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Category(), 'name') , null, ['id'=> 'categories', 'name'=> 'categories[]', 'class' => $errors->has('categories') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple'] ) }}
                                @error('categories')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">Devise</label>
                          {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name') , null, ['id'=> 'currency_id', 'name'=> 'currency_id', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('currency_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-12 required">
                          <label class="form-label">Description</label>
                          <textarea name="description" class="form-control form-control-sm @error('description') is-invalid @enderror" placeholder="Description">{{ old('description', '') }}</textarea>
                          @error('description')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Meta Title</label>
                          <textarea name="metatitle" class="form-control form-control-sm @error('metatitle') is-invalid @enderror" placeholder="metatitle">{{ old('metatitle', '') }}</textarea>
                          @error('metatitle')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Meta Keywords</label>
                          <textarea name="metakeys" class="form-control form-control-sm @error('metakeys') is-invalid @enderror" placeholder="metakeys">{{ old('metakeys', '') }}</textarea>
                          @error('metakeys')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Meta Description</label>
                          <textarea name="metadescription" class="form-control form-control-sm @error('metadescription') is-invalid @enderror" placeholder="metadescription">{{ old('metadescription', '') }}</textarea>
                          @error('metadescription')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Images</label>
                          <input class="form-control form-control-sm" type="file" name="images[]" multiple>
                        </div>
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header py-3 bg-transparent">
                              <div class="d-sm-flex align-items-center">
                                <h5 class="mb-2 mb-sm-0">Les attributs</h5>
                                <div class="ms-auto">
                                </div>
                            </div>
                          </div>
                           <div class="card-body">
                            {{-- <input data-repeater-create type="button" value="Add"/> --}}
                            @error('attributes')
                            <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                              <div class="d-flex align-items-center">
                                <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                                </div>
                                <div class="ms-3">
                                  <div class="text-danger">{{ $message }}</div>
                                </div>
                              </div>
                            </div>
                            @enderror

                            <div class="container attributes-list">
                              <button id="addItem" class="btn btn-sm btn-warning r-btnAdd"><i class="bi bi-plus"></i>Ajouter un attribut</button>

                              @php
                                  $attributes = old('attributes', []);
                                  $noattributes = is_array($attributes) ? count($attributes) : 0;

                                  if (isset($attributes)) {
                                      if(!is_array($attributes)){
                                          $noattributes = count(json_decode($attributes, true));
                                          $attributes = json_decode($attributes);
                                      }
                                  } else {
                                      $attributes = [];
                                  }
                              @endphp
                              @if($noattributes > 0)
                                  @foreach($attributes as $key => $item)
                                  <div class="r-group row">
                                    <div class="col-md-5 required">
                                      <label for="attribute_id-{{$key}}" class="form-label">Attribut</label>
                                      {!! Form::select('attribute_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Attribute(), 'name'), old('attributes.{{$key}}.attribute_id', ''), ['id' => 'attribute_id-' . $key, 'class' => $errors->has('attributes.++' . '.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id' : 'form-control form-control-sm attribute-id', 'required', 'data-pattern-name' => "attributes[++][attribute_id]", 'data-pattern-id' => "attributes_++_attribute_id"]) !!}
                                      @error('attributes.{{$key}}.attribute_id')
                                      <div class="invalid-feedback">
                                          {{ $message }}
                                      </div>
                                      @enderror
                                    </div>
                                    <div class="col-md-6 required">
                                      <label for="attribute_values-{{$key}}" class="form-label">Valeurs</label>
                                      {!! Form::select('attribute_values', [], old('attributes.{{$key}}.attribute_values', ''), ['id' => 'attribute_values-{{$key}}', 'class' => $errors->has('attributes.{{$key}}.attribute_values') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple', 'required', 'data-pattern-name' => "attributes[++][attribute_values][]", 'data-pattern-id' => "attributes_++_attribute_values", 'data-pattern-index' => "++"]) !!}
                                      @error('attributes.{{$key}}.attribute_values')
                                      <div class="invalid-feedback">
                                          {{ $message }}
                                      </div>
                                      @enderror
                                    </div>
                                    <div class="col-md-1 justify-content-around d-flex flex-column">
                                      <label class="form-label"></label>
                                      <div>
                                        <button id="deleteItem" type="button" class="r-btnRemove btn btn-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  @endforeach
                              @else
                              <div class="r-group row">
                                <div class="col-md-5 required">
                                  <label for="attribute_id-0" class="form-label">Attribut</label>
                                  {!! Form::select('attribute_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Attribute(), 'name'), old('attributes.0.attribute_id', ''), ['id' => 'attribute_id-0', 'class' => $errors->has('attributes.++' . '.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id' : 'form-control form-control-sm attribute-id', 'required', 'data-pattern-name' => "attributes[++][attribute_id]", 'data-pattern-id' => "attributes_++_attribute_id"]) !!}
                                  @error('attributes.0.attribute_id')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                                  @enderror
                                </div>
                                <div class="col-md-6 required">
                                  <label for="attribute_values-0" class="form-label">Valeurs</label>
                                  {!! Form::select('attribute_values', [], old('attributes.0.attribute_values', ''), ['id' => 'attribute_values-0', 'class' => $errors->has('attributes.0.attribute_values') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple', 'required', 'data-pattern-name' => "attributes[++][attribute_values][]", 'data-pattern-id' => "attributes_++_attribute_values", 'data-pattern-index' => "++"]) !!}
                                  @error('attributes.0.attribute_values')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                                  @enderror
                                </div>
                                <div class="col-md-1 justify-content-around d-flex flex-column">
                                  <label class="form-label"></label>
                                  <div>
                                    <button id="deleteItem" type="button" class="r-btnRemove btn btn-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
                                  </div>
                                </div>
                              </div>
                              @endif
                           </div>
                           </div>
                          </div>
                        <div class="col-12">
                          <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                        </div>
                      </div>
                      {!! Form::close() !!}

                     </div>
                    </div>
                 </div>
              </div><!--end row-->

          </main>
          @endsection


          {{--Template--}}
<script type="text/template" id="itemsList">
  <div data-repeater-item>
    <input type="text" name="attribute_id" value="" />
    <input data-repeater-delete type="button" value="Delete" />
  </div>
  <div class="row">
    <div class="col-md-5 required">
      <label class="form-label">Attribut</label>
      {!! Form::select('attributes[{?}][attribute_id]', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Attribute(), 'name'), old('attributes.{?}.attribute_id', ''), ['id' => 'attribute_id-{?}', 'data-index' => '{?}', 'class' => $errors->has('attributes.{?}.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id' : 'form-control form-control-sm attribute-id']) !!}

      @error('attributes.{?}.attribute_id')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-6 required">
      <label class="form-label">Valeurs</label>
      {!! Form::select('attributes[{?}][attribute_values][]', [], old('attributes.{?}.attribute_values', ''), ['id' => 'attribute_values-{?}', 'class' => $errors->has('attributes.{?}.attribute_values') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select' , 'multiple' => 'multiple']) !!}
      @error('attributes.{?}.attribute_values')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn btn-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>
  </div>
</script>


@section('scripts')
{{-- <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js"></script> --}}
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>
{{-- <script src="{{asset('assets/admin/plugins/repeatable-fields/jquery.repeater.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script> --}}

<script src="{{asset('assets/admin/plugins/jquery-form-repeater/jquery-form-repeater.js')}}"></script>
<script>
$('.container').repeater({
  btnAddClass: 'r-btnAdd',
  btnRemoveClass: 'r-btnRemove',
  groupClass: 'r-group',
  minItems: 0,
  maxItems: 0,
  startingIndex: 0,
  showMinItemsOnLoad: false,
  reindexOnDelete: true,
  repeatMode: 'append',
  animation: null,
  animationSpeed: 400,
  animationEasing: 'swing',
  clearValues: true,
  afterAdd: function(doppleganger) {
    $(".multiple-select").select2({ width: '100%' });
    filterAttributes();
    console.log(doppleganger)
    var attrs = @json($attrs);
    var attributesCount = attrs.length;
    var totalItems = $(".attributes-list").find(".row").length || 0;
    console.log({attributesCount})
    console.log({totalItems})
    if(attributesCount == totalItems){
      $(`#addItem`).hide();
    }
  },
  afterDelete: function() {
    $(".multiple-select").select2({ width: '100%' });
    filterAttributes();
    var attrs = @json($attrs);
    var attributesCount = attrs.length;
    var totalItems = $(".attributes-list").find(".row").length || 0;
    console.log({attributesCount})
    console.log({totalItems})
    if(totalItems < attributesCount){
      $(`#addItem`).show();
    }
  }
});
</script>

<script type="text/javascript">

  /////////////////
  // $(document).ready(function () {
  //     $('.create-product-form').repeater({
  //       initEmpty:true,
  //       // hide: function (deleteElement) {
  //       //   console.log(deleteElement)
  //       //   if(confirm('Are you sure you want todelete this element?')) {
  //       //     $(this).slideUp(deleteElement);
  //       //   }
  //       // },

  //     });
  // });
  /////////////////
  </script>

<script type="text/javascript">
  // $(function () {
  //     $(".attributes-list").repeatable({
  //         addTrigger: "#addItem",
  //         deleteTrigger: "#deleteItem",
  //         max: 400,
  //         min: 0,
  //         template: "#itemsList",
  //         itemContainer: ".row",
  //         total: $(".attributes-list").find(".row").length || 0 ,
  //         // total: {{ $noattributes }},
  //         beforeDelete: function (item) {
  //           console.log(item)
  //         },
  //         afterDelete: function () {
  //         }
  //     });
  // })

  function filterAttributes(){
    $('option').prop('disabled', false); //reset all the disabled options on every change event
    $('.attribute-id').each(function() { //loop through all the select elements
      var val = this.value;
      $('.attribute-id').not(this).find('option').filter(function() { //filter option elements having value as selected option
        return this.value === val;
      }).prop('disabled', true); //disable those option elements
    });
  }

  $(document).on('change', '.attribute-id', function() {
    filterAttributes();
    $(".multiple-select").select2({ width: '100%' });
    var val = $(this).val();
    var attributes = @json($attrs);
    console.log($(this).attr('id'))
    var id = $(this).attr('id').search(/[0-9]/);

    var index = Number($(this).attr('id')[id]);
    // if (index !== -1) {
      console.log('✅ String contains at least 1 number');
    // var index = $(this).attr('data-index');
    console.log(index)

    if(val != '')
    {
      let selectedAttribute = attributes.find(e => e.id == val);
      $('#attributes_' + index + '_attribute_values').val(null).empty();
      selectedAttribute.values.forEach(function(element) {

        var data = {
            id: element.id ,
            text: element.value
        };

        var newOption = new Option(data.text, data.id, false, false);

        $('#attributes_' + index + '_attribute_values').append(newOption).trigger('change');
      });
    }
    else{
      $('#attributes_' + index + '_attribute_values').val(null).empty();
    }
  }).change(); 
</script>

<script type="text/javascript">
  $(document).ready(function() {

    //----------------//
  //   var attributes = @json($attributes);
  //   let valuesIds = [];
  //   console.log( JSON.parse(attributes));
  //   attributes.forEach((element, index) => {
  //   var attrs = @json($attrs);
  //   let selectedAttribute = attrs.find(e => e.id == element.attribute_id);
  //   $('#attribute_values-' + (index+1)).val(null).empty();
  //   selectedAttribute.values.forEach(function(element, i) {

  //     var data = {
  //         id: element.id ,
  //         text: element.value
  //     };

  //     var newOption = new Option(data.text, data.id, false, false);

  //     $('#attribute_values-' + (index+1)).append(newOption).trigger('change');
  //   });

  //   element.values.forEach(el => {
  //     valuesIds.push(el.attribute_value_id);
  //   });
  //   $('#attribute_values-' + (index+1)).val(valuesIds);
  //   $('#attribute_values-' + (index+1)).trigger('change');
  // });
  //   //----------------//

  //   var old = @json(old('attributes', []), true);
  //   console.log( JSON.parse(Object.values(old)));
  //   Object.values(old).forEach((el, index) => {
  //     //////////////
  //     $('#attribute_values-' + (index+1)).val(null).empty();
  //   //   el.values.forEach(function(element, i) {

  //   //   var data = {
  //   //       id: element.id ,
  //   //       text: element.value
  //   //   };

  //   //   var newOption = new Option(data.text, data.id, false, false);

  //   //   $('#attribute_values-' + (index+1)).append(newOption).trigger('change');
  //   // });

  //   var valuesIds = [];

  //   el.attribute_values.forEach(el => {
  //     valuesIds.push(el.attribute_value_id);
  //   });
  //   $('#attribute_values-' + (index+1)).val( );
  //   $('#attribute_values-' + (index+1)).trigger('change');
  //     ////////////
      
  //   });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      $(".multiple-select").select2({ width: '100%' });
  });

  // $(document).on('click', '#addItem', function() {
  //   // $(".multiple-select").select2({ width: '100%' });
  // });
</script>

<script type="text/javascript">
  $(document).on('click', '#addItem', function() {

    console.log("{attributesCount}")
    filterAttributes();
    var attrs = @json($attrs);
    var attributesCount = attrs.length;
    var totalItems = $(".attributes-list").find(".row").length || 0;
    console.log({attributesCount})
    console.log({totalItems})
    if(attributesCount == totalItems){
      $(`#addItem`).hide();
    }
  });

  $(document).on('click', '#deleteItem', function() {
    filterAttributes();
    var attrs = @json($attrs);
    var attributesCount = attrs.length;
    var totalItems = $(".attributes-list").find(".row").length || 0;
    console.log({attributesCount})
    console.log({totalItems})
    if(totalItems < attributesCount){
      $(`#addItem`).show();
    }
  });
</script>

<script>
  function onTypeChange(e){
    var totalItems = $(".attributes-list").find(".row").length || 0;
    switch (e.target.value) {
      case 'Dépend d\'un attribut':
          if(totalItems < 1){
            $('#addItem').trigger('click');
          }
        break;
      case 'Dépend de deux attributs':
        if(totalItems < 2){
          for (let index = 0; index < 2 - totalItems; index++) {
            $('#addItem').trigger('click');
          }
        }
        break;
    
      default:
        break;
    }
  }
</script>

@endsection
