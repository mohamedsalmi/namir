@extends('admin.layouts.app')

@section('title', 'Ajouter un article')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/summernote/summernote-lite.min.css') }}" rel="stylesheet">
    <style>
        .duplication_table table {
            max-width: 100%;
        }

        .scrollwrapper {
            overflow: auto;
            overflow-x: scroll;
        }
    </style>
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Articles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.product.index') }}">Liste des articles</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ajouter un article</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <h5 class="mb-0">Ajouter un article</h5>
                    </div>
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => 'admin.product.store',
                                'method' => 'POST',
                                'id' => 'create-product-form',
                                'class' => 'row g-3',
                            ]) !!}
                            <div class="row mt-2">
                                <div class="col-12 required" style="display: none;">
                                    <div class="form-check form-switch">
                                        <input type='hidden' value='0' name='duplicated_product'>
                                        <input class="form-check-input"
                                            {{ old('duplicated_product') && old('duplicated_product') == 1 ? 'checked' : '' }}
                                            name="duplicated_product" type="checkbox" id="duplicated_product"
                                            value="1">
                                        <label class="form-check-label" for="duplicated_product">Article dupliqué?</label>
                                    </div>
                                </div>
                                <div class=" col-lg-12 required">
                                    <div class="row col-lg-6">
                                        <div class="col-lg-6 required">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" name="activation" type="checkbox" checked>
                                                <label class="form-label" for="exampleFormControlInput1"><i
                                                        class="fa fa-tag"></i> Activé </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 required">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" name="by_order" type="checkbox">
                                                <label class="form-label" for="exampleFormControlInput1"><i
                                                        class="fa fa-tag"></i> Par ordre </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                    <label class="form-label">Nom produit</label>
                                    <input name="name" type="text"
                                        class="form-control form-control-sm @error('name') is-invalid @enderror"
                                        placeholder="Nom produit" value="{{ old('name', '') }}">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                    <label class="form-label">Devise</label>
                                    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), null, ['id' => 'currency_id', 'name' => 'currency_id', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                    @error('currency_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                {{-- <div class="col-6 required">
                          <label class="form-label">Type de prix</label>
                          {{ Form::select('price_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.price_types') , null, ['id'=> 'price_type', 'name'=> 'price_type[type]', 'onchange' =>'onTypeChange(event)', 'class' => $errors->has('price_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('price_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>

                        <div class="col-6 required">
                          <label class="form-label">Type de quantité</label>
                          {{ Form::select('quantity_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.quantity_types') , null, ['id'=> 'quantity_type', 'name'=> 'quantity_type[type]', 'onchange' =>'onTypeChange(event)', 'class' => $errors->has('quantity_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('quantity_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div> --}}

                                <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                    <label class="form-label">Unité</label>
                                    {{ Form::select('unity', \App\Helpers\Helper::makeDropDownListFromConfig('stock.unities'), null, ['id' => 'unity', 'name' => 'unity', 'class' => $errors->has('unity') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                    @error('unity')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                    <label class="form-label">TVA</label>
                                    <input name="tva" type="number" required step="0.1" min="0"
                                        max="100" pattern="^([0-9][0-9]?|)$"
                                        title="Veuillez saisir une valeur entre 0 et 100"
                                        class="form-control form-control-sm @error('tva') is-invalid @enderror"
                                        placeholder="TVA (entre 0 et 100)" value="{{ old('tva', '0') }}">
                                    @error('tva')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="row duplication">
                                    <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                        <label class="form-label">Dupliquer par</label>
                                        {!! Form::select(
                                            'duplicated_by',
                                            \App\Helpers\Helper::makeDropDownListFromModelExcept(new \App\Models\Attribute(), 'name', 'الطبعة'),
                                            '',
                                            [
                                                'id' => 'duplication-attribute',
                                                'class' => $errors->has('attributes.attribute_id')
                                                    ? 'form-control form-control-sm is-invalid duplication-attribute'
                                                    : 'form-control form-control-sm duplication-attribute',
                                            ],
                                        ) !!}
                                        @error('duplicated_by')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                        <label class="form-label">Valeurs</label>
                                        <select class="duplication-values form-control form-control-sm single-select"
                                            id="duplication-values" name="duplication-values[]" single="single"></select>
                                        @error('duplication-values')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-1 justify-content-around d-flex flex-column">
                                        <label class="form-label"></label>
                                        <div>
                                            <button id="duplicate" type="button" class="btn btn-primary"
                                                title="Confirmer">Dupliquer</button>
                                        </div>
                                    </div>

                                    @if ($errors->any())
                                        {!! implode('', $errors->all('<div>:message</div>')) !!}
                                    @endif

                                    <div class="duplication_table">

                                        @php
                                            $duplications = old('duplications', []);
                                            $noduplications = count($duplications);
                                            $isduplication = old('duplicated_product', null);
                                        @endphp
                                        @if ($duplications)
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th scope="col"></th>
                                                        <th scope="col">Qté minimale</th>
                                                        <th scope="col">Qté réserve</th>
                                                        <th scope="col">Code à barre</th>
                                                        <th scope="col">Remise max</th>
                                                        <th scope="col">Prix d'achat</th>
                                                        <th scope="col">Prix détails</th>
                                                        <th scope="col">Prix semi gros</th>
                                                        <th scope="col">Prix gros</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($duplications as $i => $item)
                                                        <tr>
                                                            <th scope="row"></th>
                                                            <td>
                                                                <input type="hidden"
                                                                    name="duplications[{{ $i }}][attribute_value]"
                                                                    value="{{ old('duplications.' . $i . '.attribute_value', '') }}">
                                                                <input
                                                                    name="duplications[{{ $i }}][minimum_quantity]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.minimum_quantity') is-invalid @enderror"
                                                                    placeholder="Qté minimale"
                                                                    value="{{ old('duplications.' . $i . '.minimum_quantity', '') }}">
                                                                @error('duplications.' . $i . '.minimum_quantity')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input
                                                                    name="duplications[{{ $i }}][reserve_quantity]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.reserve_quantity') is-invalid @enderror"
                                                                    placeholder="Qté réserve"
                                                                    value="{{ old('duplications.' . $i . '.reserve_quantity', '') }}">
                                                                @error('duplications.' . $i . '.reserve_quantity')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][reference]"
                                                                    type="text" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.reference') is-invalid @enderror"
                                                                    placeholder="Référence"
                                                                    value="{{ old('duplications.' . $i . '.reference', '') }}">
                                                                @error('duplications.' . $i . '.reference')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][sku]"
                                                                    type="text" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.sku') is-invalid @enderror"
                                                                    placeholder="Code à barre"
                                                                    value="{{ old('duplications.' . $i . '.sku', '') }}">
                                                                @error('duplications.' . $i . '.sku')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][discount]"
                                                                    type="text" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.discount') is-invalid @enderror"
                                                                    placeholder="Code à barre"
                                                                    value="{{ old('duplications.' . $i . '.discount', '0') }}">
                                                                @error('duplications.' . $i . '.discount')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input
                                                                    name="duplications[{{ $i }}][maxdiscount]"
                                                                    type="text" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.maxdiscount') is-invalid @enderror"
                                                                    placeholder="Code à barre"
                                                                    value="{{ old('duplications.' . $i . '.maxdiscount', '100') }}">
                                                                @error('duplications.' . $i . '.maxdiscount')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][buying]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.buying') is-invalid @enderror"
                                                                    placeholder="Prix d'achat"
                                                                    value="{{ old('duplications.' . $i . '.buying', '') }}">
                                                                @error('duplications.' . $i . '.buying')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][selling1]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.selling1') is-invalid @enderror"
                                                                    placeholder="Prix détails"
                                                                    value="{{ old('duplications.' . $i . '.selling1', '') }}">
                                                                @error('duplications.' . $i . '.selling1')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][selling2]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.selling2') is-invalid @enderror"
                                                                    placeholder="Prix semi gros"
                                                                    value="{{ old('duplications.' . $i . '.selling2', '') }}">
                                                                @error('duplications.' . $i . '.selling2')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <td>
                                                                <input name="duplications[{{ $i }}][selling3]"
                                                                    type="number" required
                                                                    class="form-control form-control-sm @error('duplications.' . $i . '.selling3') is-invalid @enderror"
                                                                    placeholder="Prix gros"
                                                                    value="{{ old('duplications.' . $i . '.selling3', '') }}">
                                                                @error('duplications.' . $i . '.selling3')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-12 required duplicated">
                                    <label class="form-label">Prix d'achat</label>
                                    <input name="buying" type="number" step="0.001"  min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('buying') is-invalid @enderror"
                                        placeholder="Prix d'achat" value="{{ old('buying', '') }}">
                                    @error('buying')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated d-none" >
                                    <label class="form-label">Coef. détails</label>
                                    <input name="coef1" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('coef1') is-invalid @enderror"
                                        placeholder="Coef. détails"
                                        value="0">
                                    @error('coef1')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated">
                                    <label class="form-label">Prix détails</label>
                                    <input name="selling1" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('selling1') is-invalid @enderror"
                                        placeholder="Prix détails" value="{{ old('selling1', '') }}">
                                    @error('selling1')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated  d-none">
                                    <label class="form-label">Coef. semi gros</label>
                                    <input name="coef2" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('coef2') is-invalid @enderror"
                                        placeholder="Coef. semi gros"
                                        value="0">
                                    @error('coef2')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated">
                                    <label class="form-label">Prix semi gros</label>
                                    <input name="selling2" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('selling2') is-invalid @enderror"
                                        placeholder="Prix semi gros" value="{{ old('selling2', '') }}">
                                    @error('selling2')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated  d-none">
                                    <label class="form-label">Coef. gros</label>
                                    <input name="coef3" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('coef3') is-invalid @enderror"
                                        placeholder="Coef. gros"
                                        value="0">
                                    @error('coef3')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12 required duplicated  d-none">
                                    <label class="form-label">Prix gros</label>
                                    <input name="selling3" type="number" step="0.001" required min="1"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('selling3') is-invalid @enderror"
                                        placeholder="Prix gros" value="0">
                                    @error('selling3')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12  required duplicated">
                                    <label class="form-label">Quantité minimale</label>
                                    <input name="minimum_quantity" type="number" required min="0"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('minimum_quantity') is-invalid @enderror"
                                        placeholder="Quantité minimale" value="{{ old('minimum_quantity', '0') }}">
                                    @error('minimum_quantity')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 required duplicated">
                                    <label class="form-label">Quantité réserve</label>
                                    <input name="reserve_quantity" type="number" required min="0"
                                        pattern="^([0-9][0-9]?|)$" title=""
                                        class="form-control form-control-sm @error('reserve_quantity') is-invalid @enderror"
                                        placeholder="Quantité réserve" value="{{ old('reserve_quantity', '0') }}">
                                    @error('reserve_quantity')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12  duplicated">
                                    <label class="form-label">Référence</label>
                                    <input name="reference" type="text"
                                        class="form-control form-control-sm @error('reference') is-invalid @enderror"
                                        placeholder="Référence" value="{{ old('reference', '') }}">
                                    @error('reference')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Scanner code à bare</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="qr-reader"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12  duplicated ">
                                    <label class="form-label">SKU(Code à barre)</label>
                                    <div class="input-group mb-3">
                                        <input name="sku" type="text" id="sku"
                                            class="form-control form-control-sm @error('sku') is-invalid @enderror"
                                            placeholder="SKU" value="{{ old('sku', '') }}">
                                        <button data-bs-toggle="modal" data-bs-target="#camera" id='barcode'
                                            type="button"
                                            class="btn btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                    </div>
                                    @error('sku')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12  duplicated">
                                    <label class="form-label">Remise</label>
                                    <input name="discount" type="number"
                                        class="form-control form-control-sm remise @error('discount') is-invalid @enderror"
                                        placeholder="Remise max" step="0.1" min="0" max="100"
                                        value="{{ old('discount', '0') }}">
                                    @error('discount')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12  duplicated">
                                    <label class="form-label">Remise max</label>
                                    <input name="maxdiscount" type="number"
                                        class="form-control form-control-sm remise @error('maxdiscount') is-invalid @enderror"
                                        placeholder="Remise max" step="0.1" min="0" max="100"
                                        value="{{ old('maxdiscount', '100') }}">
                                    @error('maxdiscount')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                    <label class="form-label">Catégories</label>
                                    {{ Form::select('categories', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Category(), 'name'), null, ['id' => 'categories', 'name' => 'categories[]', 'class' => $errors->has('categories') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple']) }}
                                    @error('categories')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                    <label class="form-label">offers</label>
                                    {!! Form::select(
                                        'offers_values',
                                        \App\Helpers\Helper::makeDropDownListFromCollection($offers, 'value', true),
                                        old('offers_values', ''),
                                        [
                                            'name' => 'offers_values[]',
                                            'id' => 'offers',
                                            'class' => $errors->has('offers_values')
                                                ? 'form-control form-control-sm multiple-select is-invalid'
                                                : 'form-control form-control-sm multiple-select',
                                            'multiple' => 'multiple',
                                        ],
                                    ) !!}
                                    @error('offers_values')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-sm-12 col-xs-12">
                                    <label class="form-label">Images</label>
                                    <input class="form-control form-control-sm" type="file" name="images[]" multiple>
                                </div>

                                <div class="col-lg-6 col-sm-12"> 
                                    <label class="form-label">Description</label>
                                    <textarea name="description"
                                        class="summernote form-control form-control-sm @error('description') is-invalid @enderror"
                                        placeholder="Description">{{ old('description', '') }}</textarea>
                                    @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="form-label">Résumé</label>
                                    <textarea name="resume" class="form-control form-control-sm @error('resume') is-invalid @enderror"
                                        placeholder="Résumé">{{ old('resume', '') }}</textarea>
                                    @error('resume')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="form-label">Meta Title</label>
                                    <textarea name="metatitle" class="form-control form-control-sm @error('metatitle') is-invalid @enderror"
                                        placeholder="metatitle">{{ old('metatitle', '') }}</textarea>
                                    @error('metatitle')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="form-label">Meta Keywords</label>
                                    <textarea name="metakeys" class="form-control form-control-sm @error('metakeys') is-invalid @enderror"
                                        placeholder="metakeys">{{ old('metakeys', '') }}</textarea>
                                    @error('metakeys')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="form-label">Meta Description</label>
                                    <textarea name="metadescription" class="form-control form-control-sm @error('metadescription') is-invalid @enderror"
                                        placeholder="metadescription">{{ old('metadescription', '') }}</textarea>
                                    @error('metadescription')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header py-3 bg-transparent">
                                            <div class="d-sm-flex align-items-center">
                                                <h5 class="mb-2 mb-sm-0">Les attributs</h5>
                                                <div class="ms-auto" id="addAttributes">
                                                    {{-- <button type="button" data-bs-toggle="modal" data-bs-target="#newAttributeModal" role="button" class="btn btn-sm btn-primary" id="addNewAttribute"><i class="bi bi-plus"></i> Créer nouvel attribut</button> --}}

                                                    <button type="button" role="button" class="btn btn-sm btn-warning"
                                                        id="addItem" onclick="return 0;"><i class="bi bi-plus"></i>
                                                        Ajouter un attribut</button>
                                                    <button role="button" type="button" class="btn btn-sm btn-info"
                                                        id="addItemLivre" onclick="return 0;"><i class="bi bi-plus"></i>
                                                        Ajouter les attributs de livre
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            @error('attributes')
                                                <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                    <div class="d-flex align-items-center">
                                                        <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                                                        </div>
                                                        <div class="ms-3">
                                                            <div class="text-danger">{{ $message }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @enderror
                                            <div id="attributes-container" class="repeat_items">
                                                @php
                                                    $attributes = old('attributes', []);
                                                    $noattributes = is_array($attributes) ? count($attributes) : 0;
                                                    
                                                    if (isset($attributes)) {
                                                        if (!is_array($attributes)) {
                                                            $noattributes = count(json_decode($attributes, true));
                                                            $attributes = json_decode($attributes);
                                                        }
                                                    } else {
                                                        $attributes = [];
                                                    }
                                                    
                                                    $attributesDropDown = \App\Helpers\Helper::makeDropDownListFromModelExcept(new \App\Models\Attribute(), 'name',[ 'الطبعة','العروض']);
                                                    
                                                @endphp
                                                {{-- @json($attributesDropDown) --}}
                                                @if ($noattributes > 0)
                                                    @foreach ($attributes as $key => $item)
                                                        <div class="row">
                                                            <div class="col-md-5 required">
                                                                <label class="form-label">Attribut</label>
                                                                {!! Form::select(
                                                                    'attributes[' . $key . '][attribute_id]',
                                                                    $attributesDropDown,
                                                                    old('attributes.' . $key . '.attribute_id', is_object($item) ? $item->attribute_id : ''),
                                                                    [
                                                                        'id' => 'attribute_id-' . $key,
                                                                        'data-index' => $key,
                                                                        'class' => $errors->has('attributes.' . $key . '.attribute_id')
                                                                            ? 'form-control form-control-sm is-invalid attribute-id'
                                                                            : 'form-control form-control-sm attribute-id',
                                                                        'required',
                                                                    ],
                                                                ) !!}
                                                                @error('attributes.' . $key . '.attribute_id')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="col-md-6 required">
                                                                <label class="form-label">Valeurs</label>
                                                                <input id="'attribute_values-'{{ $key }}"
                                                                    name="attributes[{{ $key }}][attribute_values]"
                                                                    class='form-control form-control-sm single-select attribute_values @error('metakeys') is-invalid @enderror'>
                                                                @error('attributes.' . $key . '.attribute_values')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div
                                                                class="col-md-1 justify-content-around d-flex flex-column">
                                                                <label class="form-label"></label>
                                                                <div>
                                                                    <button id="deleteItem" type="button"
                                                                        class="btn btn-danger" title="Supprimer"><i
                                                                            class="bi bi-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button id="submit-form" type="submit"
                                            class="btn btn-primary px-4">Enregistrer</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <!-- New Attribute Modal -->
                <div class="modal fade" id="newAttributeModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => 'admin.attribute.store',
                                'method' => 'POST',
                                'id' => 'create-attribute-form-ajax',
                            ]) !!}
                            <div class="modal-header">
                                <h5 class="modal-title">Ajouter un attribut</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-12 required">
                                    <label class="form-label">Nom attribut</label>
                                    <input id="name" required name="name" type="text"
                                        class="form-control form-control-sm @error('name') is-invalid @enderror"
                                        placeholder="Nom attribut" value="{{ old('name', '') }}">

                                    <div class="invalid-feedback d-none"></div>

                                </div>
                                <div class="col-12 required">
                                    <label class="form-label">Type</label>
                                    {{ Form::select('type', \App\Helpers\Helper::makeAttributeTypesDropDownList(), null, ['id' => 'type', 'name' => 'type', 'onchange' => 'onAttributeTypeChange()', 'class' => $errors->has('type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}

                                    <div class="invalid-feedback d-none"></div>

                                </div>
                                <div class="col-12 required">
                                    <label class="form-label">Valeurs</label>
                                    <input required id="attribute_values" name="attribute_values" type="text"
                                        value="{{ old('attribute_values', '') }}" data-role="tagsinput"
                                        class="form-control form-control-sm @error('attribute_values') is-invalid @enderror"
                                        placeholder="Ajouter une valeur" value="{{ old('attribute_values', '') }}">

                                    <div class="invalid-feedback d-none"> </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- New Attribute Modal -->

    </main>
@endsection


{{-- Template --}}
<script type="text/template" id="itemsList">
  <div class="row">
    @json($attributesDropDown)
    <div class="col-md-5 required">
      <label class="form-label">Attribut</label>
      {!! Form::select('attributes[{?}][attribute_id]', $attributesDropDown, old('attributes.{?}.attribute_id', ''), ['id' => 'attribute_id-{?}', 'data-index' => '{?}', 'class' => $errors->has('attributes.{?}.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id' : 'form-control form-control-sm attribute-id', 'required']) !!}
 
      @error('attributes.{?}.attribute_id')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
      {{-- @foreach($attributesDropDown as $key => $value)
        <option value="{{$key}}">{{$value}}</option>
      @endforeach --}}
    </div>
    <div class="col-md-6 required">
      <label class="form-label">Valeurs</label>
      <input id='attribute_values-{?}' name="attributes[{?}][attribute_values]" class ='form-control form-control-sm  attribute_values @error('metakeys') is-invalid @enderror'>
      @error('attributes.{?}.attribute_values')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn btn-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>
  </div>
</script>


@section('scripts')
    <!-- <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-custom.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/input-tags/js/tagsinput.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/html5-qrcode/html5-qrcode.min.js') }}"></script>
    <script src="{{asset('assets/admin/plugins/summernote/summernote-lite.min.js')}}"></script>

<script>
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            //alert(`Code scanned = ${decodedText}`, decodedResult);
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>

    <script type="text/javascript">
        var attrs = @json($attrs);
        var attributesCount = attrs.length;

        function loadAttributes() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/admin/attribute/all/",
                type: "GET",
                async: false,
                success: function(response, textStatus, jqXHR) {
                    // $("#addAttributes").load(location.href + "#addAttributes");
                    attributesCount = response.data.length;
                    $('.attribute-id').empty();
                    $('.attribute-id').append($('<option>', {
                        text: 'Choisir une option',
                        value: '',
                        selected: true
                    }))
                    response.data.forEach(element => {
                        $('.attribute-id').append($('<option>', {
                            text: element.name,
                            value: element.id,
                            selected: false
                        }))
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        };

        function onBuyingPriceChange(index) {
            var selling1_coefficient = {{ $settings->selling1_coefficient }};
            var selling2_coefficient = {{ $settings->selling2_coefficient }};
            var selling3_coefficient = {{ $settings->selling3_coefficient }};
            var buying_price = $(`input[name="duplications[${index}][buying]"]`).val()
            $(`input[name="duplications[${index}][selling1]"]`).val(buying_price * (1 + selling1_coefficient / 100));
            $(`input[name="duplications[${index}][selling2]"]`).val(buying_price * (1 + selling2_coefficient / 100));
            $(`input[name="duplications[${index}][selling3]"]`).val(buying_price * (1 + selling3_coefficient / 100));
        };

        var errors = null;
        $(function() {
            $(".repeat_items").repeatable({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 0,
                template: "#itemsList",
                itemContainer: ".row",
                total: $(".repeat_items").find(".row").length || 0,

                beforeDelete: function(item) {},
                afterDelete: function() {}
            });
        })

        function filterAttributes() {
            $('option').prop('disabled', false); //reset all the disabled options on every change event
            $('.attribute-id').each(function() { //loop through all the select elements
                var val = this.value;
                $('.attribute-id').not(this).find('option').filter(
            function() { //filter option elements having value as selected option
                    return this.value === val;
                }).prop('disabled', true); //disable those option elements
            });
        }

        // $(document).on('change', '.attribute-id', function() {

        //   filterAttributes();

        //   var val = $(this).val();
        //   var attributes = @json($attrs);
        //   var index = $(this).attr('data-index');

        //   if(val != '')
        //   {
        //     let selectedAttribute = attributes.find(e => e.id == val);
        //     $('#attribute_values-' + index).val(null).empty();
        //     selectedAttribute.values.forEach(function(element) {

        //       var data = {
        //           id: element.id ,
        //           text: element.value
        //       };

        //       var newOption = new Option(data.text, data.id, false, false);

        //       $('#attribute_values-' + index).append(newOption).trigger('change');
        //     });
        //   }
        //   else{
        //     $('#attribute_values-' + index).val(null).empty();
        //   }
        // }).change(); 

        $(document).on('change', 'input[name="buying"]', function() {
            var _this = $(this);
            var selling1_coefficient = $('input[name="coef1"]').val();
            var selling2_coefficient = $('input[name="coef2"]').val();
            var selling3_coefficient = $('input[name="coef3"]').val();
            $('input[name="selling1"]').val((_this.val() * (1 + selling1_coefficient / 100)).toFixed(3));
            $('input[name="selling2"]').val((_this.val() * (1 + selling2_coefficient / 100)).toFixed(3));
            $('input[name="selling3"]').val((_this.val() * (1 + selling3_coefficient / 100)).toFixed(3));
        });

        $(document).on('change', 'input[name="coef1"]', function() {
            var _this = $(this);
            var selling1_coefficient = _this.val();
            $('input[name="selling1"]').val(($('input[name="buying"]').val() * (1 + selling1_coefficient / 100))
                .toFixed(3));
        });

        $(document).on('change', 'input[name="coef2"]', function() {
            console.log('2');
            var _this = $(this);
            var selling2_coefficient = _this.val();
            $('input[name="selling2"]').val(($('input[name="buying"]').val() * (1 + selling2_coefficient / 100))
                .toFixed(3));
        });

        $(document).on('change', 'input[name="coef3"]', function() {
            console.log('3');
            var _this = $(this);
            var selling3_coefficient = _this.val();
            $('input[name="selling3"]').val(($('input[name="buying"]').val() * (1 + selling3_coefficient / 100))
                .toFixed(3));
        });
        $(document).on('keyup change', 'input[name="selling1"]', function() {
            var _this = $(this);
            var selling1 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef1"]').val((((selling1 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
        $(document).on('keyup change', 'input[name="selling2"]', function() {
            var _this = $(this);
            var selling2 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef2"]').val((((selling2 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
        $(document).on('keyup change', 'input[name="selling3"]', function() {
            var _this = $(this);
            var selling3 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef3"]').val((((selling3 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
    </script>

    {{-- <script type="text/javascript">
  $(document).ready(function() {
    //------Load old atributes------//
    var attributes = @json($attributes);
    let valuesIds = [];
    if(!Array.isArray(attributes)){
      Object.entries(attributes).forEach((element, index) => {
      // var attrs = @json($attrs);
      let selectedAttribute = attrs.find(e => e.id == element[1].attribute_id);
      $('#attribute_values-' + (element[0]+1)).val(null).empty();
      selectedAttribute.values.forEach(function(val, i) {
        var data = {
            id: val.id ,
            text: val.value
        };
        var newOption = new Option(data.text, data.id, false, false);
  
        $('#attribute_values-' + (element[0])).append(newOption).trigger('change');
      });
      element[1].attribute_values.forEach(el => {
        valuesIds.push(el);
      });
      $('#attribute_values-' + (element[0])).val(valuesIds);
      $('#attribute_values-' + (element[0])).trigger('change');
    });
    }
    //------Load old atributes------//
  });
</script> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $(".single-select").select2({
                tags: true,
                width: '100%'
            });
            $('.duplication-values').select2({
                tags: true,
                width: '100%'
            }).trigger('change');
            // $("#attribute_values-1").select2({ tags:true,width: '100%' }).trigger('change');
            console.log('test');
        });
    </script>

    <script type="text/javascript">
        $(document).on('click', '#addItem', function() {
            $(".single-select").select2({
                tags: true,
                width: '100%'
            });
            filterAttributes();
            // var attrs = @json($attrs);
            // var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if (attributesCount == totalItems) {
                $(`#addItem`).hide();
            }
        });
        $(document).on('click', '#addItemLivre', function() {
            var attrs = @json($attrs);
            var attrs_array = ['دار النشر','اسم المؤلف','المحقق', 'نوع التجليد', 'نوعية الورق', 'عدد المجلدات', 'عدد الصفحات', 'الوزن',
                'القياس'
            ];
            attrs.forEach(element => {
                if (jQuery.inArray(element.name, attrs_array) !== -1) {
                    let row_id = $(".repeat_items").find(".row").length;
                    let id = parseFloat($(".attribute-id").last().attr("data-index")) + 1;
                    id = isNaN(id) ? 1 : id;
                    $('#addItem').click();
                    console.log(id);
                    console.log(row_id);
                    console.log($($('#attribute_id-' + id).find("option[value='" + element.id + "']").prop(
                        'disabled')).length);

                    if ($($('#attribute_id-' + id).find("option[value='" + element.id + "']").prop(
                            'disabled'))
                        .length < 1) {
                        $('#attribute_id-' + id).val(element.id).change();
                    } else {
                        $('.row').last().remove();
                    }
                }
            });
        });

        $(document).on('click', '#deleteItem', function() {
            $(".single-select").select2({
                tags: true,
                width: '100%'
            });
            filterAttributes();
            // var attrs = @json($attrs);
            // var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if (totalItems < attributesCount) {
                $(`#addItem`).show();
            }
        });
    </script>

    <script>
        function onTypeChange(e) {
            var totalItems = $(".repeat_items").find(".row").length || 0;
            switch (e.target.value) {
                case 'Dépend d\'un attribut':
                    if (totalItems < 1) {
                        $('#addItem').trigger('click');
                    }
                    break;
                case 'Dépend de deux attributs':
                    if (totalItems < 2) {
                        for (let index = 0; index < 2 - totalItems; index++) {
                            $('#addItem').trigger('click');
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    </script>

    <script>
        $(document).keypress(
            function(event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $('#attribute_values').on('beforeItemAdd', function(event) {
            var type = $('#type').val();
            var regex;
            switch (type) {
                case 'text':
                    regex = /[\s\S]*/
                    break;
                case 'decimal':
                    regex = /^-?\d+\.?\d*$/
                    break;

                default:
                    regex = null
                    break;
            }
            if (!regex || !regex.test(event.item)) {
                event.cancel = true;
            }
        });
    </script>

    <script>
        function onAttributeTypeChange() {
            $('#tags').tagsinput('removeAll');
        }
    </script>

    <script>
        $(document).ready(function() {
            $(document).on('click', 'a.delete', function(e) {
                e.preventDefault();
                var id = $(this).data("id");

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/admin/product/image/" + id,
                    type: "DELETE",
                    // data : data,
                    async: false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
                    success: function(response, textStatus, jqXHR) {
                        $("#container").load(location.href + " #container");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });
            });

            $(document).on('submit', '#create-attribute-form-ajax', function(event) {
                event.preventDefault();
                console.log($(this).serialize());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/admin/attribute/new",
                    type: "POST",
                    data: $(this).serialize(),
                    async: false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
                    success: function(response, textStatus, jqXHR) {
                        console.log(response)
                        $('#newAttributeModal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 500
                        })
                        loadAttributes();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        if (jqXHR.status === 422) {
                            if (jqXHR.responseJSON.message) {
                                Swal.fire({
                                    icon: 'error',
                                    title: jqXHR.responseJSON.message,
                                    showConfirmButton: false,
                                    timer: 500
                                })
                            }
                            errors = jqXHR.responseJSON.errors;
                            $.each(errors, function(key, value) {
                                $("#" + key).addClass('is-invalid');
                                $("#" + key).next().html(value[0]);
                                $("#" + key).next().removeClass('d-none');
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: "Une erreur est parvenue!",
                                showConfirmButton: false,
                                timer: 500
                            });
                        }

                    }
                });
            });

            /*Reset form on modal close */
            $('#newAttributeModal').on('hidden.bs.modal', function() {
                $("#attribute_values").tagsinput('removeAll');
                $(this).find('form').trigger('reset');
                if (errors) {
                    $.each(errors, function(key, value) {
                        $("#" + key).removeClass('is-invalid');
                        $("#" + key).next().html('');
                        $("#" + key).next().addClass('d-none');
                    });
                    errors = null;
                }
            })
            /*Reset form on modal close */
        });
    </script>

    <script>
        let noduplications = @json($noduplications);
        let isduplication = @json($isduplication);
        $('.duplication-values').select2({
            tags: true,
            width: '100%'
        });
        console.log(isduplication, noduplications)
        if (isduplication && isduplication != '0' && noduplications > 0) {
            $('.duplication').show();
            $('.duplicated').hide();
            $('.duplicated input').removeAttr('required');
        } else {
            $('.duplication').hide();
            $('.duplicated').show();
            $('.duplication input').removeAttr('required');
        }

        $(document).on('change', '#duplicated_product', function() {
            var val = $(this).val();
            var ischecked = $(this).is(":checked")
            console.log($(this).is(":checked"));
            if (ischecked) {
                $('.duplicated').hide();
                $('.duplication').show();
                $('.duplicated input').removeAttr('required');
            } else {
                $('.duplicated').show();
                $('.duplication').hide();
                $('.duplication input').removeAttr('required');
                // $(".duplication input").attr('required','true');
            }

        });

        $(document).on('click', '#submit-form', function(e) {
            e.preventDefault();
            var isduplication = $('#duplicated_product');
            var ischecked = isduplication.is(":checked")
            var form = $('#create-product-form');
            if (ischecked) {
                if ($('.duplication_table').children().length > 0) {
                    form[0].checkValidity()
                    form.submit();
                } else {
                    Swal.fire({
                        type: "warning",
                        title: "Erreur!",
                        text: "Vous devez insérer les champs de duplication",
                    });
                }
            } else {
                form.submit();
            }

        });

        $(document).on('click', '#duplicate', function() {
            var val = $(this).val();
            var wrapper = $('<div>').addClass('scrollwrapper');
            var table = $('<table>').addClass('table mt-3');
            wrapper.append(table);
            var header = $('<thead>').html(`
      <tr>
        <th scope="col"></th>
        <th scope="col">Qté minimale</th>
        <th scope="col">Qté réserve</th>
        <th scope="col">Référence</th>
        <th scope="col">SKU</th>
        <th scope="col">Remise max</th>
        <th scope="col">Prix d'achat</th>
        <th scope="col">Prix détails</th>
        <th scope="col">Prix semi gros</th>
        <th scope="col">Prix gros</th>
      </tr>
    `);
            table.append(header);
            var body = $('<tbody>');
            console.log(duplicationValues);
            if (duplicationValues.length == 0) {
                Swal.fire({
                    title: "Erreur!",
                    text: "Vous devez insérer des valeurs",
                    type: "warning",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                return;
            }
            duplicationValues.forEach((element, i) => {
                var row = $('<tr>').addClass('bar').html(`
          <th scope="row">${element.text}</th>
          <td>
            <input type="hidden" name="duplications[${i}][attribute_value]" value="${element.text}">
            <input name="duplications[${i}][minimum_quantity]" type="number" required class="form-control form-control-sm @error('minimum_quantity') is-invalid @enderror" placeholder="Qté minimale" value="{{ old('minimum_quantity', '') }}">
            @error('minimum_quantity')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input name="duplications[${i}][reserve_quantity]" type="number" required class="form-control form-control-sm @error('reserve_quantity') is-invalid @enderror" placeholder="Qté réserve" value="{{ old('reserve_quantity', '') }}">
            @error('reserve_quantity')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input name="duplications[${i}][reference]" type="text" required class="form-control form-control-sm @error('reference') is-invalid @enderror" placeholder="Référence" value="{{ old('reference', '') }}">
            @error('reference')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input name="duplications[${i}][sku]" type="text" required class="form-control form-control-sm @error('sku') is-invalid @enderror" placeholder="Code à barre" value="{{ old('sku', '') }}">
            @error('sku')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
                                  <input name="duplications[${i}][discount]" type="text" required class="form-control form-control-sm @error('discount') is-invalid @enderror" placeholder="Remise max" value="{{ old('discount', '') }}">
                                  @error('discount')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </td>
          <td>
                                  <input name="duplications[${i}][maxdiscount]" type="text" required class="form-control form-control-sm @error('maxdiscount') is-invalid @enderror" placeholder="Remise max" value="{{ old('maxdiscount', '') }}">
                                  @error('maxdiscount')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </td>
          <td>
            <input onchange="onBuyingPriceChange(${i})" name="duplications[${i}][buying]" step="0.001" min="0" type="number" required class="form-control form-control-sm @error('buying') is-invalid @enderror" placeholder="Prix d'achat" value="{{ old('buying', '') }}">
            @error('buying')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input readonly name="duplications[${i}][selling1]" step="0.001" min="0" type="number" required class="form-control form-control-sm @error('selling1') is-invalid @enderror" placeholder="Prix détails" value="{{ old('selling1', '') }}">
            @error('selling1')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input readonly name="duplications[${i}][selling2]" step="0.001" min="0" type="number" required class="form-control form-control-sm @error('selling2') is-invalid @enderror" placeholder="Prix semi gros" value="{{ old('selling2', '') }}">
            @error('selling2')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
          <td>
            <input readonly name="duplications[${i}][selling3]" step="0.001" min="0" type="number" required class="form-control form-control-sm @error('selling3') is-invalid @enderror" placeholder="Prix gros" value="{{ old('selling3', '') }}">
            @error('selling3')
              <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </td>
  `);
                body.append(row);
            });

            table.append(body);

            $('.duplication_table').html(wrapper);

        });

        $(document).on('change', '.duplication-attribute', function() {
            var val = $(this).val();
            var attributes = @json($attrs);
            var index = $(this).attr('data-index');

            if (val != '') {
                let selectedAttribute = attributes.find(e => e.id == val);
                $('.duplication-values').val(null).empty();
                selectedAttribute.values.forEach(function(element) {

                    var data = {
                        id: element.id,
                        text: element.value
                    };

                    var newOption = new Option(data.text, data.id, false, false);

                    $('.duplication-values').append(newOption).trigger('change');
                });
            } else {
                $('.duplication-values').val(null).empty();
            }
        }).change();
    </script>
    <script>
        $(document).on('blur', '.remise', function(e) {
            var max = 100;
            if (($(this).val() < 0) || ($(this).val() == '')) {
                $(this).val(0)
            }
            if (($(this).val() > max)) {
                $(this).val(max)
            }
            calculate_gtotal();
        });
    </script>
    <script>
        $(document).ready(function() {
            $(document).on('select2:opening', '.attribute_values', function(e) {
                let id = $(this).attr('data-select2-id').replace(/attribute_values-/, '');
                let attribute_id = $('#attribute_id-' + id).children("option:selected").val();
                console.log(attribute_id)
                $(document).on('keyup', '.select2-search__field', function(e) {
                    var query = $(this).val();
                    console.log(query)
                    let l = query.length;
                    if (l > 3) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '/admin/product/getAttributeValue',
                            method: "GET",
                            data: {
                                attribute_id: attribute_id,
                                query: query,
                            },
                            success: function(data) {
                                // console.log(data.values);
                                if (data.values.length > 0) {
                                    data.values.forEach(function(val, i) {
                                        var data = {
                                            id: val.id,
                                            text: val.value
                                        };
                                        var newOption = new Option(data.text,
                                            data.id, false, false);
                                        // console.log(newOption);
                                        $('#attribute_values-' + (id)).append(
                                            newOption).trigger('change');
                                        console.log($('#attribute_values-' + (
                                            id)).val())
                                    });
                                }

                            }
                        });
                    }
                    // console.log(attribute_id);
                });
            });
        });
    </script>
@endsection
