@extends('admin.layouts.app')

@section('title', 'Détails article')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css')}} " rel="stylesheet">
    <link href="{{url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css')}} " rel="stylesheet">
    <link href="{{url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css')}} " rel="stylesheet">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button {
                padding : 0px;
                margin-left: 0px;
                display: inline;
                border: 0px;
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                border: 0px;
                border-style: none;
    
    
            }
        .page-link{
                display: inline;
            }
    </style>
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Articles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.product.index') }}">Liste des articles</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Détails article</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-3 row-cols-xxl-3">
            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1">Quantité actuelle</p>
                                <h4 class="mb-0 text-warning">{{ $statistics->current_quantity }}</h4>
                            </div>
                            <div class="ms-auto fs-2 text-warning">
                                <i class="bi bi-stack"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <a href="{{ $statistics->carts_count > 0 ? route('admin.cart.index', ['product_id' => $product->id]) : '#' }}"
                    class="">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="">
                                    <p class="mb-1">Nombre BLs</p>
                                    <h4 class="mb-0 text-info">{{ $statistics->carts_count }}</h4>
                                </div>
                                <div class="ms-auto fs-2 text-info">
                                    <i class="bx bx-cart-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1">Total vendu</p>
                                <h4 class="mb-0 text-purple">{{ $statistics->total_sold }}</h4>
                            </div>
                            <div class="ms-auto fs-2 text-purple">
                                <i class="bi bi-box-seam"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1">Total achat</p>
                                <h4 class="mb-0 text-pink">{{ number_format($statistics->total_buying, 3) }} </h4>
                            </div>
                            <div class="ms-auto fs-2 text-pink">
                                <i class="bx bx-money"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1">Total vente</p>
                                <h4 class="mb-0 text-warning">{{ number_format($statistics->total_sale, 3) }} </h4>
                            </div>
                            <div class="ms-auto fs-2 text-warning">
                                <i class="bx bx-money"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="">
                                <p class="mb-1">Total gain approximatif</p>
                                <h4 class="mb-0 text-success">{{ number_format($statistics->total_margin, 3) }} </h4>
                            </div>
                            <div class="ms-auto fs-2 text-success">
                                <i class="bx bx-money"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12 col-lg-12 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="row align-items-center g-3">
                        <div class="col-12 col-lg-6">
                            <h6 class="mb-0">Roulement</h6>
                        </div>
                        <div class="col-12 col-lg-6 text-md-end">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1">De</span>
                                <input name="from" id="from" type="date" class="form-control form-control-sm" placeholder="De"
                                    aria-describedby="basic-addon1">
                                <span class="input-group-text" id="basic-addon2">À</span>
                                <input name="to" id="to" type="date" class="form-control form-control-sm" placeholder="À"
                                    aria-describedby="basic-addon2">
                                    <button id="refresh_chart" type="button" class="btn btn-outline-primary">Actualiser</button>
                            </div>
                        </div>
                    </div>
                    
                    <div id="product-price-evolution"></div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-header">
                    <h3>Mouvement</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table align-middle table-striped dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>Type de mouvement</th>
                                <th>Référence</th>
                                <th>Date</th>
                                <th>Quantité</th>
                                <th>Prix Unitaire</th>
                                <th>Remise%</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-header">
                    <h3>Logs</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="input-group col-6 mb-1">
                                <span class="input-group-text">Type</span>
                                <select class="form-control form-select form-select-sm" name="" id="typefilter">
                                    <option value="">Tous</option>
                                    <option value="Cart">Bon de livraison</option>
                                    <option value="OnlineSale">Vente en ligne</option>
                                    <option value="Voucher">Bon d'achat</option>
                                    <option value="Inventory">Inventaire</option>
                                    <option value="Mu">Mouvement de stock</option>
                                    <option value="ReturnNote">Bon de retour</option>
                                    <option value="CreditNote">Facture avoir</option>
                                </select>
                            </div>
                        </div>
                        <table id="log-table" class="table align-middle table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Mouvement</th>
                                    <th>Motif</th>
                                    <th>Quantité</th>
                                    <th>Date</th>
                                    <th>Ajouté par</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/chartjs/js/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/chartjs/js/Chart.extension.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/apexcharts-bundle/js/apexcharts.min.js') }}"></script>
    <script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>

    <script>
        function toMonthName(monthNumber) {
            const date = new Date();
            date.setMonth(monthNumber - 1);

            return date.toLocaleString('fr-FR', {
                month: 'short',
            });
        }

        var options = {
            series: [{
                name: 'Prix de vente',
                data: []
            }, {
                name: 'Prix d\'achat',
                data: []
            },
            {
                name: 'Marge approximative',
                data: []
            }],
            chart: {
                foreColor: '#9ba7b2',
                height: 360,
                type: 'area',
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false
                },
            },
            colors: ['#ffc107', '#e72e7a', '#0c971a'],
            // title: {
            // 	text: 'Area Chart',
            // 	align: 'left',
            // 	style: {
            // 		fontSize: "16px",
            // 		color: '#666'
            // 	}
            // },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                // type: 'datetime',
                categories: []
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            },
        };

        $(document).ready(function() {
            var data = @json($statistics->product_price_evolution);
            data.forEach(element => {
                options.series[0].data.push(element.selling);
                options.series[1].data.push(element.buying);
                options.series[2].data.push(element.margin);
                options.xaxis.categories.push(toMonthName(element.month) + '' + element.year);
            });
            var chart = new ApexCharts(document.querySelector("#product-price-evolution"), options);
            chart.render();

            $(document).on('click', '#refresh_chart', function() {
                var year = $(this).val();
                var product_id = {{ $product->id }};
                var from = $('#from').val();
                var to = $('#to').val();
                if (from && to) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url : `/product/${product_id}/statistic/price/${from}/${to}`,
                        type: "GET",
                        async: false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
                        success: function(response, textStatus, jqXHR) {
                            let selling = response.data.map(element => {
                                return {
                                    x: toMonthName(element.month) + '' + element.year,
                                    y: element.selling
                                }
                            });
                            let buying = response.data.map(element => {
                                return {
                                    x: toMonthName(element.month) + '' + element.year,
                                    y: element.buying
                                }
                            });
                            let margin = response.data.map(element => {
                                return {
                                    x: toMonthName(element.month) + '' + element.year,
                                    y: element.margin
                                }
                            });
                            chart.updateSeries([
                                {
                                    name: 'Prix de vente',
                                    data: selling
                                },
                                {
                                    name: 'Prix d\'achat',
                                    data: buying
                                },
                                {
                                    name: 'Marge approximative',
                                    data: margin
                                }
                        ]);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: "Veuillez saisir des dates valides",
                        showConfirmButton: true,
                    })
                }
            }).change();
        });
    </script>


<script>
    $(document).ready(function() {
            var base_url = '{{url(' / ')}}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Tous"] ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ sur _PAGES_",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.show' , $product->id) }}",
                aoColumns: [
                    {
                        data: 'type',
                        name: 'type'
                    },
                    {
                        data: 'codification',
                        name: 'codification'
                    },
                    {
                        data: 'date',
                        name: 'date'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity'
                    },
                    {
                        data: 'product_price',
                        name: 'product_price'
                    },
                    {
                        data: 'product_remise',
                        name: 'product_remise'
                    },
                    {
                        data: 'total',
                        name: 'total'
                    },
                    // {
                    //     data: 'action',
                    //     name: 'action',
                    //     orderable: false,
                    //     paging: false,
                    //     searchable: false,
                    //     bSearchable: false,
                    //     exportable: false
                    // },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            var log_table = $('#log-table').DataTable({
                "order": [
                    [3, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                pageLength: 50,
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                createdRow: function(row, data, dataIndex) {
                    $(row).find('td:eq(2)').attr('style', "text-align: center; font-weight:bold;")
                },
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.12.1/i18n/fr-FR.json"
                },
                ajax: "{{ route('admin.product.show', ['product' => $product->id, 'table' => 'log']) }}",
                aoColumns: [
                    {
                        data: 'type',
                        name: 'type'
                    },
                    {
                        data: 'motif',
                        name: 'motif'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        render: function(data) {
                            var date = new Date(data);
                            var formattedDate = date.toLocaleString('Fr-Fr', {
                                year: 'numeric',
                                month: '2-digit',
                                day: '2-digit',
                                hour: '2-digit',
                                minute: '2-digit',
                                second: '2-digit'
                            });
                            return formattedDate;
                        }
                    },

                    {
                        data: 'user_name',
                        name: 'user_name'
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            var filters = {};

            function filter() {
                var queryString = '&' + new URLSearchParams(filters).toString();
                var url = @json(route('admin.product.show', ['product' => $product->id, 'table' => 'log']));
                log_table.ajax.url(url + queryString).load();
                log_table.draw();
            }

            $(document).on('change', '#typefilter', function(e) {
                var value = $(this).val();
                filters['type'] = value;
                filter();
            });

            });
</script>

@endsection
