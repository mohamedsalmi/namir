@extends('admin.layouts.app')

@section('title', 'Liste des articles')

@section('stylesheets')
    <link href="{{url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css')}} " rel="stylesheet">
    <link href="{{url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css')}} " rel="stylesheet">
    <link href="{{url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css')}} " rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery.range.css')}}">
    <link href="{{ asset('assets/admin/plugins/lc_lightbox/css/lc_lightbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/lc_lightbox/skins/light.css') }}" rel="stylesheet" />
    <style>
        .product-img {
            /* display: block; */
            max-width: 60px;
            max-height: 60px;
            width: auto;
            height: auto;
        }

        td{
            padding: 2px !important;
        }

        #container {
            overflow: auto;
        }
        #slider-div {
            display: flex;
            flex-direction: row;
            margin-top: 30px;
        }

        #slider-div>div {
            margin: 8px;
        }

        .slider-label {
            position: absolute;
            background-color: #eee;
            padding: 4px;
            font-size: 0.75rem;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding : 0px;
            margin-left: 0px;
            display: inline;
            border: 0px;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 0px;
            border-style: none;


        }
        .page-link{
            display: inline;
        }
    </style>
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Articles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des articles</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center">
                    <h5 class="mb-2 mb-sm-0">Liste des articles</h5>

                    <div class="ms-auto">
                        <a class="btn btn-success " href="{{ url('/admin/product/importation') }}"><i class="bi bi-file-earmark-excel"></i>Importer Excel</a>
                        @canany(['Ajouter article'])
                            <a class="btn btn-primary" href="{{route('admin.product.create')}}"><i class="fa fa-plus" aria-hidden="true" title="Ajouter"></i>Ajouter un article</a>
                        @endcanany
                        <div class="btn-group ">
                            <button type="button" class="btn btn-outline-primary " data-bs-toggle="dropdown" aria-expanded="false">
                                Actions multiples
                            </button><span class="visually-hidden">Toggle Dropdown</span>
                            <ul class="dropdown-menu" style="">
                                {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.product.merge', 'method'=>'POST', 'id'=>'merge-product-form')) !!}
                                <li><button type="button" id="merge" name="merge" class="merge dropdown-item" >Merge</button></li>
                                {!! Form::close() !!}

                                {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.product.multipledelete', 'method'=>'POST', 'id'=>'multipledelete-product-form')) !!}
                                <li>
                                    <button type="submit" id="delete" name="delete" value="1" class="dropdown-item show_confirm">Suppression multiple</button>
                                </li>
                                {!! Form::close() !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table align-middle table-striped dataTable" style="width:100%">
                        <thead>
                        <tr>
                           <th style="width:10px;    padding-left: 10px; iportant!">
                               <div class="form-check" ><input type="checkbox" class="form-check-input" id="check-all"></div>
                           </th>
                             <th class="is_filter">ID</th>
                            <th class="is_filter">Nom</th>
                            <th class="is_filter">Nom d'auteur</th>
                            <th class="is_filter">Maison</th>
                            <th class="is_filter">SKU</th>
                            <th class="is_filter2">Etat</th>
                            <th class="is_filter">Qté</th>
                            <th class="is_filter">Prix Détail</th>
                            <th class="is_filter">Prix SemiGros</th>
                            <th width="100px">Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>


            </div>
        </div>

        {{--Template--}}
        <script type="text/template" id="itemsList">
            <div class="row">
                <div class="col-md-2">
                    <label class="form-label">Attribut</label>
                    {!! Form::select('attributes[{?}][attribute_id]', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Attribute(), 'name',true), old('attributes.{?}.attribute_id', ''), ['id' => 'attribute_id-{?}', 'data-index' => '{?}', 'class' => $errors->has('attributes.{?}.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id filter-field' : 'form-control form-control-sm attribute-id filter-field', 'required']) !!}

                    @error('attributes.{?}.attribute_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-2">
                    <label class="form-label">Valeurs</label>
                    <input class="form-control form-control-sm filter-field attribute_values" name="attributes[{?}][attribute_values]" id = 'attribute_values-{?}'>
                    @error('attributes.{?}.attribute_values')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-2 justify-content-around d-flex flex-column">
                    <label class="form-label"></label>
                    <div>
                        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-x-lg"></i></button>
                    </div>
                </div>
            </div>
        </script>

    </main>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/vfs_fonts.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/form-select2.js')}}"></script>

    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script> -->
    <script src="{{asset('assets/admin/js/jquery.range.min.js')}}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>

    <script type="text/javascript">
        var filter = {};

        //Price range input
        const setLabel = (lbl, val) => {
            const label = $(`#slider-${lbl}-label`);
            label.text(val);
            const slider = $(`#slider-div .${lbl}-slider-handle`);
            const rect = slider[0].getBoundingClientRect();
            label.offset({
                top: rect.top - 30,
                left: rect.left
            });
        }

        const setLabels = (values) => {
            setLabel("min", values[0]);
            setLabel("max", values[1]);
        }
        //Price range input

        function onFieldChange(e, fieldName){
            filter.fieldName = e.target.value;
        }
        $(document).ready(function() {
            var base_url = '{{url(' / ')}}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [1, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Tous"] ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: false,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.index') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(8)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [
                    {
                        data: 'check',
                        name: 'check',
                        orderable: false,
                    },
                    {
                        data: 'id',
                        name: 'id',
                        width:'5px',
                        sortable: true,
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'writer',
                        name: 'writer'
                    },
                    {
                        data: 'publisher',
                        name: 'publisher'
                    },
                    {
                        data: 'sku',
                        name: 'sku'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                        orderable: true,
                    },
                    {
                        data: 'selling1',
                        name: 'selling1'
                    },
                    {
                        data: 'selling2',
                        name: 'selling2'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            $('div.toolbar').html(`<div class="accordion mt-2" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse">
                        Filtres <i class="bi bi-filter"></i>
                    </button>
                </h2>
                <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body p-2">
                        <form id="filter-form">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Nom</label>
                                    <input type="text" name="name" class="form-control form-control-sm filter-field" required="required" placeholder="Nom" id="name">
                                </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">SKU</label>
                                    <input type="text" name="sku" class="form-control form-control-sm filter-field" required="required" placeholder="SKU" id="sku">
                                </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Etat</label>
                                    <select name="status" class="form-control form-control-sm filter-field" id="status">
                                        <option value="all" >Tous</option>
                                        <option selected value="1">Activé</option>
                                        <option value="0">Désactivé</option>
                                    </select>
                                </div>

                                <div class="form-group col-xs-12 col-md-6">
                                    <label class="form-label">Catégories</label>
                                    <select name="categories" id="categories" multiple="multiple" class="form-control form-control-sm form-select multiple-select filter-field">
                                        @foreach ($categories as $cat)
                    @include('admin.partials.subcategories', ['cat' => $cat])
                    @endforeach
                </select>
            </div>

            <div class="form-group col-xs-12 col-md-6" id="slider-outer-div">
                <label class="form-label">Prix</label>
                <div class="mt-2" id="slider-div">
                <div>
                    <input type="hidden" name="price" class="filter-field slider-input " value="{{$max_price}}" />
                                    </div>
                                    </div>
                                </div>

                                <div class="ms-auto mt-2">
                                    <button type="button" title="Ajouter un filtre" class="btn btn-sm text-warning" id="addItem" onclick="return 0;"><i class="bi bi-plus-lg"></i>
                                    </button>
                                </div>
                                <div class="repeat_items">
                                </div>

                                <div class=" col-sm-12 col-md-4 mt-2 float-left">
                                    <button class="btn btn-primary" id="filter">Appliquer les filtres</button>
                                    {{--<button type="button" class="btn btn-secondary" id="reset">Réinitialiser</button>--}}
                </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>`);

            $('.slider-input').jRange({
                from: 0,
                to: {{$max_price ?? 0}},
                step: 1,
                // scale: [0,25,50,75,100],
                format: '%s',
                width: 250,
                showLabels: true,
                isRange : true,
                theme : 'theme-blue'
            });

            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('name');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
            });

            $('#filter').on('click', function(e) {
                const queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.product.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                e.preventDefault();
            });

            $('#reset').on('click', function(e) {
                // e.preventDefault();
                var max_price = @json($max_price);
                $('.filter-field').val("");
                // $('#select2_example').empty();
                // $('#categories').empty();
                $('.slider-input').jRange('setValue', '0,' + max_price);
                $("#categories").select2("val", "");
                // $('#filter-form').trigger("reset");
                // $("#filter-form").reset()
                // const queryString = '?' + new URLSearchParams(filter).toString();
                // var url = @json(route('admin.product.index'));
                // table.ajax.url(url + queryString).load();
                // table.draw();
            });

            $(".multiple-select").select2({ width: '100%' });

            $('#ex2').slider().on('slide', function(ev) {
                setLabels(ev.value);
            });
            setLabels($('#ex2').attr("data-value").split(","));
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $(".repeat_items").repeatable({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 0,
                template: "#itemsList",
                itemContainer: ".row",
                total: $(".repeat_items").find(".row").length || 0 ,
                afterAdd: function (item) {
                    $("#attribute_values-1").select2({ width: '100%' }).trigger('change');
                },
                beforeDelete: function (item) {
                    var selects = item.find('select');
                    selects.each(function () {
                        var name = $(this).attr("name");
                        delete filter[name]
                    })
                },
                afterDelete: function () {
                }
            });
        })

        function filterAttributes(){
            $('option').prop('disabled', false); //reset all the disabled options on every change event
            $('.attribute-id').each(function() { //loop through all the select elements
                var val = this.value;
                $('.attribute-id').not(this).find('option').filter(function() { //filter option elements having value as selected option
                    return this.value === val;
                }).prop('disabled', true); //disable those option elements
            });
        }

        //         $(document).on('change', '.attribute-id', function() {

        // filterAttributes();

        // var val = $(this).val();
        // var attributes = @json($attrs);
        // var index = $(this).attr('data-index');

        // if(val != '')
        // {
        //   let selectedAttribute = attributes.find(e => e.id == val);
        //   $('#attribute_values-' + index).val(null).empty();
        //   selectedAttribute.values.forEach(function(element) {

        //     var data = {
        //         id: element.id ,
        //         text: element.value
        //     };

        //     var newOption = new Option(data.text, data.id, false, false);

        //     $('#attribute_values-' + index).append(newOption).trigger('change');
        //   });
        // }
        // else{
        //   $('#attribute_values-' + index).val(null).empty();
        // }
        // }).change();
    </script>

    <script type="text/javascript">
        $(document).on('click', '#addItem', function() {
            $(".multiple-select").select2({ width: '100%' });
            filterAttributes();
            var attrs = @json($attrs);
            var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if(attributesCount == totalItems){
                $(`#addItem`).hide();
            }
//       $(document).on('select2:opening','.attribute_values', function(e) {
//             let id = $(this).attr('data-select2-id').replace(/attribute_values-/, '');
//             let attribute_id = $('#attribute_id-' + id).children("option:selected").val();
//             var $searchfield = $(this).parent().find('.select2-search__field');
//             console.log($searchfield);

//             $searchfield.on('keyup', function(e) {
//             var query = $(this).val();
//             console.log(query)
//             let l = query.length;
//                 if (l > 3) {
//                     $.ajax({
//                         headers: {
//                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         url: '/product/getAttributeValue',
//                         method: "GET",
//                         data: {
//                             attribute_id: attribute_id,
//                             query: query,
//                         },
//                         success: function(data) {
//                             console.log(data.values);
//                             data.values.forEach(function(val, i) {
//                                 var data = {
//                                     id: val.id,
//                                     text: val.value
//                                 };
//                                 var newOption = new Option(data.text, data.id, false, false);
//                                 console.log(newOption);
//                                 $('#attribute_values-' + (id)).append(newOption).trigger('change');
//                                 console.log($('#attribute_values-' + (id)).val())
//                             });                            

//                         }
//                     });
//                 }
//                 console.log(attribute_id);
//             });
// });
        });

        $(document).on('click', '#deleteItem', function() {
            $(".multiple-select").select2({ width: '100%' });
            filterAttributes();
            var attrs = @json($attrs);
            var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if(totalItems < attributesCount){
                $(`#addItem`).show();
            }
        });
    </script>
    <script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 1500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '#show_confirm, .show_confirm', function(event) {
            event.preventDefault();
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });

        $(document).on('click','#merge', function(event) {
            event.preventDefault();
            var form =  $(this).closest("#merge-product-form");
            var product_id = new Array();
            $("input:checkbox[name=product_id]:checked").each(function(){
                product_id.push($(this).val());
                var input = $("<input>").attr("type", 'hidden').attr("name","product_id[]").val($(this).val());
                form.append(input);
            });
            console.log(product_id);
            console.log(form);
            if(product_id.length<2){
                Swal.fire({
                    title: 'Remarque',
                    text: "Vous devez sélectionner au moins 2 produits !",
                    icon: 'warning',
                    confirmButtonClass: "btn btn-confirm mt-2"

                })
            }  else{
                form.submit();
            }
        });





        $(document).ready(function() {
            $('#check-all').click(function() {
                var checked = this.checked;
                $('input[type="checkbox"]').each(function() {
                    this.checked = checked;
                });
            })

            lc_lightbox('.elem', {
                wrap_class: 'lcl_fade_oc',
                gallery: true,
                //  thumb_attr: 'data-lcl-thumb', 
                fading_time: 100,
                skin: 'light',
                slideshow_time: 100,
                radius: 0,
                padding: 0,
                border_w: 0,
            });
        });
    </script>

    <script>

        function attrValue(id,query){

        }

    </script>

<script>
    $('#multipledelete-product-form').submit(function(e) {
        $('input[name="product_id"]').each(function() {
            if (this.checked) {
                $('#multipledelete-product-form').append('<input type="hidden" name="product_id[]" value="' + $(
                    this).val() + '" /> ');
            }
        });
        return true;
    });
</script>
@endsection