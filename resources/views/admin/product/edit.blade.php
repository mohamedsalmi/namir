@extends('admin.layouts.app')

@section('title', 'Modifier un article')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/lc_lightbox/css/lc_lightbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/lc_lightbox/skins/light.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/summernote/summernote-lite.min.css') }}" rel="stylesheet">
    <style>
        .product-img {
            /* display: block; */
            max-width: 100px;
            max-height: 90px;
            width: auto;
            height: auto;
        }

        #container {
            overflow: auto;
        }

        /* .image { width:100px;height:100px;float:left;position:relative;margin-right: 3px} */
        /* a.delete { display:none;position:absolute;top:0;right:0;color:red; z-index: 9999; }
                                                    a.default { display:none;position:absolute;top:0;left:30;color:rgb(27, 170, 27); z-index: 9999; }
                                                    .buttons{
                                                      padding: 3px 3px;
                                                    }
                                                    .buttons:hover a.delete { display:block; }
                                                    .buttons:hover a.default { display:block; } */
    </style>

@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Articles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.product.index') }}">Liste des articles</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier un article</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <h5 class="mb-0">Modifier un article</h5>
                    </div>
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => ['admin.product.update', $product->id],
                                'method' => 'PUT',
                                'id' => 'create-product-form',
                                'class' => 'row g-3',
                            ]) !!}
                            <div class=" col-lg-12 required">
                                <div class="row col-lg-6">
                                    <div class="col-lg-6 required">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="activation" type="checkbox"
                                                {{ $product->status == '1' ? 'checked' : '' }}>
                                            <label class="form-label" for="exampleFormControlInput1"><i
                                                    class="fa fa-tag"></i> Activé </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 required">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="by_order" type="checkbox"
                                                {{ $product->by_order == '1' ? 'checked' : '' }}>
                                            <label class="form-label" for="exampleFormControlInput1"><i
                                                    class="fa fa-tag"></i> Par ordre </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                <label class="form-label">Nom produit</label>
                                <input name="name" type="text"
                                    class="form-control form-control-sm @error('name') is-invalid @enderror"
                                    placeholder="Nom produit" value="{{ old('name', $product->name) }}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            {{-- <div class="col-6 required">
                          <label class="form-label">Type prix</label>
                          {{ Form::select('price_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.price_types') , old('price_type', $product->price_type), ['id'=> 'price_type', 'onchange' =>'onTypeChange(event)', 'name'=> 'price_type[type]', 'class' => $errors->has('price_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('price_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                        <div class="col-6 required">
                          <label class="form-label">Type de quantité</label>
                          {{ Form::select('quantity_type', \App\Helpers\Helper::makeDropDownListFromConfig('stock.quantity_types') , old('quantity_type', $product->quantity_type), ['id'=> 'quantity_type', 'onchange' =>'onTypeChange(event)', 'name'=> 'quantity_type[type]', 'class' => $errors->has('quantity_type') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                @error('quantity_type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div> --}}
                            <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                <label class="form-label">Devise</label>
                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), old('currency_id', $product->currency_id), ['id' => 'currency_id', 'name' => 'currency_id', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                @error('currency_id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                <label class="form-label">Unité</label>
                                {{ Form::select('unity', \App\Helpers\Helper::makeDropDownListFromConfig('stock.unities'), old('unity', $product->unity), ['id' => 'unity', 'name' => 'unity', 'class' => $errors->has('unity') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                @error('unity')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12 required">
                                <label class="form-label">TVA</label>
                                <input name="tva" type="number" required required min="0" max="100"
                                    pattern="^([0-9][0-9]?|)$"class="form-control form-control-sm @error('tva') is-invalid @enderror"
                                    placeholder="TVA (entre 0 et 100)" value="{{ old('tva', $product->tva) }}">
                                @error('tva')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-3 col-sm-12 required duplicated">
                                <label class="form-label">Prix d'achat</label>
                                <input name="buying" type="number" step="0.001" required min="0"
                                    pattern="^([0-9][0-9]?|)$" title=""
                                    class="form-control form-control-sm @error('buying') is-invalid @enderror"
                                    placeholder="Prix" value="{{ old('buying', $product->buying) }}">
                                @error('buying')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                           
                            <div class="col-lg-3 col-sm-12 required duplicated">
                                <label class="form-label">Prix détails</label>
                                <input name="selling1" type="number" step="0.001" required pattern="^([0-9][0-9]?|)$"
                                    title=""
                                    class="form-control form-control-sm @error('selling1') is-invalid @enderror"
                                    placeholder="Prix détails" value="{{ old('selling1', $product->selling1) }}">
                                @error('selling1')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            <div class="col-lg-3 col-sm-12 required duplicated ">
                                <label class="form-label">Prix semi gros</label>
                                <input name="selling2" type="number" step="0.001" required pattern="^([0-9][0-9]?|)$"
                                    title=""
                                    class="form-control form-control-sm @error('selling2') is-invalid @enderror"
                                    placeholder="Prix semi-gros" value="{{ old('selling2', $product->selling2) }}">
                                @error('selling2')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                          

                            <div class="col-lg-3 col-sm-6 col-xs-12 required duplicated">
                                <label class="form-label">Quantité minimale</label>
                                <input name="minimum_quantity" type="number" required min="0"
                                    pattern="^([0-9][0-9]?|)$" title=""
                                    class="form-control form-control-sm @error('minimum_quantity') is-invalid @enderror"
                                    placeholder="Prix" value="{{ old('minimum_quantity', $product->minimum_quantity) }}">
                                @error('minimum_quantity')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-3 col-sm-6 col-xs-12 required duplicated">
                                <label class="form-label">Quantité réserve</label>
                                <input name="reserve_quantity" type="number" required min="0"
                                    pattern="^([0-9][0-9]?|)$" title=""
                                    class="form-control form-control-sm @error('reserve_quantity') is-invalid @enderror"
                                    placeholder="Prix" value="{{ old('reserve_quantity', $product->reserve_quantity) }}">
                                @error('reserve_quantity')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">Référence</label>
                                <input name="reference" type="text"
                                    class="form-control form-control-sm @error('reference') is-invalid @enderror"
                                    placeholder="Référence" value="{{ old('reference', $product->reference) }}">
                                @error('reference')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">SKU (Code à barre)</label>
                                <div class="input-group mb-3">
                                    <input name="sku" type="text"
                                        class="form-control form-control-sm @error('sku') is-invalid @enderror"
                                        placeholder="SKU" value="{{ old('sku', $product->sku) }}">
                                    <button data-bs-toggle="modal" data-bs-target="#camera" id='barcode'
                                        type="button"
                                        class="btn btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                </div>
                                @error('sku')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Scanner code à bare</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="qr-reader"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">Remise </label>
                                <input name="discount" type="text"
                                    class="form-control form-control-sm @error('discount') is-invalid @enderror"
                                    placeholder="discount" value="{{ old('discount', $product->discount) }}">
                                @error('discount')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">Remise max</label>
                                <input name="maxdiscount" type="text"
                                    class="form-control form-control-sm @error('maxdiscount') is-invalid @enderror"
                                    placeholder="maxdiscount" value="{{ old('maxdiscount', $product->maxdiscount) }}">
                                @error('maxdiscount')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">Catégories</label>
                                {{ Form::select('categories', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Category(), 'name'), old('categories', $product->categories), ['id' => 'categories', 'name' => 'categories[]', 'class' => $errors->has('categories') ? 'form-control form-control-sm multiple-select is-invalid' : 'form-control form-control-sm multiple-select', 'multiple' => 'multiple']) }}
                                @error('categories')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12 ">
                                <label class="form-label">offers</label>
                                {!! Form::select(
                                    'offers_values',
                                    \App\Helpers\Helper::makeDropDownListFromCollection($offers, 'value', true),
                                    old('offers_values', ''),
                                    [
                                        'name' => 'offers_values[]',
                                        'id' => 'offers',
                                        'class' => $errors->has('offers_values')
                                            ? 'form-control form-control-sm multiple-select is-invalid'
                                            : 'form-control form-control-sm multiple-select',
                                        'multiple' => 'multiple',
                                    ],
                                ) !!}
                                @error('offers_values')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="col-lg-6 col-sm-12 col-xs-12">
                                <label class="form-label">Images</label>
                                <input class="form-control form-control-sm" type="file" name="images[]" multiple>
                            </div>

                            <div class="col-lg-6 col-sm-12">
                                <label class="form-label">Description</label>
                                <textarea name="description"
                                    class="summernote form-control form-control-sm @error('description') is-invalid @enderror"
                                    placeholder="Description">{{ old('description', $product->description) }}</textarea>
                                @error('description')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label class="form-label">Résumé</label>
                                <textarea name="resume" class="form-control form-control-sm @error('resume') is-invalid @enderror"
                                    placeholder="Résumé">{{ old('resume', $product->resume) }}</textarea>
                                @error('resume')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label class="form-label">Meta Title</label>
                                <textarea name="metatitle" class="form-control form-control-sm @error('metatitle') is-invalid @enderror"
                                    placeholder="metatitle">{{ old('metatitle', $product->metatitle) }}</textarea>
                                @error('metatitle')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label class="form-label">Meta Keywords</label>
                                <textarea name="metakeys" class="form-control form-control-sm @error('metakeys') is-invalid @enderror"
                                    placeholder="metakeys">{{ old('metakeys', $product->metakeys) }}</textarea>
                                @error('metakeys')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label class="form-label">Meta Description</label>
                                <textarea name="metadescription" class="form-control form-control-sm @error('metadescription') is-invalid @enderror"
                                    placeholder="metadescription">{{ old('metadescription', $product->metadescription) }}</textarea>
                                @error('metadescription')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div id="container" class="col-12 image-set d-flex align-items-center">
                                @foreach ($product->images as $image)
                                    <div class="text-center m-1 image">
                                        <a class="elem " href="{{ $image->url }}"
                                            data-lcl-thumb="{{ $image->url }}">
                                            <img class="img-fluid img-thumbnail product-img @if ($image->is_default) border-success @endif"
                                                src="{{ $image->url }}" alt="{{ $product->name }}">
                                        </a>
                                        <div class="buttons d-flex align-items-center justify-content-center gap-2 mt-3">
                                            <a data-id="{{ $image->id }}" title="Définir comme image par défaut"
                                                class="btn btn-sm text-success default"><i
                                                    class="bi bi-check-circle"></i></a>
                                            <a data-id="{{ $image->id }}" title="Supprimer"
                                                class="btn btn-sm text-danger delete"><i class="bi bi-trash"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header py-3 bg-transparent">
                                        <div class="d-sm-flex align-items-center">
                                            <h5 class="mb-2 mb-sm-0">Les attributs</h5>
                                            <div class="ms-auto">
                                                <button role="button" class="btn btn-sm btn-warning" id="addItem"
                                                    onclick="return 0;"><i class="bi bi-plus"></i> Ajouter un attribut
                                                </button>
                                                <button role="button" type="button" class="btn btn-sm btn-info"
                                                    id="addItemLivre" onclick="return 0;"><i class="bi bi-plus"></i>
                                                    Ajouter les attributs de livre
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @error('attributes')
                                            <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                <div class="d-flex align-items-center">
                                                    <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                                                    </div>
                                                    <div class="ms-3">
                                                        <div class="text-danger">{{ $message }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        @enderror
                                        <div class="repeat_items">
                                            @php
                                                $hasOldattributes = old('attributes', null);
                                                $attributes = old(
                                                    'attributes',
                                                    $product
                                                        ->productAttributes()
                                                        ->whereHas('attribute', function ($query) {
                                                            $query->whereNot('name', 'الطبعة')->whereNot('name', 'العروض');
                                                        })
                                                        ->get()
                                                        ->load('values.attributeValue'),
                                                );
                                                $noattributes = is_array($attributes) ? count($attributes) : 0;
                                                
                                                if (isset($attributes)) {
                                                    if (!is_array($attributes)) {
                                                        $noattributes = count(json_decode($attributes, true));
                                                        $attributes = json_decode($attributes);
                                                    }
                                                } else {
                                                    $attributes = [];
                                                }
                                            @endphp

                                            @if ($noattributes > 0)
                                                @foreach ($attributes as $key => $item)
                                                    <div class="row">
                                                        <div class="col-md-5 required">
                                                            <label class="form-label">Attribut</label>
                                                            {!! Form::select(
                                                                'attributes[' . $key . '][attribute_id]',
                                                                \App\Helpers\Helper::makeDropDownListFromModelExcept(new \App\Models\Attribute(), 'name', ['الطبعة', 'العروض']),
                                                                old('attributes.' . $key . '.attribute_id', is_object($item) ? $item->attribute_id : ''),
                                                                [
                                                                    'id' => 'attribute_id-' . $key,
                                                                    'data-index' => $key,
                                                                    'class' => $errors->has('attributes.' . $key . '.attribute_id')
                                                                        ? 'form-control form-control-sm is-invalid attribute-id'
                                                                        : 'form-control form-control-sm attribute-id',
                                                                ],
                                                            ) !!}
                                                            @error('attributes.' . $key . '.attribute_id')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <div class="col-md-6 required">
                                                            <label class="form-label">Valeurs</label>
                                                            <input required id="'attribute_values-'{{ $key }}"
                                                                name="attributes[{{ $key }}][attribute_values]"
                                                                class='form-control form-control-sm  attribute_values @error('metakeys') is-invalid @enderror'
                                                                value="{{ !empty($item?->values) ? $item?->values[0]->attribute_value->value : '' }}">

                                                            @error('attributes.' . $key . '.attribute_values')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <div class="col-md-1 justify-content-around d-flex flex-column">
                                                            <label class="form-label"></label>
                                                            <div>
                                                                <button id="deleteItem" type="button"
                                                                    class="btn btn-danger" title="Supprimer"><i
                                                                        class="bi bi-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->

    </main>
@endsection


{{-- Template --}}
<script type="text/template" id="itemsList">
  <div class="row">
    <div class="col-md-5 required">
      <label class="form-label">Attribut</label>
      {!! Form::select('attributes[{?}][attribute_id]', \App\Helpers\Helper::makeDropDownListFromModelExcept(new \App\Models\Attribute(), 'name', ['الطبعة','العروض']), old('attributes.{?}.attribute_id', ''), ['id' => 'attribute_id-{?}', 'data-index' => '{?}', 'class' => $errors->has('attributes.{?}.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id' : 'form-control form-control-sm attribute-id']) !!}

      @error('attributes.{?}.attribute_id')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-6 required">
      <label class="form-label">Valeurs</label>
      <input required id='attribute_values-{?}' name="attributes[{?}][attribute_values]" class ='form-control form-control-sm attribute_values @error('metakeys') is-invalid @enderror'>
      @error('attributes.{?}.attribute_values')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn btn-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>
  </div>
</script>


@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-custom.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/html5-qrcode/html5-qrcode.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-lite.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>
    <script>
        $(document).keypress(
            function(event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $(document).on('change', 'input[name="buying"]', function() {
            var _this = $(this);
            var selling1_coefficient = $('input[name="coef1"]').val();
            var selling2_coefficient = $('input[name="coef2"]').val();
            var selling3_coefficient = $('input[name="coef3"]').val();
            $('input[name="selling1"]').val((_this.val() * (1 + selling1_coefficient / 100)).toFixed(3));
            $('input[name="selling2"]').val((_this.val() * (1 + selling2_coefficient / 100)).toFixed(3));
            $('input[name="selling3"]').val((_this.val() * (1 + selling3_coefficient / 100)).toFixed(3));
        });

        $(document).on('change', 'input[name="coef1"]', function() {
            var _this = $(this);
            var selling1_coefficient = _this.val();
            $('input[name="selling1"]').val(($('input[name="buying"]').val() * (1 + selling1_coefficient / 100))
                .toFixed(3));
        });

        $(document).on('change', 'input[name="coef2"]', function() {
            var _this = $(this);
            var selling2_coefficient = _this.val();
            $('input[name="selling2"]').val(($('input[name="buying"]').val() * (1 + selling2_coefficient / 100))
                .toFixed(3));
        });

        $(document).on('change', 'input[name="coef3"]', function() {
            var _this = $(this);
            var selling3_coefficient = _this.val();
            $('input[name="selling3"]').val(($('input[name="buying"]').val() * (1 + selling3_coefficient / 100))
                .toFixed(3));
        });
        $(document).on('keyup change', 'input[name="selling1"]', function() {
            var _this = $(this);
            var selling1 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef1"]').val((((selling1 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
        $(document).on('keyup change', 'input[name="selling2"]', function() {
            var _this = $(this);
            var selling2 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef2"]').val((((selling2 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
        $(document).on('keyup change', 'input[name="selling3"]', function() {
            var _this = $(this);
            var selling3 = _this.val();
            if ($('input[name="buying"]').val() != '') {
                $('input[name="coef3"]').val((((selling3 / $('input[name="buying"]').val()) - 1) * 100).toFixed(3));
            }
        });
        $(document).ready(function(e) {
            lc_lightbox('.elem', {
                wrap_class: 'lcl_fade_oc',
                gallery: true,
                //  thumb_attr: 'data-lcl-thumb', 
                fading_time: 100,
                skin: 'light',
                slideshow_time: 100,
                radius: 0,
                padding: 0,
                border_w: 0,
            });

            $(document).on('click', 'a.delete', function(e) {
                e.preventDefault();
                var id = $(this).data("id");

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/admin/product/image/" + id,
                    type: "DELETE",
                    // data : data,
                    async: false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
                    success: function(response, textStatus, jqXHR) {
                        $("#container").load(location.href + " #container");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });
            });

            $(document).on('click', 'a.default', function(e) {
                e.preventDefault();
                var id = $(this).data("id");

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/admin/product/default/image/" + id,
                    type: "PUT",
                    async: false,
                    success: function(response, textStatus, jqXHR) {
                        $("#container").load(location.href + " #container");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });
            });

        });
    </script>

    <script type="text/javascript">
        $(function() {
            $(".repeat_items").repeatable({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 0,
                template: "#itemsList",
                itemContainer: ".row",
                total: $(".repeat_items").find(".row").length || 0
            });
        })


        function filterAttributes() {
            $('option').prop('disabled', false); //reset all the disabled options on every change event
            $('.attribute-id').each(function() { //loop through all the select elements
                var val = this.value;
                $('.attribute-id').not(this).find('option').filter(
                    function() { //filter option elements having value as selected option
                        return this.value === val;
                    }).prop('disabled', true); //disable those option elements
            });
        }

        // $(document).on('change', '.attribute-id', function() {

        //     filterAttributes();

        //     var val = $(this).val();
        //     var attributes = @json($attrs);
        //     var index = $(this).attr('data-index');

        //     if (val != '') {
        //         let selectedAttribute = attributes.find(e => e.id == val);
        //         $('#attribute_values-' + index).val(null).empty();
        //         selectedAttribute.values.forEach(function(element) {

        //             var data = {
        //                 id: element.id,
        //                 text: element.value
        //             };

        //             var newOption = new Option(data.text, data.id, false, false);

        //             $('#attribute_values-' + index).append(newOption).trigger('change');
        //         });
        //     } else {
        //         $('#attribute_values-' + index).val(null).empty();
        //     }
        // }).change();
    </script>

    <script type="text/javascript">
        $(document).on('click', '#addItem', function() {
            filterAttributes();
            var attrs = @json($attrs);
            var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if (attributesCount == totalItems) {
                $(`#addItem`).hide();
            }
            $(".multiple-select").select2({
                tags: true,
                width: '100%'
            });
        });

        $(document).on('click', '#addItemLivre', function() {
            var attrs = @json($attrs);
            var attrs_array = ['دار النشر', 'اسم المؤلف', 'المحقق', 'نوع التجليد', 'نوعية الورق', 'عدد المجلدات',
                'عدد الصفحات', 'الوزن',
                'القياس'
            ];
            attrs.forEach(element => {
                if (jQuery.inArray(element.name, attrs_array) !== -1) {
                    let row_id = $(".repeat_items").find(".row").length;
                    let id = parseFloat($(".attribute-id").last().attr("data-index")) + 1;
                    id = isNaN(id) ? 1 : id;
                    $('#addItem').click();
                    if ($($('#attribute_id-' + id).find("option[value='" + element.id + "']").prop(
                            'disabled'))
                        .length < 1) {
                        $('#attribute_id-' + id).val(element.id).change();
                    } else {
                        $('.row').last().remove();
                    }
                }
            });
        });

        $(document).on('click', '#deleteItem', function() {
            filterAttributes();
            var attrs = @json($attrs);
            var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if (totalItems < attributesCount) {
                $(`#addItem`).show();
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".single-select").select2({
                tags: true,
                width: '100%'
            });
            $(".multiple-select").select2({
                width: '100%'
            });

            //-----Start Load product categories-----//
            var categories = @json($product->categories);
            let ids = [];
            categories.forEach(element => {
                ids.push(element.category_id);
            });
            $('#categories').val(ids);
            $('#categories').trigger('change');
            //-----End Load product categories-----//
            //-----Start Load product offers-----//
            var offers = @json($attrs_offers?->where('name', 'العروض')->first()?->values);
            let idss = [];
            offers.forEach(element => {
                idss.push(element.id);
            });
            $('#offers').val(idss);
            $('#offers').trigger('change');
            //-----End Load product offers-----//
            //-----Start Load product attributes-----//
            var attributes = @json($attributes);

            var attrs = @json($attrs);
            var attributesCount = attrs.length;
            var totalItems = $(".repeat_items").find(".row").length || 0;
            if (attributesCount == totalItems) {
                $(`#addItem`).hide();
            }

            let valuesIds = [];

            //------Load old atributes------//
            var hasOldattributes = @json($hasOldattributes);
            if (hasOldattributes) {
                Object.entries(attributes).forEach((element, index) => {
                    var attrs = @json($attrs);
                    // console.log(element);
                    let selectedAttribute = attrs.find(e => e.id == element[1].attribute_id);
                    $('#attribute_values-' + (element[0])).val(null).empty();
                    selectedAttribute.values.forEach(function(val, i) {
                        var data = {
                            id: val.id,
                            text: val.value
                        };
                        var newOption = new Option(data.text, data.id, false, false);

                        $('#attribute_values-' + (element[0])).append(newOption).trigger('change');
                    });
                    element[1].attribute_values.forEach(el => {
                        valuesIds.push(el);
                    });
                    $('#attribute_values-' + (element[0])).val(valuesIds);
                    $('#attribute_values-' + (element[0])).trigger('change');
                });
                //------Load old atributes------//

            } else {
                attributes.forEach((element, index) => {
                    var attrs = @json($attrs);
                    let selectedAttribute = attrs.find(e => e.id == element.attribute_id);
                    // console.log(attrs )
                    // console.log(selectedAttribute)
                    // $('#attribute_values-' + (index)).val(null).empty();
                    selectedAttribute.values.forEach(function(val, i) {

                        var data = {
                            id: val.id,
                            text: val.value
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $('#attribute_values-' + (index)).append(newOption).trigger('change');

                    });
                    element.values.forEach(el => {
                        valuesIds.push(el.attribute_value_id);
                    });
                    $('#attribute_values-' + (index)).val(valuesIds);
                    $('#attribute_values-' + (index)).trigger('change');
                });
            }

            filterAttributes();
            //-----End Load product attributes-----//
        });
    </script>

    <script>
        function onTypeChange(e) {
            var totalItems = $(".repeat_items").find(".row").length || 0;
            switch (e.target.value) {
                case 'Dépend d\'un attribut':
                    if (totalItems < 1) {
                        $('#addItem').trigger('click');
                    }
                    break;
                case 'Dépend de deux attributs':
                    if (totalItems < 2) {
                        for (let index = 0; index < 2 - totalItems; index++) {
                            $('#addItem').trigger('click');
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            $(document).on('select2:opening', '.attribute_values', function(e) {
                let id = $(this).attr('data-select2-id').replace(/attribute_values-/, '');
                let attribute_id = $('#attribute_id-' + id).children("option:selected").val();

                $(document).on('keyup', '.select2-search__field', function(e) {
                    var query = $(this).val();
                    console.log(query)
                    let l = query.length;
                    if (l > 3) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '/admin/product/getAttributeValue',
                            method: "GET",
                            data: {
                                attribute_id: attribute_id,
                                query: query,
                            },
                            success: function(data) {
                                // console.log(data.values);
                                data.values.forEach(function(val, i) {
                                    var data = {
                                        id: val.id,
                                        text: val.value
                                    };
                                    var newOption = new Option(data.text, data
                                        .id, false, false);
                                    // console.log(newOption);
                                    $('#attribute_values-' + (id)).append(
                                        newOption).trigger('change');
                                    // console.log($('#attribute_values-' + (id)).val())
                                });

                            }
                        });
                    }
                    // console.log(attribute_id);
                });
            });
        });
    </script>
@endsection
