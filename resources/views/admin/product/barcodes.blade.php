@extends('admin.layouts.app')

@section('title', 'Modifier un article')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Articles</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.index')}}">Liste des articles</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Modifier les codes à barre</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">

      {!! Form::open(array('enctype'=>'multipart/form-data', 'route' => array('product.updatebarcodes', $product->id), 'method'=>'PUT', 'id'=>'product-barcodes-form', 'class' => 'row g-3')) !!}
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Modifier les codes à barre</h5>
              <div class="ms-auto">
                <button name="Save_barcode" type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>

          <div class="card-body">
            <div class="border p-3 rounded">

          @switch($product->quantity_type['type'])
              @case('Simple')
              <div class="row">
                <div class="col-6 required">
                  <label class="form-label">Code à barre</label>
                <input class="form-control form-control-sm @if(!$product->barcodes()->exists()) text-secondary @endif @error('barcodes.0.barcode') is-invalid @enderror" style="width:auto;" type="text" min="0" name="barcodes[0][barcode]" value="{{old('barcodes.0.barcode', count($product->barcodes) ? $product->barcodes->first()->barcode : $product?->sku)}}">
                @error('barcodes.0.barcode')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
              </div>
              @break
          
              @case("Dépend d'un attribut")
              <div class="table-responsive">
                <table class="table ">
                  <thead>
                    <tr><th>{{$attrs[0]["attribute"]["name"]}}</th>
                    @foreach($attrs[0]["values"] as $val)
                    <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                    @endforeach
                  </tr></thead>
                  <tbody>
                      <tr>
                        <td style="width:20%; background: #ffffff;">
                          <label class="form-label">Codes à barre</label>
                        </td>
                        @foreach($attrs[0]["values"] as $key => $item)
                        <td>
                          <input type="hidden" value="{{$attrs[0]['id']}}" name="barcodes[{{ $key }}][product_attribute1]">
                          <input type="hidden" value="{{$item['id']}}" name="barcodes[{{ $key }}][product_value1]">
                          <label class="form-label">Code à barre</label>
                          <input class="form-control form-control-sm @if(!$product->barcodes()->exists()) text-secondary @endif @error('barcodes.'. $key .'.barcode') is-invalid @enderror" style="width:100px;" type="text" min="0" name="barcodes[{{ $key }}][barcode]" value="{{old('barcodes.' . $key . '.barcode', \App\Helpers\Helper::getProductBarcodeValue($product, 'barcode', $attrs[0]['id'], $item['id']))}}">
                          @error('barcodes.'. $key .'.barcode')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </td>
                          @endforeach
                      </tr>
                                      </tbody>
                </table>
              </div>
                  @break
                  @case("Dépend de deux attributs")
                  <div class="table-responsive">
                    <table class="table ">
                      <thead>
                        <tr><th>{{$attrs[1]["attribute"]["name"]}} / {{$attrs[0]["attribute"]["name"]}}</th>
                        @foreach($attrs[0]["values"] as $val)
                        <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                        @endforeach
                      </tr></thead>
                      <tbody>
                        @foreach($attrs[1]["values"] as $index => $val2)
                          <tr>
                            <td style="width:20%; background:{{ isset($val2['attribute_value']['color']) ? $val2['attribute_value']['color'] : '#ffffff;' }}">{{$val2['attribute_value']['value']}}</td>
                            @foreach($attrs[0]["values"] as $key => $item)
                            <td>
                              <input type="hidden" value="{{$attrs[0]['id']}}" name="barcodes[{{ $key . $index }}][product_attribute1]">
                              <input type="hidden" value="{{$item['id']}}" name="barcodes[{{ $key . $index }}][product_value1]">
                              <input type="hidden" value="{{$attrs[1]['id']}}" name="barcodes[{{ $key . $index }}][product_attribute2]">
                              <input type="hidden" value="{{$val2['id']}}" name="barcodes[{{ $key . $index }}][product_value2]">
                              <label class="form-label">Code à barre</label>
                              <input class="form-control form-control-sm @if(!$product->barcodes()->exists()) text-secondary @endif @error('barcodes.'. $key . $index .'.barcode') is-invalid @enderror" style="width:100px;" type="text" min="0" name="barcodes[{{ $key . $index }}][barcode]" value="{{old('barcodes.' . $key . $index . '.barcode', \App\Helpers\Helper::getProductBarcodeValue($product, 'barcode', $attrs[0]['id'], $item['id'], $attrs[1]['id'], $val2['id']))}}">
                              @error('barcodes.'. $key . $index .'.barcode')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                            </td>
                              @endforeach
                          </tr>
                          @endforeach
                                          </tbody>
                    </table>
                  </div>
                  @break
          
              @default
              <div>Default</div>
          @endswitch

        </div>
        </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>

    {{-- @if($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}

  <!--Array-->

</main>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var attrs = @json($attrs);
  });
</script>

@endsection
