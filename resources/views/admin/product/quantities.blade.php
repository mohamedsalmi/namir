@extends('admin.layouts.app')

@section('title', 'Modifier un article')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Articles</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.index')}}">Liste des articles</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Modifier les quantités</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">

      {!! Form::open(array('enctype'=>'multipart/form-data', 'route' => array('admin.product.updatequantities', $product->id), 'method'=>'PUT', 'id'=>'product-quantities-form', 'class' => 'row g-3')) !!}
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Modifier les quantités</h5>
              <div class="ms-auto">
                <button name="Save_Quantity" type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>

          <div class="card-body">
            <div class="p-1 rounded">

          @switch($product->quantity_type['type'])
              @case('Simple')
              <div class="row">
                <div class="col-6 required">
                  <label class="form-label">Quantité</label>
                <input class="form-control form-control-sm @error('quantities.0.quantity') is-invalid @enderror" style="width:auto;" type="number" min="0" name="quantities[0][quantity]" value="{{old('quantities.0.quantity', count($product->quantities) ? $product->quantities->first()->quantity : '')}}">
                @error('quantities.0.quantity')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
                <div class="col-6">
                  <label class="form-label">Quantité minimale</label>
                  <input class="form-control form-control-sm @error('quantities.0.minimum_quantity') is-invalid @enderror" style="width:auto;" type="number" min="0" name="quantities[0][minimum_quantity]" value="{{old('quantities.0.minimum_quantity', count($product->quantities) ? $product->quantities->first()->minimum_quantity : 0)}}">
                  @error('quantities.0.minimum_quantity')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              @break
          
              @case("Dépend d'un attribut")
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header py-3 bg-transparent">
                    <div class="d-sm-flex align-items-center">
                      <h5 class="mb-2 mb-sm-0">Les Editions</h5>
                      <div class="ms-auto">
                          <button role="button" type="button" class="btn btn-sm btn-warning" id="addItem" onclick="return 0;"><i class="bi bi-plus"></i> Ajouter Edition</button>
                      </div>
                  </div>
                </div>
                 <div class="card-body">
                  @error('attributes')
                  <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                    <div class="d-flex align-items-center">
                      <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                      </div>
                      <div class="ms-3">
                        <div class="text-danger">{{ $message }}</div>
                      </div>
                    </div>
                  </div>
                  @enderror
                  <div class="repeat_items">
                    @php
                        $hasOldquantities = old('quantities', null);
                        $quantities = old('quantities', $product->quantities->where('store_id', auth()->user()->store_id));
                        $noquantities = is_array($quantities) ? count($quantities) : 0;

                        if (isset($quantities)) {
                            if(!is_array($quantities)){
                                $noquantities = count(json_decode($quantities, true));
                                $quantities = json_decode($quantities, true);
                            }
                        } else {
                                $quantities = [];
                               }
                    @endphp

                    @if($noquantities > 0)
                        @foreach($quantities as $key => $item)
                        <div class="row">
                          <div class="col-md-3 required">
                            <label class="form-label">Quantité</label>
                            <input name="quantities[{{$key}}][product_id]" type="hidden" value="{{ $product->id }}">
                            <input  pattern="^([0-9][0-9]?|)$" name="quantities[{{$key}}][quantity]" type="number" 
                                class="form-control form-control-sm @error('quantities.'. ($key) .'.quantity') is-invalid @enderror" placeholder="Quantité" 
                                value="{{old('quantities.'. ($key) .'.quantity', is_array($item) ? $item['quantity'] : '')}}"
                            >
                            @error('quantities.'. ($key) .'.quantity')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                          <div class="col-md-4">
                            <label class="form-label">Edition</label>
                            <select class="form-control form-control-sm" id="edition" name="quantities[{{$key}}][edition]">
                              @foreach ($Editions as $Edition)
                              @php
                              $quantity=App\Models\Quantity::find($item ['id']);
                              $selected=$quantity->productValue1->attributeValue->id;
                              @endphp
                              <option {{ $selected == $Edition->id ? 'selected' : '' }} value="{{ $Edition->id }}" >{{ $Edition->value }} </option>
                              @endforeach
                          </select>
                            @error('quantities.'. ($key) .'.edition')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                      
                         
                          <div class="col-md-1 justify-content-around d-flex flex-column">
                            <label class="form-label"></label>
                            <div>
                              <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
                            </div>
                          </div>
                      
                        </div>

                    @endforeach
                    @else
                    {{-- <span class="text-center">
                      Aucun lot disponible
                    </span> --}}
                    @endif
                 </div>
                 </div>
                </div>
            </div>
              

              {{-- <div class="table-responsive">
                <table class="table ">
                  <thead>
                    <tr><th>{{$attrs[0]["attribute"]["name"]}}</th>
                    @foreach($attrs[0]["values"] as $val)
                    <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                    @endforeach
                  </tr></thead>
                  <tbody>
                      <tr>
                        <td style="width:20%; background: #ffffff;">
                          <label class="form-label">Quantités</label>
                        </td>
                        @foreach($attrs[0]["values"] as $key => $item)
                        <td>
                          <input type="hidden" value="{{$attrs[0]['id']}}" name="quantities[{{ $key }}][product_attribute1]">
                          <input type="hidden" value="{{$item['id']}}" name="quantities[{{ $key }}][product_value1]">
                          <label class="form-label">Quantité</label>
                          <input class="form-control form-control-sm @error('quantities.'. $key .'.quantity') is-invalid @enderror" style="width:100px;" type="number" min="0" name="quantities[{{ $key }}][quantity]" value="{{old('quantities.' . $key . '.quantity', \App\Helpers\Helper::getProductQuantityValue($product->id, 'quantity', $item['id']))}}">
                          @error('quantities.'. $key .'.quantity')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                          <label class="form-label">Quantité minimale</label>
                          <input class="form-control form-control-sm @error('quantities.'. $key .'.minimum_quantity') is-invalid @enderror" style="width:100px;" type="number" min="0" name="quantities[{{$key}}][minimum_quantity]" value="{{old('quantities.'. $key .'.minimum_quantity', \App\Helpers\Helper::getProductQuantityValue($product->id, 'minimum_quantity', $item['id']))}}">
                          @error('quantities.'. $key .'.minimum_quantity')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </td>
                          @endforeach
                      </tr>
                    </tbody>
                </table>
              </div> --}}
                  @break
                  @case("Dépend de deux attributs")
                  <div class="table-responsive">
                    <table class="table ">
                      <thead>
                        <tr><th>{{$attrs[1]["quantitie"]["name"]}} / {{$attrs[0]["quantitie"]["name"]}}</th>
                        @foreach($attrs[0]["values"] as $val)
                        <th style="background:{{ isset($val['attribute_value']['color']) ? $val['attribute_value']['color'] : '#ffffff;' }}">{{$val['attribute_value']['value']}}</th>
                        @endforeach
                      </tr></thead>
                      <tbody>
                        @foreach($attrs[1]["values"] as $index => $val2)
                          <tr>
                            <td style="width:20%; background:{{ isset($val2['attribute_value']['color']) ? $val2['attribute_value']['color'] : '#ffffff;' }}">{{$val2['attribute_value']['value']}}</td>
                            @foreach($attrs[0]["values"] as $key => $item)
                            <td>
                              <input type="hidden" value="{{$attrs[0]['id']}}" name="quantities[{{ $key . $index }}][product_attribute1]">
                              <input type="hidden" value="{{$item['id']}}" name="quantities[{{ $key . $index }}][product_value1]">
                              <input type="hidden" value="{{$attrs[1]['id']}}" name="quantities[{{ $key . $index }}][product_attribute2]">
                              <input type="hidden" value="{{$val2['id']}}" name="quantities[{{ $key . $index }}][product_value2]">
                              <label class="form-label">Quantité</label>
                              <input class="form-control form-control-sm @error('quantities.'. $key . $index .'.quantity') is-invalid @enderror" style="width:100px;" type="number" min="0" name="quantities[{{ $key . $index }}][quantity]" value="{{old('quantities.' . $key . $index . '.quantity', \App\Helpers\Helper::getProductQuantityValue($product->id, 'quantity', $item['id'], $val2['id']))}}">
                              @error('quantities.'. $key . $index .'.quantity')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              <label class="form-label">Quantité minimale</label>
                              <input class="form-control form-control-sm @error('quantities.'. $key . $index .'.minimum_quantity') is-invalid @enderror" style="width:100px;" type="number" min="0" name="quantities[{{$key . $index}}][minimum_quantity]" value="{{old('quantities.'. $key . $index .'.minimum_quantity', \App\Helpers\Helper::getProductQuantityValue($product->id, 'minimum_quantity', $item['id'], $val2['id']))}}">
                              @error('quantities.'. $key . $index .'.minimum_quantity')
                                <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                            </td>
                              @endforeach
                          </tr>
                          @endforeach
                                          </tbody>
                    </table>
                  </div>
                  @break
          
              @default
              <div>Default</div>
          @endswitch

        </div>
        </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>

    {{-- @if($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}

  <!--Array-->

</main>

 {{--Template--}}
 <script type="text/template" id="itemsList">
  <div class="row">
    <div class="col-md-3 required">
      <label class="form-label">Quantité</label>
      <input name="quantities[{?}][product_id]" type="hidden" value="{{ $product->id }}">
      <input  pattern="^([0-9][0-9]?|)$" name="quantities[{?}][quantity]" type="number" 
          class="form-control form-control-sm @error('quantities.{?}.quantity') is-invalid @enderror" placeholder="Quantité" 
          value="{{old('quantities.{?}.quantity', '')}}"
      >
      @error('quantities.{?}.discount')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col-md-4">
      <label class="form-label">Edition</label>
      <select class="form-control form-control-sm" id="edition" name="quantities[{?}][edition]">
        @foreach ($Editions as $Edition)
        <option value="{{ $Edition->id }}" >{{ $Edition->value }} </option>
        @endforeach
    </select>
      @error('quantities.{?}.edition')
      <div class="invalid-feedback">
          {{ $message }}
      </div>
      @enderror
    </div>

    
    <div class="col-md-1 justify-content-around d-flex flex-column">
      <label class="form-label"></label>
      <div>
        <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-trash"></i></button>
      </div>
    </div>

  </div>
</script>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var attrs = @json($attrs);
  });
</script>

<script type="text/javascript">
  $(function () {
      $(".repeat_items").repeatable({
          addTrigger: "#addItem",
          deleteTrigger: "#deleteItem",
          // max: 400,
          min: 1,
          template: "#itemsList",
          itemContainer: ".row",
          total: $(".repeat_items").find(".row").length || 0
      });
  })
</script>

@endsection
