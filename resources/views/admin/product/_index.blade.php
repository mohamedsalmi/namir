@extends('admin.layouts.app')

@section('title', 'Liste des articles')

@section('stylesheets')
<link href="{{url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css')}} " rel="stylesheet">
<link href="{{url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css')}} " rel="stylesheet">
<link href="{{url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css')}} " rel="stylesheet">
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets/admin/css/jquery.range.css')}}">
<style>
    #slider-div {
    display: flex;
    flex-direction: row;
    margin-top: 30px;
    }

    #slider-div>div {
    margin: 8px;
    }

    .slider-label {
    position: absolute;
    background-color: #eee;
    padding: 4px;
    font-size: 0.75rem;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button {
    padding : 0px;
    margin-left: 0px;
    display: inline;
    border: 0px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
    border: 0px;
    border-style: none;


}
.page-link{
    display: inline;
}
</style>
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Articles</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liste des articles</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
    <div class="card">
        <div class="card-header">
            <h5 class="mb-2 mb-sm-0">Filtres & Recherche</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" name="name" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="card">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des articles</h5>
                @canany(['Ajouter article'])
                <div class="ms-auto">
                    <a class="btn btn-primary" href="{{route('admin.product.create')}}"><i class="fa fa-plus" aria-hidden="true" title="Ajouter"></i>Ajouter un article</a>
                </div>
                @endcanany
                <div class="btn-group p-3">
                    <button type="button" class="btn btn-outline-primary " data-bs-toggle="dropdown" aria-expanded="false">
                        Actions multiples
                    </button><span class="visually-hidden">Toggle Dropdown</span>
                    <ul class="dropdown-menu" style="">
                        {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.product.merge', 'method'=>'POST', 'id'=>'merge-product-form')) !!}
                      <li><button type="button" id="merge" name="merge" class="merge dropdown-item" >Merge</button></li>
                        {!! Form::close() !!}

                      <li><button type="submit" id="delete" name="delete" value="1" class="dropdown-item">Supprimer</button>
                      </li>
                    </ul>
                  </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table align-middle table-striped dataTable" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width:10px;    padding-left: 10px; iportant!">
                                <div class="form-check" ><input type="checkbox" class="form-check-input" id="check-all"></div>
                            </th>
                            <th class="is_filter">Nom</th>
                            <th class="is_filter">SKU</th>
                            <th class="is_filter2">Etat</th>
                            <th class="is_filter">Qté</th>
                            <th class="is_filter">Prix</th>
                            <th width="100px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th style="width:10px;    padding-left: 10px; iportant!">
                                <div class="form-check" ><input type="checkbox" class="form-check-input" id="check-all"></div>
                            </th>
                            <th class="is_filter">{{$product->name}}</th>
                            <th class="is_filter">{{$product->sku}}</th>
                            <th class="is_filter2">{{$product->status}}</th>
                            <th class="is_filter">{{$product->quantity}}</th>
                            <th class="is_filter">{{$product->selling1}}</th>
                            <th width="100px">{{$product->status}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$products->links()}}
            </div>


        </div>
    </div>

    {{--Template--}}
    <script type="text/template" id="itemsList">
        <div class="row">
            <div class="col-md-2">
            <label class="form-label">Attribut</label>
            {!! Form::select('attributes[{?}][attribute_id]', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Attribute(), 'name'), old('attributes.{?}.attribute_id', ''), ['id' => 'attribute_id-{?}', 'data-index' => '{?}', 'class' => $errors->has('attributes.{?}.attribute_id') ? 'form-control form-control-sm is-invalid attribute-id filter-field' : 'form-control form-control-sm attribute-id filter-field', 'required']) !!}

            @error('attributes.{?}.attribute_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
            </div>
            <div class="col-md-2">
            <label class="form-label">Valeurs</label>
            {!! Form::select('attributes[{?}][attribute_values][]', [], old('attributes.{?}.attribute_values', ''), ['id' => 'attribute_values-{?}', 'class' => $errors->has('attributes.{?}.attribute_values') ? 'form-control form-control-sm multiple-select is-invalid filter-field' : 'form-control form-control-sm multiple-select filter-field' , 'multiple' => 'multiple', 'required', 'size' => '3']) !!}
            @error('attributes.{?}.attribute_values')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
            </div>
            <div class="col-md-2 justify-content-around d-flex flex-column">
            <label class="form-label"></label>
            <div>
                <button id="deleteItem" type="button" class="btn text-danger" title="Supprimer"><i class="bi bi-x-lg"></i></button>
            </div>
            </div>
        </div>
    </script>

</main>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/vfs_fonts.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/form-repeater.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/form-select2.js')}}"></script>

  <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script> -->
  <script src="{{asset('assets/admin/js/jquery.range.min.js')}}"></script>

<script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

@if (Session::has('success'))
<script>
    $(function() {
        Swal.fire({
            icon: 'success',
            title: "{{ Session::get('success') }}",
            showConfirmButton: false,
            timer: 500
        })
    })
</script>

{{ Session::forget('success') }}
{{ Session::save() }}
@endif

@if (Session::has('error'))
<script>
    $(function() {
        Swal.fire({
            icon: 'error',
            title: "{{ Session::get('error') }}",
            showConfirmButton: false,
            timer: 500
        })
    })
</script>
@endif

<script type="text/javascript">
    $(document).on('click', '#show_confirm', function(event) {
        event.preventDefault();
         var form =  $(this).closest("form");
         var name = $(this).data("name");
         Swal.fire({
            title: 'Êtes-vous sûr?',
            text: "Vous ne pourrez pas revenir en arrière !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        });
     });
     $(document).on('click','#merge', function(event) {
        event.preventDefault();
        var form =  $(this).closest("#merge-product-form");
         var product_id = new Array();
         $("input:checkbox[name=product_id]:checked").each(function(){
             product_id.push($(this).val());
             var input = $("<input>").attr("type", 'hidden').attr("name","product_id[]").val($(this).val());
             form.append(input);
            });
            console.log(product_id);
            console.log(form);
         if(product_id.length<2){
            Swal.fire({
            title: 'Remarque',
            text: "Vous devez sélectionner au moins 2 produits !",
            icon: 'warning',
            confirmButtonClass: "btn btn-confirm mt-2"
        
        })
         }  else{
                form.submit();
            }   
        });
  



        
     $(document).ready(function() {
            $('#check-all').click(function() {
                var checked = this.checked;
                $('input[type="checkbox"]').each(function() {
                    this.checked = checked;
                });
            })
        });
</script>

@endsection