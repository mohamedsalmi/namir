@extends('admin.layouts.app')

@section('title', 'merge articles')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
      <div class="breadcrumb-title pe-3">Arcticles</div>
      <div class="ps-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Articles à merger</li>
          </ol>
        </nav>
      </div>
 
    </div>
    <!--end breadcrumb-->


    <hr/>
    <div class="alert border-0 bg-light-info alert-dismissible fade show py-2">
      <div class="d-flex align-items-center">
        <div class="fs-3 text-info"><i class="bi bi-info-circle-fill"></i>
        </div>
        <div class="ms-3">
          <div class="text-info">Attention!</div>
          <div class="text-info">Aprés avoir sélectionner un produit :<br>-Les autres vont etre supprimer . <br>-Les quantités vont etre concaténer<br>-Tous les traces des produits supprimés vont etre ajouter au produit selectionner</div>
        </div>
      </div>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <div class="row row-cols-1 row-cols-lg-4 gx-0">
        @foreach ($products as $product)
      <div class="col p-3">
        <div class="card rounded-0">
          <div class="card-body">
            <div class="text-center">
              <div class="product-box border text-center">
                <img src="{{$product->default_image_url }}" alt="">
            </div>
              <h5 class="mb-4 text-primary">{{$product->name}}</h5>
              @php
                $quantity = \App\Models\Quantity::where(['product_id' => $product->id])->sum('quantity');
              @endphp
              
              <h1 class="card-price"><span class="fs-6 text-secondary">{{$product->currency->name}}</span>{{$product->selling1}}</h1>
              <h1 class="card-price"><span class="fs-6 text-secondary">{{$product->unity}}</span>{{$quantity}}</h1>
            </div>
             <ul class="list-group list-group-flush">
               <li class="list-group-item"><i class="bi bi-check-circle me-2"></i>Nombre BLs:  {{$product->carts->count()}}</li>
               <li class="list-group-item"><i class="bi bi-check-circle me-2"></i>Total vendu:  {{$product->cartItems->sum('product_quantity')}}</li>
               <li class="list-group-item"><i class="bi bi-check-circle me-2"></i>Total achat:   {{ $product->cartItems->sum(function ($item) {return $item->product_price_selling * $item->product_quantity;})}}</li>
               <li class="list-group-item"><i class="bi bi-check-circle me-2"></i>Total vente:   {{$product->cartItems->sum(
                function ($item) {
                    return $item->product_price_buying * $item->product_quantity;
                }
            )}}</li>
               <li class="list-group-item"><i class="bi bi-check-circle me-2"></i>Total gain approximatif:  {{$product->cartItems->sum(
                function ($item) {
                    return ($item->product_price_selling * $item->product_quantity) - ($item->product_price_buying * $item->product_quantity);
                }
            )}}</li>
             </ul>
             <div class=" mt-3 d-grid d-lg-block">
                <a href="{{ route('admin.product.show', $product->id) }}" class="px-4">Plus de détails >></a>
             </div>
             <div class="text-center mt-3 d-grid d-lg-block">
              {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.product.mergeing', 'method'=>'POST', 'id'=>'form')) !!}
              <input type="hidden" value="{{$product->id}}" name="selected">
              <input type="hidden" value="{{$products->pluck('id')}}" name="products">
              <button type="button" id="select" class="btn btn-outline-primary rounded-0 px-4 shadow ">Sélectionner</button>
              {!! Form::close() !!}

             </div>
          </div>
        </div>
      </div>
      @endforeach
    </div><!--end row-->

      
  </main>
@endsection

@section('scripts')

<script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

    <script>
    $(document).on('click', '#select', function(event) {
        event.preventDefault();
         var form =  $(this).closest("form");
        //  var name = $(this).data("name");
         Swal.fire({
            title: 'Êtes-vous sûr?',
            text: "Vous ne pourrez pas revenir en arrière !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        });
     });
    </script>

@endsection
