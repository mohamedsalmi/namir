@extends('admin.layouts.app')

@section('title', 'Importation des articles')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
@endsection
@section('content')

    <main class="page-content row justify-content-center">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Articles</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.product.index') }}">Liste des articles</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Importer articles</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="card col-6">
     
            <div class="card-body ">
              <div class="mb-3">
                <label for="formFile" class="form-label">Template (fichier exemplaire):</label>
                <a href="{{asset('/assets/files/Product_temp.xlsx')}}" download>
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                  stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                  class="feather feather-download">
                  <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                  <polyline points="7 10 12 15 17 10"></polyline>
                  <line x1="12" y1="15" x2="12" y2="3"></line>
              </svg>
              Product_temp
                </a>
            </div>   
                {!! Form::open([
                    'enctype' => 'multipart/form-data',
                    'route' => 'admin.product.importationxl',
                    'method' => 'POST',
                    'id' => 'importation-product-form',
                    'class' => 'row g-3',
                ]) !!}
                <div class="mb-3">
                    <label for="formFile" class="form-label">Ajouter fichier à importer:</label>
                    <input class="form-control form-control-sm" type="file" name="File" id="formFile" required>
                </div>
                <button type="submit" class="btn btn-primary px-4">Importer</button>

                {!! Form::close() !!}
                @if (count($errors->getMessages()) > 0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <strong>Validation Errors:</strong>
                        <ul>
                            @foreach ($errors->getMessages() as $errorMessages)
                                @foreach ($errorMessages as $errorMessage)
                                    <li>
                                        {{ $errorMessage }}
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>


    @endsection
