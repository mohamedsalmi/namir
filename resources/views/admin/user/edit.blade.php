@extends('admin.layouts.app')

@section('title', 'Ajouter un User')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Utilisateurs</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.user.index') }}">Liste des utilisateurs</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier un utilisateur</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['user.update', $user->id],
            'method' => 'PUT',
            'id' => 'aupdate-user-form',
        ]) !!}

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Modifier un utilisateur</h5>
                            <div class="ms-auto">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-8">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Nom</label>
                                                <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom"
                                                    value="{{ old('name', $user->name) }}">
                                                @error('name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Email</label>
                                                <input name="email" type="text" class="form-control form-control-sm @error('email') is-invalid @enderror"
                                                    placeholder="Email" value="{{ old('email', $user->email) }}">
                                                @error('email')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Phone</label>
                                                <input name="phone" type="text" class="form-control form-control-sm @error('phone') is-invalid @enderror"
                                                    placeholder="Phone" value="{{ old('phone', $user->phone) }}">
                                                @error('phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label">RIB</label>
                                                <input name="rib" type="text" class="form-control form-control-sm @error('rib') is-invalid @enderror" placeholder="RIB"
                                                    value="{{ old('rib', $user->rib) }}">
                                                @error('rib')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label">Adresse</label>
                                                <input name="adresse" type="text" class="form-control form-control-sm @error('adresse') is-invalid @enderror"
                                                    placeholder="Adresse" value="{{ old('adresse', $user->adresse) }}">
                                                @error('adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Point de vente</label>
                                                {{ Form::select('store_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Store(), 'name'), old('store_id', $user->store_id), ['id' => 'store_id', 'name' => 'store_id', 'class' => $errors->has('store_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                @error('store_id')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Avatar</label>
                                                <input name="avatar" class="form-control form-control-sm" type="file"
                                                    value="{{ old('avatar', '') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-4">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 required">
                                                <label class="form-label">Rôle</label>
                                                {{ Form::select('role', \App\Helpers\Helper::makeDropDownListFromModel(new \Spatie\Permission\Models\Role(), 'name'), old('role', $user->role_id), ['id' => 'role', 'name' => 'role', 'class' => $errors->has('role') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                @error('role')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <!--end row-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        {!! Form::close() !!}
    </main>
@endsection
