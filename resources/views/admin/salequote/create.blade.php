@extends('admin.layouts.app')

@section('title', 'Créer BL')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Devis</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{route('admin.salequote.index')}}">Liste des devis</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Créer un devis</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.salequote.store',
            'method' => 'POST',
            'id' => 'create-salequote-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Créer un devis</h5>
                            <div class="ms-auto">
                                <button type="submit" name="save" id="save"
                                    class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <label class="form-label">Liste des clients :</label>
                                                <select class="form-select" id="fidel_list" name="client_id">
                                                    <option value="0" class="text-primary">Client passager</option>
                                                    @foreach ($clients as $client)
                                                        <option value="{{ $client->id }}"
                                                            name-fidel="{{ $client->name }}"
                                                            tel-fidel="{{ $client->phone }}"
                                                            MF-fidel="{{ $client->mf }}"
                                                            AD-fidel="{{ $client->adresse }}"
                                                            Prix-fidel="{{ $client->price }}">{{ $client->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm " name="client_details[name]"
                                                    placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('client_details.name', '') }}">

                                                <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm "
                                                    name="client_details[adresse]" placeholder="Adresse" id="adresse"
                                                    value="{{ old('client_details.adresse', '') }}">

                                                <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm MF "
                                                    name="client_details[mf]" placeholder="MF" id="mf"
                                                    value="{{ old('client_details.adresse', '') }}">

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile " name="client_details[phone]"
                                                    placeholder="phone" id="phone"
                                                    value="{{ old('client_details.adresse', '') }}">

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Type de Prix
                                                    :</label>
                                                <select class="form-select " id="Prix_T" name="Prix_T"
                                                    value="{{ old('client_details.Prix_T', '') }}">
                                                    <option value="gros">Gros</option>
                                                    <option value="semigros">Semi-Gros</option>
                                                    <option value="detail" selected>Detail</option>
                                                </select>
                                                <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date" class="form-control form-control-sm datetimepicker-input  "
                                                    name="date"
                                                    value="{{ old('client_details.date', '' . date('Y-m-d') . '') }}" />
                                                <div class="invalid-feedback d-none"></div>

                                            </div>

                                            {{-- <div class="col-12">
                                                               <label class="form-label">Commentaire</label>
                                                               <textarea class="form-control form-control-sm" placeholder="Commentaire" rows="4" cols="4"></textarea>
                                                          </div> --}}
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Prix dépend de</th>
                                                            <th>Qté dépend de</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>TVA(%)</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = old('items', []);
                                                            $noitems = is_array($items) ? count($items) : 0;
                                                        @endphp
                                                        @if ($noitems > 0)
                                                            @foreach ($items as $key => $item)
                                                                <tr id="row{{ $key }}" class="item_salequote"
                                                                    grade="{{ $key }}">
                                                                    <td> <input type="hidden"
                                                                            name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_price_selling]"value="{{ $item['product_price_selling'] }}" />
                                                                        <input type="text"
                                                                            name="items[{{ $key }}][product_name]"
                                                                            placeholder="Nom"
                                                                            class="form-control form-control-sm name_list" readonly
                                                                            value="{{ $item['product_name'] }}" />
                                                                    </td>
                                                                    <td>att</td>
                                                                    <td>att</td>
                                                                    <td><input type="number" style="width:80px;"
                                                                            name="items[{{ $key }}][product_quantity]"
                                                                            class="form-control form-control-sm quantity"
                                                                            id="quantity{{ $key }}"
                                                                            data-product_grade="{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_quantity'] }}"
                                                                            min="1" max=""></td>
                                                                    <td><input type="text" style="width: 70px;"
                                                                            class="form-control form-control-sm"
                                                                            name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                            readonly /></td>
                                                                    <td><input type="number"
                                                                            name="items[{{ $key }}][product_remise]"
                                                                            style="width: 80px;" min="0"
                                                                            max="100" step="0.01"
                                                                            class="form-control form-control-sm remise"
                                                                            id="remise{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_remise'] }}"></td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_tva]"
                                                                            style="width: 50px;" class="form-control form-control-sm tva"
                                                                            id="tva{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_tva'] }}" readonly>
                                                                    </td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_price_buying]"
                                                                            style="width: 100px;"
                                                                            class="form-control form-control-sm price{{ $key }} "
                                                                            id="price{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_price_buying'] }}"
                                                                            readonly></td>
                                                                    <td><input type="text" style="width: 100px;"
                                                                            class="form-control form-control-sm price{{ $key }} "
                                                                            id="L_total{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['L_total'] }}" readonly></td>
                                                                    <td><button type="button" name="remove"
                                                                            id="{{ $key }}"
                                                                            tr="{{ $key }}"
                                                                            class="btn btn-danger btn-sm btn_remove delete"><i
                                                                                class="bi bi-trash-fill"></i></button></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', '0') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class=" col-12">
                                                <label class="form-label">Choisir Article Avec :</label>
                                                <table>
                                                    <tr>
                                                        <td width="30%">
                                                            <form id="name_search">
                                                                <label>Nom</label>
                                                                <input type="text" id="searchprod" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                        <td width="30%">
                                                            <form id="barcode_search">
                                                                <label>Barcode/Référence</label>
                                                                <input type="text" id="barcode" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/salequote.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
@endsection
