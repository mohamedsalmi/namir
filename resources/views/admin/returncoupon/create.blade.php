@extends('admin.layouts.app')

@section('title', "Créer un bon de retour")

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Bons de retour</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                          <a href="{{route('admin.returncoupon.index')}}">Liste des bons de retour</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Créer un bon de retour</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.returncoupon.store',
            'method' => 'POST',
            'id' => 'create-returncoupon-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Créer un bon de retour</h5>
                            <div class="ms-auto">
                                <div class="btn-group">
                                    <button type="submit" name="save" id="save" class="save btn btn-outline-primary">Enregistrer</button>
                                    <button type="button" class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">	<span class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" style="">
                                      <li><button type="submit" id="savewithprint" name="save&print" value="1" class="dropdown-item">Enregitrer & Imprimer PDF</button>
                                      <li><button type="submit" id="savewithticket" name="save&ticket" value="1" class="dropdown-item">Enregitrer & Imprimer ticket</button>
                                      </li>
                                    </ul>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">

                         
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm " name="provider_details[name]"
                                                    placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('name', $voucher->provider_details['name'], '') }}"readonly>
                                                <input type="hidden" name="provider_id"
                                                    value="{{ old('name', $voucher->provider_id, '') }}">
                                                <input type="hidden" name="voucher_id"
                                                    value="{{ old('name', $voucher->id, '') }}">
                                                @error('provider_details.name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label">Adresse
                                                    :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm "
                                                    name="provider_details[adresse]" placeholder="Adresse" id="adresse"
                                                    value="{{ old('adresse', $voucher->provider_details['adresse'], '') }}"readonly>
                                                @error('provider_details.adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label">MF :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm MF"
                                                    name="provider_details[mf]" placeholder="MF" id="marticule"
                                                    value="{{ old('mf', $voucher->provider_details['mf'], '') }}"readonly>
                                                @error('provider_details.mf')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-2">
                                                <label class="form-label">Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile " name="provider_details[phone]"
                                                    placeholder="tel" id="tel"
                                                    value="{{ old('phone', $voucher->provider_details['phone'], '') }}"readonly>
                                                @error('provider_details.phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                          
                                            <div class="form-group col-12 col-lg-2">
                                                <label class="form-label" for="exampleFormControlInput1"><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date" class="form-control form-control-sm datetimepicker-input"
                                                    name="date" value="{{ old('date', '' . date('Y-m-d') . '') }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-12">
                                                <h6 class="form-label" for="exampleFormControlInput1">Liste des
                                                    articles de retour :</h6>
                                            </div>
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>TVA(%)</th>
                                                            <th>Prix d'achat</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = $voucher->items;
                                                        @endphp
                                                        @foreach ($items as $key => $item)
                                                        <tr id="row{{ $key }}" class="item_cart"
                                                        grade="{{ $key }}">
                                                        <td> <input type="hidden"
                                                                name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][product_price_buying]"value="{{ $item['product_price_buying'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][quantity_id]"
                                                                value="{{ $item['quantity_id'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][price_id]"
                                                                value="{{ $item['price_id'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][old_quantity]"
                                                                value="{{ $item['product_quantity'] }}" />
                                                            <input type="hidden"
                                                                name="items[{{ $key }}][product_currency_value]"
                                                                value="{{ $item['product_currency_value'] }}" />
                                                            @php
                                                                $product = \App\Models\Product::where('id', $item->product_id)->first();
                                                                $quantity = \App\Models\Quantity::where('id', $item->quantity_id)->first();

                                                            @endphp
                                                            <input type="text"
                                                                name="items[{{ $key }}][product_name]"
                                                                placeholder="Nom" class="form-control form-control-sm name_list"
                                                                readonly value="{{ $item['product_name'] }}" />
                                                        </td>

                                                        <td><input type="number" style="width:80px;"
                                                                name="items[{{ $key }}][product_quantity]"
                                                                class="form-control form-control-sm quantity"
                                                                id="quantity{{ $key }}"
                                                                data-product_grade="{{ $key }}"
                                                                data-product_id="{{ $key }}"
                                                                data-max="{{ $item['product_quantity'] }}"
                                                                value="{{ $item['product_quantity'] }}"
                                                                min="1" max="" step="1">
                                                        </td>
                                                        <td><input type="text" style="width: 70px;"
                                                                class="form-control form-control-sm"
                                                                name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                readonly /></td>
                                                        <td><input type="number"
                                                                readonly
                                                                name="items[{{ $key }}][product_remise]"
                                                                style="width: 80px;" min="0"
                                                                max="100" step="0.01"
                                                                class="form-control form-control-sm remise"
                                                                id="remise{{ $key }}"
                                                                data-product_id="{{ $key }}"
                                                                value="{{ $item['product_remise'] }}"></td>
                                                        <td><input type="text"
                                                                name="items[{{ $key }}][product_tva]"
                                                                style="width: 50px;" class="form-control form-control-sm tva"
                                                                id="tva{{ $key }}"
                                                                data-product_id="{{ $key }}"
                                                                value="{{ $item['product_tva'] }}" readonly></td>
                                                        <td><input type="text"
                                                                name="items[{{ $key }}][product_price_buying]"
                                                                style="width: 100px;"
                                                                class="form-control form-control-sm price{{ $key }} "
                                                                id="price{{ $key }}"
                                                                data-product_id="{{ $key }}"
                                                                value="{{ $item['product_price_buying'] }}"
                                                                readonly></td>
                                                        <td><input type="text" style="width: 100px;"
                                                                class="form-control form-control-sm price{{ $key }} "
                                                                id="L_total{{ $key }}"
                                                                data-product_id="{{ $key }}"
                                                                value="{{ $item['product_price_buying']*((100-$item['product_remise'])/100) * $item['product_quantity'] }}"
                                                                readonly></td>
                                                        <td><button type="button" name="remove"
                                                                id="{{ $key }}"
                                                                tr="{{ $key }}"
                                                                class="btn btn-danger btn-sm btn_remove delete"><i
                                                                    class="bi bi-trash-fill"></i></button></td>
                                                    </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>

                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', $voucher->total, '') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/returncoupon.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
   
@endsection
