@extends('admin.layouts.app')

@section('title','Article')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Bons de retour</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="{{route('admin.returncoupon.index')}}">Liste des bons de retour</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Détails bon de retour</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$returncoupon->created_date}}</h5>
                  <p class="mb-0">N° bon de retour: #{{$returncoupon->number}}</p>
                </div>
                {{-- <div class="col-12 col-lg-3 col-6 col-md-3">
                  <select class="form-select">
                    <option>Change Status</option>
                    <option>Awaiting Payment</option>
                    <option>Confirmed</option>
                    <option>Shipped</option>
                    <option>Delivered</option>
                  </select>
                </div>
                <div class="col-12 col-lg-3 col-6 col-md-3">
                   <button type="button" class="btn btn-primary">Enregistrer</button>
                   <button type="button" class="btn btn-secondary"><i class="bi bi-printer-fill"></i> Imprimer</button>
                </div> --}}
              </div>
             </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-2">
                   <div class="col">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-primary border-0">
                            <i class="bi bi-person text-primary"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Fournisseur</h6>
                             <p class="mb-1">{{$returncoupon->provider_details['name']}}</p>
                             <p class="mb-1">{{$returncoupon->provider_details['adresse']}}</p>
                             <p class="mb-1">{{$returncoupon->provider_details['phone']}}</p>
                          </div>
                       </div>
                       </div>
                     </div>
                   </div>
                   {{-- <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-success border-0">
                            <i class="bi bi-truck text-success"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Paiement</h6>
                             <p class="mb-1"><strong>Expédition</strong> : {{config('stock.info.name')}} </p>
                             <p class="mb-1"><strong>Methode de Paiement </strong> :{{$returncoupon->commitments->first()->paiements->first()->type}}</p>
                             @php
                                 $returncoupon_status='';
                                if($returncoupon->commitments->first()->paiements->SUM('amount') >= $returncoupon->total)
                                {
                                    $returncoupon_status=' Payé';
                                }else{
                                    $returncoupon_status=' Non payé';
                                }
                             @endphp
                             <p class="mb-1"><strong>Statut</strong> {{$returncoupon_status}} </p>
                          </div>
                       </div>
                       </div>
                      </div>
                   </div> --}}
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-danger border-0">
                            <i class="bi bi-geo-alt text-danger"></i>
                          </div>
                          <div class="info">
                            <h6 class="mb-2">Livrer à</h6>
                            <p class="mb-1"><strong>Adresse</strong> : {{$returncoupon->provider_details['adresse']}}</p>
                          </div>
                        </div>
                      </div>
                     </div>
                </div>
              </div><!--end row-->

              <div class="row">
                <div class="col-12 col-lg-8">
                   <div class="card border shadow-none radius-10">
                     <div class="card-body">
                         <div class="table-responsive">
                           <table class="table align-middle mb-0">
                             <thead class="table-light">
                               <tr>
                                 <th>Produit</th>
                                 <th>Prix unitaire</th>
                                 <th>Quantité</th>
                                 <th>Total</th>
                               </tr>
                             </thead>
                             <tbody>
                              @php
                              $total_tva = 0;
                              $total_ut = 0;
                          @endphp
                              @foreach($returncoupon->items as $returncouponitem)
                              @php
                              $pu=($returncouponitem->product_price_buying*100)/(100+$returncouponitem->product_tva);
                              $couttva=($pu * $returncouponitem->product_tva) / 100;
                              $total_ut += $pu * $returncouponitem->product_quantity;
                              $total_tva += $couttva * $returncouponitem->product_quantity;
                          @endphp
                               <tr>
                                 <td>
                                   <div class="orderlist">
                                    <a class="d-flex align-items-center gap-2" href="javascript:;">
                                      <div class="product-box">
                                          <img src="{{ asset( $returncouponitem->product->default_image_url ?? config('stock.image.default.product')) }}" alt="">
                                      </div>
                                      <div>
                                          <p class="mb-0 product-title">{{$returncouponitem->product_name}}</p>
                                      </div>
                                     </a>
                                   </div>
                                 </td>
                                 <td>{{$returncouponitem->product_price_buying}}  {{$returncouponitem->product_currency_value}}</td>
                                 <td>{{$returncouponitem->product_quantity}}</td>
                                 <td>{{(float)$returncouponitem->product_quantity*$returncouponitem->product_price_buying}} {{$returncouponitem->product_currency_value}} </td>
                               </tr>
                               @endforeach
                             </tbody>
                           </table>
                         </div>
                     </div>
                   </div>
                </div>
                <div class="col-12 col-lg-4">
                  <div class="card border shadow-none bg-light radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-4">
                           <div>
                              <h5 class="mb-0">Récapitulatif de la bl</h5>
                           </div>
                        </div>
                          <div class="d-flex align-items-center mb-3">

                              <div>
                                  <p class="mb-0">TOTAL HT</p>
                                </div>
                                <div class="ms-auto">
                                  <h5 class="mb-0">{{ number_format($returncoupon->total - $total_tva, 3) }}  </h5>
                              </div>

                        </div>
                          <div class="d-flex align-items-center mb-3">

                              <div>
                                  <p class="mb-0">TOTAL TVA</p>
                                </div>
                                <div class="ms-auto">
                                  <h5 class="mb-0">{{ number_format($total_tva, 3) }}  </h5>
                              </div>
                        </div>
                          <div class="d-flex align-items-center mb-3">

                            <div>
                              <p class="mb-0">TTC</p>
                            </div>
                            <div class="ms-auto">
                              <h5 class="mb-0">{{$returncoupon->total}}  </h5>
                          </div>
                        </div>

                    </div>
                  </div>


               </div>
            </div><!--end row-->

          </div>
        </div>
      </main>
   <!--end page main-->
@endsection


