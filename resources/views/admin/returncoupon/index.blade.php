@extends('admin.layouts.app')

@section('title', 'Liste des commandes')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Bons de retour</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des bons de retour</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des bons de retour</h5>
                <div class="col-md-3 offset-md-5">
                    <h4 class="mb-0 text-success"> Total: <span id="total"
                            data-column="3">{{ number_format($sum, 3) }}</span> </h4>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="input-group col-md-6 mb-1">
                                    <span class="input-group-text">Du</span>
                                    <input required type="date" class="filter-field form-control form-control-sm" name="from"
                                        placeholder="De" id="from">
                                    <span class="input-group-text">Au</span>
                                    <input required type="date" class="filter-field form-control form-control-sm" name="to"
                                        placeholder="Au" id="to">
                                    <span class="input-group-text">Provider</span>
                                    <select class="form-control " name="" id="providerfilter">
                                        <option value="">Tous</option>
                                        @foreach (\App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Provider(), 'name') as $key => $provider)
                                            <option value="{{ $key }}">{{ $provider }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <table class="dataTable table align-middle" style="width: 100%;">
                                <thead class="table-light">
                                    <tr>
                                        <th>Numéro</th>
                                        <th>Fournisseur</th>
                                        <th>Prix</th>
                                        <th>Etat</th>
                                        <th>Date</th>
                                        <th>Creé par</th>
                                        <th>Modifié par</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($returncoupons as $returncoupon)
                                        <tr>
                                            <td>#{{ $returncoupon->id }}</td>
                                            <td>{{ $returncoupon->provider_details['name'] }}</td>
                                            <td>{{ $returncoupon->total }} DM</td>
                                            <td>{{ $returncoupon->updated_at }}</td>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 fs-6">
                                                    @canany(['Détails bon de retour'])
                                                        <a href="{{ route('admin.returncoupon.show', $returncoupon->id) }}"
                                                            class="text-primary" data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom" title=""
                                                            data-bs-original-title="Voir détails" aria-label="Views"><i
                                                                class="bi bi-eye-fill"></i></a>
                                                    @endcanany
                                                    @canany(['Imprimer bon de retour'])
                                                        <a href="{{ route('admin.returncoupon.print', $returncoupon->id) }}"
                                                            class="btn text-black" data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom" title="" target="blank"
                                                            data-bs-original-title="Imprimer" aria-label="Views"><i
                                                                class="bi bi-printer-fill text-black"></i></a>
                                                    @endcanany
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach --}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-12 col-lg-3 d-flex">
                      <div class="card w-100">
                        <div class="card-header py-3">
                          <h5 class="mb-0">Filter by</h5>
                        </div>
                        <div class="card-body">
                          <form class="row g-3">
                            <div class="col-12">
                              <label class="form-label">Order ID</label>
                              <input type="text" class="form-control form-control-sm" placeholder="Order ID">
                            </div>
                            <div class="col-12">
                             <label class="form-label">Provider</label>
                             <input type="text" class="form-control form-control-sm" placeholder="Provider name">
                           </div>
                           <div class="col-12">
                             <label class="form-label">Order Status</label>
                             <select class="form-select">
                               <option>Published</option>
                               <option>Draft</option>
                             </select>
                           </div>
                           <div class="col-12">
                            <label class="form-label">Total</label>
                            <input type="text" class="form-control form-control-sm">
                           </div>
                           <div class="col-12">
                            <label class="form-label">Date Added</label>
                            <input type="date" class="form-control form-control-sm">
                           </div>
                           <div class="col-12">
                            <label class="form-label">Date Modified</label>
                            <input type="date" class="form-control form-control-sm">
                           </div>
                           <div class="col-12">
                             <div class="d-grid">
                               <button class="btn btn-primary">Filter Product</button>
                             </div>
                           </div>
                          </form>
                        </div>
                      </div>
                    </div> --}}
        </div>
        <!--end row-->

        <input type="hidden" id="date-column" value="3">
        <input type="hidden" id="client-column" value="1">

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/sale-datatable.js') }}"></script>
    <script>
        $(document).ready(function() {

            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                drawCallback: function(settings) {
                    $('#total').text(settings.json.total);
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ sur _PAGES_",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.returncoupon.index') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(3)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [

                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'provider',
                        name: 'provider',
                    },
                    {
                        data: 'total',
                        name: 'total',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                    {
                        data: 'updated_by',
                        name: 'updated_by',
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });

            function filter() {
                var queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.returncoupon.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                // e.preventDefault();
            }

            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('id');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
                filter();
            });

            $(document).on('change', '#providerfilter', function(e) {
                var value = $(this).val();
                filter['provider'] = value;
                console.log(filter);
                filter();
            });
        });
    </script>
@endsection
