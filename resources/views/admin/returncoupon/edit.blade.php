@extends('admin.layouts.app')

@section('title', 'Modifier BL')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Bons d'achat</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                          <a href="{{route('admin.voucher.index')}}">Liste des bons d'achat</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier un bon d'achat</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['voucher.update', $voucher->id],
            'method' => 'POST',
            'id' => 'create-voucher-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Modifier un bon d'achat</h5>
                            <div class="ms-auto">
                                {{-- <button type="submit" name="save_draft" class="btn btn-secondary">Save to Draft</button> --}}
                                <button type="submit" name="save" id="save"
                                    class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <h6 class=" text-center text-info">Détails de paiement</h6>
                                            <div class="col-12">
                                                @php
                                                    $commitments = $voucher->commitments;
                                                    $noattributes = is_array($commitments) ? count($commitments) : 0;

                                                    if (isset($commitments)) {
                                                        if (!is_array($commitments)) {
                                                            $nocommitments = count(json_decode($commitments, true));
                                                            $commitments = json_decode($commitments);
                                                        }
                                                    } else {
                                                        $commitments = [];
                                                    }
                                                @endphp
                                                <div class="form-check form-switch">
                                                    {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                    @if ($noattributes > 1)
                                                        <input class="form-check-input" name="Deadline_check"
                                                            type="checkbox" id="Deadline_check" checked>
                                                    @else
                                                        <input class="form-check-input" name="Deadline_check"
                                                            type="checkbox" id="Deadline_check">
                                                    @endif
                                                    <label class="form-check-label" for="Deadline">Echéance</label>
                                                </div>
                                                <button type="button" name="Add_paiement" id="Add_paiement"
                                                    class="btn btn-primary " data-bs-toggle="modal"
                                                    data-bs-target="#paiment_modal">+ Créer les écheances</button>
                                            </div>
                                            <div class="form-group col-12 col-lg-4">

                                                <div class="form-check form-switch">
                                                    {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                    <input class="form-check-input" name="timbre" type="checkbox">
                                                    <label class="form-label" for="exampleFormControlInput1"><i
                                                            class="fa fa-tag"></i> Timbre </label>
                                                </div>

                                            </div>
                                            <div class="modal fade" id="paiment_modal" tabindex="-1" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-scrollable modal-xl">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="col-4">
                                                                <h5 class="modal-title text-info">Liste des écheances </h5>
                                                            </div>
                                                            <h6 class="form-label m-3">Montant total à payer :</h6>
                                                            <input type="text"
                                                                class="btn btn-warning px-5 radius-30 total"
                                                                value="{{ old('total', $voucher->total, '') }}" readonly>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>

                                                        </div>
                                                        <div class="modal-body">
                                                            {{-- <div class="row col-6">
                                                                <div class="col-4">
                                                                    <label class="form-label">Choisir nombre
                                                                        d'écheance:</label>
                                                                </div>
                                                                <div class="col-4">
                                                                    <input type="number" class="form-control form-control-sm"
                                                                        value="2" min="2">
                                                                </div>
                                                            </div> --}}
                                                            <div class="repeat_items">
                                                                @if ($nocommitments > 0)
                                                                    @foreach ($commitments as $key => $item)
                                                                        <div class="card bg-light row">
                                                                            <div>
                                                                                <button type="button" class="btn-close"
                                                                                    id="deleteItem"></button>
                                                                            </div>
                                                                            <div class="card-body ">
                                                                                <h6 class="text-info">Echéance
                                                                                    {{ $key + 1 }}:</h6>
                                                                                <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>Montant:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                        <div class="input-group mb-3">
                                                                                            <input
                                                                                                name="Deadline{{ $key }}[amount]"
                                                                                                type="number"
                                                                                                class="form-control form-control-sm"
                                                                                                aria-label="Text input with checkbox"
                                                                                                value="{{ old('amount', $item->amount, '') }}">
                                                                                            <div class=" col-4">
                                                                                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'Deadline[' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>date d'écheance:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                        <input
                                                                                            name="Deadline{{ $key }}[date]"
                                                                                            type="date"
                                                                                            class="form-control form-control-sm"
                                                                                            aria-label="Text input with checkbox"
                                                                                            value="{{ old('date', $item->date, '') }}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>

                                                            <div class="ms-auto">
                                                                <button role="button" class="btn btn-sm btn-warning"
                                                                    id="addItem" onclick="return 0;"><i
                                                                        class="bi bi-plus"></i> Ajouter une
                                                                    écheance</button>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Terminer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-3">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-primary px-1 rounded-1"
                                                            id="addCash">Espèce</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-primary px-1 rounded-1"
                                                            id="addCheck">Chèque</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-primary px-1 rounded-1"
                                                            id="addTransfer">Virement</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-primary px-1 rounded-1"
                                                            id="addExchange">Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        @php
                                                            $paiements = $voucher->commitments->first()->paiements;
                                                            $nopaiements = is_array($paiements) ? count($paiements) : 0;
                                                            if (isset($paiements)) {
                                                                if (!is_array($paiements)) {
                                                                    $nopaiements = count(json_decode($paiements, true));
                                                                    $paiements = json_decode($paiements);
                                                                }
                                                            } else {
                                                                $paiements = [];
                                                            }
                                                        @endphp
                                                        @if ($nopaiements > 0)
                                                            @foreach ($paiements as $key => $item)
                                                                <div class="repeat_cash">
                                                                    @if ($item->type == 'cash')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingOne{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn bx bx-trash col-2 text-danger"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseOne{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseOne{{ $key }}"
                                                                                        value="Espéces {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseOne{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingOne{{ $key }}"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label
                                                                                        class="form-label">Curency</label>
                                                                                    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'), 1, ['id' => 'currency_id', 'name' => 'paiements[cash][' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                    @error('currency_id')
                                                                                        <div class="invalid-feedback">
                                                                                            {{ $message }}
                                                                                        </div>
                                                                                    @enderror
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="text"
                                                                                        class="form-control form-control-sm amount"
                                                                                        name="paiements[cash][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control form-control-sm "
                                                                                        name="paiements[cash][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="cash">
                                                                                    <input
                                                                                        name="paiements[cash][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_check repeat">
                                                                    @if ($item->type == 'check')
                                                                        <div class="accordion-item">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingTwo{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn bx bx-trash col-2 text-danger"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseTwo{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseTwo{{ $key }}"
                                                                                        value="Chèque {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseTwo{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingTwo"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm amount amount"
                                                                                        name="paiements[check][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <label class="form-label">Numero du
                                                                                        chèque</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[check][{{ $key }}][numbre]"
                                                                                        placeholder="Numero du chèque"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Bank</label>
                                                                                    <input type="text"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[check][{{ $key }}][bank]"
                                                                                        placeholder="Bank"
                                                                                        value="{{ old('bank', $item->bank, '') }}">
                                                                                    <label class="form-label">date de
                                                                                        reglement:</label>
                                                                                    <input
                                                                                        name="paiements[check][{{ $key }}][received_at]"
                                                                                        type="date"
                                                                                        class="form-control form-control-sm"
                                                                                        aria-label="Text input with checkbox"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[check][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="check">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_transfer">
                                                                    @if ($item->type == 'transfer')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingThree{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn bx bx-trash col-2 text-danger"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseThree{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseThree{{ $key }}"
                                                                                        value="Virement {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseThree{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingThree"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm amount "
                                                                                        name="paiements[transfer][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input
                                                                                        name="paiements[transfer][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">

                                                                                    <label class="form-label">Numero du
                                                                                        Virement</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[transfer][{{ $key }}][numbre]"
                                                                                        placeholder="Numero du Virement"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Bank</label>
                                                                                    <input type="text"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[transfer][{{ $key }}][bank]"
                                                                                        placeholder="Bank"
                                                                                        value="{{ old('bank', $item->bank, '') }}">
                                                                                    <label class="form-label">Date du
                                                                                        Virement</label>
                                                                                    <input type="date"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[transfer][{{ $key }}][received_at]"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[transfer][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="transfer">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="repeat_exchange">
                                                                    @if ($item->type == 'exchange')
                                                                        <div class="accordion-item ">
                                                                            <h2 class="accordion-header"
                                                                                id="flush-headingFour{{ $key }}">
                                                                                <div class="row">
                                                                                    <button type="button"
                                                                                        class="btn bx bx-trash col-2 text-danger"
                                                                                        id="deleteCash"></button>
                                                                                    <input
                                                                                        class="accordion-button collapsed col"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#flush-collapseFour{{ $key }}"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="flush-collapseFour{{ $key }}"
                                                                                        value="Traite {{ $key + 1 }}">
                                                                                </div>
                                                                            </h2>
                                                                            <div id="flush-collapseFour{{ $key }}"
                                                                                class="accordion-collapse collapse"
                                                                                aria-labelledby="flush-headingFour"
                                                                                data-bs-parent="#accordionFlushExample">
                                                                                <div class="accordion-body">
                                                                                    <label class="form-label">Montant
                                                                                        Payé</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm amount"
                                                                                        name="paiements[exchange][{{ $key }}][amount]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="{{ old('amount', $item->amount, '') }}">
                                                                                    <input
                                                                                        name="paiements[exchange][{{ $key }}][provider_id]"
                                                                                        type="hidden" value="1">
                                                                                    <label class="form-label">Numero de
                                                                                        Traite</label>
                                                                                    <input type="number"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[exchange][{{ $key }}][numbre]"
                                                                                        placeholder="Numero de la Lettre"
                                                                                        value="{{ old('numbre', $item->numbre, '') }}">
                                                                                    <label class="form-label">Date de
                                                                                        Traite</label>
                                                                                    <input type="date"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[exchange][{{ $key }}][received_at]"
                                                                                        value="{{ old('received_at', $item->received_at, '') }}">
                                                                                    <input type="hidden"
                                                                                        class="form-control form-control-sm"
                                                                                        name="paiements[exchange][{{ $key }}][type]"
                                                                                        placeholder="Montant Payé"
                                                                                        value="exchange">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                {{-- @if ($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}
                                                <label class="form-label">Fournisseur :</label>
                                                <select class="form-select" id="fidel_list" name="provider_id">
                                                    <option value="" class="text-primary">Choisir un fournisseur</option>
                                                    @foreach ($providers as $provider)
                                                    <option {{ $voucher->provider()->exists() && $provider->id == $voucher->provider_id ? 'selected' : '' }} value="{{ $provider->id }}" name-fidel="{{ $provider->name }}"
                                                        tel-fidel="{{ $provider->phone }}" MF-fidel="{{ $provider->mf }}"
                                                        AD-fidel="{{ $provider->address }}"
                                                        Prix-fidel="{{ $provider->price }}">{{ $provider->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm " name="provider_details[name]"
                                                    placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('name', $voucher->provider_details['name'], '') }}">
                                                @error('provider_details.name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm "
                                                    name="provider_details[adresse]" placeholder="Adresse" id="adresse"
                                                    value="{{ old('adresse', $voucher->provider_details['adresse'], '') }}">
                                                @error('provider_details.adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm MF"
                                                    name="provider_details[mf]" placeholder="MF" id="marticule"
                                                    value="{{ old('mf', $voucher->provider_details['mf'], '') }}">
                                                @error('provider_details.mf')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile " name="provider_details[phone]"
                                                    placeholder="tel" id="tel"
                                                    value="{{ old('phone', $voucher->provider_details['phone'], '') }}">
                                                @error('provider_details.phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Type de Prix
                                                    :</label>
                                                <select class="form-select @error('Prix_T') is-invalid @enderror"
                                                    id="Prix_T" name="Prix_T">
                                                    <option {{ $voucher->provider()->exists() && $voucher->provider->price == 'gros' ? 'selected' : '' }}  value="gros">Gros</option>
                                                    <option {{ $voucher->provider()->exists() && $voucher->provider->price == 'semigros' ? 'selected' : '' }} value="semigros">Semi-Gros</option>
                                                    <option {{ $voucher->provider()->exists() && $voucher->provider->price == 'detail' ? 'selected' : '' }} value="detail" >Detail</option>
                                                </select>
                                                @error('Prix_T')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date" class="form-control form-control-sm datetimepicker-input"
                                                    name="date" value="{{ old('date', $voucher->date, '') }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            {{-- <div class="col-12">
                                                               <label class="form-label">Commentaire</label>
                                                               <textarea class="form-control form-control-sm" placeholder="Commentaire" rows="4" cols="4"></textarea>
                                                          </div> --}}
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Prix dépend de</th>
                                                            <th>Qté dépend de</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>TVA(%)</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = $voucher->items;
                                                        @endphp
                                                        @foreach ($items as $key => $item)
                                                            <tr id="row{{ $key }}" class="item_voucher"
                                                                grade="{{ $key }}">
                                                                <td> <input type="hidden"
                                                                        name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_price_selling]"value="{{ $item['product_price_selling'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][quantity_id]"
                                                                        value="{{ $item['quantity_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][price_id]"
                                                                        value="{{ $item['price_id'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][old_quantity]"
                                                                        value="{{ $item['product_quantity'] }}" />
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_currency_value]"
                                                                        value="{{ $item['product_currency_value'] }}" />
                                                                    @php
                                                                        $product = \App\Models\Product::where('id', $item->product_id)->first();
                                                                        $quantity = \App\Models\Quantity::where('id', $item->quantity_id)->first();
                                                                        $price = \App\Models\Price::where('id', $item->price_id)->first();
                                                                        //    dd($item->price_id);
                                                                        if ($quantity->product_attribute1 == null) {
                                                                            $attribute1 = 'aucun';
                                                                            $val1 = 0;
                                                                        } else {
                                                                            $attribute1 = $quantity->productValue1->attributeValue->value;
                                                                            $val1 = $quantity->product_value2;
                                                                        }
                                                                        if ($quantity->product_attribute2 == null) {
                                                                            $attribute2 = 'aucun';
                                                                            $val2 = 0;
                                                                        } else {
                                                                            $attribute2 = $quantity->productValue2->attributeValue->value;
                                                                            $val2 = $quantity->product_value2;
                                                                        }
                                                                        if ($price->product_attribute1 == null) {
                                                                            $attribute3 = 'aucun';
                                                                            $val3 = 0;
                                                                        } else {
                                                                            $attribute3 = $price->productValue1->attributevalue->value;
                                                                            $val3 = $price->product_value1;
                                                                        }
                                                                        if ($price->product_attribute2 == null) {
                                                                            $attribute4 = 'aucun';
                                                                            $val4 = 0;
                                                                        } else {
                                                                            $attribute4 = $price->productValue2->attributevalue->value;
                                                                            $val4 = $price->product_value2;
                                                                        }
                                                                    @endphp
                                                                    <input type="text"
                                                                        name="items[{{ $key }}][product_name]"
                                                                        placeholder="Nom" class="form-control form-control-sm name_list"
                                                                        readonly value="{{ $item['product_name'] }}" />
                                                                </td>
                                                                </td>
                                                                <td><input class="form-control form-control-sm attr1"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="items[{{ $key }}][product_id]"
                                                                        value="{{ $attribute3 }}" readonly><input
                                                                        type="hidden" id="attr1"
                                                                        name="items[{{ $key }}][attr1]"
                                                                        value="{{ $val3 }}">
                                                                    <input class="form-control form-control-sm attr1"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="items[{{ $key }}][product_id]"
                                                                        value="{{ $attribute4 }}" readonly><input
                                                                        type="hidden" id="attr1"
                                                                        name="items[{{ $key }}][attr2]"
                                                                        value="{{ $val4 }}">
                                                                </td>
                                                                <td><input class="form-control form-control-sm attr1"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="items[{{ $key }}][product_id]"
                                                                        value="{{ $attribute1 }}" readonly><input
                                                                        type="hidden" id="attr1"
                                                                        name="items[{{ $key }}][attr_quantity1]"
                                                                        value="{{ $val1 }}">
                                                                    <input class="form-control form-control-sm attr1"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="items[{{ $key }}][product_id]"
                                                                        value="{{ $attribute2 }}" readonly><input
                                                                        type="hidden" id="attr1"
                                                                        name="items[{{ $key }}][attr_quantity2]"
                                                                        value="{{ $val2 }}">
                                                                </td>
                                                                <td><input type="number" style="width:80px;"
                                                                        name="items[{{ $key }}][product_quantity]"
                                                                        class="form-control form-control-sm quantity"
                                                                        id="quantity{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_quantity'] }}"
                                                                        min="1" max="" step="1">
                                                                </td>
                                                                <td><input type="text" style="width: 70px;"
                                                                        class="form-control form-control-sm"
                                                                        name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                        readonly /></td>
                                                                <td><input type="number"
                                                                        name="items[{{ $key }}][product_remise]"
                                                                        style="width: 80px;" min="0"
                                                                        max="100" step="0.01"
                                                                        class="form-control form-control-sm remise"
                                                                        id="remise{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_remise'] }}"></td>
                                                                <td><input type="text"
                                                                        name="items[{{ $key }}][product_tva]"
                                                                        style="width: 50px;" class="form-control form-control-sm tva"
                                                                        id="tva{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_tva'] }}" readonly></td>
                                                                <td><input type="text"
                                                                        name="items[{{ $key }}][product_price_selling]"
                                                                        style="width: 100px;"
                                                                        class="form-control form-control-sm price{{ $key }} "
                                                                        id="price{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_price_selling'] }}"
                                                                        readonly></td>
                                                                <td><input type="text" style="width: 100px;"
                                                                        class="form-control form-control-sm price{{ $key }} "
                                                                        id="L_total{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_price_selling']*((100-$item['product_remise'])/100) * $item['product_quantity'] }}"
                                                                        readonly></td>
                                                                <td><button type="button" name="remove"
                                                                        id="{{ $key }}"
                                                                        tr="{{ $key }}"
                                                                        class="btn btn-danger btn-sm btn_remove delete"><i
                                                                            class="bi bi-trash-fill"></i></button></td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', $voucher->total, '') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row ">
                                            <div class=" col-12 ">
                                                <label class="form-label">Choisir Article Avec :</label>
                                                <table>
                                                    <tr>
                                                        <td width="30%">
                                                            <form id="name_search">
                                                                <label>Nom</label>
                                                                <input type="text" id="searchprod" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                        <td width="30%">
                                                            <form id="barcode_search">
                                                                <label>Barcode/Référence</label>
                                                                <input type="text" id="barcode" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/voucher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/template" id="itemsList">
        <div class="card bg-light row">
            <div>
            <button type="button" class="btn-close" id="deleteItem"></button>
            </div>
            <div  class="card-body ">
                <h6 class="text-info">Echéance {?}:</h6>
                <div class="row">
                    <div class="col-lg-5">
                        <div>
                            <br>
                            <h6>Montant:</h6>
                        </div>
                    </div>
                    <div class=" col-lg-7 ">
                        <div class="input-group mb-3">
                            <input name="Deadline[{?}][amount]" type="number" class="form-control form-control-sm Deadline" aria-label="Text input with checkbox">
                            <div class=" col-4">
                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-lg-5">
                        <div>
                            <br>
                            <h6>date d'écheance:</h6>
                        </div>
                    </div>
                    <div class=" col-lg-7 ">
                            <input  name="Deadline[{?}][date]" type="date" class="form-control form-control-sm" aria-label="Text input with checkbox">

                        </div>
                 </div>
            </div>
        </div>

</script>
    <script type="text/template" id="cashdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingOne{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteCash"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseOne{?}"
aria-expanded="false"
aria-controls="flush-collapseOne{?}" value="Espéces {?}">
</div>
</h2>
<div id="flush-collapseOne{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingOne{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Curency</label>
{{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
@error('currency_id')
<div class="invalid-feedback">
{{ $message }}
</div>
@enderror
<label class="form-label ">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[cash][{?}][amount]"
placeholder="Montant Payé" required>
<input type="hidden" class="form-control form-control-sm "
name="paiements[cash][{?}][type]"
placeholder="Montant Payé" value="cash">
<input name="paiements[cash][{?}][provider_id]" type="hidden"  value="1">
</div>
</div>
</div>
</script>
    <script type="text/template" id="checkdetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingTwo{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteCheck"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseTwo{?}"
aria-expanded="false"
aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
</div>
</h2>
<div id="flush-collapseTwo{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingTwo{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[check][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[check][{?}][provider_id]" type="hidden"  value="1">

<label class="form-label">Numero du chèque</label>
<input type="number" class="form-control form-control-sm"
name="paiements[check][{?}][numbre]"
placeholder="Numero du chèque" required>
<label class="form-label">Bank</label>
<input type="text" class="form-control form-control-sm"
name="paiements[check][{?}][bank]"
placeholder="Bank" required>
<label class="form-label">date de reglement:</label>
<input name="paiements[check][{?}][received_at]" type="date"
class="form-control form-control-sm"
aria-label="Text input with checkbox" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[check][{?}][type]"
placeholder="Montant Payé" value="check">
</div>
</div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingFour{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteExchange"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseFour{?}"
aria-expanded="false"
aria-controls="flush-collapseFour{?}" value="Traite {?}">
</div>
</h2>
<div id="flush-collapseFour{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingFour{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[exchange][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[exchange][{?}][provider_id]" type="hidden"  value="1">
<label class="form-label">Numero de Traite</label>
<input type="number" class="form-control form-control-sm"
name="paiements[exchange][{?}][numbre]"
placeholder="Numero de la Lettre" required>
<label class="form-label">Date de Traite</label>
<input type="date" class="form-control form-control-sm"
name="paiements[exchange][{?}][received_at]" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[exchange][{?}][type]"
placeholder="Montant Payé" value="exchange" >
</div>
</div>
</div>
</script>
    <script type="text/template" id="transferdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingThree{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseThree{?}"
aria-expanded="false"
aria-controls="flush-collapseThree{?}" value="Virement {?}">

</div>
</h2>
<div id="flush-collapseThree{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingThree{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount "
name="paiements[transfer][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[transfer][{?}][provider_id]" type="hidden"  value="1">

<label class="form-label">Numero du Virement</label>
<input type="number" class="form-control form-control-sm"
name="paiements[transfer][{?}][numbre]"
placeholder="Numero du Virement" required>
<label class="form-label">Bank</label>
<input type="text" class="form-control form-control-sm"
name="paiements[transfer][{?}][bank]"
placeholder="Bank" required>
<label class="form-label">Date du Virement</label>
<input type="date" class="form-control form-control-sm"
name="paiements[transfer][{?}][received_at]" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[transfer][{?}][type]"
placeholder="Montant Payé" value="transfer">
</div>
</div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="false"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[exchange][{?}][provider_id]" type="hidden"  value="1">
            <label class="form-label">Numero de Traite</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numero de la Lettre">
            <label class="form-label">Date de Traite</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[exchange][{?}][received_at]">
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="false"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm "
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[transfer][{?}][provider_id]" type="hidden"  value="1">

            <label class="form-label">Numero du Virement</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement">
                <label class="form-label">Bank</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{?}][bank]"
                    placeholder="Bank">
            <label class="form-label">Date du Virement</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[transfer][{?}][received_at]">
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
        </div>
    </div>
</div>
</script>
    <script>
        $(function() {
            $(".repeat_items").repeatable1({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 2,
                template: "#itemsList",
                itemContainer: ".row",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
    </script>
@endsection
