@extends('admin.layouts.app')

@section('title', 'Paramètres')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/jquery-toast-plugin-master/jquery.toast.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Paramètres</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i> Tableau de
                                bord</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Paramètres</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <!--start row-->
        <div class="row">
            <div class="col-xl-9 mx-auto">
                {{-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
                <hr /> --}}
                <div class="card">
                    <div class="card-body">
                        {!! Form::open([
                            'enctype' => 'multipart/form-data',
                            'route' => 'admin.setting.update',
                            'method' => 'PUT',
                            'id' => 'update-settings-form',
                        ]) !!}
                        <div class="border p-4 rounded">
                            <div class="card-title d-flex align-items-center">
                                <h5 class="mb-0">Paramètres</h5>
                            </div>
                            <hr />
                            @foreach ($settings as $key => $item)
                                <div class="row mb-3">
                                    <label for="settings.{{ $key }}.value"
                                        class="col-sm-3 col-form-label">{{ $item->name }}</label>
                                    <div class="col-sm-9">
                                        <input name="settings[{{ $key }}][name]" type="hidden"
                                            value="{{ $item->name }}">
                                        <input type="text" value="{{ old('settings.' . $key . '.value', $item->value) }}"
                                            id="value{{ $item->id }}"
                                            data-id="{{$item->id}}" data-old-value="{{$item->value}}" data-name="{{$item->name}}"
                                            {{ str_contains($item->name, 'Codification ') ? 'pattern=^[0-9]{4}-[0-9]{5}$' : ''}}
                                            class="save form-control form-control-sm @error('settings.' . $key . '.value') is-invalid @enderror" id="settings.{{ $key }}.value"
                                            placeholder="{{ $item->name }}" name="settings[{{ $key }}][value]">
                                        @error('settings.' . $key . '.value')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            @endforeach

                            {{-- <div class="row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-primary px-5">Enregistrer</button>
                                </div>
                            </div> --}}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </main>
    <!--end page main-->
@endsection

@section('scripts')
<script src="{{ asset('assets/admin/plugins/jquery-toast-plugin-master/jquery.toast.js') }}"></script>

<script>
    var errors = null;
    $(document).on('blur', '.save', function(e, options = '') {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var old_value = $(this).data('old-value');
        var name = $(this).data('name');
        var value = $(this).val();
        console.log(id, old_value);
        var url = "/admin/setting/" + id + "/update";
        if (errors) {
            $.each(errors, function(key, value) {
                var element = document.getElementById(key+id);
                if (element) {
                    element.classList.remove('is-invalid')
                };
            });
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "PUT",
            data:{name, value, old_value},
            async: false,
            success: function(response, textStatus, jqXHR) {
                $.toast({
                    heading: 'Succès!',
                    text: 'Enregistré avec succès!',
                    bgColor: '#0DC700',
                    textColor: 'white',
                    position: 'top-right', 
                    icon: 'success',
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                errors = jqXHR.responseJSON.errors;
                $.each(errors, function(key, value) {
                    var element = document.getElementById(key+id);
                    if (element) {
                        element.classList.add('is-invalid');
                        $("#" + key + id).nextAll().filter(".invalid-feedback").html(value[0]);
                        $("#" + key + id).nextAll().filter(".invalid-feedback").removeClass('d-none');
                    };
                });
            }
        });
    });
  </script>
@endsection
