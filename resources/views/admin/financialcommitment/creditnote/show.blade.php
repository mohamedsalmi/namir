@extends('admin.layouts.app')

@section('title', 'Paiement')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Encaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Détails de paiement</li>
                    </ol>
                </nav>
            </div>
            {{-- <div class="ms-auto">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-primary">Settings</button>
                    <button type="button" class="btn btn-sm btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split"
                        data-bs-toggle="dropdown"> <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end"> <a class="dropdown-item"
                            href="javascript:;">Action</a>
                        <a class="dropdown-item" href="javascript:;">Another action</a>
                        <a class="dropdown-item" href="javascript:;">Something else here</a>
                        <div class="dropdown-divider"></div> <a class="dropdown-item" href="javascript:;">Separated link</a>
                    </div>
                </div>
            </div> --}}
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="row g-3 align-items-center">
                    <div class="col-12 col-lg-4 col-md-6 me-auto">
                        <h5 class="mb-1">{{ $commitment->date }}</h5>
                        <p class="mb-0">ID du Paiement: #{{ $commitment->id }}</p>
                    </div>
                    {{-- <div class="col-12 col-lg-3 col-6 col-md-3">
                        <select class="form-select">
                            <option>Change Statut</option>
                            <option>Awaiting Payment</option>
                            <option>Confirmed</option>
                            <option>Shipped</option>
                            <option>Delivered</option>
                        </select>
                    </div> --}}
                    <div class="col-12 col-lg-3 col-6 col-md-3">
                        @if ($commitment->paiment_status == 0)
                            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                data-bs-target="#paymentmodal">Ajouter Paiement</button>
                            <div class="modal fade" id="paymentmodal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Paiemnt detail</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        {!! Form::open([
                                            'enctype' => 'multipart/form-data',
                                            'route' => ['financialcommitment.update', $commitment->id],
                                            'method' => 'POST',
                                            'id' => 'update-commitement-form',
                                        ]) !!}
                                        <div class="modal-body">
                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-3">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1"
                                                            id="addCash">Espèce</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1"
                                                            id="addCheck">Chèque</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1"
                                                            id="addTransfer">Virement</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1"
                                                            id="addExchange">Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        <div class="repeat_cash">

                                                        </div>
                                                        <div class="repeat_check repeat">

                                                        </div>
                                                        <div class="repeat_transfer">

                                                        </div>
                                                        <div class="repeat_exchange">

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-sm btn-primary">Payer</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @else
                            <span class="btn btn-sm btn-success">Payé</span>
                        @endif
                        <button type="button" class="btn btn-sm btn-secondary"><i class="bi bi-printer-fill"></i>
                            Imprimer</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-primary bpayment-0">
                                        <i class="bi bi-person text-primary"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Client</h6>
                                        <p class="mb-1">{{ $commitment->creditnote->client_details['name'] }}</p>
                                        <p class="mb-1">{{ $commitment->creditnote->client_details['adresse'] }}</p>
                                        <p class="mb-1">{{ $commitment->creditnote->client_details['phone'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-success bpayment-0">
                                        <i class="bi bi-truck text-success"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Info de Paiement </h6>
                                        <p class="mb-1"><strong>Expédition</strong> : {{config('stock.info.name')}}</p>
                                        <p class="mb-1"><strong>Statut</strong>
                                            @php
                                                if ($commitment->paiment_status == 0) {
                                                    $status = '<td><span class="badge rounded-pill alert-danger">Non Payé</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-danger bpayment-0">
                                        <i class="bi bi-geo-alt text-danger"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Livrer à</h6>
                                        <p class="mb-1"><strong>Address</strong> :
                                            {{ $commitment->creditnote->client_details['adresse'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table align-middle mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                <th>ID de Paiement</th>
                                                <th>Montant</th>
                                                <th>Méthode</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($commitment->paiements as $paiement)
                                                <tr>
                                                    <td>#{{ $paiement->id }}</td>
                                                    <td>{{ $paiement->amount }}</td>
                                                    <td>{{ $paiement->type }}</td>
                                                    <td>{{ $paiement->created_at }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end row-->

            </div>
        </div>

    </main>
    <!--end page main-->
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/template" id="cashdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingOne{?}">
        <div class="row">
      <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteCash"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseOne{?}"
            aria-expanded="false"
            aria-controls="flush-collapseOne{?}" value="Espéces {?}">
                </div>
    </h2>
    <div id="flush-collapseOne{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingOne{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Curency</label>
            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
            @error('currency_id')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
            <label class="form-label ">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[cash][{?}][amount]"
                placeholder="Montant Payé" required>
                <input type="hidden" class="form-control form-control-sm "
                name="paiements[cash][{?}][type]"
                placeholder="Montant Payé" value="cash">
                <input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="checkdetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingTwo{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteCheck"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseTwo{?}"
            aria-expanded="false"
            aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
                </div>
    </h2>
    <div id="flush-collapseTwo{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingTwo{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[check][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numero du chèque</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[check][{?}][numbre]"
                placeholder="Numero du chèque" required>
            <label class="form-label">Bank</label>
            <input type="text" class="form-control form-control-sm"
                name="paiements[check][{?}][bank]"
                placeholder="Bank" required>
            <label class="form-label">date de reglement:</label>
            <input name="paiements[check][{?}][received_at]" type="date"
                class="form-control form-control-sm"
                aria-label="Text input with checkbox" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[check][{?}][type]"
                placeholder="Montant Payé" value="check">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="false"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numero de Traite</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numero de la Lettre" required>
            <label class="form-label">Date de Traite</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[exchange][{?}][received_at]" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange" >
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="false"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount "
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numero du Virement</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement" required>
                <label class="form-label">Bank</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{?}][bank]"
                    placeholder="Bank" required>
            <label class="form-label">Date du Virement</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[transfer][{?}][received_at]" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
        </div>
    </div>
</div>
</script>
    <script>
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                // total:
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                // total:total
            });
        })
    </script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        </script>
    @endif
@endsection
