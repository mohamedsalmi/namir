@extends('admin.layouts.app')

@section('title', 'Paiement')

@section('stylesheets')
    <style>
        .swal2-popup {
            z-index: 9999999999999999999 !important;
        }
    </style>
@endsection
@section('content')

@endsection
@section('scripts')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Encaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.financialcommitment.index') }}">Liste des
                                paiements</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.financialcommitment.show', ['cart' => $commitment->parent->id, 'type' => strtolower(class_basename($commitment->parent))]) }}">Détails
                                échéance</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Détails de paiement</li>
                    </ol>
                </nav>
            </div>
            {{-- <div class="ms-auto">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-primary">Settings</button>
                    <button type="button" class="btn btn-sm btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split"
                        data-bs-toggle="dropdown"> <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end"> <a class="dropdown-item"
                            href="javascript:;">Action</a>
                        <a class="dropdown-item" href="javascript:;">Another action</a>
                        <a class="dropdown-item" href="javascript:;">Something else here</a>
                        <div class="dropdown-divider"></div> <a class="dropdown-item" href="javascript:;">Separated link</a>
                    </div>
                </div>
            </div> --}}
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="row g-3 align-items-center">
                    <div class="col-12 col-lg-4 col-md-6 me-auto">
                        <h5 class="mb-0">Echéance: {{ $commitment->id }}</h5>
                        <p class="mb-1">{{ $commitment->date }}</p>
                    </div>
                    <div class="col-12 col-lg-3 col-6 col-md-3">
                        @if ($commitment->paiment_status == 0)
                            @canany(['Modifier encaissement'])
                                <button id="add_payment" type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#paymentmodal">Ajouter paiement</button>
                            @endcanany
                            <div class="modal fade" id="paymentmodal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Détails paiement</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        {!! Form::open([
                                            'enctype' => 'multipart/form-data',
                                            'route' => [
                                                'admin.financialcommitment.update',
                                                ['commitment' => $commitment->id, 'type' => strtolower(class_basename($commitment))],
                                            ],
                                            'method' => 'POST',
                                            'id' => 'update-commitement-form',
                                        ]) !!}
                                        <div class="modal-body">
                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-1">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-success px-2 rounded-pill" id="addCash">
                                                            <i class="bi bi-currency-dollar"></i> Espèce</button></div>

                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-2 rounded-pill" id="addCheck">
                                                            <i class="bi bi-card-heading"></i> Chèque</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-secondary px-2 rounded-pill"
                                                            id="addTransfer">
                                                            <i class="bi bi-bank2"></i> Virement</button></div>
                                                    <div class="col pr-1"><button type="button"
                                                            class="btn btn-sm btn-danger px-2 rounded-pill"
                                                            id="addExchange">
                                                            <i class="bi bi-file-earmark-text"></i> Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        <div class="repeat_cash"></div>
                                                        <div class="repeat_check repeat"></div>
                                                        <div class="repeat_transfer"></div>
                                                        <div class="repeat_exchange"></div>
                                                        <div class="repeat_rs"></div>
                                                        <div class="repeat_ra"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button id="save" type="submit"
                                                class="btn btn-sm btn-primary">Payer</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            {{-- @else
                            <span class="btn btn-sm btn-success">Payé</span> --}}
                        @endif
                        {{-- <button type="button" class="btn btn-sm btn-secondary"><i class="bi bi-printer-fill"></i>Imprimer</button> --}}
                    </div>
                    <div class="col">
                        <div class="card radius-10 border-0 border-start border-warning border-3">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="">
                                        <p class="mb-1">Total </p>
                                        <h4 id="payment_total" class="mb-0 text-warning">{{ $commitment->amount }}</h4>
                                    </div>
                                    <div class="ms-auto widget-icon bg-warning text-white">
                                        <i class="bi bi-currency-dollar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $total = $commitment->paiements->sum('amount');
                    @endphp
                    <div class="col">
                        <div class="card radius-10 border-0 border-start border-success border-3">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="">
                                        <p class="mb-1">Montant payé</p>
                                        <h4 id="paid_amount" class="mb-0 text-success">{{ number_format($total, 3) }}</h4>
                                    </div>
                                    <div class="ms-auto widget-icon bg-success text-white">
                                        <i class="bi bi-currency-dollar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-primary bpayment-0">
                                        <i class="bi bi-person text-primary"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Client</h6>
                                        <p class="mb-1">{{ $commitment->parent->client_details['name'] }}</p>
                                        <p class="mb-1">{{ $commitment->parent->client_details['adresse'] }}</p>
                                        <p class="mb-1">{{ $commitment->parent->client_details['phone'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-success bpayment-0">
                                        <i class="bi bi-truck text-success"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Info de Paiement </h6>
                                        <p class="mb-1"><strong>Expédition</strong> : {{config('stock.info.name')}}</p>
                                        <p class="mb-1"><strong>Statut</strong>
                                            @php
                                                if ($commitment->paiment_status == 0) {
                                                    $status = '<td><span class="badge rounded-pill alert-danger">Non Payé</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-danger bpayment-0">
                                        <i class="bi bi-geo-alt text-danger"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Livrer à</h6>
                                        <p class="mb-1"><strong>Address</strong> :
                                            {{ $commitment->parent->client_details['adresse'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table align-middle mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                {{-- <th>ID de Paiement</th> --}}
                                                <th>Montant</th>
                                                <th>Méthode</th>
                                                <th>Date de réception</th>
                                                <th>Date de création</th>
                                                <th>Date de réglement</th>
                                                <th>Ajouté par</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($commitment->paiements as $paiement)
                                                <tr>
                                                    <td>{{ $paiement->amount }}</td>
                                                    <td>{{ $paiement->type }}</td>
                                                    <td>{{ $paiement->received_at }}</td>
                                                    <td>{{ $paiement->created_at }}</td>
                                                    <td>{{ $paiement->due_at }}</td>
                                                    <td>{{ $paiement->created_by }}</td>
                                                    <td>
                                                        @canany(['Supprimer encaissement'])
                                                            <form method="POST"
                                                                action="{{ route('admin.financialcommitment.payment.delete', ['payment' => $paiement->id, 'type' => request()->get('type')]) }}">
                                                                {{ csrf_field() }}
                                                                <input name="_method" type="hidden" value="GET">
                                                                <button id="show_confirm" type="submit"
                                                                    class="btn btn-sm text-danger show_confirm"
                                                                    data-toggle="tooltip" title="Supprimer"><i
                                                                        class="bi bi-trash-fill"></i></button>
                                                            </form>
                                                        @endcanany
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end row-->

            </div>
        </div>
        </main>
    <!--end page main-->
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/template" id="cashdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingOne{?}">
        <div class="row">
      <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCash"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseOne{?}"
            aria-expanded="true"
            aria-controls="flush-collapseOne{?}" value="Espéces {?}">
                </div>
    </h2>
    <div id="flush-collapseOne{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingOne{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Curency</label>
            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
            @error('currency_id')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
            <label class="form-label ">Montant Payé</label>
            <input type="number" class="form-control amount"
                name="paiements[cash][{?}][amount]"
                placeholder="Montant Payé" step="0.001" required>
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[cash][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control" name="paiements[cash][{?}][due_at]" value="{{date('Y-m-d')}}">
                <input type="hidden" class="form-control "
                name="paiements[cash][{?}][type]"
                placeholder="Montant Payé" value="cash">
                <input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="checkdetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingTwo{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCheck"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseTwo{?}"
            aria-expanded="true"
            aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
                </div>
    </h2>
    <div id="flush-collapseTwo{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingTwo{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control amount"
                name="paiements[check][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numéro du chèque</label>
            <input type="number" class="form-control"
                name="paiements[check][{?}][numbre]"
                placeholder="Numero du chèque" required>
            <label class="form-label">Bank</label>
            {{ Form::select('paiements[check][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[check][{?}][bank]', 'name' => 'paiements[check][{?}][bank]', 'class' => $errors->has('paiements.check.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
            
            <label class="form-label">Date de réglement:</label>
            <input name="paiements[check][{?}][due_at]" type="date"
                class="form-control"
                aria-label="Text input with checkbox" required>
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[check][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control"
                name="paiements[check][{?}][type]"
                placeholder="Montant Payé" value="check">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="true"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant payé</label>
            <input type="number" class="form-control amount"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numéro de traite</label>
            <input type="number" class="form-control"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numero de traite" required>
            <label class="form-label">Date de réglement</label>
            <input type="date" class="form-control"
                name="paiements[exchange][{?}][due_at]" required>
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[exchange][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange" >
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="true"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant payé</label>
            <input type="number" class="form-control amount"
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant payé" required>
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numéro du virement</label>
            <input type="number" class="form-control"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement" required>
                <label class="form-label">Banque</label>
                {{ Form::select('paiements[transfer][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[transfer][{?}][bank]', 'name' => 'paiements[transfer][{?}][bank]', 'class' => $errors->has('paiements.transfer.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
                
            <label class="form-label">Date de réglement</label>
            <input type="date" class="form-control"
                name="paiements[transfer][{?}][due_at]" required>
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[transfer][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
        </div>
    </div>
</div>
</script>

   

    <script>
        function initAmount(method, item) {
            var input = item.find(".amount");
            input.focus();
            var total = $('#payment_total').text();
            let paid_amount = $('#paid_amount').text();
            var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
            var amounts_count = $('.amount').length;
            if (amounts_count <= 1) {
                first_amount.val(Number(total - paid_amount).toFixed(3));
            }
        }

        function initRSAmount(method, item) {
            var input = item.find(".amount");
            input.focus();
            var total = $('#payment_total').text();
            var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
            var amounts_count = $('.amount').length;
            first_amount.val(total / 100);
        }

        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('check', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('cash', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('exchange', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                afterAdd: function(item) {
                    initAmount('transfer', item);
                },
                afterDelete: function() {
                    var amount = $(".amount");
                    amount.focus();
                },
            });
        })
    </script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 2500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 2500
                })
            })
        </script>
    @endif

    <script>
        $(document).on("click", "#save", function(e) {
            submit(e);
        });

        function submit(e, options = '') {
            e.preventDefault();
            var sum = 0;
            $('.amount').each(function() {
                sum += Number($(this).val());
            });
            total_payment_amount = sum;
            total_cart = $('#payment_total').text();
            let paid_amount = $('#paid_amount').text();

            // if ((total_payment_amount > (Number(total_cart - paid_amount).toFixed(3))) || sum == 0) {
            if (total_payment_amount < 0) {
                Swal.fire({
                    title: "Paiement incomplet!",
                    text: "Vous devez insérer les données de paiement corectement",
                    icon: 'warning',
                });
                $('.swal2-popup').focus();
            } else {
                var form = $('#update-commitement-form');
                form.submit();
            }
        }
    </script>



    <script>
        function filterReturnedAmounts() {
            $('option').prop('disabled', false); //reset all the disabled options on every change event
            $('.returned-amount').each(function() { //loop through all the select elements
                var val = this.value;
                $('.returned-amount').not(this).find('option').filter(
                    function() { //filter option elements having value as selected option
                        return this.value === val;
                    }).prop('disabled', true); //disable those option elements
            });
        }

        $(document).on('change', '.returned-amount', function() {
            filterReturnedAmounts();
            var _this = $(this).prop('id');
            var index = _this.match(/\d+/).join("");

            var selectedVal = $(".returned-amount option:selected");
            var _amount = $('input[name="paiements[ra][' + index + '][amount]"]');
            var model = $('input[name="paiements[ra][' + index + '][model]"]');
            if (_amount.length > 0) {
                _amount.val(selectedVal.attr('data-amount'));
                model.val(selectedVal.attr('data-model'));
            }
        });
    </script>

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
