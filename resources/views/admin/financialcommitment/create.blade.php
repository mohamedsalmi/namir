@extends('admin.layouts.app')

@section('title', 'Créer BL')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Encaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Creer Nouvelle Commande</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.cart.store',
            'method' => 'POST',
            'id' => 'create-cart-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Creer Nouvelle Commande</h5>
                            <div class="ms-auto">
                                <button type="submit" name="save_draft" class="btn btn-sm btn-secondary">Save to Draft</button>
                                <button type="submit" name="save" id="save" class="btn btn-sm btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-8">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                {{-- @if($errors->any())
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    @endif --}}
                                                <label class="form-label">Liste des clients :</label>
                                                <select class="form-select" id="fidel_list" name="client_id">
                                                    <option value="0" class="text-primary">Client passager</option>
                                                    @foreach ($clients as $client)
                                                        <option value="{{ $client->id }}" name-fidel="{{ $client->name }}"
                                                            tel-fidel="{{ $client->phone }}" MF-fidel="{{ $client->mf }}"
                                                            AD-fidel="{{ $client->adresse }}"
                                                            Prix-fidel="{{ $client->price }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm " name="client_details[name]"
                                                    placeholder="Nom & Prénom" id="name" value="{{ old('client_details.name', '') }}">

                                                                            <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm "
                                                    name="client_details[adresse]" placeholder="Adresse" id="adresse" value="{{ old('client_details.adresse', '') }}">

                                                                            <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm MF "
                                                    name="client_details[mf]" placeholder="MF" id="mf" value="{{ old('client_details.adresse', '') }}">

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;" class="form-control form-control-sm mobile "
                                                    name="client_details[phone]" placeholder="phone" id="phone"
                                                    value="{{ old('client_details.adresse', '') }}" >

                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Type de Prix
                                                    :</label>
                                                <select class="form-select " id="Prix_T" name="Prix_T"
                                                value="{{ old('client_details.Prix_T', '') }}">
                                                    <option value="gros">Gros</option>
                                                    <option value="semigros">Semi-Gros</option>
                                                    <option value="detail" selected>Detail</option>
                                                </select>
                                                 <div class="invalid-feedback d-none"></div>

                                            </div>
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date" class="form-control form-control-sm datetimepicker-input  "
                                                    name="date"  value="{{ old('client_details.date', ''.date('Y-m-d').'') }}" />
                                                                            <div class="invalid-feedback d-none"></div>

                                            </div>

                                            {{-- <div class="col-12">
                                                               <label class="form-label">Commentaire</label>
                                                               <textarea class="form-control form-control-sm" placeholder="Commentaire" rows="4" cols="4"></textarea>
                                                          </div> --}}
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Prix dépend de</th>
                                                            <th>Qté dépend de</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>TVA(%)</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field" >
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                        <tr>
                                                            <div class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                <div class="d-flex align-items-center">
                                                                  <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                                                                  </div>
                                                                  <div class="ms-3">
                                                                    <div class="text-danger">{{ $message }}</div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                        </tr>
                                                        @enderror
                                                        @php
                                                        $items = old('items', []);
                                                        $noitems = is_array($items) ? count($items) : 0;
                                                    @endphp
                                                    @if($noitems > 0)
                                                        @foreach($items as $key => $item)
                                                        <tr  id="row{{$key}}" class="item_cart" grade="{{$key}}">
                                                        <td>    <input type="hidden" name="items[{{$key}}][product_id]"value="{{$item['product_id']}}"/>
                                                            <input type="hidden" name="items[{{$key}}][product_currency_id]"value="{{$item['product_currency_id']}}"/>
                                                            <input type="hidden" name="items[{{$key}}][product_price_selling]"value="{{$item['product_price_selling']}}"/>
                                                            <input type="text" name="items[{{$key}}][product_name]" placeholder="Nom" class="form-control form-control-sm name_list" readonly value="{{$item['product_name']}}"/></td>
                                                        <td>att</td>
                                                        <td>att</td>
                                                        <td><input type="number" style="width:80px;" name="items[{{$key}}][product_quantity]" class="form-control form-control-sm quantity" id="quantity{{$key}}"  data-product_grade="{{$key}}" data-product_id="{{$key}}" value="{{$item['product_quantity']}}" min="1" max="" ></td>
                                                        <td><input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[{{$key}}][product_unity]"value="{{$item['product_unity']}}" readonly/></td>
                                                        <td><input type="number" name="items[{{$key}}][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise{{$key}}"  data-product_id="{{$key}}" value="{{$item['product_remise']}}" ></td>
                                                        <td><input type="text" name="items[{{$key}}][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva{{$key}}"  data-product_id="{{$key}}" value="{{$item['product_tva']}}" readonly></td>
                                                        <td><input type="text" name="items[{{$key}}][product_price_buying]" style="width: 100px;" class="form-control form-control-sm price{{$key}} "   id="price{{$key}}"  data-product_id="{{$key}}" value="{{$item['product_price_buying']}}" readonly></td>
                                                        <td><input type="text"  style="width: 100px;" class="form-control form-control-sm price{{$key}} "   id="L_total{{$key}}"  data-product_id="{{$key}}" value="{{$item['L_total']}}" readonly ></td>
                                                        <td><button type="button" name="remove" id="{{$key}}" tr="{{$key}}" class="btn btn-sm btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button></td>
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror" readonly id="total"
                                                                    value="{{ old('total', '0') }}">
                                                                    @error('total')
                                                                   <div class="invalid-feedback">{{ $message }}</div>
                                                                    @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="col-12 col-lg-4">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3" >
                                            <h6 class=" text-center text-info" id="paiements">Détails de paiement </h6>
                                            <div class="invalid-feedback d-none"></div>
                                            <div class="col-12">
                                                <div class="form-check form-switch">
                                                    {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                    <input class="form-check-input" name="Deadline_check" type="checkbox"
                                                        id="Deadline_check" >
                                                    <label class="form-check-label" for="Deadline">Echéance</label>
                                                </div>
                                                <button type="button" name="Add_paiement" id="Add_paiement"
                                                    class="btn btn-sm btn-primary " data-bs-toggle="modal"
                                                    data-bs-target="#paiment_modal">+ Créer les écheances</button>
                                            </div>
                                            <div class="form-group col-12 col-lg-4">

                                                <div class="form-check form-switch">
                                                    {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                    <input class="form-check-input" name="timbre" type="checkbox" >
                                                    <label class="form-label" for="exampleFormControlInput1"><i
                                                            class="fa fa-tag"></i> Timbre </label>
                                                </div>

                                            </div>
                                            <div class="modal fade" id="paiment_modal" tabindex="-1"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-scrollable modal-xl">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="col-4">
                                                                <h5 class="modal-title text-info">Liste des écheances </h5>
                                                            </div>
                                                            <h6 class="form-label m-3">Montant total à payer :</h6>
                                                            <input type="text"
                                                                class="btn btn-sm btn-warning px-5 radius-30 total"
                                                                value="0" readonly>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>

                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="repeat_items">
                                                                @php
                                                                    $attributes = old('attributes', []);
                                                                    $noattributes = is_array($attributes) ? count($attributes) : 0;

                                                                    if (isset($attributes)) {
                                                                        if (!is_array($attributes)) {
                                                                            $noattributes = count(json_decode($attributes, true));
                                                                            $attributes = json_decode($attributes);
                                                                        }
                                                                    } else {
                                                                        $attributes = [];
                                                                    }
                                                                @endphp
                                                                @if ($noattributes > 0)
                                                                    @foreach ($attributes as $key => $item)
                                                                        <div class="card bg-light row">
                                                                            <div>
                                                                                <button type="button" class="btn-close"
                                                                                    id="deleteItem"></button>
                                                                            </div>
                                                                            <div class="card-body ">
                                                                                <h6 class="text-info">Echéance
                                                                                    {{ $key }}:</h6>
                                                                                <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>Montant:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                        <div class="input-group mb-3">
                                                                                            <input
                                                                                                name="Deadline{{ $key }}[amount]"
                                                                                                type="number"
                                                                                                class="form-control form-control-sm"
                                                                                                aria-label="Text input with checkbox">
                                                                                            <div class=" col-4">
                                                                                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[' . $key . '][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>date d'écheance:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                        <input
                                                                                            name="Deadline{{ $key }}[date]"
                                                                                            type="date"
                                                                                            class="form-control form-control-sm"
                                                                                            aria-label="Text input with checkbox">

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>

                                                            <div class="ms-auto">
                                                                <button role="button" class="btn btn-sm btn-sm btn-warning"
                                                                    id="addItem" onclick="return 0;"><i
                                                                        class="bi bi-plus"></i> Ajouter une
                                                                    écheance</button>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary"
                                                                data-bs-dismiss="modal">Terminer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="cash">
                                                <div class="row row-cols-auto g-3">
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1" id="addCash">Espèce</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1" id="addCheck">Chèque</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1" id="addTransfer">Virement</button></div>
                                                    <div class="col"><button type="button"
                                                            class="btn btn-sm btn-primary px-1 rounded-1" id="addExchange">Traite</button></div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="accordion accordion-flush " id="accordionFlushExample">
                                                        <div class="repeat_cash">
                                                            @php
                                                            $attributes = old('attributes', []);
                                                            $noattributes = is_array($attributes) ? count($attributes) : 0;

                                                            if (isset($attributes)) {
                                                                if (!is_array($attributes)) {
                                                                    $noattributes = count(json_decode($attributes, true));
                                                                    $attributes = json_decode($attributes);
                                                                }
                                                            } else {
                                                                $attributes = [];
                                                            }
                                                        @endphp
                                                        @if ($noattributes > 0)
                                                            @foreach ($attributes as $key => $item)
                                                            <div class="accordion-item " >
                                                            <h2 class="accordion-header" id="flush-headingOne">
                                                                <button class="accordion-button collapsed" type="button"
                                                                    data-bs-toggle="collapse"
                                                                    data-bs-target="#flush-collapseOne"
                                                                    aria-expanded="false"
                                                                    aria-controls="flush-collapseOne">
                                                                    Espéces {{ $key }}
                                                                </button>
                                                                <button type="button" class="btn-close" id="delete" data-bs-toggle="collapse"
                                                                data-bs-target="#flush-collapseOne"
                                                                aria-expanded="false"
                                                                aria-controls="flush-collapseOne"></button>
                                                            </h2>
                                                            <div id="flush-collapseOne"
                                                                class="accordion-collapse collapse"
                                                                aria-labelledby="flush-headingOne"
                                                                data-bs-parent="#accordionFlushExample">
                                                                <div class="accordion-body">
                                                                    <label class="form-label">Curency</label>
                                                                    {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash]['. $key .'][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                    @error('currency_id')
                                                                        <div class="invalid-feedback">
                                                                            {{ $message }}
                                                                        </div>
                                                                    @enderror
                                                                    <label class="form-label">Montant Payé</label>
                                                                    <input type="text" class="form-control form-control-sm"
                                                                        name="paiements[cash][{{ $key }}][amount]"
                                                                        placeholder="Montant Payé">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                          <div class="repeat_check repeat">
                                                            @php
                                                            $attributes = old('attributes', []);
                                                            $noattributes = is_array($attributes) ? count($attributes) : 0;

                                                            if (isset($attributes)) {
                                                                if (!is_array($attributes)) {
                                                                    $noattributes = count(json_decode($attributes, true));
                                                                    $attributes = json_decode($attributes);
                                                                }
                                                            } else {
                                                                $attributes = [];
                                                            }
                                                        @endphp
                                                        @if ($noattributes > 0)
                                                            @foreach ($attributes as $key => $item)
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="flush-headingTwo">
                                                                    <button class="accordion-button collapsed" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#flush-collapseTwo"
                                                                        aria-expanded="false"
                                                                        aria-controls="flush-collapseTwo">
                                                                        Chèque {{ $key }}
                                                                    </button>
                                                                </h2>
                                                                <div id="flush-collapseTwo"
                                                                    class="accordion-collapse collapse"
                                                                    aria-labelledby="flush-headingTwo"
                                                                    data-bs-parent="#accordionFlushExample">
                                                                    <div class="accordion-body">
                                                                        <label class="form-label">Numero du chèque</label>
                                                                        <input type="text" class="form-control form-control-sm"
                                                                            name="paiements[check][{{ $key }}][check_nbr]"
                                                                            placeholder="Numero du chèque">
                                                                        <label class="form-label">Date du chèque</label>
                                                                        <input type="date" class="form-control form-control-sm"
                                                                            name="paiements[check][{{ $key }}][check_date]">
                                                                        <label class="form-label">Bank</label>
                                                                        <input type="text" class="form-control form-control-sm"
                                                                            name="paiements[check][{{ $key }}][bank]"
                                                                            placeholder="Bank">
                                                                        <label class="form-label">date de reglement:</label>
                                                                        <input name="paiements[check][{{ $key }}][received_at]" type="date"
                                                                            class="form-control form-control-sm"
                                                                            aria-label="Text input with checkbox">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                          </div>
<div class="repeat_transfer">
    @php
    $attributes = old('attributes', []);
    $noattributes = is_array($attributes) ? count($attributes) : 0;

    if (isset($attributes)) {
        if (!is_array($attributes)) {
            $noattributes = count(json_decode($attributes, true));
            $attributes = json_decode($attributes);
        }
    } else {
        $attributes = [];
    }
@endphp
@if ($noattributes > 0)
    @foreach ($attributes as $key => $item)
    <div class="accordion-item ">
        <h2 class="accordion-header" id="flush-headingThree">
            <button class="accordion-button collapsed" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseThree"
                aria-expanded="false"
                aria-controls="flush-collapseThree">
                Virement {{ $key }}
            </button>
        </h2>
        <div id="flush-collapseThree"
            class="accordion-collapse collapse"
            aria-labelledby="flush-headingThree"
            data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <label class="form-label">Numero du Virement</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{{ $key }}][transfer_nbr]"
                    placeholder="Numero du Virement">
                <label class="form-label">Date du Virement</label>
                <input type="date" class="form-control form-control-sm"
                    name="paiements[transfer][{{ $key }}][transfer_date]">
                <label class="form-label">Bank</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{{ $key }}][bank]"
                    placeholder="Bank">
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>
<div class="repeat_exchange">
    @php
    $attributes = old('attributes', []);
    $noattributes = is_array($attributes) ? count($attributes) : 0;

    if (isset($attributes)) {
        if (!is_array($attributes)) {
            $noattributes = count(json_decode($attributes, true));
            $attributes = json_decode($attributes);
        }
    } else {
        $attributes = [];
    }
@endphp
@if ($noattributes > 0)
    @foreach ($attributes as $key => $item)   <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingFour">
        <button class="accordion-button collapsed" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour"
            aria-expanded="false"
            aria-controls="flush-collapseFour">
            Traite {{ $key }}
        </button>
    </h2>
    <div id="flush-collapseFour"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingFour"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Numero de Traite</label>
            <input type="text" class="form-control form-control-sm"
                name="paiements[exchange][{{ $key }}][transfer_nbr]"
                placeholder="Numero de la Lettre">
            <label class="form-label">Date de Traite</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[exchange][{{ $key }}][transfer_date]">

        </div>
    </div>
</div>
@endforeach
@endif
</div>

                                                    </div>
                                                </div>
                                            </div>

                                            {{-- <div class="col-12">
                                                               <label class="form-label">Curency</label>
                                                               {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name') , null, ['id'=> 'currency_id', 'name'=> 'currency_id', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                                                               @error('currency_id')
                                                               <div class="invalid-feedback">
                                                                   {{ $message }}
                                                               </div>
                                                               @enderror
                                                          </div>
                                                          <div class="col-12">
                                                               <label class="form-label">Montant Payé</label>
                                                               <input type="text" class="form-control form-control-sm" placeholder="Montant Payé">
                                                          </div> --}}
                                            <!--si le type ne pas cash-->
                                            {{-- <div class="col-12">
                                                               <label class="form-label">Image</label>
                                                               <input class="form-control form-control-sm" type="file">
                                                          </div> --}}

                                        </div>
                                        <!--end row-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-5"></div>
                            <div class="col-12 col-lg-3">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row ">
                                            <div class=" col-12 ">
                                                <label class="form-label">Choisir Article Avec :</label>
                                                <table>
                                                    <tr>
                                                        <td width="30%">
                                                            <form id="name_search">
                                                                <label>Nom</label>
                                                                <input type="text" id="searchprod" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                        <td width="30%">
                                                            <form id="barcode_search">
                                                                <label>Barcode/Référence</label>
                                                                <input type="text" id="barcode" name=""
                                                                    style="width: 100px" class="form-control form-control-sm">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>
    <script src="{{ asset('assets/admin/js/cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/template" id="itemsList">

                                                                        <div class="card bg-light row">
                                                                            <div>
                                                                            <button type="button" class="btn-close" id="deleteItem"></button>
                                                                            </div>
                                                                            <div  class="card-body ">
                                                                                <h6 class="text-info">Echéance {?}:</h6>
                                                                                <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>Montant:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                        <div class="input-group mb-3">
                                                                                            <input name="Deadline[{?}][amount]" type="number" class="form-control form-control-sm Deadline" aria-label="Text input with checkbox">
                                                                                            <div class=" col-4">
                                                                                                {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                 <div class="row">
                                                                                    <div class="col-lg-5">
                                                                                        <div>
                                                                                            <br>
                                                                                            <h6>date d'écheance:</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-lg-7 ">
                                                                                            <input  min="{{ date('Y-m-d'); }}" name="Deadline[{?}][date]" type="date" class="form-control form-control-sm" aria-label="Text input with checkbox">

                                                                                        </div>
                                                                                 </div>
                                                                            </div>
                                                                        </div>

</script>
<script type="text/template" id="cashdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingOne{?}">
        <div class="row">
      <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteCash"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseOne{?}"
            aria-expanded="false"
            aria-controls="flush-collapseOne{?}" value="Espéces {?}">
                </div>
    </h2>
    <div id="flush-collapseOne{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingOne{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Curency</label>
            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
            @error('currency_id')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
            <label class="form-label ">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[cash][{?}][amount]"
                placeholder="Montant Payé" required>
                <input type="hidden" class="form-control form-control-sm "
                name="paiements[cash][{?}][type]"
                placeholder="Montant Payé" value="cash">
                <input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
        </div>
    </div>
</div>
</script>
<script type="text/template" id="checkdetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingTwo{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteCheck"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseTwo{?}"
            aria-expanded="false"
            aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
                </div>
    </h2>
    <div id="flush-collapseTwo{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingTwo{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[check][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numero du chèque</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[check][{?}][numbre]"
                placeholder="Numero du chèque" required>
            <label class="form-label">Bank</label>
            <input type="text" class="form-control form-control-sm"
                name="paiements[check][{?}][bank]"
                placeholder="Bank" required>
            <label class="form-label">date de reglement:</label>
            <input name="paiements[check][{?}][received_at]" type="date"
                class="form-control form-control-sm"
                aria-label="Text input with checkbox" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[check][{?}][type]"
                placeholder="Montant Payé" value="check">
        </div>
    </div>
</div>
</script>
<script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="false"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numero de Traite</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numero de la Lettre" required>
            <label class="form-label">Date de Traite</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[exchange][{?}][received_at]" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange" >
        </div>
    </div>
</div>
</script>
<script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="false"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm amount "
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé" required>
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numero du Virement</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement" required>
                <label class="form-label">Bank</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{?}][bank]"
                    placeholder="Bank" required>
            <label class="form-label">Date du Virement</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[transfer][{?}][received_at]" required>
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
        </div>
    </div>
</div>
</script>
    <script>
        $(function() {
            $(".repeat_items").repeatable1({
                addTrigger: "#addItem",
                deleteTrigger: "#deleteItem",
                max: 400,
                min: 2,
                template: "#itemsList",
                itemContainer: ".row",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
                total: {{ $noattributes }}
            });
        })
    </script>
@endsection
