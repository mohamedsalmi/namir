@extends('admin.layouts.app')

@section('title', 'Ajouter coupon')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Coupons</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.coupon.index') }}">Liste des coupons</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ajouter coupon</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.coupon.store',
            'method' => 'POST',
            'id' => 'aupdate-coupon-form',
        ]) !!}

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Ajouter coupon</h5>
                            <div class="ms-auto">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-3 required">
                                                <label class="form-label">Code</label>
                                                <input name="code" type="text" class="form-control form-control-sm @error('code') is-invalid @enderror" placeholder="Code"
                                                    value="{{ old('code', '') }}">
                                                @error('code')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-3 required">
                                                <label class="form-label">Réduction(%)</label>
                                                <input name="discount" step="0.01" type="number" class="form-control form-control-sm @error('discount') is-invalid @enderror"
                                                    placeholder="Réduction(%)" value="{{ old('discount', '') }}">
                                                @error('discount')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-3 required">
                                                <label class="form-label">Date d'éxpiration</label>
                                                <input name="expired_at" type="date" class="form-control form-control-sm @error('expired_at') is-invalid @enderror"
                                                    placeholder="expired_at" value="{{ old('expired_at', date('Y-m-d')) }}">
                                                @error('expired_at')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            
                                            <div class="col-12 col-lg-3 required">
                                                <label class="form-label">Etat</label>
                                                {{ Form::select('status', [1 => 'Activé', 0 => 'Désactivé'], old('status', 1), ['id' => 'status', 'name' => 'status', 'class' => $errors->has('status') ? 'form-control form-control-sm form-select form-select-sm is-invalid' : 'form-control form-control-sm form-select form-select-sm']) }}
                                                @error('status')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        {!! Form::close() !!}
    </main>
@endsection
