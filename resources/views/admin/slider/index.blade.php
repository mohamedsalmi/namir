@extends('admin.layouts.app')

@section('title', 'Liste des sliders')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">sliders</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des sliders</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center">
                    <h5 class="mb-2 mb-sm-0">Liste des sliders</h5>
                    @canany(['Ajouter slider'])
                        <div class="ms-auto">
                            <a class="btn btn-primary" href="{{ route('admin.slider.create') }}"><i class="fa fa-plus"
                                    aria-hidden="true" title="Ajouter"></i>Ajouter un slider</a>
                        </div>
                    @endcanany
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive mt-3">
                    <table id="example" class="table align-middle">
                        <thead class="table-secondary ">
                            <tr>
                                <th>Name</th>
                                <th>Slogan</th>
                                <th>Image</th>
                                <th>Offre</th>
                                <th>Remise</th>
                                <th>Exclusive</th>
                                <th class="not-export-col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sliders as $slider)
                                <tr>                                 
                                    <td>
                                        <p class="mb-0">{{ $slider->name }}</p>
                                    </td>
                                    <td>{{ $slider->slogan }}</td>
                                    <td> <img src="{{ $slider->image_url }}" class="w-50" width="44"
                                        height="44" alt=""></td>
                                    <td> {{ $slider->offre }}</td>

                                    <td>{{ $slider->discount }}</td>
                                    <td>
                                        @if ($slider->exclusive)
                                        <span class="badge rounded-pill bg-info text-dark">Exclusive</span>
                                    @endif
                                </td>
                                    <td>
                                        <div class="table-actions d-flex align-items-center gap-3 fs-6">
                                            {{-- <a href="javascript:;" class="text-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Views"><i class="bi bi-eye-fill"></i></a> --}}
                                            {{-- @canany(['Modifier slider']) --}}
                                                <a href="{{ route('admin.slider.edit', $slider->id) }}" class="text-warning"
                                                    data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i
                                                        class="bi bi-pencil-fill"></i></a>
                                            {{-- @endcanany --}}
                                            {{-- @canany(['Supprimer slider']) --}}
                                                <form method="POST" action="{{ route('admin.slider.delete', $slider->id) }}">
                                                    {{ csrf_field() }}
                                                    <input name="_method" type="hidden" value="GET">
                                                    <a id="show_confirm" type="submit"
                                                        class=" text-danger show_confirm" data-toggle="tooltip"
                                                        title="Supprimer"><i class="bi bi-trash-fill"></i></a>
                                                </form>
                                            {{-- @endcanany --}}
                                        </div>
                                    </td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            });
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
