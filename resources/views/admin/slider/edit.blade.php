@extends('admin.layouts.app')

@section('title', 'Modifier un slider')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
      <div class="breadcrumb-title pe-3">sliders</div>
      <div class="ps-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.slider.index')}}">Liste des sliders</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Modifier slider</li>
          </ol>
        </nav>
      </div>
    </div>
    <!--end breadcrumb-->
    {!! Form::open(array('enctype'=>'multipart/form-data','route' => ['admin.slider.update', $slider->id], 'method'=>'PUT', 'id'=>'update-slider-form'
    )) !!}

    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Modifier slider</h5>
              <div class="ms-auto">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row g-3">
              <div class="col-12 col-lg-8">
                <div class="card shadow-none bg-light border">
                  <div class="card-body">
                    <div class="row g-3">
                      <div class="col-12 col-lg-4 required">
                        <label class="form-label">Nom</label>
                        <input name="name"  type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom" value="{{ old('name', $slider->name) }}">
                        @error('name')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4 ">
                        <label class="form-label">slogan</label>
                        <input name="slogan"  type="text" class="form-control form-control-sm @error('slogan') is-invalid @enderror" placeholder="slogan" value="{{ old('slogan', $slider->slogan) }}">
                        @error('slogan')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">url</label>
                        <input name="url" type="text" class="form-control form-control-sm" placeholder="url" value="{{ old('url', $slider->url) }}">
                      </div>
                      <div class="col-12 col-lg-4 ">
                        <label class="form-label">Offre</label>
                        <input name="offre" type="text" class="form-control form-control-sm @error('offre') is-invalid @enderror" placeholder="offre" value="{{ old('offre', $slider->offre) }}">
                        @error('offre')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>

                      <div class="col-12 col-lg-4 required">
                        <label class="form-label">Remise</label>
                        <input name="discount" type="text" class="form-control form-control-sm @error('discount') is-invalid @enderror" placeholder="discount" value="{{ old('discount', $slider->discount) }}">
                        @error('discount')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12">
                        <label class="form-label">Image</label>
                        <input name="image" class="form-control form-control-sm" type="file">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-lg-4">
                <div class="card shadow-none bg-light border">
                  <div class="card-body">
                    <div class="row g-3">
                      <div class="col-12">
                        <div class="col-12 col-lg-4 ">
                          <label class="form-label">Coleur1</label>
                        <select class="form-select" name="color1" >
                          <option value="daily" {{$slider->color1 == 'daily' ? 'selected' :''}}>Rouge</option>
                          <option value="text-content"  {{$slider->color1 == 'text-content' ? 'selected' :''}}>Gris</option>
                          <option value="text-balck"  {{$slider->color1 == 'text-balck' ? 'selected' :''}}>Noir</option>
                          <option value="text-white"  {{$slider->color1 == 'text-white' ? 'selected' :''}}>blanc</option>
                        </select>
                       </div>
                      </div>
                      <div class="col-12">
                        <div class="col-12 col-lg-4 ">
                          <label class="form-label">Coleur2</label>
                        <select class="form-select" name="color2" >
                          <option value="daily" {{$slider->color1 == 'daily' ? 'selected' :''}}>Rouge</option>
                          <option value="text-content"  {{$slider->color2 == 'text-content' ? 'selected' :''}}>Gris</option>
                          <option value="text-balck"  {{$slider->color2 == 'text-balck' ? 'selected' :''}}>Noir</option>
                          <option value="text-white"  {{$slider->color2 == 'text-white' ? 'selected' :''}}>blanc</option>
                        </select>
                       </div>
                      </div>
                      <div class="col-12">
                        <div class="col-12 col-lg-4 ">
                          <label class="form-label">Coleur3</label>
                        <select class="form-select" name="color3" >
                          <option value="daily" {{$slider->color1 == 'daily' ? 'selected' :''}}>Rouge</option>
                          <option value="text-content"  {{$slider->color3 == 'text-content' ? 'selected' :''}}>Gris</option>
                          <option value="text-balck"  {{$slider->color3 == 'text-balck' ? 'selected' :''}}>Noir</option>
                          <option value="text-white"  {{$slider->color3 == 'text-white' ? 'selected' :''}}>blanc</option>
                        </select>
                       </div>
                      </div>
                      <div class="col-12">
                        <div class="col-12 col-lg-4 ">
                            <div class="col-12 col-lg-3"> <a class="form-check form-switch">
                                    <input class="form-check-input" name="exclusive"
                                        type="checkbox"  {{ $slider->exclusive == '1' ? 'checked' : '' }} >
                                    <label class="form-label" for="exampleFormControlInput1"><i
                                            class="fa fa-tag"></i>
                                        Exclusive </label>
                                </a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!--end row-->
                  </div>
                </div>
              </div>
            </div>
            <!--end row-->
          </div>
        </div>
      </div>
    </div>
    <!--end row-->
    {!! Form::close() !!}
</main>
@endsection
