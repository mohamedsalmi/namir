@extends('admin.layouts.app')

@section('title', 'Ajouter un slider')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Sliders</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.provider.index') }}">Liste des sliders</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ajouter nouveau slider</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.slider.store',
            'method' => 'POST',
            'id' => 'create-slider-form',
        ]) !!}

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Ajouter nouveau slider</h5>
                            <div class="ms-auto">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-8">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Nom du Slider</label>
                                                <input name="name" type="text"
                                                    class="form-control form-control-sm @error('name') is-invalid @enderror"
                                                    placeholder="Nom" value="{{ old('name', '') }}">
                                                @error('name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4 ">
                                                <label class="form-label">Url</label>
                                                <input name="url" type="text"
                                                    class="form-control form-control-sm @error('url') is-invalid @enderror"
                                                    placeholder="Code" value="{{ old('url', '') }}">
                                                @error('url')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label">Slogan</label>
                                                <input name="slogan" type="slogan" class="form-control form-control-sm"
                                                    placeholder="slogan" value="{{ old('slogan', '') }}">
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label">Offre</label>
                                                <input name="offre" type="text"
                                                    class="form-control form-control-sm @error('offre') is-invalid @enderror"
                                                    placeholder="offre" value="{{ old('offre', '') }}">
                                                @error('offre')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label">Remise</label>
                                                <input name="discount" type="text"
                                                    class="form-control form-control-sm @error('discount') is-invalid @enderror"
                                                    placeholder="Remise" value="{{ old('discount', '') }}">
                                                @error('discount')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Image</label>
                                                <input name="image" class="form-control form-control-sm" type="file"
                                                    single>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-4">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <div class="col-12 col-lg-4 ">
                                                    <label class="form-label">Coleur1</label>
                                                    <select class="form-select" name="color1">
                                                        <option value="daily">Rouge</option>
                                                        <option value="text-content" selected>Gris</option>
                                                        <option value="text-balck">Noir</option>
                                                        <option value="text-white">blanc</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="col-12 col-lg-4 ">
                                                    <label class="form-label">Coleur2</label>
                                                    <select class="form-select" name="color2">
                                                        <option value="daily">Rouge</option>
                                                        <option value="text-content" selected>Gris</option>
                                                        <option value="text-balck">Noir</option>
                                                        <option value="text-white">blanc</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="col-12 col-lg-4 ">
                                                    <label class="form-label">Coleur3</label>
                                                    <select class="form-select" name="color3">
                                                        <option value="daily" selected>Rouge</option>
                                                        <option value="text-content">Gris</option>
                                                        <option value="text-balck">Noir</option>
                                                        <option value="text-white">blanc</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="col-12 col-lg-4 ">
                                                    <div class="col-12 col-lg-3"> <a class="form-check form-switch">
                                                            <input class="form-check-input" name="exclusive"
                                                                type="checkbox">
                                                            <label class="form-label" for="exampleFormControlInput1"><i
                                                                    class="fa fa-tag"></i>
                                                                Exclusive </label>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end row-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        {!! Form::close() !!}
    </main>
@endsection
