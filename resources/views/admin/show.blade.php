    @extends('admin.layouts.public1')

    @section('title', 'Chercher')
    @section('stylesheet')
    
    @endsection
    <!--start wrapper-->
    <div class="wrapper">


        @section('content')
            <!--start content-->
            <main class="page-content">
                <!--breadcrumb-->

                <!--end breadcrumb-->

                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="row no-gutters">


                                    <div class="col-md-5 pr-2">
                                        <div class="card ">
                                    
                                                    <img
                                                            src="{{ asset($product->default_image_url) }}" /> 
                                        </div>

                                    </div>
                                    <div class="col-md-7">
                                        <div class="card p-3" style=" font-size: 20px; font-weight: 300;">
                                            <div class="row">
                                                <div class="col-6 ">
                                                    <h4 style=" color:#AADF15">{{ $product->name }} </h4>
                                                </div>
                                                <div class="col-6 ">
                                                    <h6 style="float: left;">réf : {{ $product->reference }}</h6>
                                                </div>
                                            </div>
                                            <div class="col ">
                                                <p><i class="bi bi-tag"></i>
                                                    الثمن : <span style=" color:#15c8df">{{ $product->selling1 }} دت</span>
                                                </p>
                                            </div>
                                            <div class="col ">
                                                <p><i class="bi bi-bookmark-check"></i>
                                                    الكمية : <span
                                                        style=" color:#15c8df">{{ $product->quantities->sum('quantity') }}</span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class=" card p-3 border border-dark " style=" color:#888; font-size: 20px; ">
                                            <p>
                                                الكاتب : {{ $auteur?->value }}</p>

                                            <p>
                                                المحقق : {{ $enqueteur?->value }}</p>

                                            <p>
                                                دار النشر : {{ $maison?->value }}</p>

                                            <p>
                                                الحجم : {{ $taille?->value }} </p>

                                            <p>
                                                الوزن : {{ $poid?->value }} </p>

                                            <p>
                                                نوعية الورق : {{ $papier?->value }}</p>

                                            <p>
                                                عدد المجلدات : {{ $tomes?->value }}</p>
                                            <p>
                                                عدد الصفحات : {{ $pages?->value }}</p>



                                        </div>
                                        {{-- <div class="card mt-2"> <span>كتب أخرى:</span>
                                            <div class="similar-products mt-2 d-flex flex-row">
                                                <div class="card border p-1" style="width: 9rem;margin-right: 3px;"> <img
                                                        src="assets/images/backgroud.jpg" class="card-img-top"
                                                        alt="...">
                                                    <div class="card-body">
                                                        <h6 class="card-title">$1,999</h6>
                                                    </div>
                                                </div>
                                                <div class="card border p-1" style="width: 9rem;margin-right: 3px;"> <img
                                                        src="assets/images/backgroud.jpg" class="card-img-top"
                                                        alt="...">
                                                    <div class="card-body">
                                                        <h6 class="card-title">$1,699</h6>
                                                    </div>
                                                </div>
                                                <div class="card border p-1" style="width: 9rem;margin-right: 3px;"> <img
                                                        src="assets/images/backgroud.jpg" class="card-img-top"
                                                        alt="...">
                                                    <div class="card-body">
                                                        <h6 class="card-title">$2,999</h6>
                                                    </div>
                                                </div>
                                                <div class="card border p-1" style="width: 9rem;margin-right: 3px;"> <img
                                                        src="assets/images/backgroud.jpg" class="card-img-top"
                                                        alt="...">
                                                    <div class="card-body">
                                                        <h6 class="card-title">$3,999</h6>
                                                    </div>
                                                </div>
                                                <div class="card border p-1" style="width: 9rem;"> <img
                                                        src="assets/images/backgroud.jpg" class="card-img-top"
                                                        alt="...">
                                                    <div class="card-body">
                                                        <h6 class="card-title">$999</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

            </main>


            <!--end page main-->

        @endsection
        @section('scripts')
            <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
            <script src='https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js'></script>
            <script>
                $('#lightSlider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 9
                });
            </script>
            <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
            @if (Session::has('success'))
                <script>
                    $(function() {
                        Swal.fire({
                            icon: 'success',
                            title: "{{ Session::get('success') }}",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    })
                </script>

                {{ Session::forget('success') }}
                {{ Session::save() }}
            @endif

            @if (Session::has('error'))
                <script>
                    $(function() {
                        Swal.fire({
                            icon: 'error',
                            title: "{{ Session::get('error') }}",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    })
                </script>
            @endif
            <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
            <script>
                function onScanSuccess(decodedText, decodedResult) {
                    //alert(`Code scanned = ${decodedText}`, decodedResult);
                    $('#sku').val(decodedText);
                    $('#camera').modal('hide');

                }

                $(document).on("click", "#barcode", function(e) {
                    const formatsToSupport = [
                        Html5QrcodeSupportedFormats.ITF
                    ];
                    var html5QrcodeScanner = new Html5QrcodeScanner(
                        "qr-reader", {
                            fps: 10,
                            qrbox: 250,
                            experimentalFeatures: {
                                useBarCodeDetectorIfSupported: false
                            }
                        });
                    html5QrcodeScanner.render(onScanSuccess);
                    html5QrcodeScanner.clear();
                })
            </script>
        @endsection
