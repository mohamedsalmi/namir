@extends('admin.layouts.app')

@section('title', 'Ajouter un client')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
      <div class="breadcrumb-title pe-3">Clients</div>
      <div class="ps-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.client.index')}}">Liste des clients</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter nouveau client</li>
          </ol>
        </nav>
      </div>
    </div>
    <!--end breadcrumb-->
    {!! Form::open(array('enctype'=>'multipart/form-data','route' => 'admin.client.store', 'method'=>'POST', 'id'=>'create-client-form'
    )) !!}

    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Ajouter nouveau client</h5>
              <div class="ms-auto">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row g-3">
              <div class="col-12 col-lg-8">
                <div class="card shadow-none bg-light border">
                  <div class="card-body">
                    <div class="row g-3">
                      <div class="col-12 col-lg-4 required">
                        <label class="form-label">Nom</label>
                        <input name="name"  type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom" value="{{ old('name', '') }}">
                        @error('name')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4 ">
                        <label class="form-label">Code</label>
                        <input name="client_code"  type="text" class="form-control form-control-sm @error('client_code') is-invalid @enderror" placeholder="Code" value="{{ old('client_code', '') }}">
                        @error('client_code')
                          <div class="invalid-feedback"> 
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">Email</label>
                        <input name="email" type="email" class="form-control form-control-sm" placeholder="Email" value="{{ old('email', '') }}">
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">Tél</label>
                        <input name="phone" type="text" class="form-control form-control-sm @error('phone') is-invalid @enderror" placeholder="Phone" value="{{ old('phone', '') }}">
                        @error('phone')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">Tél2</label>
                        <input name="phone2" type="text" class="form-control form-control-sm @error('phone2') is-invalid @enderror" placeholder="Phone2" value="{{ old('phone2', '') }}">
                        @error('phone2')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">MF</label>
                        <input name="mf" type="text" class="form-control form-control-sm @error('mf') is-invalid @enderror" placeholder="MF" value="{{ old('mf', '') }}">
                        @error('mf')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">RIB</label>
                        <input name="rib" type="text" class="form-control form-control-sm" placeholder="RIB" value="{{ old('rib', '') }}">
                      </div>
                      <div class="col-12 col-lg-4">
                        <label class="form-label">Adresse</label>
                        <input name="adresse" type="text" class="form-control form-control-sm" placeholder="Adresse" value="{{ old('adresse', '') }}">
                      </div>
                      <div class="col-12">
                        <label class="form-label">Avatar</label>
                        <input name="avatar" class="form-control form-control-sm" type="file">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-12 col-lg-4">
                <div class="card shadow-none bg-light border">
                  <div class="card-body">
                    <div class="row g-3">
                      <div class="col-12">
                        <div class="col-12 col-lg-4 required">
                          <label class="form-label">Type de Prix</label>
                          {{ Form::select('price',['detail'=>'Detail', 'semigros'=>'Semi-Gros', 'gros'=>'Gros'],old('price', 'detail'), ['id'=> 'price', 'name'=> 'price', 'class' => $errors->has('price') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}
                            @error('price')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                            @enderror
                       </div>
                      </div>
                      <div class="col-12">
                        <div class="col-12 col-lg-4 ">
                          <label class="form-label">Remise</label>
                          <input name="discount"  type="number" min="0" max="100" step="0.01" class="form-control form-control-sm remise @error('discount') is-invalid @enderror" placeholder="Remise max" value="{{ old('discount', '0') }}">
                          @error('discount')
                           <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                       </div>
                      </div>
                    </div>
                    <!--end row-->
                  </div>
                </div>
              </div>

            </div>
            <!--end row-->
          </div>
        </div>
      </div>
    </div>
    <!--end row-->
    {!! Form::close() !!}
</main>
@endsection
@section('scripts')
<script>
  $(document).on('blur', '.remise', function (e) {
    if (($(this).val() < 0) || ($(this).val() == '')) { $(this).val(0) }
    if (($(this).val() > 100)) { $(this).val(100) }
});
</script>
@endsection
