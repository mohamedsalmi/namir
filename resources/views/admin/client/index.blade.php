@extends('admin.layouts.app')

@section('title', 'Liste des clients')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Clients</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des clients</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="card">

            <div class="card">
                <div class="card-body">
                    <div class="card-header py-3">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Liste des clients</h5>
                            @canany(['Ajouter client'])
                                <div class="ms-auto">
                                    <a class="btn btn-primary" href="{{ route('admin.client.create') }}"><i class="fa fa-plus"
                                            aria-hidden="true" title="Ajouter"></i>Ajouter un client</a>
                                </div>
                            @endcanany
                        </div>
                    </div>
                    <div class="table-responsive mt-3">
                        <table id="example" class="table align-middle">
                            <thead class="table-secondary ">
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th>MF</th>
                                    <th>Type de prix</th>
                                    <th >Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>
                                            {{ $client->id }}
                                        </td>
                                        <td>{{ $client->client_code }}
                                            @if ($client->site)                                                
                                            <span class="badge bg-warning ">site</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center gap-3 cursor-pointer">
                                                <img src="{{ $client->image_url }}"
                                                    class="rounded-circle" width="44" height="44" alt="">
                                                <div class="">
                                                    <p class="mb-0">{{ $client->name }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $client->email }}</td>
                                        <td>{{ $client->adresse }}</td>
                                        <td> {{ $client->phone }}</td>
                                        <td>{{ $client->mf }}</td>
                                        <td>{{ $client->price }}</td>
                                        <td>
                                            <div class="table-actions d-flex align-items-center gap-3 fs-6">
                                                {{-- <a href="javascript:;" class="text-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Views"><i class="bi bi-eye-fill"></i></a> --}}
                                                @canany(['Modifier client'])
                                                    <a href="{{ route('admin.client.edit', $client->id) }}" class="text-warning"
                                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Modifier"><i
                                                            class="bi bi-pencil-fill"></i></a>
                                                @endcanany
                                                @canany(['Supprimer client'])
                                                <form method="POST" action="{{ route('admin.client.delete', $client->id) }}">
                                                    {{ csrf_field() }}
                                                    <input name="_method" type="hidden" value="GET">
                                                    <button id="show_confirm" type="submit"
                                                        class="btn text-danger show_confirm" data-toggle="tooltip"
                                                        title="Supprimer"><i class="bi bi-trash-fill"></i></button>
                                                </form>                                             
                                                  
                                                @endcanany
                                                {{-- @canany(['Supprimer client'])
                                                    <a href="{{ route('admin.client.pricing', $client->id) }}" class="text-info"
                                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Modifier les prix"><i
                                                        class="bi bi-currency-dollar"></i>
                                                    </a>
                                                @endcanany --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </main>
    <!--end page main-->
    @endsection

    @section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>

    <script src="{{asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js')}}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            });
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script> 
@endsection
