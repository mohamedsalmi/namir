@extends('admin.layouts.app')

@section('title', 'Liste des products')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Prix des produits</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i>Tableau de
                                bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Prix des produits</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center">
                    <h5 class="mb-2 mb-sm-0">Prix des produits</h5>
                    {{-- <div class="ms-auto">
                        {!! Form::open([
                            'enctype' => 'multipart/form-data',
                            'route' => ['client.updatepricing', $client->id],
                            'method' => 'POST',
                            'id' => 'update-pricing-form',
                            'class' => 'row g-3',
                        ]) !!}
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"
                                title="Ajouter"></i>Enregistrer</a>
                            {!! Form::close() !!}

                    </div> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive mt-3">
                    <table id="example" class="table align-middle">
                        <thead class="table-secondary ">
                            <tr>
                                <th>Nom produit</th>
                                <th>Remise %</th>
                                <th>Prix convention</th>
                                <th>Date convention</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $key => $product)
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center gap-3 cursor-pointer">
                                            <img src="{{ asset($product->default_image_url) }}" class="rounded-circle"
                                                width="44" height="44" alt="">
                                            <div class="">
                                                <p class="mb-0">{{ $product->name }} - {{ $product->reference }} -
                                                    {{ $product->sku }}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input id="product_id{{ $key }}" name="pricings[{{ $key }}][product_id]" type="hidden"
                                            value="{{ $product->id }}">
                                        <input id="remise{{ $key }}" data-id="{{ $key }}" min="0" max="100" pattern="^([0-9][0-9]?|)$"
                                            name="pricings[{{ $key }}][discount]" type="number"
                                            class="save form-control form-control-sm @error('pricings.' . $key . '.discount') is-invalid @enderror"
                                            placeholder="Remise %"
                                            value="{{ old('pricings.' . $key . '.discount', \App\Helpers\Helper::getProductPricingValue($product, $client->id, 'discount')) }}">
                                        @error('pricings.' . $key . '.discount')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </td>
                                    <td>
                                        <input id="price_cv{{ $key }}" data-id="{{ $key }}" name="pricings[{{ $key }}][price_cv]" type="number" step="0.001"
                                            class="save form-control form-control-sm @error('pricings.' . $key . '.price_cv') is-invalid @enderror"
                                            placeholder="Prix convention"
                                            value="{{ old('pricings.' . $key . '.price_cv', \App\Helpers\Helper::getProductPricingValue($product, $client->id, 'price_cv')) }}">
                                        @error('pricings.' . $key . '.price_cv')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </td>
                                    <td>
                                        <input id="date_cv{{ $key }}" data-id="{{ $key }}" name="pricings[{{ $key }}][date_cv]" type="date"
                                            class="save form-control form-control-sm @error('pricings.' . $key . '.date_cv') is-invalid @enderror"
                                            placeholder="Date convention"
                                            value="{{ old('pricings.' . $key . '.date_cv', \App\Helpers\Helper::getProductPricingValue($product, $client->id, 'date_cv')) }}">
                                        @error('pricings.' . $key . '.date_cv')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </td>


                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            });
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
    <script>
        var errors = null;
        $(document).on('blur', '.save', function(e, options = '') {
            e.preventDefault();
            var id=$(this).attr('data-id');
            var product_id=$('#product_id'+id).val();
            var discount=$('#remise'+id).val();
            var price_cv=$('#price_cv'+id).val();
            var date_cv=$('#date_cv'+id).val();
            var client_id={{$client->id}};
            var url = "/admin/client/"+client_id+"/updatepricing";
            if (errors) {
                $.each(errors, function(key, value) {
                    var element = document.getElementById(key+id);
                    if (element) {
                        element.classList.remove('is-invalid')
                    };
                });
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data:{product_id,discount,price_cv,date_cv},
                async: false,
                success: function(response, textStatus, jqXHR) {
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    errors = jqXHR.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        var element = document.getElementById(key+id);
                        if (element) {
                            element.classList.add('is-invalid')
                        };
                    });
                }
            });
        });
    </script>
@endsection
