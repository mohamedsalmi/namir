@extends('admin.layouts.app')

@section('title', 'Ajouter un point de vente')

@section('content')
<main class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
              <div class="breadcrumb-title pe-3">Points de vente</div>
              <div class="ps-3">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                      <a href="{{route('admin.store.list')}}">Liste points de vente</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Modifier un point de vente</li>
                  </ol>
                </nav>
              </div>
            </div>
            <!--end breadcrumb-->

              <div class="row">
                 <div class="col-lg-8 mx-auto">
                  <div class="card">
                    <div class="card-header py-3 bg-transparent"> 
                       <h5 class="mb-0">Modifier un point de vente</h5>
                      </div>
                    <div class="card-body">
                      <div class="border p-3 rounded">
                      {!! Form::open(array('enctype'=>'multipart/form-data','route' => ['admin.store.update', $store->id], 'method'=>'PUT', 'id'=>'create-store-form', 'class' => 'row g-3')) !!}
                        <div class="col-12 required">
                          <label class="form-label">Nom point de vente</label>
                          <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom point de vente" value="{{ old('name', $store->name) }}">
                          @error('name')
                           <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
{{--                        <div class="col-6 required">--}}
{{--                          <label class="form-label">Email</label>--}}
{{--                          <input name="email" type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', $store->email) }}">--}}
{{--                          @error('email')--}}
{{--                           <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                          @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-6 required">--}}
{{--                          <label class="form-label">Tél</label>--}}
{{--                          <input name="phone" type="phone" class="form-control form-control-sm @error('phone') is-invalid @enderror" placeholder="Tél" value="{{ old('phone', $store->phone) }}">--}}
{{--                          @error('phone')--}}
{{--                           <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                          @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-12 required">--}}
{{--                          <label class="form-label">Adresse</label>--}}
{{--                          <input name="address" type="text" class="form-control form-control-sm @error('address') is-invalid @enderror" placeholder="Adresse" value="{{ old('address', $store->address) }}">--}}
{{--                          @error('address')--}}
{{--                           <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                          @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-12">--}}
{{--                          <label class="form-label">Responsable</label>--}}
{{--                          {{ Form::select('manager_id', \App\Helpers\Helper::makeDropDownListFromCollection($managers, 'name') , old('name', $store->manager_id), ['id'=> 'manager_id', 'name'=> 'manager_id', 'class' => $errors->has('manager_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select'] ) }}--}}
{{--                                @error('manager_id')--}}
{{--                                <div class="invalid-feedback">--}}
{{--                                    {{ $message }}--}}
{{--                                </div>--}}
{{--                                @enderror--}}
{{--                        </div>--}}

                        <div class="col-12">
                            <label class="form-label" for="website_sales"><i class="fa fa-tag"></i>Vente en ligne :</label>
                            <div class="form-check form-switch">
                                <input type='hidden' value='0' name='website_sales'>
                                <input id="website_sales" class="form-check-input" {{old('website_sales', $store->website_sales) == 1 ? 'checked' : ''}} value="1" name="website_sales" type="checkbox">
                            </div>
                        </div>

                        <div class="col-12">
                          <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                        </div>
                        {!! Form::close() !!}
                      </div>
                      
                     </div>
                    </div>
                 </div>
              </div><!--end row-->

          </main>
          @endsection