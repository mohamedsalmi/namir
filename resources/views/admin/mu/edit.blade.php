@extends('admin.layouts.app')
@php
    if (!$mu->type) {
        $type = 'bs';
    } else {
        $type = $mu->type;
    }
@endphp
@section('title', 'Modifier BS')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{ url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css') }} " rel="stylesheet">
    <style>
        td {
            padding: 2px !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px;
            margin-left: 0px;
            display: inline;
            border: 0px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 0px;
            border-style: none;


        }

        .page-link {
            display: inline;
        }
    </style>
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Mouvement de stock</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i>
                                Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a
                                href="{{ route('admin.mu.index', $type) }}">Liste des {{ $type }} </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Modifier {{ $type }}</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['admin.mu.update', $mu->id],
            'method' => 'POST',
            'id' => 'create-mu-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Modifier {{ $type }}</h5>
                            <div class="ms-auto">
                                <div class="btn-group">
                                    <button type="submit" name="save" id="save"
                                        class="save btn btn-sm btn-outline-primary">Enregistrer</button>
                                    <button type="button"
                                        class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-split"
                                        data-bs-toggle="dropdown" aria-expanded="false"> <span
                                            class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" style="">
                                        <li><button type="submit" id="savewithprint" name="save&print" value="1"
                                                class="dropdown-item">Enregitrer & Imprimer PDF</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date"
                                                    class="form-control form-control-sm datetimepicker-input" name="date"
                                                    id="date" value="{{ $mu->date }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            {{-- <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><i class="fa fa-calendar"></i> PT de vente origin
                                                    :</label>
                                                    <input type="text" class="form-control" value="{{$mu->storeOrigin->name}}" readonly />
                                            </div> --}}
                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> PT de
                                                    vente destination
                                                    :</label>
                                                {{ Form::select('store_id', $store, $mu->store_destination, ['id' => 'store_destination', 'name' => 'store_destination', 'class' => $errors->has('store_id') ? 'form-control form-select form-select-sm is-invalid' : 'form-control form-select form-select-sm']) }}

                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="col-4">
                                                <label class="form-label">Note :</label>
                                                <input type="text" class="form-control form-control-sm" name="note"
                                                    placeholder="Note" id="note" id="note"
                                                    value="{{ $mu->note }}">
                                                @error('note')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <table id="products" class="table table-striped ">
                                                        <thead>
                                                            <tr>
                                                                {{-- <th style="width: 15% !important;">Code</th> --}}
                                                                <th width="70%">Nom du produit</th>
                                                                <th style="width: 7% !important;">Qté</th>
                                                                <th>Unité </th>
                                                                <th>Actions </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="dynamic_field">
                                                            <tr hidden grade="0">
                                                            </tr>
                                                            @if (isset($mu))
                                                                @php
                                                                    $items = $mu->items;
                                                                @endphp
                                                                @foreach ($items as $key => $item)
                                                                    <tr id="row{{ $key }}" class="item_voucher"
                                                                        grade="{{ $key }}"
                                                                        data-id="{{ $item->product_id }}">
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_code]"
                                                                            placeholder="Nom"
                                                                            class="form-control form-control-sm  name_list move"
                                                                            readonly
                                                                            value="{{ $item->product->reference }}" />

                                                                        <td>
                                                                            <input type="text"
                                                                                name="items[{{ $key }}][product_name]"
                                                                                placeholder="Nom"
                                                                                class="form-control form-control-sm  name_list move"
                                                                                value="{{ $item['product_name'] }}"
                                                                                readonly />
                                                                        </td>
                                                                        <td>
                                                                            <input type="number" style="width:80px;"
                                                                                name="items[{{ $key }}][product_quantity]"
                                                                                class="form-control form-control-sm  quantity move"
                                                                                id="quantity{{ $key }}"
                                                                                data-product_grade="{{ $key }}"
                                                                                data-product_id="{{ $key }}"
                                                                                value="{{ $item['product_quantity'] }}"
                                                                                min="0.001" max=""
                                                                                step="0.001">
                                                                        </td>

                                                                        {{-- <td>
                                                                            <input type="text"
                                                                                name="items[{{ $key }}][product_unity]"
                                                                                placeholder="Nom"
                                                                                class="form-control form-control-sm  name_list move"
                                                                                value="{{ $item['product_unity'] }}"
                                                                                readonly />
                                                                        </td>

                                                                        <td>
                                                                            <div class="form-check form-switch">

                                                                                <a type="button" name="remove"
                                                                                    id="{{ $key }}"
                                                                                    tr="{{ $key }}"
                                                                                    class="text-danger btn_remove delete"><i
                                                                                        class="bi bi-trash-fill"></i></a>
                                                                            </div>
                                                                        </td> --}}
                                                                        
                                                                        <td>
                                                                            <input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}" readonly/>
                                                                        </td>
                                                                        <td>
                                                                            <button type="button" name="remove" id="{{ $key }}" tr="{{ $key }}" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                        {{-- <tfoot>
                                                            <tr>
                                                                <td
                                                                    style="width: 100px !important; max-width: 100px !important;">
                                                                    <form id="barcode_search">
                                                                        <input type="text" id="barcode"
                                                                            name="" placeholder="Code à barre"
                                                                            style="width: 100px"
                                                                            class="form-control form-control-sm">
                                                                    </form>
                                                                </td>

                                                            </tr>

                                                        </tfoot> --}}
                                                    </table>
                                                    <div id="listproduct"></div>
                                                    {!! Form::close() !!}
                                            </div>

                                            <!--start filter-->
                                            <div class="card-body">
                                                <div class="card row g-3">
                                                    <h6> Recherche avancée</h6>
                                                    <div class="col-12 col-lg-12">
                                                        <div class="row g-3">
                                                            <div class="col-12 col-lg-3">
                                                                <label for="" class="form-label ">Nom</label>
                                                                <input class="form-control form-control-sm filter"
                                                                    id="product_name" placeholder="Nom" value=""
                                                                    autocomplete="product_name" autofocus>
                                                            </div>

                                                            <div class="col-12 col-lg-3">
                                                                <label for="" class="form-label ">Code à
                                                                    barre</label>

                                                                <div class="input-group mb-3">
                                                                    <input class="form-control form-control-sm filter"
                                                                        id="sku" placeholder="Code à barre"
                                                                        autocomplete="sku">
                                                                    <button data-bs-toggle="modal"
                                                                        data-bs-target="#camera" id='barcode'
                                                                        type="button"
                                                                        class="btn btn-sm btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="mt-3 mb-3 d-none">

                                                            <button type="button" id="filter"
                                                                class="btn btn-primary ">Filtrer</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="table-responsive">
                                                    <a id="addThis" role="button" class="btn btn-success">Ajout
                                                        Multiple</a>
                                                    <table id="example"
                                                        class="table align-middle table-striped dataTable"
                                                        style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:10px;    padding-left: 10px; iportant!">
                                                                    <div class="form-check"><input type="checkbox"
                                                                            class="form-check-input" id="check-all"></div>
                                                                </th>
                                                                {{-- <th class="is_filter">Référence</th> --}}
                                                                <th class="is_filter">Nom</th>
                                                                <th class="is_filter">Nom d'auteur</th>
                                                                <th class="is_filter">Maison</th>
                                                                <th class="is_filter">SKU</th>
                                                                <th class="is_filter">Qté</th>
                                                                <th class="is_filter">Prix Détail</th>
                                                                <th class="is_filter">Prix SemiGros</th>
                                                                <th width="100px">Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>

                                                    </table>
                                                </div>
                                            </div>
                                            <!--end filter-->

                                        </div>
                                    </div>
                                </div>
                                @if (isset($mu))
                                    <input name="mu_id" type="hidden" value="{{ $mu->id }}">
                                @endif
                            </div>


                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->

        <audio id="sucessAudio">
            <source src="{{ asset('assets/admin/plugins/notifications/sounds/mixkit-achievement-bell-600.wav') }}">
        </audio>

    </main>
    <!--end page main-->

    <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scanner code à bare</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="qr-reader"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/mu.js') }}"></script>
    <script src="{{ asset('assets/admin/js/doc.js') }}"></script>
    {{--    dataTables --}}
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/vfs_fonts.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script>
        $(document).on('click', '#addThis', function() {
            $("input:checkbox[name=product_id]:checked").each(function() {
                $('#addToCart' + $(this).val()).click();
            });
        });
    </script>
    <script type="text/javascript">
        var filter = {};

        //Price range input
        const setLabel = (lbl, val) => {
            const label = $(`#slider-${lbl}-label`);
            label.text(val);
            const slider = $(`#slider-div .${lbl}-slider-handle`);
            const rect = slider[0].getBoundingClientRect();
            label.offset({
                top: rect.top - 30,
                left: rect.left
            });
        }

        const setLabels = (values) => {
            setLabel("min", values[0]);
            setLabel("max", values[1]);
        }
        //Price range input

        function onFieldChange(e, fieldName) {
            filter.fieldName = e.target.value;
        }
        $(document).ready(function() {
            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: false,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.getData') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(8)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [{
                        data: 'check',
                        name: 'id',
                        width: '10px',
                        sortable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'writer',
                        name: 'writer'
                    },
                    {
                        data: 'publisher',
                        name: 'publisher'
                    },
                    {
                        data: 'sku',
                        name: 'sku'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                        orderable: true,
                    },
                    {
                        data: 'selling1',
                        name: 'selling1'
                    },
                    {
                        data: 'selling2',
                        name: 'selling2'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            $('div.toolbar').html(`<div class="accordion mt-2" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse">
                        Filtres <i class="bi bi-filter"></i>
                    </button>
                </h2>
                <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body p-2">
                        <form id="filter-form">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Nom</label>
                                    <input type="text" name="name" class="form-control form-control-sm filter-field" placeholder="Nom" id="name">
                                </div>

                                    <div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Auteur</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="writer" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>
<div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Maison</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="publisher" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>





                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">SKU</label>
                                    <input type="text" name="sku" class="form-control form-control-sm filter-field" placeholder="SKU" id="sku">
                                </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Etat</label>
                                    <select name="status" class="form-control form-control-sm filter-field" id="status">
                                        <option value="all" >Tous</option>
                                        <option selected value="1">Activé</option>
                                        <option value="0">Désactivé</option>
                                    </select>
                                </div>



            </div>






                                <div class=" col-sm-12 col-md-4 mt-2 float-left">
                                    <button class="btn btn-primary" type="button" id="apply-filter">Appliquer les filtres</button>
                                </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>`);



            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('name');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter)
            });

            $('#apply-filter').on('click', function(e) {
                const queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.product.getData'));
                table.ajax.url(url + queryString).load();
                table.draw();
                e.preventDefault();
            });



            $(".multiple-select").select2({
                width: '100%'
            });

            $('#ex2').slider().on('slide', function(ev) {
                setLabels(ev.value);
            });
            setLabels($('#ex2').attr("data-value").split(","));
        });
        $('#check-all').click(function() {
            var checked = this.checked;
            $('input[name="product_id"]').each(function() {
                this.checked = checked;
            });
        });
        lc_lightbox('.elem', {
            wrap_class: 'lcl_fade_oc',
            gallery: true,
            //  thumb_attr: 'data-lcl-thumb',
            fading_time: 100,
            skin: 'light',
            slideshow_time: 100,
            radius: 0,
            padding: 0,
            border_w: 0,
        });
    </script>
    {{--    dataTables --}}

    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            //alert(`Code scanned = ${decodedText}`, decodedResult);
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>
@endsection
