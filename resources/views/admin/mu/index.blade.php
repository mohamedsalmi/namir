@extends('admin.layouts.app')

@section('title', 'Liste des Bs')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    @php
    $type=isset($type)?$type:'bs';
    @endphp
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}} </div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i> Tableau de bord</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Liste des {{$type}}</li>
                        </ol>
                    </ol>
                </nav>
            </div>      
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des {{$type}}</h5>
                @if(in_array($type, ['bs', 'bc']))
                @canany([$type =='bs' ? 'Ajouter bon de sortie' : 'Ajouter bon de commande'])
                <div class="ms-auto">
                    <a class="btn btn-primary" href="{{ route('admin.mu.create',$type) }}"><i class="fa fa-plus" aria-hidden="true"
                            title="Ajouter"></i>Ajouter {{$type}}</a>
                </div>
                @endcanany
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">
                    <div class="card-header py-3">
                        <div class="row g-3">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="dataTable table align-middle w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th>Codification</th>
                                        {{-- @role('Admin')
                                        <th>Point de vente d'origine</th>
                                        <th>Point de vente destination</th> --}}
                                        {{-- @else --}}
                                        @if($type == 'br')
                                        <th>Point de vente d'origine</th>
                                        @else
                                        <th>Point de vente destination</th>
                                        @endif
                                        {{-- @endrole --}}
                                        <th>Etat</th>
                                        <th>Date</th>
                                        <th>Date de creation</th>
                                        <th>Creé par</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($mus as $mu)
                                        <tr>
                                            @role('Admin')
                                            <td>
                                                <p class="mb-0">{{ $mu->storeOrigin->name }}</p>
                                            </td>
                                            <td>
                                                <p class="mb-0">{{ $mu->storeDestination->name }}</p>
                                            </td>
                                            @else
                                            <td>
                                                <div class="d-flex align-items-center gap-3 cursor-pointer">
                                                    <div class="">
                                                        @if($mu->store_destination == auth()->user()->store_id)
                                                        <p class="mb-0">{{ $mu->storeOrigin->name }}</p>
                                                        @else
                                                        <p class="mb-0">{{ $mu->storeDestination->name }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                            @endrole
                                            @php
                                                if ($mu->status == 0) {
                                                    $status = '<td><span class="badge rounded-pill alert-danger">En cours</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill alert-success">Reçu </span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                            <td>{{ $mu->created_at }}</td>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 fs-6">
                                                    @canany(['Détails bon de sortie', 'Détails bon de commande', 'Détails bon de réception'])
                                                    <a href="{{ route('admin.mu.show', $mu->id) }}" class="text-primary"
                                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                                                        data-bs-original-title="View detail" aria-label="Views"><i
                                                            class="bi bi-eye-fill"></i></a>
                                                    @endcanany
                                                        @if(($mu->status == 0) && ($type == 'br'))
                                                        @canany(['Modifier bon de réception'])
                                                        <a href="{{ route('admin.mu.updatestatus',[$mu->id,$type]) }}" class="text-success"
                                                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                            title="" data-bs-original-title="confirmer"
                                                            aria-label="confirmer"><i class="lni lni-checkbox"></i></a>
                                                        @endcanany
                                                        @endif
                                                        @if(($mu->status == 0) && ($type == 'bcr'))
                                                        <a href="{{ route('admin.mu.create',['commande'=>$mu->id,'bs']) }}" class="text-success"
                                                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                            title="" data-bs-original-title="confirmer"
                                                            aria-label="confirmer"><i class="lni lni-checkbox"></i></a>
                                                        @endif
                                                        @canany(['Imprimer bon de sortie', 'Imprimer bon de commande', 'Imprimer bon de réception'])
                                                        <a href="{{ route('admin.mu.print',$mu->id) }}"
                                                            class="text-primary" data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom" title="" target="blank"
                                                            data-bs-original-title="Imprimer" aria-label="Views"><i
                                                                class="bi bi-printer-fill text-black"></i></a>
                                                        @endcanany

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach --}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end row-->
        {!! Form::close() !!}

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>
    <script>    
        $(document).ready(function() {
            $('#select-all').click(function() {
                var checked = this.checked;
                $('input[type="checkbox"]').each(function() {
                    this.checked = checked;
                });
            })
        });
    </script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
            @endif
          <script>
            $(document).ready(function() {
      
                var base_url = '{{ url(' / ') }}';
                var queryString = '';
      
                window.pdfMake.fonts = {
                    AEfont: {
                        normal: 'AEfont-Regular.ttf',
                        bold: 'AEfont-Regular.ttf',
                        italics: 'AEfont-Regular.ttf',
                        bolditalics: 'AEfont-Regular.ttf'
                    }
                };
                var table = $('.dataTable').DataTable({
                    "order": [
                        [0, "desc"]
                    ],
                    drawCallback: function(settings) {
                        $('#total').text(settings.json.total);
                    },
                    dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "Tous"]
                    ],
                    buttons: [
      
                        {
                            extend: 'excelHtml5',
                            text: "Excel",
                            exportOptions: {
                                columns: ':not(:last-child)',
                            },
                        },
      
                        {
                            extend: 'csvHtml5',
                            text: "CSV",
                            exportOptions: {
                                columns: ':not(:last-child)',
                            },
                        },
      
                        {
                            extend: 'pdfHtml5',
                            text: "PDF",
                            customize: function(doc) {
                                doc.defaultStyle = {
                                    font: 'AEfont',
                                    alignment: 'center',
                                    fontSize: 16,
                                }
                            },
                            exportOptions: {
                                columns: ':not(:last-child)',
                            },
      
      
                        },
      
                        {
                            extend: 'print',
                            text: "Imprimer",
                            exportOptions: {
                                columns: ':not(:last-child)',
                            },
                        },
      
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    "language": {
                        "sProcessing": "Traitement en cours",
                        "sZeroRecords": "Il n'y a pas de données",
                        "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                        "sInfoEmpty": "Aucune donnée",
                        "sInfoFiltered": "",
                        "sInfoPostFix": "",
                        "sSearch": "Rechercher",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "Premier",
                            "sPrevious": "Précédent",
                            "sNext": "Suivant",
                            "sLast": "Dernier",
                        }
                    },
                    ajax: "{{ route('admin.mu.index',['type'=>$type?$type:'']) }}",
                    createdRow: function(row, data, dataIndex) {
                        // Set the data-status attribute, and add a class
                        $(row).find('td:eq(2)')
                            .attr('style', "text-align: right;")
                    },
                    aoColumns: [
      
                     
                        {
                            data: 'codification',
                            name: 'codification',
                        },
                        {
                            data: 'storeDestination',
                            name: 'storeDestination',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'date',
                            name: 'date',
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                        },
                        {
                            data: 'created_by',
                            name: 'created_by',
                        },
      
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            paging: false,
                            searchable: false,
                            bSearchable: false,
                            exportable: false
                        },
                    ],
                    initComplete: function() {
                        this.api().columns('.is_filter').every(function() {
                            var column = this;
                            var input = document.createElement("input");
                            $(input).addClass("form-control form-control-sm")
                            $(input).appendTo($(column.footer()).empty())
                                .on('change', function() {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                        });
                    }
      
                });
                function filter() {
                    var queryString = '?' + new URLSearchParams(filter).toString();
                    var url = @json(route('admin.mu.index',[$type?$type:'']));
                    table.ajax.url(url + queryString).load();
                    table.draw();
                    // e.preventDefault();
                }
      
                $(document).on('change', '.filter-field', function(e) {
                    var key = $(this).attr('id');
                    var value = $(this).val();
                    console.log(value);
                    filter[key] = value;
                    console.log(filter);
                    filter();
                });
      
                $(document).on('change', '#clientfilter', function(e) {
                    var value = $(this).val();
                    filter['client'] = value;
                    console.log(filter);
                    filter();
                });
            });
        </script>

@endsection
