@extends('admin.layouts.app')

@section('title','BS '. $mu->codification)

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">{{config('stock.info.name')}} </div>
          <div class="ps-3">
            @php
            if(!$mu->type)$type='bs';
            else$type=$mu->type
            @endphp
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i> Tableau de bord</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.mu.index',$type) }}">Liste des {{$type}} </a></li>
                    <li class="breadcrumb-item active" aria-current="page">Détails de {{$type}}</li>
            </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$mu->date}}</h5>
                  <p class="mb-0">Bs N : {{$mu->codification}}</p>
                  @php
                  if ($mu->status == 0) {
                      $status = '<span class="badge rounded-pill alert-danger">En cours</span>';
                  } else {
                      $status = '<span class="badge rounded-pill alert-success">Reçu </span>';
                  }
                  echo $status;
              @endphp
                </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
           
                   <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-success border-0">
                            <i class="bi bi-truck text-success"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">BS info</h6>
                             <p class="mb-1"><strong>Origine</strong> {{ $mu->storeOrigin->name }}</p>                        
                            </div>
                       </div>
                       </div>
                      </div>
                   </div>
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-danger border-0">
                            <i class="bi bi-geo-alt text-danger"></i>
                          </div>
                          <div class="info">
                            <h6 class="mb-2">BS info</h6>
                            <p class="mb-1"><strong>Destination</strong> {{ $mu->storeDestination->name }}</p>
                          
                           </div>
                        </div>
                      </div>
                     </div>
                </div>
              </div><!--end row-->

              <div class="row">
                  <div class="col-12 col-lg-12">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                           <div class="table-responsive">
                             <table class="table align-middle mb-0">
                               <thead class="table-light">
                                 <tr>
                                   <th>Produit</th>
                                   {{-- <th>Prix unitaire</th> --}}
                                   <th>Quantité</th>
                                   {{-- <th>Total</th> --}}
                                 </tr>
                               </thead>
                               <tbody>
                                @foreach($muitems as $muitem)
                                 <tr>
                                   <td>
                                     <div class="orderlist">
                                      <a class="d-flex align-items-center gap-2" href="javascript:;">
                                        <div class="product-box">
                                            <img src="{{asset( $muitem->product->default_image_url ?? config('stock.image.default.product'))}}" alt="">
                                        </div>
                                        <div>
                                            <P class="mb-0 product-title">{{$muitem->product_name}}</P>

                                        </div>
                                       </a>
                                     </div>
                                   </td>
                                   {{-- <td>{{$muitem->product_price_buying}}  {{$muitem->product_currency_value}}</td> --}}
                                   <td>{{$muitem->product_quantity}}</td>
                                   {{-- <td>{{(float)$muitem->product_quantity*$muitem->product_price_buying}} {{$muitem->product_currency_value}} </td> --}}
                                 </tr>
                                 @endforeach
                               </tbody>
                             </table>
                           </div>
                       </div>
                     </div>
                  </div>

              </div><!--end row-->

            </div>
          </div>

      </main>
   <!--end page main-->
@endsection


