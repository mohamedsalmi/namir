@extends('admin.layouts.app')

@section('title', 'Liste des commandes')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Bons d'achat</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des bons d'achat</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des bons d'achat</h5>
                    <div class="col-md-3 offset-md-5">
                        <h4 class="mb-0 text-success"> Total: <span id="total"
                                data-column="3">{{ number_format($sum, 3) }}</span> </h4>
                    </div>  
                @canany(['Ajouter bon d\'achat'])
                    <div class="ms-auto">
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.voucher.create') }}"><i class="fa fa-plus"
                                aria-hidden="true" title="Ajouter"></i>Ajouter un bon d'achat</a>
                    </div>
                @endcanany
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">
                    {{-- <div class="card-header py-3">
                          <div class="row g-3">
                            <div class="col-lg-2 col-6 col-md-3">
                              <select class="form-select">
                                <option>Status</option>
                                <option>Active</option>
                                <option>Disabled</option>
                                <option>Pending</option>
                                <option>Show All</option>
                              </select>
                            </div>
                          </div>
                         </div> --}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="input-group col-md-6 mb-1">
                                    <span class="input-group-text">Du</span>
                                    <input required type="date" class="filter-field form-control form-control-sm" name="from"
                                        placeholder="De" id="from">
                                    <span class="input-group-text">Au</span>
                                    <input required type="date" class="filter-field form-control form-control-sm" name="to"
                                        placeholder="Au" id="to">
                                    <span class="input-group-text">Provider</span>
                                    <select class="form-control " name="" id="providerfilter">
                                        <option value="">Tous</option>
                                        @foreach (\App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Provider(), 'name') as $key => $provider)
                                            <option value="{{ $key }}">{{ $provider }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <table class="dataTable table align-middle" style="width: 100%;">
                                <thead class="table-light">
                                    <tr>
                                        <th>Numéro</th>
                                        <th>Fournisseur</th>
                                        <th>Prix</th>
                                        <th>Etat</th>
                                        <th>Date</th>
                                        <th>Creé par</th>
                                        <th>Modifié par</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <input type="hidden" id="date-column" value="3">
        <input type="hidden" id="client-column" value="1">

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/sale-datatable.js') }}"></script>
    <script>
        $(document).ready(function() {

            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                drawCallback: function(settings) {
                    $('#total').text(settings.json.total);
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.voucher.index') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(3)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [

                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'provider',
                        name: 'provider',
                    },
                    {
                        data: 'total',
                        name: 'total',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                    {
                        data: 'updated_by',
                        name: 'updated_by',
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });

            function filter() {
                var queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.voucher.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                // e.preventDefault();
            }

            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('id');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
                filter();
            });

            $(document).on('change', '#providerfilter', function(e) {
                var value = $(this).val();
                filter['provider'] = value;
                console.log(filter);
                filter();
            });
        });
    </script>

@endsection
