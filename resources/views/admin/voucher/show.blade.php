@extends('admin.layouts.app')

@section('title','Article')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Bons d'achat</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="{{route('admin.voucher.index')}}">Liste des bons d'achat</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Détails bon d'achat</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$voucher->created_date}}</h5>
                  <p class="mb-0">Bon d'achat N° {{$voucher->codification}}</p>
                </div>  
              </div>
             </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                   <div class="col">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-primary border-0">
                            <i class="bi bi-person text-primary"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Fournisseur</h6>
                             <p class="mb-1">{{$voucher->provider_details['name']}}</p>
                             <p class="mb-1">{{$voucher->provider_details['adresse']}}</p>
                             <p class="mb-1">{{$voucher->provider_details['phone']}}</p>
                          </div>
                       </div>
                       </div>
                     </div>
                   </div>
                   <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-success border-0">
                            <i class="bi bi-truck text-success"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Paiement</h6>
                             <p class="mb-1"><strong>Expédition</strong> : {{config('stock.info.name')}} </p>
                             <p class="mb-1"><strong>Methode de Paiement </strong> :{{$voucher?->commitments->first()?->paiements->first()->type}}</p>
                             @php
                                 $voucher_status='';
                                if($voucher?->commitments?->first()?->paiements?->SUM('amount') >= $voucher->total)
                                {
                                    $voucher_status=' Payé';
                                }else{
                                    $voucher_status=' Non payé';
                                }
                             @endphp
                             <p class="mb-1"><strong>Statut</strong> {{$voucher_status}} </p>
                          </div>
                       </div>
                       </div>
                      </div>
                   </div>
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-danger border-0">
                            <i class="bi bi-geo-alt text-danger"></i>
                          </div>
                          <div class="info">
                            <h6 class="mb-2">Livrer à</h6>
                            <p class="mb-1"><strong>Adresse</strong> : {{$voucher->provider_details['adresse']}}</p>
                          </div>
                        </div>
                      </div>
                     </div>
                </div>
              </div><!--end row-->

              <div class="row">
                <div class="col-12 col-lg-8">
                   <div class="card border shadow-none radius-10">
                     <div class="card-body">
                         <div class="table-responsive">
                           <table class="table align-middle mb-0">
                             <thead class="table-light">
                               <tr>
                                 <th>Produit</th>
                                 <th>Prix unitaire</th>
                                 <th>Quantité</th>
                                 <th>Total</th>
                               </tr>
                             </thead>
                             <tbody>
                              @php
                              $total_tva = 0;
                              $total_ut = 0;
                          @endphp
                              @foreach($voucheritems as $voucheritem)
                              @php
                              $pu=($voucheritem->product_price_buying*100)/(100+$voucheritem->product_tva);
                              $couttva=($pu * $voucheritem->product_tva) / 100;
                              $total_ut += $pu * $voucheritem->product_quantity;
                              $total_tva += $couttva * $voucheritem->product_quantity;
                          @endphp
                               <tr>
                                 <td>
                                   <div class="orderlist">
                                    <a class="d-flex align-items-center gap-2" href="javascript:;">
                                      <div class="product-box">
                                          <img src="{{ asset( $voucheritem->product->default_image_url ?? config('stock.image.default.product')) }}" alt="">
                                      </div>
                                      <div>
                                          <P class="mb-0 product-title">{{$voucheritem->product_name}}</P>
                                      </div>
                                     </a>
                                   </div>
                                 </td>
                                 <td>{{$voucheritem->product_price_buying}}  {{$voucheritem?->product_currency_value ?? ''}}</td>
                                 <td>{{$voucheritem->product_quantity}}</td>
                                 <td>{{(float)$voucheritem->product_quantity*$voucheritem->product_price_buying}} {{$voucheritem?->product_currency_value ?? ''}} </td>
                               </tr>
                               @endforeach
                             </tbody>
                           </table>
                         </div>
                     </div>
                   </div>
                </div>
                <div class="col-12 col-lg-4">
                  <div class="card border shadow-none bg-light radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-4">
                           <div>
                              <h5 class="mb-0">Récapitulatif de la bl</h5>
                           </div>
                        </div>
                          <div class="d-flex align-items-center mb-3">

                              <div>
                                  <p class="mb-0">TOTAL HT</p>
                                </div>
                                <div class="ms-auto">
                                  <h5 class="mb-0">{{ number_format($voucher->total - $total_tva, 3) }}  </h5>
                              </div>

                        </div>
                          <div class="d-flex align-items-center mb-3">

                              <div>
                                  <p class="mb-0">TOTAL TVA</p>
                                </div>
                                <div class="ms-auto">
                                  <h5 class="mb-0">{{ number_format($total_tva, 3) }}  </h5>
                              </div>
                        </div>
                          <div class="d-flex align-items-center mb-3">

                            <div>
                              <p class="mb-0">TTC</p>
                            </div>
                            <div class="ms-auto">
                              <h5 class="mb-0">{{$voucher->total}}  </h5>
                          </div>
                        </div>

                    </div>
                  </div>


               </div>
            </div><!--end row-->

          </div>
        </div>
      </main>
   <!--end page main-->
@endsection


