@extends('admin.layouts.app')

@section('title', 'Créer BL')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.css"
        integrity="sha512-2sFkW9HTkUJVIu0jTS8AUEsTk8gFAFrPmtAxyzIhbeXHRH8NXhBFnLAMLQpuhHF/dL5+sYoNHWYYX2Hlk+BVHQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .ms-container {
            width: 100%;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px;
            margin-left: 0px;
            display: inline;
            border: 0px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 0px;
            border-style: none;
        }

        .page-link {
            display: inline;
        }
    </style>
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Bons d'achat</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.voucher.index') }}">Liste des bons d'achat</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Créer un bon d'achat</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.voucher.store',
            'method' => 'POST',
            'id' => 'create-voucher-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Créer un bon d'achat</h5>
                            <div class="ms-auto">
                                <div class="btn-group">
                                    <button type="submit" name="save" id="save"
                                        class="save btn btn-outline-primary">Enregistrer</button>
                                    <button type="button"
                                        class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split"
                                        data-bs-toggle="dropdown" aria-expanded="false"> <span
                                            class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" style="">
                                        <li><button type="submit" id="savewithprint" name="save&print" value="1"
                                                class="dropdown-item">Enregitrer & Imprimer PDF</button>
                                        <li><button type="submit" id="savewithticket" name="save&ticket" value="1"
                                                class="dropdown-item">Enregitrer & Imprimer ticket</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-12 col-lg-4">
                                                            <label class="form-label">Fournisseur:</label>
                                                            <select class="single-select form-select-sm" id="fidel_list"
                                                                name="provider_id">
                                                                <option value="0" class="text-primary"
                                                                    tel-fidel="11111111" AD-fidel="adresse"
                                                                    MF-fidel='matricule' name-fidel="Fornisseur"
                                                                    Prix-fidel="detail">Fornisseur</option>
                                                                @foreach ($providers as $provider)
                                                                    <option value="{{ $provider->id }}"
                                                                        name-fidel="{{ $provider->name }}"
                                                                        tel-fidel="{{ $provider->phone }}"
                                                                        MF-fidel="{{ $provider->mf }}"
                                                                        AD-fidel="{{ $provider->address }}"
                                                                        Prix-fidel="{{ $provider->price }}">
                                                                        {{ $provider->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-lg-4">
                                                            <label class="form-label"><span style="color: red;">*</span>Nom
                                                                :</label>
                                                            <input type="text" class="form-control form-control-sm "
                                                                name="provider_details[name]" placeholder="Nom & Prénom"
                                                                id="name"
                                                                value="{{ old('provider_details.name', '') }}">

                                                            <div class="invalid-feedback d-none"></div>

                                                        </div>
                                                        <div class="col-12 col-lg-4">
                                                            <label class="form-label"><span
                                                                    style="color: red;">*</span>Adresse
                                                                :</label>
                                                            <input type="text" style="display: block;"
                                                                class="form-control form-control-sm "
                                                                name="provider_details[adresse]" placeholder="Adresse"
                                                                id="adresse"
                                                                value="{{ old('provider_details.adresse', '') }}">

                                                            <div class="invalid-feedback d-none"></div>

                                                        </div>
                                                        <div class="col-12 col-lg-4">
                                                            <label class="form-label"><span style="color: red;">*</span>MF
                                                                :</label>
                                                            <input type="text" style="display: block;"
                                                                class="form-control form-control-sm MF "
                                                                name="provider_details[mf]" placeholder="MF"
                                                                id="mf"
                                                                value="{{ old('provider_details.adresse', '') }}">

                                                        </div>
                                                        <div class="col-12 col-lg-4">
                                                            <label class="form-label"><span
                                                                    style="color: red;">*</span>Téléphone
                                                                :</label>
                                                            <input type="text" style="display: block;"
                                                                class="form-control form-control-sm mobile "
                                                                name="provider_details[phone]" placeholder="phone"
                                                                id="phone"
                                                                value="{{ old('provider_details.adresse', '') }}">

                                                        </div>
                                                        <div class="form-group col-12 col-lg-4">
                                                            <label class="form-label" for="exampleFormControlInput1"><span
                                                                    style="color: red;">*</span><i
                                                                    class="fa fa-calendar"></i> Date
                                                                :</label>
                                                            <input type="date"
                                                                class="form-control form-control-sm datetimepicker-input  "
                                                                name="date"
                                                                value="{{ old('provider_details.date', '' . date('Y-m-d') . '') }}" />
                                                            <div class="invalid-feedback d-none"></div>

                                                        </div>
                                                        <div class="form-group col-12 col-lg-4">
                                                            <label class="form-label">Note:</label>
                                                            <input type="text" class="form-control form-control-sm "
                                                                name="note" id="note"
                                                                value="{{ old('note', '') }}" />
                                                            @error('note')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                        <div class="col-12 col-lg-12">

                                                            <div class="row g-3">
                                                                <h6 class=" text-center text-info" id="paiements">Détails
                                                                    de paiement
                                                                </h6>
                                                                <div class="invalid-feedback d-none"></div>
                                                                <div class="col-4">
                                                                    <div class="form-check form-switch">
                                                                        {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                                        <input class="form-check-input"
                                                                            name="Deadline_check" type="checkbox"
                                                                            id="Deadline_check">
                                                                        <label class="form-check-label"
                                                                            for="Deadline">Echéance</label>
                                                                    </div>
                                                                    <button type="button" name="Add_paiement"
                                                                        id="Add_paiement" class="btn btn-primary "
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#paiment_modal">+ Créer les écheances</button>
                                                                </div>
                                                                <div class="form-group col-2 col-lg-4">

                                                                    <div class="form-check form-switch">
                                                                        {{-- <input type='hidden' value='0' name='Deadline'> --}}
                                                                        <input class="form-check-input" name="timbre"
                                                                            type="checkbox">
                                                                        <label class="form-label"
                                                                            for="exampleFormControlInput1"><i
                                                                                class="fa fa-tag"></i> Timbre </label>
                                                                    </div>

                                                                </div>
                                                                <div class="modal fade" id="paiment_modal" tabindex="-1"
                                                                    aria-hidden="true">
                                                                    <div
                                                                        class="modal-dialog modal-dialog-scrollable modal-xl">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="col-4">
                                                                                    <h5 class="modal-title text-info">Liste
                                                                                        des écheances
                                                                                    </h5>
                                                                                </div>
                                                                                <h6 class="form-label m-3">Montant total à
                                                                                    payer :</h6>
                                                                                <input type="text"
                                                                                    class="btn btn-warning px-5 radius-30 total"
                                                                                    value="0" readonly>
                                                                                <button type="button" class="btn-close"
                                                                                    data-bs-dismiss="modal"
                                                                                    aria-label="Close"></button>

                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="repeat_items">
                                                                                </div>

                                                                                <div class="ms-auto">
                                                                                    <button role="button"
                                                                                        class="btn btn-sm btn-warning"
                                                                                        id="addItem"
                                                                                        onclick="return 0;"><i
                                                                                            class="bi bi-plus"></i> Ajouter une écheance</button>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-bs-dismiss="modal">Terminer</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-12" id="cash">
                                                                    <div class="row row-cols-auto g-1">
                                                                        <div class="col"><button type="button"
                                                                                class="btn btn-sm btn-success px-2 rounded-pill"
                                                                                id="addCash">
                                                                                <i class="bi bi-currency-dollar"></i>
                                                                                Espèce</button></div>

                                                                        <div class="col"><button type="button"
                                                                                class="btn btn-sm btn-primary px-2 rounded-pill"
                                                                                id="addCheck">
                                                                                <i class="bi bi-card-heading"></i>
                                                                                Chèque</button></div>
                                                                        <div class="col pr-1"><button type="button"
                                                                                class="btn btn-sm btn-secondary px-2 rounded-pill"
                                                                                id="addTransfer">
                                                                                <i class="bi bi-bank2"></i>
                                                                                Virement</button></div>
                                                                        <div class="col pr-1"><button type="button"
                                                                                class="btn btn-sm btn-danger px-2 rounded-pill"
                                                                                id="addExchange">
                                                                                <i class="bi bi-file-earmark-text"></i>
                                                                                Traite</button>
                                                                        </div>

                                                                        {{-- <div class="col pr-1"><button type="button"
                                                                        class="btn btn-sm btn-warning px-2 rounded-pill" id="addRA">
                                                                        <i class="bi bi-arrow-return-left"></i> Retour</button></div> --}}
                                                                    </div>
                                                                    <div class="row mt-3">
                                                                        <div class="accordion accordion-flush "
                                                                            id="accordionFlushExample">
                                                                            <div class="repeat_cash">

                                                                            </div>
                                                                            <div class="repeat_check repeat">

                                                                            </div>
                                                                            <div class="repeat_transfer">

                                                                            </div>
                                                                            <div class="repeat_exchange">

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                {{-- <div class="col-12">
                                                                       <label class="form-label">Curency</label>
                                                                       {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name') , null, ['id'=> 'currency_id', 'name'=> 'currency_id', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select'] ) }}
                                                                       @error('currency_id')
                                                                       <div class="invalid-feedback">
                                                                           {{ $message }}
                                                                       </div>
                                                                       @enderror
                                                                  </div>
                                                                  <div class="col-12">
                                                                       <label class="form-label">Montant Payé</label>
                                                                       <input type="text" class="form-control" placeholder="Montant Payé">
                                                                  </div> --}}
                                                                <!--si le type ne pas cash-->
                                                                {{-- <div class="col-12">
                                                                           <label class="form-label">Image</label>
                                                                           <input class="form-control" type="file">
                                                                      </div> --}}

                                                            </div>
                                                            <!--end row-->

                                                        </div>
                                                </div>

                                            </div>
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            <th>Remise(%)</th>
                                                            <th>TVA(%)</th>
                                                            <th>Prix d'achat</th>
                                                            <th>Total</th>
                                                            <th>Màj les prix</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = old('items', []);
                                                            $noitems = is_array($items) ? count($items) : 0;
                                                        @endphp
                                                        @if ($noitems > 0)
                                                            @foreach ($items as $key => $item)
                                                                <tr id="row{{ $key }}" class="item_voucher"
                                                                    grade="{{ $key }}">
                                                                    <td> <input type="hidden"
                                                                            name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_price_selling]"value="{{ $item['product_price_selling'] }}" />
                                                                        <input type="text"
                                                                            name="items[{{ $key }}][product_name]"
                                                                            placeholder="Nom"
                                                                            class="form-control form-control-sm name_list"
                                                                            readonly
                                                                            value="{{ $item['product_name'] }}" />
                                                                    </td>
                                                                    <td><input type="number" style="width:80px;"
                                                                            name="items[{{ $key }}][product_quantity]"
                                                                            class="form-control form-control-sm quantity"
                                                                            id="quantity{{ $key }}"
                                                                            data-product_grade="{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_quantity'] }}"
                                                                            min="1" max=""></td>
                                                                    <td><input type="text" style="width: 70px;"
                                                                            class="form-control form-control-sm"
                                                                            name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                            readonly /></td>
                                                                    <td><input type="number"
                                                                            name="items[{{ $key }}][product_remise]"
                                                                            style="width: 80px;" min="0"
                                                                            max="100" step="0.01"
                                                                            class="form-control form-control-sm remise"
                                                                            id="remise{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_remise'] }}"></td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_tva]"
                                                                            style="width: 50px;"
                                                                            class="form-control form-control-sm tva"
                                                                            id="tva{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_tva'] }}" readonly>
                                                                    </td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_price_buying]"
                                                                            style="width: 100px;"
                                                                            class="form-control form-control-sm price{{ $key }} "
                                                                            id="price{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_price_buying'] }}"
                                                                            readonly></td>
                                                                    <td><input type="text" style="width: 100px;"
                                                                            class="form-control form-control-sm price{{ $key }} "
                                                                            id="L_total{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['L_total'] }}" readonly></td>
                                                                    <td><button type="button" name="remove"
                                                                            id="{{ $key }}"
                                                                            tr="{{ $key }}"
                                                                            class="btn btn-danger btn-sm btn_remove delete"><i
                                                                                class="bi bi-trash-fill"></i></button></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>
                                                                {{-- <select class="single-select" id="product_list" name="product_id">
                                                                <option disabled selected>Choisir un produit</option>
                                                                @foreach ($products as $product)
                                                                <option product-id="{{ $product->id }}" product-name="{{ $product->name }}" price-buying="{{ $product->buying }}" 
                                                                    price-selling1="{{ $product->selling1 }}" price-selling2="{{ $product->selling2 }}" price-selling3="{{ $product->selling3 }}" product-unity="{{ $product->unity }}" product-tva="{{ $product->tva }}"
                                                                    product-sku="{{ $product->sku }}" product-currency="{{ $product->currency->name }}"  
                                                                    product-currency-id="{{ $product->currency->id }}" product-pricetype="{{ $product->price_type['type']}}"                                                                     
                                                                    >{{ $product->name }} - {{$product->reference}} - {{$product->sku}} </option>
                                                                @endforeach
                                                            </select> --}}
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', '0') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>

                                        </div>
                                        {!! Form::close() !!}

                                    </div>
                                    <div class="card-body">
                                        <div class="card row g-3">
                                            <h6> Recherche avancée</h6>
                                            <div class="col-12 col-lg-12">
                                                <div class="row g-3">
                                                    <div class="col-12 col-lg-3">
                                                        <label for="" class="form-label ">Nom</label>
                                                        <input class="form-control form-control-sm filter"
                                                            id="product_name" placeholder="Nom" value=""
                                                            autocomplete="product_name" autofocus>
                                                    </div>

                                                    <div class="col-12 col-lg-3">
                                                        <label for="" class="form-label ">Code à barre</label>

                                                        <div class="input-group mb-3">
                                                            <input class="form-control form-control-sm filter"
                                                                id="sku" placeholder="Code à barre"
                                                                autocomplete="sku">
                                                            <button data-bs-toggle="modal" data-bs-target="#camera"
                                                                id='barcode' type="button"
                                                                class="btn btn-sm btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="mt-3 mb-3 d-none">

                                                    <button type="button" id="filter"
                                                        class="btn btn-primary ">Filtrer</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <a id="addThis" role="button" class="btn btn-success">Ajout Multiple</a>
                                            <table id="example" class="table align-middle table-striped dataTable"
                                                style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width:10px;    padding-left: 10px; iportant!">
                                                            <div class="form-check"><input type="checkbox"
                                                                    class="form-check-input" id="check-all"></div>
                                                        </th>
                                                        {{-- <th class="is_filter">Référence</th> --}}
                                                        <th class="is_filter">Nom</th>
                                                        <th class="is_filter">Nom d'auteur</th>
                                                        <th class="is_filter">Maison</th>
                                                        <th class="is_filter">SKU</th>
                                                        <th class="is_filter">Qté</th>
                                                        <th class="is_filter">Prix Détail</th>
                                                        <th class="is_filter">Prix SemiGros</th>
                                                        <th width="100px">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        <audio id="sucessAudio">
            <source src="{{ asset('assets/admin/plugins/notifications/sounds/mixkit-achievement-bell-600.wav') }}">
        </audio>

    </main>
    <!--end page main-->

    <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scanner code à bare</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="qr-reader"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/voucher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.js"
        integrity="sha512-X1iMoI6a2IoZFOheUVf3ZmcD1L7zN/eVtig6enIq8yBlwDcbPVao/LG8+/SdjcVn72zF+A/viRLPSxfXLu/rbQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    {{--    dataTables --}}
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/vfs_fonts.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script>
        $(document).on('click', '#addThis', function() {
            $("input:checkbox[name=product_id]:checked").each(function() {
                $('#addToCart' + $(this).val()).click();
            });
        });
    </script>
    <script type="text/javascript">
        var filter = {};

        //Price range input
        const setLabel = (lbl, val) => {
            const label = $(`#slider-${lbl}-label`);
            label.text(val);
            const slider = $(`#slider-div .${lbl}-slider-handle`);
            const rect = slider[0].getBoundingClientRect();
            label.offset({
                top: rect.top - 30,
                left: rect.left
            });
        }

        const setLabels = (values) => {
            setLabel("min", values[0]);
            setLabel("max", values[1]);
        }
        //Price range input

        function onFieldChange(e, fieldName) {
            filter.fieldName = e.target.value;
        }
        $(document).ready(function() {
            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: false,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.getData') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(8)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [{
                        data: 'check',
                        name: 'id',
                        width: '10px',
                        sortable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'writer',
                        name: 'writer'
                    },
                    {
                        data: 'publisher',
                        name: 'publisher'
                    },
                    {
                        data: 'sku',
                        name: 'sku'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                        orderable: true,
                    },
                    {
                        data: 'selling1',
                        name: 'selling1'
                    },
                    {
                        data: 'selling2',
                        name: 'selling2'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            $('div.toolbar').html(`<div class="accordion mt-2" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse">
                        Filtres <i class="bi bi-filter"></i>
                    </button>
                </h2>
                <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body p-2">
                        <form id="filter-form">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Nom</label>
                                    <input type="text" name="name" class="form-control form-control-sm filter-field" placeholder="Nom" id="name">
                                </div>

                                    <div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Auteur</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="writer" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>
<div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">Maison</label>

                                        <input class="form-control form-control-sm filter-field attribute_values" name="publisher" id="attribute_values-1" data-select2-id="attribute_values-1">
                                    </div>





                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">SKU</label>
                                    <input type="text" name="sku" class="form-control form-control-sm filter-field" placeholder="SKU" id="sku">
                                </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Etat</label>
                                    <select name="status" class="form-control form-control-sm filter-field" id="status">
                                        <option value="all" >Tous</option>
                                        <option selected value="1">Activé</option>
                                        <option value="0">Désactivé</option>
                                    </select>
                                </div>



            </div>






                                <div class=" col-sm-12 col-md-4 mt-2 float-left">
                                    <button class="btn btn-primary" type="button" id="apply-filter">Appliquer les filtres</button>
                                </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>`);



            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('name');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter)
            });

            $('#apply-filter').on('click', function(e) {
                const queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.product.getData'));
                table.ajax.url(url + queryString).load();
                table.draw();
                e.preventDefault();
            });



            $(".multiple-select").select2({
                width: '100%'
            });

            $('#ex2').slider().on('slide', function(ev) {
                setLabels(ev.value);
            });
            setLabels($('#ex2').attr("data-value").split(","));
        });
        $('#check-all').click(function() {
            var checked = this.checked;
            $('input[name="product_id"]').each(function() {
                this.checked = checked;
            });
        });
        lc_lightbox('.elem', {
            wrap_class: 'lcl_fade_oc',
            gallery: true,
            //  thumb_attr: 'data-lcl-thumb',
            fading_time: 100,
            skin: 'light',
            slideshow_time: 100,
            radius: 0,
            padding: 0,
            border_w: 0,
        });
    </script>
    {{--    dataTables --}}

    <script>
        $('#my_multi_select2').multiSelect();
    </script>
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            //alert(`Code scanned = ${decodedText}`, decodedResult);
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>

<script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
<script type="text/template" id="itemsList">
    <div class="card bg-light row pt-2 mx-1">
        <div>
        <button type="button" class="btn-close" id="deleteItem"></button>
        </div>
        <div  class="card-body ">
            <h6 class="text-info">Echéance {?}:</h6>
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <br>
                        <h6>Montant:</h6>
                    </div>
                </div>
                <div class=" col-lg-7 ">
                    <div class="input-group mb-3">
                        <input id="Deadline.{?}.amount" name="Deadline[{?}][amount]" type="number" class="form-control form-control-sm Deadline" aria-label="Text input with checkbox">
                        <div class=" col-4">
                            {{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'Deadline[{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-5">
                    <div>
                        <br>
                        <h6>Date d'écheance:</h6>
                    </div>
                </div>
                <div class=" col-lg-7 ">
                        <input id="Deadline.{?}.date" min="{{ date('Y-m-d') }}" name="Deadline[{?}][date]" type="date" class="form-control form-control-sm" aria-label="Text input with checkbox">
                    </div>
             </div>
        </div>
    </div>

</script>
<script type="text/template" id="cashdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingOne{?}">
<div class="row">
<button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCash"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseOne{?}"
aria-expanded="true"
aria-controls="flush-collapseOne{?}" value="Espéces {?}">
</div>
</h2>
<div id="flush-collapseOne{?}"
class="accordion-collapse collapse show"
aria-labelledby="flush-headingOne{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Devise</label>
{{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-select is-invalid' : 'form-control form-select']) }}
@error('currency_id')
<div class="invalid-feedback">
{{ $message }}
</div>
@enderror
<label class="form-label ">Montant Payé</label>
<input type="number" class="form-control amount"
name="paiements[cash][{?}][amount]" step="0.001"
placeholder="Montant Payé" required>
<label class="form-label ">Date</label>
<input type="date"
    class="form-control"
    name="paiements[cash][{?}][received_at]"
    placeholder="Date"
    value="{{ date('Y-m-d') }}">
<input type="hidden" class="form-control" name="paiements[cash][{?}][due_at]" value="{{date('Y-m-d')}}">
<input type="hidden" class="form-control "
name="paiements[cash][{?}][type]"
placeholder="Montant Payé" value="cash">
<input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
</div>
</div>
</div>
</script>
    <script type="text/template" id="checkdetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingTwo{?}">
<div class="row">
<button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteCheck"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseTwo{?}"
aria-expanded="true"
aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
</div>
</h2>
<div id="flush-collapseTwo{?}"
class="accordion-collapse collapse show"
aria-labelledby="flush-headingTwo{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control amount"
name="paiements[check][{?}][amount]" step="0.001"
placeholder="Montant Payé" required>
<input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

<label class="form-label">Numéro du chèque</label>
<input type="number" class="form-control"
name="paiements[check][{?}][numbre]"
placeholder="Numéro du chèque" required>

<label class="form-label">Bank</label>
{{ Form::select('paiements[check][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[check][{?}][bank]', 'name' => 'paiements[check][{?}][bank]', 'class' => $errors->has('paiements.check.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
<label class="form-label">Date de réglement:</label>
<input name="paiements[check][{?}][due_at]" type="date" value="{{ date('Y-m-d') }}"
class="form-control"
aria-label="Text input with checkbox" required>
<label class="form-label ">Date</label>
<input type="date"
    class="form-control"
    name="paiements[check][{?}][received_at]"
    placeholder="Date"
    value="{{ date('Y-m-d') }}">
<input type="hidden" class="form-control"
name="paiements[check][{?}][type]"
placeholder="Montant Payé" value="check">

</div>
</div>
</div>
</script>

    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="true"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control amount" step="0.001"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numéro de traite</label>
            <input type="number" class="form-control"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numéro de traite">
            <label class="form-label">Date de traite</label>
            <input type="date" class="form-control" value="{{ date('Y-m-d') }}"
                name="paiements[exchange][{?}][due_at]">
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[exchange][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
                <input type="hidden" class="form-control"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn btn-sm bx bx-trash col-2 text-danger delete" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="true"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse show"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control amount" step="0.001"
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numéro du Virement</label>
            <input type="number" class="form-control"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement">
                <label class="form-label">Bank</label>
                {{ Form::select('paiements[transfer][{?}][bank]', \App\Helpers\Helper::makeDropDownListFromModel2(new \App\Models\Bank(), 'name', true), old('bank', ''), ['id' => 'paiements[transfer][{?}][bank]', 'name' => 'paiements[transfer][{?}][bank]', 'class' => $errors->has('paiements.transfer.{?}.bank') ? 'form-control form-select is-invalid' : 'form-control form-select', 'required']) }}
            <label class="form-label">Date du réglement</label>
            <input type="date" class="form-control" value="{{ date('Y-m-d') }}"
                name="paiements[transfer][{?}][due_at]">
                <input type="hidden" class="form-control"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
                <label class="form-label ">Date</label>
                <input type="date"
                    class="form-control"
                    name="paiements[transfer][{?}][received_at]"
                    placeholder="Date"
                    value="{{ date('Y-m-d') }}">
        </div>
    </div>
</div>
</script>

<script>
    function getDate(added_months) {
        var date = new Date();
        var newDate = new Date(date.setMonth(date.getMonth() + added_months));
        var currentDate = newDate.toISOString().substring(0, 10);
        return currentDate;
    }

    function divideCommitments(total_items) {
        var total = $('.total').val();
        for (let index = 1; index <= total_items; index++) {
            let amount = document.getElementById('Deadline.' + index + '.amount');
            let date = document.getElementById('Deadline.' + index + '.date');
            date.value = getDate(index - 1);
            if (index == total_items) {
                amount.value = (Math.floor(total / total_items) + total % total_items).toFixed(3);
            } else {
                amount.value = (Math.floor(total / total_items)).toFixed(3);
            }
        }
    }

    function initAmount(method, item) {
        var input = item.find(".amount");
        input.focus();
        var total = $('.total').val();
        var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
        var amounts_count = $('.amount').length;
        if (amounts_count <= 1) {
            first_amount.val(total);
        }
    }

    function initRSAmount(method, item) {
        var input = item.find(".amount");
        input.focus();
        var total = $('.total').val();
        var first_amount = $('input[name="paiements[' + method + '][1][amount]"]');
        var amounts_count = $('.amount').length;
        first_amount.val(total / 100);
    }
</script>

<script>
    $(function() {
        $(".repeat_items").repeatable1({
            addTrigger: "#addItem",
            deleteTrigger: "#deleteItem",
            max: 400,
            min: 2,
            template: "#itemsList",
            itemContainer: ".row",
            afterAdd: function(item) {
                var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                divideCommitments(totalItems);
            },
            afterDelete: function() {
                var totalItems = ($(".repeat_items").find(".row").length) / 3 || 0;
                divideCommitments(totalItems);
            },
        });
    });
    $(function() {
        $(".repeat_check").repeatable({
            addTrigger: "#addCheck",
            deleteTrigger: "#deleteCheck",
            max: 400,
            min: 0,
            template: "#checkdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('check', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_cash").repeatable2({
            addTrigger: "#addCash",
            deleteTrigger: "#deleteCash",
            max: 400,
            min: 0,
            template: "#cashdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('cash', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_exchange").repeatable3({
            addTrigger: "#addExchange",
            deleteTrigger: "#deleteExchange",
            max: 400,
            min: 0,
            template: "#exchangedetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('exchange', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });
    $(function() {
        $(".repeat_transfer").repeatable4({
            addTrigger: "#addTransfer",
            deleteTrigger: "#deleteTransfer",
            max: 400,
            min: 0,
            template: "#transferdetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                initAmount('transfer', item);
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
            },

        });
    });

    $(function() {
        $(".repeat_ra").repeatable6({
            addTrigger: "#addRA",
            deleteTrigger: "#deleteRA",
            max: 400,
            min: 0,
            template: "#radetail",
            itemContainer: ".accordion-item",
            afterAdd: function(item) {
                filterReturnedAmounts();
            },
            afterDelete: function() {
                var amount = $(".amount");
                amount.focus();
                filterReturnedAmounts();
            },

        });
    });
</script>
@endsection
