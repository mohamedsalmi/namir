@extends('admin.layouts.app')

@section('title','Article')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Détails BL</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i>Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.inventory.index') }}">Liste des BLs</a></li>
                <li class="breadcrumb-item active" aria-current="page">Détails de la bl</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->
          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$inventory->created_date}}</h5>
                  <p class="mb-0">N° BL : #{{$inventory->codification}}</p>
                </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                       <div class="d-flex align-items-center gap-3">
                         <div class="icon-box bg-light-primary border-0">
                           <i class="bi bi-person text-primary"></i>
                         </div>
                         <div class="info">
                            <h6 class="mb-2">Utilisateur</h6>
                            <p class="mb-1">{{$inventory->user->name}}</p>
                         </div>
                      </div>
                      </div>
                    </div>
                  </div>
              
                
              </div><!--end row-->

              <div class="row">
                  <div class="col-12 col-lg-12">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                           <div class="table-responsive">
                             <table class="table align-middle mb-0">
                               <thead class="table-light">
                                 <tr>
                                   <th>Produit</th>
                                   <th>Quantité anciene</th>
                                   <th>Quantité Enrigitrée</th>
                                   <th>marge</th>
                                   {{-- <th>motif</th> --}}
                                 </tr>
                               </thead>
                               <tbody>                       
                                @foreach($inventory->items as $item)                               
                                 <tr>
                                   <td>
                                     <div class="orderlist">
                                      <a class="d-flex align-items-center gap-2" href="javascript:;">
                                        <div class="product-box">
                                            <img src="{{ asset( $item->product->default_image_url ?? config('stock.image.default.product')) }}" alt="">
                                        </div>
                                        <div>
                                            <P class="mb-0 product-title">{{$item->product_name}}</P>
                                        </div>
                                       </a>
                                     </div>
                                   </td>
                                   <td>{{$item->product_old_quantity}}</td>
                                   <td>{{$item->product_new_quantity}}</td>
                                   <td>{{$item->product_old_quantity-$item->product_new_quantity}}</td>
                                   {{-- <td>{{$item->pattern}}</td> --}}
                                 </tr>
                                 @endforeach
                               </tbody>
                             </table>
                           </div>
                       </div>
                     </div>
                  </div>               
              </div><!--end row-->

            </div>
          </div>

      </main>
   <!--end page main-->
@endsection


