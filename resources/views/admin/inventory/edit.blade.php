@extends('admin.layouts.app')

@section('title', 'Créer Invaentaire')

@section('stylesheets')
    <link href="{{ url('assets/admin/plugins/datatable/css/jquery.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/dataTables.bootstrap4.min.css') }} " rel="stylesheet">
    <link href="{{ url('assets/admin/plugins/datatable/css/responsive.dataTables.min.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.css"
        integrity="sha512-2sFkW9HTkUJVIu0jTS8AUEsTk8gFAFrPmtAxyzIhbeXHRH8NXhBFnLAMLQpuhHF/dL5+sYoNHWYYX2Hlk+BVHQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .ms-container {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{ App\Helpers\Helper::getTitle('inventory', 'create') }}</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                    class="bx bx-home-alt"></i>Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a
                                href="{{ route('admin.inventory' . '.index') }}">{{ App\Helpers\Helper::getTitle('inventory', 'list') }}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            {{ App\Helpers\Helper::getTitle('inventory', 'create') }}</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => ['admin.inventory.update', $inventory->id],
            'method' => 'POST',
            'id' => 'create-inventory-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">{{ App\Helpers\Helper::getTitle('inventory', 'create') }}</h5>
                            <div class="ms-auto">
                                <div class="btn-group">
                                    <button type="submit" name="save" id="save"
                                        class="save btn btn-outline-primary">Enregistrer</button>
                                    <button type="button"
                                        class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split"
                                        data-bs-toggle="dropdown" aria-expanded="false"> <span
                                            class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" style="">
                                        <li><button type="submit" id="savewithprint" name="save&print" value="1"
                                                class="dropdown-item">Enregitrer & Imprimer PDF</button>
                                        <li><button type="submit" id="savewithticket" name="save&ticket" value="1"
                                                class="dropdown-item">Enregitrer & Imprimer ticket</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-12">
                                <div class="form-group col-12 col-lg-2">
                                    <label class="form-label">Note:</label>
                                    <input type="text" class="form-control form-control-sm " name="note"
                                        id="note"
                                        value="{{ old('note', $inventory->note ? $inventory->note : '') }}" />
                                    @error('note')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Quantité Réelle</th>
                                                            <th>Quantité Enregistrée</th>
                                                            <th>Marge</th>
                                                            <th>Unité</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @php
                                                            $items = $inventory->items;
                                                        @endphp
                                                        @foreach ($items as $key => $item)
                                                            <tr>
                                                            <tr id="row{{ $key }}" class="item_cart"
                                                                grade="{{ $key }}"
                                                                data-id="{{ $item['product_id'] }}">
                                                                <td>
                                                                    <input type="hidden"
                                                                        name="items[{{ $key }}][product_id]"
                                                                        value="{{ $item['product_id'] }}" />
                                                                    <input type="text"
                                                                        name="items[{{ $key }}][product_name]"
                                                                        class="form-control form-control-sm name_list"
                                                                        readonly value="{{ $item['product_name'] }}" />
                                                                </td>
                                                                <td>
                                                                    <input type="number" style="width:80px;"
                                                                        name="items[{{ $key }}][product_new_quantity]"
                                                                        class="form-control form-control-sm new_quantity"
                                                                        oninput="this.value = Math.floor(this.value);"
                                                                        step="1" id="new_quantity{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_new_quantity'] }}"
                                                                        min="0" max="">
                                                                </td>
                                                                <td>
                                                                    <input type="number" style="width:80px;"
                                                                        name="items[{{ $key }}][product_old_quantity]"
                                                                        class="form-control form-control-sm "
                                                                        oninput="this.value = Math.floor(this.value);"
                                                                        step="1"
                                                                        id="old_quantity{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_old_quantity'] }}"
                                                                        readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="number" style="width:80px;"
                                                                        name="items[{{ $key }}][product_marge_quantity]"
                                                                        class="form-control form-control-sm marge_quantity '+ classe +'"
                                                                        oninput="this.value = Math.floor(this.value);"
                                                                        step="1"
                                                                        id="marge_quantity{{ $key }}"
                                                                        data-product_grade="{{ $key }}"
                                                                        data-product_id="{{ $key }}"
                                                                        value="{{ $item['product_new_quantity'] - $item['product_old_quantity'] }}"
                                                                        readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="text" style="width: 70px;"
                                                                        class="form-control form-control-sm"
                                                                        name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                        readonly />
                                                                </td>
                                                                <td>
                                                                    <button type="button" name="remove"
                                                                        id="{{ $key }}"
                                                                        tr="{{ $key }}"
                                                                        class="btn btn-danger btn-sm btn_remove delete"><i
                                                                            class="bi bi-trash-fill"></i></button>
                                                                </td>
                                                            </tr>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    {{-- <tfoot>
                                                        <tr>
                                                            <td> <select class="single-select" id="product_list" name="product_id">
                                                                <option disabled selected>Choisir un produit</option>
                                                                @foreach ($products as $product)
                                                                <option product-id="{{ $product->id }}" product-name="{{ $product->name }}" product-unity="{{ $product->unity }}"  old_quantity="{{ $product->quantities?->where('store_id',auth()->user()->store->id)->sum('quantity') }}"
                                                                    >{{ $product->name }} - {{$product->reference}} - {{$product->sku}} </option>
                                                                @endforeach
                                                            </select></td>                                                            
                                                        </tr>
                                                    </tfoot>                                             --}}
                                                </table>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>

                                    <div class="card-body">
                                        <div class="card row g-3">
                                            <h6> Recherche avancée</h6>
                                            <div class="col-12 col-lg-12">
                                                <div class="row g-3">
                                                    <div class="col-12 col-lg-3">
                                                        <label for="" class="form-label ">Nom</label>
                                                        <input class="form-control form-control-sm filter"
                                                            id="product_name" placeholder="Nom" value=""
                                                            autocomplete="product_name" autofocus>
                                                    </div>

                                                    <div class="col-12 col-lg-3">
                                                        <label for="" class="form-label ">Code à barre</label>

                                                        <div class="input-group mb-3">
                                                            <input class="form-control form-control-sm filter"
                                                                id="sku" placeholder="Code à barre"
                                                                autocomplete="sku">
                                                            <button data-bs-toggle="modal" data-bs-target="#camera"
                                                                id='barcode' type="button"
                                                                class="btn btn-sm btn-outline-warning text-primary bx bx-barcode-reader"></button>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="mt-3 mb-3 d-none">

                                                    <button type="button" id="filter"
                                                        class="btn btn-primary ">Filtrer</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <a id="addThis" role="button" class="btn btn-success">Ajout Multiple</a>
                                            <table id="example" class="table align-middle table-striped dataTable"
                                                style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width:10px;    padding-left: 10px; iportant!">
                                                            <div class="form-check"><input type="checkbox"
                                                                    class="form-check-input" id="check-all"></div>
                                                        </th>
                                                        {{-- <th class="is_filter">Référence</th> --}}
                                                        <th class="is_filter">Nom</th>
                                                        <th class="is_filter">Nom d'auteur</th>
                                                        <th class="is_filter">Maison</th>
                                                        <th class="is_filter">SKU</th>
                                                        <th class="is_filter">Qté</th>
                                                        <th class="is_filter">Prix Détail</th>
                                                        <th class="is_filter">Prix SemiGros</th>
                                                        <th width="100px">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        <!--end form-->
        <audio id="sucessAudio">
            <source src="{{ asset('assets/admin/plugins/notifications/sounds/mixkit-achievement-bell-600.wav') }}">
        </audio>
    </main>
    <!--end page main-->

    <div class="modal fade" id="camera" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scanner code à bare</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="qr-reader"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/inventory.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.js"
        integrity="sha512-X1iMoI6a2IoZFOheUVf3ZmcD1L7zN/eVtig6enIq8yBlwDcbPVao/LG8+/SdjcVn72zF+A/viRLPSxfXLu/rbQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{--    dataTables --}}
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.responsive.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/vfs_fonts.js') }}"></script>

    <script src="{{ asset('assets/admin/plugins/lc_lightbox/js/lc_lightbox.lite.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/lc_lightbox/lib/AlloyFinger/alloy_finger.min.js') }}"></script>
    <script>
        $(document).on('click', '#addThis', function() {
            $("input:checkbox[name=product_id]:checked").each(function() {
                $('#addToCart' + $(this).val()).click();
            });
        });
    </script>
    <script type="text/javascript">
        var filter = {};

        //Price range input
        const setLabel = (lbl, val) => {
            const label = $(`#slider-${lbl}-label`);
            label.text(val);
            const slider = $(`#slider-div .${lbl}-slider-handle`);
            const rect = slider[0].getBoundingClientRect();
            label.offset({
                top: rect.top - 30,
                left: rect.left
            });
        }

        const setLabels = (values) => {
            setLabel("min", values[0]);
            setLabel("max", values[1]);
        }
        //Price range input

        function onFieldChange(e, fieldName) {
            filter.fieldName = e.target.value;
        }
        $(document).ready(function() {
            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Blf<"toolbar">rti<"bottom-wrapper"p>',
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: false,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.product.getData') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(8)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [{
                        data: 'check',
                        name: 'id',
                        width: '10px',
                        sortable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'writer',
                        name: 'writer'
                    },
                    {
                        data: 'publisher',
                        name: 'publisher'
                    },
                    {
                        data: 'sku',
                        name: 'sku'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                        orderable: true,
                    },
                    {
                        data: 'selling1',
                        name: 'selling1'
                    },
                    {
                        data: 'selling2',
                        name: 'selling2'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });

            $('div.toolbar').html(`<div class="accordion mt-2" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse">
                    Filtres <i class="bi bi-filter"></i>
                </button>
            </h2>
            <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body p-2">
                    <form id="filter-form">
                        <div class="row">
                            <div class="form-group col-sm-12 col-md-4">
                                <label class="form-label">Nom</label>
                                <input type="text" name="name" class="form-control form-control-sm filter-field" placeholder="Nom" id="name">
                            </div>

                                <div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Auteur</label>

                                    <input class="form-control form-control-sm filter-field attribute_values" name="writer" id="attribute_values-1" data-select2-id="attribute_values-1">
                                </div>
<div class="form-group col-sm-12 col-md-4">
                                    <label class="form-label">Maison</label>

                                    <input class="form-control form-control-sm filter-field attribute_values" name="publisher" id="attribute_values-1" data-select2-id="attribute_values-1">
                                </div>





                            <div class="form-group col-sm-12 col-md-4">
                                <label class="form-label">SKU</label>
                                <input type="text" name="sku" class="form-control form-control-sm filter-field" placeholder="SKU" id="sku">
                            </div>

                            <div class="form-group col-sm-12 col-md-4">
                                <label class="form-label">Etat</label>
                                <select name="status" class="form-control form-control-sm filter-field" id="status">
                                    <option value="all" >Tous</option>
                                    <option selected value="1">Activé</option>
                                    <option value="0">Désactivé</option>
                                </select>
                            </div>



        </div>






                            <div class=" col-sm-12 col-md-4 mt-2 float-left">
                                <button class="btn btn-primary" type="button" id="apply-filter">Appliquer les filtres</button>
                            </div>
            </div>
    </form>
</div>
</div>
</div>
</div>`);



            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('name');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter)
            });

            $('#apply-filter').on('click', function(e) {
                const queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.product.getData'));
                table.ajax.url(url + queryString).load();
                table.draw();
                e.preventDefault();
            });



            $(".multiple-select").select2({
                width: '100%'
            });

            $('#ex2').slider().on('slide', function(ev) {
                setLabels(ev.value);
            });
            setLabels($('#ex2').attr("data-value").split(","));
        });
        $('#check-all').click(function() {
            var checked = this.checked;
            $('input[name="product_id"]').each(function() {
                this.checked = checked;
            });
        });
        lc_lightbox('.elem', {
            wrap_class: 'lcl_fade_oc',
            gallery: true,
            //  thumb_attr: 'data-lcl-thumb',
            fading_time: 100,
            skin: 'light',
            slideshow_time: 100,
            radius: 0,
            padding: 0,
            border_w: 0,
        });
    </script>
    {{--    dataTables --}}

    <script>
        $('#my_multi_select2').multiSelect();
    </script>
    <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            //alert(`Code scanned = ${decodedText}`, decodedResult);
            $('#sku').val(decodedText);
            $('#camera').modal('hide');

        }

        $(document).on("click", "#barcode", function(e) {
            const formatsToSupport = [
                Html5QrcodeSupportedFormats.ITF
            ];
            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", {
                    fps: 10,
                    qrbox: 250,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: false
                    }
                });
            html5QrcodeScanner.render(onScanSuccess);
            html5QrcodeScanner.clear();
        })
    </script>
@endsection
