@extends('admin.layouts.app')

@section('title', 'Liste des inventaires')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}}</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i> Tableau de
                                bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des inventaires</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des inventaires</h5>
                @canany(['Ajouter inventaire'])
                    <div class="ms-auto">
                        <a class="btn btn-primary" href="{{ route('admin.inventory.create') }}"><i class="fa fa-plus"
                                aria-hidden="true" title="Ajouter"></i>Ajouter un inventaire</a>
                    </div>
                @endcanany
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">

                    <div class="card-body">
                        <div class="table-responsive">
                            <table  class="dataTable table align-middle" style="width: 100%;">
                                <thead class="table-light">
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        {{-- <th>
                                            <div class="form-check"><input type="checkbox" class="form-check-input"
                                                    id="select-all"></div>
                                        </th> --}}
                                        <th>codification</th>
                                        <th>Creé par</th>
                                        <th>Modifié par</th>
                                        <th>Date</th>
                                        <th>Date de creation</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($inventories as $inventory)
                                        <tr>
                                            <td>
                                                {{ $inventory->codification }}
                                            </td>
                                            <td>
                                                {{ $inventory->user->name }}
                                            </td>
                                            <td>{{ $inventory->created_at }}</td>
                                            <td>
                                                <div class="d-flex align-items-center gap-3 fs-6">
                                                    @canany(['Détails inventaire'])
                                                        <a href="{{ route('admin.inventory.show', $inventory->id) }}"
                                                            class="text-primary" data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom" title=""
                                                            data-bs-original-title="Voir les détails" aria-label="Views"><i
                                                                class="bi bi-eye-fill"></i></a>
                                                    @endcanany
                                                    @canany(['Modifier inventaire'])
                                                        <a href="{{ route('admin.inventory.edit', $inventory->id) }}"
                                                            class="text-warning" data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom" title=""
                                                            data-bs-original-title="Modifier" aria-label="Edit"><i
                                                                class="bi bi-pencil-fill"></i></a>
                                                    @endcanany
                                                    @canany(['Supprimer inventaire'])
                                                        <form method="POST"
                                                            action="{{ route('admin.inventory.delete', $inventory->id) }}"
                                                            class="delete_cart">
                                                            {{ csrf_field() }}
                                                            <input name="_method" type="hidden" value="GET">
                                                            <a href="javascript:;" id="show_confirm" type="submit"
                                                                class="text-danger show_confirm" data-bs-toggle="tooltip"
                                                                data-bs-placement="bottom" title=""
                                                                data-bs-original-title="Supprimer" aria-label="Supprimer"><i
                                                                    class="bi bi-trash-fill"></i></a>
                                                        </form>
                                                    @endcanany
                                                    @canany(['Imprimer inventaire'])
                                                        <a href="{{ route('admin.inventory.print', $inventory->id) }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="bottom" title=""
                                                            target="blank" data-bs-original-title="Imprimer Inventaire"
                                                            aria-label="Views"><i class="bi bi-printer-fill text-black"></i>
                                                        </a>
                                                    @endcanany



                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach --}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end row-->

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/table-datatable.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#select-all').click(function() {
                var checked = this.checked;
                $('input[type="checkbox"]').each(function() {
                    this.checked = checked;
                });
            })
        });
    </script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif

    <script type="text/javascript">
        $(document).on('click', '.confirm-delete', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.inventory.index') }}",
                aoColumns: [

                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                    {
                        data: 'updated_by',
                        name: 'updated_by',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });

            function filter() {
                var queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.cart.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                // e.preventDefault();
            }

            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('id');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
                filter();
            });

            $(document).on('change', '#clientfilter', function(e) {
                var value = $(this).val();
                filter['client'] = value;
                console.log(filter);
                filter();
            });
        });
    </script>
@endsection
