@if (!isset($category) || (isset($category) && $cat->id != $category->id))
<option value="{{ $cat->id }}" {{ isset($category) && $cat->id == $category->parent_id ? 'selected' : '' }}>
    @if ($cat->parent()->exists())
        @for ($i = 1; $i < $cat->level; $i++)
            -
        @endfor
    @endif {{ $cat->name }}
</option>
@endif
@foreach ($cat->children as $key => $sub)
    @include('admin.partials.subcategories', ['cat' => $sub])
@endforeach
