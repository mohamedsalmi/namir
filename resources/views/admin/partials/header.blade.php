 <!--start top header-->
 <header class="top-header">
    <nav class="navbar navbar-expand-lg navbar-light bg-white rounded-0 border-bottom  ">
        <div class="mobile-toggle-icon fs-3">
            <i class="bi bi-list"></i>
        </div>
        {{-- <nav class="navbar navbar-expand-lg navbar-light bg-white rounded-0 border-bottom"> --}}
           {{-- <a class="navbar-brand" href="#"><img src="assets/images/brand-logo-2.png" width="140" alt=""/></a> --}}

    
            @role('Admin')
                <div>
                    {{ Form::select('store_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Store(), 'name'), auth()->user()->store_id, ['id' => 'store_id', 'name' => 'store_id', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select store']) }}

                </div>
            @endrole
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse bg-white " id="navbarSupportedContent">
                <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">

                   @canany(['Liste encaissement'])
                   <li class="nav-item">
                       <a class="nav-link" href="{{ route('admin.financialcommitment.index', ['from' => date("Y-m-d"), 'to' => date("Y-m-d")]) }}">
                           Encaissement
                       </a>
                   </li>
               @endcanany
                    @canany(['Liste décaissement'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/providercommitment/list') }}">
                                Décaissement
                            </a>
                        </li>
                    @endcanany
                    @canany(['Liste article', 'Ajouter article'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/product/list') }}">Articles</a>
                        </li>
                    @endcanany
                    @canany(['Détails caisse'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.fund.fund', ['from' => date("Y-m-d"), 'to' => date("Y-m-d")]) }}">
                                Caisse
                            </a>
                        </li>
                    @endcanany
                    @canany(['Détails caisse'])
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.history.history') }}">Recettes 
                        </a>
                    </li>
                    @endcanany
                    @canany(['Détails caisse'])
                    <li class="nav-item">
                         <a class="nav-link" href="{{ route('admin.history.online') }}">Recettes (En ligne)
                         </a>
                    </li>
                    @endcanany
                </ul>
               

                <div class="top-navbar-right ms-auto">
                    <ul class="navbar-nav align-items-center">

                        <li class="nav-item dropdown dropdown-large" id="notifications-container">
                            @php
                                $order_notifications = auth()->user()->unreadNotifications->where('type', 'App\Notifications\NewWebsiteOrderNotification');
                            @endphp
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#"
                                data-bs-toggle="dropdown">
                                <div class="notifications">
                                    @if ($order_notifications->count() > 0)
                                        <span
                                            class="notify-badge">{{ $order_notifications->count() }}</span>
                                    @endif
                                    <i class="bi bi-cart-plus-fill"></i>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end p-0">
                                <div class="d-sm-flex align-items-center p-2 border-bottom m-2">
                                    <h5 class="mb-2 mb-sm-0">Commandes en ligne</h5>
                                    @if ($order_notifications->count() > 0)
                                        <div class="ms-auto">
                                            <a href="{{route('admin.notification.order.markallasread', auth()->user()->id)}}" class="btn text-success mark-all-as-read"><i class="bi bi-check-all"
                                                 title="Tous marquer comme lus"></i></a>
                                        </div>
                                    @endif
                                </div>
                                <div class="header-notifications-list p-2" style="overflow: auto;">
                                    @if ($order_notifications->count() == 0)
                                        <a class="dropdown-item">
                                            <div class="text-center">Aucune donnée disponible</div>
                                        </a>
                                    @endif
                                    @foreach ($order_notifications as $notification)
                                        <a class="dropdown-item" href="{{ route('admin.onlinesale.index') }}">
                                            <div class="d-flex align-items-center">
                                                <div class="notification-box bg-light-warning text-warning"><i
                                                        class="lni lni-circle-minus"></i></div>
                                                <div class="ms-3 flex-grow-1">
                                                    <h6 class="mb-0 dropdown-msg-user">
                                                        {{ \App\Helpers\Helper::formatNotificationType($notification->type) }}
                                                        <span class="msg-time float-end text-secondary">
                                                            {{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</span>
                                                    </h6>
                                                    <small title="{{ \App\Helpers\Helper::getNotificationMessage($notification) }}"
                                                        lass="mb-0 dropdown-msg-text text-secondary d-flex align-items-center">
                                                        {{ Str::limit(\App\Helpers\Helper::getNotificationMessage($notification), 15) }}</small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                                <div class="p-2">
                                    <div>
                                        <hr class="dropdown-divider">
                                    </div>
                                    <a class="dropdown-item" href="{{ route('admin.onlinesale.index') }}">
                                        <div class="text-center">Afficher toutes les commandes</div>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown dropdown-large" id="notifications-container">
                            @php
                                $notifications = auth()->user()->unreadNotifications->where('type', '!=', 'App\Notifications\NewWebsiteOrderNotification');
                            @endphp
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#"
                                data-bs-toggle="dropdown">
                                <div class="notifications">
                                    @if ($notifications->count() > 0)
                                        <span
                                            class="notify-badge">{{ $notifications->count() }}</span>
                                    @endif
                                    <i class="bi bi-bell-fill"></i>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end p-0">
                                <div class="d-sm-flex align-items-center p-2 border-bottom m-2">
                                    <h5 class="mb-2 mb-sm-0">Notifications</h5>
                                    @if ($notifications->count() > 0)
                                        <div class="ms-auto">
                                            <a  href="{{route('admin.notification.order.markallasread', auth()->user()->id)}}" class="btn text-success mark-all-as-read"><i class="bi bi-check-all"
                                                    aria-hidden="true" title="Tous marquer comme lus"></i></a>
                                        </div>
                                    @endif
                                </div>
                                <div class="header-notifications-list p-2" style="overflow: auto;">
                                    @if ($notifications->count() == 0)
                                        <a class="dropdown-item">
                                            <div class="text-center">Aucune notification disponible</div>
                                        </a>
                                    @endif
                                    @foreach ($notifications as $notification)
                                        <a class="dropdown-item" href="#">
                                            <div class="d-flex align-items-center">
                                                <div class="notification-box bg-light-warning text-warning"><i
                                                        class="lni lni-circle-minus"></i></div>
                                                <div class="ms-3 flex-grow-1">
                                                    <h6 class="mb-0 dropdown-msg-user">
                                                        {{ \App\Helpers\Helper::formatNotificationType($notification->type) }}
                                                        <span class="msg-time float-end text-secondary">
                                                            {{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</span>
                                                    </h6>
                                                    <small title="{{ \App\Helpers\Helper::getNotificationMessage($notification) }}"
                                                        lass="mb-0 dropdown-msg-text text-secondary d-flex align-items-center">
                                                        {{ Str::limit(\App\Helpers\Helper::getNotificationMessage($notification), 15) }}</small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                                <div class="p-2">
                                    <div>
                                        <hr class="dropdown-divider">
                                    </div>
                                    <a class="dropdown-item" href="{{ route('admin.notification.index') }}">
                                        <div class="text-center">Afficher toutes les notifications</div>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown dropdown-user-setting">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#"
                                data-bs-toggle="dropdown">
                                <div class="user-setting d-flex align-items-center">
                                    <img src="{{ auth()->user()->image_url }}" class="user-img" alt="">
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <div class="d-flex align-items-center">
                                            <img src="{{ auth()->user()->image_url }}" alt=""
                                                class="rounded-circle" width="54" height="54">
                                            <div class="ms-3">
                                                <h6 class="mb-0 dropdown-user-name">{{ auth()->user()->name }}</h6>
                                                <small
                                                    class="mb-0 dropdown-user-designation text-secondary">{{ auth()->user()->role }}</small>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('admin.profile') }}">
                                        <div class="d-flex align-items-center">
                                            <div class=""><i class="bi bi-person-fill"></i></div>
                                            <div class="ms-3"><span>Mon compte</span></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('admin.setting.index') }}">
                                        <div class="d-flex align-items-center">
                                            <div class=""><i class="bi bi-gear-fill"></i></div>
                                            <div class="ms-3"><span>Paramètres</span></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ url('/admin/logout') }}">
                                        <div class="d-flex align-items-center">
                                            <div class=""><i class="bi bi-lock-fill"></i></div>
                                            <div class="ms-3"><span>Se déconnecter</span></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
     
    </nav>
</header>
<!--end top header-->

<script src="{{ asset('assets/admin/js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function(e) {

        $(document).on('click', 'a.mark-all-as-read', function(e) {
            e.preventDefault();
            var id = {{ auth()->user()->id }};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/admin/notification/user/" + id + "/markallasread",
                type: "GET",
                // data : data,
                async: false,
                success: function(response, textStatus, jqXHR) {
                    console.log(response)
                    $("#notifications-container").load(location.href +
                        " #notifications-container");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        });
        $(document).on('change', '.store', function(e) {
            console.log('change store');
            var url = "/admin/store/" + $(this).val() + "/storechange";

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                success: function() {
                    window.location.reload();
                }
            });
        });
    });
</script>
