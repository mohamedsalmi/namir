 <!--start sidebar -->
 <aside class="sidebar-wrapper" data-simplebar="true">
     <div class="sidebar-header">
         <div>
             <img src="{{ asset(config('stock.info.image')) }}" class="logo-icon" alt="logo icon">
         </div>
         <div>
             <h4 class="logo-text">{{config('stock.info.name')}} </h4>
         </div>
         <div class="toggle-icon ms-auto"> <i class="bi bi-list"></i>
         </div>
     </div>
     <!--navigation-->
     <ul class="metismenu" id="menu">
         <li>
             <a href="{{ url('/admin') }}">
                 <div class="parent-icon"><i class="lni lni-home"></i>
                 </div>
                 <div class="menu-title">Tableau de bord</div>
             </a>
         </li>
         @canany(['Liste bon de livraison', 'Ajouter bon de livraison', 'Liste devis', 'Ajouter devis','Liste commande', 'Ajouter commande', 'Liste facture', 'Liste facture avoir', 'Liste encaissement'])
         <li>
             <a href="javascript:;" class="has-arrow">
                 <div class="parent-icon"><i class="bi bi-app"></i>
                 </div>
                 <div class="menu-title">Website</div>
             </a>
             <ul>
                 @canany(['Liste bon de livraison', 'Ajouter bon de livraison'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-app"></i>
                             </div>
                             <div class="menu-title">Sliders</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de livraison'])
                                 <li> <a href="{{ url('/admin/slider/list') }}"><i class="bi bi-circle"></i>Liste des Sliders</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de livraison'])
                                 <li> <a href="{{ url('/admin/slider/create') }}"><i class="bi bi-circle"></i>Créer Slider</a>
                                 </li>
                             @endcanany
                     
                         </ul>
                     </li>
                     @endcanany
                 @canany(['Liste bon de livraison', 'Ajouter bon de livraison'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-app"></i>
                             </div>
                             <div class="menu-title">testimonials</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de livraison'])
                                 <li> <a href="{{ url('/admin/testimonial/list') }}"><i class="bi bi-circle"></i>Liste des testimonials</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de livraison'])
                                 <li> <a href="{{ url('/admin/testimonial/create') }}"><i class="bi bi-circle"></i>Créer testimonial</a>
                                 </li>
                             @endcanany
                     
                         </ul>
                     </li>
                     @endcanany
                 @canany(['Liste bon de livraison', 'Ajouter bon de livraison'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-app"></i>
                             </div>
                             <div class="menu-title">Les partenaires</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de livraison'])
                                 <li> <a href="{{ url('/admin/partner/list') }}"><i class="bi bi-circle"></i>Liste des partenaires</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de livraison'])
                                 <li> <a href="{{ url('/admin/partner/create') }}"><i class="bi bi-circle"></i>Créer partenaire</a>
                                 </li>
                             @endcanany
                     
                         </ul>
                     </li>
                     @endcanany
                     @canany(['Liste coupon', 'Ajouter coupon'])
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bi bi-ticket-perforated"></i>
                    </div>
                    <div class="menu-title">Coupons</div>
                </a>
                <ul>
                    @canany(['Liste coupon'])
                        <li> <a href="{{ url('/admin/coupon/list') }}"><i class="bi bi-circle"></i>Liste des coupons</a>
                        </li>
                    @endcanany
                    @canany(['Ajouter coupon'])
                        <li> <a href="{{ url('/admin/coupon/create') }}"><i class="bi bi-circle"></i>Nouveau coupon</a>
                        </li>
                    @endcanany
                </ul>
            </li>
        @endcanany
                  
             </ul>
         </li>
         @endcanany
         @canany(['Liste point de vente', 'Ajouter point de vente'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bi bi-shop"></i>
                     </div>
                     <div class="menu-title">Points de vente</div>
                 </a>
                 <ul>
                     @canany(['Liste point de vente'])
                         <li> <a href="{{ url('/admin/store/list') }}"><i class="bi bi-circle"></i>Liste des points de vente</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter point de vente'])
                         <li> <a href="{{ url('/admin/store/create') }}"><i class="bi bi-circle"></i>Ajouter un point de vente</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         @canany(['Liste devise', 'Ajouter devise'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bi bi-currency-dollar"></i>
                     </div>
                     <div class="menu-title">Devises</div>
                 </a>
                 <ul>
                     @canany(['Liste devise'])
                         <li> <a href="{{ url('/admin/currency/list') }}"><i class="bi bi-circle"></i>Liste des devises</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter devise'])
                         <li> <a href="{{ url('/admin/currency/create') }}"><i class="bi bi-circle"></i>Ajouter une devise</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         @canany(['Liste catégorie', 'Ajouter catégorie'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bx bx-share-alt"></i>
                     </div>
                     <div class="menu-title">Catégories</div>
                 </a>
                 <ul>
                     @canany(['Liste catégorie'])
                         <li> <a href="{{ url('/admin/category/list') }}"><i class="bi bi-circle"></i>Liste des catégories</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter catégorie'])
                         <li> <a href="{{ url('/admin/category/create') }}"><i class="bi bi-circle"></i>Ajouter une catégorie</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         @canany(['Liste attribut', 'Ajouter attribut'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bx bx-purchase-tag"></i>
                     </div>
                     <div class="menu-title">Attributs</div>
                 </a>
                 <ul>
                     @canany(['Liste attribut'])
                         <li> <a href="{{ url('/admin/attribute/list') }}"><i class="bi bi-circle"></i>Liste des attributs</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter attribut'])
                         <li> <a href="{{ url('/admin/attribute/create') }}"><i class="bi bi-circle"></i>Ajouter un attribut</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany
{{-- 
         @canany(['Liste article', 'Ajouter article'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bx bx-book-alt"></i>
                     </div>
                     <div class="menu-title">Articles</div>
                 </a>
                 <ul>
                     @canany(['Liste article'])
                         <li> <a href="{{ url('/admin/product/list') }}"><i class="bi bi-circle"></i>Liste des articles</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter article'])
                         <li> <a href="{{ url('/admin/product/create') }}"><i class="bi bi-circle"></i>Nouvel article</a>
                         </li>
                     @endcanany
                     <li> <a href="{{ url('/admin/product/importation') }}"><i class="bi bi-circle"></i>Importation des articles</a></li>
                 </ul>
             </li>
         @endcanany --}}

         @canany(['Liste client', 'Ajouter client'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="lni lni-user"></i>
                     </div>
                     <div class="menu-title">Clients</div>
                 </a>
                 <ul>
                     @canany(['Liste client'])
                         <li> <a href="{{ url('/admin/client/list') }}"><i class="bi bi-circle"></i>Liste des clients</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter client'])
                         <li> <a href="{{ url('/admin/client/create') }}"><i class="bi bi-circle"></i>Nouveau client</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         @canany(['Liste fournisseur', 'Ajouter fournisseur'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bi bi-truck"></i>
                     </div>
                     <div class="menu-title">Fournisseurs</div>
                 </a>
                 <ul>
                     @canany(['Liste fournisseur'])
                         <li> <a href="{{ url('/admin/provider/list') }}"><i class="bi bi-circle"></i>Liste des fournisseurs</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter fournisseur'])
                         <li> <a href="{{ url('/admin/provider/create') }}"><i class="bi bi-circle"></i>Nouveau fournisseur</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         @canany(['Liste bon de livraison', 'Ajouter bon de livraison', 'Liste devis', 'Ajouter devis','Liste commande', 'Ajouter commande', 'Liste facture', 'Liste facture avoir', 'Liste encaissement'])
         <li>
             <a href="javascript:;" class="has-arrow">
                 <div class="parent-icon"><i class="bx bx-money"></i>
                 </div>
                 <div class="menu-title">Ventes</div>
             </a>
             <ul>
                 @canany(['Liste bon de livraison', 'Ajouter bon de livraison'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">BL</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de livraison'])
                                 <li> <a href="{{ url('/admin/cart/list') }}"><i class="bi bi-circle"></i>Liste des BL</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de livraison'])
                                 <li> <a href="{{ url('/admin/cart/create') }}"><i class="bi bi-circle"></i>Nouveau BL</a>
                                 </li>
                             @endcanany
                             @canany(['Liste bon de livraison'])
                                 <li> <a href="{{ url('/admin/returnnote/list') }}"><i class="bi bi-circle"></i>Liste bons de retour
                                         BL</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                     @endcanany
                     @canany(['Liste facture', 'Liste facture avoir'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-file-text"></i>
                             </div>
                             <div class="menu-title">Factures</div>
                         </a>
                         <ul>
                             @canany(['Liste facture'])
                                 <li> <a href="{{ url('/admin/invoice/list') }}"><i class="bi bi-circle"></i>Liste des factures</a>
                                 </li>
                             @endcanany
                             @canany(['Liste facture avoir'])
                                 <li> <a href="{{ url('/admin/creditnote/list') }}"><i class="bi bi-circle"></i>Liste des facture
                                         d'avoirs</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                     @endcanany
                 
                 @canany(['Liste devis', 'Ajouter devis'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-file-text"></i>
                             </div>
                             <div class="menu-title">Devis</div>
                         </a>
                         <ul>
                             @canany(['Liste devis'])
                                 <li> <a href="{{ url('/admin/salequote/list') }}"><i class="bi bi-circle"></i>Liste des devis</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter devis'])
                                 <li> <a href="{{ url('/admin/salequote/create') }}"><i class="bi bi-circle"></i>Nouveau devis</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                 @endcanany
                 @canany(['Liste devis', 'Ajouter devis'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-file-text"></i>
                             </div>
                             <div class="menu-title">Vente En Ligne</div>
                         </a>
                         <ul>
                             @canany(['Liste devis'])
                                 <li> <a href="{{ url('/admin/onlinesale/list') }}"><i class="bi bi-circle"></i>Liste des Ventes En Ligne</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter devis'])
                                 <li> <a href="{{ url('/admin/onlinesale/create') }}"><i class="bi bi-circle"></i>Nouveau Vente En Ligne</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                 @endcanany

                 @canany(['Liste commande', 'Ajouter commande'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">Commandes</div>
                         </a>
                         <ul>
                             @canany(['Liste commande'])
                                 <li> <a href="{{ url('/admin/order/list') }}"><i class="bi bi-circle"></i>Liste des commandes</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter commande'])
                                 <li> <a href="{{ url('/admin/order/create') }}"><i class="bi bi-circle"></i>Nouvelle commande</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                 @endcanany
                 @canany(['Liste encaissement'])
                     <li>
                         <a href="{{ url('/admin/financialcommitment/list') }}">
                             <div class="parent-icon"><i class="bi bi-currency-dollar"></i>
                             </div>
                             <div class="menu-title">Encaissement</div>
                         </a>
                     </li>
                @endcanany
             </ul>
         </li>
         @endcanany
    

         @canany(['Liste bon d\'achat', 'Ajouter bon d\'achat', 'Liste bon de retour', 'Liste décaissement'])
         <li>
             <a href="javascript:;" class="has-arrow">
                 <div class="parent-icon"><i class="bi bi-currency-exchange"></i>
                 </div>
                 <div class="menu-title">Achats</div>
             </a>
             <ul>
                 @canany(['Liste bon d\'achat', 'Ajouter bon d\'achat'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">Bons d'achat</div>
                         </a>
                         <ul>
                             @canany(['Liste bon d\'achat'])
                                 <li> <a href="{{ url('/admin/voucher/list') }}"><i class="bi bi-circle"></i>Liste des bons
                                         d'achat</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon d\'achat'])
                                 <li> <a href="{{ url('/admin/voucher/create') }}"><i class="bi bi-circle"></i>Nouveau bon
                                         d'achat</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                 @endcanany

                 @canany(['Liste bon de retour'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bi bi-arrow-return-left"></i>
                             </div>
                             <div class="menu-title">Bons de retour</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de retour'])
                                 <li> <a href="{{ url('/admin/returncoupon/list') }}"><i class="bi bi-circle"></i>Liste des bons de
                                         retour</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                 @endcanany
                 @canany(['Liste décaissement'])
                     <li>
                         <a href="{{ url('/admin/providercommitment/list') }}">
                             <div class="parent-icon"><i class="bi bi-currency-dollar"></i>
                             </div>
                             <div class="menu-title">Décaissement</div>
                         </a>
                     </li>
                 @endcanany
             </ul>
         </li>
         @endcanany
         {{-- @canany(['Détails caisse'])
             <li>
                 <a href="{{ url('/admin/fund') }}">
                     <div class="parent-icon"><i class="lni lni-money-protection"></i>
                     </div>
                     <div class="menu-title">Caisse</div>
                 </a>
             </li>
         @endcanany
         @canany(['Détails caisse'])
             <li>
                 <a href="{{ route('admin.history.history') }}">
                     <div class="parent-icon"><i class="bx bx-wallet"></i>
                     </div>
                     <div class="menu-title">Recettes anuelles et mensuelles</div>
                 </a>
             </li>
         @endcanany --}}
         @canany(['Liste inventaire', 'Ajouter inventaire'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                     </div>
                     <div class="menu-title">Inventaire</div>
                 </a>
                 <ul>
                     @canany(['Liste inventaire'])
                         <li> <a href="{{ url('/admin/inventory/list') }}"><i class="bi bi-circle"></i>Liste des inventaires</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter inventaire'])
                         <li> <a href="{{ url('/admin/inventory/create') }}"><i class="bi bi-circle"></i>Nouveau inventaire</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany
         @canany(['Liste bon de sortie', 'Ajouter bon de sortie', 'Liste bon de commande', 'Ajouter bon de commande', 'Liste bon de réception'])
         <li>
             <a href="javascript:;" class="has-arrow">
                 <div class="parent-icon"><i class="bx bx-money"></i>
                 </div>
                 <div class="menu-title">Mouvement de stock</div>
             </a>
             <ul>
                @canany(['Liste bon de sortie', 'Ajouter bon de sortie'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">BS</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de sortie'])
                                 <li> <a href="{{ url('/admin/mu/list') }}"><i class="bi bi-circle"></i>Liste des BS</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de sortie'])
                                 <li> <a href="{{ url('/admin/mu/create') }}"><i class="bi bi-circle"></i>Nouveau BS</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>

                 @endcanany
                 {{-- @canany(['Liste bon de commande', 'Ajouter bon de commande'])
                     <li>
                         <a href="javascript:;" class="has-arrow">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">Commandes</div>
                         </a>
                         <ul>
                             @canany(['Liste bon de commande'])
                                 <li> <a href="{{ url('/admin/mu/list/bc') }}"><i class="bi bi-circle"></i>Liste des commandes
                                         envoyées</a>
                                 </li>
                             @endcanany
                             @canany(['Liste bon de commande'])
                                 <li> <a href="{{ url('/admin/mu/list/bcr') }}"><i class="bi bi-circle"></i>Liste des commandes
                                         demandées</a>
                                 </li>
                             @endcanany
                             @canany(['Ajouter bon de commande'])
                                 <li> <a href="{{ url('/admin/mu/create/bc') }}"><i class="bi bi-circle"></i>Nouvelle commande</a>
                                 </li>
                             @endcanany
                         </ul>
                     </li>
                     @endcanany --}}
                     @canany(['Liste bon de réception'])
                     <li>
                         <a href="{{ url('/admin/mu/list/br') }}">
                             <div class="parent-icon"><i class="bx bx-cart-alt"></i>
                             </div>
                             <div class="menu-title">BR</div>
                         </a>
                     </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany
         @canany(['Liste utilisateur', 'Ajouter utilisateur'])
             <li>
                 <a href="javascript:;" class="has-arrow">
                     <div class="parent-icon"><i class="lni lni-user"></i>
                     </div>
                     <div class="menu-title">Utilisateurs</div>
                 </a>
                 <ul>
                     @canany(['Liste utilisateur'])
                         <li> <a href="{{ url('/admin/user/list') }}"><i class="bi bi-circle"></i>Liste des utilisateurs</a>
                         </li>
                     @endcanany
                     @canany(['Ajouter utilisateur'])
                         <li> <a href="{{ url('/admin/user/create') }}"><i class="bi bi-circle"></i>Nouvel utilisateur</a>
                         </li>
                     @endcanany
                 </ul>
             </li>
         @endcanany

         
         @canany(['Liste rôle', 'Ajouter rôle'])
         <li>
             <a href="javascript:;" class="has-arrow">
                 <div class="parent-icon"><i class="lni lni-user"></i>
                 </div>
                 <div class="menu-title">Rôles et permissions</div>
                </a>
                <ul>
                    @canany(['Liste rôle'])
                    <li> <a href="{{ url('/admin/role/list') }}"><i class="bi bi-circle"></i>Liste des rôles</a>
                    </li>
                    @endcanany
                    @canany(['Ajouter rôle'])
                    <li> <a href="{{ url('/admin/role/create') }}"><i class="bi bi-circle"></i>Nouveau rôle</a>
                    </li>
                    @endcanany
                </ul>
            </li>
            @endcanany

        @canany(['Liste sms', 'Ajouter sms'])
            <li>
                <a href="{{ url('/admin/sms/send') }}">
                    <div class="parent-icon"><i class="lni lni-envelope"></i>
                    </div>
                    <div class="menu-title">SMS</div>
                </a>
            </li>
        @endcanany
        

         @role('Admin')
         <li>
            <a href="{{route('admin.setting.index')}}">
                <div class="parent-icon"><i class="bi bi-gear-fill"></i>
                </div>
                <div class="menu-title">Paramètres</div>
            </a>
        </li>
         @endrole
     </ul>
     <!--end navigation-->
 </aside>
 <!--end sidebar -->
