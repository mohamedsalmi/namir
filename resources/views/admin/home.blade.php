@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
<main class="page-content">
<!--start row-->
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-4 row-cols-xxl-4">
        <div class="col">
          <div class="card radius-10 border-0 border-start border-tiffany border-3">
            <a href="{{route('admin.cart.index')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Bons de livraison</p>
                  <h4 class="mb-0 text-tiffany">{{ $statistics->orders_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-tiffany text-white">
                  <i class="bi bi-bag-check-fill"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-success border-3">
            <a href="{{route('admin.client.index')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Clients</p>
                  <h4 class="mb-0 text-success">{{ $statistics->clients_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-success text-white">
                  <i class="bi bi-people"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-pink border-3">
            <a href="{{route('admin.provider.index')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Fournisseurs</p>
                  <h4 class="mb-0 text-pink">{{ $statistics->providers_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-pink text-white">
                  <i class="bi bi-truck"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-orange border-3">
            <a href="{{route('admin.user.index')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Utilisateurs</p>
                  <h4 class="mb-0 text-orange">{{ $statistics->users_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-orange text-white">
                  <i class="bi bi-person-plus-fill"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-success border-3">
            <a href="{{route('admin.product.index')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Articles</p>
                  <h4 class="mb-0 text-success">{{ $statistics->products_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-success text-white">
                  <i class="bx bx-book-alt"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-orange border-3">
            <a href="{{route('admin.store.list')}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Points de vente</p>
                  <h4 class="mb-0 text-orange">{{ $statistics->stores_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-orange text-white">
                  <i class="bi bi-shop-window"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-danger border-3">
            <a href="{{route('admin.financialcommitment.index',['filter' => 'check'])}}">
            <div class="card-body">
              <div class="d-flex align-items-center">
                <div class="">
                  <p class="mb-1">Chèques en retard</p>
                  <h4 class="mb-0 text-danger">{{ $statistics->late_checks_count }}</h4>
                </div>
                <div class="ms-auto widget-icon bg-danger text-white">
                  <i class="bi bi-exclamation-triangle-fill"></i>
                </div>
              </div>
            </div>
            </a>
          </div>
         </div>
         <div class="col">
          <div class="card radius-10 border-0 border-start border-warning border-3">
            <a href="{{route('admin.financialcommitment.index',['filter'])}}">
              <div class="card-body">
                <div class="d-flex align-items-center">
                  <div class="">
                    <p class="mb-1">Echéances en retard</p>
                    <h4 class="mb-0 text-warning">{{ $statistics->commitments_date_reached_count }}</h4>
                  </div>
                  <div class="ms-auto widget-icon bg-warning text-white">
                    <i class="bi bi-exclamation-triangle-fill"></i>
                  </div>
                </div>
              </div>
            </a>
          </div>
         </div>
        </div>
<!--end row-->

    <div class="row">
        {{-- <div class="col-12 col-lg-6 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h6 class="mb-0">Revenue</h6>
                        <div class="fs-5 ms-auto dropdown">
                            <div class="dropdown-toggle dropdown-toggle-nocaret cursor-pointer" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></div>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="chart5"></div>
                </div>
            </div>
        </div> --}}
        <div class="col-12 col-lg-12 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h6 class="mb-0">Chiffre d'affaire</h6>
                        @if(count($years) > 0)
                        <div class="fs-5 ms-auto dropdown">
                            <select name="year" id="year" class="form-control form-control-sm">
                                @foreach ($years as $item)
                                    <option {{$item == date('Y') ? 'selected' : ''}} value="{{$item}}">{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                    <div id="total-orders-by-month"></div>
                </div>
            </div>
        </div>
        {{-- <div class="col-12 col-lg-6 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h6 class="mb-0">By Device</h6>
                        <div class="fs-5 ms-auto dropdown">
                            <div class="dropdown-toggle dropdown-toggle-nocaret cursor-pointer" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></div>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row row-cols-1 row-cols-md-2 g-3 mt-2 align-items-center">
                        <div class="col-lg-7 col-xl-7 col-xxl-8">
                            <div class="by-device-container">
                                <div class="piechart-legend">
                                    <h2 class="mb-1">85%</h2>
                                    <h6 class="mb-0">Total Visitors</h6>
                                </div>
                                <canvas id="chart6"></canvas>
                            </div>
                        </div>
                        <div class="col-lg-5 col-xl-5 col-xxl-4">
                            <div class="">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex align-items-center justify-content-between border-0">
                                        <i class="bi bi-tablet-landscape-fill me-2 text-primary"></i> <span>Tablet - </span> <span>22.5%</span>
                                    </li>
                                    <li class="list-group-item d-flex align-items-center justify-content-between border-0">
                                        <i class="bi bi-phone-fill me-2 text-primary-2"></i> <span>Mobile - </span> <span>62.3%</span>
                                    </li>
                                    <li class="list-group-item d-flex align-items-center justify-content-between border-0">
                                        <i class="bi bi-display-fill me-2 text-primary-3"></i> <span>Desktop - </span> <span>15.2%</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    <!--end row-->
</main>
@endsection

@section('scripts')
<script src="{{asset('assets/admin/plugins/chartjs/js/Chart.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/chartjs/js/Chart.extension.js')}}"></script>
<script src="{{asset('assets/admin/plugins/apexcharts-bundle/js/apexcharts.min.js')}}"></script>

<script>
    function toMonthName(monthNumber) {
        const date = new Date();
        date.setMonth(monthNumber - 1);

        return date.toLocaleString('fr-FR', {
            month: 'short',
        });
    }

    var options = {
    series: [{
        name: "Total",
		data: []
    }],
    chart: {
         type: "area",
       // width: 130,
	    stacked: true,
        height: 280,
        toolbar: {
            show: !1
        },
        zoom: {
            enabled: !1
        },
        dropShadow: {
            enabled: 0,
            top: 3,
            left: 14,
            blur: 4,
            opacity: .12,
            color: "#00adb4"
        },
        sparkline: {
            enabled: !1
        }
    },
    markers: {
        size: 0,
        colors: ["#00adb4"],
        strokeColors: "#fff",
        strokeWidth: 2,
        hover: {
            size: 7
        }
    },
	grid: {
		row: {
			colors: ["transparent", "transparent"],
			opacity: .2
		},
		borderColor: "#f1f1f1"
	},
    plotOptions: {
        bar: {
            horizontal: !1,
            columnWidth: "25%",
            //endingShape: "rounded"
        }
    },
    dataLabels: {
        enabled: !1
    },
    stroke: {
        show: !0,
        width: [2.5],
		//colors: ["#00adb4"],
        curve: "smooth"
    },
	fill: {
		type: 'gradient',
		gradient: {
		  shade: 'light',
		  type: 'vertical',
		  shadeIntensity: 0.5,
		  gradientToColors: ['#00adb4'],
		  inverseColors: false,
		  opacityFrom: 0.5,
		  opacityTo: 0.1,
		 // stops: [0, 100]
		}
	},
	colors: ["#00adb4"],
    xaxis: {
        // categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        categories: []
    },
	responsive: [
		{
		  breakpoint: 1000,
		  options: {
			chart: {
				type: "area",
			   // width: 130,
				stacked: true,
			}
		  }
		}
	  ],
	legend: {
		show: false
	  },
    tooltip: {
        theme: "dark"
    }
  };

    $(document).ready(function() {


  var data = @json($statistics->carts_total_amounts_by_year_and_month);
  data.forEach(element => {
    options.series[0].data.push(element.total);
    options.xaxis.categories.push(toMonthName(element.month));
  });

  var chart = new ApexCharts(document.querySelector("#total-orders-by-month"), options);
  chart.render();

  $(document).on('change', '#year', function() {
        var year = $(this).val();
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $.ajax({
            url : "/admin/statistic/cart/total/" + year,
            type: "GET",
            async : false, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
            success: function(response, textStatus, jqXHR) {
                let data = response.data.map(element => { return {x: toMonthName(element.month), y : element.total} });
                chart.updateSeries([{
                    name: 'Total',
                    data: data
                }]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
  }).change(); 
});
</script>

@endsection
