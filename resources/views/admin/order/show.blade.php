@extends('admin.layouts.app')

@section('title','Article')

@section('stylesheets')
@endsection
@section('content')

@endsection
@section('scripts')
     <!--start content-->
     <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Commande</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{route('admin.order.index')}}">Liste des commandes</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Détails de la commande</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

          <div class="card">
            <div class="card-header py-3">
              <div class="row g-3 align-items-center">
                <div class="col-12 col-lg-4 col-md-6 me-auto">
                  <h5 class="mb-1">{{$order->created_date}}</h5>
                  <p class="mb-0">N° commande : #{{$order->number}}</p>
                </div>
                {{-- <div class="col-12 col-lg-3 col-6 col-md-3">
                  <select class="form-select">
                    <option>Change Status</option>
                    <option>Awaiting Payment</option>
                    <option>Confirmed</option>
                    <option>Shipped</option>
                    <option>Delivered</option>
                  </select>
                </div>
                <div class="col-12 col-lg-3 col-6 col-md-3">
                   <button type="button" class="btn btn-primary">Enregistrer</button>
                   <button type="button" class="btn btn-secondary"><i class="bi bi-printer-fill"></i> Imprimer</button>
                </div> --}}
              </div>
             </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-2">
                   <div class="col">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-primary border-0">
                            <i class="bi bi-person text-primary"></i>
                          </div>
                          <div class="info">
                             <h6 class="mb-2">Client</h6>
                             <p class="mb-1">{{$order->client_details['name']}}</p>
                             <p class="mb-1">{{$order->client_details['adresse']}}</p>
                             <p class="mb-1">{{$order->client_details['phone']}}</p>
                          </div>
                       </div>
                       </div>
                     </div>
                   </div>
                  
                  <div class="col">
                    <div class="card border shadow-none radius-10">
                      <div class="card-body">
                        <div class="d-flex align-items-center gap-3">
                          <div class="icon-box bg-light-danger border-0">
                            <i class="bi bi-geo-alt text-danger"></i>
                          </div>
                          <div class="info">
                            <h6 class="mb-2">Livrer à</h6>
                            <p class="mb-1"><strong>Adresse</strong> : {{$order->client_details['adresse']}}</p>
                            @if (isset($order->client_details['delivery']) && $order->client_details['delivery'] == 'home_delivery_option')
                              <div><span class="badge bg-success">Livraison à domicile</span></div>
                             @endif
                             @if (isset($order->client_details['delivery']) && $order->client_details['delivery'] == 'store_delivery_option')
                              <div><span class="badge bg-success">Livraison en magasin</span></div>
                             @endif
                          </div>
                        </div>
                      </div>
                     </div>
                </div>
              </div><!--end row-->

              <div class="row">
                  <div class="col-12 col-lg-8">
                     <div class="card border shadow-none radius-10">
                       <div class="card-body">
                           <div class="table-responsive">
                             <table class="table align-middle mb-0">
                               <thead class="table-light">
                                 <tr>
                                   <th>Produit</th>
                                   <th>Prix unitaire</th>
                                   <th>Quantité</th>
                                   <th>Total</th>
                                 </tr>
                               </thead>
                               <tbody>
                                @foreach($orderitems as $orderitem)
                                 <tr>
                                   <td>
                                     <div class="orderlist">
                                      <a class="d-flex align-items-center gap-2" href="javascript:;">
                                        <div class="product-box">
                                            <img src="{{asset( $orderitem->product->default_image_url ?? config('stock.image.default.product'))}}" alt="">
                                        </div>
                                        <div>
                                            <P class="mb-0 product-title">{{$orderitem->product_name}}</P>
                                        </div>
                                       </a>
                                     </div>
                                   </td>
                                   <td>{{$orderitem->product_price_selling}}</td>
                                   <td>{{$orderitem->product_quantity}}</td>
                                   <td>{{number_format((float)$orderitem->product_quantity * $orderitem->product_price_selling, 3)}} </td>
                                 </tr>
                                 @endforeach
                               </tbody>
                             </table>
                           </div>
                       </div>
                     </div>
                  </div>
                  <div class="col-12 col-lg-4">
                    <div class="card border shadow-none bg-light radius-10">
                      <div class="card-body">
                          <div class="d-flex align-items-center mb-4">
                             <div>
                                <h5 class="mb-0">Récapitulatif de la commande</h5>
                             </div>
                             <div class="ms-auto">
                               <button type="button" class="btn alert-success radius-30 px-4">{{$order->status}}</button>
                            </div>
                          </div>
                            <div class="d-flex align-items-center mb-3">
                              <div>
                                <p class="mb-0">TTC</p>
                              </div>
                              <div class="ms-auto">
                                <h5 class="mb-0">{{$order->total}}  DM</h5>
                            </div>
                          </div>

                          <div class="d-flex align-items-center mb-3">
                            <div>
                              <p class="mb-0">Taxes</p>
                            </div>
                            <div class="ms-auto">
                              <h5 class="mb-0">{{(float)$orderitems->SUM('product_tva')}} </h5>
                          </div>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            {{-- <div>
                              <p class="mb-0"> Timbre</p>
                            </div> --}}
                            {{-- <div class="ms-auto">
                                @if ($order->timbre == 1)
                              <h5 class="mb-0">0.60 TDN</h5>
                              @else
                              <h5 class="mb-0">0.00 TDN</h5>
                              @endif
                          </div> --}}
                        </div>
                            {{-- <div class="d-flex align-items-center mb-3">
                            <div>
                                <p class="mb-0">HT</p>
                            </div>
                            <div class="ms-auto">
                                <h5 class="mb-0">$14.00</h5>
                            </div>
                        </div> --}}
                        <div class="d-flex align-items-center mb-3">
                          <div>
                            <p class="mb-0">Remise</p>
                          </div>
                          <div class="ms-auto">
                            <h5 class="mb-0 text-danger">{{(float)$orderitems->SUM('product_remise')}} </h5>
                        </div>
                      </div>
                      @if(isset($order->client_details['delivery_price']) && $order->client_details['delivery_price'] > 0)
                        <div class="d-flex align-items-center mb-3">
                          <div>
                            <p class="mb-0">Prix de livraison</p>
                          </div>
                          <div class="ms-auto">
                            <h5 class="mb-0 text-danger">{{number_format($order->client_details['delivery_price'], 3)}} </h5>
                        </div>
                      </div>
                      @endif
                      </div>
                    </div>

                    {{-- <div class="card border shadow-none bg-warning radius-10">
                      <div class="card-body">
                          <h5>Information de paiement </h5>
                           <div class="d-flex align-items-center gap-3">
                              <div class="fs-1">
                                <i class="bi bi-credit-card-2-back-fill"></i>
                              </div>
                              <div class="">
                                <p class="mb-0 fs-6">Master Card **** **** 8956 </p>
                              </div>
                           </div>
                          <p>Business name: Template Market LLP <br>
                             Phone: +91-9910XXXXXX
                          </p>
                      </div>
                    </div> --}}


                 </div>
              </div><!--end row-->

            </div>
          </div>

      </main>
   <!--end page main-->
@endsection


