@extends('admin.layouts.app')

@section('title', 'Liste des commandes')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/datatable/css/dataTables.bootstrap5.min.css') }} " rel="stylesheet">
@endsection
@section('content')
    <!--start content-->
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Commandes</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des commandes</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="card card-body">
            <div class="d-sm-flex align-items-center">
                <h5 class="mb-2 mb-sm-0">Liste des commandes</h5>
                <div class="col offset-md-1">
                    @canany(['Ajouter facture'])
                        {!! Form::open([
                            'enctype' => 'multipart/form-data',
                            'route' => 'admin.invoice.store',
                            'method' => 'POST',
                            'id' => 'create-invoice-form',
                        ]) !!}
                        <div class="row g-4">
                            <div class="col-12 col-lg-3"> <a class="form-check form-switch">
                                    <input class="form-check-input" name="timbre" type="checkbox" checked>
                                    <input  name="document" type="hidden" value="Order">
                                    <label class="form-label" for="exampleFormControlInput1"><i class="fa fa-tag"></i>
                                        Timbre </label>
                                </a>
                            </div>
                            <div class="col-12 col-lg-3">
                                <input name="date" type="date" required
                                    class="form-control form-control-sm @error('date') is-invalid @enderror" placeholder="Date"
                                    value="{{ old('date', date('Y-m-d')) }}">
                                @error('date')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 col-lg-2">

                                <button type="submit" class="btn  btn-primary"><i class="fa fa-plus" aria-hidden="true"
                                        title="Ajouter"></i>Facturer</button>

                            </div>
                        </div>
                        {!! Form::close() !!}
                    @endcanany
                </div>
                <div class="col-md-3 ">
                    <h4 class="mb-0 text-success"> Total: <span id="total"
                            data-column="3">{{ number_format($sum, 3) }}</span> </h4>
                </div>
                @canany(['Ajouter commande'])
                    <div class="ms-auto">
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.order.create') }}"><i class="fa fa-plus"
                                aria-hidden="true" title="Ajouter"></i>Ajouter une commande</a>
                    </div>
                @endcanany
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 d-flex">
                <div class="card w-100">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="input-group col-md-6 mb-1">
                                    <span class="input-group-text">Du</span>
                                    <input required type="date" class="form-control form-control-sm filter-field"
                                        name="from" placeholder="De" id="from">
                                    <span class="input-group-text">Au</span>
                                    <input required type="date" class="form-control form-control-sm filter-field"
                                        name="to" placeholder="Au" id="to">
                                    <span class="input-group-text">Client</span>

                                    <select class="form-control " name="" id="clientfilter">
                                        <option value="">Tous</option>
                                        @foreach (\App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Client(), 'name') as $key => $client)
                                            <option value="{{ $key }}">{{ $client }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <table class="dataTable table align-middle w-100">
                                <thead class="table-light">
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        <th>
                                            <div class="form-check"><input type="checkbox" class="multi-check form-check-input"
                                                    id="select-all"></div>
                                        </th>
                                        <th>Codification</th>
                                        <th>Client</th>
                                        <th>Prix</th>
                                        {{-- <th>Gouvernerat </th> --}}
                                        {{-- <th>Delegation </th> --}}
                                        <th>Etat de paiement</th>
                                        <th>Etat de livraison</th>
                                        {{-- <th>Date</th>                                         --}}
                                        <th>Date de creation</th>
                                        <th>Creé par</th>
                                        <th>Modifié par</th>
                                        <th class="not-export-col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <input type="hidden" id="date-column" value="4">
        <input type="hidden" id="client-column" value="1">

    </main>
    <!--end page main-->
    <!--end page main-->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/sale-datatable.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 2500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 2500
                })
            })
        </script>
    @endif
    <script>
        $(document).ready(function() {
            $('#select-all').click(function() {
                var checked = this.checked;
                $('.multi-check').each(function() {
                    this.checked = checked;
                });
            })
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#show_confirm', function(event) {
            event.preventDefault();
            var form = $(this).closest("form");
            var name = $(this).data("name");
            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            var base_url = '{{ url(' / ') }}';
            var queryString = '';

            window.pdfMake.fonts = {
                AEfont: {
                    normal: 'AEfont-Regular.ttf',
                    bold: 'AEfont-Regular.ttf',
                    italics: 'AEfont-Regular.ttf',
                    bolditalics: 'AEfont-Regular.ttf'
                }
            };
            var table = $('.dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                drawCallback: function(settings) {
                    $('#total').text(settings.json.total);
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "Tous"]
                ],
                buttons: [

                    {
                        extend: 'excelHtml5',
                        text: "Excel",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'csvHtml5',
                        text: "CSV",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                    {
                        extend: 'pdfHtml5',
                        text: "PDF",
                        customize: function(doc) {
                            doc.defaultStyle = {
                                font: 'AEfont',
                                alignment: 'center',
                                fontSize: 16,
                            }
                        },
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },


                    },

                    {
                        extend: 'print',
                        text: "Imprimer",
                        exportOptions: {
                            columns: ':not(:last-child)',
                        },
                    },

                ],
                processing: true,
                serverSide: true,
                responsive: true,
                "language": {
                    "sProcessing": "Traitement en cours",
                    "sZeroRecords": "Il n'y a pas de données",
                    "sInfo": "Affichage de page _PAGE_ à _PAGES_  sur _TOTAL_ entrées",
                    "sInfoEmpty": "Aucune donnée",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Rechercher",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Précédent",
                        "sNext": "Suivant",
                        "sLast": "Dernier",
                    }
                },
                ajax: "{{ route('admin.order.index') }}",
                createdRow: function(row, data, dataIndex) {
                    // Set the data-status attribute, and add a class
                    $(row).find('td:eq(2)')
                        .attr('style', "text-align: right;")
                },
                aoColumns: [

                    {
                        data: 'check',
                        name: 'check',
                        orderable: false,
                    },
                    {
                        data: 'codification',
                        name: 'codification',
                    },
                    {
                        data: 'client',
                        name: 'client',
                    },
                    {
                        data: 'total',
                        name: 'total',
                    },
                    // {
                    //     data: 'state',
                    //     name: 'state',
                    // },
                    // {
                    //     data: 'city',
                    //     name: 'city',
                    // },
                    {
                        data: 'paiement_status',
                        name: 'paiement_status',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    // {
                    //     data: 'date',
                    //     name: 'date',
                    // },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'created_by',
                        name: 'created_by',
                    },
                    {
                        data: 'updated_by',
                        name: 'updated_by',
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        paging: false,
                        searchable: false,
                        bSearchable: false,
                        exportable: false
                    },
                ],
                initComplete: function() {
                    this.api().columns('.is_filter').every(function() {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).addClass("form-control form-control-sm")
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }

            });

            function filter() {
                var queryString = '?' + new URLSearchParams(filter).toString();
                var url = @json(route('admin.order.index'));
                table.ajax.url(url + queryString).load();
                table.draw();
                // e.preventDefault();
            }

            $(document).on('change', '.filter-field', function(e) {
                var key = $(this).attr('id');
                var value = $(this).val();
                console.log(value);
                filter[key] = value;
                console.log(filter);
                filter();
            });

            $(document).on('change', '#clientfilter', function(e) {
                var value = $(this).val();
                filter['client'] = value;
                console.log(filter);
                filter();
            });
            $(document).on('change', ".changestatus", function() {
                var order = $(this).attr('data-id');
                console.log(order);
                var status = $(this).children("option:selected").val();
                console.log(status);
                var url = "/admin/order/" + order + "/" + status + "/statuschange";

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    data: {
                        order: order,
                        status: status,
                    },
                    success: function(data) {
                        table.draw('page');
                    }
                });

            });
        });
    </script>
@endsection
