@extends('admin.layouts.app')

@section('title', 'Ajouter un testimonial')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">testimonials</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{ route('admin.provider.index') }}">Liste des testimonials</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ajouter nouveau testimonial</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.testimonial.store',
            'method' => 'POST',
            'id' => 'create-testimonial-form',
        ]) !!}

        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Ajouter nouveau testimonial</h5>
                            <div class="ms-auto">
                                <button type="submit" id="save" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-lg-8">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Nom du testimonial</label>
                                                <input name="name" id="name" type="text"
                                                    class="form-control form-control-sm @error('name') is-invalid @enderror"
                                                    placeholder="Nom" value="{{ old('name', '') }}">
                                                @error('name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4 required">
                                                <label class="form-label">Rating</label>
                                                <select class="form-select" name="rating" id="rating">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                @error('rating')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Testimonial</label>
                                                <textarea name="testimonial" id="testimonial" rows="8"
                                                    class="form-control form-control-sm @error('testimonial') is-invalid @enderror" placeholder="testimonial">{{ old('testimonial', '') }}</textarea>
                                                @error('testimonial')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-4">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <label class="form-label">Nom de l'auteur</label>
                                                <input name="writer_details[name]" id="writer_details[name]" type="text"
                                                    class="form-control form-control-sm @error('writer_details[name]') is-invalid @enderror"
                                                    placeholder="Nom" value="{{ old('writer_details[name]', '') }}">
                                                @error('writer_details[name]')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Poste d'auteur</label>

                                                <input name="writer_details[title]" id="writer_details[title]"
                                                    type="text"
                                                    class="form-control form-control-sm @error('writer_details[title]') is-invalid @enderror"
                                                    placeholder="Nom" value="{{ old('writer_details[title]', '') }}">
                                                @error('writer_details[title]')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">La compagnie d'auteur</label>
                                                <input name="writer_details[company]" id="writer_details[company]"
                                                    type="text"
                                                    class="form-control form-control-sm @error('writer_details[company]') is-invalid @enderror"
                                                    placeholder="Nom" value="{{ old('writer_details[company]', '') }}">
                                                @error('writer_details[company]')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Image</label>
                                                <input name="image" class="form-control form-control-sm" type="file"
                                                    single>
                                            </div>
                                        </div>
                                        <!--end row-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
        {!! Form::close() !!}
    </main>
@endsection
@section('scripts')
    <script>
        $(document).on("click", "#save", function(e) {
            submit(e);
        });

        var errors = null;

        function submit(e, options = '') {
            e.preventDefault();
            var Form = $('#create-testimonial-form');
            var url = "/admin/testimonial/testimonialvalidation";
            if (options != '') {
                var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
                Form.append(input);
            }
            if (errors) {
                $.each(errors, function(key, value) {
                    key = key.replace('writer_details.', '');
                    var element = document.getElementById(key);
                    if (element) {
                        element.classList.remove('is-invalid')
                    };
                });
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data: Form.serialize(),
                async: false,
                success: function(response, textStatus, jqXHR) {
                    Form.submit();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    errors = jqXHR.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        key = key.replace('writer_details.', '');
                        var element = document.getElementById(key);
                        if (element) {
                            element.classList.add('is-invalid')
                        };
                    });
                }
            });
        }
    </script>
@endsection
