@extends('admin.layouts.app')

@section('title', 'Modifier un testimonial')

@section('stylesheets')
<link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
@endsection

@section('content')
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
      <div class="breadcrumb-title pe-3">testimonials</div>
      <div class="ps-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.testimonial.index')}}">Liste des testimonials</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Modifier testimonial</li>
          </ol>
        </nav>
      </div>
    </div>
    <!--end breadcrumb-->
    {!! Form::open(array('enctype'=>'multipart/form-data','route' => ['admin.testimonial.update', $testimonial->id], 'method'=>'PUT', 'id'=>'update-testimonial-form'
    )) !!}

    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="card">
          <div class="card-header py-3 bg-transparent">
            <div class="d-sm-flex align-items-center">
              <h5 class="mb-2 mb-sm-0">Modifier testimonial</h5>
              <div class="ms-auto">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
          </div>
          <  <div class="card-body">
            <div class="row g-3">
                <div class="col-12 col-lg-8">
                    <div class="card shadow-none bg-light border">
                        <div class="card-body">
                            <div class="row g-3">
                                <div class="col-12 col-lg-4 required">
                                    <label class="form-label">Nom du testimonial</label>
                                    <input name="name" type="text"
                                        class="form-control form-control-sm @error('name') is-invalid @enderror"
                                        placeholder="Nom" value="{{ old('name', $testimonial->name) }}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-12 col-lg-4 required">
                                    <label class="form-label">Rating</label>
                                    <select class="form-select" name="rating">
                                      <option value="1" {{$testimonial->rating == '1' ? 'selected' :''}}>1</option>
                                      <option value="2" {{$testimonial->rating == '2' ? 'selected' :''}}>2</option>
                                      <option value="3" {{$testimonial->rating == '3' ? 'selected' :''}}>3</option>
                                      <option value="4" {{$testimonial->rating == '4' ? 'selected' :''}}>4</option>
                                      <option value="5" {{$testimonial->rating == '5' ? 'selected' :''}}>5</option>
                                  </select>
                                    @error('rating')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Testimonial</label>
                                    <textarea name="testimonial" rows="8" class="form-control form-control-sm @error('testimonial') is-invalid @enderror"
                                        placeholder="testimonial">{{ old('testimonial', $testimonial->testimonial) }}</textarea>
                                    @error('testimonial')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="card shadow-none bg-light border">
                        <div class="card-body">
                            <div class="row g-3">
                                <div class="col-12">
                                    <label class="form-label">Nom de l'auteur</label>
                                    <input name="writer_details[name]" type="text"
                                        class="form-control form-control-sm @error('writer_details[name]') is-invalid @enderror"
                                        placeholder="Nom" value="{{ old('writer_details[name]', $testimonial->writer_details['name']) }}">
                                    @error('writer_details[name]')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Poste d'auteur</label>

                                    <input name="writer_details[title]" type="text"
                                        class="form-control form-control-sm @error('writer_details[title]') is-invalid @enderror"
                                        placeholder="Nom" value="{{ old('writer_details[title]', $testimonial->writer_details['title']) }}">
                                    @error('writer_details[title]')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label">La compagnie d'auteur</label>
                                    <input name="writer_details[company]" type="text"
                                        class="form-control form-control-sm @error('writer_details[company]') is-invalid @enderror"
                                        placeholder="Nom" value="{{ old('writer_details[company]', $testimonial->writer_details['company']) }}">
                                    @error('writer_details[company]')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Image</label>
                                    <input name="image" class="form-control form-control-sm" type="file"
                                        single>
                                </div>
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>

            </div>
            <!--end row-->
        </div>
    </div>
</div>
</div>
<!--end row-
    {!! Form::close() !!}
</main>
@endsection
