@extends('admin.layouts.app')

@section('title', 'Mon compte')

@section('content')
<!--start content-->
<main class="page-content">
   
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Mon compte</div>
        <div class="ps-3">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i> Tableau de bord</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Mon compte</li>
            </ol>
          </nav>
        </div>
      </div>
      <!--end breadcrumb-->

    <div class="row">
      <div class="col-12 col-lg-8">
        <div class="card shadow-sm border-0">
          <div class="card-body">
            {!! Form::open([
                'enctype' => 'multipart/form-data',
                'route' => 'admin.profile.update',
                'method' => 'PUT',
                'id' => 'update-profile-form',
            ]) !!}
              <h5 class="mb-0">Mon compte</h5>
              <hr>
              <div class="card shadow-none border">
                <div class="card-header">
                  <h6 class="mb-0">Mes cordonnées</h6>
                </div>
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-12 col-lg-6 required">
                          <label class="form-label">Nom</label>
                          <input name="name"  type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom" value="{{ old('name', auth()->user()->name) }}">
                          @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="col-12 col-lg-6 required">
                          <label class="form-label">Email</label>
                          <input name="email" type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', auth()->user()->email) }}">
                          @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="col-12 col-lg-6">
                          <label class="form-label">Tél</label>
                          <input name="phone" type="text" class="form-control form-control-sm" placeholder="Tél" value="{{ old('phone', auth()->user()->phone) }}">
                        </div>
                        <div class="col-12 col-lg-6">
                          <label class="form-label">RIB</label>
                          <input name="rib" type="text" class="form-control form-control-sm" placeholder="RIB" value="{{ old('rib', auth()->user()->rib) }}">
                        </div>
                        <div class="col-12 col-lg-12">
                          <label class="form-label">Adresse</label>
                          <input name="adresse" type="text" class="form-control form-control-sm" placeholder="Adresse" value="{{ old('adresse', auth()->user()->adresse) }}">
                        </div>
                        <div class="col-12">
                          <label class="form-label">Avatar</label>
                          <input name="avatar" class="form-control form-control-sm" type="file">
                        </div>
                      </div>
                </div>
              </div>
              <div class="card shadow-none border">
                <div class="card-header">
                  <h6 class="mb-0">Modifier mot de passe</h6>
                </div>
                <div class="card-body">
                    <div class="row g-3">
                     <div class="col-6">
                        <label class="form-label">Mot de passe</label>
                        <input type="password" autocomplete="new-password" class="form-control form-control-sm @error('password') is-invalid @enderror" id="password" name="password">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                     </div>
                     <div class="col-6">
                      <label class="form-label">Confirmer mot de passe</label>
                      <input type="password" autocomplete="new-password" class="form-control form-control-sm @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation">
                    </div>
                </div>
                </div>
              </div>
              <div class="text-start">
                <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
              </div>
              {!! Form::close() !!}
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-4">
        <div class="card shadow-sm border-0 overflow-hidden">
          <div class="card-body">
              <div class="profile-avatar text-center">
                <img src="{{ asset(auth()->user()->image_url) }}" class="rounded-circle shadow" width="120" height="120" alt="">
              </div>
           
              <div class="text-center mt-4">
                <h3 class="mb-1">{{ auth()->user()->name }}</h3>
                <div class="mt-4"></div>
                <h6 class="mb-1">{{ auth()->user()->role }}</h6>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!--end row-->

  </main>
<!--end page main-->
@endsection

@section('scripts')
@endsection