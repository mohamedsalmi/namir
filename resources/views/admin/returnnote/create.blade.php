@extends('admin.layouts.app')

@section('title', 'Créer bon de retour BL')

@section('stylesheets')
    <link href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb  d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}} </div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Créer bon de retour BL</li>
                    </ol>
                </nav>
            </div>

        </div>
        <!--end breadcrumb-->
        {!! Form::open([
            'enctype' => 'multipart/form-data',
            'route' => 'admin.returnnote.store',
            'method' => 'POST',
            'id' => 'create-returnnote-form',
        ]) !!}
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <div class="d-sm-flex align-items-center">
                            <h5 class="mb-2 mb-sm-0">Créer bon de retour BL </h5>
                            <div class="ms-auto">
                                <button type="submit" name="save" id="save"
                                    class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row g-3">

                            <div class="col-12 col-lg-12">
                                <div class="card shadow-none bg-light border">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Nom :</label>
                                                <input type="text" class="form-control form-control-sm "
                                                    name="client_details[name]" placeholder="Nom & Prénom" id="name"
                                                    value="{{ old('name', $cart->client_details['name'], '') }}"readonly>
                                                <input type="hidden" name="client_id"
                                                    value="{{ old('name', $cart->client_id, '') }}">
                                                <input type="hidden" name="cart_id"
                                                    value="{{ old('name', $cart->id, '') }}">
                                                @error('client_details.name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Adresse
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm " name="client_details[adresse]"
                                                    placeholder="Adresse" id="adresse"
                                                    value="{{ old('adresse', $cart->client_details['adresse'], '') }}"readonly>
                                                @error('client_details.adresse')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>MF :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm MF" name="client_details[mf]"
                                                    placeholder="MF" id="marticule"
                                                    value="{{ old('mf', $cart->client_details['mf'], '') }}"readonly>
                                                @error('client_details.mf')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <label class="form-label"><span style="color: red;">*</span>Téléphone
                                                    :</label>
                                                <input type="text" style="display: block;"
                                                    class="form-control form-control-sm mobile "
                                                    name="client_details[phone]" placeholder="tel" id="tel"
                                                    value="{{ old('phone', $cart->client_details['phone'], '') }}"readonly>
                                                @error('client_details.phone')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-12 col-lg-4">
                                                <label class="form-label" for="exampleFormControlInput1"><span
                                                        style="color: red;">*</span><i class="fa fa-calendar"></i> Date
                                                    :</label>
                                                <input type="date"
                                                    class="form-control form-control-sm datetimepicker-input" name="date"
                                                    value="{{ old('date', '' . date('Y-m-d') . '') }}" />
                                                @error('date')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12 col-lg-12">
                                                <h6 class="form-label" for="exampleFormControlInput1">Liste des
                                                    articles de retour :</h6>
                                            </div>
                                            <div class="table-responsive card">
                                                <table class="table table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom du produit</th>
                                                            <th>Qté</th>
                                                            <th>Unité</th>
                                                            {{-- <th>Remise(%)</th> --}}
                                                            <th>TVA(%)</th>
                                                            <th>Prix</th>
                                                            <th>Total</th>
                                                            <th>Controle</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamic_field">
                                                        <tr hidden grade="0">
                                                        </tr>
                                                        @error('items')
                                                            <tr>
                                                                <div
                                                                    class="alert border-0 bg-light-danger alert-dismissible fade show py-2">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="fs-3 text-danger"><i
                                                                                class="bi bi-x-circle-fill"></i>
                                                                        </div>
                                                                        <div class="ms-3">
                                                                            <div class="text-danger">{{ $message }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </tr>
                                                        @enderror
                                                        @php
                                                            $items = $cart->items;
                                                        @endphp
                                                        @foreach ($items as $key => $item)
                                                            @if ($item['possible_return'] > 0)
                                                                <tr id="row{{ $key }}" class="item_cart"
                                                                    grade="{{ $key }}">
                                                                    <td> <input type="hidden"
                                                                            name="items[{{ $key }}][product_id]"value="{{ $item['product_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_currency_id]"value="{{ $item['product_currency_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_price_buying]"value="{{ $item['product_price_buying'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][quantity_id]"
                                                                            value="{{ $item['quantity_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][price_id]"
                                                                            value="{{ $item['price_id'] }}" />
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_currency_value]"
                                                                            value="{{ $item['product_currency_value'] }}" />
                                                                        @php
                                                                            $product = \App\Models\Product::where('id', $item->product_id)->first();
                                                                        @endphp
                                                                        <input type="text"
                                                                            name="items[{{ $key }}][product_name]"
                                                                            placeholder="Nom"
                                                                            class="form-control form-control-sm name_list"
                                                                            readonly
                                                                            value="{{ $item['product_name'] }}" />
                                                                    </td>
                                                                    <td><input type="number" style="width:80px;"
                                                                            name="items[{{ $key }}][product_quantity]"
                                                                            class="form-control form-control-sm quantity"
                                                                            id="quantity{{ $key }}"
                                                                            data-product_grade="{{ $key }}"
                                                                            data-product_id="items[{{ $key }}][product_id]"
                                                                            data-max="{{ $item['possible_return'] }}"
                                                                            value="{{ $item['possible_return'] }}"
                                                                            min="1" max="" step="1">
                                                                        <input type="hidden"
                                                                            name="items[{{ $key }}][product_remise]"
                                                                            style="width: 80px;" min="0"
                                                                            max="100" step="0.01"
                                                                            class="form-control form-control-sm remise"
                                                                            id="remise{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_remise'] }}">
                                                                    </td>
                                                                    <td><input type="text" style="width: 70px;"
                                                                            class="form-control form-control-sm"
                                                                            name="items[{{ $key }}][product_unity]"value="{{ $item['product_unity'] }}"
                                                                            readonly /></td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_tva]"
                                                                            style="width: 50px;"
                                                                            class="form-control form-control-sm tva"
                                                                            id="tva{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_tva'] }}" readonly>
                                                                    </td>
                                                                    <td><input type="text"
                                                                            name="items[{{ $key }}][product_price_selling]"
                                                                            style="width: 100px;"
                                                                            class="form-control form-control-sm price "
                                                                            id="price{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_price_selling'] }}"
                                                                            readonly></td>
                                                                    <td><input type="text" style="width: 100px;"
                                                                            class="form-control form-control-sm price{{ $key }} "
                                                                            id="L_total{{ $key }}"
                                                                            data-product_id="{{ $key }}"
                                                                            value="{{ $item['product_price_selling'] * ((100 - $item['product_remise']) / 100) * $item['product_quantity'] }}"
                                                                            readonly></td>
                                                                    <td><button type="button" name="remove"
                                                                            id="{{ $key }}"
                                                                            tr="{{ $key }}"
                                                                            class="btn btn-danger btn-sm btn_remove delete"><i
                                                                                class="bi bi-trash-fill"></i></button></td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>

                                                            <td style="width:80px">
                                                                <input type="text" name="total"
                                                                    class="form-control form-control-sm total  @error('total') is-invalid @enderror"
                                                                    readonly id="total"
                                                                    value="{{ old('total', $cart->total, '') }}">
                                                                @error('total')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </td>
                                                            <th>Total</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                                <div id="listproduct"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                        <!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

        <!--end form-->
    </main>
    <!--end page main-->
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/form-select2.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/returnnote.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/form-repeater-costom1.js') }}"></script>
    <script type="text/template" id="cashdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingOne{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteCash"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseOne{?}"
aria-expanded="false"
aria-controls="flush-collapseOne{?}" value="Espéces {?}">
</div>
</h2>
<div id="flush-collapseOne{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingOne{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Curency</label>
{{ Form::select('currency_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Currency(), 'name'),1, ['id' => 'currency_id', 'name' => 'paiements[cash][{?}][currency_id]', 'class' => $errors->has('currency_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
@error('currency_id')
<div class="invalid-feedback">
{{ $message }}
</div>
@enderror
<label class="form-label ">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[cash][{?}][amount]"
placeholder="Montant Payé" required>
<input type="hidden" class="form-control form-control-sm "
name="paiements[cash][{?}][type]"
placeholder="Montant Payé" value="cash">
<input name="paiements[cash][{?}][client_id]" type="hidden"  value="1">
</div>
</div>
</div>
</script>
    <script type="text/template" id="checkdetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingTwo{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteCheck"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseTwo{?}"
aria-expanded="false"
aria-controls="flush-collapseTwo{?}" value="Chèque {?}">
</div>
</h2>
<div id="flush-collapseTwo{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingTwo{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[check][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[check][{?}][client_id]" type="hidden"  value="1">

<label class="form-label">Numero du chèque</label>
<input type="number" class="form-control form-control-sm"
name="paiements[check][{?}][numbre]"
placeholder="Numero du chèque" required>
<label class="form-label">Bank</label>
<input type="text" class="form-control form-control-sm"
name="paiements[check][{?}][bank]"
placeholder="Bank" required>
<label class="form-label">date de reglement:</label>
<input name="paiements[check][{?}][received_at]" type="date"
class="form-control form-control-sm"
aria-label="Text input with checkbox" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[check][{?}][type]"
placeholder="Montant Payé" value="check">
</div>
</div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
<div class="accordion-item ">

<h2 class="accordion-header" id="flush-headingFour{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteExchange"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseFour{?}"
aria-expanded="false"
aria-controls="flush-collapseFour{?}" value="Traite {?}">
</div>
</h2>
<div id="flush-collapseFour{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingFour{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount"
name="paiements[exchange][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
<label class="form-label">Numero de Traite</label>
<input type="number" class="form-control form-control-sm"
name="paiements[exchange][{?}][numbre]"
placeholder="Numero de la Lettre" required>
<label class="form-label">Date de Traite</label>
<input type="date" class="form-control form-control-sm"
name="paiements[exchange][{?}][received_at]" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[exchange][{?}][type]"
placeholder="Montant Payé" value="exchange" >
</div>
</div>
</div>
</script>
    <script type="text/template" id="transferdetail">
<div class="accordion-item ">
<h2 class="accordion-header" id="flush-headingThree{?}">
<div class="row">
<button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
<input class="accordion-button collapsed col" type="button"
data-bs-toggle="collapse"
data-bs-target="#flush-collapseThree{?}"
aria-expanded="false"
aria-controls="flush-collapseThree{?}" value="Virement {?}">

</div>
</h2>
<div id="flush-collapseThree{?}"
class="accordion-collapse collapse"
aria-labelledby="flush-headingThree{?}"
data-bs-parent="#accordionFlushExample">
<div class="accordion-body">
<label class="form-label">Montant Payé</label>
<input type="number" class="form-control form-control-sm amount "
name="paiements[transfer][{?}][amount]"
placeholder="Montant Payé" required>
<input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

<label class="form-label">Numero du Virement</label>
<input type="number" class="form-control form-control-sm"
name="paiements[transfer][{?}][numbre]"
placeholder="Numero du Virement" required>
<label class="form-label">Bank</label>
<input type="text" class="form-control form-control-sm"
name="paiements[transfer][{?}][bank]"
placeholder="Bank" required>
<label class="form-label">Date du Virement</label>
<input type="date" class="form-control form-control-sm"
name="paiements[transfer][{?}][received_at]" required>
<input type="hidden" class="form-control form-control-sm"
name="paiements[transfer][{?}][type]"
placeholder="Montant Payé" value="transfer">
</div>
</div>
</div>
</script>
    <script type="text/template" id="exchangedetail">
    <div class="accordion-item ">

    <h2 class="accordion-header" id="flush-headingFour{?}">
        <div class="row">
            <button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteExchange"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseFour{?}"
            aria-expanded="false"
            aria-controls="flush-collapseFour{?}" value="Traite {?}">
                </div>
    </h2>
    <div id="flush-collapseFour{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingFour{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[exchange][{?}][client_id]" type="hidden"  value="1">
            <label class="form-label">Numero de Traite</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[exchange][{?}][numbre]"
                placeholder="Numero de la Lettre">
            <label class="form-label">Date de Traite</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[exchange][{?}][received_at]">
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[exchange][{?}][type]"
                placeholder="Montant Payé" value="exchange">
        </div>
    </div>
</div>
</script>
    <script type="text/template" id="transferdetail">
    <div class="accordion-item ">
    <h2 class="accordion-header" id="flush-headingThree{?}">
        <div class="row">
            <button type="button" class="btn bx bx-trash col-2 text-danger" id="deleteTransfer"></button>
        <input class="accordion-button collapsed col" type="button"
            data-bs-toggle="collapse"
            data-bs-target="#flush-collapseThree{?}"
            aria-expanded="false"
            aria-controls="flush-collapseThree{?}" value="Virement {?}">

    </div>
    </h2>
    <div id="flush-collapseThree{?}"
        class="accordion-collapse collapse"
        aria-labelledby="flush-headingThree{?}"
        data-bs-parent="#accordionFlushExample">
        <div class="accordion-body">
            <label class="form-label">Montant Payé</label>
            <input type="number" class="form-control form-control-sm "
                name="paiements[transfer][{?}][amount]"
                placeholder="Montant Payé">
                <input name="paiements[transfer][{?}][client_id]" type="hidden"  value="1">

            <label class="form-label">Numero du Virement</label>
            <input type="number" class="form-control form-control-sm"
                name="paiements[transfer][{?}][numbre]"
                placeholder="Numero du Virement">
                <label class="form-label">Bank</label>
                <input type="text" class="form-control form-control-sm"
                    name="paiements[transfer][{?}][bank]"
                    placeholder="Bank">
            <label class="form-label">Date du Virement</label>
            <input type="date" class="form-control form-control-sm"
                name="paiements[transfer][{?}][received_at]">
                <input type="hidden" class="form-control form-control-sm"
                name="paiements[transfer][{?}][type]"
                placeholder="Montant Payé" value="transfer">
        </div>
    </div>
</div>
</script>
    <script>
        $(function() {
            $(".repeat_check").repeatable({
                addTrigger: "#addCheck",
                deleteTrigger: "#deleteCheck",
                max: 400,
                min: 0,
                template: "#checkdetail",
                itemContainer: ".accordion-item",

            });
        })
        $(function() {
            $(".repeat_cash").repeatable2({
                addTrigger: "#addCash",
                deleteTrigger: "#deleteCash",
                max: 400,
                min: 0,
                template: "#cashdetail",
                itemContainer: ".accordion-item",
            });
        })
        $(function() {
            $(".repeat_exchange").repeatable3({
                addTrigger: "#addExchange",
                deleteTrigger: "#deleteExchange",
                max: 400,
                min: 0,
                template: "#exchangedetail",
                itemContainer: ".accordion-item",
            });
        })
        $(function() {
            $(".repeat_transfer").repeatable4({
                addTrigger: "#addTransfer",
                deleteTrigger: "#deleteTransfer",
                max: 400,
                min: 0,
                template: "#transferdetail",
                itemContainer: ".accordion-item",
            });
        })
    </script>
@endsection
