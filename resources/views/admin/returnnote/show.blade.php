@extends('admin.layouts.app')

@section("title', 'Bon de retour BL")

@section('stylesheets')
@endsection
@section('content')

    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">{{config('stock.info.name')}} </div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Bon de retour BL
                            <strong>#{{ $returnnote->codification }}</strong>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->


        <div class="card border shadow-none">
            <div class="card-header py-3">
                <div class="row align-items-center g-3">
                    <div class="col-12 col-lg-6">
                        <h5 class="mb-0">{{ $returnnote->client_details['name'] }}</h5>
                    </div>
                    @canany(['Imprimer bon de retour BL']) 
                    <div class="col-12 col-lg-6 text-md-end">
                        <a href="{{ route('admin.returnnote.print', ['returnnote' => $returnnote->id, 'type' => 'download']) }}"
                            class="btn btn-sm btn-danger me-2"><i class="bi bi-file-earmark-pdf-fill"></i> Téléchrger
                            PDF</a>
                        <a id="imprimer" type="button" class="btn btn-sm btn-secondary"><i class="bi bi-printer-fill"></i>
                            Imprimer</a>
                    </div>
                    @endcanany
                </div>
            </div>
            <div class="card-header py-2 bg-light">
                <div class="row row-cols-1 row-cols-lg-3">
                    <div class="col">
                        <div class="">
                            <small>De</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">Nom De Client</strong><br>
                                {{ $returnnote->client_details['name'] }}<br>
                                {{ $returnnote->client_details['adresse'] }}<br>
                                Tél: {{ $returnnote->client_details['phone'] }}<br>
                            </address>
                        </div>
                    </div>
                    <div class="col">
                        <div class="">
                            <small>à</small>
                            <address class="m-t-5 m-b-5">
                                <strong class="text-inverse">{{config('stock.info.name')}} </strong><br>
                                {{config('stock.info.adresse')}}<br>
                                Tél: {{config('stock.info.phone')}}<br>
                            </address>
                        </div>
                    </div>
                    <div class="col">
                        <div class="">
                            {{-- <small>ReturnNote / July period</small> --}}
                            <div class=""><b>{{ $returnnote->created_at }}</b></div>
                            <div class="returnnote-detail">
                                <strong class="text-inverse">Numéro du bon de retour BL</strong><br>
                                #{{ $returnnote->codification }}<br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-returnnote">
                        <thead>
                            <tr>
                                <th>Désignation</th>
                                <th class="text-center" width="10%">QTE</th>
                                <th class="text-center" width="10%">UT</th>
                                <th class="text-center" width="10%">PUHT</th>
                                <th class="text-center" width="10%">REMISE</th>
                                <th class="text-center" width="10%">TVA</th>
                                <th class="text-center" width="10%">PUTTC</th>
                                <th class="text-right" width="20%">TOTAL TTC</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_tva = 0;
                                $total_ut = 0;
                            @endphp
                            @foreach ($returnnote->items as $item)
                                <tr>
                                    <td>
                                        <span class="text-inverse">{{ $item->product_name }} </span><br>
                                        {{-- <small>{{ $item?->price?->productAttribute1?->attribute?->name }}:{{ $item?->price?->productValue1?->attributevalue?->value }}
                                            {{ $item?->price?->productAttribute2?->attribute?->name }}:{{ $item?->price?->productValue2?->attributevalue?->value }}
                                        </small> --}}
                                    </td>
                                    @php
                                        $pu=($item->product_price_selling*100)/(100+$item->product_tva);
                                        $couttva=($pu * $item->product_tva) / 100;
                                        $total_ut += $pu * $item->product_quantity;
                                        $total_tva += $couttva * $item->product_quantity;
                                    @endphp
                                    <td class="text-center">{{ $item->product_quantity }}</td>
                                    <td class="text-center">{{ $item->product_unity }}</td>
                                    <td class="text-center">{{ number_format($pu, 3) }}</td>
                                    <td class="text-center">{{ number_format($item->remise, 3) }}</td>
                                    <td class="text-center">{{ number_format($couttva, 3) }}</td>
                                    <td class="text-center">{{ number_format($item->product_price_selling, 3) }}</td>
                                    <td class="text-right">
                                        {{ number_format($item->product_price_selling * $item->product_quantity, 3) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @php
                    $timbre = 0;
                    if ($returnnote->timbre) {
                        $timbre = \App\Models\Setting::where('name', 'Timbre fiscal')->first()->value;
                    }
                @endphp
                <div class="row bg-light align-items-center m-0">
                    <div class="col col-auto  p-4">
                        <p class="mb-0">TOTAL HT</p>
                        <h4 class="mb-0">{{ number_format($returnnote->total - $total_tva, 3) }}</h4>
                    </div>
                    <div class="col col-auto p-4">
                        <i class="bi bi-plus-lg text-muted"></i>
                    </div>
                    <div class="col col-auto me-auto p-4">
                        <p class="mb-0">TOTAL TVA</p>
                        <h4 class="mb-0">{{ number_format($total_tva, 3) }}</h4>
                    </div>

                    <div class="col col-auto bg-secondary  p-4">
                        <p class="mb-0 text-white">TOTAL TTC</p>
                        <h4 class="mb-0 text-white">{{ number_format($returnnote->total, 3) }}</h4>
                    </div>
                    <div class="col col-auto p-4">
                        <i class="bi bi-plus-lg text-muted"></i>
                    </div>
                    <div class="col col-auto me-auto p-4">
                        <p class="mb-0">TIMBRE</p>
                        <h4 class="mb-0">{{ number_format($timbre, 3) }}</h4>
                    </div>
                    <div class="col bg-dark col-auto p-4">
                        <p class="mb-0 text-white">TOTAL A PAYER </p>
                        <h4 class="mb-0 text-white">{{ number_format($returnnote->total+$timbre, 3) }}</h4>
                    </div>
                </div>
                <!--end row-->

                {{-- <hr> --}}
                <!-- begin returnnote-note -->
                {{-- <div class="my-3">
                    * Make all cheques payable to [Your Company Name]<br>
                    * Payment is due within 30 days<br>
                    * If you have any questions concerning this returnnote, contact [Name, Phone Number, Email]
                </div> --}}
                <!-- end returnnote-note -->
            </div>

            {{-- <div class="card-footer py-3">
                <p class="text-center mb-2">
                    THANK YOU FOR YOUR BUSINESS
                </p>
                <p class="text-center d-flex align-items-center gap-3 justify-content-center mb-0">
                    <span class=""><i class="bi bi-globe"></i> www.domain.com</span>
                    <span class=""><i class="bi bi-telephone-fill"></i> T:+11-0462879</span>
                    <span class=""><i class="bi bi-envelope-fill"></i> info@example.com</span>
                </p>
            </div> --}}
        </div>

    </main>


@endsection
@section('scripts')

    <script>
        $(document).on("click", "#imprimer", function(e) {
            printPdf();
        });
        printPdf = function() {
            var iframe = this._printIframe;
            if (!this._printIframe) {
                iframe = this._printIframe = document.createElement('iframe');
                document.body.appendChild(iframe);

                iframe.style.display = 'none';
                iframe.onload = function() {
                    setTimeout(function() {
                        iframe.focus();
                        iframe.contentWindow.print();
                    }, 1);
                };
            }

            iframe.src = '{{ route('admin.returnnote.print', ['returnnote' => $returnnote->id, 'type' => 'stream']) }}';
        }
    </script>

@endsection
