@extends('admin.layouts.app')

@section('title', 'Ajouter une catégorie')

@section('content')
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Catégories</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="{{route('admin.category.list')}}">Liste des catégories</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ajouter une catégorie</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->

        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card">
                    <div class="card-header py-3 bg-transparent">
                        <h5 class="mb-0">Ajouter une catégorie</h5>
                    </div>
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            {!! Form::open([
                                'enctype' => 'multipart/form-data',
                                'route' => 'admin.category.store',
                                'method' => 'POST',
                                'id' => 'create-category-form',
                                'class' => 'row g-3',
                            ]) !!}
                            <div class="col-12">
                                <label class="form-label">Nom catégorie</label>
                                <input name="name" type="text"
                                    class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom catégorie"
                                    value="{{ old('name', '') }}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label class="form-label">Parent</label>
                                <select name="parent_id" id="parent_id" value="{{ old('parent_id', '') }}"
                                    class="form-control form-control-sm form-select @error('parent_id') is-invalid @enderror">
                                    <option value="">Choisir une catégorie</option>
                                    @foreach ($categories as $cat)
                                        @include('admin.partials.subcategories', ['cat' => $cat])
                                    @endforeach
                                </select>
                                @error('parent_id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            {{-- <div class="col-12">
                                <label class="form-label">Parent</label>
                                {{ Form::select('parent_id', \App\Helpers\Helper::makeDropDownListFromModel(new \App\Models\Category(), 'name'), null, ['id' => 'parent_id', 'name' => 'parent_id', 'class' => $errors->has('parent_id') ? 'form-control form-control-sm form-select is-invalid' : 'form-control form-control-sm form-select']) }}
                                @error('parent_id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div> --}}
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

    </main>
@endsection
