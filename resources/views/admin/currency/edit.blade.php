@extends('admin.layouts.app')

@section('title', 'Modifier une devise')

@section('content')
<main class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
              <div class="breadcrumb-title pe-3">Devises</div>
              <div class="ps-3">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Modifier une devise</li>
                  </ol>
                </nav>
              </div>
            </div>
            <!--end breadcrumb-->

              <div class="row">
                 <div class="col-lg-8 mx-auto">
                  <div class="card">
                    <div class="card-header py-3 bg-transparent"> 
                       <h5 class="mb-0">Modifier une devise</h5>
                      </div>
                    <div class="card-body">
                      <div class="border p-3 rounded">
                      {!! Form::open(array('enctype'=>'multipart/form-data','route' => array('admin.currency.update', $currency->id), 'method'=>'PUT', 'id'=>'update-currency-form', 'class' => 'row g-3')) !!}
                        <div class="col-12">
                          <label class="form-label">Nom devise</label>
                          <input name="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Nom devise" value="{{ old('name', $currency->name) }}">
                          @error('name')
                           <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Valeur</label>
                          <input name="value" step="any" type="number" class="form-control form-control-sm @error('value') is-invalid @enderror" placeholder="Valeur" value="{{ old('value', $currency->value) }}">
                          @error('value')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Symbole</label>
                          <input name="symbol" step="any" type="text" class="form-control form-control-sm @error('symbol') is-invalid @enderror" placeholder="Symbole" value="{{ old('symbol', $currency->symbol) }}">
                          @error('symbol')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="col-12">
                          <label class="form-label">Mettre à jour les prix des articles</label>
                          <div class="form-check form-switch">
                            <input class="form-check-input"  name="price_update" type="checkbox">
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="buying" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Prix d'achat</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="selling1" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Prix de vente 1</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="selling2" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Prix de vente 2</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="selling3" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Prix de vente 3</label>
                          </div>
                        </div>
                        <div class="col-12">
                          <button type="submit" class="btn btn-primary px-4">Enregistrer</button>
                        </div>
                        {!! Form::close() !!}
                      </div>
                      
                     </div>
                    </div>
                 </div>
              </div><!--end row-->

          </main>
          @endsection