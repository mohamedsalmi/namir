<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('sku')->nullable();
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->json('price_type');
            $table->json('quantity_type');
            $table->boolean('negative_inventory')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('by_order')->default(false);
            $table->string('unity');
            $table->float('tva');
            $table->decimal('discount', 15, 3)->default(0);
            $table->decimal('maxdiscount', 15, 3)->default(100);
            $table->decimal('buying', 15, 3)->default(0);
            $table->decimal('selling1', 15, 3)->default(0);
            $table->decimal('selling2', 15, 3)->default(0);
            $table->decimal('selling3', 15, 3)->default(0);
            $table->string('metatitle')->nullable();
            $table->string('metakeys')->nullable();
            $table->string('metadescription')->nullable();
            $table->integer('minimum_quantity')->default(0);
            $table->integer('reserve_quantity')->default(0);
            $table->foreignId('currency_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
