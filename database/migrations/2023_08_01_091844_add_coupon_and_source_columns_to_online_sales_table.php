<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('online_sales', 'coupon')) {
            Schema::table('online_sales', function (Blueprint $table) {
                $table->string('coupon')->nullable();
            });
        }
        if (!Schema::hasColumn('online_sales', 'source')) {
            Schema::table('online_sales', function (Blueprint $table) {
                $table->string('source')->default('app')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('online_sales', function (Blueprint $table) {
            //
        });
    }
};
