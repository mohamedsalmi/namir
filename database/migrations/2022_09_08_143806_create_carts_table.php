<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->string('note')->nullable();
            $table->string('codification')->unique();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->integer('client_id');
            $table->foreignId('store_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('invoice_id')->default(Null);
            $table->foreignId('user_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('status')->default('');
            $table->boolean('paiment_status')->default(false);
            $table->boolean('return_status')->default(true);
            $table->date('date')->nullable();
            $table->string('type')->default('simple');
            $table->json('client_details')->nullable();
            $table->integer('number')->default(0);
            $table->decimal('total', 20, 3)->default(0);
            $table->boolean('timbre')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
};
