<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mus', function (Blueprint $table) {
            $table->id();
            $table->string('note')->nullable();
            $table->integer('user_id');
            $table->string('codification')->unique();
 $table->string('created_by')->nullable();
 $table->string('updated_by')->nullable();
            $table->integer('store_origin');
            $table->integer('store_destination');
            $table->string('status')->default('0');
            $table->string('type')->nullable();
            $table->date('date')->nullable();
            $table->integer('number')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mus');
    }
};
