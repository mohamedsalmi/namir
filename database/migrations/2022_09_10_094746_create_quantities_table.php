<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quantities', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity')->default(0);
            $table->integer('minimum_quantity')->default(0);
            // $table->foreignId('barcode_id')
            // ->constrained()
            // ->onUpdate('cascade')
            // ->onDelete('cascade');
            $table->foreignId('product_id')
            ->constrained()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('store_id')
            ->constrained()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->integer('product_attribute1')->nullable();
        $table->integer('product_attribute2')->nullable();
        $table->integer('product_value1')->nullable();
        $table->integer('product_value2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quantities');
    }
};
