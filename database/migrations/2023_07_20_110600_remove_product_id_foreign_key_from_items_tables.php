<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public $tables = [
        'carts_items',
        'order_items',
        'sale_quote_items',
        'voucher_items',
        'invoice_items',
        'credit_note_items',
        'return_coupon_items',
        'mu_items',
        'return_note_items',
        'inventory_items',
        'online_sale_items',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $key => $table_name) {
            if(Schema::hasTable($table_name)){
                Schema::table($table_name, function (Blueprint $table) {
                    $table->dropForeign(['product_id']);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
};
