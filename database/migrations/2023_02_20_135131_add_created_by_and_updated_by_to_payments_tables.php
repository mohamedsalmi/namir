<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public $tables = [
        'payment_creditnotes',
        'commitment_payments',
        'payment_carts',
        'return_note_payments',
        'return_payments',
        'withdraws'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $key => $table_name) {
            Schema::table($table_name, function (Blueprint $table) {
                $table->string('created_by')->nullable();
                $table->string('updated_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $key => $table_name) {
            Schema::table($table_name, function (Blueprint $table) {
                $table->dropColumn('created_by');
                $table->dropColumn('updated_by');
            });
        }
    }
};
