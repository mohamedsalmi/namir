<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_coupons', function (Blueprint $table) {
            $table->id();
            $table->string('note')->nullable();
            $table->integer('provider_id');
            $table->string('codification')->unique();
 $table->string('created_by')->nullable();
 $table->string('updated_by')->nullable();
            $table->foreignId('voucher_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('store_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('user_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('status')->default('');
            $table->boolean('paiment_status')->default(false);
            $table->date('date')->nullable();
            $table->string('type')->default('simple');
            $table->json('provider_details')->nullable();
            $table->integer('number')->default(0);
            $table->decimal('total',20,3)->default(0);
            $table->boolean('timbre')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_coupons');
    }
};
