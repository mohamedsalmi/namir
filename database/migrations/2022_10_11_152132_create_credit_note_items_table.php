<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_note_items', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->integer('product_quantity')->default(0);
            $table->decimal('product_tva',20,3)->default(0);
            $table->string('product_unity')->nullable();
            $table->decimal('product_remise',20,3)->default(0);
            $table->decimal('product_price_buying',20,3)->default(0);
            $table->decimal('product_price_selling',20,3)->default(0);
            $table->integer('product_currency_id');
            $table->string('product_currency_value');
            $table->string('options')->nullable();
            $table->foreignId('credit_note_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('product_id')
                ->constrained();
            $table->integer('quantity_id');
            $table->integer('price_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_note_items');
    }
};
