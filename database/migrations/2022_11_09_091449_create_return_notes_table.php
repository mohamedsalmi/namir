<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_notes', function (Blueprint $table) {
            $table->id();
            $table->string('note')->nullable();
            $table->integer('client_id');
            $table->foreignId('store_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('user_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('cart_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('paiment_status')->default(false);
            $table->enum('status', ['ready', 'confirmed'])->default('ready');
            $table->json('client_details')->nullable();
            $table->integer('number')->default(0);
            $table->string('codification')->unique();
 $table->string('created_by')->nullable();
 $table->string('updated_by')->nullable();
            $table->decimal('total',20,3)->default(0);
            $table->boolean('timbre')->default(false);
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_notes');
    }
};
