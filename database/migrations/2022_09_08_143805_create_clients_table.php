<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('site')->default(0);
            $table->string('password')->default(null);
            $table->string('client_code')->nullable();
            $table->decimal('discount',15,3)->default(0);
            $table->string('email')->nullable();
            $table->string('adresse')->nullable();
            $table->string('mf')->nullable();
            $table->string('rib')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('google_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('price')->default('detail');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
