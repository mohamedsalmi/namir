<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->decimal('buying',15,3)->default(0);
            $table->decimal('selling1',15,3)->default(0);
            $table->decimal('selling2',15,3)->default(0);
            $table->decimal('selling3',15,3)->default(0);
            $table->foreignId('product_id')
            ->constrained()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('store_id')
            ->constrained()
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('product_attribute1')
            ->nullable()
            ->constrained('product_attributes')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('product_attribute2')
            ->nullable()
            ->constrained('product_attributes')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('product_value1')
            ->nullable()
            ->constrained('product_attribute_values')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('product_value2')
            ->nullable()
            ->constrained('product_attribute_values')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
};
