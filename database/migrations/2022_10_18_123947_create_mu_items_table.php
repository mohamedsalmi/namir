<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mu_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mu_id')
            ->constrained()
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreignId('product_id')
            ->constrained();
            $table->string('product_name');
            $table->integer('product_quantity')->default(0);
            $table->integer('quantity_id')->nullable();
            $table->integer('price_id')->nullable();
            $table->decimal('product_tva',20,3)->nullable();
            $table->string('product_unity')->nullable();
            $table->decimal('product_remise',20,3)->nullable();
            $table->decimal('product_price_buying',20,3)->nullable();
            $table->decimal('product_price_selling',20,3)->nullable();
            $table->integer('product_currency_id')->nullable();
            $table->string('product_currency_value')->nullable();
            $table->string('options')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mu_items');
    }
};
