<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $store = Store::first();
        
        $admin = User::create(['name' => 'Admin', 'email' => 'admin@namir.tn', 'password' => bcrypt('password'), 'store_id' => $store->id]);
        $roleAdmin = Role::where(['name' => 'Admin', 'guard_name' => 'web'])->first();
        $admin->assignRole($roleAdmin);

        $roleManager = Role::where(['name' => 'Manager', 'guard_name' => 'web'])->first();
        $manager = User::create(['name' => 'Manager', 'email' => 'manager@namir.tn', 'password' => bcrypt('password'), 'store_id' => $store->id]);
        $manager->assignRole($roleManager);

        $manager2 = User::create(['name' => 'Manager 2', 'email' => 'manager2@namir.tn', 'password' => bcrypt('password'), 'store_id' => $store->id]);
        $manager2->assignRole($roleManager);

    }
}
