<?php

namespace Database\Seeders;

use App\Models\Attribute;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
            [
                'attribute' => ['name'  => 'الطبعة', 'type' => 'string'],
                'values' => [
                    [
                        'value' => 'الطبعة الاولى'
                    ],
                    [
                        'value' => 'الطبعة الثانية'
                    ],
                    [
                        'value' => 'الطبعة الثالثة'
                    ],
                    [
                        'value' => 'الطبعة الرابعة'
                    ],
                    [
                        'value' => 'الطبعة الخامسة'
                    ],
                    [
                        'value' => 'الطبعة السادسة'
                    ],
                    [
                        'value' => 'الطبعة السابعة'
                    ],
                    [
                        'value' => 'الطبعة الثامنة'
                    ],
                    [
                        'value' => 'الطبعة التاسعة'
                    ],
                    [
                        'value' => 'الطبعة العاشرة'
                    ],
                ]
            ],
            [
                'attribute' => ['name'  => 'دار النشر', 'type' => 'text'],
                'values' => [
                    [
                        'value' => 'الدار المصرية اللبنانية'
                    ],
                    [
                        'value' => 'دار المعارف'
                    ],
                    [
                        'value' => 'دار الشروق'
                    ],

                ]
            ],
            [
                'attribute' => ['name'  => 'اسم المؤلف', 'type' => 'text'],
                'values' => [
                    [
                        'value' => 'محمد بن عبد الوهاب'
                    ],
                    [
                        'value' => 'أحمد بن عبدالحليم بن تيمية'
                    ],
                    [
                        'value' => 'شمس الدين محمد بن القيم'
                    ],

                ]
            ],
            [
                'attribute' => ['name'  => 'المحقق', 'type' => 'text'],
                'values' => [
                    [
                        'value' => 'محمد ناصر الدين الالباني'
                    ],
                    [
                        'value' => 'أحمد محمد شاكر'
                    ],
                    [
                        'value' => 'بكر أبو زيد'
                    ],
                    [
                        'value' => 'محمود محمد شاكر'
                    ],

                ]
            ],
            [
                'attribute' => ['name'  => 'عدد الصفحات', 'type' => 'decimal'],
                'values' => [
                    [
                        'value' => '200'
                    ],
                    [
                        'value' => '500'
                    ],



                ]
            ],
            [
                'attribute' => ['name'  => 'عدد المجلدات', 'type' => 'decimal'],
                'values' => [
                    [
                        'value' => '10'
                    ],
                    [
                        'value' => '5'
                    ],



                ]
            ],
            [
                'attribute' => ['name'  => 'نوعية الورق', 'type' => 'text'],
                'values' => [
                    [
                        'value' => 'ورق أبيض'
                    ],




                ]
            ],
            [
                'attribute' => ['name'  => 'الحجم', 'type' => 'text'],
                'values' => [
                    [
                        'value' => '10'
                    ],
                    [
                        'value' => '5'
                    ],



                ]
            ],
            [
                'attribute' => ['name'  => 'الوزن', 'type' => 'decimal'],
                'values' => [
                    [
                        'value' => '10'
                    ],
                    [
                        'value' => '5'
                    ],



                ]
            ],
            [
                'attribute' => ['name'  => 'العروض', 'type' => 'decimal'],
                'values' => [
                    [
                        'value' => 'العروض الاسبوعية'
                    ],
                    [
                        'value' => 'حديثا'
                    ],
                    [
                        'value' => 'المميزة'
                    ],
                    [
                        'value' => 'الاكثر مبيعا'
                    ],
                    [
                        'value' => 'العروض المحدودة'
                    ],




                ]
            ],
        ];

        DB::table('attributes')->delete();

        foreach ($attributes as $key => $att) {
            $attribute = Attribute::create($att['attribute']);
            $attribute->values()->createMany($att['values']);
        }
    }
}