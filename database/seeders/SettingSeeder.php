<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'name'  => 'Coefficient prix détails',
                'value' => 10,
            ],
            [
                'name'  => 'Coefficient prix semi-gros',
                'value' => 8,
            ],
            [
                'name'  => 'Coefficient prix gros',
                'value' => 5,
            ],
            [
                'name'  => 'Timbre fiscal',
                'value' => 1.000,
            ],
            [
                'name'  => 'Facebook',
                'value' => 'https://www.facebook.com/',
            ],
            [
                'name'  => 'Instagram',
                'value' => 'https://www.instagram.com/',
            ],
            [
                'name'  => 'Youtube',
                'value' => 'https://www.youtube.com/',
            ],
            [
                'name'  => 'Linkedin',
                'value' => 'https://www.linkedin.com/',
            ],
            [
                'name'  => 'Prix de livraison',
                'value' => 7,
            ],
            [
                'name'  => 'Adresse',
                'value' => 'Bardo',
            ],
            [
                'name'  => 'Google Maps',
                'value' => '',
            ],
            [
                'name'  => 'Téléphone',
                'value' => '',
            ],
            [
                'name'  => 'Fax',
                'value' => '',
            ],
            [
                'name'  => 'RIB',
                'value' => '',
            ],
            [
                'name'  => 'MF',
                'value' => '',
            ],
            [
                'name'  => 'Registre de commerce',
                'value' => '',
            ],
            [
                'name'  => 'Email',
                'value' => '',
            ],
            [
                'name'  => 'Domaine',
                'value' => 'Librairie',
            ],

        ];

        $documents = [
            'BL', 'devis', 'BR', 'commande', 'facture avoir', 'inventaire', 'facture', 'stock', 'bon de retour BL', 'BA', 'vente en ligne'
        ];

        foreach ($documents as $key => $document) {
            $setting = [
                'name'  => 'Codification ' . $document,
                'value' => date('Y') . '-00000',
            ];
            array_push($settings, $setting);
        }

        foreach ($settings as $key => $item) {
            $setting = Setting::firstOrCreate(['name' => $item['name']], $item);
        }
    }
}
