<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(StoreSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(RegionsTableSeeder::class);
        // $this->call(ClientsTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
    }
}
