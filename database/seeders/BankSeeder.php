<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\Store;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->delete();

        $stb = Bank::create(['name' => 'STB']);

    }
}
