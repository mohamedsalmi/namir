<?php

namespace Database\Seeders;

use App\Models\Store;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->delete();
        
        Store::create(['name' => 'Bardoo', 'email' => 'store1@namir.tn', 'phone' => '22222222', 'address' => 'Montplaisir', 'manager_id' => 2, 'website_sales' => true]);
        Store::create(['name' => 'Manar', 'email' => 'store2@namir.tn', 'phone' => '22222222', 'address' => 'Montplaisir', 'manager_id' => 3]);
    }
}
