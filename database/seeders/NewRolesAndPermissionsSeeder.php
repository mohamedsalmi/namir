<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;
use Spatie\Permission\Models\Role;

class NewRolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $models = [
            [
                'name' => 'coupon',
                'permissions' => []
            ],
            [
                'name' => 'sms',
                'permissions' => []
            ],
        ];

        $defaultPermissions = ['Liste', 'Ajouter', 'Modifier', 'Supprimer', 'Détails', 'Imprimer'];

        foreach ($models as $key => $model) {
            if(!empty($model['permissions'])){
                foreach ($model['permissions'] as $key => $permission) {
                    $permission = Permission::firstOrCreate(['name' => $permission], ['guard_name' => 'web', 'group' => $model['name']]);
                }
            }
            foreach ($defaultPermissions as $key => $perm) {
                $permission = Permission::firstOrCreate(['name' => $perm . ' ' . $model['name']], ['guard_name' => 'web', 'group' => $model['name']]);
            }
        }
    }
}
