<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        DB::table('roles')->delete();
        DB::table('permissions')->delete();

        $roleAdmin = Role::create(['name' => 'Admin', 'guard_name' => 'web']);
        $roleManager = Role::create(['name' => 'Manager', 'guard_name' => 'web']);

        $models = [
            [
                'name' => 'devise',
                'permissions' => []
            ],
            [
                'name' => 'point de vente',
                'permissions' => []
            ],
            [
                'name' => 'catégorie',
                'permissions' => []
            ],
            [
                'name' => 'attribut',
                'permissions' => []
            ],
            [
                'name' => 'article',
                'permissions' => ['Modifier les quantités']
            ],
            [
                'name' => 'client',
                'permissions' => ['Modifier les prix']
            ],
            [
                'name' => 'fournisseur',
                'permissions' => []
            ],
            [
                'name' => 'commande',
                'permissions' => ['Générer Bl']
            ],
            [
                'name' => 'utilisateur',
                'permissions' => []
            ],
            [
                'name' => 'rôle',
                'permissions' => []
            ],
            [
                'name' => 'devis',
                'permissions' => ['Générer commande']
            ],
            [
                'name' => 'facture',
                'permissions' => []
            ],
            [
                'name' => 'facture avoir',
                'permissions' => []
            ],
            [
                'name' => 'bon de livraison',
                'permissions' => []
            ],
            [
                'name' => 'bon de retour BL',
                'permissions' => []
            ],
            [
                'name' => 'bon d\'achat',
                'permissions' => []
            ],
            [
                'name' => 'bon de retour',
                'permissions' => []
            ],
            [
                'name' => 'encaissement',
                'permissions' => ['Détails échéance encaissement']
            ],
            [
                'name' => 'décaissement',
                'permissions' => ['Détails échéance décaissement']
            ],
            [
                'name' => 'bon de sortie',
                'permissions' => []
            ],
            [
                'name' => 'bon de commande',
                'permissions' => []
            ],
            [
                'name' => 'bon de réception',
                'permissions' => []
            ],
            [
                'name' => 'caisse',
                'permissions' => ['Retirer', 'Supprimer retrait']
            ],
            [
                'name' => 'inventaire',
                'permissions' => []
            ],
            [
                'name' => 'coupon',
                'permissions' => []
            ],
            [
                'name' => 'sms',
                'permissions' => []
            ],
        ];

        $defaultPermissions = ['Liste', 'Ajouter', 'Modifier', 'Supprimer', 'Détails', 'Imprimer'];

        foreach ($models as $key => $model) {
            if(!empty($model['permissions'])){
                foreach ($model['permissions'] as $key => $permission) {
                    $permission = Permission::create(['name' => $permission, 'guard_name' => 'web', 'group' => $model['name']]);
                    $roleAdmin->givePermissionTo($permission);
                }
            }
            foreach ($defaultPermissions as $key => $perm) {
                $permission = Permission::create(['name' => $perm . ' ' . $model['name'], 'guard_name' => 'web', 'group' => $model['name']]);
                $roleAdmin->givePermissionTo($permission);
            }
        }
    }
}
