<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'  => 'Product 1',
                'sku' => 'Product 1',
                'description' => 'Product 1',
                'quantity_type' => ["type"=>"Dépend de deux attributs","attributes"=>["1","2"]],
                'price_type' => ["type"=>"Dépend de deux attributs","attributes"=>["1","2"]],
                'unity' => '1',
                'tva' => 7,
                'currency_id' => '1',
            ],
            [
                'name'  => 'Product 2',
                'sku' => 'Product 2',
                'description' => 'Product 2',
                'quantity_type' => ["type"=>"simple"],
                'price_type' => ["type"=>"simple"],
                'unity' => '1',
                'tva' => 7,
                'currency_id' => '1',
            ],
            [
                'name'  => 'Product 3',
                'sku' => 'Product 3',
                'description' => 'Product 3',
                'quantity_type' => ["type"=>"Dépend d'un attribut","attributes"=>["2"]],
                'price_type' => ["type"=>"Dépend d'un attribut","attributes"=>["1"]],
                'unity' => '1',
                'tva' => 7,
                'currency_id' => '1',
            ]
        ];

        DB::table('products')->delete();

        foreach ($products as $key => $product) {
            $prod = Product::create($product);
            $prod->categories()->create(['category_id' => 1]);
        }
    }
}
