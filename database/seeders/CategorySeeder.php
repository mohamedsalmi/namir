<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'  => 'الفقه',
            ],
            [
                'name'  => 'الحديث',
            ],
            [
                'name'  => 'التاريخ',
            ],
            [
                'name'  => 'العقيدة',
            ],
        ];

        DB::table('categories')->delete();

        foreach ($categories as $key => $category) {
            Category::create($category);
        }
    }
}
