<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 26,
                'name' => 'منشورات دار المازري',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 27,
                'name' => 'القرآن وعلومه',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 28,
                'name' => 'الحديث وعلومه',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 29,
                'name' => 'الفقه وأصوله',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 30,
                'name' => 'العقيدة والفكر',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 31,
                'name' => 'اللغة والأدب',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 32,
                'name' => 'التاريخ والتراجم',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 33,
                'name' => 'الثقافة العامة',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 34,
                'name' => 'أصول الفقه',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 35,
                'name' => 'المنطق',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 36,
                'name' => 'النحو والصرف',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 37,
                'name' => 'البلاغة',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 40,
                'name' => 'مصطلح الحديث',
                'level' => 2,
                'parent_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 41,
                'name' => 'الجرح والتعديل',
                'level' => 2,
                'parent_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 44,
                'name' => 'الأدب',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 45,
                'name' => 'الروايات',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 47,
                'name' => 'القواعد الفقهية',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 48,
                'name' => 'مواريث',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 49,
                'name' => 'الطفل والأسرة ',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 50,
                'name' => 'السيرة النبوية',
                'level' => 2,
                'parent_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 51,
                'name' => 'التاريخ العام',
                'level' => 2,
                'parent_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 52,
                'name' => 'تاريخ تونس',
                'level' => 2,
                'parent_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 53,
                'name' => 'التراجم',
                'level' => 2,
                'parent_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 54,
                'name' => ' دفاتر وأدوات',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 55,
                'name' => 'التفسير',
                'level' => 2,
                'parent_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 56,
                'name' => 'الشعر',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 57,
                'name' => 'الفقه المالكي ',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 58,
                'name' => 'العقيدة الإسلامية',
                'level' => 2,
                'parent_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 59,
                'name' => 'علم الكلام',
                'level' => 2,
                'parent_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 60,
                'name' => 'الفكر المعاصر',
                'level' => 2,
                'parent_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 61,
                'name' => 'الفلسفة',
                'level' => 2,
                'parent_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 63,
                'name' => 'اللغة والترجمة',
                'level' => 2,
                'parent_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 64,
                'name' => 'علوم القرآن',
                'level' => 2,
                'parent_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 65,
                'name' => 'مصاحف',
                'level' => 2,
                'parent_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 66,
                'name' => 'الأديان والفرق',
                'level' => 2,
                'parent_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 69,
                'name' => 'المالية الإسلامية',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 70,
                'name' => 'السياسة الشرعية',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 73,
                'name' => 'الفتاوى والأجوبة',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 74,
                'name' => 'فقه عام',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 77,
                'name' => '	المجاميع',
                'level' => 2,
                'parent_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 78,
                'name' => 'الفقه الشافعي',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 79,
                'name' => 'منوعات',
                'level' => 2,
                'parent_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 80,
                'name' => 'النوازل والمسائل الفقهية المعاصرة',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 81,
                'name' => 'السنة وشروحها',
                'level' => 2,
                'parent_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 82,
                'name' => 'التخريج',
                'level' => 2,
                'parent_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 83,
                'name' => 'التنمية الذاتية',
                'level' => 2,
                'parent_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 84,
                'name' => 'تجويد وقراءات',
                'level' => 2,
                'parent_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 85,
                'name' => 'مقاصد الشريعة ',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 86,
                'name' => 'الفقه الحنبلي',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 87,
                'name' => 'تراث الآل والأصحاب',
                'level' => 2,
                'parent_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 88,
                'name' => 'الفقه المقارن',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 90,
                'name' => 'الفقه الحنفي',
                'level' => 2,
                'parent_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 91,
                'name' => 'إعجاز القرآن',
                'level' => 2,
                'parent_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 92,
                'name' => 'التزكية والسلوك',
                'level' => 2,
                'parent_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 93,
                'name' => 'ألعاب للأطفال',
                'level' => 2,
                'parent_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 94,
                'name' => 'تربية الأطفال',
                'level' => 2,
                'parent_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 95,
                'name' => 'الأسرة والزواج',
                'level' => 2,
                'parent_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 96,
                'name' => 'كتب الأطفال',
                'level' => 2,
                'parent_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 97,
                'name' => 'أجهزة الإعلامية ',
                'level' => 1,
                'parent_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 98,
                'name' => 'حواسيب ',
                'level' => 2,
                'parent_id' => 97,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 99,
                'name' => 'آلات طابعة ',
                'level' => 2,
                'parent_id' => 97,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}