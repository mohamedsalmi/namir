<?php

return [
    'price_types' => [
        'Simple',
        "Dépend d'un attribut",
        "Dépend de deux attributs",
    ],

    'quantity_types' => [
        'Simple',
        "Dépend d'un attribut",
        "Dépend de deux attributs",
    ],

    'unities' => [
        'Pièce',
    ],

    'image' => [
        'default' => [
            'user' => '/assets/admin/images/avatars/avatar.jpg',
            'customer' => '/assets/admin/images/avatars/avatar.jpg',
            'passenger' => '/assets/admin/images/avatars/avatar.jpg',
            'provider' => '/assets/admin/images/avatars/avatar.jpg',
            'slider' => '/assets/shop/images/furniture/banner/1.jpg',
            'product' => '/assets/admin/images/products/product.png',
        ],
    ],

    'info' => [
        'name' => env('INFO_NAME', 'مكتبة نمير'),
        'ar_name' => env('INFO_ARNAME', 'مكتبة نمير'),
        'ar_meta_default' => env('INFO_ARMETADEFAULT', 'كتب مكتبة نمير'),
        'domaine' => env('INFO_DOMAINE', 'Librairie'),
        'address' => env('INFO_ADRESS', 'نهج 11 رقم 7 الوالفة, الحي الحسني, مجمع سعد الخير - الدار البيضاء'),
        'map_adress' => env('INFO_MAPADRESS', "https://maps.google.com/maps?q=مكتبة نمير الدار البيضاء&t=&z=16&ie=UTF8&iwloc=&output=embed"),
        'phone' => env('INFO_PHONE', '0655916124'),
        'mf' => env('INFO_MF', '119 66 67 E M N 000'),
        'rib' => env('INFO_RIB', ''),
        'image' => env('INFO_IMAGE', 'assets/admin/images/favicon.png'),
        'print_image' => env('PRINT_IMAGE', 'assets/admin/images/favicon.png'),
        'desc_ar' => env('INFO_DESCAR', 'مكتبة نمير'),
        'desc_en' => env('INFO_DESCEN', 'Namir Book Store'),
        'desc_fr' => env('INFO_DESCFR', 'Librairie Namir'),
        'whatsapp' => env('INFO_WHATSAPP', '0655916124'),
        'mail' => env('INFO_MAIL', 'librairienamir@gmail.com'),
        'fb' => env('INFO_FB', 'https://www.facebook.com/namirbookstore'),
    ],
    'ras' => 1.50,
];
