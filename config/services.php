<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'client_id' => '897936477581-caaj5efmcsvn4v75dpp1c5oj519fvif8.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://localhost:8000/callback/google',
    ],
    'facebook' => [
        'client_id' => '1345446886272846',
        'client_secret' => '46837222a15e04615a129442ac9ddcbb',
        'redirect' => 'http://localhost:8000/callback/facebook',
    ],
];
