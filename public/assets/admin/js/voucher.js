/***** focus  */
// $(document).ready(function () {
//     $("#product_list").select2('open');
//     document.querySelector('.select2-search__field').focus();
// });
/***** */
/* cart submit*/
$(document).on("click", "#save", function (e) {
    submit(e);
});
$(document).on("click", "#savewithprint", function (e) {
    submit(e, 'savewithprint');
});
$(document).on("click", "#savewithticket", function (e) {
    submit(e, 'savewithticket');
});
var errors = null;
function submit(e, options = '') {
    e.preventDefault();
    var Form = $('#create-voucher-form');
    var url = "/admin/voucher/vouchervalidation";
    if (options != '') {
        var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
        Form.append(input);
    }

    if (errors) {
        $.each(errors, function (key, value) {
            key = key.replace('provider_details.', '');
            var element = document.getElementById(key);
            if (element) {
                element.classList.remove('is-invalid');
            };
        });
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: Form.serialize(),
        async: false,
        success: function (response, textStatus, jqXHR) {
            Form.submit();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errors = jqXHR.responseJSON.errors;
            $.each(errors, function (key, value) {
                key = key.replace('provider_details.', '')
                $("#" + key).addClass('is-invalid');
                $("#" + key).next().html(value[0]);
                $("#" + key).next().removeClass('d-none');
            });
        }
    });
}
/*** end cart submit */

/**** change provider select */
$(document).ready(function () {
    "use strict";
    /*Confirm*/
    $('#fidel_list').change(function () {
        var name = $('#fidel_list').children("option:selected").attr('name-fidel');
        var tel = $('#fidel_list').children("option:selected").attr('tel-fidel');
        var mf = $('#fidel_list').children("option:selected").attr('MF-fidel');
        var adresse = $('#fidel_list').children("option:selected").attr('AD-fidel');
        var prix = $('#fidel_list').children("option:selected").attr('Prix-fidel');
        $('#name').val(name);
        $('#phone').val(tel);
        $('#mf').val(mf);
        $('#adresse').val(adresse);
        $('#Prix_T').val(prix);
        var provider_id = $('select[name="provider_id"]').val();
    });
    /**** end change provider select */
    /****  change product select */
    // $('#product_list').change(function () {
    //     var product_id = $(this).children("option:selected").attr('product-id');
    //     var tr = $('body').find(`[data-id='${product_id}']`);
    //     if ((tr.length > 0) && (product_id != undefined)) {
    //         grade = tr.attr('grade');
    //         var qt = parseFloat($('#quantity' + grade).val()) + 1;
    //         $('#quantity' + grade).val(qt)
    //         calculate_gtotal();

    //     } else {
    //         var check = $("#dynamic_field tr:last-child").attr('grade');
    //         var grade = parseInt(check) + 1;
    //         let remise = 0;
    //         let product = {
    //             id: product_id,
    //             name: $('#product_list').children("option:selected").attr('product-name'),
    //             price_buying: $('#product_list').children("option:selected").attr('price-buying'),
    //             price_selling1 : $('#product_list').children("option:selected").attr('price-selling1'),
    //             price_selling2 : $('#product_list').children("option:selected").attr('price-selling2'),
    //             price_selling3 : $('#product_list').children("option:selected").attr('price-selling3'),              
    //             unity: $('#product_list').children("option:selected").attr('product-unity'),
    //             tva: $('#product_list').children("option:selected").attr('product-tva'),
    //             currency: $('#product_list').children("option:selected").attr('product-currency'),
    //             currency_id: $('#product_list').children("option:selected").attr('product-currency-id'),
    //             remise: remise,
    //             quantity: 1,
    //         };      

    //             createRow(grade, product);

    //     }
    //     $(document).ready(function () {
    //         $("#product_list").val('');
    //         $("#product_list").select2('open');
    //         document.querySelector('.select2-search__field').focus();
    //     });
    //     // play sound
    //     $('#sucessAudio')[0].load();
    //     $('#sucessAudio')[0].play();
    //     calculate_gtotal();
    // });

});
/**** end change product select */

/**** delete row */
$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
    calculate_gtotal();
});
/**** end delete row */
/**** change quantity */
$(document).on('keyup change', '.price', function () {
    calculate_gtotal();
});
$(document).on('keyup change', '.quantity', function () {
    calculate_gtotal();
});
$(document).on('blur', '.quantity', function (e) {
    if ($(this).val() < 1) { $(this).val(1) }
    calculate_gtotal();
});
/**** end change quantity */
/*** change remise */
$(document).on('keyup change', '.remise', function () {
    calculate_gtotal();
});
$(document).on('blur', '.remise', function (e) {
    if (($(this).val() < 0) || ($(this).val() > 100) || ($(this).val() == '')) { $(this).val(0) }
    calculate_gtotal();
});
/*** end change remise */

/** total calculate */
function calculate_gtotal() {
    $('#dynamic_field').each(function () {
        var totalPoints = 0;
        var total_line = 0;
        $(this).find('.quantity').each(function () {
            var product_id = $(this).attr('data-product_id');
            var grade = $(this).attr('data-product_grade');
            var price = $('#price' + grade).val();
            var remise = $('#remise' + grade).val();
            var tva = $('#tva' + grade).val();
            total_line = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
            totalPoints += total_line;
            $('#L_total' + grade).attr('value', total_line.toFixed(3));
            //<==== a catch  in here !! read below
        });
        $('.total').attr('value', totalPoints.toFixed(3));
    });
}
/** end total calculate */

/** create row */
function createRow(grade, product) {
    let newRow = '<tr id="row' + grade + '" class="item_cart" grade="' + grade + '"  data-id="' + product.id + '">\n' +
        '    <td>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_id]" value="' + product.id + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + product.currency + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + product.currency_id + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_price_selling1]"value="' + product.price_selling1 + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_price_selling2]"value="' + product.price_selling2 + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_price_selling3]"value="' + product.price_selling3 + '"/>\n' + '        <input type="text" name="items[' + grade + '][product_name]" class="form-control form-control-sm name_list" readonly value="' + product.name + '"/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product.quantity + '" min="1" max="" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + product.unity + '" readonly/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" value="' + (product.remise ?? 0) + '" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" name="items[' + grade + '][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + product.tva + '" readonly>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" name="items[' + grade + '][product_price_buying]" style="width: 100px;" class="form-control form-control-sm price "   id="price' + grade + '"  data-product_id="' + product.id + '" data-product_grade="' + grade + '" value="' + product.price_buying + '" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' " id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + product.price_buying + '" readonly >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '   <div class="form-check form-switch"><input class="form-check-input" name="items[' + grade + '][update_price]"" type="checkbox" ></div>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button>\n' +
        '    </td>\n' +

        '</tr>';
    $('#dynamic_field').append(newRow);
    calculate_gtotal();
}
/**end create row */

$(document).ready(function () {
    var check = $('#Deadline_check').is(":checked");
    console.log(check);
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();
        divideCommitments(2);
    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }
});
$(document).on('change', '#Deadline_check', function () {
    var check = $(this).is(":checked");
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();
        divideCommitments(2);
    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }

});

/******* */
$("#filter").on('click', function (e) {

    e.preventDefault();
    var ecrivain = $('#ecrivain').val();
    var maison = $('#maison').val();
    var enquete = $('#enquete').val();
    var name = $('#product_name').val();
    var all_status = $('#all_status').is(":checked");

    var sku = $('#sku').val();
    if (ecrivain || maison || enquete || name || sku) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/admin/cart/filter',
            method: "POST",
            data: {
                name: name,
                ecrivain: ecrivain,
                maison: maison,
                all_status: all_status,
                sku: sku,
                enquete: enquete
            },
            success: function (data) {
                $('#list_product').html('');
                if (data.length == 1) {
                    const element = data[0];
                    var product_id = element.id;
                    var tr = $('body').find(`[data-id='${product_id}']`);
                    if ((tr.length > 0) && (product_id != undefined)) {
                        grade = tr.attr('grade');
                        var qt = parseFloat($('#quantity' + grade).val()) + 1;
                        $('#quantity' + grade).val(qt)
                        calculate_gtotal();

                    } else {
                        var check = $("#dynamic_field tr:last-child").attr('grade');
                        var grade = parseInt(check) + 1;
                        var client_id = $('select[name="client_id"]').val();
                        var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
                        var price_type = $('#Prix_T').val();
                        let product_price_selling = 0;
                        let remise = 0;
                        let product = {
                            id: product_id,
                            name: element.name,
                            price_buying: element.buying,
                            price_selling1: element.selling1,
                            price_selling2: element.selling2,
                            price_selling3: element.selling3,
                            unity: "Pièce",
                            tva: element.tva,
                            currency: "TND",
                            currency_id: "1",
                            maxdiscount: element.maxdiscount,
                            remise: remise,
                            quantity: 1,
                        };
                        createRow(grade, product);
                        // }
                    }
                    $('#sucessAudio')[0].load();
                    $('#sucessAudio')[0].play();
                    calculate_gtotal();
                } else if (data.length > 1) {
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index];
                        let option = `<option 
                        product-id="${element.id}"
                        product-name="${element.name}"
                        price-buying="${element.buying}"
                        price-selling1="${element.selling1}"
                        price-selling2="${element.selling2}"
                        price-selling3="${element.selling3}"
                        product-unity="Pièce"
                        product-tva="${element.tva}"
                        product-sku="${element.sku}"
                        product-currency="TND"
                        product-currency-id="1"
                        product-maxdiscount="${element.maxdiscount}">
                        ${element.name} - ${element.reference} - ${element.sku}</option>`;
                        $('#list_product').append(option);
                    }
                } else {
                    $('#list_product').append('<option class="text-danger" disabled>aucun article</option>');
                }
                $('#my_multi_select2').val('');
                $('#my_multi_select2').multiSelect('refresh');
            }
        });
    } else {
        $('#list_product').html('');
        $('#list_product').append('<option class="text-danger" disabled>aucune données dans les filtres</option>');
        $('#my_multi_select2').val('');
        $('#my_multi_select2').multiSelect('refresh');
    }



});
//*********** add to cart list filter */
$(document).on('click', '#add_to_cart', function () {
    // let array=document.querySelectorAll('.select2-selection__rendered')[2].getElementsByTagName("li");
    // console.log(array);
    let array = $('#list_product').find(':selected');
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        var product_id = element.getAttribute('product-id');
        var tr = $('body').find(`[data-id='${product_id}']`);
        if ((tr.length > 0) && (product_id != undefined)) {
            grade = tr.attr('grade');
            var qt = parseFloat($('#quantity' + grade).val()) + 1;
            $('#quantity' + grade).val(qt)
            calculate_gtotal();

        } else {
            var check = $("#dynamic_field tr:last-child").attr('grade');
            var grade = parseInt(check) + 1;
            var client_id = $('select[name="client_id"]').val();
            var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
            var price_type = $('#Prix_T').val();
            let product_price_selling = 0;
            let remise = 0;
            let product = {
                id: product_id,
                name: element.getAttribute('product-name'),
                price_buying: element.getAttribute('price-buying'),
                price_selling1: element.getAttribute('price-selling1'),
                price_selling2: element.getAttribute('price-selling2'),
                price_selling3: element.getAttribute('price-selling3'),
                unity: element.getAttribute('product-unity'),
                tva: element.getAttribute('product-tva'),
                currency: element.getAttribute('product-currency'),
                currency_id: element.getAttribute('product-currency-id'),
                maxdiscount: element.getAttribute('product-maxdiscount'),
                remise: remise,
                quantity: 1,
            };   
            createRow(grade, product);
            // }
        }
        $('#my_multi_select2').val('');
        $('#my_multi_select2').multiSelect('refresh');

        $('#sucessAudio')[0].load();
        $('#sucessAudio')[0].play();
        calculate_gtotal();
    }
});

 /**********  */
    //*********** add to cart list filter */
    $(document).on('click', '.addToCart', function () {
        // let array=document.querySelectorAll('.select2-selection__rendered')[2].getElementsByTagName("li");
        // console.log(array);

            const element = $(this);
            var product_id = element.attr('product-id');
            var tr = $('body').find(`[data-id='${product_id}']`);
            if ((tr.length > 0) && (product_id != undefined)) {
                grade = tr.attr('grade');
                var qt = parseFloat($('#quantity' + grade).val()) + 1;
                $('#quantity' + grade).val(qt)
                calculate_gtotal();

            } else {
                var check = $("#dynamic_field tr:last-child").attr('grade');
                var grade = parseInt(check) + 1;
                var client_id = $('select[name="client_id"]').val();
                var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
                var price_type = $('#Prix_T').val();
                let product_price_selling = 0;
                let remise = 0;
                let product = {
                    id: product_id,
                    name: element.attr('product-name'),
                    price_buying: element.attr('price-buying'),
                    price_selling: product_price_selling,
                    unity: element.attr('product-unity'),
                    tva: element.attr('product-tva'),
                    currency: element.attr('product-currency'),
                    currency_id: element.attr('product-currency-id'),
                    maxdiscount: element.attr('product-maxdiscount'),
                    house: element.attr('product-house'),
                    remise: remise,
                    quantity: 1,
                };
                if (client_id != 0) {
                    if (discount > product.maxdiscount) {
                        product.remise = product.maxdiscount;
                    } else {
                        product.remise = discount;
                    }
                }
                // else {
                switch (price_type) {
                    case 'detail':
                        product_price_selling = element.attr('price-selling1');
                        break;
                    case 'semigros':
                        product_price_selling = element.attr('price-selling2');
                        break;
                    case 'gros':
                        product_price_selling = element.attr('price-selling3');
                        break;
                }
                product.price_selling = product_price_selling;
                createRow(grade, product);
                // }
            }
            $('#my_multi_select2').val('');
            $('#my_multi_select2').multiSelect('refresh');
            // $('#list_product').html('');

            $('#sucessAudio')[0].load();
            $('#sucessAudio')[0].play();
            calculate_gtotal();

    });
    
/**********  */

// $(document).keydown(function (e) {
//     switch (e.which) {
//         case 13: //F2 : Enregistrer
//             e.preventDefault();
//             break;

//     }
// });
// var input = document.getElementById("myInput");

// // Execute a function when the user presses a key on the keyboard
// input.addEventListener("keypress", function(event) {
//   // If the user presses the "Enter" key on the keyboard
//   if (event.key === "Enter") {
//     // Cancel the default action, if needed
//     event.preventDefault();
//     // Trigger the button element with a click
//     document.getElementById("myBtn").click();
//   }
// });
$(".filter").keypress(function (event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#filter").click();
    }
});