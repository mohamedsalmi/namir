$(function () {
    "use strict";

    $(document).ready(function () {
        $('#example').DataTable({
            dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
            order: [
                [0, 'desc']
            ],
            "pageLength": 50,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.12.1/i18n/fr-FR.json"
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            // responsive: true,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
             
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
            ]


        });
    });
    $(document).ready(function () {
        $('#CommitmentTable').DataTable({
            dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
            order: [[4, 'asc']],
            pageLength: 100,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.12.1/i18n/fr-FR.json"
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            // responsive: true,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
             
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    }
                },
            ]


        });
    });
    $(document).ready(function () {
        $('.caisse_table').DataTable({
   
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
            order: [
                [0, 'desc']
            ],
            "pageLength": 10,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.12.1/i18n/fr-FR.json"
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            // responsive: true,

        });
    });


    $(document).ready(function () {
        var table = $('#example2').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'print']
        });

        table.buttons().container()
            .appendTo('#example2_wrapper .col-md-6:eq(0)');
    });


});
