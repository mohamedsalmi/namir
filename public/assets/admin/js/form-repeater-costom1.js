(function ($) {

    $.fn.repeatable1 = function (userSettings) {

        /**
         * Default settings
         * @type {Object}
         */
        var defaults = {
            addTrigger: ".add",
            deleteTrigger: ".delete",
            max: null,
            min: 0,
            template: null,
            itemContainer: ".field-group",
            maxMessage: 'You reached the maximum number of fields.',
            total: undefined,
            beforeAdd: function () {
            },
            afterAdd: function (item) {
            },
            beforeDelete: function (item) {
            },
            afterDelete: function () {
            }
        };

        /**
         * Iterator used to make each added
         * repeatable element unique
         * @type {Number}
         */
        var i = 1;

        /**
         * DOM element into which repeatable
         * items will be added
         * @type {jQuery object}
         */
        var target = $(this);

        /**
         * Blend passed user settings with defauly settings
         * @type {array}
         */
        var settings = $.extend({}, defaults, userSettings);

        /**
         * Total templated items found on the page
         * at load. These may be created by server-side
         * scripts.
         * @return null
         */
        var total = function () {
            if (settings.total) {
                return settings.total
            }
            return $(target).find(settings.itemContainer).length;
        }();


        /**
         * Add an element to the target
         * and call the callback function
         * @param  object e Event
         * @return null
         */
        var addOne = function (e) {
            e.preventDefault();
            settings.beforeAdd.call(this);
            var item = createOne();
            settings.afterAdd.call(this, item);
        };

        /**
         * Delete the parent element
         * and call the callback function
         * @param  object e Event
         * @return null
         */
        var deleteOne = function (e) {
            e.preventDefault();
            if (total === settings.min) return;
            var item = $(this).parents(settings.itemContainer).first();
            settings.beforeDelete.call(this, item);
            item.remove();
            total--;
            maintainAddBtn();
            resetAttributeNames1($('.repeat_items'));
            settings.afterDelete.call(this);
        };

        /**
         * Add an element to the target
         * @return null
         */
        var createOne = function () {
            var item = getUniqueTemplate();
            item.appendTo(target);
            total++;
            maintainAddBtn();
            return item;
        };

        /**
         * Alter the given template to make
         * each form field name unique
         * @return {jQuery object}
         */
        var getUniqueTemplate = function () {
            var template = $(settings.template).html();
            template = template.replace(/{\?}/g, total + 1); 	// {?} => iterated placeholder
            template = template.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
            return $(template);
        };

        /**
         * Determines if the add trigger
         * needs to be disabled
         * @return null
         */
        var maintainAddBtn = function () {

            if (!settings.max) {
                return;
            }

            if (total === settings.max) {
                Notification.Report.Warning('', settings.maxMessage)
                $(settings.addTrigger).attr("disabled", "disabled");
            } else if (total < settings.max) {
                $(settings.addTrigger).removeAttr("disabled");
            }
        };

        /**
         * Setup the repeater
         * @return null
         */
        (function () {
            $(settings.addTrigger).on("click", addOne);
            $("form").on("click", settings.deleteTrigger, deleteOne);

            if (!total) {
                var toCreate = settings.min - total;
                for (var j = 0; j < toCreate; j++) {
                    createOne();
                }
            }

        })();
    };

    $.fn.serializeControls = function () {
        var data = {};

        function buildInputObject(arr, val) {
            if (arr.length < 1)
                return val;
            var objkey = arr[0];
            if (objkey.slice(-1) == "]") {
                objkey = objkey.slice(0, -1);
            }
            var result = {};
            if (arr.length == 1) {
                result[objkey] = val;
            } else {
                arr.shift();
                var nestedVal = buildInputObject(arr, val);
                result[objkey] = nestedVal;
            }
            return result;
        }

        $.each(this.serializeArray(), function () {
            var val = this.value;
            var c = this.name.split("[");
            var a = buildInputObject(c, val);
            $.extend(true, data, a);
        });

        return data;
    }


})(jQuery);
///////////////////
var attrs = ['value', 'name','id'];
function resetAttributeNames1(section) {
    // var acc = section.find('h6.text-info'), idx = 1;
    // acc.each(function () {
    //         var $this = $(this);
    //         var attr_val = $this.text();
    //         if (attr_val) {
    //             new_val=attr_val.replace(/\d+/, (idx));
    //             $this.text(new_val)
    //         }
    //     idx++;
    // })
    // var acc = section.find('div .card-body '), idx = 1;
    // acc.each(function () {
    //     var tags = $(this).find('select, label,input');
    //     tags.each(function () {
    //         var $this = $(this);
    //         $.each(attrs, function (i, attr) {
    //             var attr_val = $this.attr(attr);
    //             if (attr_val) {
    //                 $this.attr(attr, attr_val.replace(/\d+/, (idx)))
    //             }
    //         })
    //     })
    //     idx++;
    // })
}
        ///////////////////
