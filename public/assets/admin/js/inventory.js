/***** focus  */
// $(document).ready(function () {
//     $("#product_list").select2('open');
//     document.querySelector('.select2-search__field').focus();
// });
/***** */
/* inventory submit*/
$(document).on("click", "#save", function (e) {
    submit(e);
});
$(document).on("click", "#savewithprint", function (e) {
    submit(e, 'savewithprint');
});
$(document).on("click", "#savewithticket", function (e) {
    submit(e, 'savewithticket');
});
var errors = null;
function submit(e, options = '') {
    e.preventDefault();
    var Form = $('#create-inventory-form');
    var url = "/admin/inventory/inventoryvalidation";
    if (options != '') {
        var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
        Form.append(input);
    }
    if (errors) {
        $.each(errors, function (key, value) {
            var element = document.getElementById(key);
            if (element) {
                element.classList.remove('is-invalid')
            };
        });
    }
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: Form.serialize(),
        async: false,
        success: function (response, textStatus, jqXHR) {
            Form.submit();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errors = jqXHR.responseJSON.errors;
            $.each(errors, function (key, value) {
                var element = document.getElementById(key);
                if (element) {
                    element.classList.add('is-invalid')
                };
            });
        }
    });

}
/*** end inventory submit */

/****  change product select */
// $('#product_list').change(function () {
//     var product_id = $(this).children("option:selected").attr('product-id');
//     var tr = $('body').find(`[data-id='${product_id}']`);
//     if ((tr.length > 0) && (product_id != undefined)) {
//         grade = tr.attr('grade');
//         var qt = parseFloat($('#quantity' + grade).val()) + 1;
//         $('#quantity' + grade).val(qt)
//     } else {
//         var check = $("#dynamic_field tr:last-child").attr('grade');
//         var grade = parseInt(check) + 1;
//         let product = {
//             id: product_id,
//             name: $('#product_list').children("option:selected").attr('product-name'),
//             unity: $('#product_list').children("option:selected").attr('product-unity'),
//             old_quantity: $('#product_list').children("option:selected").attr('old_quantity'),
//             new_quantity: 0,
//         };
//             createRow(grade, product);
//     }
//     $(document).ready(function () {
//         $("#product_list").val('');
//         $("#product_list").select2('open');
//         document.querySelector('.select2-search__field').focus();
//     });
//     // play sound
//     $('#sucessAudio')[0].load();
//     $('#sucessAudio')[0].play();
// });

/**** end change product select */

/**** delete row */
$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
});
/**** end delete row */
/**** change quantity */
$(document).on('keyup change', '.new_quantity', function () {
    let grade = $(this).attr('data-product_grade');
    let new_quantity = $(this).val()
    let old_quantity = $('#old_quantity' + grade).val()
    let marge = new_quantity - old_quantity
    console.log(grade, new_quantity, old_quantity, marge)
    if (marge < 0) {
        $('#marge_quantity' + grade).removeClass('text-success')
        $('#marge_quantity' + grade).addClass('text-danger');
    } else {
        $('#marge_quantity' + grade).removeClass('text-danger')
        $('#marge_quantity' + grade).addClass('text-success');
    }
    $('#marge_quantity' + grade).val(marge)

});
$(document).on('blur', '.quantity', function (e) {
    if ($(this).val() < 0) { $(this).val(0) }
});
/**** end change quantity */


/** create row */
function createRow(grade, product) {
    if (product.old_quantity < 0) {
        classe = 'text-success'
    } else {
        classe = 'text-danger'
    }
    let newRow = '<tr id="row' + grade + '" class="item_cart" grade="' + grade + '"  data-id="' + product.id + '">\n' +
        '    <td>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_id]" value="' + product.id + '"/>\n' +
        '        <input type="text" name="items[' + grade + '][product_name]" class="form-control form-control-sm name_list" readonly value="' + product.name + '"/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" style="width:80px;" name="items[' + grade + '][product_new_quantity]" class="form-control form-control-sm new_quantity" oninput="this.value = Math.floor(this.value);" step="1" id="new_quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product.new_quantity + '" min="0" max="" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" style="width:80px;" name="items[' + grade + '][product_old_quantity]" class="form-control form-control-sm " oninput="this.value = Math.floor(this.value);" step="1" id="old_quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product.old_quantity + '"  readonly>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" style="width:80px;" name="items[' + grade + '][product_marge_quantity]" class="form-control form-control-sm marge_quantity ' + classe + '" oninput="this.value = Math.floor(this.value);" step="1" id="marge_quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + (product.new_quantity - product.old_quantity) + '"  readonly>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + product.unity + '" readonly/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button>\n' +
        '    </td>\n' +
        '</tr>';
    $('#dynamic_field').append(newRow);
}
/**end create row */
/******* */
$("#filter").on('click', function (e) {

    e.preventDefault();
    var ecrivain = $('#ecrivain').val();
    var house = $('#house').val();
    var enquete = $('#enquete').val();
    var name = $('#product_name').val();
    var sku = $('#sku').val();
    var all_status = $('#all_status').is(":checked");
    if (ecrivain || house || enquete || name || sku) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/admin/cart/filter',
            method: "POST",
            data: {
                name: name,
                ecrivain: ecrivain,
                house: house,
                sku: sku,
                all_status: all_status,
                enquete: enquete
            },
            success: function (data) {
                $('#list_product').html('');
                if (data.length == 1) {
                    const element = data[0];
                    var product_id = element.id;
                    var tr = $('body').find(`[data-id='${product_id}']`);
                    if ((tr.length > 0) && (product_id != undefined)) {
                        grade = tr.attr('grade');
                        var qt = parseFloat($('#quantity' + grade).val()) + 1;
                        $('#quantity' + grade).val(qt)

                    } else {
                        var check = $("#dynamic_field tr:last-child").attr('grade');
                        var grade = parseInt(check) + 1;
                        let product = {
                            id: product_id,
                            name: element.name,
                            unity: "Pièce",
                            old_quantity: element.old_quantity,
                            new_quantity: element.old_quantity,
                        };
                        createRow(grade, product);
                        // }
                    }
                    $('#sucessAudio')[0].load();
                    $('#sucessAudio')[0].play();
                } else if (data.length > 1) {
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index];
                        let option = `<option 
                        product-id="${element.id}"
                        product-name="${element.name}"                     
                        product-unity="Pièce"
                        old_quantity="${element.old_quantity}"
                        product-sku="${element.sku}"                  
                        >
                        ${element.name} - ${element.reference} - ${element.sku}</option>`;
                        $('#list_product').append(option);
                    }
                } else {
                    $('#list_product').append('<option class="text-danger" disabled>aucun article</option>');
                }
                $('#sku').val('');
                $('#my_multi_select2').val('');
                $('#my_multi_select2').multiSelect('refresh');

            }
        });
    } else {
        $('#sku').val('');
        $('#list_product').html('');
        $('#list_product').append('<option class="text-danger" disabled>aucune données dans les filtres</option>');
        $('#my_multi_select2').val('');
        $('#my_multi_select2').multiSelect('refresh');
    }



});
//*********** add to cart list filter */

// $(document).keydown(function (e) {
//     switch (e.which) {
//         case 13: //F2 : Enregistrer
//             e.preventDefault();
//             break;

//     }
// });
// var input = document.getElementById("myInput");

// // Execute a function when the user presses a key on the keyboard
// input.addEventListener("keypress", function(event) {
//   // If the user presses the "Enter" key on the keyboard
//   if (event.key === "Enter") {
//     // Cancel the default action, if needed
//     event.preventDefault();
//     // Trigger the button element with a click
//     document.getElementById("myBtn").click();
//   }
// });
$(".filter").keypress(function (event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#filter").click();
    }
});

//*********** add to cart list filter */
$(document).on('click', '.addToCart', function () {
    // let array=document.querySelectorAll('.select2-selection__rendered')[2].getElementsByTagName("li");
    // console.log(array);

    const element = $(this);
    var product_id = element.attr('product-id');
    var tr = $('body').find(`[data-id='${product_id}']`);
    if ((tr.length > 0) && (product_id != undefined)) {
        grade = tr.attr('grade');
        var qt = parseFloat($('#quantity' + grade).val()) + 1;
        $('#quantity' + grade).val(qt)
        // calculate_gtotal();

    } else {
        var check = $("#dynamic_field tr:last-child").attr('grade');
        var grade = parseInt(check) + 1;
        let product = {
            id: product_id,
            name: element.attr('product-name'),
            unity: "Pièce",
            old_quantity: element.attr('product-old-quantity'),
            new_quantity: element.attr('product-old-quantity'),
        };
        createRow(grade, product);
    }
    $('#my_multi_select2').val('');
    $('#my_multi_select2').multiSelect('refresh');
    // $('#list_product').html('');

    $('#sucessAudio')[0].load();
    $('#sucessAudio')[0].play();
    // calculate_gtotal();

});
/**********  */