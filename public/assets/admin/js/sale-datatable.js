function dateCheck(from, to, check) {
    var fDate, lDate, cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);

    if ((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}

function dateFormat(date) {
    var todayDate = new Date(date).toISOString().slice(0, 10);
    return todayDate;
}

var client_column = $('#client-column').val();
var date_column = $('#date-column').val();
var total_column = $('#total').data('column');
console.log(total_column);

console.log(client_column, date_column);

$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
    var clientfilter = $('#clientfilter').val();
    var client = data[client_column] || '';

    if (
        clientfilter == '' || client.trim() == clientfilter.trim()
    ) {
        return true;
    }
    return false;
});

$(document).ready(function () {
    var table = $('#example').DataTable({
        dom: "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
        order: [
            [1, 'desc']
        ],
        "pageLength": 50,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.12.1/i18n/fr-FR.json"
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }],
        // responsive: true,
        buttons: [
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':not(:last-child)'
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':not(:last-child)'
                }
            },
         
            {
                extend: 'pdf',
                exportOptions: {
                    columns: ':not(:last-child)'
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':not(:last-child)'
                }
            },
        ]


    });

    // Event listener to the two range filtering inputs to redraw on input
    $('#clientfilter').change(function () {
        table.draw();
        var sum = table.columns(total_column, { search: 'applied' }).data().sum();
        $('#total').text(sum.toFixed(3));
    });

    table.on('search.dt', function () {
        var sum = table.columns(total_column, { search: 'applied' }).data().sum();
        $('#total').text(sum.toFixed(3));
    });

    $('#from, #to').change(function () {
        table.draw();
        var sum = table.columns(total_column, { search: 'applied' }).data().sum();
        $('#total').text(sum.toFixed(3));
    });
});

jQuery.fn.dataTable.Api.register('sum()', function () {
    return this.flatten().reduce(function (a, b) {
        if (typeof a === 'string') {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if (typeof b === 'string') {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }

        return a + b;
    }, 0);
});

$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
    var from = $('#from').val();
    var to = $('#to').val();
    var date = data[date_column] || '';
    if (
        from == '' || to == '' || dateCheck(dateFormat(from), dateFormat(to), dateFormat(date))
    ) {
        return true;
    }
    return false;
});