var errors = null;
$(document).on("click","#save",function (e) {
    e.preventDefault();
    var sum = 0;
    if($('#Deadline_check').is(":checked")){
        $('.Deadline').each(function() {
            sum += Number($(this).val());
        });
        // console.log(sum);
    total_payment_amount=sum;
    toltal_cart=$('.total').val();
    }else{
        $('.amount').each(function() {
            sum += Number($(this).val());
        });
        total_payment_amount=sum;
        toltal_cart=$('.total').val();
        console.log(toltal_cart);
        console.log(sum);
        console.log(total_payment_amount == toltal_cart);
    }

    // if(total_payment_amount != toltal_cart)
    // {
    //     Swal.fire({
    //         title: "Ops commande non payée!",
    //         text: "Vous devez inserer les donnés de paiement corectement",
    //         type: "success",
    //         confirmButtonClass: "btn btn-confirm mt-2"
    //     });
    // }else{
        var Form = $('#create-creditnote-form');
        var url = "/admin/creditnote/validation";

        if (errors) {
            $.each(errors, function (key, value) {
                key = key.replace('client_details.', '');
                var element = document.getElementById(key);
                if(element) {
                    element.classList.remove('is-invalid')
                }; 
            });
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "POST",
            data: Form.serialize(),
            async: false,
            success: function(response, textStatus, jqXHR) {
                Form.submit();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                errors = jqXHR.responseJSON.errors;
                $.each(errors, function (key, value) {
                    key=key.replace('client_details.','')
                    console.log(key)
                    $("#" + key).addClass('is-invalid');
                    $("#" + key).next().html(value[0]);
                    $("#" + key).next().removeClass('d-none');
                });
            }
        });
    // }

});

function calculate_gtotal() {
    $('#dynamic_field').each(function () {
        var totalPoints = 0;
        var total_line = 0;
        $(this).find('.quantity').each(function () {
            var product_id = $(this).attr('data-product_id');
            var grade = $(this).attr('data-product_grade');
            var price = $('#price' + grade).val();
            var remise = $('#remise' + grade).val();
            var tva = $('#tva' + grade).val();
            total_line = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
            totalPoints += total_line;
            $('#L_total' + grade).attr('value', total_line.toFixed(3));
            //<==== a catch  in here !! read below
        });
        $('.total').attr('value', totalPoints.toFixed(3));


    });
}
$(document).ready(function () {
    "use strict";
    /*Confirm*/
    /*Search Products*/
    $("#name_search").on('submit', function (e) {
        e.preventDefault();
        var query = $('#searchprod').val();
        var price_type = $('#Prix_T').val();
        var url = "/admin/cart/" + query + "/" + price_type + "/serach_product";
        if (query != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data: {
                    query: query,
                    price_type: price_type
                },
                success: function (data) {
                    if (data.length > 0) {
                        // console.log(data);
                        $('#listproduct').html(data);
                        $("#name_search")[0].reset();
                    } else {
                        $('#listproduct').html('<span class="text-danger ">Aucun résultat!</span>');
                        $("#name_search")[0].reset();
                    }
                }
            });
        }
    });
    /*Search Products */
    /**/
    $("#barcode_search").on('submit', function (e) {
        e.preventDefault();
        var barcode = $('#barcode').val();
        var price_type = $('#Prix_T').val();
        var check = $("#dynamic_field tr:last-child").attr('grade');
        var grade = parseInt(check) + 1;
        var url = "/admin/cart/" + barcode + "/" + price_type + "/serach_product_c_bar";
        if (barcode.length > 6) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                dataType: "json",
                data: {
                    barcode: barcode,
                    price_type: price_type,
                },
                success: function (data) {
                    var att1='';
                    var att2='';
                    var att_quantity1='';
                    var att_quantity2='';
                    if (data['2'] == null) {
                        att1 = '<span class="badge rounded-pill alert-warning">aucun</span><input type="hidden" id="attr1" name="items[' + grade + '][attr1]" value="0">';
                    } else {
                        att1 = ' <input  class="form-control form-control-sm attr1" data-product_grade="' + grade + '" data-product_id="' + data['0'].id + '" value="'+data['2']+'" readonly><input type="hidden" id="attr1" name="items[' + grade + '][attr1]" value="'+data['4']+'">';
                    }
                    if (data['3'] == null) {
                        att2 = '<span class="badge rounded-pill alert-warning "  >aucun</span><input type="hidden" id="attr2" name="items[' + grade + '][attr2]" value="0">';
                    } else {
                        att2 = ' <input  class="form-control form-control-sm attr2" data-product_grade="' + grade + '" data-product_id="' + data['0'].id + '" value="'+data['3']+'" readonly><input type="hidden" id="attr2" name="items[' + grade + '][attr2]" value="'+data['5']+'">';
                    }
                    if (data['2'] == null) {
                        att_quantity1 = '<span class="badge rounded-pill alert-warning">aucun</span><input type="hidden" name="items[' + grade + '][attr_quantity1]" value="0">';
                    } else {
                        att_quantity1 = ' <input class="form-control form-control-sm attr_quantity1" data-product_grade="' + grade + '" data-product_id="' + data['0'].id + '" value="'+data['2']+'" readonly><input type="hidden" name="items[' + grade + '][attr_quantity1]" value="'+data['4']+'">';
                    }
                    if (data['3'] == null) {
                        att_quantity2 = '<span class="badge rounded-pill alert-warning "  >aucun</span><input type="hidden" name="items[' + grade + '][attr_quantity2]" value="0">';
                    } else {
                        att_quantity2 = ' <input class="form-control form-control-sm attr_quantity2" data-product_grade="' + grade + '" data-product_id="' + data['0'].id + '"  value="'+data['3']+'" readonly><input type="hidden" name="items[' + grade + '][attr_quantity2]" value="'+data['5']+'">';
                                          }
                  $('#dynamic_field').append('<tr id="row' + grade + '" class="item_cart" grade="' + grade + '"><td><input type="hidden" name="items[' + grade + '][product_id]" value="' + data['0'].id + '"/><input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + data['7'] + '"/><input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + data['0'].currency_id + '"/><input type="hidden" name="items[' + grade + '][product_price_selling]"value="' + data['6'] + '"/><input type="text" name="items[' + grade + '][product_name]" placeholder="Nom" class="form-control form-control-sm name_list" readonly value="' + data['0'].name + '"/></td><td>' + att1 + att2 + '</td><td>' + att_quantity1 + att_quantity2 + '</td><td><input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="1" min="1" max="" ></td><td><input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + data['0'].unity + '" readonly/></td><td> <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" value="0" ></td><td> <input type="text" name="items[' + grade + '][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + data['0'].tva + '" readonly></td><td> <input type="text" name="items[' + grade + '][product_price_buying]" style="width: 100px;" class="form-control form-control-sm price' + grade + ' "   id="price' + grade + '"  data-product_id="' + grade + '" value="' + data['1'] + '" readonly></td><td><input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' "  name="items[' + grade + '][L_total]"  id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + data['1'] + '" readonly ></td><td><button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button></td></tr>');
                  calculate_gtotal();

                },
                error: function (data) {
                    $('#listproduct').html('<span class="text-danger ">Aucun résultat!</span>');
                    $("#barcode_search")[0].reset();
                },
            });
        }
    });
    /**/
    $('#fidel_list').change(function () {
        var name = $('#fidel_list').children("option:selected").attr('name-fidel');
        var tel = $('#fidel_list').children("option:selected").attr('tel-fidel');
        var mf = $('#fidel_list').children("option:selected").attr('MF-fidel');
        var adresse = $('#fidel_list').children("option:selected").attr('AD-fidel');
        var prix = $('#fidel_list').children("option:selected").attr('Prix-fidel');
        $('#name').val(name);
        $('#phone').val(tel);
        $('#mf').val(mf);
        $('#adresse').val(adresse);
        $('#Prix_T').val(prix);

        $('#dynamic_field').html('<tr hidden="" grade="0"></tr>');
        calculate_gtotal();
    });
    /* ajax facturation */
    $('#searchprod').focus();
    /* ajax facturation */
});
/**  liste product add/change/remove */
var table = $('#table').val();
var size = 'Enfant';
/**  start add_to_cart */
$(document).on('click', '.add_to_cart', function () {
    var product_id = $(this).attr("id");
    var product_name = $('#name' + product_id).val();
    var product_price_buying = $('#product_price_buying' + product_id).val();
    var product_price_selling = $('#product_price_selling' + product_id).val();
    var product_unity = $('#product_unity' + product_id).val();
    var product_tva = $('#product_tva' + product_id).val();
    var product_currency = $('#currency' + product_id).val();
    var product_currency_id = $('#currency_id' + product_id).val();
    var attribute1 = $('#attA' + product_id).val();
    var attribute1_values = $('#attA_values' + product_id).val();
    var attribute1_ids = $('#attA_ids' + product_id).val();
    var attribute2 = $('#attB' + product_id).val();
    var attribute2_values = $('#attB_values' + product_id).val();
    var attribute2_ids = $('#attB_ids' + product_id).val();
    var attribute3 = $('#attC' + product_id).val();
    var attribute3_values = $('#attC_values' + product_id).val();
    var attribute3_ids = $('#attC_ids' + product_id).val();
    var attribute4 = $('#attD' + product_id).val();
    var attribute4_values = $('#attD_values' + product_id).val();
    var attribute4_ids = $('#attD_ids' + product_id).val();
    var product_remise = 0;
    var product_quantity = 1;
    var check = $("#dynamic_field tr:last-child").attr('grade');
    var grade = parseInt(check) + 1;
    if (attribute1 == '0') {
        att1 = '<span class="badge rounded-pill alert-warning">aucun</span><input type="hidden" id="attr1" name="items[' + grade + '][attr1]" value="0">';
    } else {
        att1 = ' <select style="width: auto;" class="form-select attr1" data-product_grade="' + grade + '" data-product_id="' + product_id + '"  id="attr1" name="items[' + grade + '][attr1]">';
        index = 0;
        JSON.parse(attribute1_values).forEach(element => {
            att_id = JSON.parse(attribute1_ids)[index];
            att1 += '<option class="form-control form-control-sm" value="' + att_id + '" >' + element + '</option>';
            index++;
        });
        att1 += '</select>';
    }
    if (attribute2 == '0') {
        att2 = '<span class="badge rounded-pill alert-warning "  >aucun</span><input type="hidden" id="attr2" name="items[' + grade + '][attr2]" value="0">';
    } else {
        index = 0;
        att2 = ' <select style="width: auto;" class="form-select attr2" data-product_grade="' + grade + '" data-product_id="' + product_id + '" id="attr2" name="items[' + grade + '][attr2]">';
        JSON.parse(attribute2_values).forEach(element => {
            att_id = JSON.parse(attribute2_ids)[index];
            att2 += '<option class="form-control form-control-sm" value="' + att_id + '" >' + element + '</option>';
            index++;
        });
        att2 += '</select>';
    }
    if (attribute3 == '0') {
        att_quantity1 = '<span class="badge rounded-pill alert-warning">aucun</span><input type="hidden" name="items[' + grade + '][attr_quantity1]" value="0">';
    } else {
        att_quantity1 = ' <select style="width: auto;" class="form-select attr_quantity1" data-product_grade="' + grade + '" data-product_id="' + product_id + '"  id="attr_quantity1" name="items[' + grade + '][attr_quantity1]">';
        index = 0;
        JSON.parse(attribute3_values).forEach(element => {
            att_id = JSON.parse(attribute3_ids)[index];
            att_quantity1 += '<option class="form-control form-control-sm" value="' + att_id + '" >' + element + '</option>';
            index++;
        });
        att_quantity1 += '</select>';
    }
    if (attribute4 == '0') {
        att_quantity2 = '<span class="badge rounded-pill alert-warning "  >aucun</span><input type="hidden" name="items[' + grade + '][attr_quantity2]" value="0">';
    } else {
        index = 0;
        att_quantity2 = ' <select style="width: auto;" class="form-select attr_quantity2" data-product_grade="' + grade + '" data-product_id="' + product_id + '" id="attr2" name="items[' + grade + '][attr_quantity2]">';
        JSON.parse(attribute4_values).forEach(element => {
            att_id = JSON.parse(attribute4_ids)[index];
            att_quantity2 += '<option class="form-control form-control-sm" value="' + att_id + '" >' + element + '</option>';
            index++;
        });
        att_quantity2 += '</select>';
    }

    $('#dynamic_field').append('<tr id="row' + grade + '" class="item_cart" grade="' + grade + '"><td><input type="hidden" name="items[' + grade + '][product_id]" value="' + product_id + '"/><input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + product_currency + '"/><input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + product_currency_id + '"/><input type="hidden" name="items[' + grade + '][product_price_buying]"value="' + product_price_buying + '"/><input type="text" name="items[' + grade + '][product_name]" placeholder="Nom" class="form-control form-control-sm name_list" readonly value="' + product_name + '"/></td><td>' + att1 + att2 + '</td><td>' + att_quantity1 + att_quantity2 + '</td><td><input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product_quantity + '" min="1" max="" ></td><td><input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + product_unity + '" readonly/></td><td> <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" value="' + product_remise + '" ></td><td> <input type="text" name="items[' + grade + '][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + product_tva + '" readonly></td><td> <input type="text" name="items[' + grade + '][product_price_selling]" style="width: 100px;" class="form-control form-control-sm price' + grade + ' "   id="price' + grade + '"  data-product_id="' + grade + '" value="' + product_price_selling + '" readonly></td><td><input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' "  name="items[' + grade + '][L_total]"  id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + product_price_selling + '" readonly ></td><td><button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button></td></tr>');
    calculate_gtotal();
});
/** end add_to_cart */
/** variable change */
$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
    calculate_gtotal();
});
$(document).on('keyup change', '.quantity', function () {
    calculate_gtotal();
});
$(document).on('keyup change', '.remise', function () {
    calculate_gtotal();
});
$(document).on('change', '.attr1', function () {
    var product_id = $(this).attr("data-product_id");
    var grade = $(this).attr("data-product_grade");
    var attr1 = $(this).find(":selected").val();
    var price_type = $('#Prix_T').val();
    var attr2 = $(this).next('#attr2').val();
    console.log(attr1);
    console.log(attr2);
    var url = "/admin/cart/" + product_id + "/" + price_type + "/" + attr1 + "/" + attr2 + "/get_product_price";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: {
            product_id: product_id,
            price_type: price_type,
            attr1: attr1,
            attr2: attr2
        },
        success: function (data) {
            $('#price' + grade).attr('value', data);
            calculate_gtotal();

        }
    });


});
$(document).on('change', '.attr2', function () {
    var product_id = $(this).attr("data-product_id");
    var grade = $(this).attr("data-product_grade");
    var attr2 = $(this).find(":selected").val();
    var price_type = $('#Prix_T').val();
    var attr1 = $(this).prev('#attr1').val();
    console.log(attr1);
    console.log(attr2);
    var url = "/admin/cart/" + product_id + "/" + price_type + "/" + attr1 + "/" + attr2 + "/get_product_price";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: {
            product_id: product_id,
            price_type: price_type,
            attr2: attr2,
            attr1: attr1
        },
        success: function (data) {
            $('#price' + grade).attr('value', data);
            calculate_gtotal();

        }
    });


});
/**  end liste product add/change/remove */

$('.mobile').blur();

// function toEnglishNumber(strNum) {
//     var ar = '٠١٢٣٤٥٦٧٨٩'.split('');
//     var en = '0123456789'.split('');
//     strNum = strNum.replace(/[٠١٢٣٤٥٦٧٨٩]/g, x => en[ar.indexOf(x)]);
//     strNum = strNum.replace(/[^\d]/g, '');
//     return strNum;
// }

$(document).on('keyup', '.mobile', function (e) {
    var val = toEnglishNumber($(this).val())
    $(this).val(val)
});
var max = 8;
$('.mobile').keypress(function (e) {
    if (e.which < 0x20) {
        // e.which < 0x20, then it's not a printable character
        // e.which === 0 - Not a character
        return; // Do nothing
    }
    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});
// $('.quantity').blur();
// $(document).on('keyup', '.quantity', function (e) {
//     var val = toEnglishNumber($(this).val())
//     $(this).val(val)
//     if ($(this).val() == '') {
//         $(this).val('1');
//         calculate_gtotal();

//     }
// });


// $('.TTC').blur();
$(document).on('blur', '.remise', function (e) {
    if (($(this).val()<0) || ($(this).val()>100))
    {$(this).val(0)}
            calculate_gtotal();

});
$(document).on('blur', '.quantity', function (e) {
    // console.log($(this).attr('data-max'));
    // console.log($(this).val());
    if (parseFloat($(this).val()-$(this).attr('data-max'))>0)
    {
        $(this).val($(this).attr('data-max'));
    }
            calculate_gtotal();

});

$(document).on('keyup', '.mobile', function () {

    var string = $(this).val();
    if ($(this).val().toString().length != 8) {
        // $('.text-danger').html('يجب ان يتكون من 8 أرقام ');
        // $(this).addClass("has-error").addClass("has-danger");
        $('#place_order').attr('type', 'button');
        $('#place_order').attr('style', 'background-color:#d9b725');
    } else {
        // $('.text-danger').html('');
        // $(this).removeClass("has-error").removeClass("has-danger");
        $('#place_order').attr('type', 'submit');
        $('#place_order').attr('style', 'background-color:#90ba57');
    }
});
/** payment */

$('#Add_paiement').hide();
$(document).on('change', '#Deadline_check', function () {
    var check = $(this).is(":checked");
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();

    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }
    console.log(check);

});


// $(document).on('keypress','.Numeric',function (e) {
//     alert(e.which);
//     if (e.which < 0x20) {
//         // e.which < 0x20, then it's not a printable character
//         // e.which === 0 - Not a character
//         return; // Do nothing
//     }
// });
