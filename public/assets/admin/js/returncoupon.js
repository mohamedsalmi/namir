/* cart submit*/
$(document).on("click", "#save", function (e) {
    submit(e);
});
$(document).on("click", "#savewithprint", function (e) {
    submit(e, 'savewithprint');
});
$(document).on("click", "#savewithticket", function (e) {
    submit(e, 'savewithticket');
});
var errors = null;
function submit(e, options = '') {
    e.preventDefault();
    var Form = $('#create-returncoupon-form');
        var url = "/admin/returncoupon/validation";
    if (options != '') {
        var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
        Form.append(input);
    }

    if (errors) {
        $.each(errors, function (key, value) {
            key = key.replace('provider_details.', '');
            var element = document.getElementById(key);
            if (element) {
                element.classList.remove('is-invalid');
            };
        });
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: Form.serialize(),
        async: false,
        success: function (response, textStatus, jqXHR) {
            Form.submit();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errors = jqXHR.responseJSON.errors;
            $.each(errors, function (key, value) {
                key = key.replace('provider_details.', '')
                $("#" + key).addClass('is-invalid');
                $("#" + key).next().html(value[0]);
                $("#" + key).next().removeClass('d-none');
            });
        }
    });
}
/******* */
function calculate_gtotal() {
    $('#dynamic_field').each(function () {
        var totalPoints = 0;
        var total_line = 0;
        $(this).find('.quantity').each(function () {
            var product_id = $(this).attr('data-product_id');
            var grade = $(this).attr('data-product_grade');
            var price = $('#price' + grade).val();
            var remise = $('#remise' + grade).val();
            var tva = $('#tva' + grade).val();
            total_line = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
            totalPoints += total_line;
            $('#L_total' + grade).attr('value', total_line.toFixed(3));
            //<==== a catch  in here !! read below
        });
        $('.total').attr('value', totalPoints.toFixed(3));


    });
}

$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
    calculate_gtotal();
});
$(document).on('keyup change', '.quantity', function () {
    calculate_gtotal();
});
$(document).on('blur', '.quantity', function (e) {
    if (parseFloat($(this).val() - $(this).attr('data-max')) > 0) {
        $(this).val($(this).attr('data-max'));
    }
    calculate_gtotal();
});
