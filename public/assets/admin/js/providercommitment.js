$(document).on("click", "#save", function (e) {
    submit(e);
});

$(document).on("click", "#savewithprint", function (e) {
    submit(e, 'savewithprint');
});

$(document).on("click", "#savewithticket", function (e) {
    submit(e, 'savewithticket');
});

var errors = null;

function submit(e, options='') {
    e.preventDefault();
    var sum = 0;
    if ($('#Deadline_check').is(":checked")) {
        $('.Deadline').each(function () {
            sum += Number($(this).val());
        });
        // console.log(sum);
        total_payment_amount = sum;
        total_voucher = $('#total_voucher').val();
        console.log(total_voucher);
    } else {
        $('.amount').each(function () {
            sum += Number($(this).val());
        });
        total_payment_amount = sum;
        total_voucher = $('#total_voucher').val();
        console.log(total_voucher);
        console.log(sum);
        console.log(total_payment_amount == total_voucher);
    }

    if (total_payment_amount != total_voucher) {
        Swal.fire({
            title: "Paiement incomplet!",
            text: "Vous devez insérer les données de paiement corectement",
            type: "success",
            confirmButtonClass: "btn btn-confirm mt-2"
        });
    } else {
        var Form = $('#create-voucher-form');
        var url = "/admin/financialcommitment/paiementvalidation";
        if(options != ''){
            var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
            Form.append(input);
        }

        if (errors) {
            $.each(errors, function (key, value) {
                key = key.replace('provider_details.', '');
                var element = document.getElementById(key);
                if(element) {
                    element.classList.remove('is-invalid')
                }; 
            });
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "POST",
            data: Form.serialize(),
            async: false,
            success: function (response, textStatus, jqXHR) {
                Form.submit();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //   console.log(jqXHR);
                errors = jqXHR.responseJSON.errors;
                $.each(errors, function (key, value) {
                    key = key.replace('provider_details.', '');
                    // $("#" + key).addClass('is-invalid');
                    // $("#" + key).next().html(value[0]);
                    // $("#" + key).next().removeClass('d-none');
                    var element = document.getElementById(key);
                    if(element) {
                        element.classList.add('is-invalid')
                    }; 
                });
            }
        });
    }
}

function calculate_gtotal() {
    $('#dynamic_field').each(function () {
        var totalPoints = 0;
        var total_line = 0;
        $(this).find('.quantity').each(function () {
            var product_id = $(this).attr('data-product_id');
            var grade = $(this).attr('data-product_grade');
            var price = $('#price' + grade).val();
            var remise = $('#remise' + grade).val();
            var tva = $('#tva' + grade).val();
            total_line = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
            totalPoints += total_line;
            $('#L_total' + grade).attr('value', total_line.toFixed(3));
            //<==== a catch  in here !! read below
        });
        $('.total').attr('value', totalPoints.toFixed(3));


    });
}
$(document).ready(function () {
    "use strict";
    /*Confirm*/
    $('#fidel_list').change(function () {
        var name = $('#fidel_list').children("option:selected").attr('name-fidel');
        var tel = $('#fidel_list').children("option:selected").attr('tel-fidel');
        var mf = $('#fidel_list').children("option:selected").attr('MF-fidel');
        var adresse = $('#fidel_list').children("option:selected").attr('AD-fidel');
        var prix = $('#fidel_list').children("option:selected").attr('Prix-fidel');
        $('#name').val(name);
        $('#phone').val(tel);
        $('#mf').val(mf);
        $('#adresse').val(adresse);
        $('#Prix_T').val(prix);

        $('#dynamic_field').html('<tr hidden="" grade="0"></tr>');
        $("#listproduct").html('');
        calculate_gtotal();
    });
    /*Search Products*/
    $("#name_search").on('submit', function (e) {
        e.preventDefault();
        var query = $('#searchprod').val();
        var price_type = $('#Prix_T').val();
        var provider_id = $('select[name="provider_id"]').val();

        var url = "/admin/voucher/" + query + "/" + price_type + "/serach_product?provider_id=" + provider_id;
        if (query != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                data: {
                    query: query,
                    price_type: price_type
                },
                success: function (data) {
                    if (data.length > 0) {
                        // console.log(data);
                        $('#listproduct').html(data);
                        $("#name_search")[0].reset();
                    } else {
                        $('#listproduct').html('<span class="text-danger ">Aucun résultat!</span>');
                        $("#name_search")[0].reset();
                    }
                }
            });
        }
    });

    /*Search Products */
    /**/
    $("#barcode_search").on('submit', function (e) {
        e.preventDefault();
        var barcode = $('#barcode').val();
        var tr = $('body').find(`[data-barcode='${barcode.trim()}']`);
        if (tr.length > 0) {
            grade = tr.attr('grade');
            var qt = parseFloat($('#quantity' + grade).val()) + 1;
            $('#quantity' + grade).val(qt)
            $('#barcode').val('')
            calculate_gtotal();

        } else {
            var price_type = $('#Prix_T').val();
            var check = $("#dynamic_field tr:last-child").attr('grade');
            var grade = parseInt(check) + 1;
            var provider_id = $('select[name="provider_id"]').val();
            var url = "/admin/voucher/" + barcode + "/" + price_type + "/serach_product_c_bar?provider_id=" + provider_id;
            var price = 0;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                dataType: "json",
                data: {
                    barcode: barcode,
                    price_type: price_type,
                },
                success: function (data) {
                    var att1 = '';
                    var att2 = '';
                    var att_quantity1 = '';
                    var att_quantity2 = '';
                    $('#barcode').val('')
                    console.log(data);              
                    price = data['2'];             
                    $('#dynamic_field').append('<tr id="row' + grade + '" class="item_voucher" grade="' + grade + '" data-barcode="' + barcode.trim() + '"><td><input type="hidden" name="items[' + grade + '][product_id]" value="' + data['0'].id + '"/><input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + data['3'] + '"/><input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + data['0'].currency_id + '"/><input type="hidden" name="items[' + grade + '][product_price_selling]"value="' + data['0'].buying + '"/><input type="text" name="items[' + grade + '][product_name]" placeholder="Nom" class="form-control form-control-sm name_list" readonly value="' + data['0'].name + '"/></td><td><input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="1" min="1" max="" ></td><td><input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + data['0'].unity + '" readonly/></td><td> <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" value="0" ></td><td> <input type="text" name="items[' + grade + '][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + data['0'].tva + '" readonly></td><td> <input type="text" name="items[' + grade + '][product_price_selling]" style="width: 100px;" class="form-control form-control-sm price' + grade + ' "   id="price' + grade + '"  data-product_id="' + grade + '" value="' + price + '" readonly></td><td><input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' "  name="items[' + grade + '][L_total]"  id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + price + '" readonly ></td><td><button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button></td></tr>');
                    calculate_gtotal();
                    $('#listproduct').html('');
                },
                error: function (data) {
                    $('#listproduct').html('<span class="text-danger ">Aucun résultat!</span>');
                    $("#barcode_search")[0].reset();
                },
            });
        }
    });
    /**/

    /* ajax facturation */
    $('#searchprod').focus();
    /* ajax facturation */
       });
/**  start add_to_voucher */
$(document).on('click', '.add_to_voucher', function () {
    var product_id = $(this).attr("id");
    var barcode = $('#product_sku' + product_id).val();
    var tr = $('body').find(`[data-barcode='${barcode}']`);
        if (tr.length > 0) {
            grade = tr.attr('grade');
            var qt = parseFloat($('#quantity' + grade).val()) + 1;
            $('#quantity' + grade).val(qt)
            calculate_gtotal();

        } else{
    var product_name = $('#name' + product_id).val();
    var product_price_buying = $('#product_price_buying' + product_id).val();
    var product_price_selling = $('#product_price_selling' + product_id).val();
    var product_unity = $('#product_unity' + product_id).val();
    var quantity_id = $('#quantity_id' + product_id).val();
    var product_tva = $('#product_tva' + product_id).val();
    var product_currency = $('#currency' + product_id).val();
    var product_currency_id = $('#currency_id' + product_id).val();
    var product_remise = 0;
    var product_quantity = 1;
    var check = $("#dynamic_field tr:last-child").attr('grade');
    var grade = parseInt(check) + 1;
    $('#dynamic_field').append('<tr id="row' + grade + '" class="item_voucher" grade="' + grade + '"  data-barcode="' + barcode + '"><td><input type="hidden" name="items[' + grade + '][product_id]" value="' + product_id + '"/><input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + product_currency + '"/><input type="hidden" name="items[' + grade + '][quantity_id]"value="' + quantity_id + '"/><input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + product_currency_id + '"/><input type="hidden" name="items[' + grade + '][product_price_buying]"value="' + product_price_buying + '"/><input type="text" name="items[' + grade + '][product_name]" placeholder="Nom" class="form-control form-control-sm name_list" readonly value="' + product_name + '"/></td><td><input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product_quantity + '" min="1" max="" ></td><td><input type="text" style="width: 70px;" class="form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + product_unity + '" readonly/></td><td> <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" value="' + product_remise + '" ></td><td> <input type="text" name="items[' + grade + '][product_tva]" style="width: 50px;" class="form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + product_tva + '" readonly></td><td> <input type="text" name="items[' + grade + '][product_price_selling]" style="width: 100px;" class="form-control form-control-sm price' + grade + ' "   id="price' + grade + '"  data-product_id="' + grade + '" value="' + product_price_selling + '" readonly></td><td><input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' "  name="items[' + grade + '][L_total]"  id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + product_price_selling + '" readonly ></td><td><button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button></td></tr>');
    }
    calculate_gtotal();
});
/** end add_to_voucher */
/** variable change */
$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
    calculate_gtotal();
});
$(document).on('keyup change', '.quantity', function () {
    calculate_gtotal();
});
$(document).on('keyup change', '.remise', function () {
    calculate_gtotal();
});
$(document).on('change', '.attr1', function () {
    var product_id = $(this).attr("data-product_id");
    var grade = $(this).attr("data-product_grade");
    var attr1 = $(this).find(":selected").val();
    var price_type = $('#Prix_T').val();
    var attr2 = $(this).next('#attr2').val();
    console.log(attr1);
    console.log(attr2);
    var url = "/admin/voucher/" + product_id + "/" + price_type + "/" + attr1 + "/" + attr2 + "/get_product_price";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: {
            product_id: product_id,
            price_type: price_type,
            attr1: attr1,
            attr2: attr2
        },
        success: function (data) {
            $('#price' + grade).attr('value', data);
            calculate_gtotal();

        }
    });


});
$(document).on('change', '.attr2', function () {
    var product_id = $(this).attr("data-product_id");
    var grade = $(this).attr("data-product_grade");
    var attr2 = $(this).find(":selected").val();
    var price_type = $('#Prix_T').val();
    var attr1 = $(this).prev('#attr1').val();
    console.log(attr1);
    console.log(attr2);
    var url = "/admin/voucher/" + product_id + "/" + price_type + "/" + attr1 + "/" + attr2 + "/get_product_price";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: {
            product_id: product_id,
            price_type: price_type,
            attr2: attr2,
            attr1: attr1
        },
        success: function (data) {
            $('#price' + grade).attr('value', data);
            calculate_gtotal();

        }
    });


});
/**  end liste product add/change/remove */

$('.mobile').blur();

// function toEnglishNumber(strNum) {
//     var ar = '٠١٢٣٤٥٦٧٨٩'.split('');
//     var en = '0123456789'.split('');
//     strNum = strNum.replace(/[٠١٢٣٤٥٦٧٨٩]/g, x => en[ar.indexOf(x)]);
//     strNum = strNum.replace(/[^\d]/g, '');
//     return strNum;
// }

$(document).on('keyup', '.mobile', function (e) {
    var val = toEnglishNumber($(this).val())
    $(this).val(val)
});
var max = 8;
$('.mobile').keypress(function (e) {
    if (e.which < 0x20) {
        // e.which < 0x20, then it's not a printable character
        // e.which === 0 - Not a character
        return; // Do nothing
    }
    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});
$('.quantity').blur();
$(document).on('keyup', '.quantity', function (e) {
    var val = toEnglishNumber($(this).val())
    $(this).val(val)
    if ($(this).val() == '') {
        $(this).val('1');
        calculate_gtotal();

    }
});


// $('.TTC').blur();
$(document).on('blur', '.remise', function (e) {
    if (($(this).val() < 0) || ($(this).val() > 100)) { $(this).val(0) }
    calculate_gtotal();

});
$(document).on('blur', '.quantity', function (e) {
    if ($(this).val() < 1) { $(this).val(1) }
    calculate_gtotal();

});

$(document).on('keyup', '.mobile', function () {

    var string = $(this).val();
    if ($(this).val().toString().length != 8) {

        // $('.text-danger').html('يجب ان يتكون من 8 أرقام ');
        // $(this).addClass("has-error").addClass("has-danger");
        $('#place_order').attr('type', 'button');
        $('#place_order').attr('style', 'background-color:#d9b725');
    } else {

        // $('.text-danger').html('');
        // $(this).removeClass("has-error").removeClass("has-danger");
        $('#place_order').attr('type', 'submit');
        $('#place_order').attr('style', 'background-color:#90ba57');

    }
});
/** payment */

$(document).ready(function () {
    var check = $('#Deadline_check').is(":checked");
    console.log(check);
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();

    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }
});
$(document).on('change', '#Deadline_check', function () {
    var check = $(this).is(":checked");
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();

    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }

});


// $(document).on('keypress','.Numeric',function (e) {
//     alert(e.which);
//     if (e.which < 0x20) {
//         // e.which < 0x20, then it's not a printable character
//         // e.which === 0 - Not a character
//         return; // Do nothing
//     }
// });