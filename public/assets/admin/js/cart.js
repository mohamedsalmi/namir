/***** focus  */
// $(document).ready(function () {
//     $("#product_list").select2('open');
//     document.querySelector('.select2-search__field').focus();
// });
/***** */
/* cart submit*/
var document_type=$('#document').val();
$(document).on("click", "#save", function (e) {
    submit(e);
});
$(document).on("click", "#savewithprint", function (e) {
    submit(e, 'savewithprint');
});
$(document).on("click", "#savewithticket", function (e) {
    submit(e, 'savewithticket');
});
var errors = null;
function submit(e, options = '') {
    e.preventDefault();
    var Form = $('#create-cart-form');
    var url = "/admin/"+document_type+"/"+document_type+"validation";
    if (options != '') {
        var input = $("<input>").attr("type", 'hidden').attr("name", options).val(true);
        Form.append(input);
    }
    if (errors) {
        $.each(errors, function (key, value) {
            key = key.replace('client_details.', '');
            var element = document.getElementById(key);
            if (element) {
                element.classList.remove('is-invalid')
            };
        });
    }
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "POST",
        data: Form.serialize(),
        async: false,
        success: function (response, textStatus, jqXHR) {
            Form.submit();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errors = jqXHR.responseJSON.errors;
            $.each(errors, function (key, value) {
                key = key.replace('client_details.', '');
                var element = document.getElementById(key);
                if (element) {
                    element.classList.add('is-invalid')
                }
            });
        }
    });

}
/*** end cart submit */

/**** change client select */
$(document).ready(function () {
    "use strict";
    /*Confirm*/
    $('#fidel_list').change(function () {
        var name = $('#fidel_list').children("option:selected").attr('name-fidel');
        var tel = $('#fidel_list').children("option:selected").attr('tel-fidel');
        var mf = $('#fidel_list').children("option:selected").attr('MF-fidel');
        var adresse = $('#fidel_list').children("option:selected").attr('AD-fidel');
        var prix = $('#fidel_list').children("option:selected").attr('Prix-fidel');
        var client_id = $('#fidel_list').children("option:selected").val();
        var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
        $('#name').val(name);
        $('#phone').val(tel);
        $('#mf').val(mf);
        $('#adresse').val(adresse);
        $('#Prix_T').val(prix);
        $('#price_type').val(prix);
        var select = $('#Prix_T')
        // if (client_id != 0) {
        //     select.attr('disabled', 'disabled');
        // } else {
        //     select.removeAttr('disabled');
        //
        // }
        var client_id = $('select[name="client_id"]').val();
        $('#dynamic_field ').each(function () {
            $(this).find('.price').each(function () {

                var grade = $(this).attr('data-product_grade');
                // var product_id = $(this).attr('data-product_id');
                // var price_type = prix;
                // var url = "/cart/" + product_id + "/" + price_type + "/" + client_id + "/get_pricing";
                // $.ajax({
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     },
                //     url: url,
                //     method: "GET",
                //     data: {
                //         product_id: product_id,
                //         price_type: price_type,
                //         grade: grade,
                //         client_id: client_id
                //     },
                //     success: function (data) {
                //         if (data.remise) {
                //             var maxdiscount= $('#remise' + grade).attr('data-maxdiscount');
                //             if(data.remise > maxdiscount)
                //             {
                //                 $('#remise' + grade).attr('value', maxdiscount);
                //             }else{
                //                 $('#remise' + grade).attr('value', data.remise);
                //             }
                //         } else {
                //             $('#price' + grade).attr('value', data.price);
                //         }
                //         calculate_gtotal();
                //     }
                // });

                var maxdiscount = parseFloat($('#remise' + grade).attr('data-maxdiscount'));
                if (discount > maxdiscount) {
                    $('#remise' + grade).attr('value', maxdiscount.toFixed(2));
                } else {
                    $('#remise' + grade).attr('value', discount);
                }

                calculate_gtotal();

            });
        });
    });
    /**** end change client select */
    /****  change product select */
    // $('#product_list').change(function () {
    //     var product_id = $(this).children("option:selected").attr('product-id');
    //     var tr = $('body').find(`[data-id='${product_id}']`);
    //     if ((tr.length > 0) && (product_id != undefined)) {
    //         grade = tr.attr('grade');
    //         var qt = parseFloat($('#quantity' + grade).val()) + 1;
    //         $('#quantity' + grade).val(qt)
    //         calculate_gtotal();

    //     } else {
    //         var check = $("#dynamic_field tr:last-child").attr('grade');
    //         var grade = parseInt(check) + 1;
    //         var client_id = $('select[name="client_id"]').val();
    //         var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
    //         var price_type = $('#Prix_T').val();
    //         let product_price_selling = 0;
    //         let remise = 0;
    //         let product = {
    //             id: product_id,
    //             name: $('#product_list').children("option:selected").attr('product-name'),
    //             price_buying: $('#product_list').children("option:selected").attr('price-buying'),
    //             price_selling: product_price_selling,
    //             unity: $('#product_list').children("option:selected").attr('product-unity'),
    //             tva: $('#product_list').children("option:selected").attr('product-tva'),
    //             currency: $('#product_list').children("option:selected").attr('product-currency'),
    //             currency_id: $('#product_list').children("option:selected").attr('product-currency-id'),
    //             maxdiscount: $('#product_list').children("option:selected").attr('product-maxdiscount'),
    //             remise: remise,
    //             quantity: 1,
    //         };
    //         if (client_id != 0) {
    //             // var url = "/cart/" + product_id + "/" + price_type + "/" + client_id + "/get_pricing";
    //             // $.ajax({
    //             //     headers: {
    //             //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             //     },
    //             //     url: url,
    //             //     method: "GET",
    //             //     data: {
    //             //         product_id: product_id,
    //             //         price_type: price_type,
    //             //         client_id: client_id
    //             //     },
    //             //     success: function (data) {
    //             //         if (data.remise) {
    //             //             if (data.remise > product.maxdiscount) {
    //             //                 product.remise = product.maxdiscount;
    //             //             } else {
    //             //                 product.remise = data.remise;
    //             //             }
    //             //         } else {
    //             //             product.price_selling = data.price;
    //             //         }
    //             //         createRow(grade, product);

    //             //     }
    //             // });
    //             if (discount > product.maxdiscount) {
    //                 product.remise = product.maxdiscount;
    //             } else {
    //                 product.remise = discount;
    //             }
    //         }
    //         // else {
    //         switch (price_type) {
    //             case 'detail':
    //                 product_price_selling = $('#product_list').children("option:selected").attr('price-selling1');
    //                 break;
    //             case 'semigros':
    //                 product_price_selling = $('#product_list').children("option:selected").attr('price-selling2');
    //                 break;
    //             case 'gros':
    //                 product_price_selling = $('#product_list').children("option:selected").attr('price-selling3');
    //                 break;
    //         }
    //         product.price_selling = product_price_selling;
    //         createRow(grade, product);
    //         // }
    //     }
    //     $(document).ready(function () {
    //         $("#product_list").val('');
    //         $("#product_list").select2('open');
    //         document.querySelectorAll('.select2-search__field')[1].focus();
    //     });
    //     // play sound
    //     $('#sucessAudio')[0].load();
    //     $('#sucessAudio')[0].play();
    //     calculate_gtotal();
    // });

});
/**** end change product select */

/**** delete row */
$(document).on('click', '.btn_remove', function () {
    var tr = $(this).attr("tr");
    $('#row' + tr + '').remove();
    calculate_gtotal();
});
/**** end delete row */
/**** change quantity */
$(document).on('keyup change', '.quantity', function () {
    calculate_gtotal();
});
$(document).on('keyup change', '.price', function () {
    calculate_gtotal();
});
$(document).on('blur', '.quantity', function (e) {
    if ($(this).val() < 1) { $(this).val(1) }
    calculate_gtotal();
});
/**** end change quantity */
/*** change remise */
$(document).on('keyup change', '.remise', function () {
    calculate_gtotal();
});
$(document).on('blur', '.remise', function (e) {
    var max = 100;
    if ($(this).attr('data-maxdiscount') != undefined && $(this).attr('data-maxdiscount') != 'null') {
        max = parseFloat($(this).attr('data-maxdiscount'));
    }
    if (($(this).val() < 0) || ($(this).val() == '')) { $(this).val(0) }
    if (($(this).val() > max)) { $(this).val(max) }
    calculate_gtotal();
});
/*** end change remise */
/*** change remise global */

$(document).on('input', '#global_discount', function() {
    const value = $(this).val();
    
    $('.remise').val(function() {
      return value;
    });
    
    calculate_gtotal();
  });
  
  $(document).on('blur', '#global_discount', function(e) {
    let value = parseFloat($(this).val());
    
    if (isNaN(value) || value < 0) {
      value = 0;
    } else if (value > 100) {
      value = 100;
    }
    
    $('.remise').each(function() {
        let max = 100;
      if ($(this).attr('data-maxdiscount') != undefined && $(this).attr('data-maxdiscount') != 'null') {
        max = parseFloat($(this).attr('data-maxdiscount'));
      }
      
      if (value > max) {
        $(this).val(max);
      } else {
        $(this).val(value);
      }
    });
    
    $(this).val(value);
    calculate_gtotal();
  });
  
  
/*** end change remise global*/

/** total calculate */
function calculate_gtotal() {
    $('#dynamic_field').each(function () {
        var totalPoints = 0;
        var total_line = 0;
        $(this).find('.quantity').each(function () {
            var product_id = $(this).attr('data-product_id');
            var grade = $(this).attr('data-product_grade');
            var price = $('#price' + grade).val();
            var remise = $('#remise' + grade).val();
            var tva = $('#tva' + grade).val();
            total_line = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
            totalPoints += total_line;
            $('#L_total' + grade).attr('value', total_line.toFixed(3));
            //<==== a catch  in here !! read below
        });
        $('.total').attr('value', totalPoints.toFixed(3));
    });
}
/** end total calculate */

/** create row */
function createRow(grade, product) {
    let newRow = '<tr id="row' + grade + '" class="item_cart" grade="' + grade + '"  data-id="' + product.id + '">\n' +
        '    <td>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_id]" value="' + product.id + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_currency_value]"value="' + product.currency + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_currency_id]"value="' + product.currency_id + '"/>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_price_buying]"value="' + product.price_buying + '"/>\n' +
        '        <input type="text" name="items[' + grade + '][product_name]" class="move form-control form-control-sm name_list" readonly value="' + product.name +'"/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" style="width:80px;" name="items[' + grade + '][product_quantity]" class="move form-control form-control-sm quantity" oninput="this.value = Math.floor(this.value);" step="1" id="quantity' + grade + '"  data-product_grade="' + grade + '" data-product_id="' + grade + '" value="' + product.quantity + '" min="1" max="" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" style="width: 70px;" class="move form-control form-control-sm" name="items[' + grade + '][product_unity]"value="' + product.unity + '" readonly/>\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="number" name="items[' + grade + '][product_remise]" style="width: 80px;" min="0" max="100" step="0.01" class="move form-control form-control-sm remise"  id="remise' + grade + '"  data-product_id="' + grade + '" data-maxdiscount="' + product.maxdiscount + '" value="' + product.remise + '" >\n' +
        '    </td>\n' +
        // '    <td>\n' +
        '        <input type="hidden" name="items[' + grade + '][product_tva]" style="width: 50px;" class="move form-control form-control-sm tva"  id="tva' + grade + '"  data-product_id="' + grade + '" value="' + product.tva + '" readonly>\n' +
        // '    </td>\n' +
        '    <td>\n' +
        '        <input type="text" name="items[' + grade + '][product_price_selling]" style="width: 100px;" class="move form-control form-control-sm price "   id="price' + grade + '"  data-product_id="' + product.id + '" data-product_grade="' + grade + '" value="' + product.price_selling + '" >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <input type="text"  style="width: 100px;" class="form-control form-control-sm price' + grade + ' "  name="items[' + grade + '][L_total]"  id="L_total' + grade + '"  data-product_id="' + grade + '" value="' + product.price_selling + '" readonly >\n' +
        '    </td>\n' +
        '    <td>\n' +
        '        <button type="button" name="remove" id="' + grade + '" tr="' + grade + '" class="btn btn-danger btn-sm btn_remove delete"><i class="bi bi-trash-fill"></i></button>\n' +
        '    </td>\n' +
        '</tr>';
    $('#dynamic_field').append(newRow);
    calculate_gtotal();
}
/**end create row */

// $(document).ready(function () {
//     var check = $('#Deadline_check').is(":checked");
//     if (check == true) {
//         $('#Add_paiement').show();
//         $('#cash').hide();

//     } else {
//         $('#Add_paiement').hide();
//         $('#cash').show();
//     }
// });
// $(document).on('change', '#Deadline_check', function () {
//     var check = $(this).is(":checked");
//     if (check == true) {
//         $('#Add_paiement').show();
//         $('#cash').hide();
//     } else {
//         $('#Add_paiement').hide();
//         $('#cash').show();
//     }
// });
/********* */
// $('#product_list').select2().('select2-search__field')
// $("#name_search").on('keyup', function(e) {
$(document).ready(function () {
    // let el = document.querySelectorAll('.select2-search__field')[1];
    // $(document).on('keypress', el, function () {

    //     var query = el.value;
    //     var url = "/cart/" + query + "/searchByName";

    //     let l = el.value.length;
    //     if (l > 3) {
    //         $.ajax({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             },
    //             url: url,
    //             method: "POST",
    //             dataType: 'json',

    //             success: function (data) {
    //                 $('#product_list').val('');
    //                 if (data.length == 1) {
    //                     const element = data[0];
    //                     let option = `<option 
    //                 product-id="${element.id}"
    //                 product-name="${element.name}"
    //                 price-buying="${element.buying}"
    //                 price-selling1="${element.selling1}"
    //                 price-selling2="${element.selling2}"
    //                 price-selling3="${element.selling3}"
    //                 product-unity="Pièce"
    //                 product-tva="${element.tva}"
    //                 product-sku="${element.sku}"
    //                 product-currency="TND"
    //                 product-currency-id="1"
    //                 product-maxdiscount="${element.maxdiscount}">
    //                 ${element.name} - ${element.reference} - ${element.sku}</option>`;
    //                     $('#product_list').append(option).trigger('change');
    //                 } else {

    //                     for (let index = 0; index < data.length; index++) {
    //                         const element = data[index];
    //                         let option = `<option 
    //                     product-id="${element.id}"
    //                     product-name="${element.name}"
    //                     price-buying="${element.buying}"
    //                     price-selling1="${element.selling1}"
    //                     price-selling2="${element.selling2}"
    //                     price-selling3="${element.selling3}"
    //                     product-unity="Pièce"
    //                     product-tva="${element.tva}"
    //                     product-sku="${element.sku}"
    //                     product-currency="TND"
    //                     product-currency-id="1"
    //                     product-maxdiscount="${element.maxdiscount}">
    //                     ${element.name} - ${element.reference} - ${element.sku}</option>`;
    //                         $('#product_list').append(option);

    //                         // $('#product_list').append(element.id, element.name);
    //                         console.log(element.id, element.name);

    //                     }
    //                 }

    //             }
    //         });
    //     }
    // });
    /******* */
    $("#filter").on('click', function (e) {

        e.preventDefault();
        var ecrivain = $('#ecrivain').val();
        var house = $('#house').val();
        var enquete = $('#enquete').val();
        var name = $('#product_name').val();
        var sku = $('#sku').val();        
        var all_status = $('#all_status').is(":checked");
        if (ecrivain || house || enquete || name || sku) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/admin/cart/filter',
                method: "POST",
                data: {
                    name: name,
                    ecrivain: ecrivain,
                    house: house,
                    sku: sku,
                    all_status: all_status,
                    enquete: enquete
                },
                success: function (data) {
                    $('#list_product').html('');
                    if (data.length == 1) {
                        const element = data[0];
                        var product_id = element.id;
                        var tr = $('body').find(`[data-id='${product_id}']`);
                        if ((tr.length > 0) && (product_id != undefined)) {
                            grade = tr.attr('grade');
                            var qt = parseFloat($('#quantity' + grade).val()) + 1;
                            $('#quantity' + grade).val(qt)
                            calculate_gtotal();

                        } else {
                            var check = $("#dynamic_field tr:last-child").attr('grade');
                            var grade = parseInt(check) + 1;
                            var client_id = $('select[name="client_id"]').val();
                            var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
                            var price_type = $('#Prix_T').val();
                            let product_price_selling = 0;
                            let remise = 0;
                            let product = {
                                id: product_id,
                                name: element.name,
                                price_buying: element.buying,
                                price_selling: product_price_selling,
                                unity: "Pièce",
                                tva: element.tva,
                                currency: "TND",
                                currency_id: "1",
                                maxdiscount: element.maxdiscount,
                                house: element.house,
                                remise: remise,
                                quantity: 1,
                            };
                            if (client_id != 0) {
                                if (discount > product.maxdiscount) {
                                    product.remise = product.maxdiscount;
                                } else {
                                    product.remise = discount;
                                }
                            }
                            // else {
                            switch (price_type) {
                                case 'detail':
                                    product_price_selling = element.selling1;
                                    break;
                                case 'semigros':
                                    product_price_selling = element.selling2;
                                    break;
                                case 'gros':
                                    product_price_selling = element.selling3;
                                    break;
                            }
                            product.price_selling = product_price_selling;
                            createRow(grade, product);
                            // }
                        }
                        $('#sucessAudio')[0].load();
                        $('#sucessAudio')[0].play();
                        calculate_gtotal();
                    } else if (data.length > 1) {
                        for (let index = 0; index < data.length; index++) {
                            const element = data[index];
                            let option = `<option 
                        product-id="${element.id}"
                        product-name="${element.name}"
                        price-buying="${element.buying}"
                        price-selling1="${element.selling1}"
                        price-selling2="${element.selling2}"
                        price-selling3="${element.selling3}"
                        product-unity="Pièce"
                        product-tva="${element.tva}"
                        product-house="${element.house}"
                        product-sku="${element.sku}"
                        product-currency="TND"
                        product-currency-id="1"
                        product-maxdiscount="${element.maxdiscount}"
                        data-img=${element.default_image_url}
                        ">
                        ${element.name} - ${element.writer}- ${element.house}- (qté:${element.old_quantity}) -(prix:${element.selling1})</span></option>`;
                            $('#list_product').append(option);
                        }
                    } else {
                        $('#list_product').append('<option class="text-danger" disabled>aucun article</option>');
                    }
                    $('#sku').val('');
                    $('#my_multi_select2').val('');
                    $('#my_multi_select2').multiSelect('refresh');

                }
            });
        } else {
            $('#sku').val('');
            $('#list_product').html('');
            $('#list_product').append('<option class="text-danger" disabled>aucune données dans les filtres</option>');
            $('#my_multi_select2').val('');
            $('#my_multi_select2').multiSelect('refresh');
        }



    });
    //*********** add to cart list filter */
    $(document).on('click', '#add_to_cart', function () {
        // let array=document.querySelectorAll('.select2-selection__rendered')[2].getElementsByTagName("li");
        // console.log(array);
        let array = $('#list_product').find(':selected');
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            var product_id = element.getAttribute('product-id');
            var tr = $('body').find(`[data-id='${product_id}']`);
            if ((tr.length > 0) && (product_id != undefined)) {
                grade = tr.attr('grade');
                var qt = parseFloat($('#quantity' + grade).val()) + 1;
                $('#quantity' + grade).val(qt)
                calculate_gtotal();

            } else {
                var check = $("#dynamic_field tr:last-child").attr('grade');
                var grade = parseInt(check) + 1;
                var client_id = $('select[name="client_id"]').val();
                var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
                var price_type = $('#Prix_T').val();
                let product_price_selling = 0;
                let remise = 0;
                let product = {
                    id: product_id,
                    name: element.getAttribute('product-name'),
                    price_buying: element.getAttribute('price-buying'),
                    price_selling: product_price_selling,
                    unity: element.getAttribute('product-unity'),
                    tva: element.getAttribute('product-tva'),
                    currency: element.getAttribute('product-currency'),
                    currency_id: element.getAttribute('product-currency-id'),
                    maxdiscount: element.getAttribute('product-maxdiscount'),
                    house: element.getAttribute('product-house'),
                    remise: remise,
                    quantity: 1,
                };
                if (client_id != 0) {
                    if (discount > product.maxdiscount) {
                        product.remise = product.maxdiscount;
                    } else {
                        product.remise = discount;
                    }
                }
                // else {
                switch (price_type) {
                    case 'detail':
                        product_price_selling = element.getAttribute('price-selling1');
                        break;
                    case 'semigros':
                        product_price_selling = element.getAttribute('price-selling2');
                        break;
                    case 'gros':
                        product_price_selling = element.getAttribute('price-selling3');
                        break;
                }
                product.price_selling = product_price_selling;
                createRow(grade, product);
                // }
            }
            $('#my_multi_select2').val('');
            $('#my_multi_select2').multiSelect('refresh');
            // $('#list_product').html('');

            $('#sucessAudio')[0].load();
            $('#sucessAudio')[0].play();
            calculate_gtotal();
        }
    });
    /**********  */
    //*********** add to cart list filter */
    $(document).on('click', '.addToCart', function () {
        // let array=document.querySelectorAll('.select2-selection__rendered')[2].getElementsByTagName("li");
        // console.log(array);

            const element = $(this);
            var product_id = element.attr('product-id');
            var tr = $('body').find(`[data-id='${product_id}']`);
            if ((tr.length > 0) && (product_id != undefined)) {
                grade = tr.attr('grade');
                var qt = parseFloat($('#quantity' + grade).val()) + 1;
                $('#quantity' + grade).val(qt)
                calculate_gtotal();

            } else {
                var check = $("#dynamic_field tr:last-child").attr('grade');
                var grade = parseInt(check) + 1;
                var client_id = $('select[name="client_id"]').val();
                var discount = $('#fidel_list').children("option:selected").attr('Remise-fidel');
                var price_type = $('#Prix_T').val();
                let product_price_selling = 0;
                let remise = 0;
                let product = {
                    id: product_id,
                    name: element.attr('product-name'),
                    price_buying: element.attr('price-buying'),
                    price_selling: product_price_selling,
                    unity: element.attr('product-unity'),
                    tva: element.attr('product-tva'),
                    currency: element.attr('product-currency'),
                    currency_id: element.attr('product-currency-id'),
                    maxdiscount: element.attr('product-maxdiscount'),
                    house: element.attr('product-house'),
                    remise: remise,
                    quantity: 1,
                };
                if (client_id != 0) {
                    if (discount > product.maxdiscount) {
                        product.remise = product.maxdiscount;
                    } else {
                        product.remise = discount;
                    }
                }
                // else {
                switch (price_type) {
                    case 'detail':
                        product_price_selling = element.attr('price-selling1');
                        break;
                    case 'semigros':
                        product_price_selling = element.attr('price-selling2');
                        break;
                    case 'gros':
                        product_price_selling = element.attr('price-selling3');
                        break;
                }
                product.price_selling = product_price_selling;
                createRow(grade, product);
                // }
            }
            $('#my_multi_select2').val('');
            $('#my_multi_select2').multiSelect('refresh');
            // $('#list_product').html('');

            $('#sucessAudio')[0].load();
            $('#sucessAudio')[0].play();
            calculate_gtotal();

    });
    /**********  */
});
// $(document).keydown(function (e) {
//     switch (e.which) {
//         case 13: //F2 : Enregistrer
//             e.preventDefault();
//             break;

//     }
// });
// var input = document.getElementById("myInput");

// // Execute a function when the user presses a key on the keyboard
// input.addEventListener("keypress", function(event) {
//   // If the user presses the "Enter" key on the keyboard
//   if (event.key === "Enter") {
//     // Cancel the default action, if needed
//     event.preventDefault();
//     // Trigger the button element with a click
//     document.getElementById("myBtn").click();
//   }
// });
$(".filter").keypress(function (event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#filter").click();
    }
});
/******** online sale */
// $(document).on('change', '#state', function () {
//     let state = $(this).val();
//     console.log(state);
//     url='/onlinesale/getcities'
//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         url: url,
//         method: "GET",
//         data: {
//             state: state,
//         }, async: false,
//         success: function (data) {
//             console.log(data);

//         },
//     });
// });
$('#state').change(function () {
    var state = $('#state').val();
    $("#city").val("");
    $("#city").select2({
        templateResult: function (option, container) {
            if ($(option.element).attr("data-state") != state) {
                $(container).css("display", "none");
            } return option.text;
        }
    });
    $('.select2').css('width', '100%');
});
/******** */

    $('#products').arrowTable({
        enabledKeys: ['left', 'right', 'up', 'down'],
        listenTarget:'.move',
        focusTarget:'.move', 
    });
    $(document).keydown(function (e) {
        // console.log(e.which);
        switch (e.which) {
            case 113: //F2 : Enregistrer
                e.preventDefault();
                var save = $("#save");
                save.click();
                break;
            // case 114: //F3 : Enregistrer & Nouveau
            //     e.preventDefault();
            //     var saveandnew = $("#saveandnew");
            //     saveandnew.click();
            //     break;
            case 40: // Arrow bottom   
                if (!$('.move').is(':focus')) {
                    e.preventDefault();
                    $(document).ready(function () {
                        $('.quantity:last-child').focus();

                    });
                }
                break;
        }
    });
    $(document).on('keydown', '.move', function (e) {
        if (e.which == 40) {
            if ($(this).is(':focus') && $(this).is(':last-child')) {
                e.preventDefault();
                $(document).ready(function () {
                $('#product_name').focus();
                });
            }
        }
    });
  /*/********** Payment */
  function getDate(added_months) {
    var date = new Date();
    var newDate = new Date(date.setMonth(date.getMonth()+ added_months));
    var currentDate = newDate.toISOString().substring(0, 10);
    return currentDate;
}

function divideCommitments(total_items) {
    var total = $('.total').val();
    console.log(total_items);
    for (let index = 1; index <= total_items; index++) {
        let amount = document.getElementById('Deadline.' + index + '.amount');
        let date = document.getElementById('Deadline.' + index + '.date');
        console.log(amount);
        date.value = getDate(index - 1);
        if (index == total_items) {
            amount.value = (Math.floor(total / total_items) + total % total_items).toFixed(3);
        } else {
            amount.value = (Math.floor(total / total_items)).toFixed(3);
        }
    }
}

$(document).ready(function () {
    var check = $('#Deadline_check').is(":checked");
    console.log(check);
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();
        divideCommitments(2);
    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }
});
$(document).on('change', '#Deadline_check', function () {
    var check = $(this).is(":checked");
    if (check == true) {
        $('#Add_paiement').show();
        $('#cash').hide();
        divideCommitments(2);
    } else {
        $('#Add_paiement').hide();
        $('#cash').show();

    }

});