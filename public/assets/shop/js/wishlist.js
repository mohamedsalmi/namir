
    $(document).on('click', '.add_wishlist', function () {
        let product_id = $(this).attr('data');
        let url = "/"+product_id + "/addwishlist";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "POST",
            data: {
                product_id: product_id,
            },
            success: function (data) {
                let icon = $("#wish" + product_id + " i");
                console.log(icon);

            if (data == 0) {
                icon.css('color', 'black');
            } else {
                icon.css('color', 'red');
            }
            },
            error:function(){
                $.notify({
                    icon: "fa fa-exclamation-triangle",
                    message: wishlistNotify,
                    url: "/login",
                    target: "_self"
                  }, {
                    type: "warning",
                    allow_dismiss: true,
                    newest_on_top: true,
                    placement: {
                      from: "top",
                      align: "center"
                    },
                    template: '<div data-notify="container" class="alert alert-warning text-center" role="alert">' +
                      '<button type="button" aria-hidden="true" class="btn-close" data-notify="dismiss"></button>' +
                      '<span data-notify="message">{2} <a href="{3}" class="btn btn-warning bg-warning w-50" target="{3}">'+login+'</a></span>' +
                      '</div>'
                  });
                  
                
            }
        });

    });
