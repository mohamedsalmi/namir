 /**=====================
     Filter Sidebar js
==========================**/
$(document).ready(function () {
    $(document).on("click", ".filter-button", function () {
        $(".bg-overlay, .left-box").addClass("show");
    });
    $(".back-button, .bg-overlay").click(function () {
        $(".bg-overlay, .left-box").removeClass("show");
    });

    $(".sort-by-button").click(function () {
        $(".top-filter-menu").toggleClass("show");
    });
});