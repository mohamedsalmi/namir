<?php

// Use statements
use App\Helpers\Helper;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Shop\Auth\ShopLoginController;
use App\Http\Controllers\Shop\Auth\ShopRegisterController;
use App\Http\Controllers\Shop\HomeController;
use App\Http\Controllers\Shop\ShopCartController;
use App\Http\Controllers\Shop\ShopController;
use App\Http\Controllers\Shop\WishlistController;
use App\Models\Currency;
use App\Models\Order;
use App\Models\Product;
use App\Models\Testimonial;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

// Route definition
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    // Home page route
    Route::get('/', [HomeController::class, 'index'])->name('shop.home');

    // Contact Us route
    Route::get('/contact-us', function () {
        $testimonials = Testimonial::all();
        return view('shop.contact_us');
    })->name('shop.contact_us');

    // About Us route
    Route::get('/about-us', function () {
        $testimonials = Testimonial::all();
        return view('shop.about_us', compact('testimonials'));
    })->name('shop.about_us');

    // Login route
    Route::get('/login', [ShopLoginController::class, 'showLoginForm'])->name('shop.login');
    Route::post('/login', [ShopLoginController::class, 'login'])->name('shop.login');

    // Forgot password route
    Route::get('/forgot-password', function () {
        return view('shop.auth.passwords.email');
    })->middleware('guest')->name('shop.password.request');

    // Register route
    Route::post('/register', [ShopRegisterController::class, 'register'])->name('shop.account.create');
    Route::get('/register', [ShopRegisterController::class, 'showRegistrationForm'])->name('shop.register');

    // Logout route
    Route::get('logout', [ShopLoginController::class, 'logout'])->name('shop.logout');

    // Hot Deal route
    Route::get('/HotDeal', function ($product_id = null) {
        $products = Product::select('id', 'name', 'selling1', 'discount')
            ->orderBy('discount')
            ->limit(12)
            ->get();

        return view('shop.partials.dealmodal', compact('products'));
    })->name('shop.HotDeal');

    // Quick view modal route
    Route::get('/quick-view-modal/{product_id?}', function ($product_id = null) {
        return view('shop.partials.modals.quick-view-modal', compact('product_id'));
    })->name('shop.quick-view-modal');

    Route::post('/send-mail', [App\Http\Controllers\Shop\HomeController::class, 'send_mail'])
        ->name('shop.send_mail');

    Route::prefix('shop')->group(function () {
        Route::get('/', [App\Http\Controllers\Shop\ShopController::class, 'index'])
            ->name('shop.index');

        Route::get('/show/{product}', [App\Http\Controllers\Shop\ShopController::class, 'show'])
            ->name('shop.show');

        Route::prefix('cart')->group(function () {
            Route::get('/', [App\Http\Controllers\Shop\ShopCartController::class, 'index'])
                ->name('shop.cart.index');

            Route::get('/checkout', [App\Http\Controllers\Shop\ShopCartController::class, 'checkout'])
                ->name('shop.cart.checkout');

            Route::get('/order/success', [App\Http\Controllers\Shop\ShopCartController::class, 'orderSuccess'])
                ->name('shop.cart.order.success');

            Route::post('/order', [App\Http\Controllers\Shop\ShopCartController::class, 'order'])
                ->name('shop.cart.order');

            Route::get('/coupon/check', [App\Http\Controllers\Shop\CouponController::class, 'checkCoupon'])
                ->name('shop.coupon.check');

            Route::post('/addtocart', [App\Http\Controllers\Shop\ShopCartController::class, 'addToCart'])
                ->name('shop.cart.addtocart');

            Route::post('/removecartitem', [App\Http\Controllers\Shop\ShopCartController::class, 'deleteCartItem'])
                ->name('shop.cart.removecartitem');
        });
    });

    Route::post('/set-currency', [App\Http\Controllers\Admin\CurrencyController::class, 'setCurrency'])
        ->name('shop.setCurrency');

    Route::middleware(['auth:shop'])->group(function () {
        Route::get('/profile', function () {
            $nav = 'profile';
            return view('shop.profile', compact('nav'));
        })->name('shop.profile');
        Route::get('/', function () {
            $nav = 'dashboard';
            $orders_number = Order::where('client_id', auth()->user()->id)
                ->count();
            $confirmed_orders_number = Order::where('client_id', auth()->user()->id)
                ->where('status', 'livré')
                ->count();
            $canceled_orders_number = Order::where('client_id', auth()->user()->id)
                ->where('status', 'annulé')
                ->count();
            $wishlists_number = Wishlist::where(['client_id' => auth()->user()->id])
                ->count();
            return view('shop.profile', compact(
                'nav',
                'orders_number',
                'confirmed_orders_number',
                'canceled_orders_number',
                'wishlists_number'
            ));
        })->name('shop.dashboard');
        Route::get('/wishlist', function () {
            $nav = 'wishlist';
            $wishlists = Wishlist::where(['client_id' => auth()->user()->id])->get();
            return view('shop.profile', compact('nav', 'wishlists'));
        })->name('shop.wishlist');
        Route::get('/order', function () {
            $nav = 'order';
            $orders = Order::where('client_id', auth()->user()->id)->get();
            return view('shop.profile', compact('nav', 'orders'));
        })->name('shop.order');
        Route::get('/order/{order}/show', function ($id) {
            $order = Order::where('id', $id)
                ->where('client_id', auth()->user()->id)
                ->first();
            if ($order) {
                return view('shop.order', compact('order'));
            } else {
                return  abort(404);
            }
        })->name('shop.order.show');
        Route::post('/updateProfile', [ClientController::class, 'updateProfile'])->name('shop.client.updateProfile');
        Route::post('/valdationprofile', [ClientController::class, 'valdationprofile'])->name('shop.client.valdationprofile');
        Route::post('/avatarchange', [ClientController::class, 'avatarchange'])->name('shop.client.avatarchange');
        Route::get('/order/{order}/tracking', function ($id) {
            $order = Order::where('id', $id)
                ->where('client_id', auth()->user()->id)
                ->first();
            return view('shop.order_tracking', compact('order'));
        })->name('shop.order.tracking');
        Route::post('/{product}/addwishlist', [WishlistController::class, 'addwishlist'])->name('shop.addwishlist');
        Route::get('/{wishlist}/delete', [WishlistController::class, 'delete'])->name('shop.wishlist.delete');
    });
});
