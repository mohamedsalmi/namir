<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\AttributeController;
use App\Http\Controllers\Admin\CartController;
use App\Http\Controllers\Admin\VoucherController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\CreditNoteController;
use App\Http\Controllers\Admin\ReturnNoteController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\StoreController;
use App\Http\Controllers\Admin\ProviderController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SaleQuoteController;
use App\Http\Controllers\Admin\FinancialCommitmentController;
use App\Http\Controllers\Admin\FundController;
use App\Http\Controllers\Admin\HistoryController;
use App\Http\Controllers\Admin\InventoryController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\MuController;
use App\Http\Controllers\Admin\OnlineSaleController;
use App\Http\Controllers\Admin\ProviderCommitmentController;
use App\Http\Controllers\Admin\ReturnCouponController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\SMSController;
use App\Http\Controllers\Shop\CouponController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'admin',
], function () {

    Route::get('/login', function () {
        return view('admin.auth.login');
    })->name('admin.login');

    Route::post('/login', [LoginController::class, 'login'])->name('admin.login');

    // Route::get('/forgot-password', function () {
    //     // $delete = DB::table('password_resets')->delete();
    //     return view('admin.auth.passwords.email');
    // })->middleware('guest')->name('admin.password.request');
    // Route::post('/forgot-password', [DealerResetPasswordController::class, 'forgot'])->middleware('guest')->name('admin.password.forgot');
    // Route::get('/reset-password/{token}', function ($token) {
    //     return view('admin.auth.passwords.reset', ['token' => $token]);
    // })->middleware('guest')->name('admin.password.reset');
    // Route::post('/reset-password', [DealerResetPasswordController::class, 'reset'])->middleware('guest')->name('admin.password.update');

    // Route::get('/register', function () {
    //     return view('admin.auth.register');
    // })->name('admin.register');
    // Route::post('/register', [DealerRegisterController::class, 'register'])->name('admin.register');

    Route::get('logout', [LoginController::class, 'logout']);
    Route::get('search', [ProductController::class, 'searchproduct'])->name('admin.product.searchproduct');
    Route::get('show', [ProductController::class, 'showproduct'])->name('admin.product.showproduct');

    Route::group([
        'middleware' => 'auth:web'
    ], function () {

        Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin.home');
        Route::get('/statistic/cart/total/{year}', [App\Http\Controllers\Admin\HomeController::class, 'cartTotalAmountByyearAndMonth'])->name('admin.cartTotalAmountByyearAndMonth');
        Route::get('/profile', [App\Http\Controllers\Admin\UserController::class, 'profile'])->name('admin.profile');
        Route::put('/profile/update', [App\Http\Controllers\Admin\UserController::class, 'updateProfile'])->name('admin.profile.update');

        //Product routes
        Route::group([
            'prefix' => 'product',
        ], function () {
            Route::get('/list', [ProductController::class, 'index'])->name('admin.product.index')->middleware(['can:Liste article']);
            Route::get('/getData', [ProductController::class, 'getData'])->name('admin.product.getData')->middleware(['can:Liste article']);
            Route::get('/create', [ProductController::class, 'create'])->name('admin.product.create')->middleware(['can:Ajouter article']);
            Route::post('/store', [ProductController::class, 'store'])->name('admin.product.store')->middleware(['can:Ajouter article']);
            Route::get('/{product}/edit', [ProductController::class, 'edit'])->name('admin.product.edit')->middleware(['can:Modifier article']);
            Route::put('/{product}/update', [ProductController::class, 'update'])->name('admin.product.update')->middleware(['can:Modifier article']);
            Route::get('/{product}/show', [ProductController::class, 'show'])->name('admin.product.show')->middleware(['can:Détails article']);
            Route::get('/{product}/delete', [ProductController::class, 'delete'])->name('admin.product.delete')->middleware(['can:Supprimer article']);
            Route::get('/{product}/settings', [ProductController::class, 'settings'])->name('admin.product.settings')->middleware(['can:Modifier les paramètres']);
            Route::put('/{product}/updatesettings', [ProductController::class, 'updateSettings'])->name('admin.product.updatesettings')->middleware(['can:Modifier les paramètres']);
            Route::get('/{product}/quantities', [ProductController::class, 'quantities'])->name('admin.product.quantities')->middleware(['can:Modifier les quantités']);
            Route::put('/{product}/updatequantities', [ProductController::class, 'updateQuantities'])->name('admin.product.updatequantities')->middleware(['can:Modifier les quantités']);
            Route::get('/{product}/barcodes', [ProductController::class, 'barcodes'])->name('admin.product.barcodes')->middleware(['can:Modifier les quantités']);
            Route::put('/{product}/updatebarcodes', [ProductController::class, 'updateBarcodes'])->name('admin.product.updatebarcodes')->middleware(['can:Modifier les quantités']);
            Route::get('/{product}/prices', [ProductController::class, 'prices'])->name('admin.product.prices')->middleware(['can:Modifier les prix']);
            Route::put('/{product}/updateprices', [ProductController::class, 'updatePrices'])->name('admin.product.updateprices')->middleware(['can:Modifier les prix']);
            Route::delete('/image/{image}', [ProductController::class, 'deleteImage'])->name('admin.product.deleteimage')->middleware(['can:Modifier article']);
            Route::put('/default/image/{image}', [ProductController::class, 'setDefaultImage'])->name('admin.product.setdefaultimage')->middleware(['can:Modifier article']);
            Route::get('/{product}/statistic/price/{from}/{to}', [App\Http\Controllers\Admin\ProductController::class, 'productPriceEvolution'])->name('admin.productPriceEvolution');
            Route::post('/importationxl', [ProductController::class, 'importationxl'])->name('admin.product.importationxl');
            Route::get('/importation', [ProductController::class, 'importation'])->name('admin.product.importation');
            Route::post('/merge', [ProductController::class, 'merge'])->name('admin.product.merge');
            Route::get('/getAttributeValue', [ProductController::class, 'getAttributeValue'])->name('admin.product.getAttributeValue');
            Route::post('/mergeing', [ProductController::class, 'mergeing'])->name('admin.product.mergeing');
            Route::post('/multipledelete', [ProductController::class, 'multipleDelete'])->name('admin.product.multipledelete')->middleware(['can:Supprimer article']);
        });

        //Provider routes
        Route::group([
            'prefix' => 'provider',
        ], function () {
            Route::get('/list', [ProviderController::class, 'index'])->name('admin.provider.index')->middleware(['can:Liste fournisseur']);
            Route::get('/create', [ProviderController::class, 'create'])->name('admin.provider.create')->middleware(['can:Ajouter fournisseur']);
            Route::post('/store', [ProviderController::class, 'store'])->name('admin.provider.store')->middleware(['can:Ajouter fournisseur']);
            Route::get('/{provider}/edit', [ProviderController::class, 'edit'])->name('admin.provider.edit')->middleware(['can:Modifier fournisseur']);
            Route::put('/{provider}/update', [ProviderController::class, 'update'])->name('admin.provider.update')->middleware(['can:Modifier fournisseur']);
            Route::get('/{provider}/delete', [ProviderController::class, 'delete'])->name('admin.provider.delete')->middleware(['can:Supprimer fournisseur']);
        });

        //Notification routes
        Route::group([
            'prefix' => 'notification',
        ], function () {
            Route::get('', [NotificationController::class, 'index'])->name('admin.notification.index');
            Route::get('/user/{user}/markallasread', [NotificationController::class, 'markAllAsRead'])->name('admin.notification.markallasread');
            Route::get('/user/{user}/markordersasread', [NotificationController::class, 'markOrdersAsRead'])->name('admin.notification.order.markallasread');
        });

        //Setting routes
        Route::group([
            'prefix' => 'setting',
        ], function () {
            Route::get('', [SettingController::class, 'index'])->name('admin.setting.index');
            Route::put('/update', [SettingController::class, 'updateSettings'])->name('admin.setting.update');
            Route::put('/{setting}/update', [SettingController::class, 'updateSetting'])->name('admin.setting.updatesetting');
        });

        //Fund routes
        Route::group([
            'prefix' => 'fund',
        ], function () {
            Route::get('/', [FundController::class, 'fund'])->name('admin.fund.fund')->middleware(['can:Détails caisse']);
            Route::post('/withdraw', [FundController::class, 'withdraw'])->name('admin.fund.withdraw')->middleware(['can:Retirer']);
            Route::get('/withdraw/{withdraw}/delete', [FundController::class, 'deleteWithdraw'])->name('admin.withdraw.delete')->middleware(['can:Supprimer retrait']);
            Route::post('/{withdraw}/EditWithdraw', [FundController::class, 'EditWithdraw'])->name('admin.fund.EditWithdraw')->middleware(['can:Détails caisse']);
        });
        Route::group([
            'prefix' => 'history',
        ], function () {
            Route::get('/', [HistoryController::class, 'history'])->name('admin.history.history')->middleware(['can:Détails caisse']);
            Route::get('/online', [HistoryController::class, 'historyOnline'])->name('admin.history.online')->middleware(['can:Détails caisse']);
        });

        //Role routes
        Route::group([
            'prefix' => 'role',
        ], function () {
            Route::get('/list', [RoleController::class, 'index'])->name('admin.role.index')->middleware(['can:Liste rôle']);
            Route::get('/create', [RoleController::class, 'create'])->name('admin.role.create')->middleware(['can:Ajouter rôle']);
            Route::post('/store', [RoleController::class, 'store'])->name('admin.role.store')->middleware(['can:Ajouter rôle']);
            Route::get('/{role}/edit', [RoleController::class, 'edit'])->name('admin.role.edit')->middleware(['can:Modifier rôle']);
            Route::put('/{role}/update', [RoleController::class, 'update'])->name('admin.role.update')->middleware(['can:Modifier rôle']);
            Route::get('/{role}/delete', [RoleController::class, 'delete'])->name('admin.role.delete')->middleware(['can:Supprimer rôle']);
        });

        //Store routes
        Route::group([
            'prefix' => 'store',
        ], function () {
            Route::get('/list', [StoreController::class, 'index'])->name('admin.store.list')->middleware(['can:Liste point de vente']);
            Route::get('/create', [StoreController::class, 'create'])->name('admin.store.create')->middleware(['can:Ajouter point de vente']);
            Route::post('/store', [StoreController::class, 'store'])->name('admin.store.store')->middleware(['can:Ajouter point de vente']);
            Route::get('/{store}/edit', [StoreController::class, 'edit'])->name('admin.store.edit')->middleware(['can:Modifier point de vente']);
            Route::put('/{store}/update', [StoreController::class, 'update'])->name('admin.store.update')->middleware(['can:Modifier point de vente']);
            Route::get('/{store}/delete', [StoreController::class, 'delete'])->name('admin.store.delete')->middleware(['can:Supprimer point de vente']);
            Route::post('/{store}/storechange', [StoreController::class, 'storechange'])->name('admin.store.storechange');
        });

        //Category routes
        Route::group([
            'prefix' => 'category',
        ], function () {
            Route::get('/list', [CategoryController::class, 'index'])->name('admin.category.list')->middleware(['can:Liste catégorie']);
            Route::get('/create', [CategoryController::class, 'create'])->name('admin.category.create')->middleware(['can:Ajouter catégorie']);
            Route::post('/store', [CategoryController::class, 'store'])->name('admin.category.store')->middleware(['can:Ajouter catégorie']);
            Route::get('/{category}/edit', [CategoryController::class, 'edit'])->name('admin.category.edit')->middleware(['can:Modifier catégorie']);
            Route::put('/{category}/update', [CategoryController::class, 'update'])->name('admin.category.update')->middleware(['can:Modifier catégorie']);
            Route::get('/{category}/delete', [CategoryController::class, 'delete'])->name('admin.category.delete')->middleware(['can:Supprimer catégorie']);
        });

        //Currency routes
        Route::group([
            'prefix' => 'currency',
        ], function () {
            Route::get('/list', [CurrencyController::class, 'index'])->name('admin.currency.list')->middleware(['can:Liste devise']);
            Route::get('/create', [CurrencyController::class, 'create'])->name('admin.currency.create')->middleware(['can:Ajouter devise']);
            Route::post('/store', [CurrencyController::class, 'store'])->name('admin.currency.store')->middleware(['can:Ajouter devise']);
            Route::get('/{currency}/edit', [CurrencyController::class, 'edit'])->name('admin.currency.edit')->middleware(['can:Modifier devise']);
            Route::put('/{currency}/update', [CurrencyController::class, 'update'])->name('admin.currency.update')->middleware(['can:Modifier devise']);
            Route::get('/{currency}/delete', [CurrencyController::class, 'delete'])->name('admin.currency.delete')->middleware(['can:Supprimer devise']);
        });

        //Attribute routes
        Route::group([
            'prefix' => 'attribute',
        ], function () {
            Route::get('/list', [AttributeController::class, 'index'])->name('admin.attribute.list')->middleware(['can:Liste attribut']);
            Route::get('/all', [AttributeController::class, 'getAll'])->name('admin.attribute.all')->middleware(['can:Liste attribut']);
            Route::get('/create', [AttributeController::class, 'create'])->name('admin.attribute.create')->middleware(['can:Ajouter attribut']);
            Route::post('/store', [AttributeController::class, 'store'])->name('admin.attribute.store')->middleware(['can:Ajouter attribut']);
            Route::post('/new', [AttributeController::class, 'new'])->name('admin.attribute.new')->middleware(['can:Ajouter attribut']);
            Route::get('/{attribute}/edit', [AttributeController::class, 'edit'])->name('admin.attribute.edit')->middleware(['can:Modifier attribut']);
            Route::put('/{attribute}/update', [AttributeController::class, 'update'])->name('admin.attribute.update')->middleware(['can:Modifier attribut']);
            Route::get('/{attribute}/delete', [AttributeController::class, 'delete'])->name('admin.attribute.delete')->middleware(['can:Supprimer attribut']);
            Route::get('/{value}/deleteValue', [AttributeController::class, 'deleteValue'])->name('admin.attribute.deleteValue')->middleware(['can:Supprimer attribut']);
            Route::post('/valuevalidation', [AttributeController::class, 'valuevalidation'])->name('admin.attribute.valuevalidation')->middleware(['can:Supprimer attribut']);
            Route::post('/{attribute}/AddValue', [AttributeController::class, 'AddValue'])->name('admin.attribute.AddValue')->middleware(['can:Supprimer attribut']);
            Route::post('/{value}/EditValue', [AttributeController::class, 'EditValue'])->name('admin.attribute.EditValue')->middleware(['can:Supprimer attribut']);
        });

        //Client routes
        Route::group([
            'prefix' => 'client',
        ], function () {
            Route::get('/list', [ClientController::class, 'index'])->name('admin.client.index')->middleware(['can:Liste client']);
            Route::get('/create', [ClientController::class, 'create'])->name('admin.client.create')->middleware(['can:Ajouter client']);
            Route::post('/store', [ClientController::class, 'store'])->name('admin.client.store')->middleware(['can:Ajouter client']);
            Route::get('/{client}/edit', [ClientController::class, 'edit'])->name('admin.client.edit')->middleware(['can:Modifier client']);
            Route::put('/{client}/update', [ClientController::class, 'update'])->name('admin.client.update')->middleware(['can:Modifier client']);
            Route::get('/{client}/delete', [ClientController::class, 'delete'])->name('admin.client.delete')->middleware(['can:Supprimer client']);
            Route::get('/{client}/pricing', [ClientController::class, 'pricing'])->name('admin.client.pricing')->middleware(['can:Supprimer client']);
            Route::post('/validatepricing', [ClientController::class, 'validatepricing'])->name('admin.client.validatepricing');
            Route::post('/{client}/updatepricing', [ClientController::class, 'updatePricing'])->name('admin.client.updatepricing')->middleware(['can:Supprimer client']);
        });

        //Cart routes
        Route::group([
            'prefix' => 'cart',
        ], function () {
            Route::get('/list', [CartController::class, 'index'])->name('admin.cart.index')->middleware(['permission:Liste bon de livraison|Ajouter facture']);
            Route::get('/getData', [CartController::class, 'getData'])->name('admin.cart.getData')->middleware(['permission:Liste bon de livraison|Ajouter facture']);
            Route::post('/{query}/searchByName', [CartController::class, 'searchByName'])->name('admin.searchByName');
            Route::post('/filter', [CartController::class, 'filter'])->name('admin.cart.filter');
            Route::get('/{cart}/show', [CartController::class, 'show'])->name('admin.cart.show')->middleware(['can:Détails bon de livraison']);
            Route::get('/create', [CartController::class, 'create'])->name('admin.cart.create')->middleware(['can:Ajouter bon de livraison']);
            Route::post('/store', [CartController::class, 'store'])->name('admin.cart.store')->middleware(['can:Ajouter bon de livraison']);
            Route::get('/{cart}/edit', [CartController::class, 'edit'])->name('admin.cart.edit')->middleware(['can:Modifier bon de livraison']);
            Route::post('/{cart}/update', [CartController::class, 'update'])->name('admin.cart.update')->middleware(['can:Modifier bon de livraison']);
            Route::get('/{cart}/delete', [CartController::class, 'delete'])->name('admin.cart.delete')->middleware(['can:Supprimer bon de livraison']);
            Route::get('/{cart}/duplicatecart', [CartController::class, 'duplicatecart'])->name('admin.cart.duplicatecart');
            Route::get('/{cart}/CartToSaleQuote', [CartController::class, 'CartToSaleQuote'])->name('admin.cart.CartToSaleQuote');
            Route::post('/{prd}/{price_type}/serach_product', [CartController::class, 'serach_product'])->name('admin.cart.serach_product');
            Route::post('/cartvalidation', [CartController::class, 'cartvalidation'])->name('admin.cart.cartvalidation');
            Route::get('/{cart}/print', [CartController::class, 'print'])->name('admin.cart.print');
            Route::get('/{cart}/printticket', [CartController::class, 'printTicket'])->name('admin.cart.printticket');
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [CartController::class, 'serach_product_c_bar'])->name('admin.cart.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [CartController::class, 'get_product_price'])->name('admin.cart.get_product_price');
            Route::get('/{product}/{client_id}/{price_type}/get_pricing', [CartController::class, 'get_pricing'])->name('admin.cart.get_pricing');
        });
        //inventory routes
        Route::group([
            'prefix' => 'inventory',
        ], function () {
            Route::get('/list', [InventoryController::class, 'index'])->name('admin.inventory.index')->middleware(['can:Liste inventaire']);
            Route::get('/{inventory}/show', [InventoryController::class, 'show'])->name('admin.inventory.show')->middleware(['can:Détails inventaire']);
            Route::get('/create', [InventoryController::class, 'create'])->name('admin.inventory.create')->middleware(['can:Ajouter inventaire']);
            Route::post('/store', [InventoryController::class, 'store'])->name('admin.inventory.store')->middleware(['can:Ajouter inventaire']);
            Route::get('/{inventory}/edit', [InventoryController::class, 'edit'])->name('admin.inventory.edit')->middleware(['can:Modifier inventaire']);
            Route::post('/{inventory}/update', [InventoryController::class, 'update'])->name('admin.inventory.update')->middleware(['can:Modifier inventaire']);
            Route::get('/{inventory}/delete', [InventoryController::class, 'delete'])->name('admin.inventory.delete')->middleware(['can:Supprimer inventaire']);
            Route::post('/inventoryvalidation', [InventoryController::class, 'inventoryvalidation'])->name('admin.inventory.inventoryvalidation')->middleware(['can:Ajouter inventaire']);
            Route::get('/{inventory}/print', [InventoryController::class, 'print'])->name('admin.inventory.print')->middleware(['can:Imprimer inventaire']);
            Route::get('/{inventory}/printticket', [InventoryController::class, 'printTicket'])->name('admin.inventory.printticket')->middleware(['can:Imprimer inventaire']);
        });
        //onlinesale routes
        Route::group([
            'prefix' => 'onlinesale',
        ], function () {
            Route::get('/list', [OnlineSaleController::class, 'index'])->name('admin.onlinesale.index');
            Route::get('/{onlinesale}/show', [OnlineSaleController::class, 'show'])->name('admin.onlinesale.show');
            Route::get('/create', [OnlineSaleController::class, 'create'])->name('admin.onlinesale.create');
            Route::post('/store', [OnlineSaleController::class, 'store'])->name('admin.onlinesale.store');
            Route::get('/{onlinesale}/edit', [OnlineSaleController::class, 'edit'])->name('admin.onlinesale.edit');
            Route::post('/{onlinesale}/update', [OnlineSaleController::class, 'update'])->name('admin.onlinesale.update');
            Route::post('/{onlinesale}/{status}/statuschange', [OnlineSaleController::class, 'statuschange'])->name('admin.onlinesale.statuschange');
            Route::get('/{onlinesale}/delete', [OnlineSaleController::class, 'delete'])->name('admin.onlinesale.delete');
            Route::post('/onlinesalevalidation', [OnlineSaleController::class, 'onlinesalevalidation'])->name('admin.onlinesale.onlinesalevalidation');
            Route::get('/{onlinesale}/print', [OnlineSaleController::class, 'print'])->name('admin.onlinesale.print');
            Route::get('/{onlinesale}/printticket', [OnlineSaleController::class, 'printTicket'])->name('admin.onlinesale.printticket');
            Route::get('/getcities', [OnlineSaleController::class, 'getcities'])->name('admin.onlinesale.getcities');
        });
        //Invoice routes
        Route::group([
            'prefix' => 'invoice',
        ], function () {
            Route::get('/list', [InvoiceController::class, 'index'])->name('admin.invoice.index')->middleware(['permission:Liste facture|Ajouter facture avoir']);
            Route::get('/{invoice}/show', [InvoiceController::class, 'show'])->name('admin.invoice.show')->middleware(['can:Détails facture']);
            // Route::get('/{invoice}/edit', [InvoiceController::class, 'edit'])->name('admin.invoice.edit')->middleware(['role:Admin']);;
            // Route::post('/{invoice}/update', [InvoiceController::class, 'update'])->name('admin.invoice.update')->middleware(['role:Admin']);;
            Route::get('/{invoice}/pdf', [InvoiceController::class, 'pdf2'])->name('admin.invoice.pdf2')->middleware(['can:Imprimer facture']);
            Route::get('/{invoice}/pdf/{type}', [InvoiceController::class, 'pdf'])->name('admin.invoice.pdf')->middleware(['can:Imprimer facture']);
            // Route::get('/create', [InvoiceController::class, 'create'])->name('admin.invoice.create')->middleware(['can:Liste facture']);
            Route::post('/store', [InvoiceController::class, 'store'])->name('admin.invoice.store')->middleware(['can:Ajouter facture']);
            Route::get('/{invoice}/printras', [InvoiceController::class, 'printras'])->name('admin.invoice.printras')->middleware(['can:Imprimer facture']);
            Route::get('/generated/{invoice}/edit', [InvoiceController::class, 'editGenerated'])->name('admin.invoice.edit.generated')->middleware(['can:Modifier facture']);
            Route::post('/generated/{invoice}/update', [InvoiceController::class, 'updateGenerated'])->name('admin.invoice.update.generated')->middleware(['can:Modifier facture']);
        });
        Route::group([
            'prefix' => 'creditnote',
        ], function () {
            Route::get('/list', [CreditNoteController::class, 'index'])->name('admin.creditnote.index')->middleware(['can:Liste facture avoir']);
            Route::get('/{creditnote}/show', [CreditNoteController::class, 'show'])->name('admin.creditnote.show')->middleware(['can:Détails facture avoir']);
            Route::get('/{creditnote}/pdf', [CreditNoteController::class, 'pdf'])->name('admin.creditnote.pdf')->middleware(['can:Imprimer facture avoir']);
            Route::get('/{invoice}/create', [CreditNoteController::class, 'create'])->name('admin.creditnote.create')->middleware(['can:Ajouter facture avoir']);
            Route::post('/validation', [CreditNoteController::class, 'validation'])->name('admin.creditnote.validation')->middleware(['can:Ajouter facture avoir']);
            Route::post('/store', [CreditNoteController::class, 'store'])->name('admin.creditnote.store')->middleware(['can:Ajouter facture avoir']);
            Route::get('/{creditnote}/print', [CreditNoteController::class, 'pdf'])->name('admin.creditnote.print')->middleware(['can:Imprimer facture avoir']);


        });

        //Bon de retour BL routes
        Route::group([
            'prefix' => 'returnnote',
        ], function () {
            Route::get('/list', [ReturnNoteController::class, 'index'])->name('admin.returnnote.index')->middleware(['can:Liste bon de retour BL']);
            Route::get('/{returnnote}/show', [ReturnNoteController::class, 'show'])->name('admin.returnnote.show')->middleware(['can:Détails bon de retour BL']);
            Route::get('/{returnnote}/print', [ReturnNoteController::class, 'print'])->name('admin.returnnote.print')->middleware(['can:Imprimer bon de retour BL']);
            Route::get('/{cart}/create', [ReturnNoteController::class, 'create'])->name('admin.returnnote.create')->middleware(['can:Ajouter bon de retour BL']);
            Route::post('/validation', [ReturnNoteController::class, 'validation'])->name('admin.returnnote.validation')->middleware(['can:Ajouter bon de retour BL']);
            Route::post('/store', [ReturnNoteController::class, 'store'])->name('admin.returnnote.store')->middleware(['can:Ajouter bon de retour BL']);
        });

        Route::group([
            'prefix' => 'financialcommitment',
        ], function () {
            Route::get('/list', [FinancialCommitmentController::class, 'index'])->name('admin.financialcommitment.index')->middleware(['can:Liste encaissement']);
            Route::get('/{cart}/show', [FinancialCommitmentController::class, 'show'])->name('admin.financialcommitment.show')->middleware(['can:Détails encaissement']);
            Route::get('/{commitment}/showcommitment', [FinancialCommitmentController::class, 'showCommitment'])->name('admin.financialcommitment.showcommitment')->middleware(['can:Détails échéance encaissement']);
            Route::get('/create', [FinancialCommitmentController::class, 'create'])->name('admin.financialcommitment.create')->middleware(['can:Ajouter encaissement']);
            Route::post('/store', [FinancialCommitmentController::class, 'store'])->name('admin.financialcommitment.store')->middleware(['can:Ajouter encaissement']);
            Route::get('/{cart}/edit', [FinancialCommitmentController::class, 'edit'])->name('admin.financialcommitment.edit')->middleware(['can:Modifier encaissement']);
            Route::post('/{commitment}/update', [FinancialCommitmentController::class, 'update'])->name('admin.financialcommitment.update')->middleware(['can:Modifier encaissement']);
            Route::post('/{cart}/updatecommitments', [FinancialCommitmentController::class, 'updateCommitments'])->name('admin.financialcommitment.updatecommitments')->middleware(['can:Modifier encaissement']);
            Route::get('/{cart}/delete', [FinancialCommitmentController::class, 'delete'])->name('admin.financialcommitment.delete')->middleware(['can:Supprimer encaissement']);
            Route::get('/{commitment}/showreturn', [FinancialCommitmentController::class, 'showReturn'])->name('admin.creditnotecommitment.show')->middleware(['can:Détails encaissement']);
            Route::get('/{commitment}/deletereturn', [FinancialCommitmentController::class, 'deleteReturn'])->name('admin.creditnotecommitment.delete')->middleware(['can:Supprimer encaissement']);
            Route::post('/paiementvalidation', [FinancialCommitmentController::class, 'paiementvalidation'])->name('admin.financialcommitment.paiementvalidation');
            Route::get('/payment/{payment}/delete', [FinancialCommitmentController::class, 'deletePayment'])->name('admin.financialcommitment.payment.delete')->middleware(['can:Supprimer encaissement']);

        });


        //Order routes
        Route::group([
            'prefix' => 'order',
        ], function () {
            Route::get('/list', [OrderController::class, 'index'])->name('admin.order.index')->middleware(['can:Liste commande']);
            Route::get('/{order}/show', [OrderController::class, 'show'])->name('admin.order.show')->middleware(['can:Détails commande']);
            Route::get('/create', [OrderController::class, 'create'])->name('admin.order.create')->middleware(['permission:Générer commande|Ajouter commande']);
            Route::post('/store', [OrderController::class, 'store'])->name('admin.order.store')->middleware(['permission:Générer commande|Ajouter commande']);
            Route::get('/{order}/edit', [OrderController::class, 'edit'])->name('admin.order.edit')->middleware(['can:Modifier commande']);
            Route::post('/{order}/update', [OrderController::class, 'update'])->name('admin.order.update')->middleware(['can:Modifier commande']);
            Route::get('/{order}/delete', [OrderController::class, 'delete'])->name('admin.order.delete')->middleware(['can:Supprimer commande']);
            Route::post('/{prd}/{price_type}/serach_product', [OrderController::class, 'serach_product'])->name('admin.order.serach_product');
            Route::post('/ordervalidation', [OrderController::class, 'ordervalidation'])->name('admin.order.ordervalidation');
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [OrderController::class, 'serach_product_c_bar'])->name('admin.order.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [OrderController::class, 'get_product_price'])->name('admin.order.get_product_price');
            Route::get('/{order}/print', [OrderController::class, 'print'])->name('admin.order.print')->middleware(['can:imprimer commande']);
            Route::get('/{order}/printticket', [OrderController::class, 'printTicket'])->name('admin.order.printticket');
            Route::post('/{order}/{status}/statuschange', [OrderController::class, 'statuschange'])->name('admin.order.statuschange');


        });

        //salequote routes
        Route::group([
            'prefix' => 'salequote',
        ], function () {
            Route::get('/list', [SaleQuoteController::class, 'index'])->name('admin.salequote.index')->middleware(['can:Liste devis']);
            Route::get('/{salequote}/show', [SaleQuoteController::class, 'show'])->name('admin.salequote.show')->middleware(['can:Détails devis']);
            Route::get('/create', [SaleQuoteController::class, 'create'])->name('admin.salequote.create')->middleware(['can:Ajouter devis']);
            Route::post('/store', [SaleQuoteController::class, 'store'])->name('admin.salequote.store')->middleware(['can:Ajouter devis']);
            Route::get('/{salequote}/edit', [SaleQuoteController::class, 'edit'])->name('admin.salequote.edit')->middleware(['can:Modifier devis']);
            Route::post('/{salequote}/update', [SaleQuoteController::class, 'update'])->name('admin.salequote.update')->middleware(['can:Modifier devis']);
            Route::get('/{salequote}/delete', [SaleQuoteController::class, 'delete'])->name('admin.salequote.delete')->middleware(['can:Supprimer devis']);
            Route::post('/{prd}/{price_type}/serach_product', [SaleQuoteController::class, 'serach_product'])->name('admin.salequote.serach_product');
            Route::post('/salequotevalidation', [SaleQuoteController::class, 'salequotevalidation'])->name('admin.salequote.salequotevalidation');
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [SaleQuoteController::class, 'serach_product_c_bar'])->name('admin.salequote.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [SaleQuoteController::class, 'get_product_price'])->name('admin.salequote.get_product_price');
            Route::get('/{salequote}/print', [SaleQuoteController::class, 'print'])->name('admin.salequote.print')->middleware(['can:Imprimer devis']);
        });

        //User routes
        Route::group([
            'prefix' => 'user',
        ], function () {
            Route::get('/list', [UserController::class, 'index'])->name('admin.user.index')->middleware(['can:Liste utilisateur']);
            Route::get('/create', [UserController::class, 'create'])->name('admin.user.create')->middleware(['can:Ajouter utilisateur']);
            Route::post('/store', [UserController::class, 'store'])->name('admin.user.store')->middleware(['can:Ajouter utilisateur']);
            Route::get('/{user}/edit', [UserController::class, 'edit'])->name('admin.user.edit')->middleware(['can:Modifier utilisateur']);
            Route::put('/{user}/update', [UserController::class, 'update'])->name('admin.user.update')->middleware(['can:Modifier utilisateur']);
            Route::get('/{user}/delete', [UserController::class, 'delete'])->name('admin.user.delete')->middleware(['can:Supprimer utilisateur']);
        });

        //User routes
        Route::group([
            'prefix' => 'coupon',
        ], function () {
            Route::get('/list', [CouponController::class, 'index'])->name('admin.coupon.index')->middleware(['can:Liste coupon']);
            Route::get('/create', [CouponController::class, 'create'])->name('admin.coupon.create')->middleware(['can:Ajouter coupon']);
            Route::post('/store', [CouponController::class, 'store'])->name('admin.coupon.store')->middleware(['can:Ajouter coupon']);
            Route::get('/{coupon}/edit', [CouponController::class, 'edit'])->name('admin.coupon.edit')->middleware(['can:Modifier coupon']);
            Route::put('/{coupon}/update', [CouponController::class, 'update'])->name('admin.coupon.update')->middleware(['can:Modifier coupon']);
            Route::get('/{coupon}/delete', [CouponController::class, 'delete'])->name('admin.coupon.delete')->middleware(['can:Supprimer coupon']);
        });

        //Voucher (Bon d'achat) routes
        Route::group([
            'prefix' => 'voucher',
        ], function () {
            Route::get('/list', [VoucherController::class, 'index'])->name('admin.voucher.index')->middleware(['can:Liste bon d\'achat']);
            Route::get('/{voucher}/show', [VoucherController::class, 'show'])->name('admin.voucher.show')->middleware(['can:Détails bon d\'achat']);
            Route::get('/create', [VoucherController::class, 'create'])->name('admin.voucher.create')->middleware(['can:Ajouter bon d\'achat']);
            Route::post('/store', [VoucherController::class, 'store'])->name('admin.voucher.store')->middleware(['can:Ajouter bon d\'achat']);
            Route::get('/{voucher}/edit', [VoucherController::class, 'edit'])->name('admin.voucher.edit')->middleware(['can:Modifier bon d\'achat']);
            Route::post('/{voucher}/update', [VoucherController::class, 'update'])->name('admin.voucher.update')->middleware(['can:Modifier bon d\'achat']);
            Route::get('/{voucher}/delete', [VoucherController::class, 'delete'])->name('admin.voucher.delete')->middleware(['can:Supprimer bon d\'achat']);
            Route::post('/{prd}/{price_type}/serach_product', [VoucherController::class, 'serach_product'])->name('admin.voucher.serach_product');
            Route::post('/vouchervalidation', [VoucherController::class, 'vouchervalidation'])->name('admin.voucher.vouchervalidation');
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [VoucherController::class, 'serach_product_c_bar'])->name('admin.voucher.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [VoucherController::class, 'get_product_price'])->name('admin.voucher.get_product_price');
            Route::get('/{voucher}/print', [VoucherController::class, 'print'])->name('admin.voucher.print')->middleware(['can:Imprimer bon d\'achat']);
            Route::get('/{voucher}/printticket', [VoucherController::class, 'printTicket'])->name('admin.voucher.printticket')->middleware(['can:Imprimer bon d\'achat']);
        });

        //Return coupon (Bon de retour) routes
        Route::group([
            'prefix' => 'returncoupon',
        ], function () {
            Route::get('/list', [ReturnCouponController::class, 'index'])->name('admin.returncoupon.index')->middleware(['can:Liste bon de retour']);
            Route::get('/{returncoupon}/show', [ReturnCouponController::class, 'show'])->name('admin.returncoupon.show')->middleware(['can:Détails bon de retour']);
            Route::get('/create', [ReturnCouponController::class, 'create'])->name('admin.returncoupon.create')->middleware(['can:Ajouter bon de retour']);
            Route::post('/store', [ReturnCouponController::class, 'store'])->name('admin.returncoupon.store')->middleware(['can:Ajouter bon de retour']);
            Route::get('/{returncoupon}/edit', [ReturnCouponController::class, 'edit'])->name('admin.returncoupon.edit')->middleware(['can:Modifier bon de retour']);
            Route::post('/{returncoupon}/update', [ReturnCouponController::class, 'update'])->name('admin.returncoupon.update')->middleware(['can:Modifier bon de retour']);
            Route::get('/{returncoupon}/delete', [ReturnCouponController::class, 'delete'])->name('admin.returncoupon.delete')->middleware(['can:Supprimer bon de retour']);
            Route::post('/{prd}/{price_type}/serach_product', [ReturnCouponController::class, 'serach_product'])->name('admin.returncoupon.serach_product');
            Route::post('/validation', [ReturnCouponController::class, 'returncouponvalidation'])->name('admin.returncoupon.returncouponvalidation');
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [ReturnCouponController::class, 'serach_product_c_bar'])->name('admin.returncoupon.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [ReturnCouponController::class, 'get_product_price'])->name('admin.returncoupon.get_product_price');
            Route::get('/{returncoupon}/print', [ReturnCouponController::class, 'print'])->name('admin.returncoupon.print')->middleware(['can:Imprimer bon de retour']);
        });

        //Purchase payments routes
        Route::group([
            'prefix' => 'providercommitment',
        ], function () {
            Route::get('/list', [ProviderCommitmentController::class, 'index'])->name('admin.providercommitment.index')->middleware(['can:Liste décaissement']);
            Route::get('/{voucher}/show', [ProviderCommitmentController::class, 'show'])->name('admin.providercommitment.show')->middleware(['can:Détails décaissement']);
            Route::get('/{commitment}/showcommitment', [ProviderCommitmentController::class, 'showCommitment'])->name('admin.providercommitment.showcommitment')->middleware(['can:Détails échéance décaissement']);
            Route::get('/create', [ProviderCommitmentController::class, 'create'])->name('admin.providercommitment.create')->middleware(['can:Ajouter décaissement']);
            Route::post('/store', [ProviderCommitmentController::class, 'store'])->name('admin.providercommitment.store')->middleware(['can:Ajouter décaissement']);
            Route::get('/{voucher}/edit', [ProviderCommitmentController::class, 'edit'])->name('admin.providercommitment.edit')->middleware(['can:Modifier décaissement']);
            Route::post('/{commitment}/update', [ProviderCommitmentController::class, 'update'])->name('admin.providercommitment.update')->middleware(['can:Modifier décaissement']);
            Route::post('/{voucher}/updatecommitments', [ProviderCommitmentController::class, 'updateCommitments'])->name('admin.providercommitment.updatecommitments')->middleware(['can:Modifier décaissement']);
            Route::get('/{voucher}/delete', [ProviderCommitmentController::class, 'delete'])->name('admin.providercommitment.delete')->middleware(['can:Supprimer décaissement']);
            Route::get('/{commitment}/showreturn', [ProviderCommitmentController::class, 'showReturn'])->name('admin.creditnotecommitment.show')->middleware(['can:Détails décaissement']);
            Route::get('/{commitment}/deletereturn', [ProviderCommitmentController::class, 'deleteReturn'])->name('admin.creditnotecommitment.delete')->middleware(['can:Supprimer décaissement']);
            Route::post('/paiementvalidation', [ProviderCommitmentController::class, 'paiementvalidation'])->name('admin.providercommitment.paiementvalidation');
            Route::get('/payment/{payment}/delete', [ProviderCommitmentController::class, 'deletePayment'])->name('admin.providercommitment.payment.delete')->middleware(['can:Supprimer décaissement']);
        });

        //MU routes
        Route::group([
            'prefix' => 'mu',
        ], function () {
            Route::get('/list/{type?}', [MuController::class, 'index'])->name('admin.mu.index')->middleware(['permission:Liste bon de sortie|Liste bon de commande|Liste bon de réception']);
            Route::get('/list2/{type?}', [MuController::class, 'indexadmin'])->name('admin.mu.indexadmin')->middleware(['permission:Liste bon de sortie|Liste bon de commande|Liste bon de réception']);
            Route::get('/{mu}/show', [MuController::class, 'show'])->name('admin.mu.show')->middleware(['permission:Détails bon de sortie|Détails bon de commande|Détails bon de réception']);
            Route::get('/create/{type?}', [MuController::class, 'create'])->name('admin.mu.create')->middleware(['permission:Ajouter bon de sortie|Ajouter bon de commande|Ajouter bon de réception']);
            Route::post('/store/{type?}', [MuController::class, 'store'])->name('admin.mu.store')->middleware(['permission:Ajouter bon de sortie|Ajouter bon de commande|Ajouter bon de réception']);
            Route::get('/{mu}/edit', [MuController::class, 'edit'])->name('admin.mu.edit')->middleware(['permission:Modifier bon de sortie|Modifier bon de commande|Modifier bon de réception']);
            Route::post('/{mu}/update', [MuController::class, 'update'])->name('admin.mu.update')->middleware(['permission:Modifier bon de sortie|Modifier bon de commande|Modifier bon de réception']);
            Route::get('/{mu}/{type?}/updatestatus', [MuController::class, 'updatestatus'])->name('admin.mu.updatestatus')->middleware(['permission:Modifier bon de sortie|Modifier bon de commande|Modifier bon de réception']);
            Route::get('/{mu}/delete', [MuController::class, 'delete'])->name('admin.mu.delete')->middleware(['permission:Supprimer bon de sortie|Supprimer bon de commande|Supprimer bon de réception']);
            Route::post('/{prd}/{store}/{type?}/serach_product', [MuController::class, 'serach_product'])->name('admin.mu.serach_product');
            Route::post('/muvalidation', [MuController::class, 'muvalidation'])->name('admin.mu.muvalidation');
            Route::get('/{mu}/print', [MuController::class, 'print'])->name('admin.mu.print')->middleware(['permission:Imprimer bon de sortie|Imprimer bon de commande|Imprimer bon de réception']);;
            Route::post('/{prd}/{price_type}/serach_product_c_bar', [MuController::class, 'serach_product_c_bar'])->name('admin.mu.serach_product_c_bar');
            Route::post('/{prd}/{price_type}/{attr1}/{attr2}/get_product_price', [MuController::class, 'get_product_price'])->name('admin.mu.get_product_price');
        });
        //Slider routes
        Route::group([
            'prefix' => 'slider',
        ], function () {
            Route::get('/list', [SliderController::class, 'index'])->name('admin.slider.index');
            Route::get('/create', [SliderController::class, 'create'])->name('admin.slider.create');
            Route::post('/store', [SliderController::class, 'store'])->name('admin.slider.store');
            Route::get('/{slider}/edit', [SliderController::class, 'edit'])->name('admin.slider.edit');
            Route::put('/{slider}/update', [SliderController::class, 'update'])->name('admin.slider.update');
            Route::get('/{slider}/delete', [SliderController::class, 'delete'])->name('admin.slider.delete');
        });
        //Testimonial routes

        Route::group([
            'prefix' => 'testimonial',
        ], function () {
            Route::get('/list', [TestimonialController::class, 'index'])->name('admin.testimonial.index');
            Route::get('/create', [TestimonialController::class, 'create'])->name('admin.testimonial.create');
            Route::post('/store', [TestimonialController::class, 'store'])->name('admin.testimonial.store');
            Route::post('/testimonialvalidation', [TestimonialController::class, 'testimonialvalidation'])->name('admin.testimonial.testimonialvalidation');
            Route::get('/{testimonial}/edit', [TestimonialController::class, 'edit'])->name('admin.testimonial.edit');
            Route::put('/{testimonial}/update', [TestimonialController::class, 'update'])->name('admin.testimonial.update');
            Route::get('/{testimonial}/delete', [TestimonialController::class, 'delete'])->name('admin.testimonial.delete');
        });
        //Partner routes

        Route::group([
            'prefix' => 'partner',
        ], function () {
            Route::get('/list', [PartnerController::class, 'index'])->name('admin.partner.index');
            Route::get('/create', [PartnerController::class, 'create'])->name('admin.partner.create');
            Route::post('/store', [PartnerController::class, 'store'])->name('admin.partner.store');
            Route::post('/partnervalidation', [PartnerController::class, 'partnervalidation'])->name('admin.partner.partnervalidation');
            Route::get('/{partner}/edit', [PartnerController::class, 'edit'])->name('admin.partner.edit');
            Route::put('/{partner}/update', [PartnerController::class, 'update'])->name('admin.partner.update');
            Route::get('/{partner}/delete', [PartnerController::class, 'delete'])->name('admin.partner.delete');
        });

        //SMS routes
        Route::group([
            'prefix' => 'sms',
        ], function () {
            Route::get('/send', [SMSController::class, 'create'])->name('sms.create')->middleware(['permission:Ajouter sms']);
            Route::post('/send', [SMSController::class, 'store'])->name('sms.store')->middleware(['permission:Ajouter sms']);
        });
    });
});