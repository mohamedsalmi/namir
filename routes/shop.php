<?php

// Use statements
use App\Helpers\Helper;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Shop\Auth\ShopLoginController;
use App\Http\Controllers\Shop\Auth\ShopRegisterController;
use App\Http\Controllers\Shop\Auth\ShopResetPasswordController;
use App\Http\Controllers\Shop\HomeController;
use App\Http\Controllers\Shop\ShopCartController;
use App\Http\Controllers\Shop\ShopController;
use App\Http\Controllers\Shop\WishlistController;
use App\Models\Currency;
use App\Models\OnlineSale;
use App\Models\Order;
use App\Models\Product;
use App\Models\Region;
use App\Models\Testimonial;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;



//$default = \App\Models\Currency::where('value', 1)->first();
//if (!Session::has('currency')) {
//    session()->put('currency', $default);
//}
//$value = Helper::get_currency_value();
//session()->put('currency_value', $value);
//

// Route definition
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    // Home page route
    Route::get('/', [HomeController::class, 'index'])->name('shop.home');

    // Contact Us route
    Route::get('/contact-us', function () {
        $testimonials = Testimonial::all();
        return view('shop.contact_us');
    })->name('shop.contact_us');

    // About Us route
    Route::get('/about-us', function () {
        $testimonials = Testimonial::all();
        return view('shop.about_us', compact('testimonials'));
    })->name('shop.about_us');

    // Terms route
    Route::get('/terms', function () {
        return view('shop.terms');
    })->name('shop.terms');

    // Login route
    Route::get('/login', [ShopLoginController::class, 'showLoginForm'])->name('shop.login');
    Route::post('/login', [ShopLoginController::class, 'login'])->name('shop.login');
    Route::get('auth/{provider}', [ShopLoginController::class,'redirectToProvider'])->name('shop.provider.redirect');
    Route::get('/callback/{provider}/', [ShopLoginController::class,'handleProviderCallback'])->name('shop.provider.handel');



    // Forgot password route
    Route::get('/forgot-password', function () {
        return view('shop.auth.passwords.email');
    })->middleware('guest')->name('shop.password.request');

    Route::post('/forgot-password', [ShopResetPasswordController::class, 'forgot'])->middleware('guest')->name('shop.password.forgot');

    Route::get('/reset-password/{token}', function ($token) {
        return view('shop.auth.passwords.reset', ['token' => $token]);
    })->middleware('guest')->name('shop.password.reset');
    
    Route::post('/reset-password', [ShopResetPasswordController::class, 'reset'])->middleware('guest')->name('shop.password.update');

    // Register route
    Route::post('/register', [ShopRegisterController::class, 'register'])->name('shop.account.create');
    Route::get('/register', function () {
        return view('shop.auth.register');
    })->name('shop.register');

    // Logout route
    Route::get('logout', [ShopLoginController::class, 'logout'])->name('shop.logout');

    Route::get('/get-hot-deals', function () {
        return Helper::get_hot_deals();
    });

    Route::get('/HotDeal', 'HomeController@hotDeals')->name('shop.HotDeal');


    Route::get('/quick-view-modal/{product_id?}', function ($product_id = null) {
        return view('shop.partials.modals.quick-view-modal', compact('product_id'));
    })->name('shop.quick-view-modal');

    Route::post('/send-mail', [App\Http\Controllers\Shop\HomeController::class, 'send_mail'])->name('shop.send_mail');
    Route::group(['prefix' => 'shop'], function () {
        Route::get('/', [App\Http\Controllers\Shop\ShopController::class, 'index'])->name('shop.index');
        Route::get('/ajax/{ajax?}', [App\Http\Controllers\Shop\ShopController::class, 'index'])->name('shop.index');
        Route::get('/show/{product}', [App\Http\Controllers\Shop\ShopController::class, 'show'])->name('shop.show');

        Route::group(['prefix' => 'cart'], function () {
            Route::get('/', [App\Http\Controllers\Shop\ShopCartController::class, 'index'])->name('shop.cart.index');
            Route::get('/checkout', [App\Http\Controllers\Shop\ShopCartController::class, 'checkout'])->name('shop.cart.checkout');
            Route::get('/order/success', [App\Http\Controllers\Shop\ShopCartController::class, 'orderSuccess'])->name('shop.cart.order.success');
            Route::post('/order', [App\Http\Controllers\Shop\ShopCartController::class, 'order'])->name('shop.cart.order');
            Route::get('/coupon/check', [App\Http\Controllers\Shop\CouponController::class, 'checkCoupon'])->name('shop.coupon.check');
            Route::get('/addtocart', [App\Http\Controllers\Shop\ShopCartController::class, 'addToCart'])->name('shop.cart.addtocart');
            Route::get('/removecartitem', [App\Http\Controllers\Shop\ShopCartController::class, 'deleteCartItem'])->name('shop.cart.removecartitem');
            Route::get('/item-section', 'ShopCartController@load_item_section');
        });
    });

    Route::post('/set-currency', [App\Http\Controllers\Admin\CurrencyController::class, 'setCurrency'])->name('shop.setCurrency');

    // get-cart-data
    Route::get('/get-cart-data', function () {
        if (!Session::has('cart') || count(Session::get('cart')) == 0) {
            return response()->json([            'message' => 'Cart is empty'        ], 200);
        }
    
        return response()->json([        'cart' => array_values(Session::get('cart'))    ], 200);
    });
    
    


    Route::group([
        'middleware' => 'auth:shop'
    ], function () {
        Route::get('/profile', function () {
            $nav = 'profile';
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            return view('shop.profile', compact('nav', 'states', 'cities'));
        })->name('shop.profile');
        Route::get('/dashboard', function () {
            $nav = 'dashboard';
            $orders_number = OnlineSale::where('client_id', auth()->user()->id)->count();
            $confirmed_orders_number = OnlineSale::where('client_id', auth()->user()->id)->where('status', 'livré')->count();
            $canceled_orders_number = OnlineSale::where('client_id', auth()->user()->id)->where('status', 'annulé')->count();
            $wishlists_number = Wishlist::where(['client_id' => auth()->user()->id])->count();
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            return view('shop.profile', compact('nav', 'orders_number', 'confirmed_orders_number', 'canceled_orders_number', 'wishlists_number', 'states', 'cities'));
        })->name('shop.dashboard');
        Route::get('/wishlist', function () {
            $nav = 'wishlist';
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            $wishlists = Wishlist::where(['client_id' => auth()->user()->id])->get();
            return view('shop.profile', compact('nav', 'wishlists', 'states', 'cities'));
        })->name('shop.wishlist');
        Route::get('/order', function () {
            $nav = 'order';
            $orders = OnlineSale::where('client_id', auth()->user()->id)->get();
            $states = Region::groupBy('state')->get();
            $cities = Region::groupBy('city')->get();
            return view('shop.profile', compact('nav', 'orders', 'states', 'cities'));
        })->name('shop.order');
        Route::get('/order/{order}/show', function ($id) {
            $order = OnlineSale::where('id', $id)->where('client_id', auth()->user()->id)->first();
            if ($order) {
                return view('shop.order', compact('order'));
            } else {
                return  abort(404);
            }
        })->name('shop.order.show');
        Route::post('/updateProfile', [ClientController::class, 'updateProfile'])->name('shop.client.updateProfile');
        Route::post('/valdationprofile', [ClientController::class, 'valdationprofile'])->name('shop.client.valdationprofile');
        Route::post('/avatarchange', [ClientController::class, 'avatarchange'])->name('shop.client.avatarchange');
        Route::get('/order/{order}/tracking', function ($id) {
            $order = OnlineSale::where('id', $id)->where('client_id', auth()->user()->id)->first();
            return view('shop.order_tracking', compact('order'));
        })->name('shop.order.tracking');
        Route::post('/{product}/addwishlist', [WishlistController::class, 'addwishlist'])->name('shop.addwishlist');
        Route::get('/{wishlist}/delete', [WishlistController::class, 'delete'])->name('shop.wishlist.delete');
    });
})->middleware('set.session');
