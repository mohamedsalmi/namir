<?php

return [
    'order'=>'Commande',
    'success'=>'réussie',
    'writer'=>'Écrivain',
    'articles'=>'Article',
    'shipping_address'=>'Adresse de livraison',
    'house'=>'Maison d\'édition',
    'track_order'=>'Suivre la commande',
    'expected_date'=>'Date de livraison prévue',
    'price_details'=>'Détails du prix',
    'order_id'=>'Identifiant de la transaction',
    'success_paiement'=>'Le paiement a bien été effectué et votre commande est en route',
];