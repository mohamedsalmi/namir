<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Ces identifiants ne correspondent pas à nos enregistrements.',
    'password' => 'Le mot de passe est incorrect.',
    'throttle' => 'Tentatives de connexion trop nombreuses. Veuillez essayer de nouveau dans :seconds secondes.',

    'forgot' => [
        'forgot_password' => 'Mot de passe oublié',
        'send' => 'Envoyer',        
        'email' => 'Adresse email',
    ],

    'reset' => [
        'request_reset_password' => 'Vous avez demandé la réinitialisation de votre mot de passe',
        'request_reset_message' => 'Ce lien de réinitialisation de mot de passe expirera dans 60 minutes. Si vous n\'avez pas demandé de réinitialisation de mot de passe, aucune autre action n\'est requise.',
        'reset_password' => 'Réinitialiser le mot de passe',
        'password' => 'Mot de passe',
        'password_confirmation' => 'Confirmer mot de passe',
    ],

];
