<?php

return [
    'order'=>'Passer commande',
    'order_summary' => 'Récapitulatif de la commande',
    'shipping' => 'Livraison',
    'delivery_address' => 'Adresse de livraison',
    'name' => 'Nom ',
    'phone' => 'Téléphone',
    'address' => 'Adresse',
    'delivery_options' => 'Options de livraison',
    'store_delivery_option' => 'Livraison en magasin',
    'home_delivery_option' => 'Livraison à domicile',
    'payment_options' => 'Options de paiement',
    'cash_on_delivery' => 'Paiement à la livraison', 
    'cash_on_delivery_details' => 'Payez votre commande au moment de la livraison', 
    'choose_option' => 'Choisir une option', 
    'state' => 'Gouvernorat', 
    'city' => 'Délégation', 

];