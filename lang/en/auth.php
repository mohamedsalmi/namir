<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'=>[
        'or'=>'Or',
        'login_with_google'=>'Log In with Google',
        'login_with_facebook'=>'Log In with Facebook',

    ],

    'forgot' => [
        'forgot_password' => 'Forgot Password',
        'send' => 'Send',
        'email' => 'Email address',
    ],

    'reset' => [
        'request_reset_password' => 'You have requested to reset your password',
        'request_reset_message' => 'This password reset link will expire in 60 minutes.If you did not request a password reset, no further action is required.',
        'reset_password' => 'Reset password',
        'password' => 'Password',
        'password_confirmation' => 'Password confirmation',
        
    ],

];
