<?php

return [
    'order'=>'Place order',
    'order_summary' => 'Order summary',
    'shipping' => 'Shipping',
    'delivery_address' => 'Delivery address',
    'name' => 'Name',
    'phone' => 'Phone',
    'address' => 'Address',
    'delivery_options' => 'Delivery options',
    'store_delivery_option' => 'Store delivery',
    'home_delivery_option' => 'Home delivery',
    'payment_options' => 'Payment options',
    'cash_on_delivery' => 'Cash on delivery', 
    'cash_on_delivery_details' => 'Pay for your order at the time of delivery', 
    'choose_option' => 'Choose an option', 
    'state' => 'State', 
    'city' => 'City', 
];