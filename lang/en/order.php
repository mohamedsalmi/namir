<?php

return [
    'order'=>'Order',
    'success'=>'Success',
    'writer'=>'Writer',
    'articles'=>'Article',
    'shipping_address'=>'Shipping Address',
    'house'=>'House',
    'track_order'=>'Track Order',
    'expected_date'=>'Expected Date Of Delivery',
    'price_details'=>'Price Details',
    'order_id'=>'Transaction ID',
    'success_paiement'=>'Payment Is Successfully And Your Order Is On The Way',
];