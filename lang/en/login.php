<?php

return [
    'log_in'=>'Log In',
    'log_out'=>'Log Out',
    'forgot_password'=>'Forgot Password ?',
    'remember_me'=>'Remember me',
    'log_in_your_account'=>'Log In Your Account',
];