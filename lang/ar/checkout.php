<?php

return [
    'order'=>'طلب',
    'order_summary' => 'ملخص الطلب',
    'shipping' => 'الشحن',
    'delivery_address' => 'عنوان التسليم',
    'name' => 'الاسم',
    'phone' => 'الهاتف',
    'address' => 'العنوان',
    'delivery_options' => 'خيارات التوصيل',
    'store_delivery_option' => 'التسليم في المتجر',
    'home_delivery_option' => 'التوصيل إلى المنزل',
    'payment_options' => 'خيارات الدفع',
    'cash_on_delivery' => 'الدفع عند الاستلام', 
    'cash_on_delivery_details' => 'ادفع مقابل طلبك وقت التسليم', // 'cash_on_delivery' => 'Pay for your order at the time of delivery', 
    'choose_option' => 'إختر', 
    'state' => 'الولاية', 
    'city' => 'المدينة', 
];