<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'هذه المعرفات لا تتطابق مع سجلاتنا',
    'password' => 'كلمة المرور المقدمة غير صحيحة.',
    'throttle' => 'محاولات تسجيل دخول كثيرة جدًا. يرجى المحاولة مرة أخرى :seconds ثواني.',
    'login'=>[
        'or'=>'أو',
        'login_with_google'=>'قم بالتسجيل عن طريق Google',
        'login_with_facebook'=>'قم بالتسجيل عن طريق Facebook',
    ],

    'forgot' => [
        'forgot_password' => 'نسيت كلمة السر',
        'send' => 'أرسل',
        'email' => 'البريد الإلكتروني',
        
    ],
    
    'reset' => [
        'request_reset_password' => 'لقد طلبت إعادة تعيين كلمة المرور الخاصة بك',
        'request_reset_message' => 'ستنتهي صلاحية رابط إعادة تعيين كلمة المرور خلال 60 دقيقة. إذا لم تطلب إعادة تعيين كلمة المرور ، فلا داعي لاتخاذ أي إجراء آخر.',
        'reset_password' => 'إعادة تعيين كلمة المرور',
        "password" => "كلمة المرور",
        'password_confirmation' => 'تأكيد كلمة المرور' ,

    ],

];
