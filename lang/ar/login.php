<?php

return [
    'log_in'=>'تسجيل الدخول',
    'log_out'=>'تسجيل الخروج',
    'remember_me'=>'تذكرني',
    'forgot_password'=>'نسيت رقمك السري؟',
    'log_in_your_account'=>'قم بتسجيل الدخول',
];