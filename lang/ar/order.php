<?php

return [
    'order'=>'طلب',
    'success'=>'ناجح',
    'articles'=>'منتج',
    'shipping_address'=>'عنوان الشحن',
    'price_details'=>'تفاصيل السعر',
    'writer'=>'الكاتب',
    'track_order'=>'متابعة الطلبية',
    'expected_date'=>'التاريخ المتوقع للتسليم',
    'house'=>'دار النشر',
    'order_id'=>'رقم الطلب',
    'success_paiement'=>'تم الدفع بنجاح وطلبك في الطريق',
];